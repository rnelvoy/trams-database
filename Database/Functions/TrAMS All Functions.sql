DELIMITER $$
CREATE FUNCTION `trams_increment_temp_app_no_sequence`(`p_id` VARCHAR(20), `p_fy` VARCHAR(20)) RETURNS int(11)
    NO SQL
BEGIN
  IF(SELECT COUNT(id) FROM trams_temp_application_number_sequence WHERE recipient_id=p_id AND fiscal_year=p_fy) = 0 THEN 
    INSERT INTO trams_temp_application_number_sequence(recipient_id, fiscal_year, sequence_no, identifier) 
    VALUES (p_id, p_fy, 0, CONCAT(p_id, p_fy));
  END IF;
  
  UPDATE trams_temp_application_number_sequence 
  SET sequence_no=last_insert_id(sequence_no+1) 
  WHERE recipient_id=p_id AND fiscal_year=p_fy;
  RETURN last_insert_id();
  
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_increment_fain_sequence`(`state_code` VARCHAR(50), `fiscal_year` VARCHAR(50)) RETURNS int(11)
    NO SQL
begin
    if(SELECT COUNT(*) from trams_fain_sequence where fain_state=state_code AND fain_fiscal_year_created=fiscal_year) = 0
        THEN INSERT INTO trams_fain_sequence(fain_state, fain_fiscal_year_created, fain_latest_sequence_no, fain_identifier) VALUES (state_code, fiscal_year, 0, CONCAT(state_code, fiscal_year));
    end if;
    update trams_fain_sequence set fain_latest_sequence_no=last_insert_id(fain_latest_sequence_no+1) where fain_state=state_code AND fain_fiscal_year_created=fiscal_year;
    return last_insert_id();
end$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_increment_po_sequence`(`state_code` VARCHAR(50), `section_code` VARCHAR(50), `activity_code` VARCHAR(50)) RETURNS int(11)
    NO SQL
begin
  if(SELECT COUNT(*) from trams_po_number_sequence where po_state=state_code AND po_section_code=section_code AND po_activity_code=activity_code) = 0
        THEN INSERT INTO trams_po_number_sequence (po_state, po_section_code, po_activity_code, po_latest_sequence_no, po_identifier) VALUES (state_code, section_code, activity_code, 0, CONCAT(state_code, section_code, activity_code));
    end if;

    update trams_po_number_sequence set po_latest_sequence_no=last_insert_id(po_latest_sequence_no+1) where po_state=state_code AND po_section_code=section_code AND po_activity_code=activity_code;
    return last_insert_id();
end$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `fntramsGetBaseAppCreationDateByContractAwardIdOrAppId`(`p_contractAwardId` INT(11), `p_appId` INT(11)) RETURNS datetime
BEGIN

--  changed data type from DATE to DATETIME 
DECLARE appCreation DATETIME;

/*
This function accepts two parameters, in which either one can be NULL, but not both.
The IF THEN statements below direct the logic to use either p_contractAwardId or p_appId
to find the appCreation date. If both parameters are passed in, the function will still
work as intended.
*/

IF (p_contractAwardId IS NULL OR p_contractAwardId = '')  THEN
  SET p_contractAwardId = (SELECT contractAward_id FROM trams_application WHERE id = p_appId);
END IF;

IF (p_contractAwardId IS NULL) THEN
-- removed DATE() casting from created_date_time in both SELECT statements
  SET appCreation = (SELECT created_date_time FROM trams_application WHERE id = p_appId);
ELSE SET appCreation = (SELECT created_date_time FROM trams_application WHERE contractAward_id = p_contractAwardId AND amendment_number = 0);
END IF;

IF appCreation IS NULL THEN
-- changed from '1901-01-01' to '1901-01-01 00:00:00'
  SET appCreation = '1901-01-01 00:00:00';
END IF;

RETURN appCreation;

END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_format_date`(p_date VARCHAR(30)) RETURNS varchar(30) CHARSET utf8
    NO SQL
BEGIN
    DECLARE dateString varchar(30);
    CASE 
	WHEN p_date IS NULL THEN
		set dateString = ' ';

	WHEN p_date <= ' ' THEN
		set dateString = ' ';

        WHEN DATE_FORMAT(p_date, '%Y-%m-%d') IS NOT NULL THEN
        	SET dateString = DATE_FORMAT(p_date, '%Y-%m-%d');
        
        WHEN p_date REGEXP '^[0-9]' THEN 
        	SET dateString = STR_TO_DATE(SUBSTRING_INDEX(p_date,' ', 1), '%c/%e/%Y');

	WHEN p_date REGEXP '^[A-Z]' THEN 
    		SET dateString = DATE(STR_TO_DATE(p_date, '%b %d %Y'));
        
        ELSE
        	SET dateString = '???';

    END CASE;
    
    IF (dateString LIKE '0000-00-00') THEN 
    	SET dateString = '???';
    END IF;

RETURN (dateString);

END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `fntramsGetApplicationPOPEndDateFinalized`(p_applicationId INT(11), p_contractAwardId INT(11)) RETURNS datetime
BEGIN

	/*
	Deployment script name: fntramsGetApplicationPOPEndDateFinalized
	Description: Function creation for fntramsGetApplicationPOPEndDateFinalized
	Created by: Garrison Dean (2019.01.22)
	*/

	DECLARE popEndDateFinalized DATETIME;
	
	IF (p_contractAwardId IS NULL OR p_contractAwardId = '')  THEN
		SET p_contractAwardid = (SELECT contractAward_id FROM trams_application WHERE id = p_applicationId);
	END IF;

	IF (p_applicationId IS NOT NULL AND p_applicationId <> '') THEN
		/*Get Finalized POP End Date By App Id*/
		SET popEndDateFinalized = (
			SELECT POPEndDate 
			FROM tramsPOPChangelog 
			WHERE ApplicationId = p_applicationId 
			AND isApproved = 1 
			AND isDeleted = 0 
			ORDER BY POPChangelogId DESC 
			LIMIT 1
		);
	END IF;
	
	IF (
		(popEndDateFinalized IS NULL OR popEndDateFinalized = '') AND 
		(p_contractAwardId IS NOT NULL AND p_contractAwardId <> '')	AND 
		(p_applicationId IS NOT NULL AND p_applicationId <> '')
	) THEN
		SET popEndDateFinalized = (
			SELECT POPEndDate 
			FROM tramsPOPChangelog 
			WHERE ContractAwardId = p_contractAwardId
			AND ApplicationId < p_applicationId
			AND isApproved = 1 
			AND isDeleted = 0 
			ORDER BY POPChangelogId DESC 
			LIMIT 1
		);
	END IF;

	RETURN popEndDateFinalized;

END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_format_user_roles`(`userGroups` VARCHAR(2048),
`roleList` VARCHAR(2048)) RETURNS varchar(2048) CHARSET utf8
    NO SQL
BEGIN

    declare _roleListOrig varchar(2048) default "";
    declare _userRolesList varchar(2048) default "";
    declare _userRolesList2 varchar(2048) default "";
    declare _roleName varchar(48) default "";
    declare _roleNameSearch varchar(48) default "";
    declare _roleStartIndex varchar(4) default "";
    declare _newRoleToEnd varchar(2048) default "";
    declare _newRole varchar(2048) default "";
    declare _allRoles varchar(2048) default "";
    declare _roleSearchString varchar(2048) default "";
    declare _allRolesLength varchar(3) default "";

set _userRolesList = userGroups;
set _roleListOrig = roleList;
		role_name_loop:
			loop
				if (_roleListOrig = '' or _roleListOrig is null) then
					leave role_name_loop;
				end if;
                
                set _roleName = substring_index(_roleListOrig, ',', 1);
                set _roleListOrig = mid(_roleListOrig, length(_roleName) + 2, length(_roleListOrig));
                
                set _roleStartIndex = locate(_roleName, _userRolesList);
                
                if _roleStartIndex > 0 then
                    set _newRoleToEnd = substring(_userRolesList, _roleStartIndex);
                    set _roleSearchString = locate(';', _newRoleToEnd);
					if _roleSearchString > 0 then 
						set _newRole = substring_index(_newRoleToEnd, ';', 1);
					else
						set _newRole = _newRoleToEnd;
					end if;
                    set _allRoles = concat(_allRoles, _newRole, "; ");
                    same_roles_loop:
						loop
							set _userRolesList2 = _newRoleToEnd;
                            set _roleSearchString = locate(';', _userRolesList2);
                            if _roleSearchString = 0 then
								leave same_roles_loop;
                            else
								set _roleSearchString = _roleSearchString + 1;
								set _userRolesList2 = substring(_userRolesList2, _roleSearchString);
								set _roleStartIndex = locate(_roleName, _userRolesList2);
								if _roleStartIndex = 0 then
									leave same_roles_loop;
								else
									set _newRoleToEnd = substring(_userRolesList2, _roleStartIndex);
									set _roleSearchString = locate(';', _newRoleToEnd);
									if _roleSearchString > 0 then 
										set _newRole = substring_index(_newRoleToEnd, ';', 1);
									else
										set _newRole = _newRoleToEnd;
									end if;
								end if;
								
							set _allRoles = concat(_allRoles, _newRole, "; ");

							end if;
						end loop same_roles_loop;
				end if;
                
			end loop role_name_loop;

			set _allRolesLength = length(_allRoles);
            set _allRolesLength = _allRolesLength - 2;
            set _allRoles = left(_allRoles, _allRolesLength);

RETURN _allRoles;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_current_fiscal_quarter`(p_date DATE) RETURNS int(11)
    NO SQL
BEGIN 
		SET @quarter =
		CASE
			WHEN MONTH(p_date) >= 10 AND MONTH(p_date) <= 12 THEN 1
			WHEN MONTH(p_date) >= 1 AND MONTH(p_date) <= 3 THEN 2
			WHEN MONTH(p_date) >= 4 AND MONTH(p_date) <= 6 THEN 3
			WHEN MONTH(p_date) >= 7 AND MONTH(p_date) <= 9 THEN 4
			ELSE ''
		END;
		
		RETURN @quarter;
	END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_current_fiscal_year`(`curDateTime` DATETIME) RETURNS int(11)
    NO SQL
BEGIN

DECLARE curYear INT;

SET curYear = YEAR(curDateTime);

IF curDateTime >= str_to_date(CONCAT("10/01/", curYear), "%m/%d/%Y") THEN
   RETURN curYear + 1;
ELSE
   RETURN curYear;
END IF;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_fta_funding_amt_pct_change`(`original_FTA_amount` decimal(19,2), revised_FTA_amount decimal(19,2)) RETURNS decimal(19,2)
    NO SQL
begin

return ifnull((
  case when (original_FTA_amount = 0) then 
    0 
  else 
    (((revised_FTA_amount - original_FTA_amount) * 100) / original_FTA_amount) end),0);

END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_ffr_period_name`(`reportPeriodType` VARCHAR(20), `fiscalYear` VARCHAR(4), `fiscalMonth` VARCHAR(20), `fiscalQuarter` INT) RETURNS varchar(50) CHARSET utf8
    NO SQL
begin

return (
    case when reportPeriodType = 'Monthly' then
        case when fiscalMonth > 0 and fiscalMonth <= 12 then 
                concat(fiscalYear,' ', monthname(str_to_date(fiscalMonth,'%m')))
            else '' 
            end
    when reportPeriodType = 'Quarterly' then 
    case when fiscalQuarter > 0 then 
        concat(fiscalYear,' Quarter ', fiscalQuarter ) 
    else '' 
    end
    when reportPeriodType = 'Annual' then 
    case when fiscalYear > 0 then 
      fiscalYear 
    else '' 
    end      
    else ''
    end
    );

END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_mpr_status`(`report_period_end_date` DATE, `orig_est_complete_date` DATE, `new_est_complete_date` DATE, `actual_complete_date` DATE, `revision` INT) RETURNS varchar(100) CHARSET utf8
BEGIN
  DECLARE p_result text;
  DECLARE ON_TRACK varchar(10);
  DECLARE PENDING varchar(10);
  DECLARE UPDATED varchar(10);
  DECLARE ZEROED_OUT varchar(10);
  SET ON_TRACK = 'ON TRACK';
  SET PENDING = 'PENDING';
  SET UPDATED = 'UPDATED';
  SET ZEROED_OUT = 'ZEROED OUT';
        
  IF orig_est_complete_date IS NULL THEN
    SET p_result = ZEROED_OUT; 
  ELSEIF report_period_end_date IS NULL THEN
    SET p_result = PENDING;    
  ELSEIF actual_complete_date IS NULL AND new_est_complete_date IS NULL THEN     
    IF orig_est_complete_date > report_period_end_date THEN
      SET p_result = ON_TRACK;
    ELSE
      SET p_result = PENDING;
    END IF;      
  ELSEIF actual_complete_date IS NULL AND new_est_complete_date <= report_period_end_date THEN
    SET p_result = PENDING;    
  ELSEIF actual_complete_date IS NULL AND new_est_complete_date > report_period_end_date THEN
    IF revision = 1 THEN 
      SET p_result = UPDATED;
    ELSE 
      SET p_result = ON_TRACK;
    END IF;
  ELSEIF actual_complete_date IS NOT NULL THEN
    IF revision = 1 THEN 
      SET p_result = UPDATED;
    ELSE 
      SET p_result = ON_TRACK;
    END IF;
  ELSE 
    SET p_result = '';
  END IF;

  RETURN p_result;
END$$
DELIMITER ;

DELIMITER $$
CREATE FUNCTION `trams_get_current_fiscal_quarter`(p_date DATE) RETURNS int(11)
    NO SQL
BEGIN 
		SET @quarter =
		CASE
			WHEN MONTH(p_date) >= 10 AND MONTH(p_date) <= 12 THEN 1
			WHEN MONTH(p_date) >= 1 AND MONTH(p_date) <= 3 THEN 2
			WHEN MONTH(p_date) >= 4 AND MONTH(p_date) <= 6 THEN 3
			WHEN MONTH(p_date) >= 7 AND MONTH(p_date) <= 9 THEN 4
			ELSE ''
		END;
		
		RETURN @quarter;
	END$$
DELIMITER ;