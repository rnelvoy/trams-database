-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2022 at 03:07 PM
-- Server version: 10.5.13-MariaDB-log
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Appian`
--

-- --------------------------------------------------------

--
-- Table structure for table `tramsAccountClassCodeReservation`
--

CREATE TABLE `tramsAccountClassCodeReservation` (
  `AccountClassCodeReservationId` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `ProjectId` int(11) NOT NULL,
  `ScopeCode` varchar(6) DEFAULT NULL,
  `ScopeSuffix` varchar(2) DEFAULT NULL,
  `FundingSourceName` varchar(255) NOT NULL,
  `FiscalYear` varchar(4) NOT NULL,
  `CostCenterCode` varchar(5) NOT NULL,
  `UzaCode` varchar(6) NOT NULL,
  `FundingFiscalYear` varchar(4) NOT NULL,
  `AppropriationCode` varchar(2) NOT NULL,
  `SectionCode` varchar(2) NOT NULL,
  `LimitationCode` varchar(2) NOT NULL,
  `AuthorityType` varchar(1) NOT NULL,
  `Fpc` varchar(2) NOT NULL,
  `ReservationAmount` decimal(19,2) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsAdHocTaskComments`
--

CREATE TABLE `tramsAdHocTaskComments` (
  `CommentId` int(11) NOT NULL,
  `CommentBy` int(11) DEFAULT NULL,
  `CommentText` varchar(4032) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CommentAdHocTaskId` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsAdHocTasks`
--

CREATE TABLE `tramsAdHocTasks` (
  `AdHocTaskId` int(11) NOT NULL,
  `Sender` int(11) DEFAULT NULL,
  `Recipient` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Description` varchar(4000) DEFAULT NULL,
  `TaskCreatedDate` datetime DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT 1,
  `ApplicationId` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsAmendmentErrorLog`
--

CREATE TABLE `tramsAmendmentErrorLog` (
  `AmendmentErrorLogId` int(11) NOT NULL,
  `SQLStateCode` varchar(5) DEFAULT NULL,
  `ErrorNumber` int(11) DEFAULT NULL,
  `MessageText` varchar(1000) DEFAULT NULL,
  `CallingObject` varchar(255) DEFAULT NULL,
  `OriginalApplicationId` int(11) DEFAULT NULL,
  `NewApplicationId` int(11) DEFAULT NULL,
  `OriginalApplicationNumber` varchar(510) DEFAULT NULL,
  `NewApplicationNumber` varchar(510) DEFAULT NULL,
  `TableName` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationDiscretionaryDetail`
--

CREATE TABLE `tramsApplicationDiscretionaryDetail` (
  `ApplicationDiscretionaryDetailId` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `TotalAppliedAmount` decimal(19,2) DEFAULT NULL,
  `TotalDiscretionaryFundingAmount` decimal(19,2) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `SubmittedBy` varchar(255) DEFAULT NULL,
  `SubmittedDate` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationHoldingPot`
--

CREATE TABLE `tramsApplicationHoldingPot` (
  `ApplicationHoldingPotId` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `BudgetRevisionId` int(11) DEFAULT NULL,
  `TypeId` int(11) NOT NULL,
  `UzaCode` varchar(6) NOT NULL,
  `CostCenterCode` varchar(5) NOT NULL,
  `AccountClassCode` varchar(15) NOT NULL,
  `FPC` varchar(2) NOT NULL,
  `HoldingAmount` decimal(19,2) NOT NULL,
  `ActiveFlag` tinyint(1) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationHoldingPotAuditHistory`
--

CREATE TABLE `tramsApplicationHoldingPotAuditHistory` (
  `ApplicationHoldingPotAuditHistoryId` int(11) NOT NULL,
  `ApplicationHoldingPotId` int(11) NOT NULL,
  `Action` varchar(255) NOT NULL,
  `ChangeAmount` decimal(19,2) NOT NULL,
  `ResultingAmount` decimal(19,2) NOT NULL,
  `ActionTakenBy` varchar(255) NOT NULL,
  `ActionTakenDate` datetime NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationHoldingPotReservation`
--

CREATE TABLE `tramsApplicationHoldingPotReservation` (
  `ApplicationHoldingPotReservationId` int(11) NOT NULL,
  `ApplicationHoldingPotId` int(11) NOT NULL,
  `ProjectId` int(11) NOT NULL,
  `ScopeCode` varchar(6) DEFAULT NULL,
  `ScopeSuffix` varchar(2) DEFAULT NULL,
  `FundingSourceName` varchar(255) NOT NULL,
  `ReservationAmount` decimal(19,2) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationReview`
--

CREATE TABLE `tramsApplicationReview` (
  `ApplicationReviewId` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `ReviewPhaseId` int(11) NOT NULL,
  `ReviewNumber` int(11) NOT NULL,
  `StatusId` int(11) NOT NULL,
  `ReviewStartDate` datetime NOT NULL,
  `ReviewEndDate` datetime DEFAULT NULL,
  `RoutedBy` varchar(255) DEFAULT NULL,
  `RoutedDate` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApplicationTransmissionHistory`
--

CREATE TABLE `tramsApplicationTransmissionHistory` (
  `Id` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `TransmittedDate` datetime DEFAULT NULL,
  `SubmittedDate` datetime DEFAULT NULL,
  `LapsingFundFlag` tinyint(1) DEFAULT NULL,
  `MPRReportFrequency` varchar(255) DEFAULT NULL,
  `FinancialReportFreq` varchar(255) DEFAULT NULL,
  `RevisionNumber` int(11) DEFAULT NULL,
  `IsLatest` tinyint(1) NOT NULL,
  `CreatedBy` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ModifiedBy` varchar(255) NOT NULL,
  `ModifiedOn` datetime NOT NULL,
  `IsDeleted` tinyint(1) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsApportionmentTransferDetails`
--

CREATE TABLE `tramsApportionmentTransferDetails` (
  `ApportionmentTransferDetailsId` int(11) NOT NULL,
  `BudgetRevisionId` int(11) DEFAULT NULL,
  `FromUzaCode` varchar(6) DEFAULT NULL,
  `FromAccountClassCode` varchar(15) DEFAULT NULL,
  `ToUzaCode` varchar(6) DEFAULT NULL,
  `ToAccountClassCode` varchar(15) DEFAULT NULL,
  `Amount` decimal(19,2) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `ApprovedBy` varchar(255) DEFAULT NULL,
  `ApprovedDate` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsArchiveAwardAuditLog`
--

CREATE TABLE `tramsArchiveAwardAuditLog` (
  `LogId` int(11) NOT NULL,
  `ApplicationId` int(11) DEFAULT NULL,
  `IsArchivedInTrAMS` tinyint(1) DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT NULL,
  `ArchivedBy` varchar(255) DEFAULT NULL,
  `ArchivedOn` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `ModifiedOn` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsAutomatedRecoveryRecords`
--

CREATE TABLE `tramsAutomatedRecoveryRecords` (
  `TramsAutomatedRecoveryRecordsId` int(11) NOT NULL,
  `DeobligationId` int(11) DEFAULT NULL,
  `AllotmentId` int(11) DEFAULT NULL,
  `OperatingBudgetId` int(11) DEFAULT NULL,
  `ManualAllotmentRecovery` tinyint(1) DEFAULT NULL,
  `ManualOperatingBudgetRecovery` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsDAFISSuffixCodeReference`
--

CREATE TABLE `tramsDAFISSuffixCodeReference` (
  `DAFISCodeId` int(11) NOT NULL,
  `DAFISCode` varchar(10) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsDaimsBusinessTypeReference`
--

CREATE TABLE `tramsDaimsBusinessTypeReference` (
  `BusinessTypeId` int(11) NOT NULL,
  `BusinessTypeCode` varchar(255) DEFAULT NULL,
  `BusinessTypeLabel` varchar(255) DEFAULT NULL,
  `BusinessTypeDescription` varchar(255) DEFAULT NULL,
  `CreatedDateTime` datetime DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `ModifiedDateTime` datetime DEFAULT NULL,
  `ModifiedBy` varchar(255) DEFAULT NULL,
  `IsActive` tinyint(1) DEFAULT 1
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsDiscretionaryChangelog`
--

CREATE TABLE `tramsDiscretionaryChangelog` (
  `DiscretionaryChangelogId` int(11) NOT NULL,
  `DiscretionaryProgramId` int(11) DEFAULT NULL,
  `ApplicationId` int(11) DEFAULT NULL,
  `SourceOfChangeId` int(11) DEFAULT NULL,
  `AmountUpdated` varchar(255) DEFAULT NULL,
  `TransactionAmount` decimal(19,2) DEFAULT NULL,
  `UpdatedBy` varchar(255) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsFYAPResultSet`
--

CREATE TABLE `tramsFYAPResultSet` (
  `Id` int(11) NOT NULL,
  `UZACode` varchar(6) DEFAULT NULL,
  `UZAReference` varchar(255) DEFAULT NULL,
  `UZAPopulation` int(11) DEFAULT NULL,
  `CostCenter` varchar(510) DEFAULT NULL,
  `AccountClassCode` varchar(40) DEFAULT NULL,
  `AppropriationCode` varchar(4) DEFAULT NULL,
  `SectionId` varchar(4) DEFAULT NULL,
  `LimitationCode` varchar(4) DEFAULT NULL,
  `FundingFiscalYear` varchar(4) DEFAULT NULL,
  `LapseYear` varchar(510) DEFAULT NULL,
  `ApportionmentInitialAmount` double DEFAULT NULL,
  `ApportionmentModifications` double DEFAULT NULL,
  `ApportionmentTransferIn` double DEFAULT NULL,
  `ApportionmentTransferOut` double DEFAULT NULL,
  `ApportionmentEffectiveAuthority` double DEFAULT NULL,
  `ApportionmentReservations` double DEFAULT NULL,
  `ApportionmentObligations` double DEFAULT NULL,
  `ApportionmentRecovery` double DEFAULT NULL,
  `ApportionmentAvailable` double DEFAULT NULL,
  `CapInitialAmount` double DEFAULT NULL,
  `CapModifications` double DEFAULT NULL,
  `CapTransferIn` double DEFAULT NULL,
  `CapTransferOut` double DEFAULT NULL,
  `CapEffectiveAuthority` double DEFAULT NULL,
  `CapReservations` double DEFAULT NULL,
  `CapObligations` double DEFAULT NULL,
  `CapRecovery` double DEFAULT NULL,
  `CapAvailable` double DEFAULT NULL,
  `CeilingInitialAmount` double DEFAULT NULL,
  `CeilingModifications` double DEFAULT NULL,
  `CeilingTransferIn` double DEFAULT NULL,
  `CeilingTransferOut` double DEFAULT NULL,
  `CeilingEffectiveAuthority` double DEFAULT NULL,
  `CeilingRecovery` double DEFAULT NULL,
  `CeilingAvailable` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsPOPChangelog`
--

CREATE TABLE `tramsPOPChangelog` (
  `POPChangelogId` int(11) NOT NULL,
  `ContractAwardId` int(11) DEFAULT NULL,
  `BudgetRevisionId` int(11) DEFAULT NULL,
  `ApplicationId` int(11) DEFAULT NULL,
  `SourceOfChangeId` int(11) DEFAULT NULL,
  `POPEndDate` datetime DEFAULT NULL,
  `RevisionNum` int(11) DEFAULT NULL,
  `UpdatedBy` varchar(255) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `IsApproved` tinyint(1) DEFAULT 0,
  `IsDeleted` tinyint(1) DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsRecoveredDeobligationDetail`
--

CREATE TABLE `tramsRecoveredDeobligationDetail` (
  `RecoveredDeobligationDetailId` int(11) NOT NULL,
  `DeobligationDetailId` int(11) NOT NULL,
  `RecoveredToOperatingBudget` tinyint(1) DEFAULT NULL,
  `RecoveredToApportionment` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsReservationSelectedAccountClassCode`
--

CREATE TABLE `tramsReservationSelectedAccountClassCode` (
  `ReservationSelectedAccountClassCodeId` int(11) NOT NULL,
  `ApplicationId` int(11) NOT NULL,
  `FiscalYear` varchar(4) NOT NULL,
  `CostCenterCode` varchar(5) NOT NULL,
  `UzaCode` varchar(6) NOT NULL,
  `FundingFiscalYear` varchar(4) NOT NULL,
  `AppropriationCode` varchar(2) NOT NULL,
  `SectionCode` varchar(2) NOT NULL,
  `LimitationCode` varchar(2) NOT NULL,
  `AuthorityType` varchar(1) NOT NULL,
  `Fpc` varchar(2) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsReviewDecision`
--

CREATE TABLE `tramsReviewDecision` (
  `ReviewId` int(11) NOT NULL,
  `ApplicationReviewId` int(11) NOT NULL,
  `ReviewTypeId` int(11) NOT NULL,
  `DecisionId` int(11) NOT NULL,
  `ReviewNumber` int(11) NOT NULL,
  `IsAdhoc` tinyint(1) NOT NULL,
  `IsConcurrence` tinyint(1) NOT NULL,
  `IsRequired` tinyint(1) NOT NULL,
  `AssignedDate` datetime DEFAULT NULL,
  `DecisionDate` datetime DEFAULT NULL,
  `DecisionBy` varchar(255) DEFAULT NULL,
  `Comment` varchar(10100) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `LastModifiedBy` varchar(255) DEFAULT NULL,
  `LastModifiedDate` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `tramsSystemAnnouncement`
--

CREATE TABLE `tramsSystemAnnouncement` (
  `TramsSystemAnnouncementId` int(11) NOT NULL,
  `AnnouncementGroup` int(11) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Message` varchar(2000) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Color` int(11) DEFAULT NULL,
  `CreatedBy` varchar(255) DEFAULT NULL,
  `CreatedDate` datetime DEFAULT NULL,
  `UpdatedBy` varchar(255) DEFAULT NULL,
  `UpdatedDate` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_account_class_code`
--

CREATE TABLE `trams_account_class_code` (
  `id` int(11) NOT NULL,
  `account_class_code` varchar(40) DEFAULT NULL,
  `acc_description` varchar(510) DEFAULT NULL,
  `code_previous_amount` decimal(19,2) DEFAULT NULL,
  `code_q1_amount` decimal(19,2) DEFAULT NULL,
  `code_q2_amount` decimal(19,2) DEFAULT NULL,
  `code_q3_amount` decimal(19,2) DEFAULT NULL,
  `code_q4_amount` decimal(19,2) DEFAULT NULL,
  `code_change_amount` decimal(19,2) DEFAULT NULL,
  `code_new_amount` decimal(19,2) DEFAULT NULL,
  `acc_new_flag` tinyint(1) DEFAULT NULL,
  `trmsprtn_ccntclss_prtngbdg` int(11) DEFAULT NULL,
  `trmsprtngbdg_ccntclsscds_dx` int(11) DEFAULT NULL,
  `cost_center_code` varchar(10) DEFAULT NULL,
  `fiscal_year` varchar(4) DEFAULT NULL,
  `authorized_flag` tinyint(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `acc_funding_fiscal_year` varchar(4) DEFAULT NULL,
  `acc_appropriation_code` varchar(4) DEFAULT NULL,
  `acc_section_code` varchar(4) DEFAULT NULL,
  `acc_limitation_code` varchar(4) DEFAULT NULL,
  `acc_auth_type` varchar(4) DEFAULT NULL,
  `cost_center_name` varchar(255) DEFAULT NULL,
  `budget_type` varchar(255) DEFAULT NULL,
  `code_transaction_type` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_account_class_code_history`
--

CREATE TABLE `trams_account_class_code_history` (
  `id` int(11) NOT NULL,
  `account_class_code_id` int(11) DEFAULT NULL,
  `account_class_code` varchar(40) DEFAULT NULL,
  `cost_center_code` varchar(6) DEFAULT NULL,
  `transaction_fiscal_year` varchar(4) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `section_id` varchar(4) DEFAULT NULL,
  `limitation_code` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(4) DEFAULT NULL,
  `transaction_number` varchar(50) DEFAULT NULL,
  `action_type` varchar(40) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(40) DEFAULT NULL,
  `comments` longtext DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `reserved_amount` double DEFAULT NULL,
  `unreserve_amount` double DEFAULT NULL,
  `obligated_amount` double DEFAULT NULL,
  `recovery_amount` double DEFAULT NULL,
  `transfer_in_amount` double DEFAULT NULL,
  `transfer_out_amount` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_account_class_code_lookup`
--

CREATE TABLE `trams_account_class_code_lookup` (
  `id` int(11) NOT NULL,
  `acc_description` varchar(510) DEFAULT NULL,
  `account_class_code` varchar(40) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(4) DEFAULT NULL,
  `section_code` varchar(4) DEFAULT NULL,
  `limitation_code` varchar(4) DEFAULT NULL,
  `authority_type` varchar(4) DEFAULT NULL,
  `original_lapse_year` varchar(4) DEFAULT NULL,
  `lapse_year` varchar(4) DEFAULT NULL,
  `appropriation_type_flag` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `dafis_suffix_code` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_acc_fpc_reference`
--

CREATE TABLE `trams_acc_fpc_reference` (
  `id` int(11) NOT NULL,
  `account_class_code` varchar(510) DEFAULT NULL,
  `fpc` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_ad_hoc_ffr_report`
--

CREATE TABLE `trams_ad_hoc_ffr_report` (
  `Recipient Id` varchar(255) DEFAULT NULL,
  `Grantee Acronym` varchar(510) DEFAULT NULL,
  `grantee_name` varchar(510) DEFAULT NULL,
  `Grantee Cost Center` varchar(255) DEFAULT NULL,
  `Award State` varchar(2) DEFAULT NULL,
  `Latest Award Fiscal Year` varchar(4) DEFAULT NULL,
  `Latest Award Number` varchar(510) DEFAULT NULL,
  `Latest Amendment Number` int(11) DEFAULT NULL,
  `Latest Award Name` longtext DEFAULT NULL,
  `Application Status` varchar(510) DEFAULT NULL,
  `Application Type` varchar(510) DEFAULT NULL,
  `Is Discretionary Grant` tinyint(1) DEFAULT NULL,
  `Grantee Point of Contact` varchar(255) DEFAULT NULL,
  `FTA Post Award Mgr` varchar(255) NOT NULL DEFAULT '',
  `Latest FFR Status` varchar(510) DEFAULT NULL,
  `Latest FFR Fiscal Year` varchar(510) DEFAULT NULL,
  `Latest FFR Period Type` varchar(510) DEFAULT NULL,
  `Period` varchar(50) DEFAULT NULL,
  `Is Final FFR` tinyint(1) DEFAULT NULL,
  `Submitted Date` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Submitted By` varchar(510) DEFAULT NULL,
  `Federal Cash on Hand Cumulative Beginning Amount` decimal(19,2) DEFAULT NULL,
  `Federal Cash Receipt Cumulative Amount` decimal(19,2) DEFAULT NULL,
  `Federal Cash Disbursement Cumulative Amount` decimal(19,2) DEFAULT NULL,
  `Federal Cash on Hand Cumulative End Amount` decimal(19,2) DEFAULT NULL,
  `Total Federal Authorized Amount` decimal(19,2) DEFAULT NULL,
  `Federal Share Expend Cumulative Amount` decimal(19,2) DEFAULT NULL,
  `Grantee Share Expend Cumulative Amount` decimal(19,2) DEFAULT NULL,
  `Total Expenditure Cumulative Amount` decimal(19,2) DEFAULT NULL,
  `Federal Share Unliquidated Obligation Amount` decimal(19,2) DEFAULT NULL,
  `Grantee Share Unliquidated Obligation Amount` decimal(19,2) DEFAULT NULL,
  `Total Unliquidated Obligation Amount` decimal(19,2) DEFAULT NULL,
  `Total Federal Share Amount` decimal(19,2) DEFAULT NULL,
  `Federal Unobligated Balance Amount` decimal(19,2) DEFAULT NULL,
  `ACC Deobligation Amount` double DEFAULT NULL,
  `Award Disbursement Amount` double DEFAULT NULL,
  `Award Unliquidated Amount` double NOT NULL DEFAULT 0,
  `Total Undisbursed Amount` double NOT NULL DEFAULT 0,
  `Award Obligated Date` datetime DEFAULT NULL,
  `Award Disbursement Date` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `Award Deobligated Date` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_allocation_type`
--

CREATE TABLE `trams_allocation_type` (
  `id` int(11) NOT NULL,
  `allocation_type_description` varchar(510) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_allotment_advice`
--

CREATE TABLE `trams_allotment_advice` (
  `id` int(11) NOT NULL,
  `program_type` varchar(510) DEFAULT NULL,
  `allotment_type_text` varchar(510) DEFAULT NULL,
  `control_number` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `authorized_by` varchar(510) DEFAULT NULL,
  `authorized_date` datetime DEFAULT NULL,
  `authorized_by_title` varchar(510) DEFAULT NULL,
  `advice_previous_total` decimal(19,2) DEFAULT NULL,
  `advice_previous_total_text` varchar(510) DEFAULT NULL,
  `advice_change_total` decimal(19,2) DEFAULT NULL,
  `advice_change_total_text` varchar(510) DEFAULT NULL,
  `advice_new_total` decimal(19,2) DEFAULT NULL,
  `advice_new_total_text` varchar(510) DEFAULT NULL,
  `carryover_previous_total` decimal(19,2) DEFAULT NULL,
  `carryover_change_total` decimal(19,2) DEFAULT NULL,
  `carryover_new_total` decimal(19,2) DEFAULT NULL,
  `recovery_previous_total` decimal(19,2) DEFAULT NULL,
  `recovery_change_total` decimal(19,2) DEFAULT NULL,
  `recovery_new_total` decimal(19,2) DEFAULT NULL,
  `remark` longtext DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_allotment_code`
--

CREATE TABLE `trams_allotment_code` (
  `id` int(11) NOT NULL,
  `allotmentAdviceID_int` int(11) DEFAULT NULL,
  `allotment_code_text` varchar(510) DEFAULT NULL,
  `code_description` varchar(510) DEFAULT NULL,
  `code_previous_amount` decimal(19,2) DEFAULT NULL,
  `code_previous_amount_text` varchar(510) DEFAULT NULL,
  `code_new_amount` decimal(19,2) DEFAULT NULL,
  `code_new_amount_text` varchar(510) DEFAULT NULL,
  `code_change_amount` decimal(19,2) DEFAULT NULL,
  `code_change_amount_text` varchar(510) DEFAULT NULL,
  `code_q1_amount` decimal(19,2) DEFAULT NULL,
  `code_q2_amount` decimal(19,2) DEFAULT NULL,
  `code_q3_amount` decimal(19,2) DEFAULT NULL,
  `code_q4_amount` decimal(19,2) DEFAULT NULL,
  `allotment_type_text` varchar(510) DEFAULT NULL,
  `authorized_flag` tinyint(1) DEFAULT NULL,
  `trmslltmntdvic_lltmntcd_idx` int(11) DEFAULT NULL,
  `advice_fiscal_year` varchar(10) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_allotment_code_lookup`
--

CREATE TABLE `trams_allotment_code_lookup` (
  `id` int(11) NOT NULL,
  `allotment_code_text` varchar(510) DEFAULT NULL,
  `allotment_code_description` varchar(510) DEFAULT NULL,
  `funding_fiscal_year` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `authorization_type` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application`
--

CREATE TABLE `trams_application` (
  `id` int(11) NOT NULL,
  `application_number` varchar(510) DEFAULT NULL,
  `temp_application_number` varchar(510) DEFAULT NULL,
  `application_name` longtext DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `dol_status` varchar(63) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `ueid` varchar(50) DEFAULT NULL,
  `recipient_name` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(4) DEFAULT NULL,
  `application_type` varchar(510) DEFAULT NULL,
  `application_description` longtext DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `delinquent_debt_flag` tinyint(1) DEFAULT NULL,
  `delinquent_debt_description` longtext DEFAULT NULL,
  `suballocation_funds` int(11) DEFAULT NULL,
  `indirect_costs` int(11) DEFAULT NULL,
  `indirect_cost_description` varchar(500) DEFAULT NULL,
  `research_development_flag` tinyint(1) DEFAULT NULL,
  `preaward_authority_flag` tinyint(1) DEFAULT NULL,
  `preaward_contract_number` varchar(510) DEFAULT NULL,
  `feed_id` varchar(20) DEFAULT NULL,
  `amendment_number` int(11) DEFAULT NULL,
  `amendment_reason` varchar(600) DEFAULT NULL,
  `amendment_details` varchar(510) DEFAULT NULL,
  `amended_by` varchar(510) DEFAULT NULL,
  `amended_date` datetime DEFAULT NULL,
  `contractAward_id` int(11) DEFAULT NULL,
  `trmscntrctwrd_wrdpplctns_dx` int(11) DEFAULT NULL,
  `trmspplctnwr_pplctnwrddtl_d` int(11) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `recipient_cost_center` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `discretionary_funds_flag` tinyint(1) DEFAULT NULL,
  `fta_contact_from_team` varchar(255) DEFAULT NULL,
  `recipient_contact` varchar(255) DEFAULT NULL,
  `app_from_team_flag` tinyint(1) DEFAULT NULL,
  `fta_pre_award_manager` varchar(255) DEFAULT NULL,
  `fta_post_award_manager` varchar(255) DEFAULT NULL,
  `application_folder_id` int(11) DEFAULT NULL,
  `fain_fiscal_year` varchar(255) DEFAULT NULL,
  `fain_state` varchar(255) DEFAULT NULL,
  `fain_sequence_number` varchar(255) DEFAULT NULL,
  `latest_approved_project_sequence_number` int(11) DEFAULT NULL,
  `latest_temporary_project_sequence_number` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `planning_flag` tinyint(1) DEFAULT NULL,
  `paper_flag` tinyint(1) DEFAULT NULL,
  `supp_agmt_flag` tinyint(1) DEFAULT NULL,
  `research_flag` tinyint(1) DEFAULT NULL,
  `special_cond_code` varchar(20) DEFAULT NULL,
  `special_cond_eff_date` datetime DEFAULT NULL,
  `special_cond_target_date` datetime DEFAULT NULL,
  `fed_domestic_asst_num` int(11) DEFAULT NULL,
  `budget_number` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_award_detail`
--

CREATE TABLE `trams_application_award_detail` (
  `id` int(11) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `final_budget_flag` tinyint(1) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `executed_by` varchar(255) DEFAULT NULL,
  `executed_by_title` varchar(255) DEFAULT NULL,
  `executed_by_organization` varchar(255) DEFAULT NULL,
  `executed_date_time` datetime DEFAULT NULL,
  `execution_comment` longtext DEFAULT NULL,
  `obligated_date_time` datetime DEFAULT NULL,
  `obligated_by` varchar(255) DEFAULT NULL,
  `obligated_by_title` varchar(255) DEFAULT NULL,
  `award_comment` longtext DEFAULT NULL,
  `frc_control_number` varchar(255) DEFAULT NULL,
  `total_eligible_cost_amount` double DEFAULT NULL,
  `total_fta_amount` double DEFAULT NULL,
  `total_state_amount` double DEFAULT NULL,
  `total_local_amount` double DEFAULT NULL,
  `total_other_fed_amount` double DEFAULT NULL,
  `total_toll_rev_credit_amount` double DEFAULT NULL,
  `total_adjustment_amount` double DEFAULT NULL,
  `closeout_amount` double DEFAULT NULL,
  `total_outyear_amount` double DEFAULT NULL,
  `total_planned_amount` double DEFAULT NULL,
  `total_under_contract` double DEFAULT NULL,
  `gross_project_cost_amount` double DEFAULT NULL,
  `total_encumbered_amount` double DEFAULT NULL,
  `closeout_requested_date` datetime DEFAULT NULL,
  `contract_withdrawn_date` datetime DEFAULT NULL,
  `deobligation_date_time` datetime DEFAULT NULL,
  `disbursement_date_time` datetime DEFAULT NULL,
  `fta_closeout_approval_date_time` datetime DEFAULT NULL,
  `fta_disapproved_date_time` datetime DEFAULT NULL,
  `record_to_frc_date` datetime DEFAULT NULL,
  `contract_complete_date` datetime DEFAULT NULL,
  `fain_assigned_date_time` datetime DEFAULT NULL,
  `scopes_updated_date_time` datetime DEFAULT NULL,
  `submitted_to_dol_date_time` datetime DEFAULT NULL,
  `dol_certification_date_time` datetime DEFAULT NULL,
  `stip_approval_date_time` datetime DEFAULT NULL,
  `upwp_approval_date_time` datetime DEFAULT NULL,
  `long_range_plan_approval_date_time` datetime DEFAULT NULL,
  `initial_concurrence_date_time` datetime DEFAULT NULL,
  `technical_concurrence_date_time` datetime DEFAULT NULL,
  `civil_rights_concurrence_date` datetime DEFAULT NULL,
  `environmental_concurrence_date_time` datetime DEFAULT NULL,
  `planning_concurrence_date_time` datetime DEFAULT NULL,
  `operations_concurrence_date_time` datetime DEFAULT NULL,
  `legal_concurrence_date_time` datetime DEFAULT NULL,
  `ra_concurrence_date_time` datetime DEFAULT NULL,
  `refund_date_time` datetime DEFAULT NULL,
  `obligation_fiscal_year` varchar(255) DEFAULT NULL,
  `deobligated_by` varchar(255) DEFAULT NULL,
  `deobligation_fiscal_year` varchar(255) DEFAULT NULL,
  `cumulative_contract_oblig_amount` double DEFAULT NULL,
  `cumulative_contract_fta_amount` double DEFAULT NULL,
  `amendment_oblig_amount` double DEFAULT NULL,
  `amendment_fta_amount` double DEFAULT NULL,
  `amendment_special_cond_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_gross_cost_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_total_eligible_cost_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_state_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_local_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_special_cond_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_contract_toll_rev_credit_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_fleet_status`
--

CREATE TABLE `trams_application_fleet_status` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `duns` varchar(255) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT NULL,
  `fleet_type` varchar(255) DEFAULT NULL,
  `peak_requirement_before` int(11) DEFAULT NULL,
  `peak_requirement_change` int(11) DEFAULT NULL,
  `peak_requirement_after` int(11) DEFAULT NULL,
  `spares_before` int(11) DEFAULT NULL,
  `spares_change` int(11) DEFAULT NULL,
  `spares_after` int(11) DEFAULT NULL,
  `active_total_before` int(11) DEFAULT NULL,
  `active_total_change` int(11) DEFAULT NULL,
  `active_total_after` int(11) DEFAULT NULL,
  `spare_ratio_before` double DEFAULT NULL,
  `spare_ratio_change` double DEFAULT NULL,
  `spare_ratio_after` double DEFAULT NULL,
  `contingency_before` int(11) DEFAULT NULL,
  `contingency_change` int(11) DEFAULT NULL,
  `contingency_after` int(11) DEFAULT NULL,
  `pending_disposal_before` int(11) DEFAULT NULL,
  `pending_disposal_change` int(11) DEFAULT NULL,
  `pending_disposal_after` int(11) DEFAULT NULL,
  `inactive_total_before` int(11) DEFAULT NULL,
  `inactive_total_change` int(11) DEFAULT NULL,
  `inactive_total_after` int(11) DEFAULT NULL,
  `comprehensive_total_before` int(11) DEFAULT NULL,
  `comprehensive_total_change` int(11) DEFAULT NULL,
  `comprehensive_total_after` int(11) DEFAULT NULL,
  `fleet_details` longtext DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_point_of_contact`
--

CREATE TABLE `trams_application_point_of_contact` (
  `id` int(11) NOT NULL,
  `applicationidint` int(11) DEFAULT NULL,
  `user_name` varchar(510) DEFAULT NULL,
  `role` varchar(510) DEFAULT NULL,
  `fta_contact_flag` tinyint(1) DEFAULT NULL,
  `trmspplctin_pintfcntcts_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_subrecipient`
--

CREATE TABLE `trams_application_subrecipient` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `subrecipient_name` varchar(510) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `subrecipient_dba_name` varchar(510) DEFAULT NULL,
  `subrecipient_address_line_1` varchar(510) DEFAULT NULL,
  `subrecipient_city` varchar(510) DEFAULT NULL,
  `subrecipient_state` varchar(510) DEFAULT NULL,
  `subrecipient_county` varchar(510) DEFAULT NULL,
  `subrecipient_zipcode` varchar(510) DEFAULT NULL,
  `subrecipient_ost_type_name` varchar(510) DEFAULT NULL,
  `subrecipient_congressional_district` longtext DEFAULT NULL,
  `subrecipient_performance_county` longtext DEFAULT NULL,
  `subaward_amount` int(11) DEFAULT NULL,
  `subaward_description` longtext DEFAULT NULL,
  `subaward_category` varchar(510) DEFAULT NULL,
  `subrecipient_ost_type_id` int(11) DEFAULT NULL,
  `subaward_amount_text` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_transaction`
--

CREATE TABLE `trams_application_transaction` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `recipient_name` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `allocation_amount` decimal(19,2) DEFAULT NULL,
  `allocation_amount_text` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `grantee_id` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_application_validation`
--

CREATE TABLE `trams_application_validation` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `no_project_flag` tinyint(1) DEFAULT NULL,
  `certification_assurance_year_flag` tinyint(1) DEFAULT NULL,
  `project_complete_flag` tinyint(1) DEFAULT NULL,
  `sam_expiration_flag` tinyint(1) DEFAULT NULL,
  `application_details_flag` tinyint(1) DEFAULT NULL,
  `discretionary_funds_flag` tinyint(1) DEFAULT NULL,
  `research_and_dev_flag` tinyint(1) DEFAULT 0,
  `end_date_flag` tinyint(1) DEFAULT 0,
  `end_date_before_start_date_flag` tinyint(1) DEFAULT 0,
  `end_date_before_closeout_create_date_flag` tinyint(1) DEFAULT 0,
  `suballocation_funds_answer_flag` tinyint(1) DEFAULT 0,
  `suballocation_funds_document_flag` tinyint(1) DEFAULT 0,
  `indirect_cost_answer_flag` tinyint(1) DEFAULT 0,
  `indirect_cost_details_flag` tinyint(1) DEFAULT 0,
  `omb_reqs_invalid_flag` tinyint(1) DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_apportionment`
--

CREATE TABLE `trams_apportionment` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `uza5337_id` int(11) DEFAULT NULL,
  `tramsstate_state_stateid` int(11) DEFAULT NULL,
  `apportionment` double DEFAULT NULL,
  `apportionmentTypeId` int(11) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `lapse_period` varchar(510) DEFAULT NULL,
  `section_id` varchar(4) DEFAULT NULL,
  `limitation_code` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(4) DEFAULT NULL,
  `operating_cap` double DEFAULT NULL,
  `ceiling_amount` double DEFAULT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_apportionment_history`
--

CREATE TABLE `trams_apportionment_history` (
  `id` int(11) NOT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `apportionment_id` int(11) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `section_id` varchar(4) DEFAULT NULL,
  `limitation_code` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(4) DEFAULT NULL,
  `transaction_number` varchar(510) DEFAULT NULL,
  `action_type` varchar(510) DEFAULT NULL,
  `is_prior_fy_recovery` tinyint(1) DEFAULT 0,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `comments` longtext DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `ceiling_amount` double DEFAULT NULL,
  `cap_amount` double DEFAULT NULL,
  `reserved_amount` double DEFAULT NULL,
  `unreserve_amount` double DEFAULT NULL,
  `obligated_amount` double DEFAULT NULL,
  `recovery_amount` double DEFAULT NULL,
  `transfer_in_amount` double DEFAULT NULL,
  `transfer_out_amount` double DEFAULT NULL,
  `modified_amount` double DEFAULT NULL,
  `split_amount` double DEFAULT NULL,
  `transaction_fiscal_year` varchar(4) DEFAULT NULL,
  `cap_reserved_amt` double DEFAULT NULL,
  `cap_unreserved_amt` double DEFAULT NULL,
  `cap_obligated_amt` double DEFAULT NULL,
  `cap_recovery_amt` double DEFAULT NULL,
  `cap_transfer_in_amt` double DEFAULT NULL,
  `cap_transfer_out_amt` double DEFAULT NULL,
  `cap_modify_amt` double DEFAULT NULL,
  `ceiling_recovery_amt` double DEFAULT NULL,
  `ceiling_transfer_in_amt` double DEFAULT NULL,
  `ceiling_transfer_out_amt` double DEFAULT NULL,
  `ceiling_modify_amt` double DEFAULT NULL,
  `ceiling_reserved_amt` double DEFAULT NULL,
  `ceiling_unreserve_amt` double DEFAULT NULL,
  `ceiling_obligated_amt` double DEFAULT NULL,
  `storage_amount` double DEFAULT NULL,
  `cap_storage_amount` double DEFAULT NULL,
  `extended_lapse_year` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_apportionment_type`
--

CREATE TABLE `trams_apportionment_type` (
  `id` int(11) NOT NULL,
  `apportionment_type_text` varchar(510) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_appropriations`
--

CREATE TABLE `trams_appropriations` (
  `id` int(11) NOT NULL,
  `preAwardID` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `initial_appropriation` double DEFAULT NULL,
  `adjusted_appropriation` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_appropriation_adjustments`
--

CREATE TABLE `trams_appropriation_adjustments` (
  `id` int(11) NOT NULL,
  `preAwardID` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `adjustment_name` varchar(510) DEFAULT NULL,
  `adjustment_value` double DEFAULT NULL,
  `adjustment_fiscal_year` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_appropriation_reference`
--

CREATE TABLE `trams_appropriation_reference` (
  `id` int(11) NOT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `appropriation_description` varchar(510) DEFAULT NULL,
  `treasury_symbol` varchar(510) DEFAULT NULL,
  `program_type` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL,
  `version` varchar(510) DEFAULT NULL,
  `treasury_symbol_1` varchar(5) DEFAULT NULL,
  `treasury_symbol_2` varchar(5) DEFAULT NULL,
  `treasury_symbol_3` varchar(5) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_app_congressional_release`
--

CREATE TABLE `trams_app_congressional_release` (
  `id` int(11) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `discretionary_title` varchar(510) DEFAULT NULL,
  `talking_point_overview` varchar(510) DEFAULT NULL,
  `talking_points` varchar(4500) DEFAULT NULL,
  `place_of_performance` varchar(510) DEFAULT NULL,
  `congressional_interest_text` varchar(510) DEFAULT NULL,
  `last_modified_date_time` varchar(510) DEFAULT NULL,
  `last_modified_by` varchar(510) DEFAULT NULL,
  `sent_to_release_date` varchar(510) DEFAULT NULL,
  `sent_to_release_by` varchar(510) DEFAULT NULL,
  `actual_release_date` varchar(510) DEFAULT NULL,
  `logged_release_date` varchar(510) DEFAULT NULL,
  `logged_release_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_app_discretionary_allocations`
--

CREATE TABLE `trams_app_discretionary_allocations` (
  `id` int(11) NOT NULL,
  `app_id` int(11) DEFAULT NULL,
  `discretionary_allocation_id` int(11) DEFAULT NULL,
  `discretionary_id` varchar(510) DEFAULT NULL,
  `discretionary_project_name` varchar(510) DEFAULT NULL,
  `application_applied_amount_text` varchar(510) DEFAULT NULL,
  `application_applied_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_audit_history`
--

CREATE TABLE `trams_audit_history` (
  `id` int(11) NOT NULL,
  `context_id` int(11) DEFAULT NULL,
  `context_details` varchar(510) DEFAULT NULL,
  `action_taken_by` varchar(510) DEFAULT NULL,
  `action_date_time` datetime DEFAULT NULL,
  `action_description` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_planning_summary`
--

CREATE TABLE `trams_budget_planning_summary` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `initiated_date_time` datetime DEFAULT NULL,
  `completed_date_time` datetime DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `initial_total_appropriation` varchar(510) DEFAULT NULL,
  `adjusted_total_appropriation` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_revision`
--

CREATE TABLE `trams_budget_revision` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `revision_status` varchar(510) DEFAULT NULL,
  `revision_reason` longtext DEFAULT NULL,
  `revision_description` longtext DEFAULT NULL,
  `change_size` tinyint(1) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `submitted_date_time` datetime DEFAULT NULL,
  `submitted_by` varchar(510) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `approved_by` varchar(510) DEFAULT NULL,
  `obligated_date_time` datetime DEFAULT NULL,
  `obligated_by` varchar(510) DEFAULT NULL,
  `executed_date_time` datetime DEFAULT NULL,
  `executed_by` varchar(510) DEFAULT NULL,
  `latest_revision_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5305d`
--

CREATE TABLE `trams_budget_summary_5305d` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `appropriation_value_text` varchar(510) DEFAULT NULL,
  `adjustment_name` varchar(510) DEFAULT NULL,
  `adjustment_value_text` varchar(510) DEFAULT NULL,
  `adjustment_fiscal_year` varchar(510) DEFAULT NULL,
  `allotment_name_text` varchar(510) DEFAULT NULL,
  `allotment_value_text` varchar(510) DEFAULT NULL,
  `allotment_percentage_text` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry530_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5305e`
--

CREATE TABLE `trams_budget_summary_5305e` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `appropriation_value_text` varchar(510) DEFAULT NULL,
  `adjustment_name` varchar(510) DEFAULT NULL,
  `adjustment_value_text` varchar(510) DEFAULT NULL,
  `adjustment_fiscal_year` varchar(510) DEFAULT NULL,
  `allotment_name_text` varchar(510) DEFAULT NULL,
  `allotment_value_text` varchar(510) DEFAULT NULL,
  `allotment_percentage_text` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry530_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5307`
--

CREATE TABLE `trams_budget_summary_5307` (
  `id` int(11) NOT NULL,
  `preawardidint` int(11) DEFAULT NULL,
  `grantnametext` varchar(510) DEFAULT NULL,
  `appropriationnametext` varchar(510) DEFAULT NULL,
  `appropriationvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry530_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5310`
--

CREATE TABLE `trams_budget_summary_5310` (
  `id` int(11) NOT NULL,
  `preawardidint` int(11) DEFAULT NULL,
  `grantnametext` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `appropriationvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry531_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5311`
--

CREATE TABLE `trams_budget_summary_5311` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `grantnametext` varchar(510) DEFAULT NULL,
  `appropriationnametext` varchar(510) DEFAULT NULL,
  `appropriation_value_text` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry531_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5337`
--

CREATE TABLE `trams_budget_summary_5337` (
  `id` int(11) NOT NULL,
  `preawardidint` int(11) DEFAULT NULL,
  `grantnametext` varchar(510) DEFAULT NULL,
  `appropriationnametext` varchar(510) DEFAULT NULL,
  `appropriationvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry533_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5339`
--

CREATE TABLE `trams_budget_summary_5339` (
  `id` int(11) NOT NULL,
  `preawardidint` int(11) DEFAULT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `appropriationnametext` varchar(510) DEFAULT NULL,
  `appropriationvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry533_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_budget_summary_5340`
--

CREATE TABLE `trams_budget_summary_5340` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `grantnametext` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `appropriationvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentnametext` varchar(510) DEFAULT NULL,
  `adjustmentvaluetext` varchar(510) DEFAULT NULL,
  `adjustmentfiscalyeartext` varchar(510) DEFAULT NULL,
  `allotmentnametext` varchar(510) DEFAULT NULL,
  `allotmentvaluetext` varchar(510) DEFAULT NULL,
  `allotmentpercentagetext` varchar(510) DEFAULT NULL,
  `trmsbdgtplnn_bdgtsmmry534_d` int(11) DEFAULT NULL,
  `trmsbdgtpln_bdgtsmmry53_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_categorical_exclusion`
--

CREATE TABLE `trams_categorical_exclusion` (
  `id` int(11) NOT NULL,
  `findingidint` int(11) DEFAULT NULL,
  `categorical_exclusion_name` varchar(255) DEFAULT NULL,
  `categorical_exclusion_description` longtext DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `trmsnvrnmnt_ctgrclxclsn_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_certification_assurance`
--

CREATE TABLE `trams_certification_assurance` (
  `id` int(11) NOT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `recipient_affirmation_text` longtext DEFAULT NULL,
  `attorney_affirmation_text` longtext DEFAULT NULL,
  `certification_assurance_doc_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_cert_assurance_lineitem`
--

CREATE TABLE `trams_cert_assurance_lineitem` (
  `id` int(11) NOT NULL,
  `certification_assurance_id` int(11) NOT NULL,
  `category` varchar(510) DEFAULT NULL,
  `title` varchar(510) DEFAULT NULL,
  `cert_description` longtext DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_civil_rights_compliance`
--

CREATE TABLE `trams_civil_rights_compliance` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `program_name` varchar(510) DEFAULT NULL,
  `current_status` varchar(510) DEFAULT NULL,
  `last_modified_by` varchar(510) DEFAULT NULL,
  `submitted_date_time` datetime DEFAULT NULL,
  `approved_date_time` datetime DEFAULT NULL,
  `expiration_date_time` datetime DEFAULT NULL,
  `due_date_time` datetime DEFAULT NULL,
  `extension_date_time` datetime DEFAULT NULL,
  `extension_days` int(11) DEFAULT NULL,
  `dbe_race_conscious_goal_percentage` double DEFAULT NULL,
  `dbe_race_neutral_goal_percentage` double DEFAULT NULL,
  `dbe_notes` longtext DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `triennial_goal_cycle` varchar(255) DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `cycle_group` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_civil_rights_grantee_history`
--

CREATE TABLE `trams_civil_rights_grantee_history` (
  `id` int(11) NOT NULL,
  `civil_rights_id` int(11) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `status_date_time` datetime DEFAULT NULL,
  `civilRightsId_int` int(11) DEFAULT NULL,
  `trmscvlrght_cvlrghtsgrn_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_congressional_appropriation`
--

CREATE TABLE `trams_congressional_appropriation` (
  `id` int(11) NOT NULL,
  `mod_reference_id` int(11) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `initiated_date_time` datetime DEFAULT NULL,
  `completed_date_time` datetime DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `funding_law_name` varchar(510) DEFAULT NULL,
  `funding_law_description` varchar(510) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `appropriation_type` varchar(510) DEFAULT NULL,
  `initial_total_appropriation` decimal(19,2) DEFAULT NULL,
  `adjusted_total_appropriation` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_contract_acc_funding`
--

CREATE TABLE `trams_contract_acc_funding` (
  `id` int(11) NOT NULL,
  `trmscntrctw_cntrctccfnd_dx` int(11) DEFAULT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  `project_number` varchar(255) DEFAULT NULL,
  `scope_number` varchar(255) DEFAULT NULL,
  `scope_name` varchar(255) DEFAULT NULL,
  `account_class_code` varchar(255) DEFAULT NULL,
  `cost_center_code` varchar(255) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `uza_code` varchar(255) DEFAULT NULL,
  `fpc` varchar(255) DEFAULT NULL,
  `amendment_amount_current` decimal(19,2) DEFAULT NULL,
  `contract_amount_cumulative` decimal(19,2) DEFAULT NULL,
  `funding_fiscal_year` varchar(255) DEFAULT NULL,
  `authorization_type` varchar(255) DEFAULT NULL,
  `appropriation_code` varchar(255) DEFAULT NULL,
  `section_code` varchar(255) DEFAULT NULL,
  `limitation_code` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `po_number` varchar(45) DEFAULT NULL,
  `project_number_sequence_number` varchar(255) DEFAULT NULL,
  `scope_suffix` varchar(10) DEFAULT NULL,
  `last_disbursement_date` datetime DEFAULT NULL,
  `object_class` varchar(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_contract_acc_transaction`
--

CREATE TABLE `trams_contract_acc_transaction` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `transaction_origin_ref_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `application_number` varchar(20) DEFAULT NULL,
  `project_number` varchar(20) DEFAULT NULL,
  `echo_number` varchar(255) DEFAULT NULL,
  `transaction_fiscal_year` varchar(4) DEFAULT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `scope_code` varchar(10) DEFAULT NULL,
  `scope_name` varchar(255) DEFAULT NULL,
  `account_class_code` varchar(40) DEFAULT NULL,
  `fpc` varchar(4) DEFAULT NULL,
  `cost_center_code` varchar(6) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `type_authority` varchar(4) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `appropriation_code` varchar(4) DEFAULT NULL,
  `section_code` varchar(4) DEFAULT NULL,
  `limitation_code` varchar(4) DEFAULT NULL,
  `transaction_date_time` datetime DEFAULT NULL,
  `transaction_by` varchar(255) DEFAULT NULL,
  `transaction_doc_num` varchar(255) DEFAULT NULL,
  `trams_entry_date_time` datetime DEFAULT NULL,
  `echo_payment_identifier` varchar(255) DEFAULT NULL,
  `object_class` varchar(20) DEFAULT NULL,
  `dafis_suffix_code` varchar(255) DEFAULT NULL,
  `obligation_amount` double DEFAULT 0,
  `deobligation_amount` double DEFAULT 0,
  `disbursement_amount` double DEFAULT 0,
  `refund_amount` double DEFAULT 0,
  `authorized_disbursement_amount` double DEFAULT 0,
  `run_number` varchar(255) DEFAULT NULL,
  `transaction_code` varchar(255) DEFAULT NULL,
  `transaction_status_code` varchar(255) DEFAULT NULL,
  `app_amendment_number` int(11) DEFAULT NULL,
  `fpc_description` varchar(255) DEFAULT NULL,
  `po_number` varchar(45) DEFAULT NULL,
  `from_trams` tinyint(1) DEFAULT NULL,
  `scope_suffix` varchar(45) DEFAULT NULL,
  `tbp_process_date` datetime DEFAULT NULL,
  `booked` tinyint(1) DEFAULT NULL,
  `transaction_description` varchar(255) DEFAULT NULL,
  `is_reconciled` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_contract_award`
--

CREATE TABLE `trams_contract_award` (
  `id` int(11) NOT NULL,
  `application_name` varchar(255) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  `application_type` varchar(255) DEFAULT NULL,
  `current_amendment_number` int(11) DEFAULT NULL,
  `cost_center_name` varchar(255) DEFAULT NULL,
  `cost_center_code` varchar(255) DEFAULT NULL,
  `frc_control_number` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `clearinghouse_number` varchar(30) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `contract_number` varchar(50) DEFAULT NULL,
  `first_obligation_date` datetime DEFAULT NULL,
  `last_obligation_date` datetime DEFAULT NULL,
  `contract_complete_date` datetime DEFAULT NULL,
  `closeout_requested_date` datetime DEFAULT NULL,
  `contract_withdrawn_date` datetime DEFAULT NULL,
  `first_deobligation_date` datetime DEFAULT NULL,
  `last_deobligation_date` datetime DEFAULT NULL,
  `last_disbursement_date` datetime DEFAULT NULL,
  `fta_closeout_approval_date` datetime DEFAULT NULL,
  `fta_disapproved_date` datetime DEFAULT NULL,
  `record_to_frc_date` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_contract_award_funding_acc_mpr`
--

CREATE TABLE `trams_contract_award_funding_acc_mpr` (
  `grantee_id` varchar(510) DEFAULT NULL,
  `contract_award_id` varchar(510) DEFAULT NULL,
  `application_id` varchar(510) DEFAULT NULL,
  `application_number` varchar(20) DEFAULT NULL,
  `amendment_number` int(11) DEFAULT NULL,
  `obligation_amount` double DEFAULT NULL,
  `disbursement_amount` double DEFAULT NULL,
  `unliquid_amount` double NOT NULL DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_cost_center`
--

CREATE TABLE `trams_cost_center` (
  `id` int(11) NOT NULL,
  `cost_center_code` varchar(510) DEFAULT NULL,
  `office_description` varchar(510) DEFAULT NULL,
  `cost_center_shortname` varchar(510) DEFAULT NULL,
  `office_acronym` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_created_document_folders`
--

CREATE TABLE `trams_created_document_folders` (
  `id` int(11) NOT NULL,
  `attachments_id` int(11) NOT NULL,
  `type` varchar(510) DEFAULT NULL,
  `createdFolderIds` varchar(128) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_cumulative_apportionment_temp`
--

CREATE TABLE `trams_cumulative_apportionment_temp` (
  `UZA_Code` varchar(50) DEFAULT NULL,
  `UZA_Code_Name` varchar(200) DEFAULT NULL,
  `UZA_Population` bigint(20) DEFAULT NULL,
  `Cost_Center_Short_Name` varchar(200) DEFAULT NULL,
  `Account_Class_Code` varchar(50) DEFAULT NULL,
  `Appropriation_Code` varchar(5) DEFAULT NULL,
  `Section_Id` varchar(5) DEFAULT NULL,
  `Limitation_Code` varchar(5) DEFAULT NULL,
  `Funding_Fiscal_Year` varchar(5) DEFAULT NULL,
  `Lapse_Year` varchar(5) DEFAULT NULL,
  `Apportionment_Initial_Amount` bigint(20) DEFAULT NULL,
  `Modification_Amount` bigint(20) DEFAULT NULL,
  `Transfer_In_Amount` bigint(20) DEFAULT NULL,
  `Transfer_Out_Amount` bigint(20) DEFAULT NULL,
  `Apportionment_Effective_Authority` bigint(20) DEFAULT NULL,
  `Reserved_Amount` bigint(20) DEFAULT NULL,
  `Obligated_Amount` bigint(20) DEFAULT NULL,
  `Recovery_Amount` bigint(20) DEFAULT NULL,
  `Apportionment_Available_Balance` bigint(20) DEFAULT NULL,
  `Cap_amount` bigint(20) DEFAULT NULL,
  `Cap_Modify_Amount` bigint(20) DEFAULT NULL,
  `Cap_Transfer_In_Amount` bigint(20) DEFAULT NULL,
  `Cap_Transfer_Out_Amount` bigint(20) DEFAULT NULL,
  `Cap_Effective_Authority` bigint(20) DEFAULT NULL,
  `Cap_Reserved_Amount` bigint(20) DEFAULT NULL,
  `Cap_Obligated_Amount` bigint(20) DEFAULT NULL,
  `Cap_Recovery_Amount` bigint(20) DEFAULT NULL,
  `Cap_Available_Balance` bigint(20) DEFAULT NULL,
  `Ceiling_Amount` bigint(20) DEFAULT NULL,
  `Ceiling_Modify_Amount` bigint(20) DEFAULT NULL,
  `Ceiling_Transfer_In_Amount` bigint(20) DEFAULT NULL,
  `Ceiling_Transfer_Out_Amount` bigint(20) DEFAULT NULL,
  `Ceiling_Executive_Authority` bigint(20) DEFAULT NULL,
  `Ceiling_Recovery_Amount` bigint(20) DEFAULT NULL,
  `Ceiling_Available_Balance` bigint(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_dbe_report`
--

CREATE TABLE `trams_dbe_report` (
  `id` int(11) NOT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `cost_center_code` varchar(255) DEFAULT NULL,
  `report_period` varchar(255) DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL,
  `report_quarter` int(11) DEFAULT NULL,
  `Version` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `report_due_date_time` datetime DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `submitted_date_time` datetime DEFAULT NULL,
  `prime_total_dollars` decimal(19,2) DEFAULT NULL,
  `prime_total_number` int(11) DEFAULT NULL,
  `prime_race_conscious_dollars` decimal(19,2) DEFAULT NULL,
  `prime_race_conscious_number` int(11) DEFAULT NULL,
  `prime_race_neutral_dollars` decimal(19,2) DEFAULT NULL,
  `prime_race_neutral_number` int(11) DEFAULT NULL,
  `sub_total_dollars` decimal(19,2) DEFAULT NULL,
  `sub_total_number` int(11) DEFAULT NULL,
  `sub_race_conscious_dollars` decimal(19,2) DEFAULT NULL,
  `sub_race_conscious_number` int(11) DEFAULT NULL,
  `sub_race_neutral_dollars` decimal(19,2) DEFAULT NULL,
  `sub_race_neutral_number` int(11) DEFAULT NULL,
  `total_number_blackamer` int(11) DEFAULT NULL,
  `total_number_hispanicamer` int(11) DEFAULT NULL,
  `total_number_nativeamer` int(11) DEFAULT NULL,
  `total_number_subcontasian` int(11) DEFAULT NULL,
  `total_number_asianpacific` int(11) DEFAULT NULL,
  `total_number_nonminority` int(11) DEFAULT NULL,
  `total_number_other` int(11) DEFAULT NULL,
  `total_dollar_blackamer` decimal(19,2) DEFAULT NULL,
  `total_dollar_hispanicamer` decimal(19,2) DEFAULT NULL,
  `total_dollar_nativeamer` decimal(19,2) DEFAULT NULL,
  `total_dollar_subcontasian` decimal(19,2) DEFAULT NULL,
  `total_dollar_asianpacific` decimal(19,2) DEFAULT NULL,
  `total_dollar_nonminority` decimal(19,2) DEFAULT NULL,
  `total_dollar_other` decimal(19,2) DEFAULT NULL,
  `race_conscious_prime_comp_number` int(11) DEFAULT NULL,
  `race_conscious_prime_comp_dollar` decimal(19,2) DEFAULT NULL,
  `race_conscious_part_needed` decimal(19,2) DEFAULT NULL,
  `race_conscious_dbe_part` decimal(19,2) DEFAULT NULL,
  `race_neutral_prime_comp_number` int(11) DEFAULT NULL,
  `race_neutral_prime_comp_dollar` decimal(19,2) DEFAULT NULL,
  `race_neutral_dbe_part` decimal(19,2) DEFAULT NULL,
  `arra_race_conscious_goal_percentage` int(11) DEFAULT NULL,
  `arra_race_neutral_goal_percentage` int(11) DEFAULT NULL,
  `reason_for_rejection` longtext DEFAULT NULL,
  `women_number_blackamer` int(11) DEFAULT NULL,
  `women_number_hispanicamer` int(11) DEFAULT NULL,
  `women_number_nativeamer` int(11) DEFAULT NULL,
  `women_number_asianpacific` int(11) DEFAULT NULL,
  `women_number_subcontasian` int(11) DEFAULT NULL,
  `women_number_nonminority` int(11) DEFAULT NULL,
  `women_number_other` int(11) DEFAULT NULL,
  `women_dollar_blackamer` decimal(19,2) DEFAULT NULL,
  `women_dollar_hispanicamer` decimal(19,2) DEFAULT NULL,
  `women_dollar_nativeamer` decimal(19,2) DEFAULT NULL,
  `women_dollar_asianpacific` decimal(19,2) DEFAULT NULL,
  `women_dollar_subcontasian` decimal(19,2) DEFAULT NULL,
  `women_dollar_nonminority` decimal(19,2) DEFAULT NULL,
  `women_dollar_other` decimal(19,2) DEFAULT NULL,
  `men_number_blackamer` int(11) DEFAULT NULL,
  `men_number_hispanicamer` int(11) DEFAULT NULL,
  `men_number_nativeamer` int(11) DEFAULT NULL,
  `men_number_asianpacific` int(11) DEFAULT NULL,
  `men_number_subcontasian` int(11) DEFAULT NULL,
  `men_number_nonminority` int(11) DEFAULT NULL,
  `men_number_other` int(11) DEFAULT NULL,
  `men_dollar_blackamer` decimal(19,2) DEFAULT NULL,
  `men_dollar_hispanicamer` decimal(19,2) DEFAULT NULL,
  `men_dollar_nativeamer` decimal(19,2) DEFAULT NULL,
  `men_dollar_asianpacific` decimal(19,2) DEFAULT NULL,
  `men_dollar_subcontasian` decimal(19,2) DEFAULT NULL,
  `men_dollar_nonminority` decimal(19,2) DEFAULT NULL,
  `men_dollar_other` decimal(19,2) DEFAULT NULL,
  `total_number_conts_prog` int(11) DEFAULT NULL,
  `total_dollars_prog` decimal(19,2) DEFAULT NULL,
  `total_number_dbe_conts_prog` int(11) DEFAULT NULL,
  `total_payments_to_dbe_firms_prog` decimal(19,2) DEFAULT NULL,
  `total_number_dbe_firms_prog` int(11) DEFAULT NULL,
  `DBERaceConsciousGoalPercentage` decimal(5,2) DEFAULT NULL,
  `DBERaceNeutralGoalPercentage` decimal(5,2) DEFAULT NULL,
  `is_report_new` tinyint(1) DEFAULT NULL,
  `IsLatest` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_deobligation_detail`
--

CREATE TABLE `trams_deobligation_detail` (
  `id` int(11) NOT NULL,
  `deobligated_fiscal_year` varchar(6) DEFAULT NULL,
  `acc_funding_fiscal_year` varchar(4) DEFAULT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `current_amendment_id` int(11) DEFAULT NULL,
  `acc_section_code` varchar(4) DEFAULT NULL,
  `acc_limitation_code` varchar(4) DEFAULT NULL,
  `acc_authorization_type` varchar(2) DEFAULT NULL,
  `acc_appropriation_code` varchar(4) DEFAULT NULL,
  `transaction_origin_ref_id` int(11) DEFAULT NULL,
  `deobligated_recovery_type` varchar(255) DEFAULT NULL,
  `deobligated_recovery_amount` decimal(19,2) DEFAULT NULL,
  `deobligated_recovery_date_time` datetime DEFAULT NULL,
  `deobligated_recovery_by` varchar(255) DEFAULT NULL,
  `earmark_flag` tinyint(1) DEFAULT NULL,
  `amendment_id` int(11) DEFAULT NULL,
  `BudgetRevisionId` int(11) DEFAULT NULL,
  `acc_cost_center_code` varchar(7) DEFAULT NULL,
  `acc_uza_code` varchar(8) DEFAULT NULL,
  `account_class_code` varchar(17) DEFAULT NULL,
  `acc_fpc` varchar(4) DEFAULT NULL,
  `obligation_fiscal_year` varchar(6) DEFAULT NULL,
  `deobligated_still_pending` tinyint(1) DEFAULT NULL,
  `FinalizedDate` datetime DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `scope_number` varchar(20) DEFAULT NULL,
  `po_number` varchar(45) DEFAULT NULL,
  `project_number_sequence_number` varchar(255) DEFAULT NULL,
  `scope_suffix` varchar(10) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_designated_recipients`
--

CREATE TABLE `trams_designated_recipients` (
  `id` int(11) NOT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `section_code` varchar(20) DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_direct_recipients`
--

CREATE TABLE `trams_direct_recipients` (
  `id` int(11) NOT NULL,
  `designated_recipient_grantee_id` varchar(255) DEFAULT NULL,
  `direct_recipient_grantee_id` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_discretionary_allocations`
--

CREATE TABLE `trams_discretionary_allocations` (
  `id` int(11) NOT NULL,
  `discretionary_id` varchar(510) DEFAULT NULL,
  `assigned_account_class_code` varchar(15) DEFAULT NULL,
  `original_fiscal_year` varchar(4) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `discretionary_fiscal_year` varchar(510) DEFAULT NULL,
  `state` varchar(510) DEFAULT NULL,
  `cost_center_code` varchar(5) DEFAULT NULL,
  `program_name` varchar(510) DEFAULT NULL,
  `discretionary_project_name` varchar(510) DEFAULT NULL,
  `lapse_year` varchar(510) DEFAULT NULL,
  `fta_program_manager` varchar(510) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `public_allocation_amount` double DEFAULT NULL,
  `unobligated_balance_amount` double DEFAULT NULL,
  `unobligated_balance_date_time` datetime DEFAULT NULL,
  `notes` longtext DEFAULT NULL,
  `regional_comment` longtext DEFAULT NULL,
  `last_modified_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(510) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_discretionary_program_lookup`
--

CREATE TABLE `trams_discretionary_program_lookup` (
  `id` int(11) NOT NULL,
  `discretionary_program_name` varchar(510) DEFAULT NULL,
  `section_id` varchar(510) DEFAULT NULL,
  `section_code` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `program_description` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_document`
--

CREATE TABLE `trams_document` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `document_context` varchar(510) DEFAULT NULL,
  `document_type` varchar(510) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `effective_date_description` varchar(510) DEFAULT NULL,
  `document_name` varchar(510) DEFAULT NULL,
  `document_description` longtext DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `environmental_finding_id` int(11) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `tramsprojectdocumentsid` int(11) DEFAULT NULL,
  `tramsproject_documents_id` int(11) DEFAULT NULL,
  `tramsproject_documents_idx` int(11) DEFAULT NULL,
  `tramsapplicatin_dcments_idx` int(11) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `tramsApplication_documents_id` int(11) DEFAULT NULL,
  `tramsapplicatin_docments_id` int(11) DEFAULT NULL,
  `deleted_date_time` datetime DEFAULT NULL,
  `document_category` varchar(100) DEFAULT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `dbe_report_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_document_content_reference`
--

CREATE TABLE `trams_document_content_reference` (
  `id` int(11) NOT NULL,
  `category` varchar(128) DEFAULT NULL,
  `content_location` varchar(128) DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_entity`
--

CREATE TABLE `trams_entity` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `trmsgnrlbsnssinf_gnrlinf_id` int(11) DEFAULT NULL,
  `trmsnttyfinncil_finncils_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_entity_financial`
--

CREATE TABLE `trams_entity_financial` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `credit_card_usage` varchar(510) DEFAULT NULL,
  `delinquent_debt_description` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_environmental_finding`
--

CREATE TABLE `trams_environmental_finding` (
  `id` int(11) NOT NULL,
  `class_level_name` varchar(510) DEFAULT NULL,
  `class_description` longtext DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_environmental_finding_date`
--

CREATE TABLE `trams_environmental_finding_date` (
  `id` int(11) NOT NULL,
  `findingidint` int(11) DEFAULT NULL,
  `date_description` longtext DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `trmsnvrnmntl_nvrnmntldts_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fain_sequence`
--

CREATE TABLE `trams_fain_sequence` (
  `fain_state` varchar(255) DEFAULT NULL,
  `fain_fiscal_year_created` varchar(255) DEFAULT NULL,
  `fain_latest_sequence_no` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `fain_identifier` varchar(255) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fda_number`
--

CREATE TABLE `trams_fda_number` (
  `id` int(11) NOT NULL,
  `fed_domestic_asst_num` int(10) DEFAULT NULL,
  `fed_domestic_asst_desc` varchar(200) DEFAULT NULL,
  `cfda_program_number` varchar(10) DEFAULT NULL,
  `cfda_program_title` varchar(100) DEFAULT NULL,
  `team_code_cat_id` int(10) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_federal_financial_report`
--

CREATE TABLE `trams_federal_financial_report` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `report_period_type` varchar(510) DEFAULT NULL,
  `report_status` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `fiscal_quarter` int(11) DEFAULT NULL,
  `fiscal_month` varchar(510) DEFAULT NULL,
  `final_report_flag` tinyint(1) DEFAULT NULL,
  `initial_submission_date_time` datetime DEFAULT NULL,
  `initial_submission_by` varchar(510) DEFAULT NULL,
  `last_modified_by` varchar(510) DEFAULT NULL,
  `last_modified_date_time` datetime DEFAULT NULL,
  `fta_review_complete_flag` tinyint(1) DEFAULT NULL,
  `fta_review_completed_by` varchar(510) DEFAULT NULL,
  `fta_review_completed_date_time` datetime DEFAULT NULL,
  `indirect_rate_type` varchar(510) DEFAULT NULL,
  `indirect_rate` decimal(19,2) DEFAULT NULL,
  `indirect_base_amount` decimal(19,2) DEFAULT NULL,
  `indirect_total_amount_charged` decimal(19,2) DEFAULT NULL,
  `indirect_federal_share_amount` decimal(19,2) DEFAULT NULL,
  `indirect_period_from_date_time` datetime DEFAULT NULL,
  `indirect_period_to_date_time` datetime DEFAULT NULL,
  `fed_cash_onhand_previous_begin_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_onhand_current_begin_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_onhand_cum_begin_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_receipt_previous_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_receipt_current_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_receipt_cum_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_disburse_previous_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_disburse_current_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_disburse_cum_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_onhand_previous_end_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_onhand_current_end_amt` decimal(19,2) DEFAULT NULL,
  `fed_cash_onhand_cum_end_amt` decimal(19,2) DEFAULT NULL,
  `total_federal_authorized_amount` decimal(19,2) DEFAULT NULL,
  `fed_share_expend_previous_amt` decimal(19,2) DEFAULT NULL,
  `fed_share_expend_current_amt` decimal(19,2) DEFAULT NULL,
  `fed_share_expend_cum_amt` decimal(19,2) DEFAULT NULL,
  `recipient_share_expend_previous_amt` decimal(19,2) DEFAULT NULL,
  `recipient_share_expend_current_amt` decimal(19,2) DEFAULT NULL,
  `recipient_share_expend_cum_amt` decimal(19,2) DEFAULT NULL,
  `total_expenditure_previous_amount` decimal(19,2) DEFAULT NULL,
  `total_expenditure_current_amount` decimal(19,2) DEFAULT NULL,
  `total_expenditure_cumulative_amount` decimal(19,2) DEFAULT NULL,
  `fed_share_unliquid_obligation_amt` decimal(19,2) DEFAULT NULL,
  `recipient_share_unliquid_obligation_amt` decimal(19,2) DEFAULT NULL,
  `total_unliquid_obligation_amount` decimal(19,2) DEFAULT NULL,
  `total_federal_share_amount` decimal(19,2) DEFAULT NULL,
  `fed_unobligated_balance_amt` decimal(19,2) DEFAULT NULL,
  `recipient_total_share_required_amt` decimal(19,2) DEFAULT NULL,
  `recipient_remain_share_toprovide_amt` decimal(19,2) DEFAULT NULL,
  `fed_program_income_onhand_amt` decimal(19,2) DEFAULT NULL,
  `total_fed_program_income_earned_amount` decimal(19,2) DEFAULT NULL,
  `fed_program_income_deduction_amt` decimal(19,2) DEFAULT NULL,
  `fed_program_income_addition_amt` decimal(19,2) DEFAULT NULL,
  `fed_program_income_acoe_amt` decimal(19,2) DEFAULT NULL,
  `fed_unexpend_program_income_amt` decimal(19,2) DEFAULT NULL,
  `due_date_time` datetime DEFAULT NULL,
  `report_period_begin_date` datetime DEFAULT NULL,
  `report_period_end_date` datetime DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL,
  `report_period_type_concat` varchar(50) DEFAULT NULL,
  `from_team` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_financial_purpose`
--

CREATE TABLE `trams_financial_purpose` (
  `id` int(11) NOT NULL,
  `fpc` varchar(255) DEFAULT NULL,
  `fpc_description` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fiscal_year_apportionments`
--

CREATE TABLE `trams_fiscal_year_apportionments` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `fiscal_year` varchar(4) DEFAULT NULL,
  `funding_source_name` varchar(20) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(2) DEFAULT NULL,
  `section_code` varchar(2) DEFAULT NULL,
  `limitation_code` varchar(2) DEFAULT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `lapse_year` varchar(4) DEFAULT NULL,
  `initial_carryover_amount` decimal(19,2) DEFAULT NULL,
  `initial_ceiling_amount` decimal(19,2) DEFAULT NULL,
  `initial_cap_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fleet_status`
--

CREATE TABLE `trams_fleet_status` (
  `id` int(11) NOT NULL,
  `duns` varchar(255) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `recipient_name` varchar(255) DEFAULT NULL,
  `fleet_type` varchar(255) DEFAULT NULL,
  `peak_requirement_before` int(11) DEFAULT NULL,
  `peak_requirement_change` int(11) DEFAULT NULL,
  `peak_requirement_after` int(11) DEFAULT NULL,
  `spares_before` int(11) DEFAULT NULL,
  `spares_change` int(11) DEFAULT NULL,
  `spares_after` int(11) DEFAULT NULL,
  `active_total_before` int(11) DEFAULT NULL,
  `active_total_change` int(11) DEFAULT NULL,
  `active_total_after` int(11) DEFAULT NULL,
  `spare_ratio_before` double DEFAULT NULL,
  `spare_ratio_change` double DEFAULT NULL,
  `spare_ratio_after` double DEFAULT NULL,
  `contingency_before` int(11) DEFAULT NULL,
  `contingency_change` int(11) DEFAULT NULL,
  `contingency_after` int(11) DEFAULT NULL,
  `pending_disposal_before` int(11) DEFAULT NULL,
  `pending_disposal_change` int(11) DEFAULT NULL,
  `pending_disposal_after` int(11) DEFAULT NULL,
  `inactive_total_before` int(11) DEFAULT NULL,
  `inactive_total_change` int(11) DEFAULT NULL,
  `inactive_total_after` int(11) DEFAULT NULL,
  `comprehensive_total_before` int(11) DEFAULT NULL,
  `comprehensive_total_change` int(11) DEFAULT NULL,
  `comprehensive_total_after` int(11) DEFAULT NULL,
  `fleet_details` longtext DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fleet_vehicle`
--

CREATE TABLE `trams_fleet_vehicle` (
  `id` int(11) NOT NULL,
  `fleet_status_id` int(11) DEFAULT NULL,
  `vehicle_number` int(11) DEFAULT NULL,
  `vehicle_year` int(11) DEFAULT NULL,
  `vehicle_description` longtext DEFAULT NULL,
  `vehicle_vin_number` varchar(510) DEFAULT NULL,
  `inservice_date_time` datetime DEFAULT NULL,
  `out_of_service_date_time` datetime DEFAULT NULL,
  `fed_useful_life_year` int(11) DEFAULT NULL,
  `min_useful_life_mileage` int(11) DEFAULT NULL,
  `actual_service_years` int(11) DEFAULT NULL,
  `actual_mileage` int(11) DEFAULT NULL,
  `total_federal_share_text` varchar(510) DEFAULT NULL,
  `remaining_federal_share_dollars_by_year` varchar(510) DEFAULT NULL,
  `remaining_federal_share_dollars_by_miles` varchar(510) DEFAULT NULL,
  `trmsfleetstts_fleetvehiclid` int(11) DEFAULT NULL,
  `trmsfltstts_fletvehicle_idx` int(11) DEFAULT NULL,
  `trmspplctnfltstts_fltvhclid` int(11) DEFAULT NULL,
  `trmspplctnfltstt_fltvhcl_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fms_compare`
--

CREATE TABLE `trams_fms_compare` (
  `id` int(11) NOT NULL,
  `run_number` varchar(3) DEFAULT NULL,
  `doc_id_type` varchar(2) DEFAULT NULL,
  `doc_id_number` varchar(13) DEFAULT NULL,
  `doc_id_ffy` varchar(2) DEFAULT NULL,
  `doc_id_suffix` varchar(3) DEFAULT NULL,
  `object_class` varchar(4) DEFAULT NULL,
  `appropriation_code` varchar(2) DEFAULT NULL,
  `appropriation_code_limitation` varchar(3) DEFAULT NULL,
  `reservation_cost_center` varchar(6) DEFAULT NULL,
  `recipient_cost_center` varchar(3) DEFAULT NULL,
  `fpc` varchar(2) DEFAULT NULL,
  `prog_elem` varchar(4) DEFAULT NULL,
  `public_gov` varchar(1) DEFAULT NULL,
  `echo_number` varchar(8) DEFAULT NULL,
  `recipient_id` varchar(9) DEFAULT NULL,
  `reservation_date_time` datetime DEFAULT NULL,
  `first_obligation_date_time` datetime DEFAULT NULL,
  `last_obligation_date_time` datetime DEFAULT NULL,
  `last_disbursement_date_time` datetime DEFAULT NULL,
  `close_date` datetime DEFAULT NULL,
  `cumulative_cy_obligation` decimal(10,0) DEFAULT NULL,
  `cumulative_cy_deobligation` decimal(10,0) DEFAULT NULL,
  `cumulative_cy_payment` decimal(10,0) DEFAULT NULL,
  `cumulative_cy_refunds` decimal(10,0) DEFAULT NULL,
  `cumulative_cy_auth_disb` decimal(10,0) DEFAULT NULL,
  `cumulative_py_obligation` decimal(10,0) DEFAULT NULL,
  `cumulative_py_deobligation` decimal(10,0) DEFAULT NULL,
  `cumulative_py_payment` decimal(10,0) DEFAULT NULL,
  `cumulative_py_refunds` decimal(10,0) DEFAULT NULL,
  `cumulative_py_auth_disb` decimal(10,0) DEFAULT NULL,
  `from_trams` varchar(1) DEFAULT NULL,
  `application_fain` varchar(11) DEFAULT NULL,
  `project_identifier` varchar(13) DEFAULT NULL,
  `scope_code` varchar(4) DEFAULT NULL,
  `scope_suffix` varchar(4) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fms_compare_team_final`
--

CREATE TABLE `trams_fms_compare_team_final` (
  `run_no` varchar(255) DEFAULT NULL,
  `doc_id_type` varchar(255) DEFAULT NULL,
  `doc_id_num` varchar(255) DEFAULT NULL,
  `doc_id_ffy` varchar(255) DEFAULT NULL,
  `doc_id_suffix` varchar(255) DEFAULT NULL,
  `obj_clas` varchar(255) DEFAULT NULL,
  `approp_code` varchar(255) DEFAULT NULL,
  `approp_code_lim` varchar(255) DEFAULT NULL,
  `cost_cntr` varchar(255) DEFAULT NULL,
  `org_code` varchar(255) DEFAULT NULL,
  `fpc` varchar(255) DEFAULT NULL,
  `prog_elem` varchar(255) DEFAULT NULL,
  `public_gov` varchar(255) DEFAULT NULL,
  `echono` varchar(255) DEFAULT NULL,
  `vendor_ssn` varchar(255) DEFAULT NULL,
  `reserv_date` varchar(255) DEFAULT NULL,
  `first_ob_date` varchar(255) DEFAULT NULL,
  `last_ob_date` varchar(255) DEFAULT NULL,
  `last_disb_date` varchar(255) DEFAULT NULL,
  `close_date` varchar(255) DEFAULT NULL,
  `cm0_obligation` double DEFAULT NULL,
  `cm0_deobs` double DEFAULT NULL,
  `cm0_payment` double DEFAULT NULL,
  `cm0_refunds` double DEFAULT NULL,
  `cm0_auth_disb` double DEFAULT NULL,
  `py0_obligation` double DEFAULT NULL,
  `py0_deobs` double DEFAULT NULL,
  `py0_payment` double DEFAULT NULL,
  `py0_refunds` double DEFAULT NULL,
  `py0_auth_disb` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fms_compare_trams_final`
--

CREATE TABLE `trams_fms_compare_trams_final` (
  `run_no` varchar(255) DEFAULT NULL,
  `doc_id_type` varchar(255) DEFAULT NULL,
  `doc_id_num` varchar(255) DEFAULT NULL,
  `doc_id_ffy` varchar(255) DEFAULT NULL,
  `doc_id_suffix` varchar(255) DEFAULT NULL,
  `obj_clas` varchar(255) DEFAULT NULL,
  `approp_code` varchar(255) DEFAULT NULL,
  `approp_code_lim` varchar(255) DEFAULT NULL,
  `cost_cntr` varchar(255) DEFAULT NULL,
  `org_code` varchar(255) DEFAULT NULL,
  `fpc` varchar(255) DEFAULT NULL,
  `prog_elem` varchar(255) DEFAULT NULL,
  `public_gov` varchar(255) DEFAULT NULL,
  `echono` varchar(255) DEFAULT NULL,
  `vendor_ssn` varchar(255) DEFAULT NULL,
  `reserv_date` varchar(255) DEFAULT NULL,
  `first_ob_date` varchar(255) DEFAULT NULL,
  `last_ob_date` varchar(255) DEFAULT NULL,
  `last_disb_date` varchar(255) DEFAULT NULL,
  `close_date` varchar(255) DEFAULT NULL,
  `cm0_obligation` double DEFAULT NULL,
  `cm0_deobs` double DEFAULT NULL,
  `cm0_payment` double DEFAULT NULL,
  `cm0_refunds` double DEFAULT NULL,
  `cm0_auth_disb` double DEFAULT NULL,
  `py0_obligation` double DEFAULT NULL,
  `py0_deobs` double DEFAULT NULL,
  `py0_payment` double DEFAULT NULL,
  `py0_refunds` double DEFAULT NULL,
  `py0_auth_disb` double DEFAULT NULL,
  `application_fain` varchar(255) DEFAULT NULL,
  `project_identifier` varchar(255) DEFAULT NULL,
  `scope_code` varchar(255) DEFAULT NULL,
  `scope_suffix` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fta_document`
--

CREATE TABLE `trams_fta_document` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `duns` varchar(255) DEFAULT NULL,
  `document_context` varchar(255) DEFAULT NULL,
  `document_type` varchar(255) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL,
  `folder_id` int(11) DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL,
  `effective_date` varchar(255) DEFAULT NULL,
  `document_name` varchar(255) DEFAULT NULL,
  `document_description` longtext DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `allotment_advice_id` int(11) DEFAULT NULL,
  `operating_budget_id` int(11) DEFAULT NULL,
  `certification_assurance_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `document_category` varchar(100) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fta_review`
--

CREATE TABLE `trams_fta_review` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `transmitted_date` varchar(255) DEFAULT NULL,
  `submitted_date` varchar(255) DEFAULT NULL,
  `review_in_progress_flag` tinyint(1) DEFAULT NULL,
  `lapsing_fund_flag` tinyint(1) DEFAULT NULL,
  `mpr_report_frequency` varchar(510) DEFAULT NULL,
  `financial_report_freq` varchar(510) DEFAULT NULL,
  `submitted_to_dol_flag` tinyint(1) DEFAULT NULL,
  `submitted_to_dol_for_cert_flag` tinyint(1) DEFAULT NULL,
  `certified_by_dol_flag` tinyint(1) DEFAULT NULL,
  `returned_by_dol_flag` tinyint(1) DEFAULT NULL,
  `requires_technical_review_flag` tinyint(1) DEFAULT NULL,
  `requires_civil_rights_review_flag` tinyint(1) DEFAULT NULL,
  `requires_environmental_review_flag` tinyint(1) DEFAULT NULL,
  `requires_planning_review_flag` tinyint(1) DEFAULT NULL,
  `requires_operations_review_flag` tinyint(1) DEFAULT NULL,
  `dol_comments` longtext DEFAULT NULL,
  `civil_rights_concur_decision` tinyint(1) DEFAULT NULL,
  `environmental_concurrence_flag` tinyint(1) DEFAULT NULL,
  `technical_concurrence_flag` tinyint(1) DEFAULT NULL,
  `director_planning_concurrence_flag` tinyint(1) DEFAULT NULL,
  `director_ops_concurrence_flag` tinyint(1) DEFAULT NULL,
  `legal_concurrence_flag` tinyint(1) DEFAULT NULL,
  `ra_concurrence_flag` tinyint(1) DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL,
  `continue_without_cert_flag` tinyint(1) DEFAULT NULL,
  `no_material_change_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fuel`
--

CREATE TABLE `trams_fuel` (
  `id` int(11) NOT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_fuel_type`
--

CREATE TABLE `trams_fuel_type` (
  `id` int(11) NOT NULL,
  `fuel_name` varchar(510) DEFAULT NULL,
  `tramsfuel_fueltype_id` int(11) DEFAULT NULL,
  `tramsfuel_fueltype_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_funding_source_reference`
--

CREATE TABLE `trams_funding_source_reference` (
  `id` int(11) NOT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `funding_source_description` varchar(510) DEFAULT NULL,
  `section_code` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `trmslmttnrfrnc_lmttnrfrncid` int(11) DEFAULT NULL,
  `discretionary_flag` tinyint(1) DEFAULT NULL,
  `section_year` varchar(255) DEFAULT NULL,
  `fed_dom_assistance_num` varchar(255) DEFAULT NULL,
  `lapse_year` int(11) DEFAULT NULL,
  `latest_flag` tinyint(1) DEFAULT NULL,
  `original_funding_source_id` int(11) DEFAULT NULL,
  `LatestFlagStartDate` datetime DEFAULT NULL COMMENT 'This field represents a single contiguous time frame when the funding source name’s latest flag is set to true',
  `LatestFlagEndDate` datetime DEFAULT NULL COMMENT 'This field represents a single contiguous time frame when the funding source name’s latest flag is set to false'
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_general_business_info`
--

CREATE TABLE `trams_general_business_info` (
  `id` int(11) NOT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `ueid` varchar(50) DEFAULT NULL,
  `duns4` varchar(510) DEFAULT NULL,
  `cage_number` varchar(510) DEFAULT NULL,
  `legal_business_name` varchar(510) DEFAULT NULL,
  `dba_name` varchar(510) DEFAULT NULL,
  `recipient_acronym` varchar(510) DEFAULT NULL,
  `recipient_id` varchar(510) DEFAULT NULL,
  `tin_number` varchar(510) DEFAULT NULL,
  `ssn_number` varchar(510) DEFAULT NULL,
  `trams_status` varchar(510) DEFAULT NULL,
  `sam_status` varchar(510) DEFAULT NULL,
  `sam_expiration_date` varchar(510) DEFAULT NULL,
  `sam_sync_date_time` datetime DEFAULT NULL,
  `division_name` varchar(510) DEFAULT NULL,
  `division_number` varchar(510) DEFAULT NULL,
  `mpin` varchar(510) DEFAULT NULL,
  `cost_center_code` varchar(510) DEFAULT NULL,
  `state_of_incorporation` varchar(510) DEFAULT NULL,
  `country_of_incorporation` varchar(510) DEFAULT NULL,
  `recipient_security_level_text` varchar(510) DEFAULT NULL,
  `highstemplysecrityleveltext` varchar(510) DEFAULT NULL,
  `webaddress_url` varchar(510) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `end_date_time` datetime DEFAULT NULL,
  `fy_end_date_text` varchar(510) DEFAULT NULL,
  `employee_number` varchar(510) DEFAULT NULL,
  `revenue` varchar(510) DEFAULT NULL,
  `organization_type_code` varchar(510) DEFAULT NULL,
  `mpo_number` varchar(510) DEFAULT NULL,
  `organization_alias` varchar(510) DEFAULT NULL,
  `ntd_code` varchar(510) DEFAULT NULL,
  `mpo_flag` tinyint(1) DEFAULT NULL,
  `assistance_flag` tinyint(1) DEFAULT NULL,
  `echo_number` varchar(255) DEFAULT NULL,
  `geo_location` varchar(255) DEFAULT NULL,
  `dbe_flag` tinyint(1) DEFAULT NULL,
  `state_dot_flag` tinyint(1) DEFAULT NULL,
  `designated_recipient_flag` tinyint(1) DEFAULT NULL,
  `requ_flag` tinyint(1) DEFAULT NULL,
  `opac_flag` tinyint(1) DEFAULT NULL,
  `wcf_flag` tinyint(1) DEFAULT NULL,
  `tsc_flag` tinyint(1) DEFAULT NULL,
  `parent_folder_id` int(11) DEFAULT NULL,
  `parent_group_id` int(11) DEFAULT NULL,
  `organizationId` int(11) DEFAULT NULL,
  `last_manual_sync_date` datetime DEFAULT NULL,
  `DaimsBusinessType` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_geographic_identifiers`
--

CREATE TABLE `trams_geographic_identifiers` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(510) DEFAULT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `uza_name` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `congressional_district_id` int(11) DEFAULT NULL,
  `congressional_district_name` varchar(510) DEFAULT NULL,
  `congressional_representative_name` varchar(510) DEFAULT NULL,
  `trmsntty_ggrphcidntifirs_id` int(11) DEFAULT NULL,
  `trmsntty_ggrphcdntifirs_idx` int(11) DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_get_cumulative_apportionments_result`
--

CREATE TABLE `trams_get_cumulative_apportionments_result` (
  `uzacode_text` varchar(6) DEFAULT NULL,
  `uza_name` varchar(255) DEFAULT NULL,
  `state_population` int(11) DEFAULT NULL,
  `cost_center_shortname` varchar(510) DEFAULT NULL,
  `accountcode_text` varchar(19) DEFAULT NULL,
  `appropriationcode_text` varchar(4) DEFAULT NULL,
  `sectionid_text` varchar(4) DEFAULT NULL,
  `limitationcode_text` varchar(4) DEFAULT NULL,
  `fundingfiscalyear_text` varchar(4) DEFAULT NULL,
  `lapseyear_text` varchar(510) DEFAULT NULL,
  `app_initialamt_dec` double NOT NULL DEFAULT 0,
  `app_modificationamt_dec` double NOT NULL DEFAULT 0,
  `app_transferinamt_dec` double NOT NULL DEFAULT 0,
  `app_transferoutamt_dec` double NOT NULL DEFAULT 0,
  `app_effectiveauthamt_dec` double NOT NULL DEFAULT 0,
  `app_reserveamt_dec` double NOT NULL DEFAULT 0,
  `app_obligationamt_dec` double NOT NULL DEFAULT 0,
  `app_recoveryamt_dec` double NOT NULL DEFAULT 0,
  `app_availableamt_dec` double NOT NULL DEFAULT 0,
  `cap_initialamt_dec` double NOT NULL DEFAULT 0,
  `cap_modificaionamt_dec` double NOT NULL DEFAULT 0,
  `cap_transferinamt_dec` double NOT NULL DEFAULT 0,
  `cap_transferoutamt_dec` double NOT NULL DEFAULT 0,
  `cap_effectiveauthamt_dec` double NOT NULL DEFAULT 0,
  `cap_reservationamt_dec` double NOT NULL DEFAULT 0,
  `cap_obligateamt_dec` double NOT NULL DEFAULT 0,
  `cap_recoveryamt_dec` double NOT NULL DEFAULT 0,
  `cap_availableamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_initialamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_modificationamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_transferinamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_transferoutamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_effectiveauthamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_recoveryamt_dec` double NOT NULL DEFAULT 0,
  `ceiling_availableamt_dec` double NOT NULL DEFAULT 0
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_get_grant_accrual_report_result`
--

CREATE TABLE `trams_get_grant_accrual_report_result` (
  `cost_center` varchar(255) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `recipient_name` varchar(510) DEFAULT NULL,
  `application_number` varchar(255) DEFAULT NULL,
  `fta_closeout_approval_date` datetime DEFAULT NULL,
  `last_obligation_date` datetime DEFAULT NULL,
  `last_deobligation_date` datetime DEFAULT NULL,
  `last_disbursement_date` datetime DEFAULT NULL,
  `net_obligations` decimal(42,2) DEFAULT NULL,
  `total_refund_amount` decimal(41,2) DEFAULT NULL,
  `total_disbursement_amount` decimal(41,2) DEFAULT NULL,
  `total_undisbursed_amount` decimal(44,2) DEFAULT NULL,
  `application_type` varchar(255) DEFAULT NULL,
  `active` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_grantee_cert_assurance`
--

CREATE TABLE `trams_grantee_cert_assurance` (
  `id` int(11) NOT NULL,
  `official_name` varchar(510) DEFAULT NULL,
  `attorney_name` varchar(510) DEFAULT NULL,
  `comments` longtext DEFAULT NULL,
  `assigned_date` datetime DEFAULT NULL,
  `certified_date` datetime DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `recipient_affirmation_text` longtext DEFAULT NULL,
  `attorney_affirmation_text` longtext DEFAULT NULL,
  `certification_assurance_doc_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `attorney_cert_date` datetime DEFAULT NULL,
  `official_cert_date` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `latest` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_grantee_cert_assurance_line_item`
--

CREATE TABLE `trams_grantee_cert_assurance_line_item` (
  `id` int(11) NOT NULL,
  `grantee_cert_assurance_id` int(11) NOT NULL,
  `category` varchar(510) DEFAULT NULL,
  `title` varchar(510) DEFAULT NULL,
  `cert_description` longtext DEFAULT NULL,
  `applicable_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_grantee_poc`
--

CREATE TABLE `trams_grantee_poc` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `contact_for_all_flag` tinyint(1) DEFAULT NULL,
  `contact_for_union_flag` tinyint(1) DEFAULT NULL,
  `contact_for_ceo_flag` tinyint(1) DEFAULT NULL,
  `contact_for_mpo_flag` tinyint(1) DEFAULT NULL,
  `contact_for_eeo_flag` tinyint(1) DEFAULT NULL,
  `contact_for_dbe_flag` tinyint(1) DEFAULT NULL,
  `contact_for_titlevi_flag` tinyint(1) DEFAULT NULL,
  `contact_for_section504_flag` tinyint(1) DEFAULT NULL,
  `contact_for_echo_flag` tinyint(1) DEFAULT NULL,
  `contact_for_grant_flag` tinyint(1) DEFAULT NULL,
  `contact_union_name` varchar(510) DEFAULT NULL,
  `union_statewide_application_flag` tinyint(1) DEFAULT NULL,
  `title` varchar(510) DEFAULT NULL,
  `first_name` varchar(510) DEFAULT NULL,
  `last_name` varchar(510) DEFAULT NULL,
  `full_name` varchar(510) DEFAULT NULL,
  `address_line_1` varchar(510) DEFAULT NULL,
  `address_line_2` varchar(510) DEFAULT NULL,
  `address_city` varchar(510) DEFAULT NULL,
  `state` varchar(510) DEFAULT NULL,
  `contact_zip_code_5` varchar(510) DEFAULT NULL,
  `contact_zip_code_4` varchar(510) DEFAULT NULL,
  `phone_number` varchar(510) DEFAULT NULL,
  `alternate_phone_number` varchar(510) DEFAULT NULL,
  `fax_number` varchar(510) DEFAULT NULL,
  `email_address` varchar(510) DEFAULT NULL,
  `contact_website_address` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `contact_for_general_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_grantee_review`
--

CREATE TABLE `trams_grantee_review` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `review_description` longtext DEFAULT NULL,
  `assigned_date_time` datetime DEFAULT NULL,
  `completed_date_time` datetime DEFAULT NULL,
  `reviewed_by` varchar(510) DEFAULT NULL,
  `reviewer_role` varchar(510) DEFAULT NULL,
  `reviewer_comment` longtext DEFAULT NULL,
  `grantee_id` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_grant_accrual_report`
--

CREATE TABLE `trams_grant_accrual_report` (
  `Cost Center` varchar(255) DEFAULT NULL,
  `Recipient Name` varchar(510) DEFAULT NULL,
  `Recipient ID` varchar(255) DEFAULT NULL,
  `Application Number w/ Current Amendment` varchar(510) DEFAULT NULL,
  `Application Status` varchar(510) DEFAULT NULL,
  `Program Description` varchar(510) DEFAULT NULL,
  `Brief Description` longtext DEFAULT NULL,
  `FFR Report Period` varchar(510) DEFAULT NULL,
  `Closed Date` datetime DEFAULT NULL,
  `Obligation Date` datetime DEFAULT NULL,
  `Deobligation Date` datetime DEFAULT NULL,
  `Disbursement Date` datetime DEFAULT NULL,
  `FTA Pre-Award Manager` varchar(255) DEFAULT NULL,
  `FTA Post-Award Manager` varchar(255) DEFAULT NULL,
  `Net Obligations` double NOT NULL DEFAULT 0,
  `Total Refund Amount` double DEFAULT NULL,
  `Total Disbursements` double DEFAULT NULL,
  `Total Undisbursed Amount` double NOT NULL DEFAULT 0,
  `Application Type` varchar(510) DEFAULT NULL,
  `FFR Submitted Date` datetime DEFAULT NULL,
  `Final FFR?` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_latest_announced_preaward_by_fy_view`
--

CREATE TABLE `trams_latest_announced_preaward_by_fy_view` (
  `id` int(11) NOT NULL,
  `grantID` int(11) DEFAULT NULL,
  `modreferenceidint` int(11) DEFAULT NULL,
  `initiatortext` varchar(255) DEFAULT NULL,
  `dateinitiateddatetime` datetime DEFAULT NULL,
  `datecompletedatetime` datetime DEFAULT NULL,
  `fiscalyeartext` varchar(255) DEFAULT NULL,
  `fundinglawtext` varchar(255) DEFAULT NULL,
  `fundinglawdescriptiontext` varchar(255) DEFAULT NULL,
  `statustext` varchar(255) DEFAULT NULL,
  `appropriationtypetext` varchar(255) DEFAULT NULL,
  `initialttalappropriationdec` double DEFAULT NULL,
  `adjstedttalappropriationdec` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_limitation_reference`
--

CREATE TABLE `trams_limitation_reference` (
  `id` int(11) NOT NULL,
  `fundingsourcereferenceid` varchar(510) DEFAULT NULL,
  `limitation_code` varchar(510) DEFAULT NULL,
  `limitation_description` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL,
  `version` varchar(510) DEFAULT NULL,
  `discretionary_or_formula` varchar(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item`
--

CREATE TABLE `trams_line_item` (
  `id` int(11) NOT NULL,
  `status` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(510) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `section_code` varchar(510) DEFAULT NULL,
  `scc_line_item_name` varchar(510) DEFAULT NULL,
  `scc_line_item_code` varchar(510) DEFAULT NULL,
  `scope_detailed_name` varchar(510) DEFAULT NULL,
  `scope_code` varchar(510) DEFAULT NULL,
  `scope_suffix` varchar(510) DEFAULT NULL,
  `line_item_number` varchar(510) DEFAULT NULL,
  `activity_name` varchar(510) DEFAULT NULL,
  `activity_code` varchar(510) DEFAULT NULL,
  `allocation_name` varchar(510) DEFAULT NULL,
  `allocation_code` varchar(510) DEFAULT NULL,
  `line_item_type_name` varchar(510) DEFAULT NULL,
  `line_item_type_code` varchar(510) DEFAULT NULL,
  `custom_item_name` varchar(510) DEFAULT NULL,
  `extended_budget_description` longtext DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `fuel_name` varchar(510) DEFAULT NULL,
  `propulsion` varchar(510) DEFAULT NULL,
  `vehicle_condition` varchar(510) DEFAULT NULL,
  `vehicle_size` varchar(510) DEFAULT NULL,
  `latest_amend_quantity` int(11) DEFAULT NULL,
  `latest_revised_quantity` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `scope_id` int(11) DEFAULT NULL,
  `original_line_item_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `revised_line_item_description` longtext DEFAULT NULL,
  `latest_amend_total_amount` decimal(19,2) DEFAULT NULL,
  `revised_total_amount` decimal(19,2) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `original_FTA_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_fta_amount` decimal(19,2) DEFAULT NULL,
  `revised_FTA_amount` decimal(19,2) DEFAULT NULL,
  `original_state_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_amount` decimal(19,2) DEFAULT NULL,
  `original_local_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_amount` decimal(19,2) DEFAULT NULL,
  `original_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `revised_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `original_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_toll_rev_amount` decimal(19,2) DEFAULT NULL,
  `revised_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `original_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `revised_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `third_party_flag` tinyint(1) DEFAULT NULL,
  `other_budget` tinyint(1) DEFAULT NULL,
  `is_line_item` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_activity_type`
--

CREATE TABLE `trams_line_item_activity_type` (
  `id` int(11) NOT NULL,
  `scope_type_id` int(11) DEFAULT NULL,
  `activity_code` varchar(510) DEFAULT NULL,
  `activity_name` varchar(510) DEFAULT NULL,
  `trmsscptyp_lntmctivitis_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_allocation_type`
--

CREATE TABLE `trams_line_item_allocation_type` (
  `id` int(11) NOT NULL,
  `scope_type_id` int(11) DEFAULT NULL,
  `allocation_code` varchar(510) DEFAULT NULL,
  `allocation_name` varchar(510) DEFAULT NULL,
  `trmsscptyp_lnitmllctins_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_change_log`
--

CREATE TABLE `trams_line_item_change_log` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `line_item_id` int(11) DEFAULT NULL,
  `original_custom_item_name` longtext DEFAULT NULL,
  `revised_custom_item_name` longtext DEFAULT NULL,
  `zeroed_out` tinyint(1) DEFAULT NULL,
  `original_FTA_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_fta_amount` decimal(19,2) DEFAULT NULL,
  `revised_FTA_amount` decimal(19,2) DEFAULT NULL,
  `original_total_eligible_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_total_eligible_amount` decimal(19,2) DEFAULT NULL,
  `revised_total_eligible_amount` decimal(19,2) DEFAULT NULL,
  `original_quantity` int(11) DEFAULT NULL,
  `latest_amend_quantity` int(11) DEFAULT NULL,
  `revised_quantity` int(11) DEFAULT NULL,
  `original_fuel` varchar(510) DEFAULT NULL,
  `original_line_item_description` longtext DEFAULT NULL,
  `revised_line_item_description` longtext DEFAULT NULL,
  `original_state_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_amount` decimal(19,2) DEFAULT NULL,
  `original_local_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_amount` decimal(19,2) DEFAULT NULL,
  `original_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `revised_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `original_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_toll_rev_amount` decimal(19,2) DEFAULT NULL,
  `revised_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `original_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `revised_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `revised_date_time` datetime DEFAULT NULL,
  `revised_by` varchar(510) DEFAULT NULL,
  `budget_revision_id` int(11) DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `thirdPartyFlag` tinyint(1) DEFAULT NULL,
  `trmsbdgtrvsn_lntmchnglgs_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_reference`
--

CREATE TABLE `trams_line_item_reference` (
  `id` int(11) NOT NULL,
  `line_item_code` varchar(255) DEFAULT NULL,
  `line_item_name` varchar(255) DEFAULT NULL,
  `activity_type` varchar(255) DEFAULT NULL,
  `activity_code` varchar(255) DEFAULT NULL,
  `allocation_type` varchar(255) DEFAULT NULL,
  `allocation_code` varchar(255) DEFAULT NULL,
  `scope_code` varchar(255) DEFAULT NULL,
  `scope_category_name` varchar(255) DEFAULT NULL,
  `scope_detailed_name` varchar(255) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_temp`
--

CREATE TABLE `trams_line_item_temp` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `original_line_item_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL,
  `duns` varchar(255) DEFAULT NULL,
  `section_code` varchar(255) DEFAULT NULL,
  `scc_line_item_name` varchar(255) DEFAULT NULL,
  `scc_line_item_code` varchar(255) DEFAULT NULL,
  `scope_detailed_name` varchar(255) DEFAULT NULL,
  `scope_code` varchar(255) DEFAULT NULL,
  `line_item_number` varchar(255) DEFAULT NULL,
  `activity_name` varchar(255) DEFAULT NULL,
  `activity_code` varchar(255) DEFAULT NULL,
  `allocation_name` varchar(255) DEFAULT NULL,
  `allocation_code` varchar(255) DEFAULT NULL,
  `line_item_type_name` varchar(255) DEFAULT NULL,
  `line_item_type_code` varchar(255) DEFAULT NULL,
  `custom_item_name` varchar(255) DEFAULT NULL,
  `extended_budget_description` longtext DEFAULT NULL,
  `revised_line_item_description` longtext DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `latest_amend_quantity` int(11) DEFAULT NULL,
  `latest_revised_quantity` int(11) DEFAULT NULL,
  `total_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_total_amount` decimal(19,2) DEFAULT NULL,
  `revised_total_amount` decimal(19,2) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `original_FTA_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_FTA_amount` decimal(19,2) DEFAULT NULL,
  `revised_FTA_amount` decimal(19,2) DEFAULT NULL,
  `original_state_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_amount` decimal(19,2) DEFAULT NULL,
  `original_local_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_amount` decimal(19,2) DEFAULT NULL,
  `original_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `revised_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `original_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_inkind_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_toll_rev_amount` decimal(19,2) DEFAULT NULL,
  `revised_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `original_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `revised_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `fuel_name` varchar(255) DEFAULT NULL,
  `propulsion` varchar(255) DEFAULT NULL,
  `vehicle_condition` varchar(255) DEFAULT NULL,
  `vehicle_size` varchar(255) DEFAULT NULL,
  `third_party_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_line_item_type`
--

CREATE TABLE `trams_line_item_type` (
  `id` int(11) NOT NULL,
  `scope_type_id` int(11) DEFAULT NULL,
  `line_item_code` varchar(510) DEFAULT NULL,
  `line_item_name` varchar(510) DEFAULT NULL,
  `tramsscpetype_lineitems_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_location`
--

CREATE TABLE `trams_location` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `address_type` varchar(510) DEFAULT NULL,
  `address_line_1` varchar(510) DEFAULT NULL,
  `address_line_2` varchar(510) DEFAULT NULL,
  `city` varchar(510) DEFAULT NULL,
  `state_or_province` varchar(510) DEFAULT NULL,
  `zip_code` varchar(510) DEFAULT NULL,
  `country` varchar(510) DEFAULT NULL,
  `congressional_district` varchar(510) DEFAULT NULL,
  `tramsentity_locations_id` int(11) DEFAULT NULL,
  `tramsentity_locations_idx` int(11) DEFAULT NULL,
  `grantee_id` varchar(510) DEFAULT NULL,
  `zip_plus4` varchar(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_milestones`
--

CREATE TABLE `trams_milestones` (
  `id` int(11) NOT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `milestone_title` varchar(510) DEFAULT NULL,
  `milestone_description` longtext DEFAULT NULL,
  `estimated_completion_date_time` datetime DEFAULT NULL,
  `tramslineitem_milestnes_idx` int(11) DEFAULT NULL,
  `line_item_id` int(11) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `original_milestone_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_milestone_progress_contractors`
--

CREATE TABLE `trams_milestone_progress_contractors` (
  `id` int(11) NOT NULL,
  `contractor_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `contractor_name` varchar(510) DEFAULT NULL,
  `contractor_acronym` varchar(510) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `amount_dollar` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_milestone_progress_remarks`
--

CREATE TABLE `trams_milestone_progress_remarks` (
  `id` int(11) NOT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `remark_by` varchar(510) DEFAULT NULL,
  `remark_on_date_time` datetime DEFAULT NULL,
  `revised_completion_date_time` datetime DEFAULT NULL,
  `revision_number` int(11) DEFAULT NULL,
  `actual_completion_date_time` datetime DEFAULT NULL,
  `original_milestone_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_milestone_progress_report`
--

CREATE TABLE `trams_milestone_progress_report` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `report_period_type` varchar(510) DEFAULT NULL,
  `report_status` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `fiscal_quarter` int(11) DEFAULT NULL,
  `fiscal_month` varchar(510) DEFAULT NULL,
  `final_report_flag` tinyint(1) DEFAULT NULL,
  `due_date_time` datetime DEFAULT NULL,
  `submission_date_time` datetime DEFAULT NULL,
  `submitted_by` varchar(510) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `review_complete_flag` tinyint(1) DEFAULT NULL,
  `review_completed_by` varchar(510) DEFAULT NULL,
  `review_completed_on_date_time` datetime DEFAULT NULL,
  `using_contractors_flag` tinyint(1) DEFAULT NULL,
  `report_period_begin_date` datetime DEFAULT NULL,
  `report_period_end_date` datetime DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL,
  `report_period_type_concat` varchar(50) DEFAULT NULL,
  `from_team` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_omb_apportionment`
--

CREATE TABLE `trams_omb_apportionment` (
  `id` int(11) NOT NULL,
  `modreferenceid` int(11) DEFAULT NULL,
  `activeflag` tinyint(1) DEFAULT NULL,
  `folderid` int(11) DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL,
  `createddatetime` datetime DEFAULT NULL,
  `activateddatetime` datetime DEFAULT NULL,
  `activatedby` varchar(255) DEFAULT NULL,
  `fiscalyear` varchar(255) DEFAULT NULL,
  `fundinglawname` varchar(255) DEFAULT NULL,
  `fundinglawdescription` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `ombapportionmenttype` varchar(255) DEFAULT NULL,
  `initialombapprtionmentamont` decimal(19,2) DEFAULT NULL,
  `adjstedombapprtionmentamont` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_operating_budget`
--

CREATE TABLE `trams_operating_budget` (
  `id` int(11) NOT NULL,
  `cost_center_name` varchar(510) DEFAULT NULL,
  `cost_center_code` varchar(510) DEFAULT NULL,
  `section_id` varchar(510) DEFAULT NULL,
  `budget_type` varchar(10) DEFAULT NULL,
  `control_number` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `authorized_by` varchar(510) DEFAULT NULL,
  `authorized_date_time` datetime DEFAULT NULL,
  `authorized_by_title` varchar(510) DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `plan_prev_total` decimal(19,2) DEFAULT NULL,
  `plan_change_total` decimal(19,2) DEFAULT NULL,
  `plan_new_total` decimal(19,2) DEFAULT NULL,
  `original_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_operating_budget_latest_rollup_by_allotment_code_view`
--

CREATE TABLE `trams_operating_budget_latest_rollup_by_allotment_code_view` (
  `id` bigint(20) NOT NULL,
  `allotmentCode_text` varchar(255) DEFAULT NULL,
  `fiscalYear_text` varchar(255) DEFAULT NULL,
  `appropriationCode_text` varchar(255) DEFAULT NULL,
  `authType_text` varchar(255) DEFAULT NULL,
  `operatingBudgetNewTotalAmount_dec` decimal(19,2) DEFAULT NULL,
  `allotmentCodeNewTotalAmount_dec` decimal(19,2) DEFAULT NULL,
  `codeTransactionType_text` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_po_number`
--

CREATE TABLE `trams_po_number` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `po_number_full` varchar(255) DEFAULT NULL,
  `po_state` varchar(255) DEFAULT NULL,
  `po_section_code` varchar(255) DEFAULT NULL,
  `po_activity_code` varchar(255) DEFAULT NULL,
  `po_sequence_number` varchar(255) DEFAULT NULL,
  `amendment_reservation_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_reservation_amount` decimal(19,2) DEFAULT NULL,
  `amendment_obligation_amount` decimal(19,2) DEFAULT NULL,
  `cumulative_obligation_amount` decimal(19,2) DEFAULT NULL,
  `funding_source_description` varchar(255) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_po_number_sequence`
--

CREATE TABLE `trams_po_number_sequence` (
  `po_state` varchar(255) DEFAULT NULL,
  `po_section_code` varchar(255) DEFAULT NULL,
  `po_activity_code` varchar(255) DEFAULT NULL,
  `po_latest_sequence_no` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `po_identifier` varchar(255) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_program_apportionments`
--

CREATE TABLE `trams_program_apportionments` (
  `id` int(11) NOT NULL,
  `ombapportionmentid` int(11) DEFAULT NULL,
  `fundingfiscalyear` varchar(255) DEFAULT NULL,
  `fundingsourcename` varchar(255) DEFAULT NULL,
  `fundingsourcedescription` varchar(255) DEFAULT NULL,
  `appropriationcode` varchar(255) DEFAULT NULL,
  `sectioncode` varchar(255) DEFAULT NULL,
  `limitationcode` varchar(255) DEFAULT NULL,
  `treasurysymbol` varchar(255) DEFAULT NULL,
  `feddomassistancenumber` varchar(255) DEFAULT NULL,
  `initialapprtinmentavailamnt` decimal(19,2) DEFAULT NULL,
  `adjstedapprtinmentavailamnt` decimal(19,2) DEFAULT NULL,
  `createddatetime` datetime DEFAULT NULL,
  `createdby` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_program_name_reference`
--

CREATE TABLE `trams_program_name_reference` (
  `id` int(11) NOT NULL,
  `section_code` varchar(20) DEFAULT NULL,
  `funding_source_name` varchar(510) DEFAULT NULL,
  `funding_source_description` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_program_plan_type`
--

CREATE TABLE `trams_program_plan_type` (
  `id` int(11) NOT NULL,
  `program_name` varchar(510) DEFAULT NULL,
  `app_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `program_plan_flag` tinyint(1) DEFAULT NULL,
  `program_date_time` datetime DEFAULT NULL,
  `stip_program_date_time` datetime DEFAULT NULL,
  `upwp_program_date_time` datetime DEFAULT NULL,
  `long_range_program_plan_date_time` datetime DEFAULT NULL,
  `stip_program_description` longtext DEFAULT NULL,
  `upwp_program_description` longtext DEFAULT NULL,
  `long_range_program_description` longtext DEFAULT NULL,
  `program_document_id` int(11) DEFAULT NULL,
  `program_page_location` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_program_type_reference`
--

CREATE TABLE `trams_program_type_reference` (
  `id` int(11) NOT NULL,
  `program_type` varchar(510) DEFAULT NULL,
  `program_description` varchar(510) DEFAULT NULL,
  `program_short_description` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project`
--

CREATE TABLE `trams_project` (
  `id` int(11) NOT NULL,
  `project_number` varchar(510) DEFAULT NULL,
  `project_title` varchar(510) DEFAULT NULL,
  `status` varchar(510) DEFAULT NULL,
  `temp_project_number` varchar(510) DEFAULT NULL,
  `project_description` longtext DEFAULT NULL,
  `benefits_description` longtext DEFAULT NULL,
  `additional_information` longtext DEFAULT NULL,
  `location_description` longtext DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(510) DEFAULT NULL,
  `environmental_finding_flag` tinyint(1) DEFAULT NULL,
  `major_capital_project_flag` tinyint(1) DEFAULT NULL,
  `type_of_major_capital_project` varchar(510) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `trmsprgrmplntyp_prgrmpln_id` int(11) DEFAULT NULL,
  `tramsapplicatin_projects_id` int(11) DEFAULT NULL,
  `tramsapplicatin_prjects_idx` int(11) DEFAULT NULL,
  `recipient_id` varchar(255) DEFAULT NULL,
  `recipient_cost_center` varchar(255) DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL,
  `region` varchar(255) DEFAULT NULL,
  `application_folder_id` int(11) DEFAULT NULL,
  `project_number_fiscal_year` varchar(255) DEFAULT NULL,
  `project_number_state` varchar(255) DEFAULT NULL,
  `project_number_sequence_number` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_environmental_finding`
--

CREATE TABLE `trams_project_environmental_finding` (
  `id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `class_level_name` varchar(255) DEFAULT NULL,
  `class_description` longtext DEFAULT NULL,
  `categorical_exclusion_name` varchar(255) DEFAULT NULL,
  `categorical_exclusion_description` longtext DEFAULT NULL,
  `comments` longtext DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_environmental_finding_date`
--

CREATE TABLE `trams_project_environmental_finding_date` (
  `id` int(11) NOT NULL,
  `project_finding_id` int(11) DEFAULT NULL,
  `finding_date_time` datetime DEFAULT NULL,
  `finding_date_type` varchar(2500) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `last_modified_date_time` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_env_finding_line_items`
--

CREATE TABLE `trams_project_env_finding_line_items` (
  `id` int(11) NOT NULL,
  `line_item_id` int(11) DEFAULT NULL,
  `project_finding_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_geography`
--

CREATE TABLE `trams_project_geography` (
  `id` int(11) NOT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `congressional_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `state_name` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `uza_name` varchar(510) DEFAULT NULL,
  `congressional_district_name` varchar(510) DEFAULT NULL,
  `congressional_representative_name` varchar(510) DEFAULT NULL,
  `selected_flag` tinyint(1) DEFAULT NULL,
  `trmsprject_prjectgegrphy_id` int(11) DEFAULT NULL,
  `trmsprjct_prjectgegrphy_idx` int(11) DEFAULT NULL,
  `geographic_identifiers_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_scope`
--

CREATE TABLE `trams_project_scope` (
  `id` int(11) NOT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `updated_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_project_scope_type`
--

CREATE TABLE `trams_project_scope_type` (
  `id` int(11) NOT NULL,
  `project_scope_id` int(11) DEFAULT NULL,
  `scope_name` varchar(510) DEFAULT NULL,
  `scope_number` varchar(510) DEFAULT NULL,
  `description` varchar(510) DEFAULT NULL,
  `trmsprjctsc_prjctscptyp_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_reassigned_task`
--

CREATE TABLE `trams_reassigned_task` (
  `id` int(11) NOT NULL,
  `assigned_people` varchar(500) DEFAULT NULL,
  `fiscal_year` varchar(10) DEFAULT NULL,
  `in_stasis` tinyint(1) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `task_name` varchar(2000) DEFAULT NULL,
  `assignee_name` varchar(500) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_references`
--

CREATE TABLE `trams_references` (
  `ID` int(11) NOT NULL,
  `ref_type_id` int(11) NOT NULL,
  `active_flag` tinyint(1) NOT NULL DEFAULT 1,
  `ref_value` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_reference_types`
--

CREATE TABLE `trams_reference_types` (
  `ID` int(11) NOT NULL,
  `type_name` varchar(255) NOT NULL,
  `created_date` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `modified_date` date DEFAULT NULL,
  `modified_by` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_regional_office_states`
--

CREATE TABLE `trams_regional_office_states` (
  `id` int(11) NOT NULL,
  `regional_office_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL,
  `trmscstcntr_rgnlfficstts_id` int(11) DEFAULT NULL,
  `trmscstcntr_rgnlffcstts_idx` int(11) DEFAULT NULL,
  `cost_center_code` varchar(10) DEFAULT NULL,
  `uza_code` varchar(10) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_report_remarks`
--

CREATE TABLE `trams_report_remarks` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `report_id` int(11) DEFAULT NULL,
  `remark_type` varchar(510) DEFAULT NULL,
  `remark` longtext DEFAULT NULL,
  `report_type` varchar(510) DEFAULT NULL,
  `report_period_type` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `fiscal_quarter` int(11) DEFAULT NULL,
  `fiscal_month` varchar(510) DEFAULT NULL,
  `recipient_user_flag` tinyint(1) DEFAULT NULL,
  `remark_by` varchar(510) DEFAULT NULL,
  `remark_on_date_time` datetime DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `report_status` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_reservation`
--

CREATE TABLE `trams_reservation` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `cost_center_name` varchar(510) DEFAULT NULL,
  `cost_center_code` varchar(510) DEFAULT NULL,
  `reservation_status` varchar(510) DEFAULT NULL,
  `fiscal_year` decimal(19,2) DEFAULT NULL,
  `total_fta_amount` decimal(19,2) DEFAULT NULL,
  `total_reserved_amount` decimal(19,2) DEFAULT NULL,
  `total_unreserved_amount` decimal(19,2) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `reservation_logged_by` varchar(255) DEFAULT NULL,
  `reservation_finalized_date_time` datetime DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_review`
--

CREATE TABLE `trams_review` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `review_description` longtext DEFAULT NULL,
  `added_date_time` datetime DEFAULT NULL,
  `assigned_date_time` datetime DEFAULT NULL,
  `completed_date_time` datetime DEFAULT NULL,
  `reviewed_by` varchar(510) DEFAULT NULL,
  `reviewer_role` varchar(510) DEFAULT NULL,
  `reviewer_comment` varchar(510) DEFAULT NULL,
  `tramsfuel_review_id` int(11) DEFAULT NULL,
  `tramsfuel_review_idx` int(11) DEFAULT NULL,
  `tramsscopetype_reviews_id` int(11) DEFAULT NULL,
  `tramsscopetype_reviews_idx` int(11) DEFAULT NULL,
  `tramsprjectscope_reviews_id` int(11) DEFAULT NULL,
  `tramsprjectscpe_reviews_idx` int(11) DEFAULT NULL,
  `trmsgrntnvrnmnt_rvwcmmnts_d` int(11) DEFAULT NULL,
  `trmsgrntnvrnmn_rvwcmmnts_dx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_review_comments`
--

CREATE TABLE `trams_review_comments` (
  `id` int(11) NOT NULL,
  `comment_context_text` varchar(510) DEFAULT NULL,
  `added_date_time` datetime DEFAULT NULL,
  `reviewed_by` varchar(510) DEFAULT NULL,
  `review_decision` varchar(510) DEFAULT NULL,
  `comment_type_text` varchar(510) DEFAULT NULL,
  `reviewer_comment` longtext DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `project_id` decimal(19,2) DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `civil_rights_id` int(11) DEFAULT NULL,
  `budget_revision_id` int(11) DEFAULT NULL,
  `line_item_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sam_poc_address`
--

CREATE TABLE `trams_sam_poc_address` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `title` varchar(510) DEFAULT NULL,
  `full_name` varchar(510) DEFAULT NULL,
  `phone_number` varchar(510) DEFAULT NULL,
  `phone_extension` varchar(510) DEFAULT NULL,
  `international_phone_number` varchar(510) DEFAULT NULL,
  `fax_number` varchar(510) DEFAULT NULL,
  `email_address` varchar(510) DEFAULT NULL,
  `address_line_1` varchar(510) DEFAULT NULL,
  `address_line_2` varchar(510) DEFAULT NULL,
  `city` varchar(510) DEFAULT NULL,
  `state` varchar(510) DEFAULT NULL,
  `zip_code` varchar(510) DEFAULT NULL,
  `foreign_province` varchar(510) DEFAULT NULL,
  `country` varchar(510) DEFAULT NULL,
  `tramsentity_smpintfcntct_id` int(11) DEFAULT NULL,
  `trmsentity_smpintfcntct_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_scope`
--

CREATE TABLE `trams_scope` (
  `id` int(11) NOT NULL,
  `scope_code` varchar(255) DEFAULT NULL,
  `scope_name` varchar(255) DEFAULT NULL,
  `scope_description` varchar(510) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `scope_suffix` varchar(255) DEFAULT NULL,
  `po_number` varchar(255) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `from_team_flag` tinyint(1) DEFAULT NULL,
  `original_quantity` int(11) DEFAULT NULL,
  `latest_amend_quantity` int(11) DEFAULT NULL,
  `revised_quantity` int(11) DEFAULT NULL,
  `original_fta_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_fta_amount` decimal(19,2) DEFAULT NULL,
  `revised_fta_amount` decimal(19,2) DEFAULT NULL,
  `original_total_eligible_cost` decimal(19,2) DEFAULT NULL,
  `latest_amend_total_eligible_cost` decimal(19,2) DEFAULT NULL,
  `revised_total_eligible_cost` decimal(19,2) DEFAULT NULL,
  `original_state_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_amount` decimal(19,2) DEFAULT NULL,
  `original_local_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_amount` decimal(19,2) DEFAULT NULL,
  `original_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `revised_other_fed_amount` decimal(19,2) DEFAULT NULL,
  `original_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `revised_state_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `revised_local_in_kind_amount` decimal(19,2) DEFAULT NULL,
  `original_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `revised_toll_revenue_amount` decimal(19,2) DEFAULT NULL,
  `original_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `latest_amend_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `revised_adjustment_amount` decimal(19,2) DEFAULT NULL,
  `other_budget` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_scope_acc`
--

CREATE TABLE `trams_scope_acc` (
  `id` int(11) NOT NULL,
  `cost_center_code` varchar(510) DEFAULT NULL,
  `account_class_code` varchar(510) DEFAULT NULL,
  `acc_fpc` varchar(510) DEFAULT NULL,
  `acc_description` varchar(510) DEFAULT NULL,
  `acc_reservation_amount` decimal(19,2) DEFAULT NULL,
  `trmsscprsrv_scpccntclss_dx` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `po_number` varchar(20) DEFAULT NULL,
  `scope_number` varchar(255) DEFAULT NULL,
  `scope_suffix` varchar(20) DEFAULT NULL,
  `scope_name` varchar(255) DEFAULT NULL,
  `funding_uza_code` varchar(255) DEFAULT NULL,
  `funding_uza_name` varchar(255) DEFAULT NULL,
  `acc_funding_fiscal_year` varchar(255) DEFAULT NULL,
  `acc_appropriation_code` varchar(255) DEFAULT NULL,
  `acc_section_code` varchar(255) DEFAULT NULL,
  `acc_limitation_code` varchar(255) DEFAULT NULL,
  `acc_auth_type` varchar(255) DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `project_number` varchar(255) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `cumulative_reservation_amount` decimal(19,2) DEFAULT NULL,
  `date_last_modified` datetime DEFAULT NULL,
  `last_modified_by` varchar(255) DEFAULT NULL,
  `reservation_by` varchar(255) DEFAULT NULL,
  `reservation_date` datetime DEFAULT NULL,
  `fiscal_year` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_scope_type`
--

CREATE TABLE `trams_scope_type` (
  `id` int(11) NOT NULL,
  `scope_code` varchar(510) DEFAULT NULL,
  `scope_name` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date_time` datetime DEFAULT NULL,
  `inactive_date_time` datetime DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_section_5337_area`
--

CREATE TABLE `trams_section_5337_area` (
  `id` int(11) NOT NULL,
  `state_name` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `section_area_name` varchar(510) DEFAULT NULL,
  `share_of_fg_fund` double DEFAULT NULL,
  `fixed_guideway_drm` double DEFAULT NULL,
  `fixed_guideway_Vrm` double DEFAULT NULL,
  `motorbus_drm` double DEFAULT NULL,
  `motorbus_vrm` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_section_5337_area_allocations`
--

CREATE TABLE `trams_section_5337_area_allocations` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `area_allocation` double DEFAULT NULL,
  `allocationTypeId` int(11) DEFAULT NULL,
  `formula_program_state_name` varchar(510) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_section_code_reference`
--

CREATE TABLE `trams_section_code_reference` (
  `id` int(11) NOT NULL,
  `section_code` varchar(5) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `codification` varchar(30) DEFAULT NULL,
  `effective_date` datetime DEFAULT NULL,
  `section_code_description` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_security`
--

CREATE TABLE `trams_security` (
  `id` int(11) NOT NULL,
  `application_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `modified_date_time` datetime DEFAULT NULL,
  `modified_by` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `security_name` varchar(510) DEFAULT NULL,
  `security_finding` varchar(510) DEFAULT NULL,
  `security_comment` longtext DEFAULT NULL,
  `trmsprjctrevisin_secrity_id` int(11) DEFAULT NULL,
  `trmsprjctrvisin_secrity_idx` int(11) DEFAULT NULL,
  `tramsproject_security_id` int(11) DEFAULT NULL,
  `tramsproject_security_idx` int(11) DEFAULT NULL,
  `reason_1_flag` tinyint(1) DEFAULT NULL,
  `reason_2_flag` tinyint(1) DEFAULT NULL,
  `reason_3_flag` tinyint(1) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sp_log`
--

CREATE TABLE `trams_sp_log` (
  `caller_number` varchar(40) NOT NULL,
  `created_date_time` datetime DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `log_text` varchar(4096) NOT NULL,
  `log_level` varchar(20) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sp_log_c`
--

CREATE TABLE `trams_sp_log_c` (
  `name` varchar(255) NOT NULL,
  `log_level` int(11) NOT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sp_log_config`
--

CREATE TABLE `trams_sp_log_config` (
  `name` varchar(255) DEFAULT NULL,
  `log_level` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state`
--

CREATE TABLE `trams_state` (
  `id` int(11) NOT NULL,
  `state_name` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `territory_flag` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_acs_data`
--

CREATE TABLE `trams_state_acs_data` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `low_income_rural_population` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_allocations`
--

CREATE TABLE `trams_state_allocations` (
  `id` int(11) NOT NULL,
  `preAwardID` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `state_allocation` double DEFAULT NULL,
  `trmsrbnizedre_rbnizedre_zid` int(11) DEFAULT NULL,
  `tramsstate_state_stateid` int(11) DEFAULT NULL,
  `allocationTypeId` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `lapse_year` varchar(510) DEFAULT NULL,
  `section_id` varchar(510) DEFAULT NULL,
  `limitation_code` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `operating_cap` decimal(19,2) DEFAULT NULL,
  `ceiling_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_allocation_factor`
--

CREATE TABLE `trams_state_allocation_factor` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `allocation_factor` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_appalachian_factor`
--

CREATE TABLE `trams_state_appalachian_factor` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `appalachian_factor` double DEFAULT NULL,
  `active_flag` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_application_review`
--

CREATE TABLE `trams_state_application_review` (
  `id` int(11) NOT NULL,
  `applicationidint` int(11) DEFAULT NULL,
  `eo12372_review_flag` tinyint(1) DEFAULT NULL,
  `state_application_id` varchar(510) DEFAULT NULL,
  `received_by_state_date` datetime DEFAULT NULL,
  `trmspplictin_sttppreview_id` int(11) DEFAULT NULL,
  `trmspplictin_sttpprview_idx` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_apportionment_year_population`
--

CREATE TABLE `trams_state_apportionment_year_population` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_land_area`
--

CREATE TABLE `trams_state_land_area` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `land_area` double DEFAULT NULL,
  `rural_land_area` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_ntd_summary`
--

CREATE TABLE `trams_state_ntd_summary` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `rural_vehicle_revenue_miles` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_state_population`
--

CREATE TABLE `trams_state_population` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `pop_older_adults_factor` double DEFAULT NULL,
  `pop_people_with_disabilities_factor` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_static_reports_record`
--

CREATE TABLE `trams_static_reports_record` (
  `uId` int(11) NOT NULL,
  `folderName` varchar(255) DEFAULT NULL,
  `folderId` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_status_history`
--

CREATE TABLE `trams_status_history` (
  `id` int(11) NOT NULL,
  `context_id` int(11) DEFAULT NULL,
  `previous_status` varchar(255) DEFAULT NULL,
  `new_status` varchar(255) DEFAULT NULL,
  `change_date` datetime DEFAULT NULL,
  `change_by` varchar(255) DEFAULT NULL,
  `context_details` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_stic_ntd_average`
--

CREATE TABLE `trams_stic_ntd_average` (
  `id` int(11) NOT NULL,
  `ave_pmt_per_vrh` double NOT NULL,
  `ave_pmt_per_vrm` double NOT NULL,
  `ave_vrm_per_capita` double NOT NULL,
  `ave_vrh_per_capita` double NOT NULL,
  `ave_pmt_per_capita` double NOT NULL,
  `ave_upt_per_capita` double NOT NULL,
  `fiscal_year` varchar(510) NOT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_subrecipient_congress_districts`
--

CREATE TABLE `trams_subrecipient_congress_districts` (
  `id` int(11) NOT NULL,
  `subrecipient_id` int(11) DEFAULT NULL,
  `county_number` int(11) DEFAULT NULL,
  `district_number` varchar(10) DEFAULT NULL,
  `district_state` varchar(50) DEFAULT NULL,
  `district_name` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_subrecipient_counties`
--

CREATE TABLE `trams_subrecipient_counties` (
  `id` int(11) NOT NULL,
  `subrecipient_id` int(11) DEFAULT NULL,
  `county_number` int(11) DEFAULT NULL,
  `county_state` varchar(50) DEFAULT NULL,
  `county_name` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sub_allocation`
--

CREATE TABLE `trams_sub_allocation` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `direct_recipient_id` varchar(20) DEFAULT NULL,
  `direct_recipient_name` varchar(510) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `section_code` varchar(510) DEFAULT NULL,
  `suballocation_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sub_allocation_parent`
--

CREATE TABLE `trams_sub_allocation_parent` (
  `id` int(11) NOT NULL,
  `designated_recipient_id` varchar(20) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `section_code` varchar(510) DEFAULT NULL,
  `initial_allocation` decimal(19,2) DEFAULT NULL,
  `remaining_allocation` decimal(19,2) DEFAULT NULL,
  `letter_folder_id` int(11) DEFAULT NULL,
  `letter_document_id` int(11) DEFAULT NULL,
  `start_date_time` datetime DEFAULT NULL,
  `finalize_date_time` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_sub_recipients`
--

CREATE TABLE `trams_sub_recipients` (
  `id` int(11) NOT NULL,
  `duns` varchar(510) DEFAULT NULL,
  `subrecipient_name` varchar(510) DEFAULT NULL,
  `parent_recipient_duns` varchar(510) DEFAULT NULL,
  `dba_name` varchar(510) DEFAULT NULL,
  `address` varchar(510) DEFAULT NULL,
  `city` varchar(510) DEFAULT NULL,
  `state` varchar(510) DEFAULT NULL,
  `county_name` varchar(510) DEFAULT NULL,
  `zip_code` varchar(510) DEFAULT NULL,
  `organization_type` varchar(510) DEFAULT NULL,
  `organization_type_id` int(11) DEFAULT NULL,
  `recipient_id` varchar(20) DEFAULT NULL,
  `parent_recipient_id` varchar(20) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_suspense_contract_acc_transaction`
--

CREATE TABLE `trams_suspense_contract_acc_transaction` (
  `id` int(11) NOT NULL,
  `contract_award_id` int(11) DEFAULT NULL,
  `recipient_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `application_id` int(11) DEFAULT NULL,
  `application_number` varchar(20) DEFAULT NULL,
  `project_number` varchar(20) DEFAULT NULL,
  `echo_number` varchar(255) DEFAULT NULL,
  `transaction_fiscal_year` varchar(4) DEFAULT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `scope_code` varchar(10) DEFAULT NULL,
  `scope_name` varchar(255) DEFAULT NULL,
  `account_class_code` varchar(20) DEFAULT NULL,
  `fpc` varchar(2) DEFAULT NULL,
  `cost_center_code` varchar(5) DEFAULT NULL,
  `funding_source_name` varchar(255) DEFAULT NULL,
  `funding_fiscal_year` varchar(4) DEFAULT NULL,
  `type_authority` varchar(1) DEFAULT NULL,
  `transaction_type` varchar(255) DEFAULT NULL,
  `appropriation_code` varchar(2) DEFAULT NULL,
  `section_code` varchar(2) DEFAULT NULL,
  `limitation_code` varchar(2) DEFAULT NULL,
  `transaction_date_time` datetime DEFAULT NULL,
  `transaction_by` varchar(255) DEFAULT NULL,
  `trams_entry_date_time` datetime DEFAULT NULL,
  `object_class` varchar(4) DEFAULT NULL,
  `dafis_suffix_code` varchar(2) DEFAULT NULL,
  `obligation_amount` double DEFAULT 0,
  `deobligation_amount` double DEFAULT 0,
  `disbursement_amount` double DEFAULT 0,
  `refund_amount` double DEFAULT 0,
  `authorized_disbursement_amount` double DEFAULT 0,
  `run_number` varchar(255) DEFAULT NULL,
  `transaction_code` varchar(3) DEFAULT NULL,
  `transaction_status_code` varchar(1) DEFAULT NULL,
  `app_amendment_number` int(2) DEFAULT NULL,
  `fpc_description` varchar(255) DEFAULT NULL,
  `po_number` varchar(20) DEFAULT NULL,
  `from_trams` tinyint(1) DEFAULT NULL,
  `scope_suffix` varchar(10) DEFAULT NULL,
  `tbp_process_date` datetime DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `transaction_description` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `updated_date_time` datetime DEFAULT NULL,
  `comment` varchar(510) DEFAULT NULL,
  `document_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_system_adjustments`
--

CREATE TABLE `trams_system_adjustments` (
  `id` int(11) NOT NULL,
  `adjustment_name` varchar(510) DEFAULT NULL,
  `operator_text` varchar(510) DEFAULT NULL,
  `adjustment_notes` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `created_by` varchar(510) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_take_downs`
--

CREATE TABLE `trams_take_downs` (
  `id` int(11) NOT NULL,
  `preAwardID` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `take_down_name` varchar(510) DEFAULT NULL,
  `take_down_value` double DEFAULT NULL,
  `take_down_percentage` double DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_team_role_mapping`
--

CREATE TABLE `trams_team_role_mapping` (
  `id` int(11) NOT NULL,
  `team_user_id` int(11) DEFAULT NULL,
  `trams_role` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_transportation_type`
--

CREATE TABLE `trams_transportation_type` (
  `id` int(11) NOT NULL,
  `ost_type_code` varchar(510) DEFAULT NULL,
  `ost_type_name` varchar(510) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `created_date_time` datetime DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_urbanized_area`
--

CREATE TABLE `trams_urbanized_area` (
  `id` int(11) NOT NULL,
  `uza_name` varchar(510) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `uza_identifier` varchar(510) DEFAULT NULL,
  `as_large_pop_uza_flag` tinyint(1) DEFAULT NULL,
  `uace` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) NOT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_usa_spending`
--

CREATE TABLE `trams_usa_spending` (
  `CFDA_PROGRAM` varchar(7) DEFAULT NULL,
  `SAI` varchar(20) DEFAULT NULL,
  `REC_NAME` varchar(45) DEFAULT NULL,
  `REC_CITY_CODE` varchar(5) DEFAULT NULL,
  `REC_CITY_NAME` varchar(21) DEFAULT NULL,
  `REC_COUNTY_CODE` varchar(3) DEFAULT NULL,
  `REC_COUNTY_NAME` varchar(21) DEFAULT NULL,
  `REC_STATE_CODE` varchar(2) DEFAULT NULL,
  `REC_ZIP_CODE` varchar(9) DEFAULT NULL,
  `REC_TYPE_CODE` varchar(2) DEFAULT NULL,
  `ACTION_TYPE` varchar(1) DEFAULT NULL,
  `REC_CONG_DIST` varchar(2) DEFAULT NULL,
  `FTA_IDEN` varchar(4) DEFAULT NULL,
  `FED_ID` varchar(16) DEFAULT NULL,
  `FED_ID_NO_OF_CHANGES` varchar(4) DEFAULT NULL,
  `FED_FUNDING_SIGN` varchar(1) DEFAULT NULL,
  `FED_FUNDING_AMT` varchar(10) DEFAULT NULL,
  `NONFED_FUNDING_SIGN` varchar(1) DEFAULT NULL,
  `NONFED_FUNDING_AMT` varchar(10) DEFAULT NULL,
  `TOTAL_FUNDING_SIGN` varchar(1) DEFAULT NULL,
  `TOTAL_FUNDING_AMT` varchar(11) DEFAULT NULL,
  `AWARD_DATE` varchar(8) DEFAULT NULL,
  `STARTING_DATE` varchar(8) DEFAULT NULL,
  `ENDING_DATE` varchar(8) DEFAULT NULL,
  `ASSISTANCE_TRANS_TYPE` varchar(2) DEFAULT NULL,
  `RECORD_TYPE` varchar(1) DEFAULT NULL,
  `CORRECTION_LATE_IND` varchar(1) DEFAULT NULL,
  `FY_QTR_CORRECTION` varchar(5) DEFAULT NULL,
  `P_STATE_CODE` varchar(7) DEFAULT NULL,
  `P_STATE_NAME` varchar(25) DEFAULT NULL,
  `P_COUNTY_CITY_NAME` varchar(25) DEFAULT NULL,
  `P_ZIP` varchar(9) DEFAULT NULL,
  `P_CONG_DIST` varchar(2) DEFAULT NULL,
  `CFDA_PROGRAM_TITLE` varchar(74) DEFAULT NULL,
  `FED_AGENCY_NAME` varchar(72) DEFAULT NULL,
  `REC_STATE_NAME` varchar(25) DEFAULT NULL,
  `PROJECT_DESCRIPTION` varchar(149) DEFAULT NULL,
  `DUNS` varchar(9) DEFAULT NULL,
  `DUNSPLUS` varchar(4) DEFAULT NULL,
  `D_N_B_CONF_CODE` varchar(2) DEFAULT NULL,
  `TAS_AGENCY_CODE` varchar(2) DEFAULT NULL,
  `TAS_ACCT_CODE` varchar(4) DEFAULT NULL,
  `TAS_SUB_ACCT` varchar(3) DEFAULT NULL,
  `REC_ADDRESS_1` varchar(35) DEFAULT NULL,
  `REC_ADDRESS_2` varchar(35) DEFAULT NULL,
  `REC_ADDRESS_3` varchar(35) DEFAULT NULL,
  `FACE_VALUE` varchar(16) DEFAULT NULL,
  `ORIG_SUBSIDY` varchar(16) DEFAULT NULL,
  `BUSINESS_FUNDS_IND` varchar(3) DEFAULT NULL,
  `REC_COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `P_COUNTRY_CODE` varchar(3) DEFAULT NULL,
  `UNIQUE_RECORD_IND` varchar(70) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_user_roles`
--

CREATE TABLE `trams_user_roles` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) DEFAULT NULL,
  `group_name` varchar(10000) DEFAULT NULL,
  `active_flag` tinyint(1) DEFAULT NULL,
  `active_date` datetime DEFAULT NULL,
  `deactivated_date_time` datetime DEFAULT NULL,
  `grantee_flag` tinyint(1) DEFAULT NULL,
  `fta_flag` tinyint(1) DEFAULT NULL,
  `external_flag` tinyint(1) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `system_user_id` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_user_to_grantee`
--

CREATE TABLE `trams_user_to_grantee` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `grantee_id` int(11) NOT NULL,
  `access_level` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_ussp_county_city`
--

CREATE TABLE `trams_ussp_county_city` (
  `county_city_id` int(11) NOT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `county_code` varchar(10) DEFAULT NULL,
  `city_code` varchar(10) DEFAULT NULL,
  `county_city_name` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_ussp_grantee_type`
--

CREATE TABLE `trams_ussp_grantee_type` (
  `id` int(11) NOT NULL,
  `ost_type_code` varchar(25) DEFAULT NULL,
  `ost_type_desc` varchar(255) DEFAULT NULL,
  `ussp_rec_type_code` varchar(10) DEFAULT NULL,
  `ussp_rec_type_desc` varchar(50) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_ussp_state`
--

CREATE TABLE `trams_ussp_state` (
  `id` int(11) NOT NULL,
  `state_code` varchar(10) DEFAULT NULL,
  `state_name` varchar(25) DEFAULT NULL,
  `state_abbr` varchar(10) DEFAULT NULL,
  `country_name` varchar(10) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_allocations`
--

CREATE TABLE `trams_uza_allocations` (
  `id` int(11) NOT NULL,
  `preAwardID` int(11) DEFAULT NULL,
  `uzaallocation` double DEFAULT NULL,
  `formulagrantnametext` varchar(510) DEFAULT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `allocationTypeId` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `lapse_year` varchar(510) DEFAULT NULL,
  `sectionID_text` varchar(510) DEFAULT NULL,
  `limitationCode_text` varchar(510) DEFAULT NULL,
  `appropriationCode_text` varchar(510) DEFAULT NULL,
  `operatingCap_dec` decimal(19,2) DEFAULT NULL,
  `ceiling_dec` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_allocation_by_state`
--

CREATE TABLE `trams_uza_allocation_by_state` (
  `id` int(11) NOT NULL,
  `congressional_appropriation_id` int(11) DEFAULT NULL,
  `formula_program_name` varchar(510) DEFAULT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `uza_allocation` double DEFAULT NULL,
  `allocationTypeId` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `lapse_year` varchar(510) DEFAULT NULL,
  `section_id` varchar(510) DEFAULT NULL,
  `limitation_code` varchar(510) DEFAULT NULL,
  `appropriation_code` varchar(510) DEFAULT NULL,
  `operating_cap` decimal(19,2) DEFAULT NULL,
  `ceiling_amount` decimal(19,2) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_census`
--

CREATE TABLE `trams_uza_census` (
  `id` int(11) NOT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `pop_poverty` double DEFAULT NULL,
  `pop_older_adults` double DEFAULT NULL,
  `pop_people_with_disabilities` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_code_reference`
--

CREATE TABLE `trams_uza_code_reference` (
  `id` int(11) NOT NULL,
  `uza_code` varchar(6) DEFAULT NULL,
  `uza_name` varchar(255) DEFAULT NULL,
  `cost_center_code` varchar(5) DEFAULT NULL,
  `state_flag` tinyint(1) DEFAULT NULL,
  `parent_uza_code` varchar(6) DEFAULT NULL,
  `parent_state_uza_code` varchar(510) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `census_year` varchar(4) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_ntd_summary`
--

CREATE TABLE `trams_uza_ntd_summary` (
  `id` int(11) NOT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `passenger_miles_per_vrm` double DEFAULT NULL,
  `passenger_miles_per_vrh` double DEFAULT NULL,
  `vehicle_revenue_mile_per_capita` double DEFAULT NULL,
  `vehicle_revenue_hour_per_capita` double DEFAULT NULL,
  `passenger_miles_per_capita` double DEFAULT NULL,
  `passenger_Trips_per_capita` double DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_ntd_summary_by_state`
--

CREATE TABLE `trams_uza_ntd_summary_by_state` (
  `id` int(11) NOT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `fixed_gdw_drm` double DEFAULT NULL,
  `fixed_gdw_vrm` bigint(20) DEFAULT NULL,
  `fixed_gdw_pmt` double DEFAULT NULL,
  `fixed_gdw_operating_exp` double DEFAULT NULL,
  `non_fixed_gdw_vrm` bigint(20) DEFAULT NULL,
  `non_fixed_gdw_pmt` double DEFAULT NULL,
  `non_fixed_gdw_operating_exp` double DEFAULT NULL,
  `has_commuter_rail` tinyint(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Table structure for table `trams_uza_population_by_state`
--

CREATE TABLE `trams_uza_population_by_state` (
  `id` int(11) NOT NULL,
  `state_id` int(11) DEFAULT NULL,
  `uza_id` int(11) DEFAULT NULL,
  `uza_code` varchar(510) DEFAULT NULL,
  `population` int(11) DEFAULT NULL,
  `population_density` double DEFAULT NULL,
  `pop_poverty` double DEFAULT NULL,
  `fiscal_year` varchar(510) DEFAULT NULL,
  `pop_older_adults` double DEFAULT NULL,
  `pop_people_with_disabilities` double DEFAULT NULL,
  `uace` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `primarystatebool` tinyint(1) DEFAULT NULL,
  `small_uza_bool` tinyint(1) DEFAULT NULL,
  `parent_uza` varchar(255) DEFAULT NULL
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `trams_view_dbereport`
-- (See below for the actual view)
--
CREATE TABLE `trams_view_dbereport` (
`dbe_id` int(11)
,`recipient_id` int(11)
,`report_period` varchar(255)
,`fiscal_year` varchar(255)
,`report_quarter` int(11)
,`status` varchar(255)
,`Version` int(11)
,`report_due_date` datetime
,`updated_by` varchar(255)
,`legal_business_name` varchar(120)
,`cost_center_code` varchar(510)
,`submitted_date` datetime
,`IsLatest` tinyint(1)
,`isActive` int(1)
);

-- --------------------------------------------------------

--
-- Structure for view `trams_view_dbereport`
--
DROP TABLE IF EXISTS `trams_view_dbereport`;

CREATE ALGORITHM=UNDEFINED SQL SECURITY DEFINER VIEW `trams_view_dbereport`  AS SELECT `dbe`.`id` AS `dbe_id`, `dbe`.`recipient_id` AS `recipient_id`, `dbe`.`report_period` AS `report_period`, `dbe`.`fiscal_year` AS `fiscal_year`, `dbe`.`report_quarter` AS `report_quarter`, `dbe`.`status` AS `status`, `dbe`.`Version` AS `Version`, `dbe`.`report_due_date_time` AS `report_due_date`, `dbe`.`updated_by` AS `updated_by`, `gbi`.`legalBusinessName` AS `legal_business_name`, `gbi`.`costCenterCode` AS `cost_center_code`, `dbe`.`submitted_date_time` AS `submitted_date`, `dbe`.`IsLatest` AS `IsLatest`, `app`.`id` FROM ((`trams_dbe_report` `dbe` left join `vwtramsGeneralBusinessInfo` `gbi` on(`gbi`.`id` = `dbe`.`recipient_id`)) left join (select `trams_application`.`id` AS `id`,`trams_application`.`recipient_id` AS `recipient_id` from `trams_application` where `trams_application`.`status` not in ('Closed','Archived') group by `trams_application`.`recipient_id`) `app` on(`app`.`recipient_id` = `dbe`.`recipient_id`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tramsAccountClassCodeReservation`
--
ALTER TABLE `tramsAccountClassCodeReservation`
  ADD PRIMARY KEY (`AccountClassCodeReservationId`),
  ADD KEY `IX_AccountClassCodeReservation_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_AccountClassCodeReservation_ProjectId_ScopeCode_Funding` (`ProjectId`,`ScopeCode`,`FundingSourceName`);

--
-- Indexes for table `tramsAdHocTaskComments`
--
ALTER TABLE `tramsAdHocTaskComments`
  ADD PRIMARY KEY (`CommentId`),
  ADD KEY `CommentBy_FK` (`CommentBy`),
  ADD KEY `CommentAdHocTaskId_FK` (`CommentAdHocTaskId`);

--
-- Indexes for table `tramsAdHocTasks`
--
ALTER TABLE `tramsAdHocTasks`
  ADD PRIMARY KEY (`AdHocTaskId`),
  ADD KEY `AdHocTaskSender_FK` (`Sender`),
  ADD KEY `AdHocTaskRecipient_FK` (`Recipient`),
  ADD KEY `AdHocTasksApplicationId_FK` (`ApplicationId`);

--
-- Indexes for table `tramsAmendmentErrorLog`
--
ALTER TABLE `tramsAmendmentErrorLog`
  ADD PRIMARY KEY (`AmendmentErrorLogId`);

--
-- Indexes for table `tramsApplicationDiscretionaryDetail`
--
ALTER TABLE `tramsApplicationDiscretionaryDetail`
  ADD PRIMARY KEY (`ApplicationDiscretionaryDetailId`),
  ADD KEY `IX_ApplicationDiscretionaryDetail_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_ApplicationDiscretionaryDetail_StatusId` (`StatusId`);

--
-- Indexes for table `tramsApplicationHoldingPot`
--
ALTER TABLE `tramsApplicationHoldingPot`
  ADD PRIMARY KEY (`ApplicationHoldingPotId`),
  ADD KEY `IX_ApplicationHoldingPot_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_ApplicationHoldingPot_BudgetRevisionId` (`BudgetRevisionId`),
  ADD KEY `IX_ApplicationHoldingPot_TypeId` (`TypeId`);

--
-- Indexes for table `tramsApplicationHoldingPotAuditHistory`
--
ALTER TABLE `tramsApplicationHoldingPotAuditHistory`
  ADD PRIMARY KEY (`ApplicationHoldingPotAuditHistoryId`),
  ADD KEY `IX_ApplicationHoldingPotAuditHistory_ApplicationHoldingPotId` (`ApplicationHoldingPotId`);

--
-- Indexes for table `tramsApplicationHoldingPotReservation`
--
ALTER TABLE `tramsApplicationHoldingPotReservation`
  ADD PRIMARY KEY (`ApplicationHoldingPotReservationId`),
  ADD KEY `IX_ApplicationHoldingPotReservation_ApplicationHoldingPotId` (`ApplicationHoldingPotId`),
  ADD KEY `IX_ApplicationHoldingPotReservation_ProjectId_ScopeCode_Funding` (`ProjectId`,`ScopeCode`,`FundingSourceName`);

--
-- Indexes for table `tramsApplicationReview`
--
ALTER TABLE `tramsApplicationReview`
  ADD PRIMARY KEY (`ApplicationReviewId`),
  ADD KEY `IX_ApplicationReview_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_ApplicationApplicationReview_ReviewPhaseId` (`ReviewPhaseId`),
  ADD KEY `IX_ApplicationReview_StatusId` (`StatusId`);

--
-- Indexes for table `tramsApplicationTransmissionHistory`
--
ALTER TABLE `tramsApplicationTransmissionHistory`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `ApplicationId` (`ApplicationId`);

--
-- Indexes for table `tramsApportionmentTransferDetails`
--
ALTER TABLE `tramsApportionmentTransferDetails`
  ADD PRIMARY KEY (`ApportionmentTransferDetailsId`),
  ADD KEY `IX_ApportionmentTransferDetails_BudgetRevisionId` (`BudgetRevisionId`);

--
-- Indexes for table `tramsArchiveAwardAuditLog`
--
ALTER TABLE `tramsArchiveAwardAuditLog`
  ADD PRIMARY KEY (`LogId`),
  ADD KEY `ArchivedOn` (`ArchivedOn`),
  ADD KEY `ApplicationId` (`ApplicationId`);

--
-- Indexes for table `tramsAutomatedRecoveryRecords`
--
ALTER TABLE `tramsAutomatedRecoveryRecords`
  ADD PRIMARY KEY (`TramsAutomatedRecoveryRecordsId`),
  ADD KEY `DeobligationId` (`DeobligationId`),
  ADD KEY `AllotmentId` (`AllotmentId`),
  ADD KEY `OperatingBudgetId` (`OperatingBudgetId`);

--
-- Indexes for table `tramsDAFISSuffixCodeReference`
--
ALTER TABLE `tramsDAFISSuffixCodeReference`
  ADD PRIMARY KEY (`DAFISCodeId`);

--
-- Indexes for table `tramsDaimsBusinessTypeReference`
--
ALTER TABLE `tramsDaimsBusinessTypeReference`
  ADD PRIMARY KEY (`BusinessTypeId`);

--
-- Indexes for table `tramsDiscretionaryChangelog`
--
ALTER TABLE `tramsDiscretionaryChangelog`
  ADD PRIMARY KEY (`DiscretionaryChangelogId`),
  ADD KEY `IX_tramsDiscretionaryChangelog_DiscretionaryProgramId` (`DiscretionaryProgramId`),
  ADD KEY `IX_tramsDiscretionaryChangelog_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_tramsDiscretionaryChangelog_SourceOfChangeId` (`SourceOfChangeId`);

--
-- Indexes for table `tramsFYAPResultSet`
--
ALTER TABLE `tramsFYAPResultSet`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `tramsPOPChangelog`
--
ALTER TABLE `tramsPOPChangelog`
  ADD PRIMARY KEY (`POPChangelogId`),
  ADD KEY `IX_tramsPOPChangelog_ContractAwardId` (`ContractAwardId`),
  ADD KEY `IX_tramsPOPChangelog_ApplicationId` (`ApplicationId`),
  ADD KEY `IX_tramsPOPChangelog_BudgetRevisionId` (`BudgetRevisionId`),
  ADD KEY `IX_tramsPOPChangelog_SourceOfChangeId` (`SourceOfChangeId`);

--
-- Indexes for table `tramsRecoveredDeobligationDetail`
--
ALTER TABLE `tramsRecoveredDeobligationDetail`
  ADD PRIMARY KEY (`RecoveredDeobligationDetailId`),
  ADD KEY `fkToDeobDetailId` (`DeobligationDetailId`);

--
-- Indexes for table `tramsReservationSelectedAccountClassCode`
--
ALTER TABLE `tramsReservationSelectedAccountClassCode`
  ADD PRIMARY KEY (`ReservationSelectedAccountClassCodeId`),
  ADD KEY `IX_ReservationSelectedAccountClassCode_ApplicationId` (`ApplicationId`);

--
-- Indexes for table `tramsReviewDecision`
--
ALTER TABLE `tramsReviewDecision`
  ADD PRIMARY KEY (`ReviewId`),
  ADD KEY `IX_ReviewDecision_ApplicationReviewId` (`ApplicationReviewId`),
  ADD KEY `IX_ReviewDecision_ReviewTypeId` (`ReviewTypeId`),
  ADD KEY `IX_ReviewDecision_DecisionId` (`DecisionId`);

--
-- Indexes for table `tramsSystemAnnouncement`
--
ALTER TABLE `tramsSystemAnnouncement`
  ADD PRIMARY KEY (`TramsSystemAnnouncementId`);

--
-- Indexes for table `trams_account_class_code`
--
ALTER TABLE `trams_account_class_code`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsprtingbdgt_ccntclsscdes` (`trmsprtn_ccntclss_prtngbdg`),
  ADD KEY `account_class_code_authorized_flag` (`authorized_flag`),
  ADD KEY `account_class_code_account_code` (`account_class_code`),
  ADD KEY `account_class_code_opbid` (`trmsprtn_ccntclss_prtngbdg`),
  ADD KEY `account_class_code_acctcode_fy_costctrcode` (`account_class_code`,`fiscal_year`,`cost_center_code`),
  ADD KEY `account_class_code_fiscalyear` (`fiscal_year`);

--
-- Indexes for table `trams_account_class_code_history`
--
ALTER TABLE `trams_account_class_code_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_class_code_history_acctcode` (`account_class_code`),
  ADD KEY `accountclasscodetext` (`account_class_code`),
  ADD KEY `transactionFiscalYearText` (`transaction_fiscal_year`);

--
-- Indexes for table `trams_account_class_code_lookup`
--
ALTER TABLE `trams_account_class_code_lookup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `acct_class_code` (`account_class_code`);

--
-- Indexes for table `trams_acc_fpc_reference`
--
ALTER TABLE `trams_acc_fpc_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `accountClassCode_text` (`account_class_code`(255)),
  ADD KEY `ACC_FPC_reference_acctcode` (`account_class_code`(255));

--
-- Indexes for table `trams_allocation_type`
--
ALTER TABLE `trams_allocation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_allotment_advice`
--
ALTER TABLE `trams_allotment_advice`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_allotment_code`
--
ALTER TABLE `trams_allotment_code`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmslltmentdvice_lltmentcde` (`allotmentAdviceID_int`);

--
-- Indexes for table `trams_allotment_code_lookup`
--
ALTER TABLE `trams_allotment_code_lookup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_AllotmentCodeLookup_FundingFiscalYear` (`funding_fiscal_year`),
  ADD KEY `IX_AllotmentCodeLookup_AppropriationCode` (`appropriation_code`);

--
-- Indexes for table `trams_application`
--
ALTER TABLE `trams_application`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscntrctwrd_wrdapplictins` (`contractAward_id`),
  ADD KEY `trmspplctin_pplictinwrddtil` (`trmspplctnwr_pplctnwrddtl_d`),
  ADD KEY `active` (`active_flag`),
  ADD KEY `application_status_fy` (`status`(255),`fiscal_year`),
  ADD KEY `recipientdunstext` (`duns`(255)),
  ADD KEY `trams_application_granteeid` (`recipient_id`),
  ADD KEY `trams_application_fy` (`fiscal_year`),
  ADD KEY `temp_application_number` (`temp_application_number`(255)),
  ADD KEY `app_number_lookup` (`application_number`(40));
ALTER TABLE `trams_application` ADD FULLTEXT KEY `trams_application_name_fulltext_index` (`application_name`);

--
-- Indexes for table `trams_application_award_detail`
--
ALTER TABLE `trams_application_award_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `app_number_lookup` (`application_number`(40));

--
-- Indexes for table `trams_application_fleet_status`
--
ALTER TABLE `trams_application_fleet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_application_point_of_contact`
--
ALTER TABLE `trams_application_point_of_contact`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsapplictin_pintfcntacts` (`applicationidint`);

--
-- Indexes for table `trams_application_subrecipient`
--
ALTER TABLE `trams_application_subrecipient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_application_transaction`
--
ALTER TABLE `trams_application_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_application_validation`
--
ALTER TABLE `trams_application_validation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_apportionment`
--
ALTER TABLE `trams_apportionment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsapportionment_state` (`tramsstate_state_stateid`),
  ADD KEY `tramsapprtinment_rbnizedare` (`uza_id`),
  ADD KEY `trmspprtinmnt_pprtinmnttype` (`apportionmentTypeId`),
  ADD KEY `trmspprtinmnt_rbnizedre5337` (`uza5337_id`),
  ADD KEY `is_active_bool` (`active_flag`),
  ADD KEY `apportionment_ffy_appr_sec_lim` (`uza_code`,`funding_fiscal_year`,`appropriation_code`,`section_id`,`limitation_code`);

--
-- Indexes for table `trams_apportionment_history`
--
ALTER TABLE `trams_apportionment_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `apportionment_hist_ffy_appr_sec_lim` (`uza_code`,`funding_fiscal_year`,`appropriation_code`,`section_id`,`limitation_code`),
  ADD KEY `actiontypetext` (`action_type`(255)),
  ADD KEY `uzacodetext` (`uza_code`),
  ADD KEY `fundingfiscalyeartext` (`funding_fiscal_year`),
  ADD KEY `appropriationcodetext` (`appropriation_code`),
  ADD KEY `sectionidtext` (`section_id`),
  ADD KEY `limitationcodetext` (`limitation_code`);

--
-- Indexes for table `trams_apportionment_type`
--
ALTER TABLE `trams_apportionment_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_appropriations`
--
ALTER TABLE `trams_appropriations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramspreaward_apprpriations` (`preAwardID`);

--
-- Indexes for table `trams_appropriation_adjustments`
--
ALTER TABLE `trams_appropriation_adjustments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramspreaward_adjustments` (`preAwardID`);

--
-- Indexes for table `trams_appropriation_reference`
--
ALTER TABLE `trams_appropriation_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_app_congressional_release`
--
ALTER TABLE `trams_app_congressional_release`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appId_int` (`app_id`);

--
-- Indexes for table `trams_app_discretionary_allocations`
--
ALTER TABLE `trams_app_discretionary_allocations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `app_discretionary_allocations_application` (`app_id`);

--
-- Indexes for table `trams_audit_history`
--
ALTER TABLE `trams_audit_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_budget_planning_summary`
--
ALTER TABLE `trams_budget_planning_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_budget_revision`
--
ALTER TABLE `trams_budget_revision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `contract_award_id` (`contract_award_id`);

--
-- Indexes for table `trams_budget_summary_5305d`
--
ALTER TABLE `trams_budget_summary_5305d`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5305` (`trmsbdgtplnn_bdgtsmmry530_d`);

--
-- Indexes for table `trams_budget_summary_5305e`
--
ALTER TABLE `trams_budget_summary_5305e`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5305e` (`trmsbdgtplnn_bdgtsmmry530_d`);

--
-- Indexes for table `trams_budget_summary_5307`
--
ALTER TABLE `trams_budget_summary_5307`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5307` (`trmsbdgtplnn_bdgtsmmry530_d`);

--
-- Indexes for table `trams_budget_summary_5310`
--
ALTER TABLE `trams_budget_summary_5310`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5310` (`trmsbdgtplnn_bdgtsmmry531_d`);

--
-- Indexes for table `trams_budget_summary_5311`
--
ALTER TABLE `trams_budget_summary_5311`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5311` (`trmsbdgtplnn_bdgtsmmry531_d`);

--
-- Indexes for table `trams_budget_summary_5337`
--
ALTER TABLE `trams_budget_summary_5337`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5337` (`trmsbdgtplnn_bdgtsmmry533_d`);

--
-- Indexes for table `trams_budget_summary_5339`
--
ALTER TABLE `trams_budget_summary_5339`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5339` (`trmsbdgtplnn_bdgtsmmry533_d`);

--
-- Indexes for table `trams_budget_summary_5340`
--
ALTER TABLE `trams_budget_summary_5340`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsbdgtplnnn_bdgtsmmry5340` (`trmsbdgtplnn_bdgtsmmry534_d`);

--
-- Indexes for table `trams_categorical_exclusion`
--
ALTER TABLE `trams_categorical_exclusion`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsnvrnmntlfn_ctgrclxclsns` (`findingidint`);

--
-- Indexes for table `trams_certification_assurance`
--
ALTER TABLE `trams_certification_assurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_cert_assurance_lineitem`
--
ALTER TABLE `trams_cert_assurance_lineitem`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscrtfctnssr_crtfctnlntms` (`certification_assurance_id`);

--
-- Indexes for table `trams_civil_rights_compliance`
--
ALTER TABLE `trams_civil_rights_compliance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `civil_rights_compliance_recipient_id_idx` (`recipient_id`),
  ADD KEY `civil_rights_compliance_program_name_idx` (`program_name`(255)),
  ADD KEY `RecipientGoals_idx` (`recipient_id`,`program_name`);

--
-- Indexes for table `trams_civil_rights_grantee_history`
--
ALTER TABLE `trams_civil_rights_grantee_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscvlrghtsc_cvlrghtsgrnth` (`civilRightsId_int`);

--
-- Indexes for table `trams_congressional_appropriation`
--
ALTER TABLE `trams_congressional_appropriation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_contract_acc_funding`
--
ALTER TABLE `trams_contract_acc_funding`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscntrctwrd_cntrctccfndng` (`contract_award_id`),
  ADD KEY `funding_source_name` (`funding_source_name`),
  ADD KEY `app_id_index` (`application_id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `uza_code` (`uza_code`);

--
-- Indexes for table `trams_contract_acc_transaction`
--
ALTER TABLE `trams_contract_acc_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscntrctwrd_cntrctcctrnsc` (`contract_award_id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `contract_acc_trans_awardid_fundingsourcename` (`contract_award_id`,`funding_source_name`),
  ADD KEY `contract_acc_trans_fundingsourcename` (`funding_source_name`),
  ADD KEY `contract_acc_trans_appid` (`application_id`),
  ADD KEY `contract_acc_trans_awardid_fpc` (`contract_award_id`,`fpc`),
  ADD KEY `contract_acc_trans_awardid_fpc_long_idx` (`contract_award_id`,`fpc`,`recipient_id`,`application_id`,`project_id`,`funding_source_name`,`scope_code`,`uza_code`,`cost_center_code`,`account_class_code`),
  ADD KEY `contract_acc_transaction_granteeid_fpc_long_idx` (`recipient_id`,`contract_award_id`,`application_id`,`project_id`,`fpc`),
  ADD KEY `contract_acc_transaction_pid_fundingsourcename_seccode` (`project_id`,`funding_source_name`,`section_code`),
  ADD KEY `trams_contract_acc_trans_proj_number_idx` (`project_number`),
  ADD KEY `tmp_contract_award_funding_acc_appid` (`application_id`),
  ADD KEY `tmp_contract_award_funding_acc_contractawardid` (`contract_award_id`),
  ADD KEY `tmp_contract_award_funding_acc_granteeid` (`recipient_id`),
  ADD KEY `transactionOriginRefId_int` (`transaction_origin_ref_id`),
  ADD KEY `IX_trams_contract_acc_transaction_project_id` (`project_id`);

--
-- Indexes for table `trams_contract_award`
--
ALTER TABLE `trams_contract_award`
  ADD PRIMARY KEY (`id`),
  ADD KEY `current_amendment_number` (`current_amendment_number`),
  ADD KEY `IX_trams_contract_award_current_amendment_number` (`current_amendment_number`);

--
-- Indexes for table `trams_contract_award_funding_acc_mpr`
--
ALTER TABLE `trams_contract_award_funding_acc_mpr`
  ADD KEY `grantee_id` (`grantee_id`),
  ADD KEY `application_id` (`application_id`),
  ADD KEY `contract_award_id` (`contract_award_id`);

--
-- Indexes for table `trams_cost_center`
--
ALTER TABLE `trams_cost_center`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code` (`cost_center_code`(255));

--
-- Indexes for table `trams_created_document_folders`
--
ALTER TABLE `trams_created_document_folders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_dbe_report`
--
ALTER TABLE `trams_dbe_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_trams_dbe_report_recipient_id` (`recipient_id`);

--
-- Indexes for table `trams_deobligation_detail`
--
ALTER TABLE `trams_deobligation_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `currentAmendmentId_int` (`current_amendment_id`),
  ADD KEY `transactionOriginRefId_int` (`transaction_origin_ref_id`),
  ADD KEY `IX_DeobligationDetail_BudgetRevisionId` (`BudgetRevisionId`);

--
-- Indexes for table `trams_designated_recipients`
--
ALTER TABLE `trams_designated_recipients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_direct_recipients`
--
ALTER TABLE `trams_direct_recipients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_discretionary_allocations`
--
ALTER TABLE `trams_discretionary_allocations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_DiscretionaryAllocationsDiscretionaryId` (`discretionary_id`),
  ADD KEY `IX_DiscretionaryAllocationsRecipientId` (`recipient_id`);

--
-- Indexes for table `trams_discretionary_program_lookup`
--
ALTER TABLE `trams_discretionary_program_lookup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_document`
--
ALTER TABLE `trams_document`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trams_document_application_id` (`tramsApplication_documents_id`),
  ADD KEY `tramspreawrd_preawrddcments` (`congressional_appropriation_id`),
  ADD KEY `tramsproject_documents` (`tramsproject_documents_id`),
  ADD KEY `tramsapplication_documents` (`tramsapplicatin_docments_id`);

--
-- Indexes for table `trams_document_content_reference`
--
ALTER TABLE `trams_document_content_reference`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `trams_doc_content_ref_cat_loc_uniq_idx` (`category`,`content_location`,`description`);

--
-- Indexes for table `trams_entity`
--
ALTER TABLE `trams_entity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsentity_financials` (`trmsnttyfinncil_finncils_id`),
  ADD KEY `tramsentity_generalinfo` (`trmsgnrlbsnssinf_gnrlinf_id`);

--
-- Indexes for table `trams_entity_financial`
--
ALTER TABLE `trams_entity_financial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_environmental_finding`
--
ALTER TABLE `trams_environmental_finding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_environmental_finding_date`
--
ALTER TABLE `trams_environmental_finding_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsnvrnmntlfnd_nvrnmntldts` (`findingidint`);

--
-- Indexes for table `trams_fain_sequence`
--
ALTER TABLE `trams_fain_sequence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fain_identifier_UNIQUE` (`fain_identifier`);

--
-- Indexes for table `trams_fda_number`
--
ALTER TABLE `trams_fda_number`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_trams_fda_number_fed_domestic_asst_num` (`fed_domestic_asst_num`);

--
-- Indexes for table `trams_federal_financial_report`
--
ALTER TABLE `trams_federal_financial_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contractAwardID` (`contract_award_id`),
  ADD KEY `fiscalYear` (`fiscal_year`(255));

--
-- Indexes for table `trams_financial_purpose`
--
ALTER TABLE `trams_financial_purpose`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_fiscal_year_apportionments`
--
ALTER TABLE `trams_fiscal_year_apportionments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fy_apportionment_idx` (`fiscal_year`,`funding_fiscal_year`,`appropriation_code`,`section_code`,`limitation_code`,`uza_code`),
  ADD KEY `uza_code` (`uza_code`);

--
-- Indexes for table `trams_fleet_status`
--
ALTER TABLE `trams_fleet_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_fleet_vehicle`
--
ALTER TABLE `trams_fleet_vehicle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsfleetstts_fleetvehicle` (`trmsfleetstts_fleetvehiclid`),
  ADD KEY `trmspplctinfltstts_fltvhicl` (`trmspplctnfltstts_fltvhclid`);

--
-- Indexes for table `trams_fms_compare`
--
ALTER TABLE `trams_fms_compare`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_fta_document`
--
ALTER TABLE `trams_fta_document`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_fta_review`
--
ALTER TABLE `trams_fta_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application_id` (`application_id`);

--
-- Indexes for table `trams_fuel`
--
ALTER TABLE `trams_fuel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_fuel_type`
--
ALTER TABLE `trams_fuel_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsfuel_fueltype` (`tramsfuel_fueltype_id`);

--
-- Indexes for table `trams_funding_source_reference`
--
ALTER TABLE `trams_funding_source_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsfndngsrcrfrn_lmttnrfrnc` (`trmslmttnrfrnc_lmttnrfrncid`),
  ADD KEY `IX_FundingSourceReferenceFundingSourceName` (`funding_source_name`),
  ADD KEY `OriginalFundingSourceId_FK` (`original_funding_source_id`),
  ADD KEY `IX_trams_funding_source_reference_fed_dom_assistance_num` (`fed_dom_assistance_num`);

--
-- Indexes for table `trams_general_business_info`
--
ALTER TABLE `trams_general_business_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `recipientidtext` (`recipient_id`(255)),
  ADD KEY `dunstext` (`duns`(255)),
  ADD KEY `costcentertext` (`cost_center_code`(255)),
  ADD KEY `recipient_id` (`recipient_id`(255)),
  ADD KEY `organizationId` (`organizationId`),
  ADD KEY `DaimsBusinessType_FK` (`DaimsBusinessType`);
ALTER TABLE `trams_general_business_info` ADD FULLTEXT KEY `trams_general_business_info_name_fulltext_index` (`legal_business_name`);

--
-- Indexes for table `trams_geographic_identifiers`
--
ALTER TABLE `trams_geographic_identifiers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsentity_ggrphicidntifirs` (`trmsntty_ggrphcidntifirs_id`);

--
-- Indexes for table `trams_grantee_cert_assurance`
--
ALTER TABLE `trams_grantee_cert_assurance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cert_fiscalYear_idx` (`fiscal_year`(255)),
  ADD KEY `cert_recipientId_idx` (`recipient_id`);

--
-- Indexes for table `trams_grantee_cert_assurance_line_item`
--
ALTER TABLE `trams_grantee_cert_assurance_line_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsgrntcrtssrn_crtctnlntms` (`grantee_cert_assurance_id`);

--
-- Indexes for table `trams_grantee_poc`
--
ALTER TABLE `trams_grantee_poc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `granteeDUNS` (`duns`(255));

--
-- Indexes for table `trams_grantee_review`
--
ALTER TABLE `trams_grantee_review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_latest_announced_preaward_by_fy_view`
--
ALTER TABLE `trams_latest_announced_preaward_by_fy_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_limitation_reference`
--
ALTER TABLE `trams_limitation_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_line_item`
--
ALTER TABLE `trams_line_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `line_item_appid` (`app_id`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `revised_amt_idx` (`revised_FTA_amount`,`revised_total_amount`),
  ADD KEY `line_item_app_project_idx` (`app_id`,`project_id`),
  ADD KEY `line_item_appid_fundingsourcename_idx` (`app_id`,`funding_source_name`),
  ADD KEY `line_item_appid_prjid_fsn_idx` (`app_id`,`project_id`,`funding_source_name`),
  ADD KEY `line_item_fundingsourcename_idx` (`funding_source_name`),
  ADD KEY `trams_line_item_scope_id` (`scope_id`),
  ADD KEY `original_line_item_id` (`original_line_item_id`),
  ADD KEY `line_item_number` (`line_item_number`(255)),
  ADD KEY `IX_trams_line_item_project_id_app_id_section_code` (`project_id`,`app_id`,`section_code`);

--
-- Indexes for table `trams_line_item_activity_type`
--
ALTER TABLE `trams_line_item_activity_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsscptyp_linitemctivities` (`scope_type_id`);

--
-- Indexes for table `trams_line_item_allocation_type`
--
ALTER TABLE `trams_line_item_allocation_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsscptype_lineitemllctins` (`scope_type_id`);

--
-- Indexes for table `trams_line_item_change_log`
--
ALTER TABLE `trams_line_item_change_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `line_item_change_log_lineitemid` (`line_item_id`),
  ADD KEY `budget_revision_id` (`budget_revision_id`),
  ADD KEY `line_item_change_log_appId` (`application_id`);

--
-- Indexes for table `trams_line_item_reference`
--
ALTER TABLE `trams_line_item_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scopeCode_idx` (`scope_code`(7)),
  ADD KEY `activity_idx` (`activity_type`(20));

--
-- Indexes for table `trams_line_item_temp`
--
ALTER TABLE `trams_line_item_temp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_line_item_type`
--
ALTER TABLE `trams_line_item_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsscopetype_lineitems` (`scope_type_id`);

--
-- Indexes for table `trams_location`
--
ALTER TABLE `trams_location`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsentity_locations` (`tramsentity_locations_id`),
  ADD KEY `grantee_id` (`grantee_id`(255));

--
-- Indexes for table `trams_milestones`
--
ALTER TABLE `trams_milestones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramslineitem_milestones` (`line_item_id`);

--
-- Indexes for table `trams_milestone_progress_contractors`
--
ALTER TABLE `trams_milestone_progress_contractors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_milestone_progress_remarks`
--
ALTER TABLE `trams_milestone_progress_remarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `milestone_progress_remarks_milestoneid` (`milestone_id`),
  ADD KEY `milestone_progress_remarks_milestone_prog_report` (`report_id`),
  ADD KEY `milestone_progress_remarks_org_milestoneid` (`original_milestone_id`);

--
-- Indexes for table `trams_milestone_progress_report`
--
ALTER TABLE `trams_milestone_progress_report`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contract_award_id` (`contract_award_id`),
  ADD KEY `fiscal_year` (`fiscal_year`(255)),
  ADD KEY `applicationID` (`application_id`),
  ADD KEY `IX_trams_milestone_progress_report_report_status` (`report_status`),
  ADD KEY `IX_trams_milestone_progress_report_fiscal_quarter` (`fiscal_quarter`),
  ADD KEY `IX_trams_milestone_progress_report_fiscal_month` (`fiscal_month`),
  ADD KEY `IX_trams_milestone_progress_report_report_period_type` (`report_period_type`),
  ADD KEY `IX_trams_milestone_progress_report_due_date_time` (`due_date_time`),
  ADD KEY `IX_trams_milestone_progress_report_final_report_flag` (`final_report_flag`);

--
-- Indexes for table `trams_omb_apportionment`
--
ALTER TABLE `trams_omb_apportionment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_operating_budget`
--
ALTER TABLE `trams_operating_budget`
  ADD PRIMARY KEY (`id`),
  ADD KEY `operating_budget_fy_costctr_sectionid` (`fiscal_year`(255),`cost_center_name`(255),`section_id`(255)),
  ADD KEY `operating_budget_status_version` (`status`(255),`version`);

--
-- Indexes for table `trams_operating_budget_latest_rollup_by_allotment_code_view`
--
ALTER TABLE `trams_operating_budget_latest_rollup_by_allotment_code_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_po_number`
--
ALTER TABLE `trams_po_number`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contract_award_id` (`contract_award_id`),
  ADD KEY `trams_ponumber_application_id_idx` (`application_id`);

--
-- Indexes for table `trams_po_number_sequence`
--
ALTER TABLE `trams_po_number_sequence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `po_identifier_UNIQUE` (`po_identifier`);

--
-- Indexes for table `trams_program_apportionments`
--
ALTER TABLE `trams_program_apportionments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_program_name_reference`
--
ALTER TABLE `trams_program_name_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_program_plan_type`
--
ALTER TABLE `trams_program_plan_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_program_type_reference`
--
ALTER TABLE `trams_program_type_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_project`
--
ALTER TABLE `trams_project`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsproject_programplan` (`trmsprgrmplntyp_prgrmpln_id`),
  ADD KEY `tramsapplication_projects` (`tramsapplicatin_projects_id`),
  ADD KEY `applicationidint` (`application_id`);

--
-- Indexes for table `trams_project_environmental_finding`
--
ALTER TABLE `trams_project_environmental_finding`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_env_finding_project` (`project_id`);

--
-- Indexes for table `trams_project_environmental_finding_date`
--
ALTER TABLE `trams_project_environmental_finding_date`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_env_finding_date_projectfindingid` (`project_finding_id`);

--
-- Indexes for table `trams_project_env_finding_line_items`
--
ALTER TABLE `trams_project_env_finding_line_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_env_line_items_lineitemid` (`line_item_id`),
  ADD KEY `project_env_finding_projectfindingid` (`project_finding_id`);

--
-- Indexes for table `trams_project_geography`
--
ALTER TABLE `trams_project_geography`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsprject_prjectgeography` (`trmsprject_prjectgegrphy_id`);

--
-- Indexes for table `trams_project_scope`
--
ALTER TABLE `trams_project_scope`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_project_scope_type`
--
ALTER TABLE `trams_project_scope_type`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsprjctscp_prjctscpetypes` (`project_scope_id`);

--
-- Indexes for table `trams_reassigned_task`
--
ALTER TABLE `trams_reassigned_task`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_references`
--
ALTER TABLE `trams_references`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `refTypeId_int` (`ref_type_id`);

--
-- Indexes for table `trams_reference_types`
--
ALTER TABLE `trams_reference_types`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `trams_regional_office_states`
--
ALTER TABLE `trams_regional_office_states`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmscstcntr_rginlfficesttes` (`trmscstcntr_rgnlfficstts_id`);

--
-- Indexes for table `trams_report_remarks`
--
ALTER TABLE `trams_report_remarks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `report_type` (`report_type`(255)),
  ADD KEY `contract_award_id` (`contract_award_id`),
  ADD KEY `report_id` (`report_id`);

--
-- Indexes for table `trams_reservation`
--
ALTER TABLE `trams_reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_applicationid` (`application_id`);

--
-- Indexes for table `trams_review`
--
ALTER TABLE `trams_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsfuel_review` (`tramsfuel_review_id`),
  ADD KEY `tramsprojectscope_reviews` (`tramsprjectscope_reviews_id`),
  ADD KEY `tramsscopetype_reviews` (`tramsscopetype_reviews_id`),
  ADD KEY `trmsgrntnvrnmntlf_rvwcmmnts` (`trmsgrntnvrnmnt_rvwcmmnts_d`);

--
-- Indexes for table `trams_review_comments`
--
ALTER TABLE `trams_review_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `appId_idx` (`application_id`),
  ADD KEY `context_idx` (`comment_context_text`(30)),
  ADD KEY `budgRevId_idx` (`budget_revision_id`),
  ADD KEY `recipientId_idx` (`recipient_id`),
  ADD KEY `civilRightsId_idx` (`civil_rights_id`);

--
-- Indexes for table `trams_sam_poc_address`
--
ALTER TABLE `trams_sam_poc_address`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsentity_sampintfcontact` (`tramsentity_smpintfcntct_id`),
  ADD KEY `dunstext` (`duns`(255));

--
-- Indexes for table `trams_scope`
--
ALTER TABLE `trams_scope`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trams_scope_application_id_idx` (`application_id`),
  ADD KEY `trams_scope_project_id_idx` (`project_id`);

--
-- Indexes for table `trams_scope_acc`
--
ALTER TABLE `trams_scope_acc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `scope_acc_project_scope_idx` (`project_id`,`scope_number`),
  ADD KEY `projectID_int` (`project_id`),
  ADD KEY `scopeNumber_text` (`scope_number`),
  ADD KEY `scope_acc_reservationid` (`reservation_id`),
  ADD KEY `funding_source_group` (`project_id`,`funding_source_name`),
  ADD KEY `IX_ScopeAccApplicationIdAccountClassCode` (`application_id`,`account_class_code`);

--
-- Indexes for table `trams_scope_type`
--
ALTER TABLE `trams_scope_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_section_5337_area`
--
ALTER TABLE `trams_section_5337_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_section_5337_area_allocations`
--
ALTER TABLE `trams_section_5337_area_allocations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmssctn5337rllctn_llctntyp` (`allocationTypeId`);

--
-- Indexes for table `trams_section_code_reference`
--
ALTER TABLE `trams_section_code_reference`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_security`
--
ALTER TABLE `trams_security`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsprjectrevision_secrity` (`trmsprjctrevisin_secrity_id`),
  ADD KEY `tramsproject_security` (`tramsproject_security_id`);

--
-- Indexes for table `trams_sp_log_c`
--
ALTER TABLE `trams_sp_log_c`
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `trams_sp_log_config`
--
ALTER TABLE `trams_sp_log_config`
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `trams_state`
--
ALTER TABLE `trams_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_state_acs_data`
--
ALTER TABLE `trams_state_acs_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstateacsdata_state` (`state_id`);

--
-- Indexes for table `trams_state_allocations`
--
ALTER TABLE `trams_state_allocations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramspreawrd_stateallcatins` (`preAwardID`),
  ADD KEY `tramsstateallocations_state` (`tramsstate_state_stateid`),
  ADD KEY `trmsstteallctins_llctintype` (`allocationTypeId`),
  ADD KEY `trmsstteallctins_rbnizedare` (`trmsrbnizedre_rbnizedre_zid`);

--
-- Indexes for table `trams_state_allocation_factor`
--
ALTER TABLE `trams_state_allocation_factor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstteallctinfactr_state` (`state_id`);

--
-- Indexes for table `trams_state_appalachian_factor`
--
ALTER TABLE `trams_state_appalachian_factor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstteappalchinfctr_stte` (`state_id`);

--
-- Indexes for table `trams_state_application_review`
--
ALTER TABLE `trams_state_application_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmsapplictin_stteappreview` (`trmspplictin_sttppreview_id`),
  ADD KEY `applicationidint` (`applicationidint`);

--
-- Indexes for table `trams_state_apportionment_year_population`
--
ALTER TABLE `trams_state_apportionment_year_population`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trmssttpprtnmntyrppltin_stt` (`state_id`);

--
-- Indexes for table `trams_state_land_area`
--
ALTER TABLE `trams_state_land_area`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstatelandarea_state` (`state_id`);

--
-- Indexes for table `trams_state_ntd_summary`
--
ALTER TABLE `trams_state_ntd_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstatentdsummary_state` (`state_id`);

--
-- Indexes for table `trams_state_population`
--
ALTER TABLE `trams_state_population`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsstatepopulation_state` (`state_id`);

--
-- Indexes for table `trams_static_reports_record`
--
ALTER TABLE `trams_static_reports_record`
  ADD PRIMARY KEY (`uId`);

--
-- Indexes for table `trams_status_history`
--
ALTER TABLE `trams_status_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IX_trams_status_history_context_id` (`context_id`),
  ADD KEY `IX_trams_status_history_context_details` (`context_details`),
  ADD KEY `IX_trams_status_history_new_status` (`new_status`);

--
-- Indexes for table `trams_stic_ntd_average`
--
ALTER TABLE `trams_stic_ntd_average`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_subrecipient_congress_districts`
--
ALTER TABLE `trams_subrecipient_congress_districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_subrecipient_counties`
--
ALTER TABLE `trams_subrecipient_counties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_sub_allocation`
--
ALTER TABLE `trams_sub_allocation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_sub_allocation_parent`
--
ALTER TABLE `trams_sub_allocation_parent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_sub_recipients`
--
ALTER TABLE `trams_sub_recipients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_suspense_contract_acc_transaction`
--
ALTER TABLE `trams_suspense_contract_acc_transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_system_adjustments`
--
ALTER TABLE `trams_system_adjustments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_take_downs`
--
ALTER TABLE `trams_take_downs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramspreaward_takedowns` (`preAwardID`);

--
-- Indexes for table `trams_team_role_mapping`
--
ALTER TABLE `trams_team_role_mapping`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_transportation_type`
--
ALTER TABLE `trams_transportation_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_urbanized_area`
--
ALTER TABLE `trams_urbanized_area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_user_roles`
--
ALTER TABLE `trams_user_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_name` (`user_name`),
  ADD UNIQUE KEY `system_user_id` (`system_user_id`);

--
-- Indexes for table `trams_user_to_grantee`
--
ALTER TABLE `trams_user_to_grantee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `trams_user_to_grantee_usr_idx` (`user_name`),
  ADD KEY `trams_user_to_grantee_grnte_idx` (`grantee_id`);

--
-- Indexes for table `trams_ussp_county_city`
--
ALTER TABLE `trams_ussp_county_city`
  ADD PRIMARY KEY (`county_city_id`);

--
-- Indexes for table `trams_ussp_grantee_type`
--
ALTER TABLE `trams_ussp_grantee_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_ussp_state`
--
ALTER TABLE `trams_ussp_state`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trams_uza_allocations`
--
ALTER TABLE `trams_uza_allocations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramspreaward_zaallocations` (`preAwardID`),
  ADD KEY `tramszallctins_allcatintype` (`allocationTypeId`),
  ADD KEY `tramszallctins_rbanizedarea` (`uza_id`);

--
-- Indexes for table `trams_uza_allocation_by_state`
--
ALTER TABLE `trams_uza_allocation_by_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramszallcatinbystate_state` (`state_id`),
  ADD KEY `trmszllctinbystt_llctintype` (`allocationTypeId`),
  ADD KEY `trmszllctinbystte_rbnizedre` (`uza_id`);

--
-- Indexes for table `trams_uza_census`
--
ALTER TABLE `trams_uza_census`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramsuzacenss_urbanizedarea` (`uza_id`);

--
-- Indexes for table `trams_uza_code_reference`
--
ALTER TABLE `trams_uza_code_reference`
  ADD PRIMARY KEY (`id`),
  ADD KEY `uza_code` (`uza_code`);

--
-- Indexes for table `trams_uza_ntd_summary`
--
ALTER TABLE `trams_uza_ntd_summary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramszntdsmmry_rbanizedarea` (`uza_id`);

--
-- Indexes for table `trams_uza_ntd_summary_by_state`
--
ALTER TABLE `trams_uza_ntd_summary_by_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramszntdsmmrybystate_state` (`state_id`),
  ADD KEY `trmszntdsmmrybystt_rbnizdre` (`uza_id`);

--
-- Indexes for table `trams_uza_population_by_state`
--
ALTER TABLE `trams_uza_population_by_state`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tramszapplatinbystate_state` (`state_id`),
  ADD KEY `trmszppltinbystte_rbnizedre` (`uza_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tramsAccountClassCodeReservation`
--
ALTER TABLE `tramsAccountClassCodeReservation`
  MODIFY `AccountClassCodeReservationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsAdHocTaskComments`
--
ALTER TABLE `tramsAdHocTaskComments`
  MODIFY `CommentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsAdHocTasks`
--
ALTER TABLE `tramsAdHocTasks`
  MODIFY `AdHocTaskId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsAmendmentErrorLog`
--
ALTER TABLE `tramsAmendmentErrorLog`
  MODIFY `AmendmentErrorLogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationDiscretionaryDetail`
--
ALTER TABLE `tramsApplicationDiscretionaryDetail`
  MODIFY `ApplicationDiscretionaryDetailId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationHoldingPot`
--
ALTER TABLE `tramsApplicationHoldingPot`
  MODIFY `ApplicationHoldingPotId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationHoldingPotAuditHistory`
--
ALTER TABLE `tramsApplicationHoldingPotAuditHistory`
  MODIFY `ApplicationHoldingPotAuditHistoryId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationHoldingPotReservation`
--
ALTER TABLE `tramsApplicationHoldingPotReservation`
  MODIFY `ApplicationHoldingPotReservationId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationReview`
--
ALTER TABLE `tramsApplicationReview`
  MODIFY `ApplicationReviewId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApplicationTransmissionHistory`
--
ALTER TABLE `tramsApplicationTransmissionHistory`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsApportionmentTransferDetails`
--
ALTER TABLE `tramsApportionmentTransferDetails`
  MODIFY `ApportionmentTransferDetailsId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsArchiveAwardAuditLog`
--
ALTER TABLE `tramsArchiveAwardAuditLog`
  MODIFY `LogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsAutomatedRecoveryRecords`
--
ALTER TABLE `tramsAutomatedRecoveryRecords`
  MODIFY `TramsAutomatedRecoveryRecordsId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsDAFISSuffixCodeReference`
--
ALTER TABLE `tramsDAFISSuffixCodeReference`
  MODIFY `DAFISCodeId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsDaimsBusinessTypeReference`
--
ALTER TABLE `tramsDaimsBusinessTypeReference`
  MODIFY `BusinessTypeId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsDiscretionaryChangelog`
--
ALTER TABLE `tramsDiscretionaryChangelog`
  MODIFY `DiscretionaryChangelogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsFYAPResultSet`
--
ALTER TABLE `tramsFYAPResultSet`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsPOPChangelog`
--
ALTER TABLE `tramsPOPChangelog`
  MODIFY `POPChangelogId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsRecoveredDeobligationDetail`
--
ALTER TABLE `tramsRecoveredDeobligationDetail`
  MODIFY `RecoveredDeobligationDetailId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsReservationSelectedAccountClassCode`
--
ALTER TABLE `tramsReservationSelectedAccountClassCode`
  MODIFY `ReservationSelectedAccountClassCodeId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsReviewDecision`
--
ALTER TABLE `tramsReviewDecision`
  MODIFY `ReviewId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tramsSystemAnnouncement`
--
ALTER TABLE `tramsSystemAnnouncement`
  MODIFY `TramsSystemAnnouncementId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_account_class_code`
--
ALTER TABLE `trams_account_class_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_account_class_code_history`
--
ALTER TABLE `trams_account_class_code_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_account_class_code_lookup`
--
ALTER TABLE `trams_account_class_code_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_acc_fpc_reference`
--
ALTER TABLE `trams_acc_fpc_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_allocation_type`
--
ALTER TABLE `trams_allocation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_allotment_advice`
--
ALTER TABLE `trams_allotment_advice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_allotment_code`
--
ALTER TABLE `trams_allotment_code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_allotment_code_lookup`
--
ALTER TABLE `trams_allotment_code_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application`
--
ALTER TABLE `trams_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_award_detail`
--
ALTER TABLE `trams_application_award_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_fleet_status`
--
ALTER TABLE `trams_application_fleet_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_point_of_contact`
--
ALTER TABLE `trams_application_point_of_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_subrecipient`
--
ALTER TABLE `trams_application_subrecipient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_transaction`
--
ALTER TABLE `trams_application_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_application_validation`
--
ALTER TABLE `trams_application_validation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_apportionment`
--
ALTER TABLE `trams_apportionment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_apportionment_history`
--
ALTER TABLE `trams_apportionment_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_appropriations`
--
ALTER TABLE `trams_appropriations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_appropriation_adjustments`
--
ALTER TABLE `trams_appropriation_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_appropriation_reference`
--
ALTER TABLE `trams_appropriation_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_app_congressional_release`
--
ALTER TABLE `trams_app_congressional_release`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_app_discretionary_allocations`
--
ALTER TABLE `trams_app_discretionary_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_audit_history`
--
ALTER TABLE `trams_audit_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_planning_summary`
--
ALTER TABLE `trams_budget_planning_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_revision`
--
ALTER TABLE `trams_budget_revision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5305d`
--
ALTER TABLE `trams_budget_summary_5305d`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5305e`
--
ALTER TABLE `trams_budget_summary_5305e`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5307`
--
ALTER TABLE `trams_budget_summary_5307`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5310`
--
ALTER TABLE `trams_budget_summary_5310`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5311`
--
ALTER TABLE `trams_budget_summary_5311`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5337`
--
ALTER TABLE `trams_budget_summary_5337`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5339`
--
ALTER TABLE `trams_budget_summary_5339`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_budget_summary_5340`
--
ALTER TABLE `trams_budget_summary_5340`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_categorical_exclusion`
--
ALTER TABLE `trams_categorical_exclusion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_certification_assurance`
--
ALTER TABLE `trams_certification_assurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_cert_assurance_lineitem`
--
ALTER TABLE `trams_cert_assurance_lineitem`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_civil_rights_compliance`
--
ALTER TABLE `trams_civil_rights_compliance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_civil_rights_grantee_history`
--
ALTER TABLE `trams_civil_rights_grantee_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_congressional_appropriation`
--
ALTER TABLE `trams_congressional_appropriation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_contract_acc_funding`
--
ALTER TABLE `trams_contract_acc_funding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_contract_acc_transaction`
--
ALTER TABLE `trams_contract_acc_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_contract_award`
--
ALTER TABLE `trams_contract_award`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_cost_center`
--
ALTER TABLE `trams_cost_center`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_created_document_folders`
--
ALTER TABLE `trams_created_document_folders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_dbe_report`
--
ALTER TABLE `trams_dbe_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_deobligation_detail`
--
ALTER TABLE `trams_deobligation_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_designated_recipients`
--
ALTER TABLE `trams_designated_recipients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_direct_recipients`
--
ALTER TABLE `trams_direct_recipients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_discretionary_allocations`
--
ALTER TABLE `trams_discretionary_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_discretionary_program_lookup`
--
ALTER TABLE `trams_discretionary_program_lookup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_document`
--
ALTER TABLE `trams_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_document_content_reference`
--
ALTER TABLE `trams_document_content_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_entity`
--
ALTER TABLE `trams_entity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_entity_financial`
--
ALTER TABLE `trams_entity_financial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_environmental_finding`
--
ALTER TABLE `trams_environmental_finding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_environmental_finding_date`
--
ALTER TABLE `trams_environmental_finding_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fain_sequence`
--
ALTER TABLE `trams_fain_sequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fda_number`
--
ALTER TABLE `trams_fda_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_federal_financial_report`
--
ALTER TABLE `trams_federal_financial_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_financial_purpose`
--
ALTER TABLE `trams_financial_purpose`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fiscal_year_apportionments`
--
ALTER TABLE `trams_fiscal_year_apportionments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fleet_status`
--
ALTER TABLE `trams_fleet_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fleet_vehicle`
--
ALTER TABLE `trams_fleet_vehicle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fms_compare`
--
ALTER TABLE `trams_fms_compare`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fta_document`
--
ALTER TABLE `trams_fta_document`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fta_review`
--
ALTER TABLE `trams_fta_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fuel`
--
ALTER TABLE `trams_fuel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_fuel_type`
--
ALTER TABLE `trams_fuel_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_funding_source_reference`
--
ALTER TABLE `trams_funding_source_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_general_business_info`
--
ALTER TABLE `trams_general_business_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_geographic_identifiers`
--
ALTER TABLE `trams_geographic_identifiers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_grantee_cert_assurance`
--
ALTER TABLE `trams_grantee_cert_assurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_grantee_cert_assurance_line_item`
--
ALTER TABLE `trams_grantee_cert_assurance_line_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_grantee_poc`
--
ALTER TABLE `trams_grantee_poc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_grantee_review`
--
ALTER TABLE `trams_grantee_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_latest_announced_preaward_by_fy_view`
--
ALTER TABLE `trams_latest_announced_preaward_by_fy_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_limitation_reference`
--
ALTER TABLE `trams_limitation_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item`
--
ALTER TABLE `trams_line_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_activity_type`
--
ALTER TABLE `trams_line_item_activity_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_allocation_type`
--
ALTER TABLE `trams_line_item_allocation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_change_log`
--
ALTER TABLE `trams_line_item_change_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_reference`
--
ALTER TABLE `trams_line_item_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_temp`
--
ALTER TABLE `trams_line_item_temp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_line_item_type`
--
ALTER TABLE `trams_line_item_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_location`
--
ALTER TABLE `trams_location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_milestones`
--
ALTER TABLE `trams_milestones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_milestone_progress_contractors`
--
ALTER TABLE `trams_milestone_progress_contractors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_milestone_progress_remarks`
--
ALTER TABLE `trams_milestone_progress_remarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_milestone_progress_report`
--
ALTER TABLE `trams_milestone_progress_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_omb_apportionment`
--
ALTER TABLE `trams_omb_apportionment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_operating_budget`
--
ALTER TABLE `trams_operating_budget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_operating_budget_latest_rollup_by_allotment_code_view`
--
ALTER TABLE `trams_operating_budget_latest_rollup_by_allotment_code_view`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_po_number`
--
ALTER TABLE `trams_po_number`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_po_number_sequence`
--
ALTER TABLE `trams_po_number_sequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_program_apportionments`
--
ALTER TABLE `trams_program_apportionments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_program_name_reference`
--
ALTER TABLE `trams_program_name_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_program_plan_type`
--
ALTER TABLE `trams_program_plan_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_program_type_reference`
--
ALTER TABLE `trams_program_type_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project`
--
ALTER TABLE `trams_project`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_environmental_finding`
--
ALTER TABLE `trams_project_environmental_finding`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_environmental_finding_date`
--
ALTER TABLE `trams_project_environmental_finding_date`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_env_finding_line_items`
--
ALTER TABLE `trams_project_env_finding_line_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_geography`
--
ALTER TABLE `trams_project_geography`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_scope`
--
ALTER TABLE `trams_project_scope`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_project_scope_type`
--
ALTER TABLE `trams_project_scope_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_reassigned_task`
--
ALTER TABLE `trams_reassigned_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_references`
--
ALTER TABLE `trams_references`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_reference_types`
--
ALTER TABLE `trams_reference_types`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_regional_office_states`
--
ALTER TABLE `trams_regional_office_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_report_remarks`
--
ALTER TABLE `trams_report_remarks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_reservation`
--
ALTER TABLE `trams_reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_review`
--
ALTER TABLE `trams_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_review_comments`
--
ALTER TABLE `trams_review_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_sam_poc_address`
--
ALTER TABLE `trams_sam_poc_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_scope`
--
ALTER TABLE `trams_scope`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_scope_acc`
--
ALTER TABLE `trams_scope_acc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_scope_type`
--
ALTER TABLE `trams_scope_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_section_5337_area`
--
ALTER TABLE `trams_section_5337_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_section_5337_area_allocations`
--
ALTER TABLE `trams_section_5337_area_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_section_code_reference`
--
ALTER TABLE `trams_section_code_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_security`
--
ALTER TABLE `trams_security`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state`
--
ALTER TABLE `trams_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_acs_data`
--
ALTER TABLE `trams_state_acs_data`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_allocations`
--
ALTER TABLE `trams_state_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_allocation_factor`
--
ALTER TABLE `trams_state_allocation_factor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_appalachian_factor`
--
ALTER TABLE `trams_state_appalachian_factor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_application_review`
--
ALTER TABLE `trams_state_application_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_apportionment_year_population`
--
ALTER TABLE `trams_state_apportionment_year_population`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_land_area`
--
ALTER TABLE `trams_state_land_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_ntd_summary`
--
ALTER TABLE `trams_state_ntd_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_state_population`
--
ALTER TABLE `trams_state_population`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_static_reports_record`
--
ALTER TABLE `trams_static_reports_record`
  MODIFY `uId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_status_history`
--
ALTER TABLE `trams_status_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_stic_ntd_average`
--
ALTER TABLE `trams_stic_ntd_average`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_subrecipient_congress_districts`
--
ALTER TABLE `trams_subrecipient_congress_districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_subrecipient_counties`
--
ALTER TABLE `trams_subrecipient_counties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_sub_allocation`
--
ALTER TABLE `trams_sub_allocation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_sub_allocation_parent`
--
ALTER TABLE `trams_sub_allocation_parent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_sub_recipients`
--
ALTER TABLE `trams_sub_recipients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_suspense_contract_acc_transaction`
--
ALTER TABLE `trams_suspense_contract_acc_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_system_adjustments`
--
ALTER TABLE `trams_system_adjustments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_take_downs`
--
ALTER TABLE `trams_take_downs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_team_role_mapping`
--
ALTER TABLE `trams_team_role_mapping`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_transportation_type`
--
ALTER TABLE `trams_transportation_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_urbanized_area`
--
ALTER TABLE `trams_urbanized_area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_user_roles`
--
ALTER TABLE `trams_user_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_user_to_grantee`
--
ALTER TABLE `trams_user_to_grantee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_ussp_county_city`
--
ALTER TABLE `trams_ussp_county_city`
  MODIFY `county_city_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_ussp_grantee_type`
--
ALTER TABLE `trams_ussp_grantee_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_ussp_state`
--
ALTER TABLE `trams_ussp_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_allocations`
--
ALTER TABLE `trams_uza_allocations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_allocation_by_state`
--
ALTER TABLE `trams_uza_allocation_by_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_census`
--
ALTER TABLE `trams_uza_census`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_code_reference`
--
ALTER TABLE `trams_uza_code_reference`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_ntd_summary`
--
ALTER TABLE `trams_uza_ntd_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_ntd_summary_by_state`
--
ALTER TABLE `trams_uza_ntd_summary_by_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `trams_uza_population_by_state`
--
ALTER TABLE `trams_uza_population_by_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tramsAccountClassCodeReservation`
--
ALTER TABLE `tramsAccountClassCodeReservation`
  ADD CONSTRAINT `AccountClassCodeReservation_Application_ApplicationId_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `AccountClassCodeReservation_Project_ProjectId_FK` FOREIGN KEY (`ProjectId`) REFERENCES `trams_project` (`id`);

--
-- Constraints for table `tramsAdHocTaskComments`
--
ALTER TABLE `tramsAdHocTaskComments`
  ADD CONSTRAINT `CommentAdHocTaskId_FK` FOREIGN KEY (`CommentAdHocTaskId`) REFERENCES `tramsAdHocTasks` (`AdHocTaskId`),
  ADD CONSTRAINT `CommentBy_FK` FOREIGN KEY (`CommentBy`) REFERENCES `ACS_User` (`id`);

--
-- Constraints for table `tramsAdHocTasks`
--
ALTER TABLE `tramsAdHocTasks`
  ADD CONSTRAINT `AdHocTaskRecipient_FK` FOREIGN KEY (`Recipient`) REFERENCES `ACS_User` (`id`),
  ADD CONSTRAINT `AdHocTaskSender_FK` FOREIGN KEY (`Sender`) REFERENCES `ACS_User` (`id`),
  ADD CONSTRAINT `AdHocTasksApplicationId_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`);

--
-- Constraints for table `tramsApplicationDiscretionaryDetail`
--
ALTER TABLE `tramsApplicationDiscretionaryDetail`
  ADD CONSTRAINT `ApplicationDiscretionaryDetail_ApplicationId_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `ApplicationDiscretionaryDetail_StatusId_FK` FOREIGN KEY (`StatusId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `tramsApplicationHoldingPot`
--
ALTER TABLE `tramsApplicationHoldingPot`
  ADD CONSTRAINT `ApplicationHoldingPot_ApplicationId_Application_Id_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `ApplicationHoldingPot_BudgetRevisionId_BudgetRevision_Id_FK` FOREIGN KEY (`BudgetRevisionId`) REFERENCES `trams_budget_revision` (`id`),
  ADD CONSTRAINT `ApplicationHoldingPot_TypeId_References_Id_FK` FOREIGN KEY (`TypeId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `tramsApplicationHoldingPotAuditHistory`
--
ALTER TABLE `tramsApplicationHoldingPotAuditHistory`
  ADD CONSTRAINT `ApplicationHoldingPotAuditHistory_ApplicationHoldingPotId_FK` FOREIGN KEY (`ApplicationHoldingPotId`) REFERENCES `tramsApplicationHoldingPot` (`ApplicationHoldingPotId`);

--
-- Constraints for table `tramsApplicationHoldingPotReservation`
--
ALTER TABLE `tramsApplicationHoldingPotReservation`
  ADD CONSTRAINT `ApplicationHoldingPotReservation_ApplicationHoldingPotId_FK` FOREIGN KEY (`ApplicationHoldingPotId`) REFERENCES `tramsApplicationHoldingPot` (`ApplicationHoldingPotId`),
  ADD CONSTRAINT `ApplicationHoldingPotReservation_Project_ProjectId_FK` FOREIGN KEY (`ProjectId`) REFERENCES `trams_project` (`id`);

--
-- Constraints for table `tramsApplicationReview`
--
ALTER TABLE `tramsApplicationReview`
  ADD CONSTRAINT `ApplicationReview_ApplicationId_Application_Id_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `ApplicationReview_ReviewPhaseId_References_Id_FK` FOREIGN KEY (`ReviewPhaseId`) REFERENCES `trams_references` (`ID`),
  ADD CONSTRAINT `ApplicationReview_StatusId_References_Id_FK` FOREIGN KEY (`StatusId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `tramsApplicationTransmissionHistory`
--
ALTER TABLE `tramsApplicationTransmissionHistory`
  ADD CONSTRAINT `tramsApplicationTransmissionHistory_ibfk_1` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`);

--
-- Constraints for table `tramsApportionmentTransferDetails`
--
ALTER TABLE `tramsApportionmentTransferDetails`
  ADD CONSTRAINT `ApportionmentTransferDetails_BudgetRevision_Id_FK` FOREIGN KEY (`BudgetRevisionId`) REFERENCES `trams_budget_revision` (`id`);

--
-- Constraints for table `tramsAutomatedRecoveryRecords`
--
ALTER TABLE `tramsAutomatedRecoveryRecords`
  ADD CONSTRAINT `tramsAutomatedRecoveryRecords_ibfk_1` FOREIGN KEY (`DeobligationId`) REFERENCES `trams_deobligation_detail` (`id`),
  ADD CONSTRAINT `tramsAutomatedRecoveryRecords_ibfk_2` FOREIGN KEY (`AllotmentId`) REFERENCES `trams_allotment_advice` (`id`),
  ADD CONSTRAINT `tramsAutomatedRecoveryRecords_ibfk_3` FOREIGN KEY (`OperatingBudgetId`) REFERENCES `trams_operating_budget` (`id`);

--
-- Constraints for table `tramsDiscretionaryChangelog`
--
ALTER TABLE `tramsDiscretionaryChangelog`
  ADD CONSTRAINT `DiscretionaryChangelog_ApplicationId_References_Id_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `DiscretionaryChangelog_DiscretionaryProgramId_References_Id_FK` FOREIGN KEY (`DiscretionaryProgramId`) REFERENCES `trams_discretionary_allocations` (`id`),
  ADD CONSTRAINT `DiscretionaryChangelog_SourceOfChangeId_References_Id_FK` FOREIGN KEY (`SourceOfChangeId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `tramsPOPChangelog`
--
ALTER TABLE `tramsPOPChangelog`
  ADD CONSTRAINT `POPChangelog_ApplicationId_References_Id_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `POPChangelog_BudgetRevisionId_References_Id_FK` FOREIGN KEY (`BudgetRevisionId`) REFERENCES `trams_budget_revision` (`id`),
  ADD CONSTRAINT `POPChangelog_ContractAwardId_References_Id_FK` FOREIGN KEY (`ContractAwardId`) REFERENCES `trams_contract_award` (`id`),
  ADD CONSTRAINT `POPChangelog_SourceOfChangeId_References_Id_FK` FOREIGN KEY (`SourceOfChangeId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `tramsRecoveredDeobligationDetail`
--
ALTER TABLE `tramsRecoveredDeobligationDetail`
  ADD CONSTRAINT `fkToDeobDetailId` FOREIGN KEY (`DeobligationDetailId`) REFERENCES `trams_deobligation_detail` (`id`);

--
-- Constraints for table `tramsReservationSelectedAccountClassCode`
--
ALTER TABLE `tramsReservationSelectedAccountClassCode`
  ADD CONSTRAINT `ReservationSelectedAccountClassCode_ApplicationId_FK` FOREIGN KEY (`ApplicationId`) REFERENCES `trams_application` (`id`);

--
-- Constraints for table `tramsReviewDecision`
--
ALTER TABLE `tramsReviewDecision`
  ADD CONSTRAINT `ApplicationReviewId_ApplicationReview_ApplicationReviewId_FK` FOREIGN KEY (`ApplicationReviewId`) REFERENCES `tramsApplicationReview` (`ApplicationReviewId`),
  ADD CONSTRAINT `ReviewDecision_DecisionId_References_Id_FK` FOREIGN KEY (`DecisionId`) REFERENCES `trams_references` (`ID`),
  ADD CONSTRAINT `ReviewDecision_ReviewTypeId_References_Id_FK` FOREIGN KEY (`ReviewTypeId`) REFERENCES `trams_references` (`ID`);

--
-- Constraints for table `trams_account_class_code`
--
ALTER TABLE `trams_account_class_code`
  ADD CONSTRAINT `trmsprtingbdgt_ccntclsscdes` FOREIGN KEY (`trmsprtn_ccntclss_prtngbdg`) REFERENCES `trams_operating_budget` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_allotment_code`
--
ALTER TABLE `trams_allotment_code`
  ADD CONSTRAINT `trmslltmentdvice_lltmentcde` FOREIGN KEY (`allotmentAdviceID_int`) REFERENCES `trams_allotment_advice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_application`
--
ALTER TABLE `trams_application`
  ADD CONSTRAINT `trmscntrctwrd_wrdapplictins` FOREIGN KEY (`contractAward_id`) REFERENCES `trams_contract_award` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmspplctnvw_pplctinwrddtil` FOREIGN KEY (`trmspplctnwr_pplctnwrddtl_d`) REFERENCES `trams_application_award_detail` (`id`);

--
-- Constraints for table `trams_application_point_of_contact`
--
ALTER TABLE `trams_application_point_of_contact`
  ADD CONSTRAINT `tramsapplictin_pintfcntacts` FOREIGN KEY (`applicationidint`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_app_congressional_release`
--
ALTER TABLE `trams_app_congressional_release`
  ADD CONSTRAINT `app_congressional_release_appid` FOREIGN KEY (`app_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_app_discretionary_allocations`
--
ALTER TABLE `trams_app_discretionary_allocations`
  ADD CONSTRAINT `app_discretionary_allocations_application` FOREIGN KEY (`app_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_revision`
--
ALTER TABLE `trams_budget_revision`
  ADD CONSTRAINT `trams_budget_revision_ibfk_1` FOREIGN KEY (`contract_award_id`) REFERENCES `trams_contract_award` (`id`),
  ADD CONSTRAINT `trams_budget_revision_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `trams_application` (`id`);

--
-- Constraints for table `trams_budget_summary_5305d`
--
ALTER TABLE `trams_budget_summary_5305d`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5305` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry530_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5305e`
--
ALTER TABLE `trams_budget_summary_5305e`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5305e` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry530_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5307`
--
ALTER TABLE `trams_budget_summary_5307`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5307` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry530_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5310`
--
ALTER TABLE `trams_budget_summary_5310`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5310` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry531_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5311`
--
ALTER TABLE `trams_budget_summary_5311`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5311` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry531_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5337`
--
ALTER TABLE `trams_budget_summary_5337`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5337` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry533_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5339`
--
ALTER TABLE `trams_budget_summary_5339`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5339` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry533_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_budget_summary_5340`
--
ALTER TABLE `trams_budget_summary_5340`
  ADD CONSTRAINT `trmsbdgtplnnn_bdgtsmmry5340` FOREIGN KEY (`trmsbdgtplnn_bdgtsmmry534_d`) REFERENCES `trams_budget_planning_summary` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_categorical_exclusion`
--
ALTER TABLE `trams_categorical_exclusion`
  ADD CONSTRAINT `trmsnvrnmntlfn_ctgrclxclsns` FOREIGN KEY (`findingidint`) REFERENCES `trams_environmental_finding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_cert_assurance_lineitem`
--
ALTER TABLE `trams_cert_assurance_lineitem`
  ADD CONSTRAINT `certificationAssuranceID` FOREIGN KEY (`certification_assurance_id`) REFERENCES `trams_certification_assurance` (`id`);

--
-- Constraints for table `trams_civil_rights_grantee_history`
--
ALTER TABLE `trams_civil_rights_grantee_history`
  ADD CONSTRAINT `trmscvlrghtsc_cvlrghtsgrnth` FOREIGN KEY (`civilRightsId_int`) REFERENCES `trams_civil_rights_compliance` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_contract_acc_funding`
--
ALTER TABLE `trams_contract_acc_funding`
  ADD CONSTRAINT `trmscntrctwrd_cntrctccfndng` FOREIGN KEY (`contract_award_id`) REFERENCES `trams_contract_award` (`id`);

--
-- Constraints for table `trams_contract_acc_transaction`
--
ALTER TABLE `trams_contract_acc_transaction`
  ADD CONSTRAINT `contractTransactionOriginRefId_int` FOREIGN KEY (`transaction_origin_ref_id`) REFERENCES `trams_references` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmscntrctwrd_cntrctcctrnsc` FOREIGN KEY (`contract_award_id`) REFERENCES `trams_contract_award` (`id`);

--
-- Constraints for table `trams_dbe_report`
--
ALTER TABLE `trams_dbe_report`
  ADD CONSTRAINT `trams_recipient_id_fk` FOREIGN KEY (`recipient_id`) REFERENCES `trams_general_business_info` (`id`);

--
-- Constraints for table `trams_deobligation_detail`
--
ALTER TABLE `trams_deobligation_detail`
  ADD CONSTRAINT `DeobligationDetail_BudgetRevisionId_BudgetRevision_Id_FK` FOREIGN KEY (`BudgetRevisionId`) REFERENCES `trams_budget_revision` (`id`),
  ADD CONSTRAINT `currentAmendmentId_int` FOREIGN KEY (`current_amendment_id`) REFERENCES `trams_application` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `transactionOriginRefId_int` FOREIGN KEY (`transaction_origin_ref_id`) REFERENCES `trams_references` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_document`
--
ALTER TABLE `trams_document`
  ADD CONSTRAINT `trams_document_ibfk_1` FOREIGN KEY (`tramsApplication_documents_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tramsapplication_documents` FOREIGN KEY (`tramsapplicatin_docments_id`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `tramsproject_documents` FOREIGN KEY (`tramsproject_documents_id`) REFERENCES `trams_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_entity`
--
ALTER TABLE `trams_entity`
  ADD CONSTRAINT `tramsentity_financials` FOREIGN KEY (`trmsnttyfinncil_finncils_id`) REFERENCES `trams_entity_financial` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tramsentity_generalinfo` FOREIGN KEY (`trmsgnrlbsnssinf_gnrlinf_id`) REFERENCES `trams_general_business_info` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_environmental_finding_date`
--
ALTER TABLE `trams_environmental_finding_date`
  ADD CONSTRAINT `trmsnvrnmntlfnd_nvrnmntldts` FOREIGN KEY (`findingidint`) REFERENCES `trams_environmental_finding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_fleet_vehicle`
--
ALTER TABLE `trams_fleet_vehicle`
  ADD CONSTRAINT `tramsfleetstts_fleetvehicle` FOREIGN KEY (`trmsfleetstts_fleetvehiclid`) REFERENCES `trams_fleet_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmspplctinfltstts_fltvhicl` FOREIGN KEY (`trmspplctnfltstts_fltvhclid`) REFERENCES `trams_application_fleet_status` (`id`);

--
-- Constraints for table `trams_fuel_type`
--
ALTER TABLE `trams_fuel_type`
  ADD CONSTRAINT `tramsfuel_fueltype` FOREIGN KEY (`tramsfuel_fueltype_id`) REFERENCES `trams_fuel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_funding_source_reference`
--
ALTER TABLE `trams_funding_source_reference`
  ADD CONSTRAINT `OriginalFundingSourceId_FK` FOREIGN KEY (`original_funding_source_id`) REFERENCES `trams_funding_source_reference` (`id`),
  ADD CONSTRAINT `trmsfndngsrcrfrn_lmttnrfrnc` FOREIGN KEY (`trmslmttnrfrnc_lmttnrfrncid`) REFERENCES `trams_limitation_reference` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_general_business_info`
--
ALTER TABLE `trams_general_business_info`
  ADD CONSTRAINT `trams_general_business_info_ibfk_1` FOREIGN KEY (`organizationId`) REFERENCES `ORG_Organization` (`id`),
  ADD CONSTRAINT `trams_general_business_info_ibfk_2` FOREIGN KEY (`DaimsBusinessType`) REFERENCES `tramsDaimsBusinessTypeReference` (`BusinessTypeId`);

--
-- Constraints for table `trams_geographic_identifiers`
--
ALTER TABLE `trams_geographic_identifiers`
  ADD CONSTRAINT `trmsentity_ggrphicidntifirs` FOREIGN KEY (`trmsntty_ggrphcidntifirs_id`) REFERENCES `trams_entity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_grantee_cert_assurance`
--
ALTER TABLE `trams_grantee_cert_assurance`
  ADD CONSTRAINT `trams_grantee_cert_assurance_recipient_id_fk` FOREIGN KEY (`recipient_id`) REFERENCES `trams_general_business_info` (`id`);

--
-- Constraints for table `trams_grantee_cert_assurance_line_item`
--
ALTER TABLE `trams_grantee_cert_assurance_line_item`
  ADD CONSTRAINT `trams_grantee_cert_ass_line_item_grantee_cert_ass_id_fk` FOREIGN KEY (`grantee_cert_assurance_id`) REFERENCES `trams_grantee_cert_assurance` (`id`);

--
-- Constraints for table `trams_line_item`
--
ALTER TABLE `trams_line_item`
  ADD CONSTRAINT `trams_line_item_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `trams_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trams_line_item_ibfk_2` FOREIGN KEY (`app_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trams_line_item_scope_id` FOREIGN KEY (`scope_id`) REFERENCES `trams_scope` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_line_item_activity_type`
--
ALTER TABLE `trams_line_item_activity_type`
  ADD CONSTRAINT `trmsscptyp_linitemctivities` FOREIGN KEY (`scope_type_id`) REFERENCES `trams_scope_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_line_item_allocation_type`
--
ALTER TABLE `trams_line_item_allocation_type`
  ADD CONSTRAINT `trmsscptype_lineitemllctins` FOREIGN KEY (`scope_type_id`) REFERENCES `trams_scope_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_line_item_change_log`
--
ALTER TABLE `trams_line_item_change_log`
  ADD CONSTRAINT `trmsbdgtrvsin_linitmchnglgs` FOREIGN KEY (`budget_revision_id`) REFERENCES `trams_budget_revision` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_line_item_type`
--
ALTER TABLE `trams_line_item_type`
  ADD CONSTRAINT `tramsscopetype_lineitems` FOREIGN KEY (`scope_type_id`) REFERENCES `trams_scope_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_location`
--
ALTER TABLE `trams_location`
  ADD CONSTRAINT `tramsentity_locations` FOREIGN KEY (`tramsentity_locations_id`) REFERENCES `trams_entity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_milestones`
--
ALTER TABLE `trams_milestones`
  ADD CONSTRAINT `tramslineitem_milestones` FOREIGN KEY (`line_item_id`) REFERENCES `trams_line_item` (`id`);

--
-- Constraints for table `trams_milestone_progress_remarks`
--
ALTER TABLE `trams_milestone_progress_remarks`
  ADD CONSTRAINT `milestone_progress_remarks_milestone` FOREIGN KEY (`milestone_id`) REFERENCES `trams_milestones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trams_milestone_progress_remarks_ibfk_1` FOREIGN KEY (`report_id`) REFERENCES `trams_milestone_progress_report` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_milestone_progress_report`
--
ALTER TABLE `trams_milestone_progress_report`
  ADD CONSTRAINT `milestone_progress_report_application` FOREIGN KEY (`application_id`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `milestone_progress_report_contract_award` FOREIGN KEY (`contract_award_id`) REFERENCES `trams_contract_award` (`id`);

--
-- Constraints for table `trams_po_number`
--
ALTER TABLE `trams_po_number`
  ADD CONSTRAINT `trams_po_number_ibfk_1` FOREIGN KEY (`contract_award_id`) REFERENCES `trams_contract_award` (`id`),
  ADD CONSTRAINT `trams_po_number_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `trams_application` (`id`);

--
-- Constraints for table `trams_project`
--
ALTER TABLE `trams_project`
  ADD CONSTRAINT `tramsapplication_projects` FOREIGN KEY (`tramsapplicatin_projects_id`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `tramsproject_programplan` FOREIGN KEY (`trmsprgrmplntyp_prgrmpln_id`) REFERENCES `trams_program_plan_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_project_environmental_finding`
--
ALTER TABLE `trams_project_environmental_finding`
  ADD CONSTRAINT `trams_project_environmental_finding_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `trams_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_project_environmental_finding_date`
--
ALTER TABLE `trams_project_environmental_finding_date`
  ADD CONSTRAINT `trams_project_environmental_finding_date_ibfk_1` FOREIGN KEY (`project_finding_id`) REFERENCES `trams_project_environmental_finding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_project_env_finding_line_items`
--
ALTER TABLE `trams_project_env_finding_line_items`
  ADD CONSTRAINT `trams_project_env_finding_line_items_ibfk_1` FOREIGN KEY (`line_item_id`) REFERENCES `trams_line_item` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trams_project_env_finding_line_items_ibfk_2` FOREIGN KEY (`project_finding_id`) REFERENCES `trams_project_environmental_finding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_project_geography`
--
ALTER TABLE `trams_project_geography`
  ADD CONSTRAINT `tramsprject_prjectgeography` FOREIGN KEY (`trmsprject_prjectgegrphy_id`) REFERENCES `trams_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_project_scope_type`
--
ALTER TABLE `trams_project_scope_type`
  ADD CONSTRAINT `trmsprjctscp_prjctscpetypes` FOREIGN KEY (`project_scope_id`) REFERENCES `trams_project_scope` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_references`
--
ALTER TABLE `trams_references`
  ADD CONSTRAINT `refTypeId_int` FOREIGN KEY (`ref_type_id`) REFERENCES `trams_reference_types` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_regional_office_states`
--
ALTER TABLE `trams_regional_office_states`
  ADD CONSTRAINT `trmscstcntr_rginlfficesttes` FOREIGN KEY (`trmscstcntr_rgnlfficstts_id`) REFERENCES `trams_cost_center` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_reservation`
--
ALTER TABLE `trams_reservation`
  ADD CONSTRAINT `trams_reservation_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_review`
--
ALTER TABLE `trams_review`
  ADD CONSTRAINT `tramsfuel_review` FOREIGN KEY (`tramsfuel_review_id`) REFERENCES `trams_fuel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tramsprojectscope_reviews` FOREIGN KEY (`tramsprjectscope_reviews_id`) REFERENCES `trams_project_scope` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tramsscopetype_reviews` FOREIGN KEY (`tramsscopetype_reviews_id`) REFERENCES `trams_scope_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmsgrntnvrnmntlf_rvwcmmnts` FOREIGN KEY (`trmsgrntnvrnmnt_rvwcmmnts_d`) REFERENCES `grantee_environmental_finding` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_sam_poc_address`
--
ALTER TABLE `trams_sam_poc_address`
  ADD CONSTRAINT `tramsentity_sampintfcontact` FOREIGN KEY (`tramsentity_smpintfcntct_id`) REFERENCES `trams_entity` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_scope`
--
ALTER TABLE `trams_scope`
  ADD CONSTRAINT `trams_scope_application_id` FOREIGN KEY (`application_id`) REFERENCES `trams_application` (`id`),
  ADD CONSTRAINT `trams_scope_project_id` FOREIGN KEY (`project_id`) REFERENCES `trams_project` (`id`);

--
-- Constraints for table `trams_scope_acc`
--
ALTER TABLE `trams_scope_acc`
  ADD CONSTRAINT `trams_scope_acc_ibfk_1` FOREIGN KEY (`reservation_id`) REFERENCES `trams_reservation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_section_5337_area_allocations`
--
ALTER TABLE `trams_section_5337_area_allocations`
  ADD CONSTRAINT `trmssctn5337rllctn_llctntyp` FOREIGN KEY (`allocationTypeId`) REFERENCES `trams_allocation_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_security`
--
ALTER TABLE `trams_security`
  ADD CONSTRAINT `tramsproject_security` FOREIGN KEY (`tramsproject_security_id`) REFERENCES `trams_project` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_acs_data`
--
ALTER TABLE `trams_state_acs_data`
  ADD CONSTRAINT `tramsstateacsdata_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_allocations`
--
ALTER TABLE `trams_state_allocations`
  ADD CONSTRAINT `tramsstateallocations_state` FOREIGN KEY (`tramsstate_state_stateid`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmsstteallctins_llctintype` FOREIGN KEY (`allocationTypeId`) REFERENCES `trams_allocation_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmsstteallctins_rbnizedare` FOREIGN KEY (`trmsrbnizedre_rbnizedre_zid`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_allocation_factor`
--
ALTER TABLE `trams_state_allocation_factor`
  ADD CONSTRAINT `tramsstteallctinfactr_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_appalachian_factor`
--
ALTER TABLE `trams_state_appalachian_factor`
  ADD CONSTRAINT `tramsstteappalchinfctr_stte` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_application_review`
--
ALTER TABLE `trams_state_application_review`
  ADD CONSTRAINT `trmsapplictin_stteappreview` FOREIGN KEY (`trmspplictin_sttppreview_id`) REFERENCES `trams_application` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_apportionment_year_population`
--
ALTER TABLE `trams_state_apportionment_year_population`
  ADD CONSTRAINT `trmssttpprtnmntyrppltin_stt` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_land_area`
--
ALTER TABLE `trams_state_land_area`
  ADD CONSTRAINT `tramsstatelandarea_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_ntd_summary`
--
ALTER TABLE `trams_state_ntd_summary`
  ADD CONSTRAINT `tramsstatentdsummary_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_state_population`
--
ALTER TABLE `trams_state_population`
  ADD CONSTRAINT `tramsstatepopulation_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_user_roles`
--
ALTER TABLE `trams_user_roles`
  ADD CONSTRAINT `FK_TRAMS_USERROLE_USERID` FOREIGN KEY (`system_user_id`) REFERENCES `FACS_SystemUsers` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_allocations`
--
ALTER TABLE `trams_uza_allocations`
  ADD CONSTRAINT `tramszallctins_allcatintype` FOREIGN KEY (`allocationTypeId`) REFERENCES `trams_allocation_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `tramszallctins_rbanizedarea` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_allocation_by_state`
--
ALTER TABLE `trams_uza_allocation_by_state`
  ADD CONSTRAINT `tramszallcatinbystate_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmszllctinbystt_llctintype` FOREIGN KEY (`allocationTypeId`) REFERENCES `trams_allocation_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmszllctinbystte_rbnizedre` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_census`
--
ALTER TABLE `trams_uza_census`
  ADD CONSTRAINT `tramsuzacenss_urbanizedarea` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_ntd_summary`
--
ALTER TABLE `trams_uza_ntd_summary`
  ADD CONSTRAINT `tramszntdsmmry_rbanizedarea` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_ntd_summary_by_state`
--
ALTER TABLE `trams_uza_ntd_summary_by_state`
  ADD CONSTRAINT `tramszntdsmmrybystate_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmszntdsmmrybystt_rbnizdre` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `trams_uza_population_by_state`
--
ALTER TABLE `trams_uza_population_by_state`
  ADD CONSTRAINT `tramszapplatinbystate_state` FOREIGN KEY (`state_id`) REFERENCES `trams_state` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `trmszppltinbystte_rbnizedre` FOREIGN KEY (`uza_id`) REFERENCES `trams_urbanized_area` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
