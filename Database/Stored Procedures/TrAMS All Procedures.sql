DELIMITER $$
CREATE PROCEDURE `sptramsGenerateMilestoneDetailStaticReport`(
    IN `annualReportFiscalYear` VARCHAR(20),
	IN `quarterlyReportFiscalYear` VARCHAR(20),
	IN `monthlyReportFiscalYear` VARCHAR(20),
    IN `reportFiscalQuarter` VARCHAR(20),
    IN `reportFiscalMonth` VARCHAR(20)
)
BEGIN
    DECLARE _spName varchar(255) default "sptramsGenerateMilestoneDetailStaticReport";

    SET @SID = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));

-- write procedure call to the log
    CALL `trams_log`(@SID, _spName, CONCAT(
        now(),
        ' CALL SP Inputs: ',
        IFNULL(`annualReportFiscalYear` ,'NULL'),',',
        IFNULL(`quarterlyReportFiscalYear` ,'NULL'),',',
        IFNULL(`monthlyReportFiscalYear` ,'NULL'),',',
		IFNULL(`reportFiscalQuarter` ,'NULL'),',',
		IFNULL(`reportFiscalMonth` ,'NULL'),','
    ), 1);



-- pulls the data from the view based on the passed in date parameters
SELECT
    mpr.RecipientCostCenter,
    mpr.RecipientId,
    mpr.RecipientLegalBusinessName,
    mpr.AppNumber,
    mpr.AppType,
    mpr.AppPostAwardManager,
    mpr.FundingSourceName,
    mpr.ReportPeriodType,
    mpr.ReportPeriodTypeConcat,
    mpr.ReportDueDate,
    mpr.ReportSubmittedDateTime,
    mpr.ReportStatus,
    mpr.LineItemNumber,
    mpr.LineItemActivityName,
    mpr.LineItemQuantity,
    mpr.LineItemRevisedFTAAmount,
    mpr.MilestoneTitle,
    mpr.MilestoneEstimatedCompletionDate,
    mpr.RemarkRevisedCompletionDate,
    mpr.RemarkActualCompletionDate,
    mpr.AppEndDate,
    mpr.EstimatedRevisedDateDelta,
    mpr.EstimatedActualDateDelta
FROM `vwtramsMilestoneProgressReportDetail` mpr
WHERE mpr.AwardActiveFlag = 1
	-- final MPRs
	AND (mpr.ReportFinalFlag = 1
		-- annual MPRs awards
		OR (mpr.ReportFiscalYear =  `annualReportFiscalYear` AND mpr.ReportPeriodType = "Annual")
		-- quarterly MPRs
		OR (mpr.ReportFiscalYear =  `quarterlyReportFiscalYear` AND mpr.ReportFiscalQuarter = `reportFiscalQuarter` AND mpr.ReportPeriodType = "Quarterly")
		-- monthly MPRs
		OR (mpr.ReportFiscalYear =  `monthlyReportFiscalYear` AND mpr.ReportFiscalMonth = `reportFiscalMonth` AND mpr.ReportPeriodType = "Monthly"));

-- write success to the log
CALL `trams_log`(@SID, _spName, CONCAT( now(), 'Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_delete_budget_revision`(IN p_application_id INT(11), IN p_budget_revision_number INT(11), IN p_budget_revision_id INT(11))
BEGIN

	/*
     Stored Procedure Name : trams_delete_budget_revision
     Description : Used during budget revision deletion.
     Updated by  : Garrison Dean (2020.Jan.23)
	*/

    -- Gather the changelogs from the previous budget revision. Use these to overwrite the current changes
	SET @uuid = UUID_SHORT();
	-- sid will be written to the new table as a time stamp to be used for identifying and cleaning up any junk data
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f')); 
	
	IF EXISTS( 
	    SELECT * FROM `trams_line_item` WHERE app_id = p_application_id AND revision_number = p_budget_revision_number ) 
	THEN
        
		INSERT INTO 
			trams_temp_project_env_findings_to_delete
		SELECT 
			NULL AS trams_temp_project_env_findings_to_delete_id,
			project_finding_id AS project_finding_id,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM 
			trams_project_env_finding_line_items 
		WHERE 
			line_item_id IN
				(SELECT id 
				 FROM trams_line_item 
				 WHERE app_id = p_application_id AND revision_number = p_budget_revision_number);

		-- DELETE project env finding line items
		DELETE FROM trams_project_env_finding_line_items 
			WHERE line_item_id IN(SELECT id FROM trams_line_item WHERE app_id = p_application_id AND revision_number = p_budget_revision_number);

		-- DELETE project env finding date		
		DELETE FROM trams_project_environmental_finding_date 
			WHERE project_finding_id IN(SELECT project_finding_id FROM trams_temp_project_env_findings_to_delete WHERE uuid = @uuid);

		-- DELETE project env finding
		DELETE FROM trams_project_environmental_finding 
			WHERE id IN(SELECT project_finding_id FROM trams_temp_project_env_findings_to_delete WHERE uuid = @uuid);

		-- DELETE milestones
		DELETE FROM trams_milestones 
			WHERE line_item_id IN(SELECT id FROM trams_line_item WHERE app_id = p_application_id AND revision_number = p_budget_revision_number);

		-- DELETE line item
		DELETE FROM trams_line_item WHERE app_id = p_application_id AND revision_number = p_budget_revision_number;
		
	END IF;
	
	-- NEW: Delete the application holding pot audit entries as well as the application holdding pot entries
	-- ApplicationHoldingPotAuditHistory
	DELETE FROM AUDIT USING tramsApplicationHoldingPotAuditHistory AS AUDIT
	JOIN tramsApplicationHoldingPot holding ON holding.ApplicationHoldingPotId = AUDIT.ApplicationHoldingPotId
	WHERE holding.BudgetRevisionId = p_budget_revision_id;
        
	-- ApplicationHoldingPot
	DELETE FROM tramsApplicationHoldingPot WHERE BudgetRevisionId = p_budget_revision_id;

	-- DELETE line item change log
	DELETE FROM trams_line_item_change_log WHERE budget_revision_id = p_budget_revision_id;
	
	-- DELETE review comments 
	DELETE FROM trams_review_comments WHERE budget_revision_id = p_budget_revision_id;
	
	-- DELETE tramsPOPChangelog entry
	DELETE FROM tramsPOPChangelog WHERE BudgetRevisionId = p_budget_revision_id;
	
	-- DELETE budget revision 
	DELETE FROM trams_budget_revision WHERE id = p_budget_revision_id;	

	-- mark previous budget revision as the latest
	UPDATE trams_budget_revision SET latest_revision_flag = 1
	WHERE application_id = p_application_id AND revision_number = p_budget_revision_number - 1;
	
	-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
	DELETE FROM trams_temp_project_env_findings_to_delete WHERE uuid = @uuid;	
 
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsAmendmentErrorLogCleanOut`(
	IN i_CrDtFrom DATETIME,
	IN i_CrDtTo DATETIME)
BEGIN

DECLARE _CallingObject VARCHAR(255) DEFAULT "TRAMS CREATE AMENDMENT";

DELETE FROM tramsAmendmentErrorLog WHERE CallingObject = _CallingObject AND CreatedDate BETWEEN i_CrDtFrom AND i_CrDtTo;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_create_auto_disbursement_transaction`()
    NO SQL
BEGIN
  DECLARE _done INT;
  DECLARE _max_idx INT;
  DECLARE _contract_award_id INT;
  DECLARE _id INT;
  DECLARE curs CURSOR FOR  SELECT id, contract_award_id FROM tmp_contract_acc_trans;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done = 1;
  
INSERT INTO `trams_contract_acc_transaction`( `contract_award_id`, `recipient_id`, `project_id`, `application_id`, `application_number`, `project_number`, `echo_number`, `transaction_fiscal_year`, `uza_code`, `scope_code`, `scope_name`, `account_class_code`, `fpc`, `cost_center_code`, `funding_source_name`, `funding_fiscal_year`, `type_authority`, `transaction_type`, 
`appropriation_code`, `section_code`, `limitation_code`, 
`transaction_date_time`, 
`transaction_by`, `transaction_doc_num`, `trams_entry_date_time`, `echo_payment_identifier`, `dafis_suffix_code`, `trmscntrctw_cntrctcctrn_dx`, 
`obligation_amount`, 
`deobligation_amount`, `disbursement_amount`, `refund_amount`, 
`authorized_disbursement_amount`, 
`run_number`, `transaction_code`, `transaction_status_code`, `app_amendment_number`, `fpc_description`, `po_number`, `from_trams`, `scope_suffix`, `tbp_process_date`) 
SELECT 
`contract_award_id`, `recipient_id`, `project_id`, `application_id`, `application_number`, `project_number`, `echo_number`, `transaction_fiscal_year`, `uza_code`, `scope_code`, `scope_name`, `account_class_code`, `fpc`, `cost_center_code`, `funding_source_name`, `funding_fiscal_year`, `type_authority`, 
'Auth Disbursement', 
`appropriation_code`, `section_code`, `limitation_code`, 
date_add(`transaction_date_time`, interval 1 second), 
`transaction_by`, `transaction_doc_num`, `trams_entry_date_time`, `echo_payment_identifier`, `dafis_suffix_code`, 
`trmscntrctw_cntrctcctrn_dx`,
0, `deobligation_amount`, `disbursement_amount`, `refund_amount`, 
`obligation_amount`, 
`run_number`, `transaction_code`, `transaction_status_code`, `app_amendment_number`, `fpc_description`, `po_number`, `from_trams`, `scope_suffix`, `tbp_process_date`
FROM `trams_contract_acc_transaction`
where `trams_contract_acc_transaction`.`transaction_date_time` BETWEEN '2015-02-01 00:00:00' and now()
and transaction_type='Obligation';

drop table if exists tmp_contract_acc_trans;

create table tmp_contract_acc_trans engine=memory 
SELECT id, contract_award_id FROM trams_contract_acc_transaction WHERE `trams_contract_acc_transaction`.`transaction_date_time` BETWEEN '2015-02-01 00:00:00' and now() and transaction_type ='Auth Disbursement';


OPEN curs;
  SET _done = 0;
  REPEAT
    FETCH curs INTO _id, _contract_award_id;
  
    set _max_idx = (select max(trans1.trmscntrctw_cntrctcctrn_dx) from trams_contract_acc_transaction trans1 where contract_award_id=_contract_award_id);
  
    UPDATE trams_contract_acc_transaction  
  set trmscntrctw_cntrctcctrn_dx = _max_idx+1
  where id=_id;

  UNTIL _done END REPEAT;
  
  CLOSE curs;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_announced_apportionment_rollup`(IN `p_uza_code` VARCHAR(10), IN `p_funding_fiscal_year` VARCHAR(10), IN `p_appropriation_code` VARCHAR(10), IN `p_section_id` VARCHAR(10), IN `p_limitation_code` VARCHAR(10))
BEGIN

drop  temporary table if exists temp_apportionment_rollup;
create temporary table temp_apportionment_rollup engine=memory

select  `a`.`uza_code` AS `uza_code`, 
    `a`.`funding_fiscal_year` AS `funding_fiscal_year`, 
    `a`.`appropriation_code` AS `appropriation_code`, 
    `a`.`section_id` AS `section_id`, 
    `a`.`limitation_code` AS `limitation_code`,
    `a`.`lapse_period` AS `lapse_period`, 
    a.active_flag AS active_flag,
    Ifnull(Sum(`a`.`apportionment`), 0) AS `apportionment`,  
    Ifnull(Sum(`a`.`operating_cap`), 0) AS operating_cap,
        Ifnull(Sum(`a`.`ceiling_amount`), 0) AS ceiling_amount
from    trams_apportionment a where `a`.`funding_fiscal_year` = p_funding_fiscal_year and `a`.`uza_code`= p_uza_code 
group by uza_code, funding_fiscal_year, appropriation_code,section_id, limitation_code;

alter table temp_apportionment_rollup add index `apportionment_rollup_idx` (`uza_code`,`funding_fiscal_year`,`appropriation_code`,`section_id`,`limitation_code`);
    

SELECT  Uuid_short() AS `id`, 
      `a`.`uza_code` AS `uzaCode_text`, 
    `a`.`funding_fiscal_year` AS `fundingFiscalYear_text`, 
    `a`.`appropriation_code` AS `appropriationCode_text`, 
    `a`.`section_id` AS `sectionId_text`, 
    `a`.`limitation_code` AS `limitationCode_text`, 
    Concat(`a`.`funding_fiscal_year`, '.', `a`.`appropriation_code`, '.', `a`.`section_id`, '.', `a`.`limitation_code`) AS `accountClassCode_text`, 
        `ur`.`uza_name` AS `urbanizedAreaName_text`, 
    `a`.`lapse_period` AS `lapseYear_text`, 
    Ifnull(`a`.`apportionment`, 0) AS `apportionment_initialAuthority_dec`, 
    Ifnull(Sum(`ah`.`modified_amount`), 0) AS `apportionment_modification_dec`, 
    Ifnull(Sum(`ah`.`transfer_in_amount`), 0) AS `apportionment_transferIn_dec`, 
    Ifnull(Sum(`ah`.`transfer_out_amount`), 0) AS `apportionment_transferOut_dec`, 
    ( ( ( Ifnull(`a`.`apportionment`, 0) + Ifnull(Sum(`ah`.`modified_amount`), 0) ) + 
        Ifnull(Sum(`ah`.`transfer_in_amount`), 0) ) - Ifnull(Sum(`ah`.`transfer_out_amount`), 0) ) AS `apportionment_effectiveAuthority_dec`, 
    Ifnull(Sum(`ah`.`reserved_amount`), 0) AS `apportionment_reserved_dec`, 
    Ifnull(Sum(`ah`.`unreserve_amount`), 0) AS `apportionment_unreserved_dec`, 
    ( Sum(`ah`.`reserved_amount`) - Sum(`ah`.`unreserve_amount`) ) AS `apportionment_reservation_dec`, 
    Ifnull(Sum(`ah`.`obligated_amount`), 0) AS `apportionment_obligation_dec`, 
    Ifnull(Sum(`ah`.`recovery_amount`), 0) AS `apportionment_recovery_dec`, 
( ( ( ( ( ( ( Ifnull(`a`.`apportionment`, 0) 
              + Ifnull(Sum(`ah`.`modified_amount`), 0) ) + 
                      Ifnull(Sum(`ah`.`transfer_in_amount`), 0) ) + 
                  Ifnull(Sum(`ah`.`unreserve_amount`), 0) ) + 
              Ifnull(Sum(`ah`.`recovery_amount`), 0) ) - 
          Ifnull(Sum(`ah`.`reserved_amount`), 0) ) - 
      Ifnull(Sum(`ah`.`obligated_amount`), 0) ) - 
  Ifnull(Sum(`ah`.`transfer_out_amount`), 0) ) 
       AS `apportionment_availableBalance_dec`, 
Ifnull(`a`.`operating_cap`, 0) 
       AS `cap_initialAuthority_dec`, 
Ifnull(Sum(`ah`.`cap_modify_amt`), 0) 
       AS `cap_modification_dec`, 
Ifnull(Sum(`ah`.`cap_transfer_in_amt`), 0) 
       AS `cap_transferIn_dec`, 
Ifnull(Sum(`ah`.`cap_transfer_out_amt`), 0) 
       AS `cap_transferOut_dec`, 
( ( ( Ifnull(`a`.`operating_cap`, 0) 
      + Ifnull(Sum(`ah`.`cap_modify_amt`), 0) ) + 
      Ifnull(Sum(`ah`.`cap_transfer_in_amt`), 0) ) - 
  Ifnull(Sum(`ah`.`cap_transfer_out_amt`), 0) ) 
       AS `cap_effectiveAuthority_dec`, 
( Ifnull(Sum(`ah`.`cap_reserved_amt`), 0) - 
  Ifnull(Sum(`ah`.`cap_unreserved_amt`), 0) ) 
       AS `cap_reservation_dec`, 
Ifnull(Sum(`ah`.`cap_obligated_amt`), 0) 
       AS `cap_obligation_dec`, 
Ifnull(Sum(`ah`.`cap_recovery_amt`), 0) 
       AS `cap_recovery_dec`, 
( ( ( ( ( ( ( Ifnull(`a`.`operating_cap`, 0) 
              + Ifnull(Sum(`ah`.`cap_modify_amt`), 0) ) + 
                      Ifnull(Sum(`ah`.`cap_transfer_in_amt`), 0) ) + 
                  Ifnull(Sum(`ah`.`cap_unreserved_amt`), 0) ) + 
              Ifnull(Sum(`ah`.`cap_recovery_amt`), 0) ) - 
          Ifnull(Sum(`ah`.`cap_reserved_amt`), 0) ) - 
      Ifnull(Sum(`ah`.`cap_obligated_amt`), 0) ) - 
  Ifnull(Sum(`ah`.`cap_transfer_out_amt`), 0) ) 
       AS `cap_availableBalance_dec`, 
Ifnull(`a`.`ceiling_amount`, 0) 
       AS `ceiling_initialAuthority_dec`, 
Ifnull(Sum(`ah`.`ceiling_modify_amt`), 0) 
       AS `ceiling_modification_dec`, 
Ifnull(Sum(`ah`.`ceiling_transfer_in_amt`), 0) 
       AS `ceiling_transferIn_dec`, 
Ifnull(Sum(`ah`.`ceiling_transfer_out_amt`), 0) 
       AS `ceiling_transferOut_dec`, 
( ( ( Ifnull(`a`.`ceiling_amount`, 0) 
      + Ifnull(Sum(`ah`.`ceiling_modify_amt`), 0) ) + 
      Ifnull(Sum(`ah`.`ceiling_transfer_in_amt`), 0) ) - 
  Ifnull(Sum(`ah`.`ceiling_transfer_out_amt`), 0) ) 
       AS 
`ceiling_effectiveAuthority_dec`, 
Ifnull(Sum(`ah`.`ceiling_recovery_amt`), 0) 
       AS `ceiling_recovery_dec`, 
( ( ( ( ( ( ( Ifnull(`a`.`ceiling_amount`, 0) 
              + Ifnull(Sum(`ah`.`ceiling_modify_amt`), 0) ) + 
                      Ifnull(Sum(`ah`.`ceiling_transfer_in_amt`), 0) ) + 
                  Ifnull(Sum(`ah`.`ceiling_unreserve_amt`), 0) ) + 
              Ifnull(Sum(`ah`.`ceiling_recovery_amt`), 0) ) - 
          Ifnull(Sum(`ah`.`ceiling_reserved_amt`), 0) ) - 
      Ifnull(Sum(`ah`.`ceiling_obligated_amt`), 0) ) - 
  Ifnull(Sum(`ah`.`ceiling_transfer_out_amt`), 0) ) 
       AS 
`ceiling_availableBalance_dec` 
FROM   ((`temp_apportionment_rollup` `a` 
         JOIN `trams_uza_code_reference` `ur` 
                ON(( `a`.`uza_code` = `ur`.`uza_code` ))) 
        LEFT JOIN `trams_apportionment_history` `ah` 
               ON(( ( `a`.`uza_code` = `ah`.`uza_code` ) 
                    AND ( `a`.`funding_fiscal_year` = 
                        `ah`.`funding_fiscal_year` ) 
                    AND ( `a`.`section_id` = `ah`.`section_id` ) 
                    AND ( `a`.`appropriation_code` = `ah`.`appropriation_code` ) 
                    AND ( `a`.`limitation_code` = `ah`.`limitation_code` ) ))) 
WHERE `a`.`active_flag` = 1  and `a`.`uza_code` = p_uza_code and `a`.`appropriation_code` = p_appropriation_code and 
      `a`.`section_id` = p_section_id and `a`.`limitation_code` = p_limitation_code and `a`.`funding_fiscal_year` = p_funding_fiscal_year 
GROUP  BY `ah`.`funding_fiscal_year`, 
          `ah`.`section_id`, 
          `ah`.`limitation_code`, 
          `ah`.`appropriation_code`, 
          `ah`.`uza_code`, 
          `a`.`lapse_period` 
ORDER  BY `ah`.`funding_fiscal_year`   ;
          
drop  temporary table if exists temp_apportionment_rollup;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_apportionment_load_excel_report`(IN `p_fiscalYear` varchar(4))
BEGIN

/*
 * Date Created: 2016-03-01
 * Date Updated: 2016-03-03
 * Stored procedure to retrieve data for the apportionment load excel report
 */

  SET @uuid = UUID_SHORT();
  SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  
  /*********************************************************************************
   * Create a temporary table for the formula program - account class code mapping *
   *********************************************************************************/
  CALL `trams_log`(@sid, 'trams_get_apportionment_load_excel_report', 'Build temp table for Formula Program - Account Class Code mapping', 1);
  
  /* Changed previous temp table creation statement to insert into the new table */
  INSERT INTO `trams_temp_formula_program_account_class_code_mapping` (`formula_program`, `cost_center_code`, `funding_fiscal_year`, `appropriation_code`, `section_code`, `limitation_code`, `authority_type`, `uuid`, `time_stamp`) VALUES
  ('5303/5304',  '71000', p_fiscalYear, '25', '08', '8F', '2', @uuid, @sid),
  ('5303/5304',  '71000', p_fiscalYear, '25', '26', 'A2', '2', @uuid, @sid),
  ('5307',       '65000', p_fiscalYear, '25', '90', '91', '2', @uuid, @sid),
  ('5310',       '65000', p_fiscalYear, '25', '16', 'DL', '2', @uuid, @sid),
  ('5310',       '65000', p_fiscalYear, '25', '16', 'DM', '2', @uuid, @sid),
  ('5310',       '65000', p_fiscalYear, '25', '16', 'DS', '2', @uuid, @sid),
  ('5311 RTAP',  '65000', p_fiscalYear, '25', '18', '81', '2', @uuid, @sid),
  ('5311 RTAP',  '65000', p_fiscalYear, '25', '18', 'AF', '2', @uuid, @sid),
  ('5311 RTAP',  '65000', p_fiscalYear, '25', '18', 'R7', '2', @uuid, @sid),
  ('5311 Triba', '65000', p_fiscalYear, '25', '18', 'TF', '2', @uuid, @sid),
  ('5329',       '65000', p_fiscalYear, '25', '74', 'US', '2', @uuid, @sid),
  ('5337',       '65000', p_fiscalYear, '25', '54', '38', '2', @uuid, @sid),
  ('5337',       '65000', p_fiscalYear, '25', '54', 'GR', '2', @uuid, @sid),
  ('5339',       '65000', p_fiscalYear, '25', '34', '31', '2', @uuid, @sid),
  ('5339',       '65000', p_fiscalYear, '25', '34', 'BF', '2', @uuid, @sid);
  
  /****************************************************************************************
   * Create a temporary table containing all apportionments for the specified fiscal year *
   ****************************************************************************************/
  CALL `trams_log`(@sid, 'trams_get_apportionment_load_excel_report', 'Build temp table for all apportionments', 1);
 
 
 /* Changed previous temp table creation statement to insert into the new table */
  INSERT INTO `trams_temp_all_fiscal_year_apportionment` (`formula_program`, `uza_code`, `account_class_code`, `fiscal_year`, `funding_fiscal_year`, `lapse_year`, `apportionment`, `operating_cap`, `ceiling`, `uuid`, `time_stamp`)
  SELECT t.formula_program,
         a.uza_code,
         concat(
           a.funding_fiscal_year, '.',
           a.appropriation_code, '.',
           a.section_id, '.',
           a.limitation_code
         ) as account_class_code,
         a.funding_fiscal_year as fiscal_year,
         a.funding_fiscal_year,
         a.lapse_period as lapse_year,
         a.apportionment,
         a.operating_cap,
         a.ceiling_amount as `ceiling`,
		 @uuid AS uuid,
		 @sid AS time_stamp 
  FROM `trams_apportionment` as a
  /* Replaced reference to temp table with call to new table and added uuid condition */
  INNER JOIN `trams_temp_formula_program_account_class_code_mapping` AS t ON t.funding_fiscal_year = a.funding_fiscal_year
    AND t.appropriation_code = a.appropriation_code
    AND t.section_code = a.section_id
    AND t.limitation_code = a.limitation_code
	AND t.uuid = @uuid
  WHERE a.active_flag = 1;
  
  /*************************************************************************
   * Query the result sets for each table in the apportionment load report *
   *************************************************************************/
  CALL `trams_log`(@sid, 'trams_get_apportionment_load_excel_report', concat('Begin generating report for fiscal year ', p_fiscalYear), 1);

  /* Result Set 1 - Apportionment history rollup (before load) */

  SELECT t.formula_program,
         count(DISTINCT h.uza_code) as uza_count,
         concat(
           t.funding_fiscal_year, '.',
           t.appropriation_code, '.',
           t.section_code, '.',
           t.limitation_code
         ) as account_class_code,
         h.lapse_year,
         ifnull(sum(h.apportionment), 0) as apportionment,
         ifnull(sum(h.operating_cap), 0) as operating_cap,
         ifnull(sum(h.ceiling), 0) as `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_formula_program_account_class_code_mapping` AS t
  LEFT JOIN (
    SELECT a.uza_code,
           a.appropriation_code,
           a.section_id as section_code,
           a.limitation_code,
           a.funding_fiscal_year,
           a.lapse_period as lapse_year,
           h.amount as apportionment,
           h.cap_amount as operating_cap,
           h.ceiling_amount as `ceiling`
    FROM `trams_apportionment` as a
    INNER JOIN `trams_apportionment_history` as h ON h.apportionment_id = a.id
    WHERE a.active_flag = 0
  ) as h
  ON h.funding_fiscal_year = t.funding_fiscal_year
    AND h.appropriation_code = t.appropriation_code
    AND h.section_code = t.section_code
    AND h.limitation_code = t.limitation_code
  WHERE t.uuid = @uuid
  GROUP BY account_class_code
  ORDER BY formula_program ASC, account_class_code ASC;

  /* Result Set 2 - Operating budget account class code rollup */
  
  SELECT t.formula_program,
         concat(
           t.funding_fiscal_year, '.',
           t.appropriation_code, '.',
           t.section_code, '.',
           t.limitation_code, '.',
           t.authority_type
         ) as account_class_code,
         t.funding_fiscal_year as fiscal_year,
         t.cost_center_code,
         'F' as formula_flag,
         if(a.authorized_flag = 1, 'A', 'NA') as authorized_flag,
         ifnull(sum(a.code_amount), 0) as code_amount
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_formula_program_account_class_code_mapping` AS t
  LEFT JOIN
  (
    SELECT acc_funding_fiscal_year as funding_fiscal_year,
           acc_appropriation_code as appropriation_code,
           acc_section_code as section_code,
           acc_limitation_code as limitation_code,
           code_new_amount as code_amount,
           authorized_flag
    FROM `trams_account_class_code`
    WHERE authorized_flag = 1
  ) as a
  ON a.funding_fiscal_year = t.funding_fiscal_year
    AND a.appropriation_code = t.appropriation_code
    AND a.section_code = t.section_code
    AND a.limitation_code = t.limitation_code
  WHERE t.uuid = @uuid
  GROUP BY account_class_code
  ORDER BY formula_program ASC, account_class_code ASC;
  
  /* Result Set 3 - Apportionment history rollup (after load) */

  SELECT t.formula_program,
         count(DISTINCT h.uza_code) as uza_count,
         concat(
           t.funding_fiscal_year, '.',
           t.appropriation_code, '.',
           t.section_code, '.',
           t.limitation_code
         ) as account_class_code,
         h.lapse_year,
         ifnull(sum(h.apportionment), 0) as apportionment,
         ifnull(sum(h.operating_cap), 0) as operating_cap,
         ifnull(sum(h.ceiling), 0) as `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_formula_program_account_class_code_mapping` as t
  LEFT JOIN (
    SELECT a.uza_code,
           a.appropriation_code,
           a.section_id as section_code,
           a.limitation_code,
           a.funding_fiscal_year,
           a.lapse_period as lapse_year,
           h.amount as apportionment,
           h.cap_amount as operating_cap,
           h.ceiling_amount as `ceiling`
    FROM `trams_apportionment` as a
    INNER JOIN `trams_apportionment_history` as h ON h.funding_fiscal_year = a.funding_fiscal_year
      AND h.appropriation_code = a.appropriation_code
      AND h.section_id = a.section_id
      AND h.limitation_code = a.limitation_code
      AND h.uza_code = a.uza_code
    WHERE a.active_flag = 1
  ) as h
  ON h.funding_fiscal_year = t.funding_fiscal_year
    AND h.appropriation_code = t.appropriation_code
    AND h.section_code = t.section_code
    AND h.limitation_code = t.limitation_code
  WHERE t.uuid = @uuid
  GROUP BY account_class_code
  ORDER BY formula_program ASC, account_class_code ASC;

  /* Result Set 4 - Apportionments for all formula programs */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE uuid = @uuid
  ORDER BY formula_program ASC, account_class_code ASC, uza_code ASC;

  /* Result Set 5 - Apportionments for formula program 5303/5304 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5303/5304'
	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 6 - Apportionments for formula program 5307 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5307'
	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 7 - Apportionments for formula program 5310 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5310'
	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 8 - Apportionments for formula program 5311 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program IN ('5311 RTAP', '5311 Triba')
  	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 9 - Apportionments for formula program 5329 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5329'
	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 10 - Apportionments for formula program 5337 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5337'
  	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  /* Result Set 11 - Apportionments for formula program 5339 */

  SELECT formula_program,
         uza_code,
         account_class_code,
         fiscal_year,
         funding_fiscal_year,
         lapse_year,
         apportionment,
         operating_cap,
         `ceiling`
  /* Replaced reference to temp table with call to new table and added uuid condition */
  FROM `trams_temp_all_fiscal_year_apportionment`
  WHERE formula_program = '5339'
  	AND uuid = @uuid
  ORDER BY account_class_code ASC, uza_code ASC;

  DELETE FROM trams_temp_formula_program_account_class_code_mapping WHERE uuid = @uuid;
  DELETE FROM trams_temp_all_fiscal_year_apportionment WHERE uuid = @uuid;
  
 
  CALL `trams_log`(@sid, 'trams_get_apportionment_load_excel_report', 'Return', 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_FMS_compare_data`(
  IN p_fiscalYear VARCHAR(50),
  IN p_beginDate  VARCHAR(50),
  IN p_endDate    VARCHAR(50)
)
    NO SQL
BEGIN

/*
   Stored Procedure Name : trams_get_FMS_compare_data
   Description : Create TrAMS compare data send to FMS
   Created by  : Nathan Park (2014.Nov.14)
   Update By   : Nathan Park (2015.Feb.11)
   
    --Legacy Table layout --------------
   
    run_no        VARCHAR(3),  
  doc_id_type     varchar(2),  
  doc_id_numb     varchar(13), 
  doc_id_ffy      varchar(2),  
  doc_id_suffix   varchar(3),  
  obj_class     varchar(4),  
  approp_code     varchar(4),  
  approp_code_lim   varchar(3),  
  cost_cntr     varchar(6),  
  org_code      varchar(3),  
  fpc         varchar(2),  
  prog_elem     varchar(4),  
  public_gov      char(1),   
  echono        varchar(8),  
  vendor_ssn      varchar(9),  
  reserv_date     date,    
  first_ob_date   date,    
  last_ob_date    date,    
  last_disb_date    date,    
  close_date      date,    
  cm0_obligation    money,     
  cm0_deobs     money,     
  cm0_payment     money,     
  cm0_refunds     money,     
  cm0_auth_disb   money,     
  py0_obligation    money,     
  py0_deobs     money,     
  py0_payment     money,     
  py0_refunds     money,     
  py0_auth_disb   money)     
  ------------------------------------
*/


    declare _sql varchar(40960) default "";
    declare _whereClause varchar(4096) default " ";
    
    set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

    CALL `trams_log`(@sid, 'trams_get_FMS_compare_date', 'trams_get_FMS_compare_date', 1);
    
  

    
    
  /* prior year data */
  drop temporary table if exists tmp_table_prior_FMS_compare;
  create temporary table tmp_table_prior_FMS_compare engine=memory
  select application_id,project_id,cost_center_code, account_class_code, fpc, scope_code,
  ifnull(sum(obligation_amount),0) as total_obligation_amt,
  ifnull(sum(deobligation_amount),0) as total_deobligation_amt,
  ifnull(sum(disbursement_amount),0) as total_disbursement_amt,
  ifnull(sum(refund_amount),0) as total_refund_amt,
  ifnull(sum(authorized_disbursement_amount),0) as total_authorized_disbursement_amt
  from trams_contract_acc_transaction
  where transaction_date_time between DATE_SUB(DATE_FORMAT(p_beginDate,'%Y-%m-%d'),INTERVAL 1 YEAR) and DATE_SUB(DATE_FORMAT(p_endDate,'%Y-%m-%d'),INTERVAL 1 YEAR)
  group by application_id,project_id,cost_center_code, account_class_code, fpc, scope_code;
    
  create index tmp_table_prior_FMS_compare_idx on tmp_table_prior_FMS_compare(application_id,project_id,cost_center_code, account_class_code, fpc, scope_code); 

    CALL `trams_log`(@sid, 'trams_get_FMS_compare_date', 'tmp_table_prior_FMS_compare', 1);
  
  /* current year data */
  drop temporary table if exists tmp_table_current_FMS_compare;
  create temporary table tmp_table_current_FMS_compare engine=memory
  select application_id, project_id,cost_center_code, account_class_code, fpc, scope_code,
  ifnull(sum(obligation_amount),0) as total_obligation_amt,
  ifnull(sum(deobligation_amount),0) as total_deobligation_amt,
  ifnull(sum(disbursement_amount),0) as total_disbursement_amt,
  ifnull(sum(refund_amount),0) as total_refund_amt,
  ifnull(sum(authorized_disbursement_amount),0) as total_authorized_disbursement_amt
  from trams_contract_acc_transaction
  where transaction_date_time between DATE_FORMAT(p_beginDate,'%Y-%m-%d') and DATE_FORMAT(p_endDate,'%Y-%m-%d')
  group by application_id, project_id,cost_center_code, account_class_code, fpc, scope_code;

  create index tmp_table_current_FMS_compare_idx on tmp_table_current_FMS_compare(application_id,project_id,cost_center_code, account_class_code, fpc, scope_code); 
  
  
    CALL `trams_log`(@sid, 'trams_get_FMS_compare_date', 'tmp_table_current_FMS_compare', 1);
  
  
  drop temporary table if exists tmp_table_final_FMS_compare;
  create temporary table tmp_table_final_FMS_compare engine=memory
  SELECT a.application_id,a.project_id,a.account_class_code,a.cost_center_code,a.fpc,a.scope_code,
  (ifnull(a.total_obligation_amt,0) + ifnull(b.total_obligation_amt,0)) as cm0_obligation,
  (ifnull(a.total_deobligation_amt,0) + ifnull(b.total_deobligation_amt,0)) as cm0_deobs,
  (ifnull(a.total_disbursement_amt,0) + ifnull(b.total_disbursement_amt,0)) as cm0_payment,
  (ifnull(a.total_refund_amt,0) + ifnull(b.total_refund_amt,0)) as cm0_refunds,
  (ifnull(a.total_authorized_disbursement_amt,0) + ifnull(b.total_authorized_disbursement_amt,0)) as cm0_auth_disb,
  ifnull(b.total_obligation_amt,0) as py0_obligation,
  ifnull(b.total_deobligation_amt,0) as py0_deobs,
  ifnull(b.total_disbursement_amt,0) as py0_payment,
  ifnull(b.total_refund_amt,0) as py0_refunds,
  ifnull(b.total_authorized_disbursement_amt,0) as py0_auth_disb 
  FROM `tmp_table_prior_FMS_compare` a left join tmp_table_current_FMS_compare b
  on a.application_id = b.application_id
  and a.project_id = b.project_id
  and a.cost_center_code = b.cost_center_code
  and a.account_class_code = b.account_class_code
  and a.fpc = b.fpc
  and a.scope_code = b.scope_code;
  
  insert into tmp_table_final_FMS_compare
  SELECT a.application_id,a.project_id,a.account_class_code,a.cost_center_code,a.fpc,a.scope_code,
  (ifnull(a.total_obligation_amt,0) + ifnull(b.total_obligation_amt,0)) as cm0_obligation,
  (ifnull(a.total_deobligation_amt,0) + ifnull(b.total_deobligation_amt,0)) as cm0_deobs,
  (ifnull(a.total_disbursement_amt,0) + ifnull(b.total_disbursement_amt,0)) as cm0_payment,
  (ifnull(a.total_refund_amt,0) + ifnull(b.total_refund_amt,0)) as cm0_refunds,
  (ifnull(a.total_authorized_disbursement_amt,0) + ifnull(b.total_authorized_disbursement_amt,0)) as cm0_auth_disb,
  ifnull(b.total_obligation_amt,0) as py0_obligation,
  ifnull(b.total_deobligation_amt,0) as py0_deobs,
  ifnull(b.total_disbursement_amt,0) as py0_payment,
  ifnull(b.total_refund_amt,0) as py0_refunds,
  ifnull(b.total_authorized_disbursement_amt,0) as py0_auth_disb 
  FROM tmp_table_current_FMS_compare a left join tmp_table_prior_FMS_compare b
  on a.application_id = b.application_id
  and a.project_id = b.project_id
  and a.cost_center_code = b.cost_center_code
  and a.account_class_code = b.account_class_code
  and a.fpc = b.fpc
  and a.scope_code = b.scope_code ;
    
  create index tmp_table_final_FMS_compare_idx on tmp_table_final_FMS_compare(application_id,project_id,account_class_code,cost_center_code,fpc,scope_code);  
  

  CALL `trams_log`(@sid, 'trams_get_FMS_compare_date', 'tmp_table_final_FMS_compare', 1);
  
  SELECT distinct '000' as run_no,
       '26'  as doc_id_type,
       replace(ca.po_number,'-','') as doc_id_num,
       right(a.fiscal_year,2) as doc_id_ffy, 
       concat(right(ca.fpc,1),'00',ca.dafis_suffix_code) as doc_id_suffix,
       right(ca.fpc,1) as obj_clas,          /* Possible values: 
      Fixed text ("411") + FPC (1 character) -- for xxx reason, 
      Fixed text ("2596") -- for vv reason, 
      Fixed text ("0000") -- for zz reason, 
      Fixed text ("410") + FPC (1 character) -- for yyy reason */
       ca.appropriation_code as approp_code, 
       concat(ca.type_authority,right(funding_fiscal_year,2)) as approp_code_lim,
       ca.cost_center_code as cost_cntr,
       gb.cost_center_code as org_code,
       ca.fpc as fpc, 
       concat(ca.section_code,ca.limitation_code) as prog_elem,
       case when LOCATE('Public',gb.organization_type_code)>1 then 'P' else 'G' end as public_gov,  
       concat(ca.echo_number,ca.echo_payment_identifier) as echono, 
       ca.recipient_id as vendor_ssn,
       DATE_FORMAT(re.reservation_finalized_date_time, "%m%d%y")  as reserv_date,
       DATE_FORMAT(ad.obligated_date_time, "%m%d%y") as first_ob_date,
       (select distinct DATE_FORMAT(ca.transaction_date_time, "%m%d%y") from trams_contract_acc_transaction 
       where ca.recipient_id = recipient_id and transaction_type = 'Obligation' )
       as last_ob_date,    /* latest for transaction_type_text="Obligation" */
       (select distinct DATE_FORMAT(ca.transaction_date_time, "%m%d%y") from trams_contract_acc_transaction 
       where ca.recipient_id = recipient_id and transaction_type = 'Disbursement' )
       as last_disb_date,   /* latest for transaction_type_text="Disbursement" */
       DATE_FORMAT(ad.fta_closeout_approval_date_time, "%m%d%y") as close_date,
       c.cm0_obligation as cm0_obligation,    /* sum */
       c.cm0_deobs as cm0_deobs,
       c.cm0_payment as cm0_payment,
       c.cm0_refunds as cm0_refunds,
       c.cm0_auth_disb as cm0_auth_disb,
       c.py0_obligation as py0_obligation,
       c.py0_deobs as py0_deobs,
       c.py0_payment as py0_payment,
       c.py0_refunds as py0_refunds,
       c.py0_auth_disb as py0_auth_disb,
       'Y' as from_trams,
       replace(ca.application_number,'-','') as application_fain,        /* remove - */
       replace(ca.project_number,'-','') as project_identifier,          /* remove - */
       left(replace(ca.scope_code,'-',''),4) as scope_code,
       ca.scope_suffix as scope_suffix
  FROM `trams_contract_acc_transaction` ca left join trams_application a on ca.application_id = a.id 
  left join trams_general_business_info gb on gb.recipient_id = ca.recipient_id
  left join trams_reservation re on re.application_id = ca.application_id
  left join trams_application_award_detail ad on ad.application_id = a.id 
  left join tmp_table_final_FMS_compare c on ca.application_id = c.application_id
  and ca.project_id = c.project_id
  and ca.cost_center_code = c.cost_center_code
  and ca.account_class_code = c.account_class_code
  and ca.fpc = c.fpc
  and ca.scope_code = c.scope_code 
  where ca.transaction_date_time between DATE_FORMAT(p_beginDate,'%Y-%m-%d') and DATE_FORMAT(p_endDate,'%Y-%m-%d') ;
  
  CALL `trams_log`(@sid, 'trams_get_FMS_compare_date', 'done', 1);
  
  drop temporary table if exists tmp_table_prior_FMS_compare;
  drop temporary table if exists tmp_table_current_FMS_compare;
  drop temporary table if exists tmp_table_final_FMS_compare;


END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsGenerateObligationsDeobligationsByFiscalYearReport`(
    IN `p_transactionFiscalYear` VARCHAR(20),
	IN `p_recipientId` VARCHAR(255),
	IN `p_recipientCostCenter` VARCHAR(255),
	IN `p_appId` VARCHAR(255),
    IN `p_appType` VARCHAR(255),
    IN `p_projectId` VARCHAR(255),
    IN `p_poNumber` VARCHAR(255),
    IN `p_scopeCode` VARCHAR(255),
    IN `p_fundingUzaCode` VARCHAR(255),
    IN `p_accFundingFiscalYear` VARCHAR(255),
    IN `p_accAppropriationCode` VARCHAR(255),
    IN `p_accSectionCode` VARCHAR(255),
    IN `p_accLimitationCode` VARCHAR(255),
    IN `p_accTypeAuthority` VARCHAR(255),
    IN `p_startDate` VARCHAR(40),
    IN `p_endDate` VARCHAR(40),
    IN `p_transactionType` VARCHAR(255),
    IN `p_transactionAppType` VARCHAR(255),
    IN `p_currentYearDeobligation` VARCHAR(20),
    IN `p_discretionary` INT,
    IN `p_appStatus` VARCHAR(2048),
    IN `p_gmtOffset` VARCHAR(10)
)
BEGIN
    DECLARE _spName varchar(255) default "sptramsGenerateObligationsDeobligationsByFiscalYearReport";
	DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _tramsObScopeClause VARCHAR(4096) DEFAULT " ";
	DECLARE _tramsDeobScopeClause VARCHAR(4096) DEFAULT " ";
	DECLARE _obligationClause VARCHAR(4096) DEFAULT " ";
	DECLARE _deobClause VARCHAR(4096) DEFAULT " ";
	DECLARE _tramsObligations VARCHAR(4096) DEFAULT " ";
	DECLARE _teamObligations VARCHAR(4096) DEFAULT " ";
	DECLARE _tramsDeobligations VARCHAR(4096) DEFAULT " ";
	DECLARE _teamDeobligations VARCHAR(4096) DEFAULT " ";
	DECLARE _union VARCHAR (128) DEFAULT " ";
	DECLARE _orderBy VARCHAR(4096) DEFAULT " ";


    SET @SID = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));

    /* write procedure call to the log */
    CALL `trams_log`(@SID, _spName, CONCAT(
        ' CALL SP Inputs: ',
		IFNULL(`p_transactionFiscalYear`,'NULL'),',',
		IFNULL(`p_recipientId`,'NULL'),',',
		IFNULL(`p_recipientCostCenter`,'NULL'),',',
		IFNULL(`p_appId`,'NULL'),',',
		IFNULL(`p_appType`,'NULL'),',',
		IFNULL(`p_projectId`,'NULL'),',',
		IFNULL(`p_poNumber`,'NULL'),',',
		IFNULL(`p_scopeCode`,'NULL'),',',
		IFNULL(`p_fundingUzaCode`,'NULL'),',',
		IFNULL(`p_accFundingFiscalYear`,'NULL'),',',
		IFNULL(`p_accAppropriationCode`,'NULL'),',',
		IFNULL(`p_accSectionCode`,'NULL'),',',
		IFNULL(`p_accLimitationCode`,'NULL'),',',
		IFNULL(`p_accTypeAuthority`,'NULL'),',',
		IFNULL(`p_startDate`,'NULL'),',',
		IFNULL(`p_endDate`,'NULL'),',',
		IFNULL(`p_transactionType`,'NULL'),',',
		IFNULL(`p_transactionAppType`,'NULL'),',',
		IFNULL(`p_currentYearDeobligation`,'NULL'),',',
		IFNULL(`p_discretionary`,'NULL'),',',
		IFNULL(`p_appStatus`,'NULL'),',',
		IFNULL(`p_gmtOffset`,'NULL'),','
    ), 1);
	
	/* General filteres */
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.recipient_id in (",NULLIF(p_recipientId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.recipient_cost_center in (",NULLIF(p_recipientCostCenter,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.id in (",NULLIF(p_appId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.application_type in (",NULLIF(p_appType,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.discretionary_funds_flag IN (",NULLIF(p_discretionary,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.status in (",NULLIF(p_appStatus,''),")"),"")));
	
	/*TrAMS specific*/
	SET _tramsObScopeClause = CONCAT(_tramsObScopeClause, (SELECT IFNULL(CONCAT(" and tx.scope_code in (",NULLIF(p_scopeCode,''),")"),"")));
	SET _tramsDeobScopeClause = CONCAT(_tramsDeobScopeClause, (SELECT IFNULL(CONCAT(" and deob.scope_number in (",NULLIF(p_scopeCode,''),")"),"")));
	
	/*Obligation specific*/
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.transaction_fiscal_year in (",NULLIF(p_transactionFiscalYear,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and DATE(CONVERT_TZ(tx.transaction_date_time,'+00:00',",p_gmtOffset,")) >= ",NULLIF(p_startDate,'')),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and DATE(CONVERT_TZ(tx.transaction_date_time,'+00:00',",p_gmtOffset,")) <= ",NULLIF(p_endDate,'')),"")));	
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.project_id in (",NULLIF(p_projectId,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.po_number in (",NULLIF(p_poNumber,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.uza_code in (",NULLIF(p_fundingUzaCode,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.funding_fiscal_year in (",NULLIF(p_accFundingFiscalYear,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.appropriation_code in (",NULLIF(p_accAppropriationCode,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.section_code in (",NULLIF(p_accSectionCode,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.limitation_code in (",NULLIF(p_accLimitationCode,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.type_authority in (",NULLIF(p_accTypeAuthority,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and 'Obligation' in (",NULLIF(p_transactionType,''),")"),"")));
	SET _obligationClause = CONCAT(_obligationClause, (SELECT IFNULL(CONCAT(" and tx.transaction_origin_ref_id in (",NULLIF(p_transactionAppType,''),")"),"")));

	
	/*Deob specific*/
	SET _deobClause = (CASE 
		WHEN NULLIF(p_currentYearDeobligation,'') = 'Yes' THEN "and deob.deobligated_fiscal_year = deob.obligation_fiscal_year"
		WHEN NULLIF(p_currentYearDeobligation,'') = 'No' THEN "and deob.deobligated_fiscal_year <> deob.obligation_fiscal_year"
		ELSE ""
		END);
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.deobligated_fiscal_year in (",NULLIF(p_transactionFiscalYear,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and DATE(CONVERT_TZ(coalesce(deob.FinalizedDate, deob.deobligated_recovery_date_time),'+00:00',",p_gmtOffset,")) >= (",NULLIF(p_startDate,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and DATE(CONVERT_TZ(coalesce(deob.FinalizedDate, deob.deobligated_recovery_date_time),'+00:00',",p_gmtOffset,")) <= (",NULLIF(p_endDate,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and proj.id in (",NULLIF(p_projectId,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.po_number in (",NULLIF(p_poNumber,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_uza_code in (",NULLIF(p_fundingUzaCode,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_funding_fiscal_year in (",NULLIF(p_accFundingFiscalYear,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_appropriation_code in (",NULLIF(p_accAppropriationCode,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_section_code in (",NULLIF(p_accSectionCode,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_limitation_code in (",NULLIF(p_accLimitationCode,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.acc_authorization_type in (",NULLIF(p_accTypeAuthority,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and 'Deobligation' in (",NULLIF(p_transactionType,''),")"),"")));
	SET _deobClause = CONCAT(_deobClause, (SELECT IFNULL(CONCAT(" and deob.transaction_origin_ref_id in (",NULLIF(p_transactionAppType,''),")"),"")));

		
	SET _union = " UNION ALL ";
	
	SET _orderBy = "ORDER BY
    `Federal Award ID`,
    `Transaction Date`,
    `Transaction App Type`";

/* TrAMS Obligations */
SET _tramsObligations = 
CONCAT(
"SELECT 
    tx.transaction_fiscal_year AS 'Transaction Fiscal Year',
    app.recipient_id AS 'Recipient ID',
    app.recipient_name AS 'Recipient Name',
    app.recipient_cost_center AS 'Recipient Cost Center',
    app.application_number AS 'Federal Award ID',
    app.status AS 'Award Status',
    app.application_type AS 'Award Type',
    tx.project_number AS 'Project Number',
    tx.po_number AS 'PO Number',
    tx.scope_code AS 'Scope Code',
    tx.scope_suffix AS 'Scope Suffix',
    tx.cost_center_code AS 'Funding Cost Center',
    tx.uza_code AS 'Funding UZA Code',
    tx.funding_fiscal_year AS 'Funding Fiscal Year',
    tx.appropriation_code AS 'Appropriation Code',
    tx.section_code AS 'Section Code',
    tx.limitation_code AS 'Limitation Code',
    tx.type_authority AS 'Type Authority',
    tx.account_class_code AS 'Account Class Code',
    tx.fpc AS 'FPC',
    trams_format_date(CONVERT_TZ(tx.transaction_date_time,'+00:00',",p_gmtOffset,")) AS 'Transaction Date',
    IFNULL(tx.obligation_amount, 0) AS 'Transaction Amount',
    'Obligation' AS 'Transaction Type',
    ref.ref_value AS 'Transaction App Type',
    'N/A' AS 'Current Year Deob',
    IF(app.discretionary_funds_flag, 'Yes', 'No') AS 'Discretionary Application',
    IF(tx.is_reconciled, 'Yes', 'No') AS 'Reconciliation'
FROM
    trams_application app
JOIN
    trams_contract_acc_transaction tx
        ON app.id = tx.application_id
LEFT JOIN
    trams_references ref
        ON tx.transaction_origin_ref_id = ref.id
WHERE
    tx.transaction_type = 'Obligation'
    AND app.app_FROM_team_flag = 0");

/* TEAM Obligations */
SET _teamObligations = 
CONCAT(
"SELECT
    tx.transaction_fiscal_year AS 'Transaction Fiscal Year',
    app.recipient_id AS 'Recipient ID',
    app.recipient_name AS 'Recipient Name',
    app.recipient_cost_center AS 'Recipient Cost Center',
    app.application_number AS 'Federal Award ID',
    app.status AS 'Award Status',
    app.application_type AS 'Award Type',
    tx.project_number AS 'Project Number',
    tx.po_number AS 'PO Number',
    '' AS 'Scope Code',
    '' AS 'Scope Suffix',
    tx.cost_center_code AS 'Funding Cost Center',
    tx.uza_code AS 'Funding UZA Code',
    tx.funding_fiscal_year AS 'Funding Fiscal Year',
    tx.appropriation_code AS 'Appropriation Code',
    tx.section_code AS 'Section Code',
    tx.limitation_code AS 'Limitation Code',
    tx.type_authority AS 'Type Authority',
    tx.account_class_code AS 'Account Class Code',
    tx.fpc AS 'FPC',
    trams_format_date(CONVERT_TZ(tx.transaction_date_time,'+00:00',",p_gmtOffset,")) AS 'Transaction Date',
    IFNULL(tx.obligation_amount, 0) AS 'Transaction Amount',
    'Obligation' AS 'Transaction Type',
    ref.ref_value AS 'Transaction App Type',
    'N/A' AS 'Current Year Deob',
    IF(app.discretionary_funds_flag, 'Yes', 'No') AS 'Discretionary Application',
    IF(tx.is_reconciled, 'Yes', 'No') AS 'Reconciliation'
FROM
    trams_application app
JOIN
    trams_contract_acc_transaction tx
        ON app.id = tx.application_id
LEFT JOIN
    trams_references ref
        ON tx.transaction_origin_ref_id = ref.id
WHERE
    tx.transaction_type = 'Obligation'
    AND tx.transaction_origin_ref_id <> 5
    AND app.app_FROM_team_flag = 1");


/* TrAMS Debligations */
SET _tramsDeobligations = 
CONCAT(
"SELECT
    deob.deobligated_fiscal_year AS 'Transaction Fiscal Year',
    app.recipient_id AS 'Recipient ID',
    app.recipient_name AS 'Recipient Name',
    app.recipient_cost_center AS 'Recipient Cost Center',
    app.application_number AS 'Federal Award ID',
    app.status AS 'Award Status',
    app.application_type AS 'Award Type',
    proj.project_number AS 'Project Number',
    deob.po_number AS 'PO Number',
    deob.scope_number AS 'Scope Code',
    deob.scope_suffix AS 'Scope Suffix',
    deob.acc_cost_center_code AS 'Funding Cost Center',
    deob.acc_uza_code AS 'Funding UZA Code',
    deob.acc_funding_fiscal_year AS 'Funding Fiscal Year',
    deob.acc_appropriation_code AS 'Appropriation Code',
    deob.acc_section_code AS 'Section Code',
    deob.acc_limitation_code AS 'Limitation Code',
    deob.acc_authorization_type AS 'Type Authority',
    deob.account_class_code AS 'Account Class Code',
    deob.acc_fpc AS 'FPC',
    trams_format_date(CONVERT_TZ(coalesce(deob.FinalizedDate, deob.deobligated_recovery_date_time),'+00:00',",p_gmtOffset,")) AS 'Transaction Date',
    ifnull(-deob.deobligated_recovery_amount, 0) AS 'Transaction Amount',
    'Deobligation' AS 'Transaction Type',
    ref.ref_value AS 'Transaction App Type',
    if(deob.deobligated_fiscal_year = deob.obligation_fiscal_year, 'Yes', 'No') AS 'Current Year Deob',
    if(app.discretionary_funds_flag, 'Yes', 'No') AS 'Discretionary Application',
    'No' AS 'Reconciliation'
FROM
    trams_application app
JOIN
    trams_deobligation_detail deob
        ON app.id = deob.current_amendment_id
LEFT JOIN
    trams_project proj
        ON app.id = proj.application_id
        AND deob.project_number_sequence_number = proj.project_number_sequence_number
LEFT JOIN
    trams_references ref
        ON deob.transaction_origin_ref_id = ref.id
WHERE
    deob.deobligated_still_pending = 0
    AND app.app_FROM_team_flag = 0");


/* TEAM Debligations */
SET _teamDeobligations = 
CONCAT(
"SELECT
    deob.deobligated_fiscal_year AS 'Transaction Fiscal Year',
    app.recipient_id AS 'Recipient ID',
    app.recipient_name AS 'Recipient Name',
    app.recipient_cost_center AS 'Recipient Cost Center',
    app.application_number AS 'Federal Award ID',
    app.status AS 'Award Status',
    app.application_type AS 'Award Type',
    app.application_number AS 'Project Number',
    deob.po_number AS 'PO Number',
    '' AS 'Scope Code',
    '' AS 'Scope Suffix',
    deob.acc_cost_center_code AS 'Funding Cost Center',
    deob.acc_uza_code AS 'Funding UZA Code',
    deob.acc_funding_fiscal_year AS 'Funding Fiscal Year',
    deob.acc_appropriation_code AS 'Appropriation Code',
    deob.acc_section_code AS 'Section Code',
    deob.acc_limitation_code AS 'Limitation Code',
    deob.acc_authorization_type AS 'Type Authority',
    deob.account_class_code AS 'Account Class Code',
    deob.acc_fpc AS 'FPC',
    trams_format_date(CONVERT_TZ(COALESCE(deob.FinalizedDate, deob.deobligated_recovery_date_time),'+00:00',",p_gmtOffset,")) AS 'Transaction Date',
    ifnull(-deob.deobligated_recovery_amount, 0) AS 'Transaction Amount',
    'Deobligation' AS 'Transaction Type',
    ref.ref_value AS 'Transaction App Type',
    if(deob.deobligated_fiscal_year = deob.obligation_fiscal_year, 'Yes', 'No') AS 'Current Year Deob',
    if(app.discretionary_funds_flag, 'Yes', 'No') AS 'Discretionary Application',
    'No' AS 'Reconciliation'
FROM
    trams_application app
JOIN
    trams_deobligation_detail deob
        on app.id = deob.current_amendment_id
JOIN trams_project proj
		on app.id = proj.application_id
LEFT JOIN
    trams_references ref
        on deob.transaction_origin_ref_id = ref.id
WHERE
    deob.deobligated_still_pending = 0
    AND deob.transaction_origin_ref_id <> 5
    AND app.app_FROM_team_flag = 1");
	
SET @query = 
	(CASE 
	WHEN NULLIF(p_transactionType, '') = 'Obligation' 
	THEN CONCAT(_tramsObligations, _whereClause, _tramsObScopeClause, _obligationClause, _union, _teamObligations, _whereClause, _obligationClause, _orderBy)
	WHEN NULLIF(p_transactionType, '') = 'Debligation'
	THEN CONCAT(_tramsDeobligations, _whereClause, _tramsDeobScopeClause, _deobClause, _union, _teamDeobligations, _whereClause, _deobClause, _orderBy)
	ELSE CONCAT(_tramsObligations, _whereClause, _tramsObScopeClause, _obligationClause, _union,
	_teamObligations, _whereClause, _obligationClause, _union,
	_tramsDeobligations, _whereClause, _tramsDeobScopeClause, _deobClause, _union, 
	_teamDeobligations, _whereClause, _deobClause, _orderBy)
	END);

PREPARE ObsDeobsByFYStmt FROM @query;
	EXECUTE ObsDeobsByFYStmt;

   /* write success to the log */
CALL `trams_log`(@SID, _spName, CONCAT( now(), 'Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsReconciliationReportOperatingBudget`(
	IN p_accountClassCode VARCHAR(40)
)
BEGIN
   DECLARE _spName varchar(255) default "sptramsReconciliationReportOperatingBudget";
   DECLARE _whereClause VARCHAR(4096) default "";
   
   SET @SID = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));

    /* write procedure call to the log */
    CALL `trams_log`(@SID, _spName, CONCAT(
        ' CALL SP Inputs: ',
		IFNULL(`p_accountClassCode`,'NULL')
    ), 1);
   
SET
   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and recOperBud.AccountClassCode in (\"",NULLIF(p_accountClassCode, ''),"\")"), "") ));

SET
   @reconciliationOperBudInformationQuery = CONCAT(" 
   SELECT 
	recOperBud.ControlNumber,
	recOperBud.SectionCode,
	recOperBud.CostCenterCode,
	recOperBud.OperatingBudgetAvailableBalance
	FROM
		vwtramsReconciliationReportOperatingBudget recOperBud WHERE 1=1", _whereClause);
PREPARE reconciliationReportOperBud 
   FROM
      @reconciliationOperBudInformationQuery;
EXECUTE reconciliationReportOperBud ;

CALL `trams_log`(@SID, _spName, CONCAT( now(), ' Done!'), 1);
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_application_status_budget_report`(IN p_granteeId VARCHAR(512), IN p_costCenterCode VARCHAR(512), IN p_fiscalYear VARCHAR(512),
IN p_appNumber VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_appType VARCHAR(512), IN p_preAwardManager VARCHAR(1024), IN p_postAwardManager VARCHAR(1024),
IN p_return_limit VARCHAR(20)
)
BEGIN

	/*
     Stored Procedure Name : trams_get_application_status_budget_report
     Description : To populate the data for TrAMS nightly 'Generate Static Reports' process.
     Updated by  : Garrison Dean (2020.Nov.24)
		Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
	*/
	
	DECLARE _sql VARCHAR(4096) DEFAULT "";
	DECLARE _whereClause VARCHAR(4096) DEFAULT "";
	DECLARE _spName VARCHAR(255) DEFAULT "trams_get_application_status_budget_report";

	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	
	SET @uuid = UUID_SHORT();

	CALL `trams_log`(@sid, _spName, CONCAT(now(),' CALL SP Inputs: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1); 

	SET SESSION max_heap_table_size = 1024*1024*128;
	
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_id IN (",NULLIF(p_granteeId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_cost_center IN (",NULLIF(p_costCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fiscal_year IN (",NULLIF(p_fiscalYear,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_number IN ('",NULLIF(p_appNumber,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.status IN (",NULLIF(p_appStatus,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_type IN ('",NULLIF(p_appType,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_pre_award_manager IN ('",NULLIF(p_preAwardManager,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_post_award_manager IN ('",NULLIF(p_postAwardManager,''),"')"),"")));
	
	/*2-26-19: Modified CONCAT to include new INSERT statement for permanent tables*/
	SET _sql = CONCAT(
		"INSERT INTO trams_temp_asbr_application (
			SELECT NULL, 
			id AS application_id,",
			@uuid, " AS uuid,",
			@sid, " AS time_stamp 
			FROM trams_application a 
			WHERE 1"
		);	
	
	SET @query = CONCAT (_sql, _whereClause, ')');
	
	PREPARE query FROM @query;
	EXECUTE query;

	SET @appcount=(SELECT COUNT(*) FROM trams_temp_asbr_application WHERE uuid = @uuid);
	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Total application: ',@appcount), 1);

	INSERT INTO `trams_temp_asbr_budget_revision_latest` (
		SELECT
			NULL,
			application_id AS application_id, 
			MAX(revision_number) AS revision_number,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM 
			trams_budget_revision
		WHERE 
			application_id IN (SELECT application_id FROM trams_temp_asbr_application WHERE uuid = @uuid)
		GROUP BY 
			application_id
	);
	
	INSERT INTO `trams_temp_asbr_max_application_number` (
		SELECT
			NULL,
			contractAward_id,
			MAX(application_number) AS application_number,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM
			trams_application
		WHERE
			id IN (SELECT application_id FROM trams_temp_asbr_application WHERE uuid = @uuid)
			AND
			fiscal_year = COALESCE(p_fiscalYear, fiscal_year)
		GROUP BY
			contractAward_id
	);

	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'BEGIN trams_temp_asbr_resultset creation'), 1);

	INSERT INTO `trams_temp_asbr_resultset`(
		SELECT
			NULL,
			a.temp_application_number,
			a.recipient_id,
			gb.recipientAcronym AS recipient_acronym,
			gb.legalBusinessName AS legal_business_name,
			a.recipient_cost_center,
			a.fiscal_year,
			a.application_number,
			a.amendment_number,
			a.application_name,
			br.revision_number,
			a.status,
			a.application_type,
			IF(
				a.app_from_team_flag = 1, 
				a.recipient_contact, 
				trams_poc.user_name
			) AS grantee_poc,		
			a.fta_pre_award_manager,
			a.fta_post_award_manager,
			totals.total_obligation_amount,
			totals.total_deobligation_amount,		
			scope_acc.current_reserved_amount,
			fr.lapsing_fund_flag AS lapsing_funds,
			fr.financial_report_freq AS ffr_reporting_period,
			fr.mpr_report_frequency AS mpr_report_frequency,
			a.preaward_authority_flag AS pre_award_authority,
			IFNULL(latest_transmission_history.original_transmitted_date, trams_format_date(fr.transmitted_date)) AS transmitted_date,
            latest_transmission_history.latest_transmitted_date AS latest_transmitted_date,
			IFNULL(latest_transmission_history.original_submitted_date, trams_format_date(fr.submitted_date)) AS submitted_date,
            latest_transmission_history.latest_submitted_date AS latest_submitted_date,
			aa.fain_assigned_date_time AS fain_assigned_date_time,
			-- These if statements were added on 5/21/18
			IF(aa.submitted_to_dol_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_flag, 0), 'N/A', trams_format_date(aa.submitted_to_dol_date_time)) as submitted_to_dol_date_time,
			IF(aa.dol_certification_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_for_cert_flag, 0), 'N/A', trams_format_date(aa.dol_certification_date_time)) as dol_certification_date_time,
			IF(aa.technical_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_technical_review_flag, 0), 'N/A', trams_format_date(aa.technical_concurrence_date_time)) as technical_concurrence_date_time,
			IF(aa.civil_rights_concurrence_date IS NULL AND NOT IFNULL(fr.requires_civil_rights_review_flag, 0), 'N/A', trams_format_date(aa.civil_rights_concurrence_date)) as civil_rights_concurrence_date,
			aa.environmental_concurrence_date_time AS environmental_concurrence_date_time, 
			aa.planning_concurrence_date_time AS planning_concurrence_date_time,
			IF(aa.operations_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_operations_review_flag, 0), 'N/A', trams_format_date(aa.operations_concurrence_date_time)) as operations_concurrence_date_time,
			aa.legal_concurrence_date_time AS legal_concurrence_date_time,
			aa.ra_concurrence_date_time AS ra_concurrence_date_time,
			cr.logged_release_date AS logged_release_date,
			max_reserve.reservation_finalized_date_time AS reservation_finalized_date_time,
			ca.last_obligation_date AS obligated_date_time,
			ca.last_deobligation_date AS deobligation_date_time,
			aa.executed_date_time AS executed_date_time,
			fntramsGetApplicationPOPEndDateFinalized(a.id, a.contractAward_id) AS period_of_performance_end_date,
			aa.fta_closeout_approval_date_time AS fta_closeout_approval_date_time,
			a.discretionary_funds_flag,
            a.created_date_time AS created_date_time,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM
			trams_application a		
		JOIN
			trams_temp_asbr_max_application_number man
		ON
			a.contractAward_id = man.contractAward_id 
			AND a.application_number = man.application_number
			AND man.uuid = @uuid
		JOIN 
			vwtramsGeneralBusinessInfo gb 
			ON a.recipient_id = gb.recipientId
		JOIN 
			trams_application_award_detail aa 
			ON a.id = aa.application_id
		LEFT JOIN 
			trams_app_congressional_release cr 
			ON a.id=cr.app_id
		LEFT JOIN 
			(
			SELECT
				cat.contract_award_id,
				cat.section_code,
				SUM(cat.obligation_amount) AS total_obligation_amount,
				SUM(cat.deobligation_amount) AS total_deobligation_amount,
				SUM(cat.disbursement_amount) AS total_disbursement_amount,
				SUM(cat.refund_amount) AS total_refund_amount,
				(((IFNULL(SUM(cat.obligation_amount),0) 
				- IFNULL(SUM(cat.deobligation_amount),0)) 
				- IFNULL(SUM(cat.disbursement_amount),0)) 
				+ IFNULL(SUM(cat.refund_amount),0)) AS total_unliquidated_amount,
				(SUM(cat.disbursement_amount) 
				/ (SUM(cat.obligation_amount)-IFNULL(SUM(cat.deobligation_amount),0))) AS percent_disbursement_amount
			FROM 
				trams_contract_acc_transaction cat
			GROUP BY cat.contract_award_id
		) totals
		ON
			a.contractAward_id = totals.contract_award_id
		LEFT JOIN 
			trams_temp_asbr_budget_revision_latest br 
			ON a.id = br.application_id
			AND br.uuid = @uuid
		LEFT JOIN 
			(
				SELECT 
					a.id,
					MAX(r.reservation_finalized_date_time) AS reservation_finalized_date_time,
					MAX(r.total_reserved_amount) AS total_reserved_amount
				FROM
					trams_application a
				JOIN
					trams_reservation r
				ON 
					a.id = r.application_id
				GROUP BY
					a.id
				
			) max_reserve
		ON
			a.id = max_reserve.id
		LEFT JOIN 
			trams_fta_review fr 
			ON a.id = fr.application_id 
		LEFT JOIN 
		(	SELECT
				a.id,
				MAX(poc.user_name) AS user_name
			FROM
				trams_application a

			JOIN
				trams_application_point_of_contact poc
			ON
				a.id = poc.applicationidint
			WHERE
				poc.role = 'Grantee'
			GROUP BY
				a.id
			) trams_poc 
			ON 
				a.id = trams_poc.id 
		LEFT JOIN 
		(	SELECT
				sa.application_id,
				SUM(sa.acc_reservation_amount)AS current_reserved_amount
			FROM
				trams_scope_acc sa
			GROUP BY
				sa.application_id
			) scope_acc 
			ON 
				a.id = scope_acc.application_id
		LEFT JOIN 
			trams_contract_award ca
			ON ca.id = a.contractAward_id
        LEFT JOIN
            (
                SELECT
                    th.applicationId AS applicationId,
                    MAX(th.TransmittedDate) AS latest_transmitted_date,
                    MAX(th.SubmittedDate) AS latest_submitted_date,
                    MIN(th.TransmittedDate) AS original_transmitted_date,
                    MIN(th.SubmittedDate) AS original_submitted_date
                FROM
                    tramsApplicationTransmissionHistory th
                GROUP BY th.applicationId
            ) latest_transmission_history
            ON a.id = latest_transmission_history.applicationId
	);

	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Finished trams_temp_asbr_resultset creation, BEGIN INSERT statement'), 1); 

	INSERT INTO trams_temp_asbr_resultset
		SELECT
			NULL,
			a.temp_application_number, 
			a.recipient_id,
			gb.recipientAcronym AS recipient_acronym,
			gb.legalBusinessName AS legal_business_name,
			a.recipient_cost_center,
			a.fiscal_year,
			a.application_number,
			a.amendment_number,
			a.application_name,
			br.revision_number,
			a.status,
			a.application_type,
			IF(
				a.app_FROM_team_flag = 1, 
				a.recipient_cONtact, 
				trams_poc.user_name
			) AS grantee_poc,		
			a.fta_pre_award_manager,
			a.fta_post_award_manager,
			"" AS total_obligation_amount, 			
			"" AS total_deobligation_amount, 			
			scope_acc.current_reserved_amount,
			fr.lapsing_fund_flag AS lapsing_funds,
			fr.financial_report_freq AS ffr_reporting_period,
			fr.mpr_report_frequency AS mpr_report_frequency,
			a.preaward_authority_flag AS pre_award_authority,
            IFNULL(latest_transmission_history.original_transmitted_date, trams_format_date(fr.transmitted_date)) AS transmitted_date,
            latest_transmission_history.latest_transmitted_date AS latest_transmitted_date,
			IFNULL(latest_transmission_history.original_submitted_date, trams_format_date(fr.submitted_date)) AS submitted_date,
            latest_transmission_history.latest_submitted_date AS latest_submitted_date,
			aa.fain_assigned_date_time AS fain_assigned_date_time,
			-- These if statements were added on 5/21/18
			IF(aa.submitted_to_dol_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_flag, 0), 'N/A', trams_format_date(aa.submitted_to_dol_date_time)) as submitted_to_dol_date_time,
			IF(aa.dol_certification_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_for_cert_flag, 0), 'N/A', trams_format_date(aa.dol_certification_date_time)) as dol_certification_date_time,
			IF(aa.technical_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_technical_review_flag, 0), 'N/A', trams_format_date(aa.technical_concurrence_date_time)) as technical_concurrence_date_time,
			IF(aa.civil_rights_concurrence_date IS NULL AND NOT IFNULL(fr.requires_civil_rights_review_flag, 0), 'N/A', trams_format_date(aa.civil_rights_concurrence_date)) as civil_rights_concurrence_date,
			aa.environmental_concurrence_date_time AS environmental_concurrence_date_time, 
			aa.planning_concurrence_date_time AS planning_concurrence_date_time,
			IF(aa.operations_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_operations_review_flag, 0), 'N/A', trams_format_date(aa.operations_concurrence_date_time)) as operations_concurrence_date_time,
			aa.legal_concurrence_date_time AS legal_concurrence_date_time,
			aa.ra_concurrence_date_time AS ra_concurrence_date_time,
			cr.logged_release_date AS logged_release_date,
			max_reserve.reservation_finalized_date_time AS reservation_finalized_date_time,
			NULL AS obligated_date_time,
			NULL AS deobligation_date_time,
			aa.executed_date_time AS executed_date_time,
			fntramsGetApplicationPOPEndDateFinalized(a.id, a.contractAward_id) AS period_of_performance_end_date,
			aa.fta_closeout_approval_date_time AS fta_closeout_approval_date_time,
			a.discretionary_funds_flag,
            a.created_date_time AS created_date_time,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM
			trams_application a
		JOIN 
			vwtramsGeneralBusinessInfo gb 
			ON a.recipient_id = gb.recipientId
		JOIN 
			trams_application_award_detail aa 
			ON a.id = aa.application_id
		LEFT JOIN 
			trams_app_congressional_release cr 
			ON a.id=cr.app_id
		LEFT JOIN 
			trams_temp_asbr_budget_revision_latest br 
			ON a.id = br.application_id
			AND br.uuid = @uuid
		LEFT JOIN 
			(
				SELECT 
					a.id,
					MAX(r.reservation_finalized_date_time) AS reservation_finalized_date_time,
					MAX(r.total_reserved_amount) AS total_reserved_amount
				FROM
					trams_application a
				JOIN
					trams_reservation r
				ON 
					a.id = r.application_id
				GROUP BY
					a.id
				
			) max_reserve
		ON
			a.id = max_reserve.id
		LEFT JOIN 
			trams_fta_review fr 
			ON a.id = fr.application_id 
		LEFT JOIN 
		(	SELECT
				a.id,
				max(poc.user_name) AS user_name
			FROM
				trams_application a

			JOIN
				trams_application_point_of_contact poc
			ON
				a.id = poc.applicationidint
			WHERE
				poc.role = 'Grantee'
			GROUP BY
				a.id
			) trams_poc 
			ON 
				a.id = trams_poc.id
		LEFT JOIN 
		(	SELECT
				sa.application_id,
				SUM(sa.acc_reservation_amount) AS current_reserved_amount
			FROM
				trams_scope_acc sa
			GROUP BY
				sa.application_id
			) scope_acc 
			ON 
				a.id = scope_acc.application_id		
        LEFT JOIN
            (
                SELECT
                    th.applicationId AS applicationId,
                    MAX(th.TransmittedDate) AS latest_transmitted_date,
                    MAX(th.SubmittedDate) AS latest_submitted_date,
                    MIN(th.TransmittedDate) AS original_transmitted_date,
                    MIN(th.SubmittedDate) AS original_submitted_date
                FROM
                    tramsApplicationTransmissionHistory th
                GROUP BY th.applicationId
            ) latest_transmission_history
            ON a.id = latest_transmission_history.applicationId
		WHERE
			(a.contractAward_id IS NULL OR a.contractAward_id = '') AND
			(a.id IN (SELECT application_id FROM trams_temp_asbr_application WHERE uuid = @uuid));
	

    SET @appcount=(SELECT COUNT(*) FROM trams_temp_asbr_resultset WHERE uuid = @uuid);

	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Total application COUNT: ', @appcount), 1); 
		
	IF @appcount>0 THEN 

	UPDATE `trams_temp_asbr_resultset` trs
	SET trs.created_date_time = 
	(
	SELECT	MIN(`created_date_time`)
	FROM trams_application a
	WHERE a.temp_application_number = trs.temp_application_number
	AND trs.uuid = @uuid 
	)
	WHERE trs.uuid = @uuid;

	END IF;

	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Finished Updates'), 1); 

	SELECT 
	recipient_id,
	recipient_acronym,
	legal_business_name,
	recipient_cost_center,
	fiscal_year,
	temp_application_number,
	application_number,
	amendment_number,
	application_name,
	revision_number,
	status,
	application_type,
	grantee_poc,
	fta_pre_award_manager,
	fta_post_award_manager,
	total_obligation_amount,
	total_deobligation_amount,
	current_reserved_amount,
	lapsing_funds,
	ffr_reporting_period,
	mpr_report_frequency,
	pre_award_authority,
	/*2-26-19: Modified all date fields by wrapping in NULLIF() to fix an issue with dates matching '0000-00-00 00:00:00' in permanent tables*/
	NULLIF(trams_format_date(transmitted_date), '0000-00-00 00:00:00') AS transmitted_date,
    NULLIF(trams_format_date(latest_transmitted_date), '0000-00-00 00:00:00') AS latest_transmitted_date,
    NULLIF(trams_format_date(submitted_date), '0000-00-00 00:00:00') AS submitted_date,
    NULLIF(trams_format_date(latest_submitted_date), '0000-00-00 00:00:00') AS latest_submitted_date,
	NULLIF(trams_format_date(fain_assigned_date_time), '0000-00-00 00:00:00') AS fain_assigned_date_time,
	NULLIF(submitted_to_dol_date_time, '0000-00-00 00:00:00') AS submitted_to_dol_date_time,
	NULLIF(dol_certification_date_time, '0000-00-00 00:00:00') AS dol_certification_date_time,
	NULLIF(technical_concurrence_date_time, '0000-00-00 00:00:00') AS technical_concurrence_date_time,
	NULLIF(civil_rights_concurrence_date, '0000-00-00 00:00:00') AS civil_rights_concurrence_date,
	NULLIF(trams_format_date(environmental_concurrence_date_time), '0000-00-00 00:00:00') AS environmental_concurrence_date_time,
	NULLIF(trams_format_date(planning_concurrence_date_time), '0000-00-00 00:00:00') AS planning_concurrence_date_time,
	NULLIF(operations_concurrence_date_time, '0000-00-00 00:00:00') AS operations_concurrence_date_time,
	NULLIF(trams_format_date(legal_concurrence_date_time), '0000-00-00 00:00:00') AS legal_concurrence_date_time,
	NULLIF(trams_format_date(ra_concurrence_date_time), '0000-00-00 00:00:00') AS ra_concurrence_date_time,
	NULLIF(trams_format_date(logged_release_date), '0000-00-00 00:00:00') AS logged_release_date,
	NULLIF(trams_format_date(reservation_finalized_date_time), '0000-00-00 00:00:00') AS reservation_finalized_date_time,
	NULLIF(trams_format_date(obligated_date_time), '0000-00-00 00:00:00') AS obligated_date_time,
	NULLIF(trams_format_date(deobligation_date_time), '0000-00-00 00:00:00') AS deobligation_date_time,
	NULLIF(trams_format_date(executed_date_time), '0000-00-00 00:00:00') AS executed_date_time,
	IF(period_of_performance_end_date IS NOT NULL, DATE_FORMAT(period_of_performance_end_date, '%Y-%m-%d'), 'N/A') AS period_of_performance_end_date,
	NULLIF(trams_format_date(fta_closeout_approval_date_time), '0000-00-00 00:00:00') AS fta_closeout_approval_date_time,
	discretionary_funds_flag,
	NULLIF(trams_format_date(created_date_time), '0000-00-00 00:00:00') AS created_date_time
	FROM trams_temp_asbr_resultset
	WHERE uuid = @uuid
	ORDER BY recipient_id, application_number, fiscal_year;
        
	DELETE FROM trams_temp_asbr_application WHERE uuid = @uuid;
	DELETE FROM trams_temp_asbr_budget_revision_latest WHERE uuid = @uuid;
	DELETE FROM trams_temp_asbr_max_application_number WHERE uuid = @uuid;
	DELETE FROM trams_temp_asbr_resultset WHERE uuid = @uuid;
	
	CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Done!'), 1);
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsCreateAmendment`(
	IN i_OriginalAppId INT(11),
	IN i_NewAppNum VARCHAR(510),
	IN i_Status VARCHAR(510),
	IN i_CreatedBy VARCHAR(510),
	IN i_AmendmentReason VARCHAR(600),
	IN i_AmendmentDetails VARCHAR(510),
	IN i_FAINYear VARCHAR(255),
	IN i_FAINState VARCHAR(255),
	IN i_FAINSequence VARCHAR(255),
	OUT o_NewAppId INT,
  OUT o_Completed INT
)
BEGIN
	
/*

	Last Updated By: Fuadul Islam
	Last Updated Date: 01/20/22

*/	
	
/* Error handling variables */
	DECLARE _SQLStateCode CHAR(5) DEFAULT '00000';
	DECLARE _ErrorNumber INTEGER;
	DECLARE _MessageText VARCHAR(1000);
	DECLARE _CallingObject VARCHAR(255) DEFAULT 'TRAMS CREATE AMENDMENT';
	DECLARE _TableName VARCHAR(255);
	
/* Variables to store original data */
	DECLARE _OriginalAppNum VARCHAR(510);
	DECLARE _OldLineItemId INTEGER;
	DECLARE _OriginalBudgetRevisionId INTEGER;
	DECLARE _OriginalProjectFindingId INTEGER;
	DECLARE _OriginalFindingLineItemId INTEGER;
  DECLARE _OriginalLineItemId INTEGER;

/* Variables to store new data */
/* DECLARE o_NewAppId INTEGER; */
	DECLARE _NewProjNum VARCHAR(510);
	DECLARE _NewProjId INTEGER;
	DECLARE _NewAwardDetailId INTEGER;
  DECLARE _NewScopeId INTEGER;
  DECLARE _NewContractAwardId INTEGER;
  DECLARE _NewBudgetRevisionId INTEGER;
  DECLARE _AppFromTEAM BOOLEAN;
  DECLARE _AmendmentTimestamp DATETIME;
	/*DECLARE i_CreatedBy VARCHAR(255);*/
  /*DECLARE i_OriginalAppId INTEGER DEFAULT 10003658; /* 10003577,10003658 */
	/*DECLARE i_NewAppNum VARCHAR(510) DEFAULT 'CA-2020-002-01';*/
  
  
/*Cursors variable declarations*/ 
  DECLARE done INT DEFAULT FALSE;


  DECLARE a INTEGER;
  DECLARE b INTEGER;
  DECLARE c INTEGER;
  DECLARE d INTEGER;
  DECLARE e INTEGER;
  DECLARE f INTEGER;
  DECLARE g INTEGER;
  DECLARE h INTEGER;
  DECLARE i INTEGER;
  DECLARE j INTEGER;

/*Project Id Cursors*/
  DECLARE oldProj CURSOR FOR SELECT id FROM trams_project WHERE application_id = i_OriginalAppId ORDER BY project_number_sequence_number;
  DECLARE newProj CURSOR FOR SELECT id FROM trams_project WHERE application_id = o_NewAppId ORDER BY project_number_sequence_number;

/*Cursor Continue Handler*/
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

/*Error handler*/
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
			
      GET DIAGNOSTICS CONDITION 1 _SQLStateCode = RETURNED_SQLSTATE,
          _ErrorNumber = MYSQL_ERRNO,
          _MessageText = MESSAGE_TEXT;
          IF _ErrorNumber <> '00000' THEN
          	ROLLBACK;
            CALL sptramsAmendmentErrorLogInsert(_SQLStateCode, _ErrorNumber, _MessageText, _CallingObject, i_OriginalAppId,	o_NewAppId, _OriginalAppNum, i_NewAppNum,_TableName, i_CreatedBy);
            COMMIT;
          END IF;
		END;
	  
  /*Notes: Top-Level application data must be copied first to ensure unique ID auto-generates.

    Table Hierarchy:
    
    trams_application 
      trams_fta_review 
      trams_app_congressional_release 
      trams_applicaiton_point_of_contact 
      trams_budget_revision 
      trams_po_number
      trams_application_award_detail 
      trams_state_application_review 
      trams_application_fleet_status
      trams_project 
        trams_security
        trams_project_geography
        trams_program_plan_type
        trams_scope
        trams_contract_acc_funding
        trams_line_item
          trams_milestones
          trams_line_item_change_log
        trams_project_environmental_finding
          trams_project_env_finding_line_items
          trams_project_environmental_finding_date */

  SET autocommit = OFF;

  START TRANSACTION;
  
    SET o_Completed = 0;
	  SELECT application_number FROM trams_application WHERE id = i_OriginalAppId INTO _OriginalAppNum;
	
/*Create new insert for trams_application */
    SET _TableName = "trams_application";

    INSERT INTO trams_application (application_number, temp_application_number,application_name, status,dol_status,duns, ueid, recipient_name,fiscal_year, application_type,application_description, 
      created_by,created_date_time,modified_date_time,modified_by,active_flag,delinquent_debt_flag,delinquent_debt_description, preaward_authority_flag, preaward_contract_number, feed_id,amendment_number,
      contractAward_id,trmscntrctwrd_wrdpplctns_dx,amendment_reason,amendment_details,amended_by, amended_date, recipient_id,recipient_cost_center,region,discretionary_funds_flag,fta_contact_from_team, 
      recipient_contact, app_from_team_flag,fta_pre_award_manager, fta_post_award_manager, fain_fiscal_year, fain_state,fain_sequence_number,latest_approved_project_sequence_number,latest_temporary_project_sequence_number)
    SELECT
      i_NewAppNum AS application_number, /*input parameter i_NewAppNum*/
      temp_application_number,
      application_name, 
      i_Status AS status, /*input parameter i_Status*/
      dol_status,
      duns, 
	  IF((ueid IS NULL OR ueid = ''), (SELECT ueid FROM vwtramsGeneralBusinessInfo WHERE recipientId = recipient_id), ueid) AS ueid,
      recipient_name,
      CASE 
        WHEN MONTH(CURRENT_DATE()) > 9 THEN CONVERT(YEAR(CURRENT_DATE()) + 1, CHAR(4))
        WHEN MONTH(CURRENT_DATE()) <= 9 THEN CONVERT(YEAR(CURRENT_DATE()), CHAR(4))
        ELSE NULL
      END AS fiscal_year,
      application_type,
      application_description, 
      created_by,
      UTC_TIMESTAMP() AS created_date_time,
      UTC_TIMESTAMP() AS modified_date_time,
      i_CreatedBy AS modified_by, /*input parameter i_CreatedBy*/
      0 AS active_flag,
      delinquent_debt_flag,
      delinquent_debt_description,
      preaward_authority_flag,
      preaward_contract_number,
      feed_id,
      CASE
        WHEN amendment_number IS NULL THEN 0
        WHEN amendment_number IS NOT NULL THEN amendment_number + 1
        ELSE NULL
      END AS amendment_number,
      contractAward_id,
      CASE
        WHEN amendment_number IS NULL THEN 0
        WHEN amendment_number IS NOT NULL THEN amendment_number + 1
        ELSE NULL
      END AS trmscntrctwrd_wrdpplctns_dx,
      i_AmendmentReason AS amendment_reason, /*input parameter i_AmendmentReason*/
      i_AmendmentDetails AS amendment_details, /*input parameter i_AmendmentDetails*/
      i_CreatedBy AS amended_by /*input parameter i_CreatedBy*/,
      UTC_TIMESTAMP() AS amended_date,
      recipient_id,
      recipient_cost_center,
      region,
      discretionary_funds_flag,
      fta_contact_from_team, 
      recipient_contact, 
      app_from_team_flag,
      fta_pre_award_manager, 
      fta_post_award_manager, 
      i_FAINYear AS fain_fiscal_year, /*input parameter i_FAINYear*/
      i_FAINState AS fain_state, /*input parameter i_FAINState*/
      i_FAINSequence AS fain_sequence_number, /*input parameter i_FAINSequence*/
      CASE
      WHEN latest_approved_project_sequence_number IS NULL THEN (SELECT COUNT(project_number) FROM trams_project WHERE application_id = i_OriginalAppId)
      WHEN latest_approved_project_sequence_number IS NOT NULL THEN latest_approved_project_sequence_number               
      ELSE NULL 
    END AS latest_approved_project_sequence_number,
    CASE
      WHEN latest_temporary_project_sequence_number IS NULL THEN (SELECT COUNT(project_number) FROM trams_project WHERE application_id = i_OriginalAppId)
      WHEN latest_temporary_project_sequence_number IS NOT NULL THEN latest_temporary_project_sequence_number               
      ELSE NULL 
    END AS latest_temporary_project_sequence_number
    FROM trams_application
    WHERE id = i_OriginalAppId;
    
 /*Get new application id and TEAM flag from new application*/
    SELECT LAST_INSERT_ID() INTO o_NewAppId;
    SELECT app_from_team_flag FROM trams_application WHERE id = o_NewAppId INTO _AppFromTEAM;
    SELECT modified_date_time FROM trams_application WHERE id = o_NewAppId INTO _AmendmentTimestamp;

    
  /*Create new inserts for trams_application_point_of_contact*/
    SET _TableName = "trams_application_point_of_contact";

    INSERT INTO trams_application_point_of_contact(applicationidint,user_name,role,	fta_contact_flag,trmspplctin_pintfcntcts_idx)
    SELECT 
      o_NewAppId AS applicationidint,
      user_name,
      role,
      fta_contact_flag,
      trmspplctin_pintfcntcts_idx
    FROM trams_application_point_of_contact
    WHERE applicationidint = i_OriginalAppId order by id;
	
	/*Create new insert for tramsPOPChangelog*/
	INSERT INTO tramsPOPChangelog(ContractAwardId, ApplicationId, POPEndDate, RevisionNum, SourceOfChangeId, IsApproved, IsDeleted)
	SELECT
		app.contractAward_id AS ContractAwardId,
		o_NewAppId AS ApplicationId,
		pop.POPEndDate AS POPEndDate,
		pop.RevisionNum AS RevisionNum,
		CASE
			WHEN i_Status = 'In-Progress / Admin Amendment' THEN 67
			WHEN i_Status = 'Active Award / Ready for Closeout' THEN 68
			ELSE 66
		END AS SourceOfChangeId,
		false AS IsApproved,
		false AS IsDeleted
	FROM trams_application app
	JOIN tramsPOPChangelog pop ON app.id = pop.ApplicationId 
	AND pop.POPChangelogId = (SELECT MAX(POPChangelogId) FROM tramsPOPChangelog WHERE ApplicationId = i_OriginalAppId)
	AND pop.isApproved = 1
	AND pop.isDeleted = 0
	WHERE app.id = i_OriginalAppId;
    
 /*Create new insert for trams_application_award_detail*/
    SET _TableName = "trams_application_award_detail";

    INSERT INTO trams_application_award_detail ( 
      recipient_id, application_id, application_name, application_number, fiscal_year, active_flag, final_budget_flag, updated_by, updated_date_time,executed_by, executed_by_title, executed_by_organization, executed_date_time, execution_comment, obligated_date_time, obligated_by, obligated_by_title, /*award_comment,*/ frc_control_number, total_eligible_cost_amount, total_fta_amount, total_state_amount, total_local_amount, total_other_fed_amount,
      total_toll_rev_credit_amount, total_adjustment_amount, closeout_amount, total_outyear_amount, total_planned_amount, total_under_contract, gross_project_cost_amount, total_encumbered_amount, /*closeout_requested_date, contract_withdrawn_date, deobligation_date_time,*/ disbursement_date_time, /*fta_closeout_approval_date_time, fta_disapproved_date_time, record_to_frc_date, contract_complete_date,*/ fain_assigned_date_time, scopes_updated_date_time, /*submitted_to_dol_date_time, dol_certification_date_time, stip_approval_date_time, upwp_approval_date_time, long_range_plan_approval_date_time, initial_concurrence_date_time, 
      technical_concurrence_date_time, civil_rights_concurrence_date, environmental_concurrence_date_time, planning_concurrence_date_time, operations_concurrence_date_time, legal_concurrence_date_time, 
      ra_concurrence_date_time, refund_date_time, obligation_fiscal_year, deobligated_by, deobligation_fiscal_year,*/cumulative_contract_oblig_amount, cumulative_contract_fta_amount, amendment_oblig_amount, amendment_fta_amount,
      amendment_special_cond_amount, cumulative_contract_gross_cost_amount, cumulative_contract_adjustment_amount, cumulative_contract_total_eligible_cost_amount, cumulative_contract_state_amount, cumulative_contract_local_amount, cumulative_contract_other_fed_amount, cumulative_contract_special_cond_amount, cumulative_contract_toll_rev_credit_amount
    )
    SELECT  
      recipient_id, 
      o_NewAppId AS application_id, 
      application_name, 
      i_NewAppNum AS application_number, /*input parameter i_NewAppNum */
      (SELECT fiscal_year FROM trams_application WHERE id = o_NewAppId) AS fiscal_year, 
      0 AS active_flag, 
      0 AS final_budget_flag, 
      (SELECT amended_by FROM trams_application WHERE id = o_NewAppId) AS updated_by, 
      (SELECT amended_date FROM trams_application WHERE id = o_NewAppId) AS updated_date_time,
      executed_by, 
      executed_by_title, 
      executed_by_organization, 
      executed_date_time, 
      execution_comment, 
      obligated_date_time, 
      obligated_by, 
      obligated_by_title, 
      /*award_comment,*/
      frc_control_number, 
      total_eligible_cost_amount, 
      total_fta_amount, 
      total_state_amount, 
      total_local_amount, 
      total_other_fed_amount, 
      total_toll_rev_credit_amount, 
      total_adjustment_amount, 
      CASE 
        WHEN closeout_amount IS NULL THEN 0
        WHEN closeout_amount IS NOT NULL THEN closeout_amount
        ELSE NULL
      END AS closeout_amount, 
      CASE 
        WHEN total_outyear_amount IS NULL THEN 0
        WHEN total_outyear_amount IS NOT NULL THEN total_outyear_amount
        ELSE NULL
      END AS total_outyear_amount, 
      CASE 
        WHEN total_planned_amount IS NULL THEN 0
        WHEN total_planned_amount IS NOT NULL THEN total_planned_amount
        ELSE NULL
      END AS total_planned_amount, 
      CASE 
        WHEN total_under_contract IS NULL THEN 0
        WHEN total_under_contract IS NOT NULL THEN total_under_contract
        ELSE NULL
      END AS total_under_contract, 
      gross_project_cost_amount, 
      CASE 
        WHEN total_encumbered_amount IS NULL THEN 0
        WHEN total_encumbered_amount IS NOT NULL THEN total_encumbered_amount
        ELSE NULL
      END AS total_encumbered_amount, 
      /*closeout_requested_date, 
      contract_withdrawn_date, 
      deobligation_date_time, */
      disbursement_date_time, 
      /*fta_closeout_approval_date_time, 
      fta_disapproved_date_time, 
      record_to_frc_date, 
      contract_complete_date, */
      fain_assigned_date_time, 
      scopes_updated_date_time, 
      /*submitted_to_dol_date_time, 
      dol_certification_date_time, 
      stip_approval_date_time, 
      upwp_approval_date_time, 
      long_range_plan_approval_date_time, 
      initial_concurrence_date_time, 
      technical_concurrence_date_time, 
      civil_rights_concurrence_date, 
      environmental_concurrence_date_time, 
      planning_concurrence_date_time, 
      operations_concurrence_date_time, 
      legal_concurrence_date_time, 
      ra_concurrence_date_time, 
      refund_date_time, 
      obligation_fiscal_year, 
      deobligated_by, 
      deobligation_fiscal_year,*/
      cumulative_contract_oblig_amount,
      cumulative_contract_fta_amount, 
      amendment_oblig_amount, 
      amendment_fta_amount,
      amendment_special_cond_amount, 
      cumulative_contract_gross_cost_amount, 
      cumulative_contract_adjustment_amount, 
      cumulative_contract_total_eligible_cost_amount, 
      cumulative_contract_state_amount, 
      cumulative_contract_local_amount, 
      cumulative_contract_other_fed_amount, 
      cumulative_contract_special_cond_amount, 
      cumulative_contract_toll_rev_credit_amount
    FROM trams_application_award_detail
    WHERE application_id = i_OriginalAppId;

    /* Get new application award detail id and update the new application */
    SELECT LAST_INSERT_ID() INTO _NewAwardDetailId;
    UPDATE trams_application SET trmspplctnwr_pplctnwrddtl_d = _NewAwardDetailId WHERE id = o_NewAppId;

    /* Create new inserts for trams_state_application_review */
    SET _TableName = "trams_state_application_review";

    INSERT INTO trams_state_application_review(applicationidint, eo12372_review_flag, state_application_id, received_by_state_date, trmspplictin_sttppreview_id, trmspplictin_sttpprview_idx)
    SELECT 
      o_NewAppId AS applicationidint,
      eo12372_review_flag,
      state_application_id,
      received_by_state_date,
      o_NewAppId AS trmspplictin_sttppreview_id,
      trmspplictin_sttpprview_idx
    FROM trams_state_application_review
    WHERE applicationidint = i_OriginalAppId;

    /* Create new inserts for trams_fta_review */
    SET _TableName = "trams_fta_review";

    INSERT INTO trams_fta_review(duns, application_id, /*review_in_progress_flag,*/ lapsing_fund_flag, mpr_report_frequency, financial_report_freq, transmitted_date, submitted_date, 
      /*submitted_to_dol_flag, submitted_to_dol_for_cert_flag, certified_by_dol_flag, returned_by_dol_flag, requires_technical_review_flag, requires_civil_rights_review_flag,
      requires_environmental_review_flag, requires_planning_review_flag, requires_operations_review_flag, 
      dol_comments, civil_rights_concur_decision, environmental_concurrence_flag, technical_concurrence_flag, director_planning_concurrence_flag, director_ops_concurrence_flag, 
      legal_concurrence_flag, ra_concurrence_flag,*/ recipient_id)
    SELECT 
      duns, 
      o_NewAppId AS application_id, 
      /*review_in_progress_flag, */
      lapsing_fund_flag, 
      mpr_report_frequency, 
      financial_report_freq, 
      "" AS transmitted_date, 
      "" AS submitted_date, 
      /* submitted_to_dol_flag, 
      submitted_to_dol_for_cert_flag, 
      certified_by_dol_flag, 
      returned_by_dol_flag, 
      requires_technical_review_flag, 
      requires_civil_rights_review_flag,
      requires_environmental_review_flag, 
      requires_planning_review_flag, 
      requires_operations_review_flag, 
      dol_comments, 
      civil_rights_concur_decision, 
      environmental_concurrence_flag, 
      technical_concurrence_flag, 
      director_planning_concurrence_flag, 
      director_ops_concurrence_flag, 
      legal_concurrence_flag, 
      ra_concurrence_flag, */
      recipient_id
    FROM trams_fta_review
    WHERE application_id = i_OriginalAppId order by id;


/*Create new inserts for trams_app_congressional_release */
	SET _TableName = "trams_app_congressional_release";

    INSERT INTO trams_app_congressional_release (app_id, discretionary_title, talking_point_overview, talking_points, place_of_performance, congressional_interest_text, last_modified_date_time, last_modified_by) 
    SELECT 
		  o_NewAppId AS app_id, 
		  discretionary_title, 
		  talking_point_overview, 
		  talking_points, 
		  place_of_performance, 
		  congressional_interest_text,
		  last_modified_date_time,
		  last_modified_by
    FROM trams_app_congressional_release
    WHERE app_id = i_OriginalAppId;

 
/* Create new insert for trams_budget_revision */
    SET _TableName = "trams_budget_revision";

    INSERT INTO trams_budget_revision (contract_award_id, application_id, revision_number, revision_status, revision_reason, revision_description, change_size, created_date_time, created_by, submitted_date_time, submitted_by, approved_date, approved_by, obligated_date_time, obligated_by, executed_date_time, executed_by, latest_revision_flag)
    SELECT 
      contract_award_id, 
      o_NewAppId AS application_id, 
      0 AS revision_number, 
      "Approved" AS revision_status, 
      "New Application" AS revision_reason, 
      "New Application" AS revision_description, 
      NULL AS change_size, 
      UTC_TIMESTAMP() AS created_date_time, 
      i_CreatedBy AS created_by, /*input parameter i_CreatedBy*/
      NULL AS submitted_date_time, 
      NULL AS submitted_by, 
      NULL AS approved_date, 
      NULL AS approved_by,
      NULL AS obligated_date_time, 
      NULL AS obligated_by, 
      NULL AS executed_date_time, 
      NULL AS executed_by, 
      1 AS latest_revision_flag
    FROM trams_budget_revision
    WHERE application_id = i_OriginalAppId AND revision_number = 0 order by id;

    SELECT id FROM trams_budget_revision WHERE application_id  = o_NewAppId order by id INTO _NewBudgetRevisionId;
	SELECT id FROM trams_budget_revision WHERE (application_id = i_OriginalAppId AND latest_revision_flag = 1) INTO _OriginalBudgetRevisionId;


/*Create new inserts for trams_po_number */
    SET _TableName = "trams_po_number";

    INSERT INTO  trams_po_number (application_id, contract_award_id, po_number_full, po_state, po_section_code, po_activity_code, po_sequence_number, amendment_reservation_amount, cumulative_reservation_amount,  
      amendment_obligation_amount, cumulative_obligation_amount, funding_source_description, funding_source_name) 
    SELECT 
      o_NewAppId AS application_id,  
      contract_award_id,  
      po_number_full,  
      po_state,  
      po_section_code,  
      po_activity_code,  
      po_sequence_number,  
      0.0 AS amendment_reservation_amount,  
      cumulative_reservation_amount,  
      0.0 AS amendment_obligation_amount,  
      cumulative_obligation_amount, 
      funding_source_description, 
      funding_source_name  
    FROM trams_po_number
    WHERE application_id = i_OriginalAppId order by id;

/*Create new inserts for trams_application_fleet_status*/
    SET _TableName = "trams_application_fleet_status";

    INSERT INTO trams_application_fleet_status (application_id, duns, recipient_id, recipient_name, fleet_type, peak_requirement_before,
      peak_requirement_change, peak_requirement_after, spares_before, spares_change, spares_after, active_total_before, active_total_change, active_total_after, 
      spare_ratio_before, spare_ratio_change, spare_ratio_after, contingency_before, contingency_change, contingency_after, pending_disposal_before, 
      pending_disposal_change, pending_disposal_after, inactive_total_before, inactive_total_change, inactive_total_after, comprehensive_total_before,
      comprehensive_total_change, comprehensive_total_after, fleet_details, updated_date_time, updated_by)
    SELECT 
      o_NewAppId AS application_id, 
      duns, 
      recipient_id, 
      recipient_name, 
      fleet_type, 
      peak_requirement_before,
      peak_requirement_change, 
      peak_requirement_after, 
      spares_before, 
      spares_change, 
      spares_after, 
      active_total_before, 
      active_total_change, 
      active_total_after, 
      spare_ratio_before, 
      spare_ratio_change, 
      spare_ratio_after, 
      contingency_before, 
      contingency_change, 
      contingency_after, 
      pending_disposal_before, 
      pending_disposal_change, 
      pending_disposal_after, 
      inactive_total_before, 
      inactive_total_change, 
      inactive_total_after, 
      comprehensive_total_before,
      comprehensive_total_change, 
      comprehensive_total_after, 
      fleet_details, 
      _AmendmentTimestamp AS updated_date_time, 
      i_CreatedBy AS updated_by /*input parameter i_CreatedBy*/
    FROM trams_application_fleet_status 
    WHERE application_id = i_OriginalAppId order by id;

    
/*Create new inserts for trams_projects*/
    SET _TableName = "trams_project";

    INSERT INTO trams_project (project_number, project_title, status, temp_project_number, project_description, benefits_description, additional_information, location_description, created_by, created_date_time, modified_date_time, modified_by, environmental_finding_flag, major_capital_project_flag, type_of_major_capital_project, application_id, tramsapplicatin_projects_id, tramsapplicatin_prjects_idx, 
      recipient_id, recipient_cost_center, fiscal_year, region, project_number_fiscal_year, project_number_state, project_number_sequence_number, trmsprgrmplntyp_prgrmpln_id) 
    SELECT 
      CASE 
        WHEN _AppFromTEAM = 1 THEN i_NewAppNum 
        WHEN _AppFromTEAM = 0 THEN concat(substr(i_NewAppNum,1,12),substr(project_number,13,2),substr(i_NewAppNum,12,3))
      END AS project_number, /* i_NewAppNum input parameter */
      project_title,	
      'In-Progress' AS status, 
      temp_project_number, 
      project_description, 
      benefits_description, 
      additional_information, 
      location_description, 
      created_by, 
      UTC_TIMESTAMP() AS created_date_time, 
      UTC_TIMESTAMP() AS modified_date_time, 
      (SELECT amended_by FROM trams_application WHERE id = o_NewAppId) AS modified_by, 
      environmental_finding_flag, 
      major_capital_project_flag, 
      type_of_major_capital_project, 
      o_NewAppId AS application_id, 
      o_NewAppId AS tramsapplicatin_projects_id, 
      tramsapplicatin_prjects_idx, 
      recipient_id, 
      recipient_cost_center, 
      fiscal_year, 
      region, 
      project_number_fiscal_year, 
      project_number_state, 
      project_number_sequence_number,
			trmsprgrmplntyp_prgrmpln_id
    FROM trams_project
    WHERE application_id = i_OriginalAppId order by id;
    
  /*Create new inserts for trams_security*/
    
    SET _TableName = "trams_security";
    SET done = FALSE;
    
    OPEN oldProj;
    OPEN newProj;

    read_loop: LOOP
      FETCH oldProj INTO a;
      FETCH newProj INTO b;
      IF done THEN 
        LEAVE read_loop;
      END IF;
      INSERT INTO trams_security(
        application_id, project_id, created_by, created_date_time, modified_date_time, modified_by, active_flag, security_name,
        security_finding, security_comment, reason_1_flag, reason_2_flag, reason_3_flag)
      SELECT 
        o_NewAppId AS application_id,
        b AS project_id,
        created_by,
        UTC_TIMESTAMP() AS created_date_time,
        UTC_TIMESTAMP() AS modified_date_time,
        (SELECT amended_by FROM trams_application WHERE id = o_NewAppId) AS modified_by,
        active_flag,
        security_name,
        security_finding,
        security_comment,
        reason_1_flag,
        reason_2_flag,
        reason_3_flag
      FROM trams_security WHERE project_id = a order by id;	    
	  
	  /* Insert for trams_program_plan_type*/
	   SET _TableName = "trams_program_plan_type";
	   INSERT INTO trams_program_plan_type(
        program_name, app_id, project_id, program_plan_flag, program_date_time, stip_program_date_time, 
        upwp_program_date_time, long_range_program_plan_date_time, stip_program_description, upwp_program_description, long_range_program_description, 
        program_document_id, program_page_location, active_flag, active_date_time, inactive_date_time, created_by)
      SELECT 
        program_name, 
        o_NewAppId AS app_id, 
        b AS project_id, 
        program_plan_flag, 
        program_date_time, 
        stip_program_date_time, 
        upwp_program_date_time, 
        long_range_program_plan_date_time, 
        stip_program_description, 
        upwp_program_description, 
        long_range_program_description, 
        program_document_id, 
        program_page_location, 
        active_flag, 
        active_date_time, 
        inactive_date_time, 
        created_by
      FROM trams_program_plan_type WHERE project_id = a;
	  
	  	  /* Insert for trams_program_plan_type*/
	   SET _TableName = "trams_program_plan_type";
			BLOCK1: BEGIN
                DECLARE no_more_rows1 BOOLEAN DEFAULT FALSE;
                DECLARE oldProgramPlan CURSOR FOR SELECT id FROM trams_program_plan_type WHERE project_id = a;
				DECLARE newProgramPlan CURSOR FOR SELECT id FROM trams_program_plan_type WHERE project_id = b;
				DECLARE CONTINUE HANDLER FOR NOT FOUND SET no_more_rows1 = TRUE;
					
				SET no_more_rows1 = FALSE;
                OPEN oldProgramPlan;
				OPEN newProgramPlan;
				read_loop1: LOOP
								FETCH oldProgramPlan INTO c;
								FETCH newProgramPlan INTO d;
								IF no_more_rows1 THEN 
									CLOSE oldProgramPlan;
									CLOSE newProgramPlan;
									LEAVE read_loop1;
								END IF;
								UPDATE trams_project SET trmsprgrmplntyp_prgrmpln_id = d WHERE id = b AND trmsprgrmplntyp_prgrmpln_id = c;
							END LOOP;


			END BLOCK1;
	/* Create inserts for trams_project_geography */
    SET _TableName = "trams_project_geography";
	
	INSERT INTO trams_project_geography(
        uza_id, congressional_id, state_id, state_name, uza_code, uza_name, 
		    congressional_district_name, congressional_representative_name, selected_flag, 
        trmsprject_prjectgegrphy_id, trmsprjct_prjectgegrphy_idx, geographic_identifiers_id)
      SELECT 
        uza_id, 
        congressional_id, 
        state_id, 
        state_name, 
        uza_code, 
        uza_name, 
		    congressional_district_name, 
        congressional_representative_name, 
        selected_flag, 
        b AS trmsprject_prjectgegrphy_id, 
        trmsprjct_prjectgegrphy_idx, 
        geographic_identifiers_id
      FROM trams_project_geography WHERE trmsprject_prjectgegrphy_id = a order by id;
	  
	      /* Create inserts for trams_contract_acc_funding */
    SET _TableName = "trams_contract_acc_funding";
	
	INSERT INTO trams_contract_acc_funding(
        trmscntrctw_cntrctccfnd_dx, contract_award_id, recipient_id, project_id, application_id, 
        application_number, project_number, scope_number, scope_name, account_class_code, cost_center_code, 
        funding_source_name, uza_code, fpc, amendment_amount_current, contract_amount_cumulative, funding_fiscal_year, authorization_type, appropriation_code, section_code, 
        limitation_code, created_date_time, created_by, po_number, project_number_sequence_number, scope_suffix, last_disbursement_date, object_class)
      SELECT 
        CASE 
          WHEN trmscntrctw_cntrctccfnd_dx IS NULL THEN NULL
          WHEN trmscntrctw_cntrctccfnd_dx IS NOT NULL THEN trmscntrctw_cntrctccfnd_dx + 1  
          ELSE NULL
        END AS trmscntrctw_cntrctccfnd_dx,
        contract_award_id, 
        recipient_id, 
        b AS project_id, 
        o_NewAppId AS application_id, 
        i_NewAppNum AS application_number, /*input parameter p_applnum*/
        (SELECT project_number FROM trams_project WHERE id = b) AS project_number, 
        scope_number, 
        scope_name, 
        account_class_code, 
        cost_center_code, 
        funding_source_name, 
        uza_code, 
        fpc, 
        0.0 AS amendment_amount_current, 
        contract_amount_cumulative, 
        funding_fiscal_year, 
        authorization_type, 
        appropriation_code, 
        section_code, 
        limitation_code, 
        (SELECT created_date_time FROM trams_project WHERE id = b) AS created_date_time, 
        (SELECT modified_by FROM trams_project WHERE id = b) AS created_by,
        po_number, 
        project_number_sequence_number, 
        scope_suffix, 
        last_disbursement_date, 
        object_class
      FROM trams_contract_acc_funding WHERE project_id = a order by id;
	  
	  
	SET _TableName = "trams_line_item";

    INSERT INTO trams_line_item ( project_id, app_id, scope_id, original_line_item_id, status, created_by, created_date_time, modified_date_time, modified_by, duns, section_code, scc_line_item_name, scc_line_item_code, scope_detailed_name, scope_code, scope_suffix, line_item_number, activity_name, activity_code, allocation_name, allocation_code, line_item_type_name, line_item_type_code, custom_item_name, extended_budget_description, revised_line_item_description, quantity, latest_amend_quantity, latest_revised_quantity, total_amount, latest_amend_total_amount, revised_total_amount, funding_source_name, original_FTA_amount, original_state_amount, original_local_amount, original_other_fed_amount, original_state_in_kind_amount, original_local_in_kind_amount, original_toll_revenue_amount, latest_amend_fta_amount, latest_amend_state_amount, latest_amend_local_amount,  
    latest_amend_other_fed_amount, latest_amend_state_inkind_amount, latest_amend_local_inkind_amount, latest_amend_toll_rev_amount, revised_FTA_amount, revised_state_amount, revised_local_amount,  
    revised_other_fed_amount, revised_state_in_kind_amount, revised_local_in_kind_amount, revised_toll_revenue_amount, fuel_name,propulsion, vehicle_condition, vehicle_size, third_party_flag, original_adjustment_amount, latest_amend_adjustment_amount, revised_adjustment_amount,revision_number, other_budget, is_line_item)
    SELECT 
      b AS project_id, 
      o_NewAppId AS app_id, 
      scope_id, 
      original_line_item_id,
      status, 
      created_by, 
      UTC_TIMESTAMP() AS created_date_time, 
      UTC_TIMESTAMP() AS modified_date_time, 
      modified_by, 
      duns, 
      section_code, 
      scc_line_item_name, 
      scc_line_item_code, 
      scope_detailed_name, 
      scope_code, 
      scope_suffix, 
      line_item_number,
      activity_name, 
      activity_code, 
      allocation_name, 
      allocation_code, 
      line_item_type_name,
      line_item_type_code, 
      custom_item_name, 
      extended_budget_description, 
      revised_line_item_description, 
      latest_revised_quantity AS quantity,
      latest_revised_quantity AS latest_amend_quantity, 
      latest_revised_quantity, 
      revised_total_amount AS total_amount, 
      revised_total_amount AS latest_amend_total_amount, 
      revised_total_amount, 
      funding_source_name,  
      revised_FTA_amount AS original_FTA_amount, 
      revised_state_amount AS original_state_amount, 
      revised_local_amount AS original_local_amount,  
      revised_other_fed_amount AS original_other_fed_amount, 
      revised_state_in_kind_amount AS original_state_in_kind_amount, 
      revised_local_in_kind_amount AS original_local_in_kind_amount, 
      revised_toll_revenue_amount AS original_toll_revenue_amount,
      revised_FTA_amount AS latest_amend_fta_amount, 
      revised_state_amount AS latest_amend_state_amount, 
      revised_local_amount AS latest_amend_local_amount,  
      revised_other_fed_amount AS latest_amend_other_fed_amount, 
      revised_state_in_kind_amount AS latest_amend_state_inkind_amount, 
      revised_local_in_kind_amount AS latest_amend_local_inkind_amount, 
      revised_toll_revenue_amount AS latest_amend_toll_rev_amount,
      revised_FTA_amount, 
      revised_state_amount, 
      revised_local_amount,  
      revised_other_fed_amount, 
      revised_state_in_kind_amount, 
      revised_local_in_kind_amount, 
      revised_toll_revenue_amount,
      fuel_name,propulsion, 
      vehicle_condition, 
      vehicle_size, 
      third_party_flag, 
      revised_adjustment_amount AS original_adjustment_amount, 
      revised_adjustment_amount AS latest_amend_adjustment_amount, 
      revised_adjustment_amount,
      0 AS revision_number,
      other_budget, 
      is_line_item
    FROM trams_line_item 
    WHERE project_id = a ORDER BY id;
	
	
		  	  /* Insert for trams_milestones*/
	   SET _TableName = "trams_milestones";
			BLOCK2: Begin
                declare no_more_rows2 boolean DEFAULT FALSE;
				DECLARE oldLine CURSOR FOR SELECT id FROM trams_line_item WHERE project_id = a ORDER BY original_line_item_id;
				DECLARE newLine CURSOR FOR SELECT id FROM trams_line_item WHERE project_id = b ORDER BY original_line_item_id;
                declare continue handler for not found
					SET no_more_rows2 =TRUE;
					
                open oldLine;
				open newLine;
				read_loop2: LOOP
								FETCH oldLine INTO e;
								FETCH newLine INTO f;
								IF no_more_rows2 THEN 
									CLOSE oldLine;
									CLOSE newLine;
									LEAVE read_loop2;
								END IF;
								INSERT INTO trams_milestones(line_item_id, created_by, created_date_time, modified_date_time, modified_by, active_flag, milestone_title, milestone_description, estimated_completion_date_time, tramslineitem_milestnes_idx, original_milestone_id)
								SELECT
									f AS line_item_id, 
									created_by, 
									UTC_TIMESTAMP() AS created_date_time, 
									UTC_TIMESTAMP() AS modified_date_time, 
									modified_by, 
									active_flag, 
									milestone_title, 
									milestone_description, 
									estimated_completion_date_time, 
									tramslineitem_milestnes_idx, 
									original_milestone_id
								FROM trams_milestones 
								WHERE line_item_id = e order by id;
								
								INSERT INTO trams_line_item_change_log(
								application_id, line_item_id, budget_revision_id, revision_number, 
								original_custom_item_name, revised_custom_item_name, zeroed_out, original_FTA_amount, latest_amend_fta_amount,  
								revised_FTA_amount, original_total_eligible_amount, latest_amend_total_eligible_amount, revised_total_eligible_amount, 
								original_quantity, latest_amend_quantity, revised_quantity, original_fuel, 
								original_line_item_description, revised_line_item_description, original_state_amount, latest_amend_state_amount, 
								revised_state_amount, original_local_amount, latest_amend_local_amount, revised_local_amount, 
								original_other_fed_amount, latest_amend_other_fed_amount, revised_other_fed_amount, original_state_in_kind_amount, latest_amend_state_inkind_amount,
								revised_state_in_kind_amount, original_local_in_kind_amount, latest_amend_local_inkind_amount, revised_local_in_kind_amount, 
								original_toll_revenue_amount, latest_amend_toll_rev_amount, revised_toll_revenue_amount, revised_date_time, revised_by, 
								original_adjustment_amount, latest_amend_adjustment_amount, revised_adjustment_amount, thirdPartyFlag
								)
								SELECT
									o_NewAppId AS application_id, 
									f AS line_item_id, 
									_NewBudgetRevisionId AS budget_revision_id, 
									0 AS revision_number, 
									original_custom_item_name, 
									revised_custom_item_name, 
									zeroed_out, 
									revised_FTA_amount AS original_FTA_amount, 
									revised_FTA_amount AS latest_amend_fta_amount,  
									revised_FTA_amount, 
									revised_total_eligible_amount AS original_total_eligible_amount, 
									revised_total_eligible_amount AS latest_amend_total_eligible_amount, 
									revised_total_eligible_amount, 
									revised_quantity AS original_quantity, 
									revised_quantity AS latest_amend_quantity, 
									revised_quantity, 
									original_fuel, 
									original_line_item_description, 
									revised_line_item_description, 
									revised_state_amount AS original_state_amount, 
									revised_state_amount AS latest_amend_state_amount, 
									revised_state_amount, 
									revised_local_amount AS original_local_amount, 
									revised_local_amount AS latest_amend_local_amount, 
									revised_local_amount, 
									revised_other_fed_amount AS original_other_fed_amount, 
									revised_other_fed_amount AS latest_amend_other_fed_amount, 
									revised_other_fed_amount, 
									revised_state_in_kind_amount AS original_state_in_kind_amount, 
									revised_state_in_kind_amount AS latest_amend_state_inkind_amount,
									revised_state_in_kind_amount, 
									revised_local_in_kind_amount AS original_local_in_kind_amount, 
									revised_local_in_kind_amount AS latest_amend_local_inkind_amount, 
									revised_local_in_kind_amount, 
									revised_toll_revenue_amount AS original_toll_revenue_amount, 
									revised_toll_revenue_amount AS latest_amend_toll_rev_amount, 
									revised_toll_revenue_amount, 
									revised_date_time, 
									revised_by, 
									revised_adjustment_amount AS original_adjustment_amount, 
									revised_adjustment_amount AS latest_amend_adjustment_amount, 
									revised_adjustment_amount,
									thirdPartyFlag
								FROM trams_line_item_change_log
								WHERE line_item_id = e AND budget_revision_id = _OriginalBudgetRevisionId ORDER BY id;
							END LOOP;


			END BLOCK2;
			
	      /* Create inserts for trams_project_environmental_finding */
	    SET _TableName = "trams_project_environmental_finding";
		INSERT INTO trams_project_environmental_finding(
        project_id, class_level_name, class_description, categorical_exclusion_name, categorical_exclusion_description, comments, 
        created_date_time, created_by, last_modified_date_time, last_modified_by)
		SELECT 
        b AS project_id, 
        class_level_name, 
        class_description, 
        categorical_exclusion_name, 
        categorical_exclusion_description, 
        comments, 
        UTC_TIMESTAMP() AS created_date_time, 
        (SELECT modified_by FROM trams_project WHERE id = b) AS created_by, 
        UTC_TIMESTAMP() AS last_modified_date_time, 
        (SELECT modified_by FROM trams_project WHERE id = b) AS last_modified_by
		FROM trams_project_environmental_finding WHERE project_id = a order by id;
		
			      /* Create inserts for trams_project_environmental_finding_date */
	    SET _TableName = "trams_project_environmental_finding_date";
		BLOCK3: Begin
                declare no_more_rows3 boolean DEFAULT FALSE;

				DECLARE oldEnvFinding CURSOR FOR SELECT id FROM trams_project_environmental_finding WHERE project_id = a;
				DECLARE newEnvFinding CURSOR FOR SELECT id FROM trams_project_environmental_finding WHERE project_id = b;
                declare continue handler for not found
					SET no_more_rows3 =TRUE;
					
				SET no_more_rows3 = FALSE;
                open oldEnvFinding;
				open newEnvFinding;
				read_loop3: LOOP
								FETCH oldEnvFinding INTO g;
								FETCH newEnvFinding INTO h;
								IF no_more_rows3 THEN 
									CLOSE oldEnvFinding;
									CLOSE newEnvFinding;
									LEAVE read_loop3;
								END IF;
								INSERT INTO trams_project_environmental_finding_date(
								project_finding_id, finding_date_time, finding_date_type, created_date_time, created_by, last_modified_date_time, last_modified_by)
								SELECT 
									h AS project_finding_id, 
									finding_date_time, 
									finding_date_type, 
									created_date_time, 
									(SELECT created_by FROM trams_project_environmental_finding WHERE id = a) AS created_by, 
									(SELECT last_modified_date_time FROM trams_project_environmental_finding WHERE id = a) AS last_modified_date_time, 
									(SELECT last_modified_by FROM trams_project_environmental_finding WHERE id = a) AS last_modified_by
								FROM trams_project_environmental_finding_date WHERE project_finding_id = g order by id;
								
								/* Create inserts for trams_project_env_finding_line_items */
								SET _TableName = "trams_project_env_finding_line_items";			
								SET _OriginalProjectFindingId = NULL;

								SELECT project_finding_id FROM trams_project_env_finding_line_items WHERE project_finding_id = g  INTO _OriginalProjectFindingId;
								SELECT line_item_id FROM trams_project_env_finding_line_items WHERE project_finding_id = g INTO _OriginalFindingLineItemId;
								SELECT original_line_item_id FROM trams_line_item WHERE id = _OriginalFindingLineItemId INTO _OriginalLineItemId;
								/*SET done = FALSE;*/

								/*INSERT INTO tramsPFLDebug(output, oldEnvFindingId, newEnvFindingId) VALUES (_OriginalProjectFindingId, g, h);*/

								IF _OriginalProjectFindingId IS NOT NULL THEN
      
									INSERT INTO trams_project_env_finding_line_items(
									line_item_id, project_finding_id)
									VALUES 
									(
										(SELECT id FROM trams_line_item WHERE original_line_item_id = _OriginalLineItemId AND app_id = o_NewAppId), 
										h
									);

								END IF;
							END LOOP;


			END BLOCK3;
		    /* Create new inserts for project scope */
		SET _TableName = "trams_scope";
		INSERT INTO trams_scope(
        scope_code,  scope_name,  scope_description,  application_id,  project_id,  scope_suffix,  po_number,  funding_source_name, from_team_flag, 
        original_quantity,  latest_amend_quantity,  revised_quantity,  original_fta_amount,  latest_amend_fta_amount,  
        revised_fta_amount,  original_total_eligible_cost,  latest_amend_total_eligible_cost,  revised_total_eligible_cost,  
        original_state_amount,  latest_amend_state_amount,  revised_state_amount,  original_local_amount,  latest_amend_local_amount,  revised_local_amount,  
        original_other_fed_amount,  latest_amend_other_fed_amount,  revised_other_fed_amount,  original_state_in_kind_amount,  latest_amend_state_in_kind_amount,  
        revised_state_in_kind_amount,  latest_amend_local_in_kind_amount,  original_local_in_kind_amount,  revised_local_in_kind_amount,  
        original_toll_revenue_amount,  latest_amend_toll_revenue_amount,  revised_toll_revenue_amount, original_adjustment_amount, latest_amend_adjustment_amount,
        revised_adjustment_amount, other_budget)
		SELECT 
			scope_code, 
			scope_name, 
			scope_description, 
			o_NewAppId AS application_id, 
			b AS project_id, 
			scope_suffix, 
			po_number, 
			funding_source_name,
			from_team_flag, 
			revised_quantity AS original_quantity,
			revised_quantity AS latest_amend_quantity, 
			revised_quantity,
			revised_fta_amount AS original_fta_amount,
			revised_fta_amount AS latest_amend_fta_amount,  
			revised_fta_amount,
			revised_total_eligible_cost AS original_total_eligible_cost,
			revised_total_eligible_cost AS latest_amend_total_eligible_cost,
			revised_total_eligible_cost,  
			revised_state_amount AS original_state_amount,
			revised_state_amount AS latest_amend_state_amount,
			revised_state_amount,
			revised_local_amount AS original_local_amount,
			revised_local_amount AS latest_amend_local_amount,
			revised_local_amount,  
			revised_other_fed_amount AS original_other_fed_amount,
			revised_other_fed_amount AS latest_amend_other_fed_amount, 
			revised_other_fed_amount,  
			revised_state_in_kind_amount AS original_state_in_kind_amount,  
			revised_state_in_kind_amount AS latest_amend_state_in_kind_amount,  
			revised_state_in_kind_amount,  
			revised_local_in_kind_amount AS latest_amend_local_in_kind_amount,  
			revised_local_in_kind_amount AS original_local_in_kind_amount,  
			revised_local_in_kind_amount,  
			revised_toll_revenue_amount AS original_toll_revenue_amount,  
			revised_toll_revenue_amount AS latest_amend_toll_revenue_amount,  
			revised_toll_revenue_amount, 
			revised_adjustment_amount AS original_adjustment_amount, 
			revised_adjustment_amount AS latest_amend_adjustment_amount,
			revised_adjustment_amount, 
			other_budget
		FROM trams_scope WHERE project_id = a order by id;
		
		/* Update scope id for new line items */
    SET _TableName = "Update Scope Id for New Line Items";
				BLOCK4: Begin
                declare no_more_rows4 boolean DEFAULT FALSE;
                /*Scope Id Cursor*/
				DECLARE oldScope CURSOR FOR SELECT id FROM trams_scope WHERE project_id = a ORDER BY scope_suffix;
				DECLARE newScope CURSOR FOR SELECT id FROM trams_scope WHERE project_id = b ORDER BY scope_suffix;
                declare continue handler for not found
					SET no_more_rows4 =TRUE;
					
				SET no_more_rows4 = FALSE;
                open oldScope;
				open newScope;
				read_loop4: LOOP
								FETCH oldScope INTO i;
								FETCH newScope INTO j;
								IF no_more_rows4 THEN 
									CLOSE oldScope;
									CLOSE newScope;
									LEAVE read_loop4;
								END IF;
								UPDATE trams_line_item SET scope_id = j WHERE scope_id = i AND project_id = b;
							END LOOP;


			END BLOCK4;
	
	
    END LOOP;

    CLOSE oldProj;
    CLOSE newProj;
    
    SET o_Completed = 1;

  COMMIT;
 
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsObligationsByAmendmentReport`(
    IN p_begin_date DATE,
    IN p_end_date DATE
  )
BEGIN

    DECLARE _SQLStateCode CHAR(5) DEFAULT '00000';
	  DECLARE _ErrorNumber INTEGER;
	  DECLARE _MessageText VARCHAR(1000);

    DECLARE v_fiscal_year VARCHAR(4);

    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
			
      GET DIAGNOSTICS CONDITION 1 _SQLStateCode = RETURNED_SQLSTATE,
          _ErrorNumber = MYSQL_ERRNO,
          _MessageText = MESSAGE_TEXT;
          IF _ErrorNumber <> '00000' THEN
            CALL trams_log(@sid, 'sptramsObligationsByAmendmentReport', CONCAT(_SQLStateCode, '', _ErrorNumber, '', _MessageText), 3);
            COMMIT;
          END IF;
		END;

    SET v_fiscal_year = trams_get_current_fiscal_year(NOW());
    
    SET @uuid = UUID_SHORT();
    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f')); 

    INSERT INTO trams_temp_CFDATable (application_id, CFDA_number, total_amount, funding_source_name, uuid, time_stamp)
        SELECT
          ta.id AS application_Id,
          fdaNum.cfda_program_number AS CFDA_number,
          SUM(tpn.amendment_Obligation_amount) AS total_amount,
          tfsr.funding_source_name AS funding_source_name,
      @uuid AS uuid,
      @sid AS time_stamp
        FROM trams_application ta
          JOIN trams_po_number tpn                 ON ta.id = tpn.application_id
          JOIN trams_funding_source_reference tfsr ON tpn.funding_source_name = tfsr.funding_source_name
        AND tfsr.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        AND tfsr.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
          JOIN trams_fda_number fdaNum             ON fdaNum.fed_domestic_asst_num = tfsr.fed_dom_assistance_num
          JOIN trams_application_award_detail aa   ON ta.id = aa.application_id
        WHERE
          ta.fiscal_year <= v_fiscal_year
          AND (DATE(aa.obligated_date_time) BETWEEN p_begin_date AND p_end_date OR DATE(aa.fta_closeout_approval_date_time) BETWEEN p_begin_date AND p_end_date)
          GROUP BY
          Application_Id,
          CFDA_number;
    
    INSERT INTO trams_temp_nonFedTotalAmount (application_id, funding_source_name, CFDANumber, local_funding_amount, uuid, time_stamp)
      SELECT
      ta.id AS application_id,
      li.funding_source_name AS funding_source_name,
      fdaNum.cfda_program_number AS CFDANumber,
      IF(ta.amendment_number = 0,
        (SUM(IFNULL(li.revised_total_amount, 0)) - SUM(IFNULL(li.revised_FTA_amount, 0)) - SUM(IFNULL(li.revised_other_fed_amount, 0))),
        ((SUM(IFNULL(li.revised_total_amount, 0)) - SUM(IFNULL(li.revised_FTA_amount, 0)) - SUM(IFNULL(li.revised_other_fed_amount, 0))) -
        (SUM(IFNULL(li.latest_amend_total_amount, 0)) - SUM(IFNULL(li.latest_amend_FTA_amount, 0)) - SUM(IFNULL(li.latest_amend_other_fed_amount, 0))))
      ) AS local_funding_amount,
      @uuid AS uuid,
      @sid AS time_stamp
      FROM trams_application ta
      JOIN trams_application_award_detail aa ON aa.application_id = ta.id
      JOIN trams_line_item li                ON li.app_id  = ta.id
        AND li.other_budget = 0
      JOIN trams_funding_source_reference tfsr ON li.funding_source_name = tfsr.funding_source_name
        AND tfsr.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        AND tfsr.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
      JOIN trams_fda_number fdaNum             ON fdaNum.fed_domestic_asst_num = tfsr.fed_dom_assistance_num
      WHERE
      ta.fiscal_year <= v_fiscal_year
      AND ta.id IN (SELECT application_id FROM trams_temp_CFDATable WHERE uuid = @uuid)
      GROUP BY
      application_id,
      CFDANumber;

      SELECT
      ta.recipient_id AS recipientId,
      IF(NULLIF(ta.recipient_name, ' ') = NULL, 'N/A', ta.recipient_name) AS AwardeeOrRecipientLegalEntityName,
      ta.application_name AS AwardDescription,
      TRIM(TRAILING RIGHT(ta.application_number, 3) FROM ta.application_number) AS FAIN,
      ta.application_number AS appNumber,
      ta.amendment_number AS AwardModificationAmendmentNumber,
      tfsr.funding_source_description AS fundingSourceDescription,
      IFNULL(CFDA.CFDA_number, '00.000') AS CFDA_number,
      tfsr.section_code AS sectionCode,
      CONCAT('$ ', FORMAT(IFNULL(CFDA.total_amount,0), 2)) AS FederalActionObligation,
      CONCAT('$ ', FORMAT(IFNULL(li.local_funding_amount,0), 2)) AS NonFederalFundingAmount,
      IF(NULLIF(aa.fta_closeout_approval_date_time, '') IS NULL, DATE_FORMAT(aa.obligated_date_time, '%m/%d/%Y'), '') AS obligationDate,
      IF(NULLIF(aa.fta_closeout_approval_date_time, '') IS NULL, '', DATE_FORMAT(aa.fta_closeout_approval_date_time, '%m/%d/%Y')) AS closeoutApprovalDate,
      IF(ta.start_date IS NULL OR ta.start_date = '',
        '',
        DATE_FORMAT(ta.start_date, '%m/%d/%Y')
      ) AS PeriodOfPerformanceStartDate,
	  -- Gets the PeriodOfPerformanceCurrentEndDate from a function instead of trams_application
      DATE_FORMAT(fntramsGetApplicationPOPEndDateFinalized(ta.id, ta.contractAward_id), '%m/%d/%Y') AS PeriodOfPerformanceCurrentEndDate
    FROM trams_application ta
      JOIN trams_temp_CFDATable CFDA                      ON CFDA.application_id = ta.id
		AND CFDA.uuid = @uuid
      JOIN trams_application_award_detail aa   ON ta.id = aa.application_id
      JOIN trams_funding_source_reference tfsr ON CFDA.funding_source_name = tfsr.funding_source_name
      JOIN trams_temp_nonFedTotalAmount li                ON li.application_id  = ta.id
        AND li.CFDANumber = CFDA.cfda_number
		AND li.uuid = @uuid
    WHERE
      ta.fiscal_year <= v_fiscal_year
      AND ta.status IN ('Active (Executed)', 'Active Award / Inactive Amendment', 'Active / Budget Revision In-Progress', 'Active / Budget Revision Under Review', 'Obligated / Ready for Execution', 'Closed')
    GROUP BY
      CFDA_number,
      FAIN,
      AwardModificationAmendmentNumber;

    DELETE FROM trams_temp_CFDATable WHERE uuid = @uuid;
    DELETE FROM trams_temp_nonFedTotalAmount WHERE uuid = @uuid;

    CALL trams_log(@sid, 'sptramsObligationsByAmendmentReport', 'Done!', 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_allotment_advice_report_excel`(IN `P_fundingFiscalYear` VARCHAR(50), IN `P_appropriationCode` VARCHAR(50), IN `P_authorizationCode` VARCHAR(50))
    NO SQL
BEGIN

declare _selectStatement VARCHAR(4096);
declare _fromStatement VARCHAR(4096);
declare _whereClause VARCHAR(4096) default " ";
declare _groupBy VARCHAR(4096) default " ";
declare _orderBy VARCHAR(4096);



set _selectStatement = 
  "SELECT
    c.allotment_code_text AS Allotment_Code,
    max(lc.funding_fiscal_year) AS ffy,
    lc.appropriation_code AS Approp_Code,
    lc.authorization_type AS Type_Auth,
    c.code_description AS Description,
    allo.status AS Status,
    sum(c.code_new_amount) AS Amount";

set _fromStatement = 
  " FROM
    trams_allotment_code c
    JOIN trams_allotment_code_lookup lc ON lc.allotment_code_text = c.allotment_code_text
    JOIN trams_allotment_advice allo ON allo.id = c.allotmentAdviceID_int";

set _whereClause = concat(_whereClause, " and allo.status = 'Authorized' ");
set _whereClause = concat(_whereClause, " and c.authorized_flag = 1 ");
set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and lc.funding_fiscal_year in (", NULLIF(P_fundingFiscalYear,''),")"),"")));
set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and lc.appropriation_code in (", NULLIF(P_appropriationCode,''),")"),"")));
set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and lc.authorization_type in (", NULLIF(P_authorizationCode,''),")"),"")));

set _groupBy = 
  " GROUP BY
    c.allotment_code_text";

set _orderBy = 
  " ORDER BY
  c.allotment_code_text";
    
set @query = concat(_selectStatement, _fromStatement,_whereClause, _groupBy, _orderBy);
PREPARE report FROM @query;
EXECUTE report;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_fix_award_detail_with_non_add_scopes`(
  IN p_application_id int(11)
)
BEGIN
  
  

  

  DROP TABLE IF EXISTS `trams_tmp_team_line_item_rollup_on_application_id`;

  CREATE TEMPORARY TABLE `trams_tmp_team_line_item_rollup_on_application_id`
  SELECT p_application_id as application_id,
    
         ifnull(sum(l.`revised_total_amount`), 0) - ifnull(sum(l.`latest_amend_total_amount`), 0)
         as total_eligible_cost_amount,

         ifnull(sum(l.`revised_FTA_amount`), 0) - ifnull(sum(l.`latest_amend_fta_amount`), 0)
         as total_fta_amount,
     
         (ifnull(sum(l.`revised_total_amount`), 0) - ifnull(sum(l.`latest_amend_total_amount`), 0)) -
         (ifnull(sum(l.`revised_FTA_amount`), 0) - ifnull(sum(l.`latest_amend_fta_amount`), 0))
         as total_local_amount,

         ifnull(sum(l.`revised_total_amount`), 0) - ifnull(sum(l.`latest_amend_total_amount`), 0) +
         ifnull(a.`total_adjustment_amount`, 0)
         as gross_project_cost_amount,

         ifnull(sum(l.`revised_total_amount`), 0)
         as cumulative_contract_total_eligible_cost_amount,

         ifnull(sum(l.`revised_FTA_amount`), 0)
         as cumulative_contract_fta_amount,

         ifnull(sum(l.`revised_total_amount`), 0) - ifnull(sum(l.`revised_fta_amount`), 0)
         as cumulative_contract_local_amount,

         ifnull(sum(l.`revised_total_amount`), 0) + ifnull(a.`cumulative_contract_adjustment_amount`, 0)
         as cumulative_contract_gross_cost_amount
  FROM `trams_line_item` l
  INNER JOIN `trams_application_award_detail` a ON a.`application_id` = l.`app_id`
  WHERE l.`app_id` = p_application_id
    AND l.`is_line_item` = 1
    AND l.`other_budget` = 0
  GROUP BY l.`app_id`;

  UPDATE `trams_application_award_detail` a
  INNER JOIN `trams_tmp_team_line_item_rollup_on_application_id` l ON l.`application_id` = a.`application_id`
  SET a.`total_eligible_cost_amount` = l.`total_eligible_cost_amount`,
      a.`total_fta_amount` = l.`total_fta_amount`,
      a.`total_local_amount` = l.`total_local_amount`,
      a.`gross_project_cost_amount` = l.`gross_project_cost_amount`,
      a.`cumulative_contract_total_eligible_cost_amount` = l.`cumulative_contract_total_eligible_cost_amount`,
      a.`cumulative_contract_fta_amount` = l.`cumulative_contract_fta_amount`,
      a.`cumulative_contract_local_amount` = l.`cumulative_contract_local_amount`,
      a.`cumulative_contract_gross_cost_amount` = l.`cumulative_contract_gross_cost_amount`
  WHERE a.`application_id` = p_application_id;

  DROP TABLE IF EXISTS `trams_tmp_team_line_item_rollup_on_application_id`;

  

  DROP TABLE IF EXISTS `trams_tmp_team_line_item_rollup_on_scope_code`;

  CREATE TEMPORARY TABLE `trams_tmp_team_line_item_rollup_on_scope_code`
  SELECT p_application_id as application_id,
         `scope_code` as scope_code,
         `other_budget` as other_budget,

         ifnull(sum(`quantity`), 0) as original_quantity,
         ifnull(sum(`latest_amend_quantity`), 0) as latest_amend_quantity,
         ifnull(sum(`latest_revised_quantity`), 0) as revised_quantity,
         
         ifnull(sum(`total_amount`), 0)  as original_fta_amount,
         ifnull(sum(`latest_amend_total_amount`), 0) as latest_amend_fta_amount,
         ifnull(sum(`revised_FTA_amount`), 0) as revised_fta_amount,
         
         ifnull(sum(`total_amount`), 0) as original_total_eligible_cost,
         ifnull(sum(`latest_amend_total_amount`), 0) as latest_amend_total_eligible_cost,
         ifnull(sum(`revised_total_amount`), 0) as revised_total_eligible_cost,
         
         ifnull(sum(`original_state_amount`), 0) as original_state_amount,
         ifnull(sum(`latest_amend_state_amount`), 0) as latest_amend_state_amount,
         ifnull(sum(`revised_state_amount`), 0) as revised_state_amount,
         
         ifnull(sum(`original_local_amount`), 0) as original_local_amount,
         ifnull(sum(`latest_amend_local_amount`), 0) as latest_amend_local_amount,
         ifnull(sum(`revised_local_amount`), 0) as revised_local_amount,
         
         ifnull(sum(`original_other_fed_amount`), 0) as original_other_fed_amount,
         ifnull(sum(`latest_amend_other_fed_amount`), 0) as latest_amend_other_fed_amount,
         ifnull(sum(`revised_other_fed_amount`), 0) as revised_other_fed_amount,
         
         ifnull(sum(`original_state_in_kind_amount`), 0) as original_state_in_kind_amount,
         ifnull(sum(`latest_amend_state_inkind_amount`), 0) as latest_amend_state_in_kind_amount,
         ifnull(sum(`revised_state_in_kind_amount`), 0) as revised_state_in_kind_amount,
         
         ifnull(sum(`original_local_in_kind_amount`), 0) as latest_amend_local_in_kind_amount,
         ifnull(sum(`latest_amend_local_inkind_amount`), 0) as original_local_in_kind_amount,
         ifnull(sum(`revised_local_in_kind_amount`), 0) as revised_local_in_kind_amount,
         
         ifnull(sum(`original_toll_revenue_amount`), 0) as original_toll_revenue_amount,
         ifnull(sum(`latest_amend_toll_rev_amount`), 0) as latest_amend_toll_revenue_amount,
         ifnull(sum(`revised_toll_revenue_amount`), 0) as revised_toll_revenue_amount,
         
         ifnull(sum(`original_adjustment_amount`), 0) as original_adjustment_amount,
         ifnull(sum(`latest_amend_adjustment_amount`), 0) as latest_amend_adjustment_amount,
         ifnull(sum(`revised_adjustment_amount`), 0) as revised_adjustment_amount
  FROM `trams_line_item`
  WHERE `app_id` = p_application_id
  GROUP BY `scope_code`, `other_budget`;

  UPDATE `trams_scope` s
  INNER JOIN `trams_tmp_team_line_item_rollup_on_scope_code` l ON l.`application_id` = s.`application_id`
    AND l.`scope_code` = s.`scope_code`
    AND l.`other_budget` = s.`other_budget`
  SET s.`original_quantity` = l.`original_quantity`,
      s.`latest_amend_quantity` = l.`latest_amend_quantity`,
      s.`revised_quantity` = l.`revised_quantity`,
      
      s.`original_fta_amount` = l.`original_fta_amount`,
      s.`latest_amend_fta_amount` = l.`latest_amend_fta_amount`,
      s.`revised_fta_amount` = l.`revised_fta_amount`,
      
      s.`original_total_eligible_cost` = l.`original_total_eligible_cost`,
      s.`latest_amend_total_eligible_cost` = l.`latest_amend_total_eligible_cost`,
      s.`revised_total_eligible_cost` = l.`revised_total_eligible_cost`,
      
      s.`original_state_amount` = l.`original_state_amount`,
      s.`latest_amend_state_amount` = l.`latest_amend_state_amount`,
      s.`revised_state_amount` = l.`revised_state_amount`,
      
      s.`original_local_amount` = l.`original_local_amount`,
      s.`latest_amend_local_amount` = l.`latest_amend_local_amount`,
      s.`revised_local_amount` = l.`revised_local_amount`,
      
      s.`original_other_fed_amount` = l.`original_other_fed_amount`,
      s.`latest_amend_other_fed_amount` = l.`latest_amend_other_fed_amount`,
      s.`revised_other_fed_amount` = l.`revised_other_fed_amount`,
      
      s.`original_state_in_kind_amount` = l.`original_state_in_kind_amount`,
      s.`latest_amend_state_in_kind_amount` = l.`latest_amend_state_in_kind_amount`,
      s.`revised_state_in_kind_amount` = l.`revised_state_in_kind_amount`,
      
      s.`latest_amend_local_in_kind_amount` = l.`latest_amend_local_in_kind_amount`,
      s.`original_local_in_kind_amount` = l.`original_local_in_kind_amount`,
      s.`revised_local_in_kind_amount` = l.`revised_local_in_kind_amount`,
      
      s.`original_toll_revenue_amount` = l.`original_toll_revenue_amount`,
      s.`latest_amend_toll_revenue_amount` = l.`latest_amend_toll_revenue_amount`,
      s.`revised_adjustment_amount` = l.`revised_adjustment_amount`,
      
      s.`original_adjustment_amount` = l.`original_adjustment_amount`,
      s.`latest_amend_adjustment_amount` = l.`latest_amend_adjustment_amount`,
      s.`revised_adjustment_amount` = l.`revised_adjustment_amount`
  WHERE s.`application_id` = p_application_id;

  DROP TABLE IF EXISTS `trams_tmp_team_line_item_rollup_on_scope_code`;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_allotment_advice_summary_report_excel`(IN `P_allotmentAdvice` VARCHAR(50), IN `P_allotmentType` VARCHAR(50), IN `P_fiscalYear` VARCHAR(50))
    NO SQL
BEGIN

	declare _selectStatement VARCHAR(4096);
	declare _fromStatement VARCHAR(4096);
	declare _orderBy VARCHAR(4096);
	declare _whereClause VARCHAR(4096) default " ";


	set _selectStatement = 
	"SELECT
		allo.control_number AS Allotment_Advice,
		co.allotment_type_text AS Allotment_Type,
		co.allotment_code_text AS Allotment_Code,
		co.code_description AS Description,
		concat('$', FORMAT(co.code_previous_amount, 2)) AS Previous_Amount,
		concat('$', FORMAT(co.code_change_amount, 2)) AS Change_Amount,
		concat('$', FORMAT(co.code_new_amount, 2)) AS New_Amount";
		
	set _fromStatement = 
	" FROM trams_allotment_code co
		 INNER JOIN trams_allotment_advice allo ON allo.id = co.allotmentAdviceID_int";
	
	set _whereClause = concat(_whereClause, (" and co.code_change_amount != 0 " ));
	set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and allo.control_number LIKE '%", NULLIF(P_allotmentAdvice,''),"%'"),"")));
	set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and co.allotment_type_text = '", NULLIF(P_allotmentType,''),"'"),"")));
	set _whereClause = concat(_whereClause, (SELECT IFNULL(concat(" and allo.fiscal_year = '", NULLIF(P_fiscalYear,''),"'"),"")));
	
	set _orderBy = 
	" Order By
		allo.control_number,
		co.allotment_code_text";
	
	set @query = concat(_selectStatement, _fromStatement, _whereClause, _orderBy);
	PREPARE report FROM @query;
	EXECUTE report;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_fms_reverse_deobligation_transaction`(
  IN p_application_id int(11),
  IN p_deob_transaction_id int(11)
)
BEGIN

  
  
  DECLARE _current_fiscal_year varchar(255);

  SELECT trams_get_current_fiscal_year(now()) INTO _current_fiscal_year;

  

  INSERT INTO `trams_contract_acc_transaction`(
    `contract_award_id`,
    `recipient_id`,
    `project_id`,
    `application_id`,
    `application_number`,
    `project_number`,
    `echo_number`,
    `transaction_fiscal_year`,
    `uza_code`,
    `scope_code`,
    `scope_name`,
    `account_class_code`,
    `fpc`,
    `cost_center_code`,
    `funding_source_name`,
    `funding_fiscal_year`,
    `type_authority`,
    `transaction_type`,
    `appropriation_code`,
    `section_code`,
    `limitation_code`,
    `transaction_date_time`,
    `transaction_by`,
    `transaction_doc_num`,
    `trams_entry_date_time`,
    `echo_payment_identifier`,
    `object_class`,
    `dafis_suffix_code`,
    `obligation_amount`,
    `deobligation_amount`,
    `disbursement_amount`,
    `refund_amount`,
    `authorized_disbursement_amount`,
    `run_number`,
    `transaction_code`,
    `transaction_status_code`,
    `app_amendment_number`,
    `fpc_description`,
    `po_number`,
    `from_trams`,
    `scope_suffix`,
    `tbp_process_date`,
    `booked`,
    `transaction_description`
  )
  SELECT `contract_award_id`,
         `recipient_id`,
         `project_id`,
         `application_id`,
         `application_number`,
         `project_number`,
         `echo_number`,
         _current_fiscal_year AS `transaction_fiscal_year`,
         `uza_code`,
         `scope_code`,
         `scope_name`,
         `account_class_code`,
         `fpc`,
         `cost_center_code`,
         `funding_source_name`,
         `funding_fiscal_year`,
         `type_authority`,
         'Obligation' AS `transaction_type`,
         `appropriation_code`,
         `section_code`,
         `limitation_code`,
         now() AS `transaction_date_time`,
         'trams.administrator' AS `transaction_by`,
         `transaction_doc_num`,
         now() AS `trams_entry_date_time`,
         `echo_payment_identifier`,
         `object_class`,
         `dafis_suffix_code`,
         `deobligation_amount` AS `obligation_amount`,
         0 AS `deobligation_amount`,
         0 AS `disbursement_amount`,
         0 AS `refund_amount`,
         0 AS `authorized_disbursement_amount`,
         NULL AS `run_number`,
         '051' AS `transaction_code`,
         '1' AS `transaction_status_code`,
         `app_amendment_number`,
         `fpc_description`,
         `po_number`,
         `from_trams`,
         `scope_suffix`,
         NULL `tbp_process_date`,
         0 AS `booked`,
         concat('Reverse closeout deobligation to clear suspended payment from FMS for ', `application_number`, '.') AS `transaction_description`
  FROM `trams_contract_acc_transaction`
  WHERE `application_id` = p_application_id
    AND `id` = p_deob_transaction_id;

  SET @contract_acc_transaction_id = last_insert_id();

  

  INSERT INTO `trams_contract_acc_transaction`(
    `contract_award_id`,
    `recipient_id`,
    `project_id`,
    `application_id`,
    `application_number`,
    `project_number`,
    `echo_number`,
    `transaction_fiscal_year`,
    `uza_code`,
    `scope_code`,
    `scope_name`,
    `account_class_code`,
    `fpc`,
    `cost_center_code`,
    `funding_source_name`,
    `funding_fiscal_year`,
    `type_authority`,
    `transaction_type`,
    `appropriation_code`,
    `section_code`,
    `limitation_code`,
    `transaction_date_time`,
    `transaction_by`,
    `transaction_doc_num`,
    `trams_entry_date_time`,
    `echo_payment_identifier`,
    `object_class`,
    `dafis_suffix_code`,
    `obligation_amount`,
    `deobligation_amount`,
    `disbursement_amount`,
    `refund_amount`,
    `authorized_disbursement_amount`,
    `run_number`,
    `transaction_code`,
    `transaction_status_code`,
    `app_amendment_number`,
    `fpc_description`,
    `po_number`,
    `from_trams`,
    `scope_suffix`,
    `tbp_process_date`,
    `booked`,
    `transaction_description`
  )
  SELECT `contract_award_id`,
         `recipient_id`,
         `project_id`,
         `application_id`,
         `application_number`,
         `project_number`,
         `echo_number`,
         _current_fiscal_year AS `transaction_fiscal_year`,
         `uza_code`,
         `scope_code`,
         `scope_name`,
         `account_class_code`,
         `fpc`,
         `cost_center_code`,
         `funding_source_name`,
         `funding_fiscal_year`,
         `type_authority`,
         'Authorized Disbursement' AS `transaction_type`,
         `appropriation_code`,
         `section_code`,
         `limitation_code`,
         now() AS `transaction_date_time`,
         'trams.administrator' AS `transaction_by`,
         `transaction_doc_num`,
         now() AS `trams_entry_date_time`,
         `echo_payment_identifier`,
         `object_class`,
         `dafis_suffix_code`,
         0 AS `obligation_amount`,
         0 AS `deobligation_amount`,
         0 AS `disbursement_amount`,
         0 AS `refund_amount`,
         `deobligation_amount` AS `authorized_disbursement_amount`,
         NULL AS `run_number`,
         '024' AS `transaction_code`,
         '1' AS `transaction_status_code`,
         `app_amendment_number`,
         `fpc_description`,
         `po_number`,
         `from_trams`,
         `scope_suffix`,
         NULL `tbp_process_date`,
         0 AS `booked`,
         concat('Reverse closeout deobligation to clear suspended payment from FMS for ', `application_number`, '.') AS `transaction_description`
  FROM `trams_contract_acc_transaction`
  WHERE `application_id` = p_application_id
    AND `id` = p_deob_transaction_id;

  
  
  INSERT INTO `trams_apportionment_history` (
    `uza_code`,
    `apportionment_id`,
    `funding_fiscal_year`,
    `section_id`,
    `limitation_code`,
    `appropriation_code`,
    `transaction_number`,
    `action_type`,
    `created_date_time`,
    `created_by`,
    `comments`,
    `amount`,
    `ceiling_amount`,
    `cap_amount`,
    `reserved_amount`,
    `unreserve_amount`,
    `obligated_amount`,
    `recovery_amount`,
    `transfer_in_amount`,
    `transfer_out_amount`,
    `modified_amount`,
    `split_amount`,
    `transaction_fiscal_year`,
    `cap_reserved_amt`,
    `cap_unreserved_amt`,
    `cap_obligated_amt`,
    `cap_recovery_amt`,
    `cap_transfer_in_amt`,
    `cap_transfer_out_amt`,
    `cap_modify_amt`,
    `ceiling_recovery_amt`,
    `ceiling_transfer_in_amt`,
    `ceiling_transfer_out_amt`,
    `ceiling_modify_amt`,
    `ceiling_reserved_amt`,
    `ceiling_unreserve_amt`,
    `ceiling_obligated_amt`,
    `storage_amount`,
    `cap_storage_amount`,
    `extended_lapse_year`
  )
  SELECT `uza_code`,
         NULL AS `apportionment_id`,
         `funding_fiscal_year`,
         `section_code` AS `section_id`,
         `limitation_code`,
         `appropriation_code`,
         NULL AS `transaction_number`,
         'Obligated' AS `action_type`,
         now() AS `created_date_time`,
         'trams.administrator' AS `created_by`,
         concat('Reverse closeout deobligation to clear suspended payment from FMS for ', `application_number`, '.') AS `comments`,
         NULL AS `amount`,
         NULL AS `ceiling_amount`,
         NULL AS `cap_amount`,
         NULL AS `reserved_amount`,
         NULL AS `unreserve_amount`,
         `deobligation_amount` AS `obligated_amount`,
         NULL AS `recovery_amount`,
         NULL AS `transfer_in_amount`,
         NULL AS `transfer_out_amount`,
         NULL AS `modified_amount`,
         NULL AS `split_amount`,
         _current_fiscal_year AS `transaction_fiscal_year`,
         NULL AS `cap_reserved_amt`,
         NULL AS `cap_unreserved_amt`,
         NULL AS `cap_obligated_amt`,
         NULL AS `cap_recovery_amt`,
         NULL AS `cap_transfer_in_amt`,
         NULL AS `cap_transfer_out_amt`,
         NULL AS `cap_modify_amt`,
         NULL AS `ceiling_recovery_amt`,
         NULL AS `ceiling_transfer_in_amt`,
         NULL AS `ceiling_transfer_out_amt`,
         NULL AS `ceiling_modify_amt`,
         NULL AS `ceiling_reserved_amt`,
         NULL AS `ceiling_unreserve_amt`,
         NULL AS `ceiling_obligated_amt`,
         NULL AS `storage_amount`,
         NULL AS `cap_storage_amount`,
         NULL AS `extended_lapse_year`
  FROM `trams_contract_acc_transaction`
  WHERE `application_id` = p_application_id
    AND `id` = p_deob_transaction_id;
  
  SET @apportionment_id = last_insert_id();

  

  INSERT INTO `trams_account_class_code_history` (
    `account_class_code_id`,
    `account_class_code`,
    `cost_center_code`,
    `transaction_fiscal_year`,
    `funding_fiscal_year`,
    `section_id`,
    `limitation_code`,
    `appropriation_code`,
    `transaction_number`,
    `action_type`,
    `created_date_time`,
    `created_by`,
    `comments`,
    `amount`,
    `reserved_amount`,
    `unreserve_amount`,
    `obligated_amount`,
    `recovery_amount`,
    `transfer_in_amount`,
    `transfer_out_amount`
  )
  SELECT NULL AS `account_class_code_id`,
         `account_class_code`,
         `cost_center_code`,
         _current_fiscal_year AS `transaction_fiscal_year`,
         `funding_fiscal_year`,
         `section_code` AS `section_id`,
         `limitation_code`,
         `appropriation_code`,
         NULL AS `transaction_number`,
         'Obligated' AS `action_type`,
         now() AS `created_date_time`,
         'trams.administrator' AS `created_by`,
         concat('Reverse closeout deobligation to clear suspended payment from FMS for ', `application_number`, '.') AS `comments`,
         NULL AS `amount`,
         NULL AS `reserved_amount`,
         NULL AS `unreserve_amount`,
         `deobligation_amount` AS `obligated_amount`,
         NULL AS `recovery_amount`,
         NULL AS `transfer_in_amount`,
         NULL AS `transfer_out_amount`
  FROM `trams_contract_acc_transaction`
  WHERE `application_id` = p_application_id
    AND `id` = p_deob_transaction_id;

  SET @account_class_code_id = last_insert_id();

  SELECT * FROM `trams_contract_acc_transaction`
  WHERE `id` >= @contract_acc_transaction_id;

  SELECT * FROM `trams_apportionment_history`
  WHERE `id` >= @apportionment_id;

  SELECT * FROM `trams_account_class_code_history`
  WHERE `id` >= @account_class_code_id;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsSAMStatusReport`(IN p_recipientId VARCHAR(512), IN p_recipientCostCenter VARCHAR(512), IN p_state VARCHAR(512), IN p_recipientLegalName VARCHAR(512), 
IN p_SAMStatus VARCHAR(50), IN p_expirationDateTo VARCHAR(50), IN p_expirationDateFrom VARCHAR(50), IN p_gmtOffset VARCHAR(10))
BEGIN

	/*
     Stored Procedure Name : sptramsSAMStatusReport
     Description : Populate data for TrAMS 'SAM Status Report'
     Updated by  : Garrison Dean (2021.Sep.28)
	*/
	
    DECLARE _whereClause VARCHAR(4096) default "";
	
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and gbi.recipientId in (",NULLIF(p_recipientId, ''),")"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and cc.name in (",NULLIF(p_recipientCostCenter, ''),")"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and loc.stateOrProvince in (",NULLIF(p_state, ''),")"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and gbi.samStatus = '",NULLIF(p_SAMStatus, ''),"'"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and STR_TO_DATE(gbi.samExpirationDate, '%m/%d/%Y') < '",NULLIF(STR_TO_DATE(p_expirationDateTo, '%Y-%m-%d'), ''),"'"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and STR_TO_DATE(gbi.samExpirationDate, '%m/%d/%Y') > '",NULLIF(STR_TO_DATE(p_expirationDateFrom, '%Y-%m-%d'), ''),"'"), "") ));
	SET
	   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and gbi.legalBusinessName LIKE '%", NULLIF(p_recipientLegalName, ''),"%'"), "") ));
	SET
	   @samInformationQuery = CONCAT(" 
	   SELECT 
		gbi.recipientId AS 'recipient_id',
		gbi.duns,
		gbi.legalBusinessName AS 'legal_name',
		cc.name AS 'cost_center',
		loc.stateOrProvince as 'State',
		gbi.tramsStatus AS trams_status,
		gbi.samStatus AS sam_status,
		trams_format_date(gbi.samExpirationDate) AS sam_expiration_date,
		trams_format_date(cast(CONVERT_TZ(gbi.lastManualSyncDate, '+00:00', ", p_gmtOffset, ") AS char)) AS 'sam_sync_date'
		FROM
			vwtramsGeneralBusinessInfo gbi
			LEFT JOIN
				vwtramsLocation loc ON gbi.recipientId = loc.recipientId AND loc.addressType LIKE 'Physical Address'
			LEFT JOIN
				ACS_CostCenter cc ON gbi.costCenterCode = cc.code WHERE 1=1 ", _whereClause);
	
	PREPARE samStatusReport 
	   FROM
		  @samInformationQuery;
	EXECUTE samStatusReport;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_application_award_funding_account_class_code`(IN p_contractAwardId INT, IN p_fromTeam TINYINT(1))
    NO SQL
BEGIN

		DECLARE _havingClause varchar(4096) default " ";
		DECLARE _groupByClause varchar(4096) default " ";


		 SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
		 CALL trams_log(@sid, 'trams_get_application_funding_status', CONCAT('Parameter contract award id: ',p_contractAwardId), 0); 

		SET _havingClause = CONCAT(_havingClause, 
			"SUM(acc.deobligation_amount) <> 0 OR 
			SUM(acc.obligation_amount) <> 0 OR 
			SUM(acc.disbursement_amount) <> 0 OR 
			SUM(acc.refund_amount) <> 0"
		);
		SET _groupByClause = CONCAT(_groupByClause, 
			" acc.po_number,", 
			" acc.cost_center_code,", 
			" acc.account_class_code,", 
			" acc.fpc", 
			(SELECT IF(p_fromTeam = 1, " ", 
				CONCAT(", ", 
						" CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(acc.project_number, '-', 4), LENGTH(SUBSTRING_INDEX(acc.project_number, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER),", 
				" acc.scope_code,", 
				" acc.scope_suffix"
				)
				)
			)
		); 
		
		SET @awardFundingAccQuery = 
		CONCAT('
			SELECT acc.recipient_id AS granteeId_int,
				acc.contract_award_id AS contractAwardId_int,
				acc.application_id AS applicationId_int,
				acc.application_number AS applicationNumber_text,
				IFNULL(acc.app_amendment_number,0) AS amendmentNumber_int,
				acc.project_id AS projectId_int,
				SUBSTRING(acc.project_number, 1, CHAR_LENGTH(acc.project_number) - 3) AS projectNumber_text,
				acc.po_number AS poNumber_text,
				acc.scope_code AS scopeNumber_text,
				acc.scope_name AS scopeName_text,
				acc.scope_suffix AS scopeSuffix_text,
				acc.cost_center_code AS costCenterCode_text,
				acc.account_class_code AS accountClassCode_text,
				acc.funding_fiscal_year AS fundingFiscalYear_text,
				acc.appropriation_code AS appropriationCode_text,
				acc.section_code AS sectionCode_text,
				acc.limitation_code AS limitationCode_text,
				acc.type_authority AS authorizationType_text,
				acc.fpc AS fpc_text,
				acc.funding_source_name AS fundingSourceName_text,
				SUM(acc.obligation_amount) AS obligationAmount_dec,
				SUM(acc.deobligation_amount) AS deobligationAmount_dec,
				SUM(acc.disbursement_amount) AS disbursementAmount_dec,
				SUM(acc.refund_amount) AS refundAmount_dec,
				(((IFNULL(SUM(acc.obligation_amount),0) - IFNULL(SUM(acc.deobligation_amount),0)) - IFNULL(SUM(acc.disbursement_amount),0)) + IFNULL(SUM(acc.refund_amount),0)) AS unliquidAmount_dec 
			FROM 
				trams_contract_acc_transaction acc 
			WHERE 
				acc.contract_award_id= ', p_contractAwardId,
			' GROUP BY ',
				_groupByClause,
			' HAVING ',
				_havingClause
		); 

		CALL trams_log(@sid, 'trams_get_application_funding_status', CONCAT('Get Award Funding - Account Class Code SQL: ', @awardFundingAccQuery), 0); 
		PREPARE awardFundingAccStmt FROM @awardFundingAccQuery;
		EXECUTE awardFundingAccStmt; 
	
	END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_application_funding_status`(IN `p_contractAwardId` INT, IN `p_fromTeam` TINYINT(1))
    NO SQL
BEGIN

declare _havingClause varchar(4096) default " ";
declare _groupByClause varchar(4096) default " ";
declare _groupByFPCClause varchar(4096) default " ";
declare _onClause varchar(4096) default " ";
-- changed data type from date to datetime
declare _appCreation datetime;

/* For Logging*/
set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
CALL `trams_log`(@sid, 'trams_get_application_funding_status', concat('Parameter contract award id: ',p_contractAwardId), 0);

set _havingClause = concat(_havingClause, "sum(`trams_contract_acc_transaction`.`deobligation_amount`)<>0 OR sum(`trams_contract_acc_transaction`.`obligation_amount`) <> 0 OR sum(`trams_contract_acc_transaction`.`disbursement_amount`) <> 0 OR sum(`trams_contract_acc_transaction`.`refund_amount`) <> 0");
set _groupByClause = concat(_groupByClause,
	" `trams_contract_acc_transaction`.`po_number`,",
	" `trams_contract_acc_transaction`.`cost_center_code`,",
	" `trams_contract_acc_transaction`.`account_class_code`,",
	" `trams_contract_acc_transaction`.`fpc`",
	(select if(p_fromTeam = 1, " ",
		concat(", ",
		/* The next line is to group by project sequence number for trams applications. We want to see the cumulative amounts for a project across all amendments.*/
		" CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4), LENGTH(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER),",
		" `trams_contract_acc_transaction`.`scope_code`,",
		" `trams_contract_acc_transaction`.`scope_suffix`"
		)
	  )
	)
);

set _groupByFPCClause = concat(_groupByFPCClause,
	" `trams_contract_acc_transaction`.`fpc`,",
    " `trams_contract_acc_transaction`.`po_number`",
	(select if (p_fromTeam = 1, " ",
	  concat(", ",
	    " CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4), LENGTH(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER)"
		)
	)
  )
);
/*Previous _appCreation was initialized to (SELECT DATE(created_date_time) FROM trams_application WHERE contractAward_id = p_contractAwardId AND amendment_number = 0).
  Replaced the previous initialization with a function to retrieve the date directly.*/
set _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(p_contractAwardId,NULL);
/*Previous on _onClause was initialized to: if(_appCreation<p_fundingDeploy,'`fs`.`version` = 0','`fs`.`latest_flag` = 1')
  Replaced the previous initialization to retrieve the funding source record at the time the funding source latest flag was set to true for the base application.   */
SET _onClause = CONCAT(" fs.LatestFlagStartDate <= '",_appCreation,"' AND fs.LatestFlagEndDate >= '",_appCreation,"'");

/* Award Funding Summary */
SET @awardFundingSummary = concat('select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
`trams_contract_acc_transaction`.`po_number` AS `po_number`,
`trams_contract_acc_transaction`.`funding_source_name` AS `funding_source_short_name`,
`trams_contract_acc_transaction`.`section_code` AS `section_code`,
`fs`.`funding_source_description` AS `fundingSourceName_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from ((`trams_contract_acc_transaction` join `trams_application` `a` on((`trams_contract_acc_transaction`.`application_id` = `a`.`id`)))
	left join `trams_funding_source_reference` `fs` on((`trams_contract_acc_transaction`.`funding_source_name` = `fs`.`funding_source_name` AND',_onClause,	')))
where `trams_contract_acc_transaction`.`contract_award_id`=',p_contractAwardId,
' group by `trams_contract_acc_transaction`.`contract_award_id`,
`trams_contract_acc_transaction`.`funding_source_name`,
`trams_contract_acc_transaction`.`po_number`');

PREPARE awardFundingSummary FROM @awardFundingSummary;
EXECUTE awardFundingSummary;

/* Award Funding - Account Class Code */
/*We strip out the last two digits + '-' of the project number because we want to group by project sequence number and discard whatever amendment number the project is associated with*/
SET @awardFundingAccQuery = concat('
select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`application_number` AS `applicationNumber_text`,
ifnull(`trams_contract_acc_transaction`.`app_amendment_number`,0) AS `amendmentNumber_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
SUBSTRING(`trams_contract_acc_transaction`.`project_number`, 1, CHAR_LENGTH(`trams_contract_acc_transaction`.`project_number`) - 3) AS `projectNumber_text`,
`trams_contract_acc_transaction`.`po_number` AS `poNumber_text`,
`trams_contract_acc_transaction`.`scope_code` AS `scopeNumber_text`,
`trams_contract_acc_transaction`.`scope_name` AS `scopeName_text`,
`trams_contract_acc_transaction`.`scope_suffix` AS `scopeSuffix_text`,
`trams_contract_acc_transaction`.`cost_center_code` AS `costCenterCode_text`,
`trams_contract_acc_transaction`.`account_class_code` AS `accountClassCode_text`,
`trams_contract_acc_transaction`.`funding_fiscal_year` AS `fundingFiscalYear_text`,
`trams_contract_acc_transaction`.`appropriation_code` AS `appropriationCode_text`,
`trams_contract_acc_transaction`.`section_code` AS `sectionCode_text`,
`trams_contract_acc_transaction`.`limitation_code` AS `limitationCode_text`,
`trams_contract_acc_transaction`.`type_authority` AS `authorizationType_text`,
`trams_contract_acc_transaction`.`fpc` AS `fpc_text`,
`trams_contract_acc_transaction`.`funding_source_name` AS `fundingSourceName_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from `trams_contract_acc_transaction`',
'where `trams_contract_acc_transaction`.`contract_award_id`=',p_contractAwardId,
' group by ',
_groupByClause,
' HAVING ',
_havingClause
);

CALL `trams_log`(@sid, 'trams_get_application_funding_status', concat('Get Award Funding - Account Class Code SQL: ', @awardFundingAccQuery), 0);
PREPARE awardFundingAccStmt FROM @awardFundingAccQuery;
EXECUTE awardFundingAccStmt;

/* Award Funding - Financial Purpose Code (FPC) */

SET @FPCFundingQuery = concat("
select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
`trams_contract_acc_transaction`.`po_number` AS `poNumber_text`,
`trams_contract_acc_transaction`.`fpc` AS `fpc_text`,
`trams_contract_acc_transaction`.`fpc_description` AS `fpcDescription_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from `trams_contract_acc_transaction`
where `trams_contract_acc_transaction`.`contract_award_id`=", p_contractAwardId,
" group by ",
_groupByFPCClause
);

PREPARE fpcFundingStmt FROM @FPCFundingQuery;
EXECUTE fpcFundingStmt;

CALL `trams_log`(@sid, 'trams_get_application_funding_status', 'Done', 0);
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsAmendmentErrorLogInsert`(
  IN _SQLStateCode CHAR(5),
  IN _ErrorNumber INT(10),
  IN _MessageText VARCHAR(1000),
  IN _CallingObject VARCHAR(255),
  IN _OldAppId INT(11),
  IN _NewAppId INT(11),
  IN _OldAppNum VARCHAR(510),
  IN _NewAppNum VARCHAR(510),
  IN _TableName VARCHAR(255),
  IN _CreatedBy VARCHAR(255)
)
BEGIN

  -- Create ErrorLog entry
    INSERT INTO tramsAmendmentErrorLog
      (SQLStateCode,
      ErrorNumber,
      MessageText,
      CallingObject,
      OriginalApplicationId,
      NewApplicationId,
      OriginalApplicationNumber,
      NewApplicationNumber,
      TableName,
      CreatedDate,
      CreatedBy)
    VALUES
      (
		_SQLStateCode,
		_ErrorNumber,
		_MessageText,
		_CallingObject,
		_OldAppId,
		_NewAppId,
		_OldAppNum,
		_NewAppNum,
		_TableName,
		NOW(),
		_CreatedBy
      );
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsUndoApprovedBudgetRevision`(
	  IN BudgetRevisionId INT(11),
	  IN UserName VARCHAR(255)
)
BEGIN

/*
* Modified By: Fuadul Islam
* Modified Date: 01/13/2022
*/


	DECLARE ApplicationId int(11);
    DECLARE ApplicationNumber varchar(255);
    DECLARE CurrentRevisionNumber int(11);
    DECLARE PreviousRevisionNumber int(11);

    SELECT `application_id`,`revision_number`, (`revision_number` - 1)
		INTO ApplicationId, CurrentRevisionNumber, PreviousRevisionNumber
	FROM `trams_budget_revision`
	WHERE `id` = BudgetRevisionid;

	SELECT `application_number`
		INTO ApplicationNumber
	FROM `trams_application`
	WHERE `id` = ApplicationId;

    -- Gather the changelogs from the previous budget revision. Use these to overwrite the current changes
	SET @uuid = UUID_SHORT();
    -- sid will be written to the new table as a time stamp to be used for identifying and cleaning up any junk data
    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f')); 
  
	INSERT INTO 
	   trams_temp_change_log_previous_revision
	SELECT 
	      NULL AS trams_temp_change_log_previous_revision_id,
		  line_item_id AS line_item_id,
		  original_line_item_description AS original_line_item_description,
		  revised_line_item_description AS revised_line_item_description,
		  original_quantity AS original_quantity,
		  latest_amend_quantity AS latest_amend_quantity,
		  revised_quantity AS revised_quantity,
		  original_total_eligible_amount AS original_total_eligible_amount,
		  latest_amend_total_eligible_amount AS latest_amend_total_eligible_amount,
		  revised_total_eligible_amount AS revised_total_eligible_amount,
		  original_FTA_amount AS original_FTA_amount,
		  latest_amend_fta_amount AS latest_amend_fta_amount,
		  revised_FTA_amount AS revised_FTA_amount,
		  original_state_amount AS original_state_amount,
		  latest_amend_state_amount AS latest_amend_state_amount,
		  revised_state_amount AS revised_state_amount,
		  original_local_amount AS original_local_amount,
		  latest_amend_local_amount AS latest_amend_local_amount,
		  revised_local_amount AS revised_local_amount,
		  original_other_fed_amount AS original_other_fed_amount,
		  latest_amend_other_fed_amount AS latest_amend_other_fed_amount,
		  revised_other_fed_amount AS revised_other_fed_amount,
		  original_state_in_kind_amount AS original_state_in_kind_amount,
		  latest_amend_state_inkind_amount AS latest_amend_state_inkind_amount,
		  revised_state_in_kind_amount AS revised_state_in_kind_amount,
		  original_local_in_kind_amount AS original_local_in_kind_amount,
		  latest_amend_local_inkind_amount AS latest_amend_local_inkind_amount,
		  revised_local_in_kind_amount AS revised_local_in_kind_amount,
		  original_toll_revenue_amount AS original_toll_revenue_amount,
		  latest_amend_toll_rev_amount AS latest_amend_toll_rev_amount,
		  revised_toll_revenue_amount AS revised_toll_revenue_amount,
		  original_adjustment_amount AS original_adjustment_amount,
		  latest_amend_adjustment_amount AS latest_amend_adjustment_amount,
		  revised_adjustment_amount AS revised_adjustment_amount,
		  thirdPartyFlag AS thirdPartyFlag,
		  @uuid AS uuid,
		  @sid AS time_stamp
	FROM `trams_line_item_change_log`
    WHERE `application_id` = ApplicationId
    AND `revision_number` = PreviousRevisionNumber;

	-- Revert the existing line items to their values from the previous revision using the changelogs
	UPDATE `trams_line_item` AS lineItem
	INNER JOIN `trams_temp_change_log_previous_revision` AS changeLog 
		ON changeLog.`line_item_id` = lineItem.`id` AND changeLog.uuid = @uuid
	SET 
	  lineItem.`extended_budget_description` = changeLog.`original_line_item_description`,
	  lineItem.`revised_line_item_description` = changeLog.`revised_line_item_description`,
	  lineItem.`quantity` = changeLog.`original_quantity`,
	  lineItem.`latest_amend_quantity` = changeLog.`latest_amend_quantity`,
	  lineItem.`latest_revised_quantity` = changeLog.`revised_quantity`,
	  lineItem.`total_amount` = changeLog.`original_total_eligible_amount`,
	  lineItem.`latest_amend_total_amount` = changeLog.`latest_amend_total_eligible_amount`,
	  lineItem.`revised_total_amount` = changeLog.`revised_total_eligible_amount`,
	  lineItem.`original_FTA_amount` = changeLog.`original_FTA_amount`,
	  lineItem.`latest_amend_fta_amount` = changeLog.`latest_amend_fta_amount`,
	  lineItem.`revised_FTA_amount` = changeLog.`revised_FTA_amount`,
	  lineItem.`original_state_amount` = changeLog.`original_state_amount`,
	  lineItem.`latest_amend_state_amount` = changeLog.`latest_amend_state_amount`,
	  lineItem.`revised_state_amount` = changeLog.`revised_state_amount`,
	  lineItem.`original_local_amount` = changeLog.`original_local_amount`,
	  lineItem.`latest_amend_local_amount` = changeLog.`latest_amend_local_amount`,
	  lineItem.`revised_local_amount` = changeLog.`revised_local_amount`,
	  lineItem.`original_other_fed_amount` = changeLog.`original_other_fed_amount`,
	  lineItem.`latest_amend_other_fed_amount` = changeLog.`latest_amend_other_fed_amount`,
	  lineItem.`revised_other_fed_amount` = changeLog.`revised_other_fed_amount`,
	  lineItem.`original_state_in_kind_amount` = changeLog.`original_state_in_kind_amount`,
	  lineItem.`latest_amend_state_inkind_amount` = changeLog.`latest_amend_state_inkind_amount`,
	  lineItem.`revised_state_in_kind_amount` = changeLog.`revised_state_in_kind_amount`,
	  lineItem.`original_local_in_kind_amount` = changeLog.`original_local_in_kind_amount`,
	  lineItem.`latest_amend_local_inkind_amount` = changeLog.`latest_amend_local_inkind_amount`,
	  lineItem.`revised_local_in_kind_amount` = changeLog.`revised_local_in_kind_amount`,
	  lineItem.`original_toll_revenue_amount` = changeLog.`original_toll_revenue_amount`,
	  lineItem.`latest_amend_toll_rev_amount` = changeLog.`latest_amend_toll_rev_amount`,
	  lineItem.`revised_toll_revenue_amount` = changeLog.`revised_toll_revenue_amount`,
	  lineItem.`original_adjustment_amount` = changeLog.`original_adjustment_amount`,
	  lineItem.`latest_amend_adjustment_amount` = changeLog.`latest_amend_adjustment_amount`,
	  lineItem.`revised_adjustment_amount` = changeLog.`revised_adjustment_amount`,
	  lineItem.`third_party_flag` = changeLog.`thirdPartyFlag`;

	-- Revert any newly created line items to their original null values
    UPDATE `trams_line_item` lineItem
      SET 
	  lineItem.`extended_budget_description` = NULL,
      lineItem.`revised_line_item_description` = NULL,
      lineItem.`quantity` = NULL,
      lineItem.`latest_amend_quantity` = NULL,
      lineItem.`latest_revised_quantity` = NULL,
      lineItem.`total_amount` = NULL,
      lineItem.`latest_amend_total_amount` = NULL,
      lineItem.`revised_total_amount` = NULL,
      lineItem.`original_FTA_amount` = NULL,
      lineItem.`latest_amend_fta_amount` = NULL,
      lineItem.`revised_FTA_amount` = NULL,
      lineItem.`original_state_amount` = NULL,
      lineItem.`latest_amend_state_amount` = NULL,
      lineItem.`revised_state_amount` = NULL,
      lineItem.`original_local_amount` = NULL,
      lineItem.`latest_amend_local_amount` = NULL,
      lineItem.`revised_local_amount` = NULL,
      lineItem.`original_other_fed_amount` = NULL,
      lineItem.`latest_amend_other_fed_amount` = NULL,
      lineItem.`revised_other_fed_amount` = NULL,
      lineItem.`original_state_in_kind_amount` = NULL,
      lineItem.`latest_amend_state_inkind_amount` = NULL,
      lineItem.`revised_state_in_kind_amount` = NULL,
      lineItem.`original_local_in_kind_amount` = NULL,
      lineItem.`latest_amend_local_inkind_amount` = NULL,
      lineItem.`revised_local_in_kind_amount` = NULL,
      lineItem.`original_toll_revenue_amount` = NULL,
      lineItem.`latest_amend_toll_rev_amount` = NULL,
      lineItem.`revised_toll_revenue_amount` = NULL,
      lineItem.`original_adjustment_amount` = NULL,
      lineItem.`latest_amend_adjustment_amount` = NULL,
      lineItem.`revised_adjustment_amount` = NULL,
	  lineItem.`third_party_flag` = NULL
      WHERE 
	     `app_id` = ApplicationId
      AND 
	     `revision_number` = CurrentRevisionNumber;
  
	-- Group the newly updated line items by scope suffix. This SELECT statement was previously used to generate/populate the temp table. Now the results of the select are inserted INTO the new permanent table.
 
	INSERT INTO 
		trams_temp_roll_up_previous_revision
	SELECT 
		NULL AS trams_temp_roll_up_previous_revision_id,
		scope_id AS `scope_id`,
		SUM(IFNULL(`quantity`, 0)) AS `Quantity`,
		SUM(IFNULL(`latest_amend_quantity`, 0)) AS `LatestAmendmentQuantity`,
		SUM(IFNULL(`latest_revised_quantity`, 0)) AS `LatestRevisedQuantity`,
		SUM(IFNULL(`total_amount`, 0)) AS `TotalAmount`,
		SUM(IFNULL(`latest_amend_total_amount`, 0)) AS `LatestAmendmentTotalAmount`,
		SUM(IFNULL(`revised_total_amount`, 0)) AS `RevisedTotalAmount`,
		SUM(IFNULL(`original_FTA_amount`, 0)) AS `OriginalFTAAmount`,
		SUM(IFNULL(`latest_amend_fta_amount`, 0)) AS `LatestAmendmentFTAAmount`,
		SUM(IFNULL(`revised_FTA_amount`, 0)) AS `RevisedFTAAmount`,
		SUM(IFNULL(`original_state_amount`, 0)) AS `OriginalStateAmount`,
		SUM(IFNULL(`latest_amend_state_amount`, 0)) AS `LatestAmendmentStateAmount`,
		SUM(IFNULL(`revised_state_amount`, 0)) AS `RevisedStateAmount`,
		SUM(IFNULL(`original_local_amount`, 0)) AS `OriginalLocalAmount`,
		SUM(IFNULL(`latest_amend_local_amount`, 0)) AS `LatestAmendmentLocalAmount`,
		SUM(IFNULL(`revised_local_amount`, 0)) AS `RevisedLocalAmount`,
		SUM(IFNULL(`original_other_fed_amount`, 0)) AS `OriginalOtherFederalAmount`,
		SUM(IFNULL(`latest_amend_other_fed_amount`, 0)) AS `LatestAmendmentOtherFederalAmount`,
		SUM(IFNULL(`revised_other_fed_amount`, 0)) AS `RevisedOtherFederalAmount`,
		SUM(IFNULL(`original_state_in_kind_amount`, 0)) AS `OriginalStateInKindAmount`,
		SUM(IFNULL(`latest_amend_state_inkind_amount`, 0)) AS `LatestAmendmentStateInKindAmount`,
		SUM(IFNULL(`revised_state_in_kind_amount`, 0)) AS `RevisedStateInKindAmount`,
		SUM(IFNULL(`original_local_in_kind_amount`, 0)) AS `OriginalLocalInKindAmount`,
		SUM(IFNULL(`latest_amend_local_inkind_amount`, 0)) AS `LatestAmendmentLocalInKindAmount`,
		SUM(IFNULL(`revised_local_in_kind_amount`, 0)) AS `RevisedLocalInKindAmount`,
		SUM(IFNULL(`original_toll_revenue_amount`, 0)) AS `OriginalTollRevenueAmount`,
		SUM(IFNULL(`latest_amend_toll_rev_amount`, 0)) AS `LatestAmendmentTollRevenueAmount`,
		SUM(IFNULL(`revised_toll_revenue_amount`, 0)) AS `RevisedTollRevenueAmount`,
		SUM(IFNULL(`original_adjustment_amount`, 0)) AS `OriginalAdjustmentAmount`,
		SUM(IFNULL(`latest_amend_adjustment_amount`, 0)) AS `LatestAmendmentAdjustmentAmount`,
		SUM(IFNULL(`revised_adjustment_amount`, 0)) AS `RevisedAdjustmentAmount`,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM 
	   `trams_line_item`
	WHERE 
	   `app_id` = ApplicationId
	GROUP BY 
	   `scope_id`;

	-- Update the scope records based on the newly updated line items
	UPDATE `trams_scope` scp
     INNER JOIN `trams_temp_roll_up_previous_revision` lineItem 
	   ON lineItem.`scope_id` = scp.`id` AND lineItem.uuid = @uuid
     SET 
	  scp.`original_quantity` = lineItem.`Quantity`,
	  scp.`latest_amend_quantity` = lineItem.`LatestAmendmentQuantity`,
	  scp.`revised_quantity` = lineItem.`LatestRevisedQuantity`,
	  scp.`original_total_eligible_cost` = lineItem.`TotalAmount`,
	  scp.`latest_amend_total_eligible_cost` = lineItem.`LatestAmendmentTotalAmount`,
	  scp.`revised_total_eligible_cost` = lineItem.`RevisedTotalAmount`,
	  scp.`original_fta_amount` = lineItem.`OriginalFTAAmount`,
	  scp.`latest_amend_fta_amount` = lineItem.`LatestAmendmentFTAAmount`,
	  scp.`revised_fta_amount` = lineItem.`RevisedFTAAmount`,
	  scp.`original_state_amount` = lineItem.`OriginalStateAmount`,
	  scp.`latest_amend_state_amount` = lineItem.`LatestAmendmentStateAmount`,
	  scp.`revised_state_amount` = lineItem.`RevisedStateAmount`,
	  scp.`original_local_amount` = lineItem.`OriginalLocalAmount`,
	  scp.`latest_amend_local_amount` = lineItem.`LatestAmendmentLocalAmount`,
	  scp.`revised_local_amount` = lineItem.`RevisedLocalAmount`,
	  scp.`original_other_fed_amount` = lineItem.`OriginalOtherFederalAmount`,
	  scp.`latest_amend_other_fed_amount` = lineItem.`LatestAmendmentOtherFederalAmount`,
	  scp.`revised_other_fed_amount` = lineItem.`RevisedOtherFederalAmount`,
	  scp.`original_state_in_kind_amount` = lineItem.`OriginalStateInKindAmount`,
	  scp.`latest_amend_state_in_kind_amount` = lineItem.`LatestAmendmentStateInKindAmount`,
	  scp.`revised_state_in_kind_amount` = lineItem.`RevisedStateInKindAmount`,
	  scp.`original_local_in_kind_amount` = lineItem.`OriginalLocalInKindAmount`,
	  scp.`latest_amend_local_in_kind_amount` = lineItem.`LatestAmendmentLocalInKindAmount`,
	  scp.`revised_local_in_kind_amount` = lineItem.`RevisedLocalInKindAmount`,
	  scp.`original_toll_revenue_amount` = lineItem.`OriginalTollRevenueAmount`,
	  scp.`latest_amend_toll_revenue_amount` = lineItem.`LatestAmendmentTollRevenueAmount`,
	  scp.`revised_toll_revenue_amount` = lineItem.`RevisedTollRevenueAmount`,
	  scp.`original_adjustment_amount` = lineItem.`OriginalAdjustmentAmount`,
	  scp.`latest_amend_adjustment_amount` = lineItem.`LatestAmendmentAdjustmentAmount`,
	  scp.`revised_adjustment_amount` = lineItem.`RevisedAdjustmentAmount`;

	-- Mark the budget revision as pending, NULL the approved date field
    UPDATE `trams_budget_revision`
    SET `revision_status` = 'Pending',
        `approved_by` = NULL,
        `approved_date` = NULL
    WHERE `id` = BudgetRevisionid;

	-- Write to the audit table to keep track of who performed the action and waht the action was
	INSERT INTO `trams_audit_history`(`context_id`,`context_details`,`action_taken_by`,`action_date_time`,`action_description`)
         VALUES (ApplicationId,'Application', UserName,NOW(),CONCAT('Budget Revision returned to recipient for application ', ApplicationNumber, '.')
    );

	SET @AuditHistoryId = LAST_INSERT_ID();

	-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
    DELETE FROM trams_temp_change_log_previous_revision WHERE uuid = @uuid;
    DELETE FROM trams_temp_roll_up_previous_revision WHERE uuid = @uuid;

	-- Confirmation of deletion, for when SP is run manually by the help desk
	SELECT * FROM `trams_budget_revision` WHERE `id` = BudgetRevisionid;
	SELECT * FROM `trams_scope` WHERE `application_id` = ApplicationId;
    SELECT * FROM `trams_line_item` WHERE `app_id` = ApplicationId;
	SELECT * FROM `trams_audit_history` WHERE `id` >= @AuditHistoryId;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_acc_reservation_apportionment`(
	IN `p_cost_center_code` VARCHAR(20),
	IN `p_section_codes` VARCHAR(4096),
	IN `p_uza_code` VARCHAR(6),
	IN `p_uza_reference_version` INT(11))
    NO SQL
BEGIN

	DECLARE v_fiscal_year VARCHAR(4);
	SET v_fiscal_year = trams_get_current_fiscal_year(NOW());
	-- added sid varible to be used as a timepstamp
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
	-- added uuid variable to act as identifier
	SET @uuid = UUID_SHORT();

	-- removed created temporary table statement
	SET @querysc = CONCAT(
		"INSERT INTO trams_temp_section_codes (section_code, uuid, time_stamp)
			SELECT
				DISTINCT acc_section_code,",
				-- inserted uuid
				@uuid, ",'",
				-- inserted sid
				@sid, "' ",
			"FROM
				trams_account_class_code
			WHERE
				acc_section_code IN (", p_section_codes, ")"
	);

	PREPARE stmtsc FROM @querysc;
	EXECUTE stmtsc;

	-- replaced create temporary table with insert into
	INSERT INTO trams_temp_resv_account_class_code
		SELECT
			NULL AS `trams_temp_resv_account_class_code_id`,
			`fiscal_year` AS `fiscal_year`,
			`cost_center_code` AS `cost_center_code`,
			`cost_center_name` AS `cost_center`,
			`account_class_code` AS `account_class_code`,
			`acc_funding_fiscal_year` AS `funding_fiscal_year`,
			`acc_appropriation_code` AS `appropriation_code`,
			`acc_section_code` AS `section_code`,
			`acc_limitation_code` AS `limitation_code`,
			`acc_auth_type` AS `auth_type`,
			`acc_description` AS `acct_class_code_desc`,
			`trmsprtn_ccntclss_prtngbdg` AS `opb_id`,
			IFNULL(SUM(`code_new_amount`), 0) AS `initial_amount`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM `trams_account_class_code`
		WHERE
			`fiscal_year` = v_fiscal_year
			AND `cost_center_code` = p_cost_center_code
			AND `code_transaction_type` <> 'Recovery'
			-- added uuid WHERE clause
			AND `acc_section_code` IN (SELECT section_code FROM trams_temp_section_codes WHERE uuid = @uuid)
			AND `authorized_flag` = 1
		GROUP BY
			`acc_funding_fiscal_year`,
			`acc_appropriation_code`,
			`acc_section_code`,
			`acc_limitation_code`,
			`acc_auth_type`;

	-- replaced create temporary table with insert into
	INSERT INTO trams_temp_account_class_code_history
		SELECT
			NULL AS `trams_temp_account_class_code_history_id`,
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`,
			`account_class_code`,
			IFNULL(SUM(`transfer_in_amount`), 0) AS `transfer_in_amount`,
			IFNULL(SUM(`transfer_out_amount`), 0) AS `transfer_out_amount`,
			IFNULL(SUM(`reserved_amount`), 0) AS `reserved_amount`,
			IFNULL(SUM(`unreserve_amount`), 0) AS `unreserve_amount`,
			IFNULL(SUM(`obligated_amount`), 0) AS `obligated_amount`,
			IFNULL(SUM(`recovery_amount`), 0) AS `recovery_amount`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM `trams_account_class_code_history`
		WHERE
			`transaction_fiscal_year` = v_fiscal_year
			AND `cost_center_code` = p_cost_center_code
			-- added uuid WHERE clause
			AND `section_id` IN (SELECT section_code FROM trams_temp_section_codes WHERE uuid = @uuid)
		GROUP BY
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`,
			`account_class_code`;

	-- replaced create temporary table with insert into
	INSERT INTO trams_temp_acc_acchistory_rollup
		SELECT
			NULL AS `trams_temp_acc_acchistory_rollup_id`,
			`ac`.`fiscal_year`,
			`ac`.`cost_center_code`,
			`ac`.`cost_center`,
			`ac`.`account_class_code`,
			`ac`.`funding_fiscal_year`,
			`ac`.`appropriation_code`,
			`ac`.`section_code`,
			`ac`.`limitation_code`,
			`ac`.`auth_type`,
			`ac`.`acct_class_code_desc`,
			`ac`.`opb_id`,
			`ac`.`initial_amount`,
			IFNULL(`ac`.`initial_amount`, 0)
				- IFNULL(`acch`.`transfer_out_amount`, 0)
				+ IFNULL(`acch`.`transfer_in_amount`, 0)
				- IFNULL(`acch`.`reserved_amount`, 0)
				+ IFNULL(`acch`.`unreserve_amount`, 0)
				- IFNULL(`acch`.`obligated_amount`, 0)
				+ IFNULL(`acch`.`recovery_amount`, 0) AS `oper_budget_avail`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM `trams_temp_resv_account_class_code` `ac`
			LEFT JOIN `trams_temp_account_class_code_history` `acch`
				ON `ac`.`funding_fiscal_year` = `acch`.`funding_fiscal_year`
				AND `ac`.`appropriation_code` = `acch`.`appropriation_code`
				AND `ac`.`section_code` = `acch`.`section_id`
				AND `ac`.`limitation_code` = `acch`.`limitation_code`
				AND `ac`.`account_class_code` = `acch`.`account_class_code`
				-- inserted uuid
				AND `acch`.`uuid` = @uuid
		-- added uuid WHERE clause
		WHERE `ac`.`uuid` = @uuid;

	-- replaced create temporary table with insert into
	INSERT INTO trams_temp_acc_reservation
		SELECT
			NULL AS `trams_temp_acc_reservation_id`,
			uuid_short() AS `a_id`,
			`accf`.`account_class_code` AS `acct_class_code`,
			`accf`.`fpc` AS `fpc`,
			`a`.`fiscal_year`,
			`a`.`cost_center_code`,
			`a`.`cost_center`,
			`a`.`account_class_code`,
			`a`.`funding_fiscal_year`,
			`a`.`appropriation_code`,
			`a`.`section_code`,
			`a`.`limitation_code`,
			`a`.`auth_type`,
			`a`.`acct_class_code_desc`,
			`a`.`opb_id`,
			`a`.`initial_amount`,
			`a`.`oper_budget_avail`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM
			`trams_acc_fpc_reference` `accf`
			JOIN
				`trams_temp_acc_acchistory_rollup` `a`
				ON `a`.`account_class_code` = `accf`.`account_class_code`
				-- added uuid AND clause
				AND `a`.`uuid` = @uuid
		GROUP BY
			`accf`.`account_class_code`,
			`accf`.`fpc`;

  -- replaced create temporary table with insert into
	INSERT INTO trams_temp_apportionment
		SELECT
			NULL AS `trams_temp_apportionment_id`,
			`uza_code`,
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`,
			IFNULL(`apportionment`, 0) AS `apportionment`,
			IFNULL(`operating_cap`, 0) AS `operating_cap`,
			IFNULL(`ceiling_amount`, 0) AS `ceiling_amount`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM `trams_apportionment`
		WHERE
			`uza_code` = p_uza_code
			-- added uuid WHERE clause
			AND `section_id` IN (SELECT section_code FROM trams_temp_section_codes WHERE uuid = @uuid)
			AND `active_flag` = 1
			AND lapse_period > v_fiscal_year
		GROUP BY
			`uza_code`,
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`;

 -- replaced create temporary table with insert into
	INSERT INTO trams_temp_apportionment_history
		SELECT
			NULL AS `trams_temp_apportionment_history_id`,
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`,
			IFNULL(SUM(`reserved_amount`), 0) AS `reserved_amount`,
			IFNULL(SUM(`unreserve_amount`), 0) AS `unreserve_amount`,
			IFNULL(SUM(`obligated_amount`), 0) AS `obligated_amount`,
			IFNULL(SUM(`recovery_amount`), 0) AS `recovery_amount`,
			IFNULL(SUM(`transfer_in_amount`), 0) AS `transfer_in_amount`,
			IFNULL(SUM(`transfer_out_amount`), 0) AS `transfer_out_amount`,
			IFNULL(SUM(`modified_amount`), 0) AS `modified_amount`,
			-- inserted uuid
			@uuid AS `uuid`,
			-- inserted sid
			@sid AS `time_stamp`
		FROM `trams_apportionment_history`
		WHERE
			`uza_code` = p_uza_code
			-- added uuid WHERE clause
			AND `section_id` IN (SELECT section_code FROM trams_temp_section_codes WHERE uuid = @uuid)
		GROUP BY
			`uza_code`,
			`funding_fiscal_year`,
			`appropriation_code`,
			`section_id`,
			`limitation_code`;

  -- replaced create temporary table with insert into
	INSERT INTO trams_temp_acc_resv_apportionment_rollup
		SELECT
			NULL AS `trams_temp_acc_resv_apportionment_rollup_id`,
			`a`.`uza_code`,
			`a`.`funding_fiscal_year`,
			`a`.`appropriation_code`,
			`a`.`section_id`,
			`a`.`limitation_code`,
			`a`.`apportionment`,
			`a`.`operating_cap`,
			`a`.`ceiling_amount`,
			`a`.`apportionment`
				+ IFNULL(`ah`.`modified_amount`, 0)
				+ IFNULL(`ah`.`transfer_in_amount`, 0)
				+ IFNULL(`ah`.`unreserve_amount`, 0)
				+ IFNULL(`ah`.`recovery_amount`, 0)
				- IFNULL(`ah`.`reserved_amount`, 0)
				- IFNULL(`ah`.`obligated_amount`, 0)
				- IFNULL(`ah`.`transfer_out_amount`, 0) AS `available_amount`,
				-- inserted uuid
				@uuid AS `uuid`,
				-- inserted sid
				@sid AS `sid`
		FROM `trams_temp_apportionment` `a`
			LEFT JOIN `trams_temp_apportionment_history` `ah`
				ON `a`.`funding_fiscal_year` = `ah`.`funding_fiscal_year`
				AND `a`.`appropriation_code` = `ah`.`appropriation_code`
				AND `a`.`section_id` = `ah`.`section_id`
				AND `a`.`limitation_code` = `ah`.`limitation_code`
				-- added uuid AND clause
				AND `ah`.`uuid` = @uuid
		-- added uuid WHERE clause
		WHERE `a`.`uuid` = @uuid;

	SELECT
		uuid_short() AS `id`,
		`ac`.`opb_id` AS `opbId_int`,
		`ac`.`fiscal_year` AS `fiscalYear_text`,
		`ac`.`cost_center_code` AS `costCenterCode_text`,
		`ac`.`cost_center` AS `costCenter_text`,
		`ap`.`uza_code` AS `uzaCode_text`,
		`uv`.`uza_name` AS `uzaName_text`,
		`ac`.`funding_fiscal_year` AS `fundingFiscalYear_text`,
		`ac`.`appropriation_code` AS `appropriationCode_text`,
		`ac`.`section_code` AS `sectionCode_text`,
		`ac`.`limitation_code` AS `limitationCode_text`,
		`ac`.`auth_type` AS `authType_text`,
		`ac`.`account_class_code` AS `accountClassCode_text`,
		`ac`.`fpc` AS `fpc_text`,
		`ac`.`acct_class_code_desc` AS `accountClassCodeDesc_text`,
		`ac`.`oper_budget_avail` AS `operBudgetAvail_dec`,
		`ap`.`available_amount` AS `apportionmentAvailable_dec`,
		`ap`.`operating_cap` AS `operatingCap_dec`,
		`ap`.`ceiling_amount` AS `ceiling_dec`
	FROM
		`trams_temp_acc_reservation` `ac`
		LEFT JOIN `trams_temp_acc_resv_apportionment_rollup` `ap`
			ON `ac`.`funding_fiscal_year` = `ap`.`funding_fiscal_year`
			AND `ac`.`section_code` = `ap`.`section_id`
			AND `ac`.`limitation_code` = `ap`.`limitation_code`
			AND `ac`.`appropriation_code` = `ap`.`appropriation_code`
			-- inserted uuid
			AND `ap`.`uuid` = @uuid
		JOIN `trams_uza_code_reference` `uv`
			ON `ap`.`uza_code` = `uv`.`uza_code`
	WHERE
		`uv`.`version` = p_uza_reference_version
		-- added uuid AND statement
		AND `ac`.`uuid` = @uuid;

-- added delete statements to remove entries inserted during stored procedure call
DELETE FROM trams_temp_section_codes WHERE uuid = @uuid;
DELETE FROM trams_temp_resv_account_class_code WHERE uuid = @uuid;
DELETE FROM trams_temp_account_class_code_history WHERE uuid = @uuid;
DELETE FROM trams_temp_acc_acchistory_rollup WHERE uuid = @uuid;
DELETE FROM trams_temp_acc_reservation WHERE uuid = @uuid;
DELETE FROM trams_temp_apportionment WHERE uuid = @uuid;
DELETE FROM trams_temp_apportionment_history WHERE uuid = @uuid;
DELETE FROM trams_temp_acc_resv_apportionment_rollup WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsReconciliationReportApportionment`(
	IN p_fundingFiscalYear VARCHAR(4), 
	IN p_appropriationCode VARCHAR(4), 
	IN p_sectionCode VARCHAR(4), 
	IN p_limitationCode VARCHAR(4)
)
BEGIN
   DECLARE _spName varchar(255) default "sptramsReconciliationReportApportionment";
   DECLARE _whereClause VARCHAR(4096) default "";
   
   SET @SID = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));
   
   /* write procedure call to the log */
    CALL `trams_log`(@SID, _spName, CONCAT(
        ' CALL SP Inputs: ',
		IFNULL(`p_fundingFiscalYear`,'NULL'),',',
		IFNULL(`p_appropriationCode`,'NULL'),',',
		IFNULL(`p_sectionCode`,'NULL'),',',
		IFNULL(`p_limitationCode`,'NULL')
    ), 1);
   
SET
   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and recApportionment.FundingFiscalYear in (\"",NULLIF(p_fundingFiscalYear, ''),"\")"), "") ));
SET
   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and recApportionment.AppropriationCode in (\"",NULLIF(p_appropriationCode, ''),"\")"), "") ));
SET
   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and recApportionment.SectionCode in (\"",NULLIF(p_sectionCode, ''),"\")"), "") ));
SET
   _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and recApportionment.LimitationCode in (\"",NULLIF(p_limitationCode, ''),"\")"), "") ));

SET
   @reconciliationApportionmentInformationQuery = CONCAT(" 
   SELECT 
    CONCAT(\"UZA Code: \", recApportionment.UzaCode, \" | \", recApportionment.FundingFiscalYear, \".\", recApportionment.AppropriationCode, \".\", recApportionment.SectionCode, \".\", recApportionment.LimitationCode),
	recApportionment.UzaCode,
	recApportionment.FundingFiscalYear,
	recApportionment.AppropriationCode,
	recApportionment.SectionCode,
	recApportionment.LimitationCode,
	recApportionment.ApportionmentAvailableBalance
	FROM
		vwtramsReconciliationReportApportionment recApportionment WHERE 1=1", _whereClause);
PREPARE reconciliationReportApportionment 
   FROM
      @reconciliationApportionmentInformationQuery;
EXECUTE reconciliationReportApportionment ;

CALL `trams_log`(@SID, _spName, CONCAT( now(), ' Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `sptramsUpdateUsernameInTramsTables`(
    IN p_oldUsername VARCHAR(255),
    IN p_newUsername VARCHAR(255)
)
BEGIN
    UPDATE trams_application_point_of_contact       SET `user_name`             = p_newUsername WHERE `user_name`             = p_oldUsername;
	UPDATE tramsApplicationTransmissionHistory      SET `ModifiedBy`            = p_newUsername WHERE `ModifiedBy`            = p_oldUsername;
	UPDATE tramsPOPChangelog                        SET `UpdatedBy`             = p_newUsername WHERE `UpdatedBy`             = p_oldUsername;
	UPDATE trams_application                        SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_application                        SET `modified_by`           = p_newUsername WHERE `modified_by`           = p_oldUsername;
	UPDATE trams_application                        SET `amended_by`            = p_newUsername WHERE `amended_by`            = p_oldUsername;
	UPDATE trams_application_award_detail           SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_application_award_detail           SET `executed_by`           = p_newUsername WHERE `executed_by`           = p_oldUsername;
	UPDATE trams_application_fleet_status           SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_audit_history                      SET `action_taken_by`       = p_newUsername WHERE `action_taken_by`       = p_oldUsername;
	UPDATE trams_budget_revision                    SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_budget_revision                    SET `submitted_by`          = p_newUsername WHERE `submitted_by`          = p_oldUsername;
	UPDATE trams_budget_revision                    SET `executed_by`           = p_newUsername WHERE `executed_by`           = p_oldUsername;
	UPDATE trams_civil_rights_compliance            SET `last_modified_by`      = p_newUsername WHERE `last_modified_by`      = p_oldUsername;
	UPDATE trams_civil_rights_grantee_history       SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_contract_acc_funding               SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_dbe_report                         SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_document                           SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_federal_financial_report           SET `initial_submission_by` = p_newUsername WHERE `initial_submission_by` = p_oldUsername;
	UPDATE trams_federal_financial_report           SET `last_modified_by`      = p_newUsername WHERE `last_modified_by`      = p_oldUsername;
	UPDATE trams_fleet_status                       SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_general_business_info              SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_geographic_identifiers             SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_grantee_poc                        SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_grantee_poc                        SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_line_item                          SET `modified_by`           = p_newUsername WHERE `modified_by`           = p_oldUsername;
	UPDATE trams_line_item                          SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_line_item_change_log               SET `revised_by`            = p_newUsername WHERE `revised_by`            = p_oldUsername;
	UPDATE trams_milestone_progress_remarks         SET `remark_by`             = p_newUsername WHERE `remark_by`             = p_oldUsername;
	UPDATE trams_milestone_progress_report          SET `submitted_by`          = p_newUsername WHERE `submitted_by`          = p_oldUsername;
	UPDATE trams_milestone_progress_report          SET `updated_by`            = p_newUsername WHERE `updated_by`            = p_oldUsername;
	UPDATE trams_milestones                         SET `modified_by`           = p_newUsername WHERE `modified_by`           = p_oldUsername;
	UPDATE trams_milestones                         SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_project                            SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_project                            SET `modified_by`           = p_newUsername WHERE `modified_by`           = p_oldUsername;
	UPDATE trams_project_environmental_finding      SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_project_environmental_finding      SET `last_modified_by`      = p_newUsername WHERE `last_modified_by`      = p_oldUsername;
	UPDATE trams_project_environmental_finding_date SET `created_by`            = p_newUsername WHERE `created_by`            = p_oldUsername;
	UPDATE trams_project_environmental_finding_date SET `last_modified_by`      = p_newUsername WHERE `last_modified_by`      = p_oldUsername;
	UPDATE trams_report_remarks                     SET `remark_by`             = p_newUsername WHERE `remark_by`             = p_oldUsername;
	UPDATE trams_review_comments                    SET `reviewed_by`           = p_newUsername WHERE `reviewed_by`           = p_oldUsername;
	UPDATE trams_security                           SET `modified_by`           = p_newUsername WHERE `modified_by`           = p_oldUsername;
	UPDATE trams_status_history                     SET `change_by`             = p_newUsername WHERE `change_by`             = p_oldUsername;
	
	INSERT INTO `trams_audit_history` (
		`context_id`, 
		`context_details`, 
		`action_taken_by`, 
		`action_date_time`, 
		`action_description`
	) 
	VALUES(
		(SELECT id FROM `ACS_User` WHERE `username` = p_newUsername), 
		"User Account", 
		"trams.administrator", 
		now(), 
		CONCAT(
			"Username updated from ",
			p_oldUsername,
			" to ",
			p_newUsername,
			" across multiple columns/tables in support of the move to multi-factor authentication. Scope of change may include updates to user_name in the trams_application_point_of_contact table and audit columns in tables associated with the following application areas: Applications, Recipient Records, Budget Revisions, Civil Rights, Projects, Line Items, Milestones and Reports."
		)
	);
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_acc_reservation_apportionment_by_uniqueIdentifiers`(IN `p_cost_center_codes` VARCHAR(4096), IN `p_uza_codes` VARCHAR(4096), IN `p_acc_funding_fiscal_years` VARCHAR(4096), IN `p_acc_appropriation_codes` VARCHAR(4096), IN `p_acc_section_codes` VARCHAR(4096), IN `p_acc_limitation_codes` VARCHAR(4096), IN `p_acc_auth_types` VARCHAR(4096), IN `p_account_class_codes` VARCHAR(4096), IN `p_acc_fpcs` VARCHAR(4096), IN `p_uza_reference_version` INT(11))
BEGIN
  
  DECLARE _done INT DEFAULT 0;
  DECLARE _fiscal_year VARCHAR(4);
  DECLARE _cost_center_code VARCHAR(6);
  DECLARE _uza_code VARCHAR(6);
  DECLARE _acc_funding_fiscal_year VARCHAR(4);
  DECLARE _acc_appropriation_code VARCHAR(2);
  DECLARE _acc_section_code VARCHAR(4);
  DECLARE _acc_limitation_code VARCHAR(2);
  DECLARE _acc_auth_type VARCHAR(1);
  DECLARE _account_class_code VARCHAR(40);
  DECLARE _acc_fpc VARCHAR(2);
  /* Cursor updated to pull parameter values from the new trams_temp_ui_parameter_values instead of from 9 different parameter temp tables*/
  DECLARE parameter_cursor CURSOR FOR 
	SELECT 
	  cost_center_code, 
      uza_code, 
      acc_funding_fiscal_year, 
      acc_appropriation_code, 
      acc_section_code, 
      acc_limitation_code, 
      acc_auth_type, 
      account_class_code, 
      acc_fpc
	FROM trams_temp_ui_parameter_values
	/* Added uuid constraint*/
	WHERE uuid = @uuid;
  DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done = 1;


  SET _fiscal_year = trams_get_current_fiscal_year(NOW());

  /* For Logging*/

  SET @uuid = UUID_SHORT();
  SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
  
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Fiscal Year: ', _fiscal_year), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter Cost Center Codes: ', p_cost_center_codes), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter UZA Codes: ', p_uza_codes), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC Funding Fiscal Years: ', p_acc_funding_fiscal_years), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC Appropriation Codes: ', p_acc_appropriation_codes), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC Section Codes: ', p_acc_section_codes), 0); 
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC Limitation Codes: ', p_acc_limitation_codes), 0);
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC Auth Types: ', p_acc_auth_types), 0);
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter Account Class Code: ', p_account_class_codes), 0);
  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Parameter ACC FPCs: ', p_acc_fpcs), 0);  


  /* 
	This procedure accepts array parameters in the form of a comma separated list. In order to iterate through these array parameters, they are written to a table (trams_temp_ui_parameter_values) and read through using a cursor. Previously, this was handled by inserting the values for each parameter into individual temporary tables.
  */


  SET @loopCount = 1;
  /* @paramLength contains the length of the parameter arrays, determined by counting the number of commas in the comma separated input. This value will determine how many times the while loop will run */
  SET @paramLength = LENGTH(p_cost_center_codes) - LENGTH(Replace(p_cost_center_codes, ',', '')) + 1;
  
  /* This loop will insert one entry into the table trams_temp_ui_parameter_values for each item in the input array */
  WHILE @loopCount <= @paramLength DO
	SET @paramInsertQuery = "INSERT INTO trams_temp_ui_parameter_values(cost_center_code, uza_code, acc_funding_fiscal_year, acc_appropriation_code, acc_section_code, acc_limitation_code, acc_auth_type, account_class_code, acc_fpc, uuid, time_stamp) VALUES (";
	
	/* The substring rule will isolate the nth in each parameter array where n is the current iteration of the loop */
	SET @costCenterCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_cost_center_codes, ",", @loopCount), ",", -1);
	SET @uzaCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_uza_codes, ",", @loopCount), ",", -1);
	SET @accFundingFiscalYear = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_funding_fiscal_years, ",", @loopCount), ",", -1);
	SET @accAppropriationCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_appropriation_codes, ",", @loopCount), ",", -1);
	SET @accSectionCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_section_codes, ",", @loopCount), ",", -1);
	SET @accLimitationCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_limitation_codes, ",", @loopCount), ",", -1);
	SET @accAuthType = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_auth_types, ",", @loopCount), ",", -1);
	SET @accountClassCode = SUBSTRING_INDEX(SUBSTRING_INDEX(p_account_class_codes, ",", @loopCount), ",", -1);
	SET @AccFpc = SUBSTRING_INDEX(SUBSTRING_INDEX(p_acc_fpcs, ",", @loopCount), ",", -1);
	
	/* The selected array entries of each parameter will then be concatenated together along with uuid and sid to be inserted as one entry in the trams_temp_ui_parameter_values table */
	SET @paramInsertQuery = CONCAT(@paramInsertQuery, @costCenterCode,',', @uzaCode,',', @accFundingFiscalYear,',', @accAppropriationCode,',', @accSectionCode,',', @accLimitationCode,',', @accAuthType,',', @accountClassCode,',', @AccFpc,',', @uuid,", '", @sid, "')");
	 PREPARE stmtparam FROM @paramInsertQuery;
     EXECUTE stmtparam;
	
	SET @loopCount = @loopCount + 1;
  END WHILE;
  

  OPEN parameter_cursor;
  SET @cursor_cnt = (SELECT FOUND_ROWS());
  SET @parameter_cnt = (SELECT COUNT(*) FROM trams_temp_ui_parameter_values WHERE uuid = @uuid);

  IF @parameter_cnt = @cursor_cnt THEN

    -- Begin to loop through the parameters 
    SET @createResultTable = true;
    get_result: LOOP
      
      CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', 'In loop', 0);
      
      -- Fetch the parameter
      FETCH parameter_cursor INTO _cost_center_code, _uza_code, _acc_funding_fiscal_year, _acc_appropriation_code, _acc_section_code, _acc_limitation_code, _acc_auth_type, _account_class_code, _acc_fpc;
      
      IF _done = 1 THEN
        LEAVE get_result;
      END IF;
      
	  
	  /* Added Delete to replicate previous dropping behavior */
	  DELETE FROM trams_temp_ui_acc_acchistory_rollup WHERE uuid = @uuid;
	  INSERT INTO trams_temp_ui_acc_acchistory_rollup (fiscal_year, cost_center_code, cost_center, account_class_code, funding_fiscal_year, appropriation_code, section_code, limitation_code, auth_type, acct_class_code_desc, opb_id, initial_amount, oper_budget_avail, uuid, time_stamp)
        SELECT
            `ac`.`fiscal_year`,
            `ac`.`cost_center_code`,
            `ac`.`cost_center`,
            `ac`.`account_class_code`,
            `ac`.`funding_fiscal_year`,
            `ac`.`appropriation_code`,
            `ac`.`section_code`,
            `ac`.`limitation_code`,
            `ac`.`auth_type`,
            `ac`.`acct_class_code_desc`,
            `ac`.`opb_id`,
			`ac`.`initial_amount`,
            `ac`.`initial_amount` 
            - IFNULL(SUM(`acch`.`transfer_out_amount`), 0) 
            + IFNULL(SUM(`acch`.`transfer_in_amount`), 0) 
            - IFNULL(SUM(`acch`.`reserved_amount`), 0) 
            + IFNULL(SUM(`acch`.`unreserve_amount`), 0) 
            - IFNULL(SUM(`acch`.`obligated_amount`), 0) 
            + IFNULL(SUM(`acch`.`recovery_amount`), 0) AS `oper_budget_avail`,
			@uuid as uuid,
			@sid as time_stamp
          FROM (
            SELECT 
              `fiscal_year` AS `fiscal_year`,
              `cost_center_code` AS `cost_center_code`,
              `cost_center_name` AS `cost_center`,
              `account_class_code` AS `account_class_code`,
              `acc_funding_fiscal_year` AS `funding_fiscal_year`,
              `acc_appropriation_code` AS `appropriation_code`,
              `acc_section_code` AS `section_code`,
              `acc_limitation_code` AS `limitation_code`,
              `acc_auth_type` AS `auth_type`,
              `acc_description` AS `acct_class_code_desc`,
              `trmsprtn_ccntclss_prtngbdg` AS `opb_id`,
              IFNULL(SUM(`code_new_amount`), 0) AS `initial_amount`
            FROM 
              `trams_account_class_code` 
            WHERE 
              `fiscal_year` = _fiscal_year
              AND `cost_center_code` = _cost_center_code
              AND `code_transaction_type` <> 'Recovery' 
              -- The Recovery ACC type is excluded because recovery is written as a transaction and is therefore rolled up along with ACC Histories
              AND `acc_funding_fiscal_year` = _acc_funding_fiscal_year
              AND `acc_appropriation_code` = _acc_appropriation_code
              AND `acc_section_code` = _acc_section_code
              AND `acc_limitation_code` = _acc_limitation_code
              AND `acc_auth_type` = _acc_auth_type
              AND `authorized_flag` = 1
            GROUP BY 
              `acc_funding_fiscal_year`, 
              `acc_appropriation_code`, 
              `acc_section_code`, 
              `acc_limitation_code`, 
              `acc_auth_type`
          ) `ac`
          LEFT JOIN
            `trams_account_class_code_history` `acch` ON 
              `ac`.`fiscal_year` = `acch`.`transaction_fiscal_year`
              AND `ac`.`cost_center_code` = `acch`.`cost_center_code`
              AND `ac`.`funding_fiscal_year` = `acch`.`funding_fiscal_year`
              AND `ac`.`appropriation_code` = `acch`.`appropriation_code`
              AND `ac`.`section_code` = `acch`.`section_id`
              AND `ac`.`limitation_code` = `acch`.`limitation_code`
              AND `ac`.`account_class_code` = `acch`.`account_class_code`;
      SET @cnt = (SELECT COUNT(*) FROM trams_temp_ui_acc_acchistory_rollup WHERE uuid = @uuid);
      CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Total trams_temp_ui_acc_acchistory_rollup: ',@cnt), 0);  
    
	
	  /* Added Delete to replicate previous dropping behavior */
	  DELETE FROM trams_temp_ui_acc_reservation WHERE uuid = @uuid;
	  INSERT INTO trams_temp_ui_acc_reservation (a_id, acct_class_code, fpc, fiscal_year, cost_center_code, cost_center, account_class_code, funding_fiscal_year, appropriation_code, section_code, limitation_code, auth_type, acct_class_code_desc, opb_id, initial_amount, oper_budget_avail, uuid, time_stamp)
        SELECT 
          uuid_short() AS `a_id`,
          `accf`.`account_class_code` AS `acct_class_code`,
          `accf`.`fpc` AS `fpc`,
		  `a`.`fiscal_year` AS `fiscal_year`,
		  `a`.`cost_center_code` AS `cost_center_code`,
		  `a`.`cost_center` AS `cost_center`,
		  `a`.`account_class_code` AS `account_class_code`,
		  `a`.`funding_fiscal_year` AS `funding_fiscal_year`,
		  `a`.`appropriation_code` AS `appropriation_code`,
		  `a`.`section_code` AS `section_code`,
		  `a`.`limitation_code` AS `limitation_code`,
		  `a`.`auth_type` AS `auth_type`,
		  `a`.`acct_class_code_desc` AS `acct_class_code_desc`,
		  `a`.`opb_id` AS `opb_id`,
		  `a`.`initial_amount` AS `initial_amount`,
		  `a`.`oper_budget_avail` AS `oper_budget_avail`,
		  /* Added uuid and time_stamp*/
          @uuid AS `uuid`,
		  @sid AS `time_stamp` 
        FROM 
          `trams_acc_fpc_reference` `accf` 
          JOIN `trams_temp_ui_acc_acchistory_rollup` `a` 
            ON `accf`.`account_class_code` = `a`.`account_class_code`
			AND `a`.`uuid` = @uuid
        WHERE 
          `accf`.`fpc` = _acc_fpc
        GROUP BY 
          `accf`.`account_class_code`,
          `accf`.`fpc`;
      SET @cnt = (SELECT COUNT(*) FROM trams_temp_ui_acc_reservation WHERE uuid = @uuid);
      CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Total trams_temp_ui_acc_reservation: ', @cnt), 0); 
      
	  
	  /* Added Delete to replicate previous dropping behavior */
	  DELETE FROM trams_temp_ui_acc_resv_apportionment_rollup WHERE uuid = @uuid;
	  INSERT INTO trams_temp_ui_acc_resv_apportionment_rollup (uza_code, funding_fiscal_year, appropriation_code, section_id, limitation_code, apportionment, operating_cap, ceiling_amount, reserved_amount, unreserve_amount, obligated_amount, recovery_amount, transfer_in_amount, transfer_out_amount, modified_amount, available_amount, uuid, time_stamp)
        SELECT 
          `a`.`uza_code` AS `uza_code`,
          `a`.`funding_fiscal_year` AS `funding_fiscal_year`,
          `a`.`appropriation_code` AS `appropriation_code`,
          `a`.`section_id` AS `section_id`,
          `a`.`limitation_code` AS `limitation_code`,
          IFNULL(`a`.`apportionment`, 0) AS `apportionment`,
          IFNULL(`a`.`operating_cap`, 0) AS `operating_cap`,
          IFNULL(`a`.`ceiling_amount`, 0) AS `ceiling_amount`,
          IFNULL(SUM(`ah`.`reserved_amount`), 0) AS `reserved_amount`,
          IFNULL(SUM(`ah`.`unreserve_amount`), 0) AS `unreserve_amount`,
          IFNULL(SUM(`ah`.`obligated_amount`), 0) AS `obligated_amount`,
          IFNULL(SUM(`ah`.`recovery_amount`), 0) AS `recovery_amount`,
          IFNULL(SUM(`ah`.`transfer_in_amount`), 0) AS `transfer_in_amount`,
          IFNULL(SUM(`ah`.`transfer_out_amount`), 0) AS `transfer_out_amount`,
          IFNULL(SUM(`ah`.`modified_amount`), 0) AS `modified_amount`,
          IFNULL(`a`.`apportionment`, 0) 
            + IFNULL(SUM(`ah`.`modified_amount`), 0) 
            + IFNULL(SUM(`ah`.`transfer_in_amount`), 0) 
            + IFNULL(SUM(`ah`.`unreserve_amount`), 0) 
            + IFNULL(SUM(`ah`.`recovery_amount`), 0) 
            - IFNULL(SUM(`ah`.`reserved_amount`), 0) 
            - IFNULL(SUM(`ah`.`obligated_amount`), 0) 
            - IFNULL(SUM(`ah`.`transfer_out_amount`), 0) AS `available_amount`,
          @uuid AS `uuid`,
		  @sid AS `time_stamp`
        FROM 
          `trams_apportionment` `a` 
          LEFT JOIN 
            `trams_apportionment_history` `ah` ON 
              `a`.`uza_code` = `ah`.`uza_code` 
              AND `a`.`funding_fiscal_year` = `ah`.`funding_fiscal_year`
              AND `a`.`appropriation_code` = `ah`.`appropriation_code`
              AND `a`.`section_id` = `ah`.`section_id`
              AND `a`.`limitation_code` = `ah`.`limitation_code`
        WHERE 
          `a`.`uza_code` = _uza_code
          AND `a`.`funding_fiscal_year` = _acc_funding_fiscal_year
          AND `a`.`appropriation_code` = _acc_appropriation_code
          AND `a`.`section_id` = _acc_section_code
          AND `a`.`limitation_code` = _acc_limitation_code
          AND `a`.`active_flag` = 1;
      SET @cnt = (SELECT COUNT(*) FROM trams_temp_ui_acc_resv_apportionment_rollup WHERE uuid = @uuid);
      CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', CONCAT('Total trams_temp_ui_acc_resv_apportionment_rollup: ', @cnt), 0); 
      
      /* Final Result (create the result table FROM the result data if the result table not exists, otherwise insert the results) */
      SET @selectResultQuery = CONCAT(
        "SELECT 
          NULL AS `id`,
          `ac`.`opb_id` AS `opbId_int`,
          `ac`.`fiscal_year` AS `fiscalYear_text`,
          `ac`.`cost_center_code` AS `costCenterCode_text`,
          `ac`.`cost_center` AS `costCenter_text`,
          `ap`.`uza_code` AS `uzaCode_text`,
          `uv`.`uza_name` AS `uzaName_text`,
          `ac`.`funding_fiscal_year` AS `fundingFiscalYear_text`,
          `ac`.`appropriation_code` AS `appropriationCode_text`,
          `ac`.`section_code` AS `sectionCode_text`,
          `ac`.`limitation_code` AS `limitationCode_text`,
          `ac`.`auth_type` AS `authType_text`,
          `ac`.`account_class_code` AS `accountClassCode_text`,
          `ac`.`fpc` AS `fpc_text`,
          `ac`.`acct_class_code_desc` AS `accountClassCodeDesc_text`,
          `ac`.`oper_budget_avail` AS `operBudgetAvail_dec`,
          `ap`.`available_amount` AS `apportionmentAvailable_dec`,
          `ap`.`operating_cap` AS `operatingCap_dec`,
          `ap`.`ceiling_amount` AS `ceiling_dec`,
		  /* Added uuid and time_stamp*/
		  @uuid as `uuid`,
		  @sid AS `time_stamp`
        FROM 
          `trams_temp_ui_acc_reservation` `ac` 
          LEFT JOIN 
            `trams_temp_ui_acc_resv_apportionment_rollup` `ap` ON 
              `ac`.`funding_fiscal_year` = `ap`.`funding_fiscal_year` 
              AND `ac`.`section_code` = `ap`.`section_id` 
              AND `ac`.`limitation_code` = `ap`.`limitation_code` 
              AND `ac`.`appropriation_code` = `ap`.`appropriation_code`
			  AND `ap`.`uuid` = @uuid
          JOIN 
            `trams_uza_code_reference` `uv` ON 
              `ap`.`uza_code` = `uv`.`uza_code`
        WHERE 
          `uv`.`version` = ", p_uza_reference_version, "
		   /* added uuid condition for new table */
		   AND `ac`.`uuid` = @uuid
        GROUP BY 
          `ap`.`uza_code`, 
          `uv`.`uza_name`, 
          `ac`.`acct_class_code`, 
          `ac`.`acct_class_code_desc`, 
          `ac`.`funding_fiscal_year`, 
          `ac`.`appropriation_code`, 
          `ac`.`section_code`, 
          `ac`.`limitation_code`, 
          `ac`.`auth_type`, 
          `ac`.`fpc`, 
          `ac`.`oper_budget_avail`, 
          `ac`.`opb_id`, 
          `ac`.`cost_center`, 
          `ac`.`cost_center_code`, 
          `ac`.`fiscal_year`"
      );
        
      -- create table with the data 
      IF @createResultTable = true THEN
	    /* Added Delete to replicate previous dropping behavior */
		DELETE FROM trams_temp_ui_get_acc_resv_apport_by_uniq_ids_result WHERE uuid = @uuid;	
        SET @createResultTableQuery = CONCAT("INSERT INTO trams_temp_ui_get_acc_resv_apport_by_uniq_ids_result (id, opbId_int, fiscalYear_text, costCenterCode_text, costCenter_text, uzaCode_text, uzaName_text, fundingFiscalYear_text, appropriationCode_text, sectionCode_text, limitationCode_text, authType_text, accountClassCode_text, fpc_text, accountClassCodeDesc_text, operBudgetAvail_dec, apportionmentAvailable_dec, operatingCap_dec, ceiling_dec, uuid, time_stamp) ", @selectResultQuery);
        PREPARE createResultTableStmt FROM @createResultTableQuery;
        EXECUTE createResultTableStmt;
        
        SET @createResultTable = false;
      ELSE
        SET @insertResultTableQuery = CONCAT("INSERT INTO trams_temp_ui_get_acc_resv_apport_by_uniq_ids_result (id, opbId_int, fiscalYear_text, costCenterCode_text, costCenter_text, uzaCode_text, uzaName_text, fundingFiscalYear_text, appropriationCode_text, sectionCode_text, limitationCode_text, authType_text, accountClassCode_text, fpc_text, accountClassCodeDesc_text, operBudgetAvail_dec, apportionmentAvailable_dec, operatingCap_dec, ceiling_dec, uuid, time_stamp) ", @selectResultQuery);
        PREPARE insertResultTableStmt FROM @insertResultTableQuery;
        EXECUTE insertResultTableStmt;
        
      END IF;
    
    END LOOP get_result;
    
    /*Return result*/
	SELECT DISTINCT id, opbId_int, fiscalYear_text, costCenterCode_text, costCenter_text, uzaCode_text, uzaName_text, fundingFiscalYear_text, appropriationCode_text, sectionCode_text, limitationCode_text, authType_text, accountClassCode_text, fpc_text, accountClassCodeDesc_text, operBudgetAvail_dec, apportionmentAvailable_dec, operatingCap_dec, ceiling_dec FROM trams_temp_ui_get_acc_resv_apport_by_uniq_ids_result WHERE uuid = @uuid;


	DELETE FROM trams_temp_ui_parameter_values WHERE uuid = @uuid;
	DELETE FROM trams_temp_ui_acc_acchistory_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_ui_acc_reservation WHERE uuid = @uuid;
	DELETE FROM trams_temp_ui_acc_resv_apportionment_rollup WHERE uuid = @uuid;	 
	DELETE FROM trams_temp_ui_get_acc_resv_apport_by_uniq_ids_result WHERE uuid = @uuid;	 
 
  END IF; 
  CLOSE parameter_cursor; 
  
  

  CALL `trams_log`(@sid, 'trams_get_acc_reservation_apportionment_by_uniqueIdentifiers', 'Done!', 0); 

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_application_original_award_report`(IN p_granteeId VARCHAR(2048), IN p_costCenterCode VARCHAR(512), IN p_fiscalYear VARCHAR(512),
IN p_appNumber VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_appType VARCHAR(512), IN p_sectioncode VARCHAR(512), IN p_preAwardManager VARCHAR(1024),
IN p_postAwardManager VARCHAR(1024), IN p_gmtOffset VARCHAR(10), IN p_return_limit VARCHAR(20)
)
BEGIN

	/*
	 Stored Procedure Name : trams_get_application_original_award_report
     Description : Populate data for TrAMS App By Original Award excel report.
     Updated by  : Garrison Dean (2020.Nov.24)
					Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
	*/

	DECLARE _spName VARCHAR(255) DEFAULT "trams_get_application_original_award_report";
	DECLARE _sql TEXT DEFAULT "";
	DECLARE _whereClause VARCHAR(8192) DEFAULT " ";
	DECLARE _whereClause_sectionCode VARCHAR(8192) DEFAULT " ";
	DECLARE _limitClause VARCHAR(100) DEFAULT " ";
	CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.recipient_id IN (",NULLIF(p_granteeId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.recipient_cost_center IN (",NULLIF(p_costCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.application_number LIKE ('%",NULLIF(LEFT(p_appNumber,11),''),"%')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.status IN (",NULLIF(p_appStatus,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.application_type IN ('",NULLIF(p_appType,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fta_pre_award_manager IN ('",NULLIF(p_preAwardManager,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fta_post_award_manager IN ('",NULLIF(p_postAwardManager,''),"')"),"")));
	-- Modified section code WHERE clause to only use the section code filter and not the filters specific to applications
	SET _whereClause_sectionCode = (SELECT IFNULL(CONCAT(" WHERE cat.section_code IN ('",NULLIF(p_sectioncode,''),"')"),""));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fiscal_year IN (",NULLIF(p_fiscalYear,''),")"),"")));


	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, _spName, CONCAT(NOW(),' CALL SP INPUTS: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_sectioncode` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1);

	/*
	This select query previously pulled both TrAMS and TEAM awards, and used section code to distinguish between funding sources of supergrants.
	This select now just pulls TrAMS awards while TEAM awards are handled in a different query addressed in more detail in its own comments.
	The TrAMS query replaced section code with funding source name for join and grouping logic, because discrepancies in section code were resulting in awards being missed by the query.
	*/


	SET _sql = CONCAT("
			INSERT INTO trams_temp_aowr_budget_resultset 
			SELECT
			NULL,
			a.id,
			a.temp_application_number,
			a.application_number,
			a.recipient_id,
			gb.recipientAcronym AS recipient_acronym,
			gb.legalBusinessName AS legal_business_name,
			a.recipient_cost_center,
			a.fiscal_year,
			a.amendment_number,
			a.application_name,
			br.revision_number,
			a.status,
			a.application_type,
			a.recipient_contact,
			a.fta_pre_award_manager,
			a.fta_post_award_manager,
			CASE
				WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', ",p_gmtOffset,") AS char))
				ELSE trams_format_date(fr.transmitted_date)
			END,
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00', ",p_gmtOffset,") AS char)),
			CASE
				WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', ",p_gmtOffset,") AS char))
				ELSE trams_format_date(fr.submitted_date)
			END,
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00', ",p_gmtOffset,") AS char)),
			totals.section_code,
			IFNULL(SUM(li.revised_total_amount), 0),
			IFNULL((SUM(li.revised_total_amount) - SUM(li.revised_FTA_amount)),0),
			IFNULL(res.total_reserved_amount,0),
			totals.total_obligation_amount,
			totals.total_deobligation_amount,
			totals.total_disbursement_amount,
			totals.total_refund_amount,
			totals.total_unliquidated_amount,
			totals.percent_disbursement_amount,
			trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			trams_format_date(cast(CONVERT_TZ(aa.disbursement_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			trams_format_date(cast(CONVERT_TZ(aa.fta_closeout_approval_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			a.discretionary_funds_flag,
			@uuid AS uuid,
			@sid AS time_stamp
			FROM
			trams_application a
			JOIN
			(
			SELECT
			contractAward_Id,
			MAX(application_number) AS application_number
			FROM
			trams_application a
			-- now pulling in only TrAMS grants in this query
			WHERE a.app_from_team_flag = 0 ", _whereClause,
			" GROUP BY
			contractAward_Id
			) ta
			ON a.contractAward_Id = ta.contractAward_Id and a.application_number = ta.application_number
			JOIN
			vwtramsGeneralBusinessInfo gb
			ON a.recipient_id = gb.recipientId
			JOIN
			(
			SELECT
			ca.id,
			cat.section_code,
			-- now including funding source name for use in grouping the whole query
			cat.funding_source_name,
			SUM(cat.obligation_amount) AS total_obligation_amount,
			SUM(cat.deobligation_amount) AS total_deobligation_amount,
			SUM(cat.disbursement_amount) AS total_disbursement_amount,
			SUM(cat.refund_amount) AS total_refund_amount,
			(((IFNULL(SUM(cat.obligation_amount),0)
			- IFNULL(SUM(cat.deobligation_amount),0))
			- IFNULL(SUM(cat.disbursement_amount),0))
			+ IFNULL(SUM(cat.refund_amount),0)) AS total_unliquidated_amount,
			(SUM(cat.disbursement_amount)
			/ (SUM(cat.obligation_amount)-IFNULL(SUM(cat.deobligation_amount),0)+IFNULL(SUM(cat.refund_amount),0))) AS percent_disbursement_amount
			FROM
			trams_contract_award ca
			-- removed extra JOIN on trams_application
			JOIN
			trams_contract_acc_transaction cat
			ON ca.id = cat.contract_award_id "
			, _whereClause_sectionCode, "
			-- grouping by funding source name instead of section code
			GROUP BY ca.id, cat.funding_source_name
			) totals
			ON
			a.contractAward_ID = totals.id
			JOIN
			trams_application_award_detail aa
			ON a.id = aa.application_id
			JOIN
			-- Relocated MAX function on Dates from SELECT Statement
			(select MAX(id), application_id, MAX(transmitted_date) AS transmitted_date, MAX(submitted_date) AS submitted_date from trams_fta_review group by application_id) fr
			ON a.id = fr.application_id
			-- now joining on funding source name instead of section code to distinguish between funding sources of supergrants
			LEFT JOIN trams_line_item li ON (li.app_id = a.id AND li.funding_source_name = totals.funding_source_name)
			LEFT JOIN
			(
			SELECT application_id,
			MAX(revision_number) AS revision_number
			FROM trams_budget_revision GROUP BY application_id
			) br
			ON br.application_id = a.id
			LEFT JOIN
			(
			SELECT application_id, total_reserved_amount AS total_reserved_amount
			FROM trams_reservation
			WHERE id IN
				(
				SELECT MAX(id)
				FROM trams_reservation GROUP BY application_id
				)
			)res
			ON res.application_id = a.id
			LEFT JOIN
				(
					SELECT
						th.applicationId AS applicationId,
						MAX(th.TransmittedDate) AS latest_transmitted_date,
						MAX(th.SubmittedDate) AS latest_submitted_date,
						MIN(th.TransmittedDate) AS original_transmitted_date,
						MIN(th.SubmittedDate) AS original_submitted_date
					FROM
						tramsApplicationTransmissionHistory th
					-- removed extraneous join on trams_application
					GROUP BY th.applicationId
				) latest_transmission_history
				ON a.id = latest_transmission_history.applicationId
			-- now grouping by funding source name instead of section code
			GROUP BY a.id, totals.funding_source_name
	");

	SET @applicationQuery  = CONCAT(_sql,_limitClause);
	CALL `trams_log`(@sid, _spName, CONCAT(NOW(),' Create Temp Table Select: ',@applicationQuery), 1);

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;


	/*
	This insert statement was added to calculate the values for TEAM awards differently than TrMAS awards.
	TEAM awards cannot be super grants, so they don't require logic to distinguish between different funding sources.
	This query is mostly identical to the above TrAMS award query, but the differences are marked with in line comments
	*/

	SET  _sql = CONCAT("
			INSERT INTO trams_temp_aowr_budget_resultset
			SELECT
			NULL,
			a.id,
			a.temp_application_number,
			a.application_number,
			a.recipient_id,
			gb.recipientAcronym AS recipient_acronym,
			gb.legalBusinessName AS legal_business_name,
			a.recipient_cost_center,
			a.fiscal_year,
			a.amendment_number,
			a.application_name,
			br.revision_number ,
			a.status,
			a.application_type,
			a.recipient_contact ,
			a.fta_pre_award_manager,
			a.fta_post_award_manager,
			CASE
				WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', ",p_gmtOffset,") AS char))
				ELSE trams_format_date(fr.transmitted_date)
			END,
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00', ",p_gmtOffset,") AS char)),
			CASE
				WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', ",p_gmtOffset,") AS char))
				ELSE trams_format_date(fr.submitted_date)
			END,
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00', ",p_gmtOffset,") AS char)),
			totals.section_code,
			IFNULL(SUM(li.revised_total_amount),0),
			-- the disbursement amount from ACC transactions is used as the total FTA amount, because this is more accurate for TEAM grants
			(IFNULL(SUM(li.revised_total_amount),0) - IFNULL(li.revised_FTA_amount,0)),
			IFNULL(res.total_reserved_amount,0),
			totals.total_obligation_amount,
			totals.total_deobligation_amount,
			totals.total_disbursement_amount,
			totals.total_refund_amount,
			totals.total_unliquidated_amount,
			totals.percent_disbursement_amount,
			trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			trams_format_date(cast(CONVERT_TZ(aa.disbursement_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			trams_format_date(cast(CONVERT_TZ(aa.fta_closeout_approval_date_time, '+00:00', ",p_gmtOffset,") AS char)),
			a.discretionary_funds_flag,
			@uuid AS uuid,
			@sid AS time_stamp
			FROM
			trams_application a
			JOIN
			(
			SELECT
			contractAward_Id,
			MAX(application_number) AS application_number
			FROM
			trams_application a
			-- This query selects only TEAM awards
			WHERE a.app_from_team_flag = 1 ", _whereClause,
			"
			 GROUP BY
			contractAward_Id
			) ta
			ON a.contractAward_Id = ta.contractAward_Id and a.application_number = ta.application_number
			JOIN
			vwtramsGeneralBusinessInfo gb
			ON a.recipient_id = gb.recipientId
			JOIN
			(
			SELECT
			ca.id,
			cat.section_code,
			SUM(cat.obligation_amount) AS total_obligation_amount,
			SUM(cat.deobligation_amount) AS total_deobligation_amount,
			SUM(cat.disbursement_amount) AS total_disbursement_amount,
			SUM(cat.refund_amount) AS total_refund_amount,
			-- authorized disbursements are summed to capture the total FTA amount
			SUM(cat.authorized_disbursement_amount) AS authorized_disbursement_amount,
			(((IFNULL(SUM(cat.obligation_amount),0)
			- IFNULL(SUM(cat.deobligation_amount),0))
			- IFNULL(SUM(cat.disbursement_amount),0))
			+ IFNULL(SUM(cat.refund_amount),0)) AS total_unliquidated_amount,
			(SUM(cat.disbursement_amount)
			/ (SUM(cat.obligation_amount)-IFNULL(SUM(cat.deobligation_amount),0)+IFNULL(SUM(cat.refund_amount),0))) AS percent_disbursement_amount
			FROM
			trams_contract_award ca
			-- removed extra JOIN on trams_application
			JOIN
			trams_contract_acc_transaction cat
			ON ca.id = cat.contract_award_id"
			, _whereClause_sectionCode, "
			-- only grouping by app id, because there can only be one funding source per TEAM app
			GROUP BY ca.id
			) totals
			ON
			a.contractAward_ID = totals.id
			JOIN
			trams_application_award_detail aa
			ON a.id = aa.application_id
			JOIN
			-- Relocated MAX function on Dates from SELECT Statement
			(SELECT MAX(id), application_id, MAX(transmitted_date) AS transmitted_date, MAX(submitted_date) AS submitted_date FROM trams_fta_review group by application_id) fr
			ON a.id = fr.application_id
			-- only only joining on id
			LEFT JOIN trams_line_item li ON li.app_id = a.id
			LEFT JOIN
			(
			SELECT application_id,
			MAX(revision_number) AS revision_number
			FROM trams_budget_revision GROUP BY application_id
			) br
			ON br.application_id = a.id
			LEFT JOIN
			(
			SELECT application_id, total_reserved_amount AS total_reserved_amount
			FROM trams_reservation
			WHERE id IN
				(
				SELECT MAX(id)
				FROM trams_reservation GROUP BY application_id
				)
			)res
			ON res.application_id = a.id
			LEFT JOIN
				(
					SELECT
						th.applicationId AS applicationId,
						MAX(th.TransmittedDate) AS latest_transmitted_date,
						MAX(th.SubmittedDate) AS latest_submitted_date,
						MIN(th.TransmittedDate) AS original_transmitted_date,
						MIN(th.SubmittedDate) AS original_submitted_date
					FROM
						tramsApplicationTransmissionHistory th
					-- removed extraneous join on trams_application
					GROUP BY th.applicationId
				) latest_transmission_history
				ON a.id = latest_transmission_history.applicationId
			-- only grouping by app id, because there can only be one funding source per TEAM app
			GROUP BY a.id
	");


	SET @applicationQuery  = CONCAT(_sql,_limitClause);

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;


	SET _sql = CONCAT("
		INSERT INTO trams_temp_aowr_budget_resultset
		SELECT
		NULL,
		a.id,
		a.temp_application_number,
		a.application_number,
		a.recipient_id,
		gb.recipientAcronym AS recipient_acronym,
		gb.legalBusinessName AS legal_business_name,
		a.recipient_cost_center,
		a.fiscal_year,
		a.amendment_number,
		a.application_name,
		'' AS revision_number,
		a.status,
		a.application_type,
		a.recipient_contact ,
		a.fta_pre_award_manager,
		a.fta_post_award_manager,
		CASE
			WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(fr.transmitted_date)
		END,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00', ",p_gmtOffset,") AS char)),
		CASE
			WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(fr.submitted_date)
		END,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00', ",p_gmtOffset,") AS char)),
		'' AS section_code,
		IFNULL(aa.total_eligible_cost_amount, 0),
		IFNULL((aa.total_eligible_cost_amount - aa.total_fta_amount),0),
		'' AS total_reserved_amount,
		'' AS total_obligation_amount,
		'' AS total_deobligation_amount,
		'' AS total_disbursement_amount,
		'' AS total_refund_amount,
		'' AS total_unliquidated_amount,
		'' AS percent_disbursement_amount,
		trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time, '+00:00', ",p_gmtOffset,") AS char)),
		trams_format_date(cast(CONVERT_TZ(aa.disbursement_date_time, '+00:00', ",p_gmtOffset,") AS char)),
		trams_format_date(cast(CONVERT_TZ(aa.fta_closeout_approval_date_time, '+00:00', ",p_gmtOffset,") AS char)),
		a.discretionary_funds_flag,
		@uuid AS uuid,
		@sid AS time_stamp
		FROM
		trams_application a
		JOIN
		vwtramsGeneralBusinessInfo gb
		ON a.recipient_id = gb.recipientId
		JOIN
		trams_application_award_detail aa
		ON a.id = aa.application_id
		LEFT JOIN
		-- same aggregation change as in previous temp table
		-- Relocated MAX function on Dates from SELECT Statement
		(select MAX(id), application_id, MAX(transmitted_date) AS transmitted_date, MAX(submitted_date) AS submitted_date FROM trams_fta_review group by application_id) fr
		ON a.id = fr.application_id
		LEFT JOIN
				(
					SELECT
						th.applicationId AS applicationId,
						MAX(th.TransmittedDate) AS latest_transmitted_date,
						MAX(th.SubmittedDate) AS latest_submitted_date,
						MIN(th.TransmittedDate) AS original_transmitted_date,
						MIN(th.SubmittedDate) AS original_submitted_date
					FROM
						tramsApplicationTransmissionHistory th
					-- removed extraneous join on trams_application
					GROUP BY th.applicationId
				) latest_transmission_history
				ON a.id = latest_transmission_history.applicationId
		WHERE (a.contractAward_id is null or a.contractAward_id = '') ", _whereClause);

	SET @applicationQuery  = CONCAT(_sql,_limitClause);
	SET @appCount=(SELECT COUNT(*) FROM trams_temp_aowr_budget_resultset WHERE uuid = @uuid);
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));


	IF p_sectioncode IS NULL THEN
		 CALL `trams_log`(@sid, _spName, CONCAT(NOW(),' Data Insert Query: ',@applicationQuery), 1);
		 PREPARE applicationStmt FROM @applicationQuery;
		 EXECUTE applicationStmt;
	END IF;

	SELECT
		tmp.temp_application_number,
		tmp.application_number,
		tmp.recipient_id,
		tmp.recipient_acronym,
		tmp.legal_business_name,
		tmp.recipient_cost_center,
		tmp.fiscal_year,
		tmp.amendment_number,
		tmp.application_name,
		tmp.revision_number,
		tmp.status,
		tmp.application_type,
		tmp.recipient_contact,
		tmp.fta_pre_award_manager,
		tmp.fta_post_award_manager,
		IF(tmp.transmitted_date1='0000-00-00 00:00:00', '', trams_format_date(tmp.transmitted_date1)),
		IF(tmp.latest_transmitted_date='0000-00-00', '', tmp.latest_transmitted_date),
		IF(tmp.submitted_date='0000-00-00 00:00:00', '', trams_format_date(tmp.submitted_date)),
		IF(tmp.latest_submitted_date='0000-00-00', '', tmp.latest_submitted_date),
		tmp.section_code,
		tmp.total_eligible_cost_amount,
		tmp.total_non_fta_amount,
		tmp.total_reserved_amount,
		tmp.total_obligation_amount,
		tmp.total_deobligation_amount,
		tmp.total_disbursement_amount,
		tmp.total_refund_amount,
		tmp.total_unliquidated_amount,
		tmp.percent_disbursement_amount,
		IF(tmp.obligated_date_time='0000-00-00 00:00:00', '', trams_format_date(tmp.obligated_date_time)),
		IF(tmp.disbursement_date_time='0000-00-00 00:00:00', '', trams_format_date(tmp.disbursement_date_time)),
		IF(tmp.fta_closeout_approval_date_time='0000-00-00 00:00:00', '', trams_format_date(tmp.fta_closeout_approval_date_time)),
		tmp.discretionary_funds_flag
		FROM trams_temp_aowr_budget_resultset tmp
		WHERE tmp.uuid = @uuid
		ORDER BY application_number, fiscal_year;
		CALL `trams_log`(@sid, _spName,CONCAT(NOW(),' Done!'), 1);

		-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
		DELETE FROM trams_temp_aowr_budget_resultset WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_active_closeout_with_deobligation_report`(IN p_fundingDeploy DATE)
    NO SQL
BEGIN	

declare _onClause varchar(4096) default " ";
declare _appCreation date;

set _appCreation = (SELECT DATE(created_date_time) FROM trams_application WHERE contractAward_id = p_contractAwardId AND amendment_number = 0);

set _onClause = if(_appCreation<p_fundingDeploy,'`fs`.`version` = 0','`fs`.`latest_flag` = 1');

SET @sqlStatement = concat('select
		a.recipient_cost_center as "project_cost_center",
		a.application_number as "project_id",
		a.amendment_number as "amend_#",
		a.status as "status_code",
		a.created_date_time as "amnd_entered_date",
		a.recipient_id as "grantee_id",
		a.recipient_name as "grantee_name",
		a.application_name as "project_description",
		"" as "contract_no.",
		"" as "paper_grant",
		caf.section_code as "sect",
		caf.funding_source_name as "section_id",
		fsr.funding_source_description as "program_description",
		if (a.app_from_team_flag = 1, a.fta_contact_from_team, a.fta_post_award_manager) as "fta_project_manager" /* TODO: Split the fta_post_award_manager into first and last name */,
		aad.obligated_date_time as "grant_obl_date",
		caf.last_disbursement_date as "last_disb_date",
		a.application_type as "project_type",
		caf.account_class_code as "acc_class_code",
		caf.cost_center_code as "fund_cost_center",
		ala.account_level_net_obligations,
		ala.account_level_net_disbursements,
		ala.account_level_net_obligations - ala.account_level_net_disbursements as "account_level_undisbursed_amount",
		cla.total_obligation_amount,
		cla.total_disbursements,
		cla.total_obligation_amount - cla.total_disbursements as "total_undisbursed"
	from
		trams_application a
	join
		trams_contract_acc_funding caf
	on
		a.id = caf.application_id and
		a.id in 
		(
			select 
				id 
			from 
				trams_application
			where
				status in ("Active Award / Ready for Closeout", "Active Award / Closeout Requested") 
		)
	join
		trams_funding_source_reference fsr
	on
		caf.funding_source_name = fsr.funding_source_name
	and ',_onClause,'
	join
		trams_application_award_detail aad
	on
		caf.application_id = aad.application_id
	join
		(
			/* account level amounts */
			select 
				account_class_code, 
				contract_award_id, 
				cost_center_code,
				sum(obligation_amount) - sum(deobligation_amount) as "account_level_net_obligations",
				sum(disbursement_amount) - sum(refund_amount) as "account_level_net_disbursements"
			from
				trams_contract_acc_transaction
			group by
				account_class_code,
				contract_award_id,
				cost_center_code
		) ala
	on
		caf.account_class_code = ala.account_class_code and
		caf.contract_award_id = ala.contract_award_id and
		caf.cost_center_code = ala.cost_center_code and
		ala.account_level_net_obligations <> ala.account_level_net_disbursements
	join
		(
			/* contract level amounts */
			select
				contract_award_id,
				sum(obligation_amount) - sum(deobligation_amount) as "total_obligation_amount",
				sum(disbursement_amount) - sum(refund_amount) as "total_disbursements"
			from
				trams_contract_acc_transaction
			group by
				contract_award_id
		) cla
	on
		caf.contract_award_id = cla.contract_award_id');
		
PREPARE sqlStatement FROM @sqlStatement;
EXECUTE sqlStatement;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_drop_fms_compare_tmp_tables`( )
    NO SQL
BEGIN	
	
	DROP TABLE IF EXISTS trams_fms_compare_trams_final;
	
	
	DROP TABLE IF EXISTS trams_fms_compare_team_final;	
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_active_application_for_reporting_freq`(IN `p_report_frequency` VARCHAR(100))
BEGIN

	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _limitClause VARCHAR(100) DEFAULT " ";
	DECLARE _report_frequency VARCHAR(100) DEFAULT " ";
	DECLARE _tmpAppId VARCHAR(20) DEFAULT "";

	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

	IF LOCATE ("Annual"  , p_report_frequency)> 1   OR LOCATE ("Quarterly"  , p_report_frequency) >1 OR LOCATE ("Monthly"  , p_report_frequency) > 1 THEN

		INSERT INTO
			trams_temp_get_active_application_for_reporting_freq
		SELECT
		    NULL AS trams_temp_get_active_application_for_reporting_freq_id,
			CASE WHEN(LOCATE (fta_r.financial_report_freq, p_report_frequency) > 1) THEN 1 ELSE 0 END AS createffrReport_bool,
			CASE WHEN(LOCATE (fta_r.mpr_report_frequency, p_report_frequency) > 1) THEN 1 ELSE 0 END AS createmprReport_bool,
			con_awar.current_amendment_number AS applicationID_int,
			con_awar.id AS contractAwardId_int,
			fta_r.mpr_report_frequency AS mpr_report_frequency_text,
			fta_r.financial_report_freq AS financial_report_freq_text,
			1 AS active_flag,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM
			trams_contract_award con_awar
			INNER JOIN trams_fta_review fta_r ON (fta_r.application_id = con_awar.current_amendment_number)
		WHERE
			con_awar.active = 1
			AND con_awar.current_amendment_number IS NOT NULL;

		UPDATE
			trams_temp_get_active_application_for_reporting_freq temp
		SET
			-- restructured CASE statement
			temp.active_flag = (
				SELECT (
					CASE
						WHEN status LIKE "%Closeout%" THEN 0
						-- added additional status clause
						WHEN status = "Obligated / Ready for Execution" AND amendment_number = 0 THEN 0
						ELSE 1
					END)
				FROM trams_application
				WHERE temp_application_number = (SELECT a1.temp_application_number FROM trams_application a1 WHERE a1.id = temp.applicationID_int)
				-- replaced "MAX(id) logic with "ORDER BY id DESC LIMIT 1" clause
				ORDER BY id DESC LIMIT 1
			);

		 SET _sql =
			"SELECT
				createffrReport_bool AS createffrReport_bool,
				createmprReport_bool AS createmprReport_bool,
				applicationID_int AS applicationID_int,
				contractAwardId_int AS contractAwardId_int,
				mpr_report_frequency_text AS mpr_report_frequency_text,
				financial_report_freq_text AS financial_report_freq_text
			FROM
				trams_temp_get_active_application_for_reporting_freq tempTABLE
			WHERE
				tempTABLE.active_flag = 1
				AND tempTABLE.uuid = @uuid";

		CALL `trams_log`(@sid, 'trams_get_active_application_for_reporting_freq', CONCAT('1. Kinfu clause : ',_sql), 1);

		SET _WHEREClause = CONCAT(_WHEREClause, (SELECT IFNULL(CONCAT(" AND ( tempTABLE.mpr_report_frequency_text in (",NULLIF(p_report_frequency,''),")"),"")));
		SET _WHEREClause = CONCAT(_WHEREClause, (SELECT IFNULL(CONCAT(" or tempTABLE.financial_report_freq_text in (",NULLIF(p_report_frequency,''),"))"),"")));
		SET @finalQuery = CONCAT(_sql,_WHEREClause,_limitClause);

		PREPARE finalStmt FROM @finalQuery;
		EXECUTE finalStmt;

	END IF;

		DELETE FROM trams_temp_get_active_application_for_reporting_freq WHERE uuid = @uuid;

 END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_build_fms_compare_report_data`(OUT p_fullError varchar(2000))
BEGIN

  /*
     Stored Procedure Name : trams_build_fms_compare_report_data
     Description : To populate the data for TrAMS compare report to send to FMS.
     Created by  : Nathan Park  (2014.Nov.14)
     Update by   : Nathan Park  (2015.Feb.11)
                   Chong Wang   (2015.May.20)
                   Darryl Price (2015.Nov.11)
				   Garrison Dean (2019.Jan.29)
				   Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
  */

    DECLARE _current_fy_beginning_datetime datetime;
    
    /****************************************************************************************************
     ERROR HANDLER
    *****************************************************************************************************/ 
    DECLARE EXIT HANDLER FOR SQLEXCEPTION
    BEGIN
        GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
         @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
        SET p_fullError = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
    END;

    SET _current_fy_beginning_datetime = (SELECT STR_TO_DATE(concat('10/1/', trams_get_current_fiscal_year(now()) - 1, ' 00:00:00'), '%m/%d/%Y %H:%i:%s'));
    
    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	
	set @uuid = UUID_SHORT();

    SET @cnt = (SELECT count(*) 
                FROM information_schema.tables
                WHERE table_name IN
                    ('trams_fms_compare_trams_final',
                     'trams_fms_compare_team_final'
                    )
               );
    
    IF @cnt > 0 THEN
      CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'Found existing temp tables in the database. Please remove those tables before running this stored procedure', 1);
    END IF;   
    
    /* **************************************************************************************************
    Notes:
       FMS Compare report will have two parts: One for TrAMS data, One for TEAM data;
       For TrAMS transaction data, the transaction data should be grouped by contract_award_id, po_number, cost_center_code, account_class_code, fpc, scope_code, scope_suffix, project_number_sequence_number;
       For TEAM transaction data, the transaction data should be grouped by grouped by contract_award_id, po_number, cost_center_code, account_class_code, fpc;

    FMS report for TrAMS:
       Step 1: Get Current FY transactions roll up data
       Step 2: Get Current FY deobligation roll up data
       Step 3: Get All FY transactions roll up data
       Step 4: Get FMS report for TrAMS
       Step 5: Get close date
       Step 6: Update TrAMS FMS report table with dates
       Step 7: Create final FMS Compare report for TrAMS data
    **************************************************************************************************** */ 
    /* Step 1: Get Current FY transactions roll up data*/
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_trams_current_fy_transaction table', 1);
	
	INSERT INTO `trams_temp_fms_trams_current_fy_transaction` (
	SELECT 
		   NULL,
		   t.contract_award_id,
           t.po_number,
           t.cost_center_code,
           t.account_class_code,
           t.fpc,
           t.scope_code,
           t.scope_suffix,
           p.project_number_sequence_number,
           ifnull(sum(t.obligation_amount), 0) as obligation_amount,
           ifnull(sum(t.deobligation_amount), 0) as deobligation_amount,
           ifnull(sum(t.disbursement_amount), 0) as disbursement_amount,
           ifnull(sum(t.refund_amount), 0) as refund_amount,
           ifnull(sum(t.authorized_disbursement_amount), 0) as authorized_disbursement_amount,
		   @uuid AS uuid,
		   @sid AS time_stamp 
    FROM trams_contract_acc_transaction t
    LEFT JOIN trams_project p ON p.id = t.project_id
    WHERE t.transaction_date_time >= _current_fy_beginning_datetime
      AND t.from_trams = 1
    GROUP BY t.contract_award_id, t.po_number, t.cost_center_code, t.account_class_code, t.fpc, t.scope_code, t.scope_suffix, p.project_number_sequence_number
	);

    /* Step 2: Get Current FY deobligation roll up data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_trams_current_fy_deobligation', 1);
	
	INSERT INTO `trams_temp_fms_trams_current_fy_deobligation` (
	SELECT 
		   NULL,
		   a.contractAward_id as contract_award_id,
           d.po_number as po_number,
           d.acc_cost_center_code as cost_center_code,
           d.account_class_code as account_class_code,
           d.acc_fpc as fpc,
           d.scope_number as scope_code,
           d.scope_suffix as scope_suffix,
           d.project_number_sequence_number as project_number_sequence_number,
           ifnull(sum(d.deobligated_recovery_amount), 0) as deobligation_amount,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_deobligation_detail d
    LEFT JOIN trams_application a ON a.id = d.amendment_id
    WHERE d.deobligated_recovery_date_time < _current_fy_beginning_datetime
      AND d.deobligated_recovery_type IN ('Current Year Deob', 'Discretionary', 'Lapsed Year Deob', 'Non-Lapsed Deob')
    GROUP BY a.contractAward_id, d.po_number, d.acc_cost_center_code, d.account_class_code, d.acc_fpc, d.scope_number, d.scope_suffix, d.project_number_sequence_number
	);

    /* Step 3: Get All FY transactions roll up data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_trams_all_fy_transaction table', 1);
	
	INSERT INTO `trams_temp_fms_trams_all_fy_transaction` (
	SELECT 
		   NULL,
		   t.contract_award_id,
           t.po_number,
           t.cost_center_code,
           t.account_class_code,
           t.fpc,
           t.scope_code,
           t.scope_suffix,
           p.project_number_sequence_number,
           t.object_class,
           t.dafis_suffix_code,
           t.from_trams,
           ifnull(sum(t.obligation_amount), 0) as cm0_obligation,
           ifnull(sum(t.deobligation_amount), 0) as cm0_deobs,
           ifnull(sum(t.disbursement_amount), 0) as cm0_payment,
           ifnull(sum(t.refund_amount), 0) as cm0_refunds,
           ifnull(sum(t.authorized_disbursement_amount), 0) as cm0_auth_disb,
           ifnull(sum(t.obligation_amount), 0) - ifnull(c.obligation_amount, 0) as py0_obligation,
           ifnull(sum(t.deobligation_amount), 0) - ifnull(d.deobligation_amount, 0) as py0_deobs,
           ifnull(sum(t.disbursement_amount), 0) - ifnull(c.disbursement_amount, 0) as py0_payment,
           ifnull(sum(t.refund_amount), 0) - ifnull(c.refund_amount, 0) as py0_refunds,
           0.0 as py0_auth_disb, /* Do not attempt to calculate the prior year authorized disbursements */
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_contract_acc_transaction t
    LEFT JOIN trams_project p ON p.id = t.project_id
    LEFT JOIN trams_temp_fms_trams_current_fy_transaction c ON c.contract_award_id = t.contract_award_id
      AND c.po_number = t.po_number
      AND c.cost_center_code = t.cost_center_code
      AND c.account_class_code = t.account_class_code
      AND c.fpc = t.fpc
      AND c.scope_code = t.scope_code
      AND c.scope_suffix = t.scope_suffix
      AND c.project_number_sequence_number = p.project_number_sequence_number
	  AND c.uuid = @uuid
    LEFT JOIN trams_temp_fms_trams_current_fy_deobligation d ON d.contract_award_id = t.contract_award_id
      AND d.po_number = t.po_number
      AND d.cost_center_code = t.cost_center_code
      AND d.account_class_code = t.account_class_code
      AND d.fpc = t.fpc
      AND d.scope_code = t.scope_code
      AND d.scope_suffix = t.scope_suffix
      AND d.project_number_sequence_number = p.project_number_sequence_number
	  AND d.uuid = @uuid
    WHERE t.from_trams = 1
    GROUP BY t.contract_award_id, t.po_number, t.cost_center_code, t.account_class_code, t.fpc, t.scope_code, t.scope_suffix, p.project_number_sequence_number
	);
           
    /* Step 4: Get FMS report for TrAMS */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_trams_compare', 1);
	
	INSERT INTO `trams_temp_fms_trams_compare` (
	SELECT 
		   NULL,
		   f.contract_award_id as contract_award_id,
           c.current_amendment_number as current_amendment_number,
           f.account_class_code as account_class_code,
           f.cost_center_code as cost_center_code,
           f.fpc as fpc,
           f.scope_number as scope_number,
           f.scope_suffix as scope_suffix,
           f.application_number as application_number,
           f.project_number as project_number,
           f.project_number_sequence_number as project_number_sequence_number,
           ifnull(nullif(f.object_class, ''), t.object_class) as object_class,
           f.po_number as po_number,
           t.dafis_suffix_code as dafis_suffix_code,
           f.funding_fiscal_year as funding_fiscal_year,
           f.appropriation_code as appropriation_code,
           f.section_code as section_code,
           f.limitation_code as limitation_code,
           f.authorization_type as authorization_type,
           a.recipient_cost_center as org_code,
           cast(c.recipient_id as char(4)) as recipient_id,
           date(null) as reserv_date,
           date(c.first_obligation_date) as first_ob_date,
           date(c.last_obligation_date) as last_ob_date,
           max(date(f.last_disbursement_date)) as last_disb_date,
           date(null) as close_date,
           ifnull(t.cm0_obligation, 0) as cm0_obligation,
           ifnull(t.cm0_deobs, 0) as cm0_deobs,
           ifnull(t.cm0_payment, 0) as cm0_payment,
           ifnull(t.cm0_refunds, 0) as cm0_refunds,
           ifnull(t.cm0_auth_disb, 0) as cm0_auth_disb,
           ifnull(t.py0_obligation, 0) as py0_obligation,
           ifnull(t.py0_deobs, 0) as py0_deobs,
           ifnull(t.py0_payment, 0) as py0_payment,
           ifnull(t.py0_refunds, 0) as py0_refunds,
           ifnull(t.py0_auth_disb, 0) as py0_auth_disb,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_contract_acc_funding f
    LEFT JOIN trams_contract_award c ON c.id = f.contract_award_id
    LEFT JOIN trams_application a ON a.contractAward_id = f.contract_award_id
    LEFT JOIN trams_temp_fms_trams_all_fy_transaction t ON t.contract_award_id = f.contract_award_id
      AND t.po_number = f.po_number
      AND t.cost_center_code = f.cost_center_code
      AND t.account_class_code = f.account_class_code
      AND t.fpc = f.fpc
      AND t.scope_code = f.scope_number
      AND t.scope_suffix = f.scope_suffix
      AND t.project_number_sequence_number = f.project_number_sequence_number
	  AND t.uuid = @uuid
    WHERE a.app_from_team_flag <> 1
      AND c.application_type IN ('Grant','Cooperative Agreement')
    GROUP BY f.contract_award_id, f.po_number, f.cost_center_code, f.account_class_code, f.fpc, f.scope_number, f.scope_suffix, f.project_number_sequence_number
	);
    
    /* Step 5: Get close date */
    /* Create a temp table for close date for trams data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_trams_all_fy_close_date table', 1);
    
    INSERT INTO `trams_temp_fms_trams_all_fy_close_date`(
	SELECT 
		   NULL,
		   a.contractAward_id as contract_award_id,
           max(ad.fta_closeout_approval_date_time) as close_date,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_application a
    INNER JOIN trams_application_award_detail ad ON ad.application_id = a.id
    GROUP BY a.contractAward_id
	);

    /* Step 6: Update TrAMS FMS report table with dates */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'update trams_temp_fms_trams_compare with dates', 1);

    UPDATE trams_temp_fms_trams_compare c
    LEFT JOIN trams_reservation r ON r.application_id = c.current_amendment_number
    SET c.reserv_date = date(r.reservation_finalized_date_time)
	WHERE c.uuid = @uuid;

    UPDATE trams_temp_fms_trams_compare c
    LEFT JOIN trams_temp_fms_trams_all_fy_close_date d ON d.contract_award_id = c.contract_award_id
	AND d.uuid = @uuid
    SET c.close_date = date(d.close_date)
	WHERE c.uuid = @uuid;
    
    /* Step 7: Create final FMS Compare report for TrAMS data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'create trams_fms_compare_trams_final table', 1);

    CREATE TABLE trams_fms_compare_trams_final(
      run_no varchar(255),
      doc_id_type varchar(255),
      doc_id_num varchar(255),
      doc_id_ffy varchar(255),
      doc_id_suffix varchar(255),
      obj_clas varchar(255),
      approp_code varchar(255),
      approp_code_lim varchar(255),
      cost_cntr varchar(255),
      org_code varchar(255),
      fpc varchar(255),
      prog_elem varchar(255),
      public_gov varchar(255),
      echono varchar(255),
      vendor_ssn varchar(255),
      reserv_date varchar(255),
      first_ob_date varchar(255),
      last_ob_date varchar(255),
      last_disb_date varchar(255),
      close_date varchar(255),
      cm0_obligation double,
      cm0_deobs double,
      cm0_payment double,
      cm0_refunds double,
      cm0_auth_disb double,
      py0_obligation double,
      py0_deobs double,
      py0_payment double,
      py0_refunds double,
      py0_auth_disb double,
      application_fain varchar(255),
      project_identifier varchar(255),
      scope_code varchar(255),
      scope_suffix varchar(255)
    );

    INSERT INTO trams_fms_compare_trams_final
    SELECT '000' as run_no,
           '26'  as doc_id_type,
           replace(ifnull(t.po_number, ''), '-', '') as doc_id_num,
           right(t.funding_fiscal_year, 2) as doc_id_ffy, 
           concat(right(concat('0', ifnull(t.fpc, '')), 1), right(concat('00', ifnull(a.dafis_suffix_code, '')), 2)) as doc_id_suffix,
           t.object_class as obj_clas,         
           t.appropriation_code as approp_code, 
           concat(t.authorization_type, right(t.funding_fiscal_year, 2)) as approp_code_lim,
           t.cost_center_code as cost_cntr,
           left(t.org_code, 3) as org_code,
           t.fpc as fpc, 
           concat(t.section_code, t.limitation_code) as prog_elem,
           case when LOCATE('Federal Agency', gb.organizationType) > 0 then 'G' else 'P' end as public_gov,  
           if(gb.requFlag = 1, 'REQU', if(gb.opacFlag = 1, 'OPAC', if(gb.wcfFlag = 1, 'WCF', if(gb.tscFlag = 1, 'TSC', if(ifnull(gb.echoNumber, '') <> '', gb.echoNumber, ''))))) as echono,
           t.recipient_id as vendor_ssn,
           DATE_FORMAT(ifnull(t.reserv_date, '0000-00-00'), "%m%d%y")  as reserv_date,
           DATE_FORMAT(ifnull(t.first_ob_date, '0000-00-00'), "%m%d%y") as first_ob_date,
           DATE_FORMAT(ifnull(t.last_ob_date, '0000-00-00'), "%m%d%y")  as last_ob_date,
           DATE_FORMAT(ifnull(t.last_disb_date, '0000-00-00'), "%m%d%y")  as last_disb_date,
           DATE_FORMAT(ifnull(t.close_date, '0000-00-00'), "%m%d%y") as close_date,
           ifnull(t.cm0_obligation, 0) as cm0_obligation,
           ifnull(t.cm0_deobs, 0) as cm0_deobs,
           ifnull(t.cm0_payment, 0) as cm0_payment,
           ifnull(t.cm0_refunds, 0) as cm0_refunds,
           ifnull(t.cm0_auth_disb, 0) as cm0_auth_disb,
           ifnull(t.py0_obligation, 0) as py0_obligation,
           ifnull(t.py0_deobs, 0) as py0_deobs,
           ifnull(t.py0_payment, 0) as py0_payment,
           ifnull(t.py0_refunds, 0) as py0_refunds,
           ifnull(t.py0_auth_disb, 0) as py0_auth_disb,
           left(replace(ifnull(t.application_number, ''), '-', ''), 9) as application_fain,
           left(replace(ifnull(t.project_number, ''), '-', ''), 11) as project_identifier,
           replace(ifnull(t.scope_number, ''), '-', '') as scope_code,
           t.scope_suffix as scope_suffix
    FROM trams_temp_fms_trams_compare t
    LEFT JOIN vwtramsGeneralBusinessInfo gb ON gb.recipientId = t.recipient_id
    LEFT JOIN trams_account_class_code_lookup a ON a.account_class_code = t.account_class_code
	WHERE t.uuid = @uuid
    ORDER BY t.trams_temp_fms_trams_compare_id;
    

    /* **************************************************************************************************
    FMS report for TEAM:
       Step 1: Get Current FY transactions roll up data
       Step 2: Get Current FY deobligation roll up data
       Step 3: Get All FY transactions roll up data
       Step 4: Get FMS report for TrAMS
       Step 5: Get close date
       Step 6: Update TrAMS FMS report table with dates
       Step 7: Create final FMS Compare report for TrAMS data
    **************************************************************************************************** */ 
    
    /* Step 1: Get Current FY transactions roll up data*/
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_team_current_fy_transaction', 1);
	
	INSERT INTO `trams_temp_fms_team_current_fy_transaction` (
	SELECT 
		   NULL,
		   contract_award_id,
           po_number,
           cost_center_code,
           account_class_code,
           fpc,
           ifnull(sum(obligation_amount), 0) as obligation_amount,
           ifnull(sum(deobligation_amount), 0) as deobligation_amount,
           ifnull(sum(disbursement_amount), 0) as disbursement_amount,
           ifnull(sum(refund_amount), 0) as refund_amount,
           ifnull(sum(authorized_disbursement_amount), 0) as authorized_disbursement_amount,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_contract_acc_transaction
    WHERE transaction_date_time >= _current_fy_beginning_datetime
      AND from_trams <> 1
    GROUP BY contract_award_id, po_number, cost_center_code, account_class_code, fpc
	);

    /* Step 2: Get Current FY deobligation roll up data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_team_current_fy_deobligation', 1);
	
	INSERT INTO `trams_temp_fms_team_current_fy_deobligation`(
		SELECT 
		   NULL,
		   a.contractAward_id as contract_award_id,
           d.po_number as po_number,
           d.acc_cost_center_code as cost_center_code,
           d.account_class_code as account_class_code,
           d.acc_fpc as fpc,
           ifnull(sum(d.deobligated_recovery_amount), 0) as deobligation_amount,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_deobligation_detail d
    LEFT JOIN trams_application a ON a.id = d.amendment_id
    WHERE d.deobligated_recovery_date_time < _current_fy_beginning_datetime
      AND d.deobligated_recovery_type IN ('Current Year Deob', 'Discretionary', 'Lapsed Year Deob', 'Non-Lapsed Deob')
    GROUP BY a.contractAward_id, d.po_number, d.acc_cost_center_code, d.account_class_code, d.acc_fpc
	);
    
    /* Step 3: Get All FY transactions roll up data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_team_all_fy_transaction table', 1);
    
	INSERT INTO `trams_temp_fms_team_all_fy_transaction` (
	SELECT 
		   NULL,
		   t.contract_award_id,
           t.po_number,
           t.cost_center_code,
           t.account_class_code,
           t.fpc,
           t.object_class,
           t.dafis_suffix_code,
           t.from_trams,
           ifnull(sum(t.obligation_amount), 0) as cm0_obligation,
           ifnull(sum(t.deobligation_amount), 0) as cm0_deobs,
           ifnull(sum(t.disbursement_amount), 0) as cm0_payment,
           ifnull(sum(t.refund_amount), 0) as cm0_refunds,
           ifnull(sum(t.authorized_disbursement_amount), 0) as cm0_auth_disb,
           ifnull(sum(t.obligation_amount), 0) - ifnull(c.obligation_amount, 0) as py0_obligation,
           ifnull(sum(t.deobligation_amount), 0) - ifnull(d.deobligation_amount, 0) as py0_deobs,
           ifnull(sum(t.disbursement_amount), 0) - ifnull(c.disbursement_amount, 0) as py0_payment,
           ifnull(sum(t.refund_amount), 0) - ifnull(c.refund_amount, 0) as py0_refunds,
           0.0 as py0_auth_disb, /* Do not attempt to calculate the prior year authorized disbursements */
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_contract_acc_transaction t
    LEFT JOIN trams_temp_fms_team_current_fy_transaction c ON c.contract_award_id = t.contract_award_id
      AND c.po_number = t.po_number
      AND c.cost_center_code = t.cost_center_code
      AND c.account_class_code = t.account_class_code
      AND c.fpc = t.fpc
	  AND c.uuid = @uuid
    LEFT JOIN trams_temp_fms_team_current_fy_deobligation d ON d.contract_award_id = t.contract_award_id
      AND d.po_number = t.po_number
      AND d.cost_center_code = t.cost_center_code
      AND d.account_class_code = t.account_class_code
      AND d.fpc = t.fpc
	  AND d.uuid = @uuid
    WHERE t.from_trams <> 1
    GROUP BY t.contract_award_id, t.po_number, t.cost_center_code, t.account_class_code, t.fpc

	);	
	
	/* Step 4: Get FMS report for TEAM */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_team_compare table', 1);
	
	INSERT INTO `trams_temp_fms_team_compare`(
	SELECT 
		   NULL,	
		   f.contract_award_id as contract_award_id,
           c.current_amendment_number as current_amendment_number,
           f.account_class_code as account_class_code,
           f.cost_center_code as cost_center_code,
           f.fpc as fpc,
           ifnull(nullif(f.object_class, ''), t.object_class) as object_class,
           f.po_number as po_number,
           t.dafis_suffix_code as dafis_suffix_code,
           f.funding_fiscal_year as funding_fiscal_year,
           f.appropriation_code as appropriation_code,
           f.section_code as section_code,
           f.limitation_code as limitation_code,
           f.authorization_type as authorization_type,
           a.recipient_cost_center as org_code,
           cast(c.recipient_id as char(4)) as recipient_id,
           date(null) as reserv_date,
           date(c.first_obligation_date) as first_ob_date,
           date(c.last_obligation_date) as last_ob_date,
           max(date(f.last_disbursement_date)) as last_disb_date,
           date(null) as close_date,
           ifnull(t.cm0_obligation, 0) as cm0_obligation,
           ifnull(t.cm0_deobs, 0) as cm0_deobs,
           ifnull(t.cm0_payment, 0) as cm0_payment,
           ifnull(t.cm0_refunds, 0) as cm0_refunds,
           ifnull(t.cm0_auth_disb, 0) as cm0_auth_disb,
           ifnull(t.py0_obligation, 0) as py0_obligation,
           ifnull(t.py0_deobs, 0) as py0_deobs,
           ifnull(t.py0_payment, 0) as py0_payment,
           ifnull(t.py0_refunds, 0) as py0_refunds,
           ifnull(t.py0_auth_disb, 0) as py0_auth_disb,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_contract_acc_funding f
    LEFT JOIN trams_contract_award c ON c.id = f.contract_award_id
    LEFT JOIN trams_application a ON a.contractAward_id = f.contract_award_id
    LEFT JOIN trams_temp_fms_team_all_fy_transaction t ON t.contract_award_id = f.contract_award_id
      AND t.po_number = f.po_number
      AND t.cost_center_code = f.cost_center_code
      AND t.account_class_code = f.account_class_code
      AND t.fpc = f.fpc
	  AND t.uuid = @uuid
    WHERE a.app_from_team_flag = 1
      AND c.application_type IN ('Grant','Cooperative Agreement')
    GROUP BY f.contract_award_id, f.po_number, f.cost_center_code, f.account_class_code, f.fpc
	);
    
    /* Step 5: Get close date */
    /* Create a temp table for close date for team data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'insert trams_temp_fms_team_all_fy_close_date table', 1);
	
	INSERT INTO `trams_temp_fms_team_all_fy_close_date`(
	SELECT 
		   NULL,
		   a.contractAward_id as contract_award_id,
           max(ad.fta_closeout_approval_date_time) as close_date,
		   @uuid AS uuid,
		   @sid AS time_stamp
    FROM trams_application a
    INNER JOIN trams_application_award_detail ad ON ad.application_id = a.id
    GROUP BY a.contractAward_id
	);
    
    /* Step 6: Update TEAM FMS report table with dates */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'update trams_temp_fms_team_compare with dates', 1);

    UPDATE trams_temp_fms_team_compare c
    LEFT JOIN trams_reservation r ON r.application_id = c.current_amendment_number
    SET c.reserv_date = date(r.reservation_finalized_date_time)
	WHERE c.uuid = @uuid;

    UPDATE trams_temp_fms_team_compare c
    LEFT JOIN trams_temp_fms_team_all_fy_close_date d ON d.contract_award_id = c.contract_award_id
	AND d.uuid = @uuid
    SET c.close_date = date(d.close_date)
	WHERE c.uuid = @uuid;
    
    /* Step 7: Create final FMS Compare report for TEAM data */
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'create trams_fms_compare_team_final table', 1);

    CREATE TABLE trams_fms_compare_team_final(
      run_no varchar(255),
      doc_id_type varchar(255),
      doc_id_num varchar(255),
      doc_id_ffy varchar(255),
      doc_id_suffix varchar(255),
      obj_clas varchar(255),
      approp_code varchar(255),
      approp_code_lim varchar(255),
      cost_cntr varchar(255),
      org_code varchar(255),
      fpc varchar(255),
      prog_elem varchar(255),
      public_gov varchar(255),
      echono varchar(255),
      vendor_ssn varchar(255),
      reserv_date varchar(255),
      first_ob_date varchar(255),
      last_ob_date varchar(255),
      last_disb_date varchar(255),
      close_date varchar(255),
      cm0_obligation double,
      cm0_deobs double,
      cm0_payment double,
      cm0_refunds double,
      cm0_auth_disb double,
      py0_obligation double,
      py0_deobs double,
      py0_payment double,
      py0_refunds double,
      py0_auth_disb double
    );

    INSERT INTO trams_fms_compare_team_final
    SELECT '000' as run_no,
           '26'  as doc_id_type,
           replace(ifnull(t.po_number, ''), '-', '') as doc_id_num,
           right(t.funding_fiscal_year, 2) as doc_id_ffy, 
           concat(right(concat('0', ifnull(t.fpc, '')), 1), right(concat('00', ifnull(a.dafis_suffix_code, '')), 2)) as doc_id_suffix,
           t.object_class as obj_clas,         
           t.appropriation_code as approp_code,
           concat(t.authorization_type, right(t.funding_fiscal_year, 2)) as approp_code_lim,
           t.cost_center_code as cost_cntr,
           left(t.org_code, 3) as org_code,
           t.fpc as fpc, 
           concat(t.section_code, t.limitation_code) as prog_elem,
           case when LOCATE('Federal Agency', gb.organizationType) > 0 then 'G' else 'P' end as public_gov,  
           if(gb.requFlag = 1, 'REQU', if(gb.opacFlag = 1, 'OPAC', if(gb.wcfFlag = 1, 'WCF', if(gb.tscFlag = 1, 'TSC', if(ifnull(gb.echoNumber, '') <> '', gb.echoNumber, ''))))) as echono,
           t.recipient_id as vendor_ssn,
           DATE_FORMAT(ifnull(t.reserv_date, '0000-00-00'), "%m%d%y")  as reserv_date,
           DATE_FORMAT(ifnull(t.first_ob_date, '0000-00-00'), "%m%d%y") as first_ob_date,
           DATE_FORMAT(ifnull(t.last_ob_date, '0000-00-00'), "%m%d%y")  as last_ob_date,
           DATE_FORMAT(ifnull(t.last_disb_date, '0000-00-00'), "%m%d%y")  as last_disb_date,
           DATE_FORMAT(ifnull(t.close_date, '0000-00-00'), "%m%d%y") as close_date,
           ifnull(t.cm0_obligation, 0) as cm0_obligation,
           ifnull(t.cm0_deobs, 0) as cm0_deobs,
           ifnull(t.cm0_payment, 0) as cm0_payment,
           ifnull(t.cm0_refunds, 0) as cm0_refunds,
           ifnull(t.cm0_auth_disb, 0) as cm0_auth_disb,
           ifnull(t.py0_obligation, 0) as py0_obligation,
           ifnull(t.py0_deobs, 0) as py0_deobs,
           ifnull(t.py0_payment, 0) as py0_payment,
           ifnull(t.py0_refunds, 0) as py0_refunds,
           ifnull(t.py0_auth_disb, 0) as py0_auth_disb
    FROM trams_temp_fms_team_compare t
    LEFT JOIN vwtramsGeneralBusinessInfo gb ON gb.recipientId = t.recipient_id
    LEFT JOIN trams_account_class_code_lookup a ON a.account_class_code = t.account_class_code
	WHERE t.uuid = @uuid
    ORDER BY t.trams_temp_fms_team_compare_id;
	
	DELETE FROM trams_temp_fms_trams_current_fy_transaction WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_trams_current_fy_deobligation WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_trams_all_fy_transaction WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_trams_compare WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_trams_all_fy_close_date WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_team_current_fy_transaction WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_team_current_fy_deobligation WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_team_all_fy_transaction WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_team_compare WHERE uuid = @uuid;
	DELETE FROM trams_temp_fms_team_all_fy_close_date WHERE uuid = @uuid;
    
    CALL `trams_log`(@sid, 'trams_build_fms_compare_report_data', 'done', 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_civil_rights_status_report_query`(
IN `p_recipientId` VARCHAR(30), 
IN `p_acronym` VARCHAR(30), 
IN `p_state_code` VARCHAR(30),  
IN `p_cost_center_code` VARCHAR(255),  
IN `p_title_vi_flag` INTEGER,
IN `p_dbe_goals_flag` INTEGER,
IN `p_dbe_program_flag` INTEGER,
IN `p_eeo_program_flag` INTEGER,

IN `p_Title_VI_Program_Status` VARCHAR(255),
IN `p_DBE_Goals_Status` VARCHAR(255),
IN `p_DBE_Program_Status` VARCHAR(255),
IN `p_EEO_Program_Status` VARCHAR(255),

IN `p_Overall_Goals_Amount` VARCHAR(20),
IN `p_Overall_Goals_Compare` VARCHAR(2),
IN `p_Overall_Goals_Amount_End` VARCHAR(20),

IN `p_Title_VI_Due_Date` VARCHAR(10),
IN `p_Title_VI_Due_Date_Compare` VARCHAR(2),
IN `p_Title_VI_Due_Date_End` VARCHAR(10),

IN `p_Title_VI_Concur_Date` VARCHAR(10),
IN `p_Title_VI_Concur_Date_Compare` VARCHAR(2),
IN `p_Title_VI_Concur_Date_End` VARCHAR(10),

IN `p_Title_VI_Submission_Date` VARCHAR(10),
IN `p_Title_VI_Submission_Date_Compare` VARCHAR(2),
IN `p_Title_VI_Submission_Date_End` VARCHAR(10),

IN `p_Title_VI_Expiration_Date` VARCHAR(10),
IN `p_Title_VI_Expiration_Date_Compare` VARCHAR(2),
IN `p_Title_VI_Expiration_Date_End` VARCHAR(10),

IN `p_DBE_Goals_Due_Date` VARCHAR(10),
IN `p_DBE_Goals_Due_Date_Compare` VARCHAR(2),
IN `p_DBE_Goals_Due_Date_End` VARCHAR(10),

IN `p_DBE_Goals_Submission_Date` VARCHAR(10),
IN `p_DBE_Goals_Submission_Date_Compare` VARCHAR(2),
IN `p_DBE_Goals_Submission_Date_End` VARCHAR(10),

IN `p_DBE_Goals_Expiration_Date` VARCHAR(10),
IN `p_DBE_Goals_Expiration_Date_Compare` VARCHAR(2),
IN `p_DBE_Goals_Expiration_Date_End` VARCHAR(10),

IN `p_DBE_Goals_Concur_Date` VARCHAR(10),
IN `p_DBE_Goals_Concur_Date_Compare` VARCHAR(2),
IN `p_DBE_Goals_Concur_Date_End` VARCHAR(10),

IN `p_DBE_Program_Due_Date` VARCHAR(10),
IN `p_DBE_Program_Due_Date_Compare` VARCHAR(2),
IN `p_DBE_Program_Due_Date_End` VARCHAR(10),

IN `p_DBE_Program_Submission_Date` VARCHAR(10),
IN `p_DBE_Program_Submission_Date_Compare` VARCHAR(2),
IN `p_DBE_Program_Submission_Date_End` VARCHAR(10),

IN `p_DBE_Program_Concur_Date` VARCHAR(10),
IN `p_DBE_Program_Concur_Date_Compare` VARCHAR(2),
IN `p_DBE_Program_Concur_Date_End` VARCHAR(10),

IN `p_EEO_Program_Due_Date` VARCHAR(10),
IN `p_EEO_Program_Due_Date_Compare` VARCHAR(2),
IN `p_EEO_Program_Due_Date_End` VARCHAR(10),

IN `p_EEO_Program_Submission_Date` VARCHAR(10),
IN `p_EEO_Program_Submission_Date_Compare` VARCHAR(2),
IN `p_EEO_Program_Submission_Date_End` VARCHAR(10),

IN `p_EEO_Program_Expiration_Date` VARCHAR(10),
IN `p_EEO_Program_Expiration_Date_Compare` VARCHAR(2),
IN `p_EEO_Program_Expiration_Date_End` VARCHAR(10),

IN `p_EEO_Program_Concur_Date` VARCHAR(10),
IN `p_EEO_Program_Concur_Date_Compare` VARCHAR(2),
IN `p_EEO_Program_Concur_Date_End` VARCHAR(10),

IN `p_start_page` VARCHAR(4),
IN `p_pages` VARCHAR(4),
IN `p_page_size` VARCHAR(4),
IN `p_mode` VARCHAR(10), IN `p_return_limit` VARCHAR(20))
    NO SQL
BEGIN
 
 DECLARE _spName varchar(255) DEFAULT "trams_get_civil_rights_status_report_query";
 DECLARE _sql VARCHAR(4096) DEFAULT "";
 DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
 DECLARE _limitClause VARCHAR(100) DEFAULT " ";
 DECLARE _limitSize INTEGER DEFAULT 100;  DECLARE _resultWhereClause VARCHAR(500) DEFAULT " ";
 DECLARE _outputWhereClause VARCHAR(500) DEFAULT " ";
 DECLARE _orderbyClause VARCHAR(100) DEFAULT " ";
 DECLARE _contract_award_id INTEGER DEFAULT NULL;
 DECLARE _cccWhere VARCHAR(4096) DEFAULT "";
 DECLARE _noRecordsText VARCHAR(50) DEFAULT "";

  DECLARE _SQLStateCode CHAR(5) DEFAULT '00000';
 DECLARE _ErrorNumber INTEGER;
 DECLARE _MessageText VARCHAR(1000) DEFAULT ' ';
	
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
 BEGIN
	GET DIAGNOSTICS CONDITION 1
		_SQLStateCode = RETURNED_SQLSTATE,
		_ErrorNumber = MYSQL_ERRNO,
		_MessageText = MESSAGE_TEXT;
	IF _ErrorNumber <> '00000' THEN
		CALL `trams_log`(@sid, _spName, CONCAT('SQL STATE/ERROR: ',IFNULL(_SQLStateCode,'?'),' ',IFNULL(_ErrorNumber,'?'),' ',IFNULL(_MessageText,'?')), 1);
	END IF;
 END;

 SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
 SET SESSION max_heap_table_size = 1024*1024*128;
 SET @cnt = 0;
 SET @p_mode = p_mode;
 
 IF p_pages IS NULL OR p_page_size IS NULL OR p_start_page IS NULL
 THEN
	CALL trams_get_sql_limit_clause(200,_limitClause);
 ELSE
	SET _limitSize = (p_start_page + p_pages) * p_page_size;
	CALL trams_get_sql_limit_clause(_limitSize,_limitClause);
 END IF;

 IF p_mode = 'Excel' THEN
	SET _noRecordsText = '-- No Records Found --';
 ELSE
	SET _noRecordsText = concat('-- No matches found in first ',_limitSize,' records --');
 END IF;
 
 DROP TEMPORARY TABLE IF EXISTS tmp_civil_rights_status_report_excel;
 
 SET _sql = "CREATE TEMPORARY TABLE tmp_civil_rights_status_report_excel
SELECT * FROM (
SELECT
 gbi.recipient_id AS 'GranteeId',
 gbi.recipient_acronym AS 'Grantee',
 gbi.legal_business_name AS 'Business Name',
 gbi.organization_type_code AS 'Business Type',
 tl.city AS 'City',
 gbi.cost_center_code AS 'Region Office',
 tl.state_or_province AS 'State Code'
 FROM trams_general_business_info gbi 
 JOIN trams_location AS tl ON gbi.recipient_id=tl.grantee_id AND tl.address_type='Physical Address'
              ";



 SET _whereClause = (SELECT IFNULL(CONCAT(" AND gbi.cost_center_code IN ('",NULLIF(p_cost_center_code,''),"')"),""));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND gbi.recipient_id IN (",NULLIF(p_recipientId,''),")"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND gbi.recipient_acronym IN ('",NULLIF(p_acronym,''),"')"),"")));

 SET _resultWhereClause = CONCAT(_resultWhereClause, (SELECT IFNULL(CONCAT(" WHERE `State Code` IN ('",NULLIF(p_state_code,''),"')"),"")));

 SET _orderbyClause = " ORDER BY `State Code`, gbi.cost_center_code, gbi.recipient_id "; 
 IF p_mode = 'Excel' THEN
   SET _limitClause = '';
 END IF;
 

 SET @applicationQuery = CONCAT(_sql,replace(_whereClause,'''''',''''),_orderbyClause,_limitClause,') AS result',_resultWhereClause,';');

 CALL `trams_log`(@sid, _spName, concat(now(),' Start CALL SP Inputs (',connection_id(),') : (',
IFNULL(`p_recipientId`,'NULL'),',',
IFNULL(`p_acronym`,'NULL'),',',
IFNULL(`p_state_code`,'NULL'),',',
IFNULL(`p_cost_center_code`,'NULL'),',',
IFNULL(`p_title_vi_flag`,'NULL'),',',
IFNULL(`p_dbe_goals_flag`,'NULL'),',',
IFNULL(`p_dbe_program_flag`,'NULL'),',',
IFNULL(`p_eeo_program_flag`,'NULL'),',',
IFNULL(`p_Title_VI_Program_Status`,'NULL'),',',
IFNULL(`p_DBE_Goals_Status`,'NULL'),',',
IFNULL(`p_DBE_Program_Status`,'NULL'),',',
IFNULL(`p_EEO_Program_Status`,'NULL'),',',

IFNULL(`p_Overall_Goals_Amount`,'NULL'),',',
IFNULL(`p_Overall_Goals_Compare`,'NULL'),',',
IFNULL(`p_Overall_Goals_Amount_End`,'NULL'),',',

IFNULL(`p_Title_VI_Due_Date`,'NULL'),',',
IFNULL(`p_Title_VI_Due_Date_Compare`,'NULL'),',',
IFNULL(`p_Title_VI_Due_Date_End`,'NULL'),',',
IFNULL(`p_Title_VI_Concur_Date`,'NULL'),',',
IFNULL(`p_Title_VI_Concur_Date_Compare`,'NULL'),',', 
IFNULL(`p_Title_VI_Concur_Date_End`,'NULL'),',', 
IFNULL(`p_Title_VI_Submission_Date`,'NULL'),',', 
IFNULL(`p_Title_VI_Submission_Date_Compare`,'NULL'),',', 
IFNULL(`p_Title_VI_Submission_Date_End`,'NULL'),',', 
IFNULL(`p_Title_VI_Expiration_Date`,'NULL'),',', 
IFNULL(`p_Title_VI_Expiration_Date_Compare`,'NULL'),',', 
IFNULL(`p_Title_VI_Expiration_Date_End`,'NULL'),',', 

IFNULL(`p_DBE_Goals_Due_Date`,'NULL'),',',
IFNULL(`p_DBE_Goals_Due_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Goals_Due_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Goals_Submission_Date`,'NULL'),',',
IFNULL(`p_DBE_Goals_Submission_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Goals_Submission_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Goals_Expiration_Date`,'NULL'),',',
IFNULL(`p_DBE_Goals_Expiration_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Goals_Expiration_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Goals_Concur_Date`,'NULL'),',',
IFNULL(`p_DBE_Goals_Concur_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Goals_Concur_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Program_Due_Date`,'NULL'),',',
IFNULL(`p_DBE_Program_Due_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Program_Due_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Program_Submission_Date`,'NULL'),',',
IFNULL(`p_DBE_Program_Submission_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Program_Submission_Date_End`,'NULL'),',',

IFNULL(`p_DBE_Program_Concur_Date`,'NULL'),',',
IFNULL(`p_DBE_Program_Concur_Date_Compare`,'NULL'),',',
IFNULL(`p_DBE_Program_Concur_Date_End`,'NULL'),',',

IFNULL(`p_EEO_Program_Due_Date`,'NULL'),',',
IFNULL(`p_EEO_Program_Due_Date_Compare`,'NULL'),',',
IFNULL(`p_EEO_Program_Due_Date_End`,'NULL'),',',

IFNULL(`p_EEO_Program_Submission_Date`,'NULL'),',',
IFNULL(`p_EEO_Program_Submission_Date_Compare`,'NULL'),',',
IFNULL(`p_EEO_Program_Submission_Date_End`,'NULL'),',',

IFNULL(`p_EEO_Program_Expiration_Date`,'NULL'),',',
IFNULL(`p_EEO_Program_Expiration_Date_Compare`,'NULL'),',',
IFNULL(`p_EEO_Program_Expiration_Date_End`,'NULL'),',',

IFNULL(`p_EEO_Program_Concur_Date`,'NULL'),',',
IFNULL(`p_EEO_Program_Concur_Date_Compare`,'NULL'),',',
IFNULL(`p_EEO_Program_Concur_Date_End`,'NULL'),',',

IFNULL(`p_start_page`,'NULL'),',',
IFNULL(`p_pages`,'NULL'),',',
IFNULL(`p_page_size`,'NULL'),',',
IFNULL(`p_mode`,'NULL'),',',
IFNULL(`p_return_limit`,'NULL'),')'), 1);  

 CALL `trams_log`(@sid, _spName, CONCAT('Data Selection Query: ',@applicationQuery), 1);
 
 PREPARE applicationStmt FROM @applicationQuery;
 EXECUTE applicationStmt;


 SET @appCount=(SELECT COUNT(*) FROM tmp_civil_rights_status_report_excel);
 CALL `trams_log`(@sid, _spName, CONCAT(now(),' Total Matched Entities: ',@appCount), 1);

 IF @appCount > 0 THEN

	 CREATE INDEX tmp_grantee_id ON tmp_civil_rights_status_report_excel(`State Code`(2),`Region Office`(5),GranteeId(5));
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `Cycle Group` VARCHAR(20) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `Overall Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Race Neutral Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Race Conscious Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Status` VARCHAR(100) NULL AFTER `state code`;

 	 UPDATE `tmp_civil_rights_status_report_excel` crsr
	 SET crsr.`Cycle Group` =
	 (SELECT cycle_group FROM trams_civil_rights_compliance tcrc where tcrc.recipient_id = crsr.GranteeId and cycle_group > ' ' LIMIT 1);

 	 UPDATE `tmp_civil_rights_status_report_excel` crsr
	  INNER JOIN 
	  (
	   SELECT recipient_id, program_name, ifnull(dbe_race_neutral_goal_percentage,0.00) AS `Neutral Goal`, ifnull(dbe_race_conscious_goal_percentage,0.00) AS `Conscious Goal` 
	   FROM trams_civil_rights_compliance
	  ) tcrc ON tcrc.recipient_id = crsr.GranteeId AND tcrc.program_name = 'DBE Goal'

	SET crsr.`Overall Goal` = tcrc.`Neutral Goal` + tcrc.`Conscious Goal`;

 	  UPDATE `tmp_civil_rights_status_report_excel` crsr
	  INNER JOIN 
	  (
	   SELECT recipient_id, program_name, current_status, due_date_time, submitted_date_time, expiration_date_time, approved_date_time,
		  ifnull(dbe_race_neutral_goal_percentage,0.00) AS `Neutral Goal`, ifnull(dbe_race_conscious_goal_percentage,0.00) AS `Conscious Goal`  
	   FROM trams_civil_rights_compliance
	  ) tcrc ON tcrc.recipient_id = crsr.GranteeId AND tcrc.program_name = 'DBE Goal'

	  SET crsr.`DBE Goals Program Status` = tcrc.current_status, 
	      crsr.`DBE Goals Due Date` = ifnull(date(tcrc.due_date_time),' '),
 	      crsr.`DBE Goals Submission Date` = ifnull(date(tcrc.submitted_date_time),' '),
	      crsr.`DBE Goals Expiration Date` = ifnull(date(tcrc.expiration_date_time),' '),
	      crsr.`DBE Goals Concur Date` = ifnull(date(tcrc.approved_date_time),' '),
	      crsr.`DBE Race Conscious Goal` = `Conscious Goal`,
	      crsr.`DBE Race Neutral Goal` = `Neutral Goal`;

	 IF p_title_vi_flag = 1 THEN
 	  UPDATE `tmp_civil_rights_status_report_excel` crsr
	  INNER JOIN 
	  (
	   SELECT recipient_id, program_name, current_status, due_date_time, submitted_date_time, expiration_date_time, approved_date_time 
	   FROM trams_civil_rights_compliance
	  ) tcrc ON tcrc.recipient_id = crsr.GranteeId AND tcrc.program_name = 'Title VI Program'

	  SET crsr.`TitleVI Program Status` = tcrc.current_status, 
	      crsr.`TitleVI Due Date` = ifnull(date(tcrc.due_date_time),' '),
 	      crsr.`TitleVI Submission Date` = ifnull(date(tcrc.submitted_date_time),' '),
	      crsr.`TitleVI Expiration Date` = ifnull(date(tcrc.expiration_date_time),' '),
	      crsr.`TitleVI Concur Date` = ifnull(date(tcrc.approved_date_time),' ');
	 END IF;

	 IF p_dbe_program_flag = 1 THEN
 	  UPDATE `tmp_civil_rights_status_report_excel` crsr
	  INNER JOIN 
	  (
	   SELECT recipient_id, program_name, current_status, due_date_time, submitted_date_time, expiration_date_time, approved_date_time 
	   FROM trams_civil_rights_compliance
	  ) tcrc ON tcrc.recipient_id = crsr.GranteeId AND tcrc.program_name = 'DBE Program'

	  SET crsr.`DBE Program Status` = tcrc.current_status, 
	      crsr.`DBE Program Due Date` = ifnull(date(tcrc.due_date_time),' '),
 	      crsr.`DBE Program Submission Date` = ifnull(date(tcrc.submitted_date_time),' '),
	      crsr.`DBE Program Expiration Date` = ifnull(date(tcrc.expiration_date_time),' '),
	      crsr.`DBE Program Concur Date` = ifnull(date(tcrc.approved_date_time),' ');
	 END IF;

	 IF p_eeo_program_flag = 1 THEN
 	  UPDATE `tmp_civil_rights_status_report_excel` crsr
	  INNER JOIN 
	  (
	   SELECT recipient_id, program_name, current_status, due_date_time, submitted_date_time, expiration_date_time, approved_date_time 
	   FROM trams_civil_rights_compliance
	  ) tcrc ON tcrc.recipient_id = crsr.GranteeId AND tcrc.program_name = 'EEO Program'

	  SET crsr.`EEO Program Status` = tcrc.current_status, 
	      crsr.`EEO Program Due Date` = ifnull(date(tcrc.due_date_time),' '),
 	      crsr.`EEO Program Submission Date` = ifnull(date(tcrc.submitted_date_time),' '),
	      crsr.`EEO Program Expiration Date` = ifnull(date(tcrc.expiration_date_time),' '),
	      crsr.`EEO Program Concur Date` = ifnull(date(tcrc.approved_date_time),' ');
	 END IF;

	 SET @appCount=(SELECT COUNT(*) FROM tmp_civil_rights_status_report_excel);

	 IF @appCount < 1 THEN
		INSERT INTO tmp_civil_rights_status_report_excel (GranteeId, `Business Name`) VALUES ('0000',_noRecordsText);
	 END IF;
 ELSE
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `TitleVI Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `Cycle Group` VARCHAR(20) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `Overall Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Goals Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Race Neutral Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Race Conscious Goal` DOUBLE NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `DBE Program Status` VARCHAR(100) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Concur Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Due Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Expiration Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Submission Date` VARCHAR(15) NULL AFTER `state code`;
	 ALTER TABLE `tmp_civil_rights_status_report_excel` ADD `EEO Program Status` VARCHAR(100) NULL AFTER `state code`;

	 INSERT INTO tmp_civil_rights_status_report_excel (GranteeId, `Business Name`) VALUES ('0000',_noRecordsText);
 END IF;


 

 SET _outputWhereClause = 
	CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND crsr.`TitleVI Program Status` IN ('",NULLIF(p_Title_VI_Program_Status,''),"')"),"")));
 SET _outputWhereClause = 
	CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND crsr.`DBE Goals Program Status` IN ('",NULLIF(p_DBE_Goals_Status,''),"')"),"")));
 SET _outputWhereClause = 
	CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND crsr.`DBE Program Status` IN ('",NULLIF(p_DBE_Program_Status,''),"')"),"")));
 SET _outputWhereClause = 
	CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND crsr.`EEO Program Status` IN ('",NULLIF(p_EEO_Program_Status,''),"')"),"")));

 IF p_Title_VI_Due_Date_End IS NULL AND p_Title_VI_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Due Date`) ",IFNULL(p_Title_VI_Due_Date_Compare,"=")," '",NULLIF(p_Title_VI_Due_Date,''),"'"),"")));
 ELSE
  IF p_Title_VI_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Due Date`) ",">="," '",NULLIF(p_Title_VI_Due_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Due Date`) ","<="," '",NULLIF(p_Title_VI_Due_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_Title_VI_Concur_Date_End IS NULL AND p_Title_VI_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Concur Date`) ",IFNULL(p_Title_VI_Concur_Date_Compare,"=")," '",NULLIF(p_Title_VI_Concur_Date,''),"'"),"")));
 ELSE
  IF p_Title_VI_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Concur Date`) ",">="," '",NULLIF(p_Title_VI_Concur_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Concur Date`) ","<="," '",NULLIF(p_Title_VI_Concur_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_Title_VI_Submission_Date_End IS NULL AND p_Title_VI_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Submission Date`) ",IFNULL(p_Title_VI_Submission_Date_Compare,"=")," '",NULLIF(p_Title_VI_Submission_Date,''),"'"),"")));
 ELSE
  IF p_Title_VI_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Submission Date`) ",">="," '",NULLIF(p_Title_VI_Submission_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Submission Date`) ","<="," '",NULLIF(p_Title_VI_Submission_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_Title_VI_Expiration_Date_End IS NULL AND p_Title_VI_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Expiration Date`) ",IFNULL(p_Title_VI_Expiration_Date_Compare,"=")," '",NULLIF(p_Title_VI_Expiration_Date,''),"'"),"")));
 ELSE
  IF p_Title_VI_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Expiration Date`) ",">="," '",NULLIF(p_Title_VI_Expiration_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`TitleVI Expiration Date`) ","<="," '",NULLIF(p_Title_VI_Expiration_Date_End,''),"'"),"")));
  END IF;
 END IF;	


 IF p_DBE_Goals_Due_Date_End IS NULL AND p_DBE_Goals_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Due Date`) ",IFNULL(p_DBE_Goals_Due_Date_Compare,"=")," '",NULLIF(p_DBE_Goals_Due_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Goals_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Due Date`) ",">="," '",NULLIF(p_DBE_Goals_Due_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Due Date`) ","<="," '",NULLIF(p_DBE_Goals_Due_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_DBE_Goals_Concur_Date_End IS NULL AND p_DBE_Goals_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Concur Date`) ",IFNULL(p_DBE_Goals_Concur_Date_Compare,"=")," '",NULLIF(p_DBE_Goals_Concur_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Goals_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Concur Date`) ",">="," '",NULLIF(p_DBE_Goals_Concur_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Concur Date`) ","<="," '",NULLIF(p_DBE_Goals_Concur_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_DBE_Goals_Submission_Date_End IS NULL AND p_DBE_Goals_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Submission Date`) ",IFNULL(p_DBE_Goals_Submission_Date_Compare,"=")," '",NULLIF(p_DBE_Goals_Submission_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Goals_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Submission Date`) ",">="," '",NULLIF(p_DBE_Goals_Submission_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Submission Date`) ","<="," '",NULLIF(p_DBE_Goals_Submission_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_DBE_Goals_Expiration_Date_End IS NULL AND p_DBE_Goals_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Expiration Date`) ",IFNULL(p_DBE_Goals_Expiration_Date_Compare,"=")," '",NULLIF(p_DBE_Goals_Expiration_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Goals_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Expiration Date`) ",">="," '",NULLIF(p_DBE_Goals_Expiration_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Goals Expiration Date`) ","<="," '",NULLIF(p_DBE_Goals_Expiration_Date_End,''),"'"),"")));
  END IF;
 END IF;


 IF p_DBE_Program_Due_Date_End IS NULL AND p_DBE_Program_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Due Date`) ",IFNULL(p_DBE_Program_Due_Date_Compare,"=")," '",NULLIF(p_DBE_Program_Due_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Program_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Due Date`) ",">="," '",NULLIF(p_DBE_Program_Due_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Due Date`) ","<="," '",NULLIF(p_DBE_Program_Due_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_DBE_Program_Concur_Date_End IS NULL AND p_DBE_Program_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Concur Date`) ",IFNULL(p_DBE_Program_Concur_Date_Compare,"=")," '",NULLIF(p_DBE_Program_Concur_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Program_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Concur Date`) ",">="," '",NULLIF(p_DBE_Program_Concur_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Concur Date`) ","<="," '",NULLIF(p_DBE_Program_Concur_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_DBE_Program_Submission_Date_End IS NULL AND p_DBE_Program_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Submission Date`) ",IFNULL(p_DBE_Program_Submission_Date_Compare,"=")," '",NULLIF(p_DBE_Program_Submission_Date,''),"'"),"")));
 ELSE
  IF p_DBE_Program_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Submission Date`) ",">="," '",NULLIF(p_DBE_Program_Submission_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`DBE Program Submission Date`) ","<="," '",NULLIF(p_DBE_Program_Submission_Date_End,''),"'"),"")));
  END IF;
 END IF;




 IF p_EEO_Program_Due_Date_End IS NULL AND p_EEO_Program_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Due Date`) ",IFNULL(p_EEO_Program_Due_Date_Compare,"=")," '",NULLIF(p_EEO_Program_Due_Date,''),"'"),"")));
 ELSE
  IF p_EEO_Program_Due_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Due Date`) ",">="," '",NULLIF(p_EEO_Program_Due_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Due Date`) ","<="," '",NULLIF(p_EEO_Program_Due_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_EEO_Program_Concur_Date_End IS NULL AND p_EEO_Program_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Concur Date`) ",IFNULL(p_EEO_Program_Concur_Date_Compare,"=")," '",NULLIF(p_EEO_Program_Concur_Date,''),"'"),"")));
 ELSE
  IF p_EEO_Program_Concur_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Concur Date`) ",">="," '",NULLIF(p_EEO_Program_Concur_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Concur Date`) ","<="," '",NULLIF(p_EEO_Program_Concur_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_EEO_Program_Submission_Date_End IS NULL AND p_EEO_Program_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Submission Date`) ",IFNULL(p_EEO_Program_Submission_Date_Compare,"=")," '",NULLIF(p_EEO_Program_Submission_Date,''),"'"),"")));
 ELSE
  IF p_EEO_Program_Submission_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Submission Date`) ",">="," '",NULLIF(p_EEO_Program_Submission_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Submission Date`) ","<="," '",NULLIF(p_EEO_Program_Submission_Date_End,''),"'"),"")));
  END IF;
 END IF;

 IF p_EEO_Program_Expiration_Date_End IS NULL AND p_EEO_Program_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Expiration Date`) ",IFNULL(p_EEO_Program_Expiration_Date_Compare,"=")," '",NULLIF(p_EEO_Program_Expiration_Date,''),"'"),"")));
 ELSE
  IF p_EEO_Program_Expiration_Date IS NOT NULL THEN
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Expiration Date`) ",">="," '",NULLIF(p_EEO_Program_Expiration_Date,''),"'"),"")));
   SET _outputWhereClause = CONCAT(_outputWhereClause, (SELECT IFNULL(CONCAT(" AND date(crsr.`EEO Program Expiration Date`) ","<="," '",NULLIF(p_EEO_Program_Expiration_Date_End,''),"'"),"")));
  END IF;
 END IF;


 SET _outputWhereClause = replace(_outputWhereClause,'''''','''');


 IF _outputWhereClause > ' ' AND @appCount > 0 THEN
	DROP TEMPORARY TABLE IF EXISTS tmp_records_count;
	SET _sql = "CREATE TEMPORARY TABLE tmp_records_count
	SELECT
 	1 AS rowNumber,
	count(*) AS totalRecordsCnt

	FROM tmp_civil_rights_status_report_excel crsr 
	WHERE 1 ";
 	SET @applicationQuery = CONCAT(_sql,replace(_outputWhereClause,'''''',''''));

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	SET @outputCount = (SELECT totalRecordsCnt FROM tmp_records_count WHERE rowNumber = 1);
 ELSE
	SET @outputCount = @appCount;
 END IF;

 CALL `trams_log`(@sid, _spName, concat('Filter returns ',@outputCount,' records WHERE: [',_outputWhereClause,']'), 1);

 SET @curRow = 0;

 IF p_pages IS NULL OR p_page_size IS NULL OR p_start_page IS NULL
 THEN
	ALTER TABLE tmp_civil_rights_status_report_excel ADD rowNumber INTEGER DEFAULT 0;

	UPDATE `tmp_civil_rights_status_report_excel` crsr
	 SET crsr.rowNumber = (@curRow := @curRow + 1)
	 ORDER BY `State Code`, `Region Office`, GranteeId ASC;  

	IF p_mode = 'Excel' THEN
	    	SET @applicationQuery = "SELECT
		crsr.GranteeId,
		crsr.Grantee,
		crsr.`Business Name`,
		crsr.`Business Type`,
 		crsr.City,
		crsr.`Region Office`,
		crsr.`State Code`";

		IF p_title_vi_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`TitleVI Program Status`,
		trams_format_date(crsr.`TitleVI Due Date`) AS `TitleVI Due Date`,
		trams_format_date(crsr.`TitleVI Submission Date`) AS `TitleVI Submission Date`,
		trams_format_date(crsr.`TitleVI Expiration Date`) AS `TitleVI Expiration Date`,
		trams_format_date(crsr.`TitleVI Concur Date`) AS `TitleVI Concur Date`");
		END IF;
        
		IF p_dbe_goals_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`DBE Goals Program Status`");
		END IF;

		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`Cycle Group`,
		Cast(crsr.`Overall Goal` AS decimal(12,2) ) AS `Overall Goal`");

		IF p_dbe_goals_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		trams_format_date(crsr.`DBE Goals Due Date`) AS `DBE Goals Due Date`,
		trams_format_date(crsr.`DBE Goals Submission Date`) AS `DBE Goals Submission Date`,
		trams_format_date(crsr.`DBE Goals Expiration Date`) AS `DBE Goals Expiration Date`,
		trams_format_date(crsr.`DBE Goals Concur Date`) AS `DBE Goals Concur Date`");
		END IF;

		SET @applicationQuery = CONCAT(@applicationQuery,",
		Cast(crsr.`DBE Race Neutral Goal` AS decimal(12,2) ) AS `DBE Race Neutral Goal`,
		Cast(crsr.`DBE Race Conscious Goal` AS decimal(12,2) ) AS `DBE Race Conscious Goal`");

		IF p_dbe_program_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`DBE Program Status`,
		trams_format_date(crsr.`DBE Program Due Date`) AS `DBE Program Due Date`,
		trams_format_date(crsr.`DBE Program Submission Date`) AS `DBE Program Submission Date`,
--		trams_format_date(crsr.`DBE Program Expiration Date`) AS `DBE Program Expiration Date`,
		trams_format_date(crsr.`DBE Program Concur Date`) AS `DBE Program Concur Date`");
		END IF;

		IF p_eeo_program_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`EEO Program Status`,
		trams_format_date(crsr.`EEO Program Due Date`) AS `EEO Program Due Date`,
		trams_format_date(crsr.`EEO Program Submission Date`) AS `EEO Program Submission Date`,
		trams_format_date(crsr.`EEO Program Expiration Date`) AS `EEO Program Expiration Date`,
		trams_format_date(crsr.`EEO Program Concur Date`) AS `EEO Program Concur Date`");
		END IF;

		SET @applicationQuery = CONCAT(@applicationQuery,"
		FROM tmp_civil_rights_status_report_excel crsr WHERE 1 ",
		_outputWhereClause,
		" ORDER BY crsr.rowNumber ASC;");

      		IF @outputCount < 1 AND _outputWhereClause > ' ' THEN
		    SELECT concat('0000') AS 'GranteeId',concat('No records matched filter conditions') AS 'Business Name';
		ELSE

		PREPARE applicationStmt FROM @applicationQuery;
		EXECUTE applicationStmt;

      		END IF;

    	ELSE
	SET @applicationQuery = "SELECT
	crsr.GranteeId,
	crsr.Grantee,
	crsr.`Business Name`,
	crsr.`Business Type`,
	crsr.City,
	crsr.`Region Office`,
	crsr.`State Code`";

	IF p_title_vi_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`TitleVI Program Status`,
	crsr.`TitleVI Due Date`,
	crsr.`TitleVI Submission Date`,
	crsr.`TitleVI Expiration Date`,
	crsr.`TitleVI Concur Date`");
	END IF;
    
	IF p_dbe_goals_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Goals Program Status`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`Cycle Group`,
	crsr.`Overall Goal`");

	IF p_dbe_goals_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Goals Due Date`,
	crsr.`DBE Goals Submission Date`,
	crsr.`DBE Goals Expiration Date`,
	crsr.`DBE Goals Concur Date`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Race Neutral Goal`,
	crsr.`DBE Race Conscious Goal`");

	IF p_dbe_program_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Program Status`,
	crsr.`DBE Program Due Date`,
	crsr.`DBE Program Submission Date`,
--	crsr.`DBE Program Expiration Date`,
	crsr.`DBE Program Concur Date`");
	END IF;

	IF p_eeo_program_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`EEO Program Status`,
	crsr.`EEO Program Due Date`,
	crsr.`EEO Program Submission Date`,
	crsr.`EEO Program Expiration Date`,
	crsr.`EEO Program Concur Date`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,"
	FROM tmp_civil_rights_status_report_excel crsr WHERE 1 ",
	_outputWhereClause,
	" ORDER BY crsr.rowNumber ASC;");

   	IF @outputCount < 1 AND _outputWhereClause > ' ' THEN
	    SELECT concat('0000') AS 'GranteeId',concat('No records matched filter conditions') AS 'Business Name';
	ELSE


	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	END IF;

	END IF;
 ELSE
	ALTER TABLE tmp_civil_rights_status_report_excel ADD rowNumber INTEGER DEFAULT 0;
	UPDATE `tmp_civil_rights_status_report_excel` crsr
	 SET crsr.rowNumber = (@curRow := @curRow + 1)
	 ORDER BY `State Code`, `Region Office`, GranteeId ASC;  
	SET @startRow = p_start_page * p_page_size;
	
	SET @totalRows = (SELECT count(*) FROM tmp_civil_rights_status_report_excel);

	DROP TEMPORARY TABLE IF EXISTS tmp_records_count;

	IF @totalRows = _limitSize THEN
		SET _sql = "CREATE TEMPORARY TABLE tmp_records_count
	SELECT
 	1 AS rowNumber,
	count(*) AS totalRecordsCnt

	FROM trams_general_business_info gbi 
	JOIN trams_location AS tl ON gbi.recipient_id=tl.grantee_id AND tl.address_type='Physical Address'
              ";
 	SET @applicationQuery = CONCAT(_sql,replace(_whereClause,'''''',''''));


	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	SET @totalRecords = (SELECT totalRecordsCnt FROM tmp_records_count WHERE rowNumber = 1);
      		IF @totalRecords > _limitSize THEN		
		    INSERT INTO tmp_civil_rights_status_report_excel (GranteeId, `Business Name`)
		    VALUES ('0000',concat('-- ',_limitSize,' records returned from total of: ',@totalRecords,' records --'));
      		END IF;
	END IF;

	IF p_mode = 'Excel' THEN
	    	SET @applicationQuery = "SELECT
		crsr.GranteeId,
		crsr.Grantee,
		crsr.`Business Name`,	
		crsr.`Business Type`,
 		crsr.City,
		crsr.`Region Office`,
		crsr.`State Code`";

		IF p_title_vi_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`TitleVI Program Status`,
		trams_format_date(crsr.`TitleVI Due Date`) AS `TitleVI Due Date`,
		trams_format_date(crsr.`TitleVI Submission Date`) AS `TitleVI Submission Date`,
		trams_format_date(crsr.`TitleVI Expiration Date`) AS `TitleVI Expiration Date`,
		trams_format_date(crsr.`TitleVI Concur Date`) AS `TitleVI Concur Date`");
		END IF;

		IF p_dbe_goals_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`DBE Goals Program Status`");
		END IF;
        
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`Cycle Group`,
		Cast(crsr.`Overall Goal` AS decimal(12,2) ) AS `Overall Goal`");
        
		IF p_dbe_goals_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		trams_format_date(crsr.`DBE Goals Due Date`) AS `DBE Goals Due Date`,
		trams_format_date(crsr.`DBE Goals Submission Date`) AS `DBE Goals Submission Date`,
		trams_format_date(crsr.`DBE Goals Expiration Date`) AS `DBE Goals Expiration Date`,
		trams_format_date(crsr.`DBE Goals Concur Date`) AS `DBE Goals Concur Date`");
		END IF;

		SET @applicationQuery = CONCAT(@applicationQuery,",
		Cast(crsr.`DBE Race Neutral Goal` AS decimal(12,2) ) AS `DBE Race Neutral Goal`,
		Cast(crsr.`DBE Race Conscious Goal` AS decimal(12,2) ) AS `DBE Race Conscious Goal`");

		IF p_dbe_program_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`DBE Program Status`,
		trams_format_date(crsr.`DBE Program Due Date`) AS `DBE Program Due Date`,
		trams_format_date(crsr.`DBE Program Submission Date`) AS `DBE Program Submission Date`,
--		trams_format_date(crsr.`DBE Program Expiration Date`) AS `DBE Program Expiration Date`,
		trams_format_date(crsr.`DBE Program Concur Date`) AS `DBE Program Concur Date`");
		END IF;

		IF p_eeo_program_flag = 1 THEN
		SET @applicationQuery = CONCAT(@applicationQuery,",
		crsr.`EEO Program Status`,
		trams_format_date(crsr.`EEO Program Due Date`) AS `EEO Program Due Date`,
		trams_format_date(crsr.`EEO Program Submission Date`) AS `EEO Program Submission Date`,
		trams_format_date(crsr.`EEO Program Expiration Date`) AS `EEO Program Expiration Date`,
		trams_format_date(crsr.`EEO Program Concur Date`) AS `EEO Program Concur Date`");
		END IF;

		SET @applicationQuery = CONCAT(@applicationQuery,"
		FROM tmp_civil_rights_status_report_excel crsr
		WHERE crsr.rowNumber >= @startRow ", _outputWhereClause,
		" ORDER BY crsr.rowNumber ASC;");

	   	IF @outputCount < 1 AND _outputWhereClause > ' ' THEN
		    SELECT concat('0000') AS 'GranteeId',concat('No records matched filter conditions') AS 'Business Name';
		ELSE


		PREPARE applicationStmt FROM @applicationQuery;
		EXECUTE applicationStmt;

		END IF;
    
    ELSE
	SET @applicationQuery = "SELECT
	crsr.GranteeId,
	crsr.Grantee,
	crsr.`Business Name`,
	crsr.`Business Type`,
	crsr.City,
	crsr.`Region Office`,
	crsr.`State Code`";

	IF p_title_vi_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`TitleVI Program Status`,
	crsr.`TitleVI Due Date`,
	crsr.`TitleVI Submission Date`,
	crsr.`TitleVI Expiration Date`,
	crsr.`TitleVI Concur Date`");
	END IF;
    
	IF p_dbe_goals_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Goals Program Status`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`Cycle Group`,
	crsr.`Overall Goal`");

	IF p_dbe_goals_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Goals Due Date`,
	crsr.`DBE Goals Submission Date`,
	crsr.`DBE Goals Expiration Date`,
	crsr.`DBE Goals Concur Date`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Race Neutral Goal`,
	crsr.`DBE Race Conscious Goal`");

	IF p_dbe_program_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`DBE Program Status`,
	crsr.`DBE Program Due Date`,
	crsr.`DBE Program Submission Date`,
--	crsr.`DBE Program Expiration Date`,
	crsr.`DBE Program Concur Date`");
	END IF;

	IF p_eeo_program_flag = 1 THEN
	SET @applicationQuery = CONCAT(@applicationQuery,",
	crsr.`EEO Program Status`,
	crsr.`EEO Program Due Date`,
	crsr.`EEO Program Submission Date`,
	crsr.`EEO Program Expiration Date`,
	crsr.`EEO Program Concur Date`");
	END IF;

	SET @applicationQuery = CONCAT(@applicationQuery,"
	FROM tmp_civil_rights_status_report_excel crsr
	WHERE crsr.rowNumber >= @startRow ", _outputWhereClause,
	" ORDER BY crsr.rowNumber ASC;");

   	IF @outputCount < 1 AND _outputWhereClause > ' ' THEN
	    SELECT concat('0000') AS 'GranteeId',concat('No records matched filter conditions') AS 'Business Name';
	ELSE

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	END IF;

    END IF;
	
 END IF;

 CALL `trams_log`(@sid, _spName,concat(now(),' Done!'), 1);
 
 DROP TEMPORARY TABLE IF EXISTS tmp_civil_rights_status_report_excel;
 DROP TEMPORARY TABLE IF EXISTS tmp_records_count;
 
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_by_lineitem`(
	IN `p_application_id` INT,
	IN `p_revision_number` INT
)
BEGIN
	SELECT
		li.id AS id,
		li.app_id AS application_id,
		li.project_id AS project_id,
		lc.revision_number AS revision_number,
		li.scope_code AS scopeCode_text,
		li.scope_detailed_name AS scopeName_text,
		li.line_item_number AS itemNumber_text,
		li.line_item_type_name AS itemTypeName_text,
		-- NEW: Get the custom line item name from the line item changelog table
		-- Does not follow naming convention because stored procedure maps directly to appian cdt
		lc.revised_custom_item_name as customItemName_text,
		CONCAT('$', FORMAT(IFNULL(lc.original_FTA_amount,'0'), 2)) AS originalAwardFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL(lc.latest_amend_FTA_amount,'0'), 2)) AS previousFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL(lc.revised_FTA_amount,'0'), 2)) AS revisedFTAAmount_text,
		CONCAT('$', FORMAT((IFNULL(lc.revised_FTA_amount,'0') - IFNULL(IFNULL(lc.original_FTA_amount,'0'),'0')), 2)) AS differenceFTAAmount_text,
		CONCAT('$', FORMAT((IFNULL(lc.revised_FTA_amount,'0') - IFNULL(IFNULL(lc.latest_amend_FTA_amount,'0'),'0')), 2)) AS previousDifferenceFTAAmount_text,


		CONCAT('$', FORMAT(IFNULL((lc.original_total_eligible_amount - lc.original_FTA_amount),'0'), 2)) AS originalNonFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL((lc.latest_amend_total_eligible_amount - lc.latest_amend_FTA_amount),'0'), 2)) AS previousNonFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL((lc.revised_total_eligible_amount - lc.revised_FTA_amount),'0'), 2)) AS revisedNonFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL(((lc.revised_total_eligible_amount - lc.revised_FTA_amount) - (lc.original_total_eligible_amount - lc.original_FTA_amount)),'0'), 2)) AS differenceNonFTAAmount_text,
		CONCAT('$', FORMAT(IFNULL(((lc.revised_total_eligible_amount - lc.revised_FTA_amount) - (lc.latest_amend_total_eligible_amount - lc.latest_amend_FTA_amount)),'0'), 2)) AS previousDifferenceNonFTAAmount_text,
IFNULL(lc.original_quantity,'0') AS originalQuantity_int,
		IFNULL(Cast(lc.latest_amend_quantity AS UNSIGNED), '0') AS previousQuantity_int,
		IFNULL(Cast(lc.revised_quantity AS UNSIGNED),'0') AS revisedQuantity_int,
		IFNULL(IFNULL(lc.revised_quantity,'0'),'0') - IFNULL(IFNULL(lc.original_quantity,'0'),'0') AS differenceQuantity_int,
		IFNULL(IFNULL(lc.revised_quantity,'0'),'0') - IFNULL(IFNULL(lc.latest_amend_quantity,'0'),'0') AS previousDifferenceQuantity_int,

		CONCAT('$', FORMAT(IFNULL(lc.original_total_eligible_amount,'0'), 2)) AS originalTotalEligibleAmount_text,
		CONCAT('$', FORMAT(IFNULL(lc.latest_amend_total_eligible_amount,'0'), 2)) AS previousTotalEligibleAmount_text,
		CONCAT('$', FORMAT(IFNULL(lc.revised_total_eligible_amount,'0'), 2)) AS revisedTotalEligibleAmount_text,
		CONCAT('$', FORMAT(IFNULL((lc.revised_total_eligible_amount - lc.original_total_eligible_amount),'0'), 2)) AS differenceTotalEligibleAmount_text,
		CONCAT('$', FORMAT(IFNULL((lc.revised_total_eligible_amount - lc.latest_amend_total_eligible_amount),'0'), 2)) AS previousDifferenceTotalEligibleAmount_text

	FROM
		trams_application a
		JOIN trams_line_item li
			ON a.id = li.app_id
		JOIN trams_line_item_change_log lc
			ON li.id = lc.line_item_id
	WHERE
		a.id = p_application_id
		AND li.other_budget = '0'
		AND lc.revision_number = p_revision_number;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_grant_accrual_report`(IN p_begin_date VARCHAR(128), IN p_end_date VARCHAR(128))
BEGIN
	/* DEPRECATED*/
	/*
     Stored Procedure Name : trams_grant_accrual_report
     Description : This stored procedure is being deprecated in favor of the newer procedure (trams_get_grant_accrual_report_update)
     Updated by  : Dawit Asmelash (2021.Dec.30)
	*/

	/*DROP TABLE IF EXISTS trams_grant_accrual_report;
	CREATE TABLE trams_grant_accural_report
		SELECT
			a.recipient_cost_center AS 'Cost Center',
			gbi.legal_business_name AS 'Recipient Name',
			a.recipient_id AS 'Recipient ID',
			a.application_number AS 'Application Number w/ Current Amendment',
			a.status AS 'Application Status',
			fsr.funding_source_description  AS 'Program Description',
			a.application_name AS 'Brief Description',
			ffr.report_period_type AS 'FFR Report Period', -- Need to verify (Previously Annually or Quarterly)
			aad.obligated_date_time AS 'Obligation Date',
			totals.disbursement_date AS 'Disbursement Date',
			a.fta_pre_award_manager AS 'FTA Pre-Award Manager',
			a.fta_post_award_manager AS 'FTA Post-Award Manager',
			IFNULL(totals.total_obligation_amount,0) - IFNULL(totals.total_deobligation_amount,0) AS 'Net Obligations',
			totals.total_refund_amount AS 'Total Refund Amount',
			totals.total_disbursement_amount AS 'Total Disbursements',
			totals.total_unliquidated_amount AS 'Total Undisbursed Amount',
			a.application_type AS 'Application Type',
			max(ffr.initial_submission_date_time) AS 'FFR Submitted Date',
			ffr.final_report_flag AS 'Final FFR?'
		FROM
			trams_application a
		JOIN
			(
				SELECT
					id,
					max(application_number) as application_number,
					contractAward_id
				FROM
					trams_application
				GROUP BY
					contractAward_id
			) max_application
		ON
			a.application_number = max_application.application_number AND
			a.contractAward_id = max_application.contractAward_id
		JOIN
			trams_general_business_info gbi
		ON
			a.recipient_id = gbi.recipient_id
		JOIN
			trams_application_award_detail aad
		ON
			a.id = aad.application_id
		JOIN
			trams_federal_financial_report ffr
		ON
			a.contractAward_id = ffr.contract_award_id
		JOIN
			(*/
			/* This derived table performs all of the roll up calculations */
			/*SELECT
				ca.id,
				cat.section_code,
				SUM(cat.obligation_amount) AS total_obligation_amount,
				SUM(cat.deobligation_amount) AS total_deobligation_amount,
				SUM(cat.disbursement_amount) AS total_disbursement_amount,
				SUM(cat.refund_amount) AS total_refund_amount,
				(((IFNULL(SUM(cat.obligation_amount),0) 
				- IFNULL(SUM(cat.deobligation_amount),0)) 
				- IFNULL(SUM(cat.disbursement_amount),0)) 
				+ IFNULL(SUM(cat.refund_amount),0)) AS total_unliquidated_amount,
				(SUM(cat.disbursement_amount) 
				/ (SUM(cat.obligation_amount)-IFNULL(SUM(cat.deobligation_amount),0))) AS percent_disbursement_amount,
				MAX(last_disbursement_date) AS disbursement_date
			FROM 
				trams_contract_award ca
			JOIN 
				trams_application a 
			ON 
				ca.id = a.contractAward_id
			JOIN
				trams_contract_acc_transaction cat 
			ON 
				a.id = cat.application_id
			GROUP BY ca.id
			) totals
		ON
			a.contractAward_id = totals.id
		JOIN 
			trams_po_number pn 
		ON 
			a.id = pn.application_id 
		LEFT JOIN 
			trams_funding_source_reference fsr 
		ON
			pn.funding_source_name = fsr.funding_source_name
		WHERE
			a.active_flag = 1
			AND ((aad.obligated_date_time BETWEEN p_begin_date AND p_end_date) OR
			(aad.executed_date_time BETWEEN p_begin_date AND p_end_date) OR
			(aad.deobligation_date_time BETWEEN p_begin_date AND p_end_date))
		GROUP BY
			a.contractAward_id;*/
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_close_out_award_funds_status`(IN `p_contract_award_id` INT)
    NO SQL
BEGIN
  
  DECLARE _groupByClause varchar(4096) DEFAULT " ";
  DECLARE _appFromTeamFlag TINYINT(1) DEFAULT 0;
  
  SELECT app_from_team_flag INTO _appFromTeamFlag FROM `trams_application` WHERE contractAward_id = p_contract_award_id ORDER BY id DESC LIMIT 1;
  
  set _groupByClause = concat(_groupByClause, 
    " transact.po_number,",
    " transact.cost_center_code,",
    " transact.account_class_code,",
    " transact.fpc",
    (SELECT 
      IF(_appFromTeamFlag = 1, " ",
        CONCAT(",",
          " transact.scope_code,",
          " transact.scope_suffix,",
          " CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(transact.project_number, '-', 4), LENGTH(SUBSTRING_INDEX(transact.project_number, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER)"
        )
      )
    )
  );
    
  SET @closeoutFundingQuery = concat(
  "SELECT
    transact.id, /*This is used as the unique identifier for generating fake acc transactions*/
    transact.cost_center_code,
    transact.po_number,
    transact.project_number,
    concat(transact.scope_code, ' / ', transact.scope_suffix) AS scope_number,
    transact.scope_suffix,
    transact.account_class_code,
    transact.fpc,
    ifnull(sum(transact.obligation_amount), 0)      AS obligation_amount,
    ifnull(sum(transact.deobligation_amount),0)     AS deobligation_amount,
    ifnull(sum(transact.disbursement_amount),0)     AS disbursement_amount,
    ifnull(sum(transact.refund_amount),0)         AS refund_amount,
    ifnull(sum(transact.obligation_amount), 0)  - ifnull(sum(transact.deobligation_amount), 0) - ifnull(sum(transact.disbursement_amount), 0) + ifnull(sum(transact.refund_amount), 0) AS unliquidated_balance
  FROM
    `trams_contract_acc_transaction` transact
  WHERE
    transact.contract_award_id = ", p_contract_award_id,
  " GROUP BY ", 
  _groupByClause,
  " ORDER BY
    transact.application_number DESC;"
  );
  
  PREPARE closeoutFundingStmt FROM @closeoutFundingQuery;
  EXECUTE closeoutFundingStmt;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_dynamic_application_status_budget_report`(IN p_granteeId VARCHAR(2048), IN p_costCenterCode VARCHAR(512), IN p_fiscalYear VARCHAR(512),
IN p_appNumber VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_appType VARCHAR(512), IN p_preAwardManager VARCHAR(1024), IN p_postAwardManager VARCHAR(1024),
IN p_gmtOffset VARCHAR(10), IN p_return_limit VARCHAR(20)
)
BEGIN

    /*
     Stored Procedure Name : trams_get_dynamic_application_status_budget_report
     Description : To populate data for dynamic TrAMS 'Application by Status Report' action.
     Updated by  : Garrison Dean (2020.Nov.24)
		Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
    */

    DECLARE _sql VARCHAR(8192) DEFAULT "";
    DECLARE _whereClause VARCHAR(8192) DEFAULT "";
    DECLARE _spName VARCHAR(255) DEFAULT "trams_get_dynamic_application_status_budget_report";

    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

    SET @uuid = UUID_SHORT();

    CALL `trams_log`(@sid, _spName, CONCAT(now(),' CALL SP Inputs: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1);

    SET SESSION max_heap_table_size = 1024*1024*128;

    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_id IN (",NULLIF(p_granteeId,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_cost_center IN (",NULLIF(p_costCenterCode,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fiscal_year IN (",NULLIF(p_fiscalYear,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_number IN ('",NULLIF(p_appNumber,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.status IN (",NULLIF(p_appStatus,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_type IN ('",NULLIF(p_appType,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_pre_award_manager IN ('",NULLIF(p_preAwardManager,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_post_award_manager IN ('",NULLIF(p_postAwardManager,''),"')"),"")));

    /*2-26-19: Modified CONCAT to include new INSERT statement for permanent tables*/

    SET _sql = CONCAT(
        "INSERT INTO trams_temp_dasbr_application (
            SELECT NULL,
            id AS application_id,",
            @uuid, " AS uuid,",
            @sid, " AS time_stamp
            FROM trams_application a
            WHERE 1"
        );

    SET @query = CONCAT (_sql, _whereClause, ')');

    PREPARE query FROM @query;
    EXECUTE query;

    SET @appcount=(SELECT COUNT(*) FROM trams_temp_dasbr_application WHERE uuid = @uuid);
    CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Total application: ',@appcount), 1);

    INSERT INTO `trams_temp_dasbr_budget_revision_latest` (
        SELECT
        NULL,
            application_id AS application_id,
            MAX(revision_number) AS revision_number,
            @uuid AS uuid,
            @sid AS time_stamp
        FROM
            trams_budget_revision
        WHERE
            application_id IN (SELECT application_id FROM trams_temp_dasbr_application WHERE uuid = @uuid)
        GROUP BY
            application_id
    );

    CALL `trams_log`(@sid, _spName, CONCAT( now(), 'BEGIN trams_temp_dasbr_resultset creation'), 1);

    INSERT INTO `trams_temp_dasbr_resultset`(
        SELECT
            NULL,
            a.temp_application_number,
            a.recipient_id,
            gb.recipientAcronym AS recipient_acronym,
            gb.legalBusinessName AS legal_business_name,
            a.recipient_cost_center,
            a.fiscal_year,
            a.application_number,
            a.amendment_number,
            a.application_name,
            br.revision_number,
            a.status,
            a.application_type,
            IF(
                a.app_from_team_flag = 1,
                a.recipient_contact,
                trams_poc.user_name
            ) AS grantee_poc,
            a.fta_pre_award_manager,
            a.fta_post_award_manager,
            totals.total_obligation_amount,
            totals.total_deobligation_amount,
            scope_acc.current_reserved_amount,
            fr.lapsing_fund_flag AS lapsing_funds,
            fr.financial_report_freq AS ffr_reporting_period,
            fr.mpr_report_frequency AS mpr_report_frequency,
            a.preaward_authority_flag AS pre_award_authority,
			CASE
				WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', p_gmtOffset) AS char))
				ELSE trams_format_date(fr.transmitted_date)
			END AS transmitted_date,
            CASE
				WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
				THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', p_gmtOffset) AS char))
				ELSE trams_format_date(fr.submitted_date)
			END AS submitted_date,
            aa.fain_assigned_date_time AS fain_assigned_date_time,
            -- These if statements were added on 5/21/18
            IF(aa.submitted_to_dol_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_flag, 0), 'N/A', aa.submitted_to_dol_date_time) as submitted_to_dol_date_time,
            IF(aa.dol_certification_date_time IS NULL AND NOT IFNULL(fr.submitted_to_dol_for_cert_flag, 0), 'N/A', aa.dol_certification_date_time) as dol_certification_date_time,
            IF(aa.technical_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_technical_review_flag, 0),'N/A', aa.technical_concurrence_date_time) as technical_concurrence_date_time,
            IF(aa.civil_rights_concurrence_date IS NULL AND NOT IFNULL(fr.requires_civil_rights_review_flag, 0), 'N/A', aa.civil_rights_concurrence_date) as civil_rights_concurrence_date,
            aa.environmental_concurrence_date_time as environmental_concurrence_date_time,
            aa.planning_concurrence_date_time as planning_concurrence_date_time,
            IF(aa.operations_concurrence_date_time IS NULL AND NOT IFNULL(fr.requires_operations_review_flag, 0), 'N/A', aa.operations_concurrence_date_time) as operations_concurrence_date_time,
            aa.legal_concurrence_date_time AS legal_concurrence_date_time,
            aa.ra_concurrence_date_time AS ra_concurrence_date_time,
            cr.logged_release_date AS logged_release_date,
            max_reserve.reservation_finalized_date_time AS reservation_finalized_date_time,
            ca.last_obligation_date AS obligated_date_time,
            ca.last_deobligation_date AS deobligation_date_time,
            aa.executed_date_time AS executed_date_time,
            fntramsGetApplicationPOPEndDateFinalized(a.id, a.contractAward_id) AS period_of_performance_end_date,
            aa.fta_closeout_approval_date_time AS fta_closeout_approval_date_time,
            a.discretionary_funds_flag,
            a.created_date_time AS created_date_time,
            @uuid AS uuid,
            @sid AS time_stamp,
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00', p_gmtOffset) AS char)),
			trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00', p_gmtOffset) AS char))
        FROM
            trams_application a
        JOIN
            trams_temp_dasbr_application aid
            ON a.id = aid.application_id
            AND aid.uuid = @uuid
        JOIN
            vwtramsGeneralBusinessInfo gb
            ON a.recipient_id = gb.recipientId
        JOIN
            trams_application_award_detail aa
            ON a.id = aa.application_id
        LEFT JOIN
            trams_app_congressional_release cr
            ON a.id=cr.app_id
        LEFT JOIN
            (
            SELECT
                cat.contract_award_id,
                cat.section_code,
                SUM(cat.obligation_amount) AS total_obligation_amount,
                SUM(cat.deobligation_amount) AS total_deobligation_amount,
                SUM(cat.disbursement_amount) AS total_disbursement_amount,
                SUM(cat.refund_amount) AS total_refund_amount,
                (((IFNULL(SUM(cat.obligation_amount),0)
                - IFNULL(SUM(cat.deobligation_amount),0))
                - IFNULL(SUM(cat.disbursement_amount),0))
                + IFNULL(SUM(cat.refund_amount),0)) AS total_unliquidated_amount,
                (SUM(cat.disbursement_amount)
                / (SUM(cat.obligation_amount)-IFNULL(SUM(cat.deobligation_amount),0))) AS percent_disbursement_amount
            FROM
                trams_contract_acc_transaction cat
            GROUP BY cat.contract_award_id
        ) totals
        ON
            a.contractAward_id = totals.contract_award_id
        LEFT JOIN
            trams_temp_dasbr_budget_revision_latest br
            ON a.id = br.application_id
            AND br.uuid = @uuid
        LEFT JOIN
            (
                SELECT
                    a.id,
                    MAX(r.reservation_finalized_date_time) AS reservation_finalized_date_time,
                    MAX(r.total_reserved_amount) AS total_reserved_amount
                FROM
                    trams_application a
                JOIN
                    trams_reservation r
                ON
                    a.id = r.application_id
                GROUP BY
                    a.id

            ) max_reserve
        ON
            a.id = max_reserve.id
        LEFT JOIN
            trams_fta_review fr
            ON a.id = fr.application_id
        LEFT JOIN
        (   SELECT
                a.id,
                max(poc.user_name) AS user_name
            FROM
                trams_application a

            JOIN
                trams_application_point_of_contact poc
            ON
                a.id = poc.applicationidint
            WHERE
                poc.role = 'Grantee'
            GROUP BY
                a.id
            ) trams_poc
            ON
                a.id = trams_poc.id
        LEFT JOIN
        (   SELECT
                sa.application_id,
                SUM(sa.acc_reservation_amount) AS current_reserved_amount
            FROM
                trams_scope_acc sa
            GROUP BY
                sa.application_id
            ) scope_acc
            ON
                a.id = scope_acc.application_id
        LEFT JOIN
            trams_contract_award ca
            ON ca.id = a.contractAward_id
        LEFT JOIN
            (
                SELECT
                    th.applicationId AS applicationId,
                    MAX(th.TransmittedDate) AS latest_transmitted_date,
                    MAX(th.SubmittedDate) AS latest_submitted_date,
                    MIN(th.TransmittedDate) AS original_transmitted_date,
                    MIN(th.SubmittedDate) AS original_submitted_date
                FROM
                    tramsApplicationTransmissionHistory th
                GROUP BY th.applicationId
            ) latest_transmission_history
            ON a.id = latest_transmission_history.applicationId
    );

    SET @appcount=(SELECT COUNT(*) FROM trams_temp_dasbr_resultset WHERE uuid = @uuid);

    CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Total application COUNT: ', @appcount), 1);

    IF @appcount>0 THEN

    UPDATE `trams_temp_dasbr_resultset` trs
    SET trs.created_date_time =
    (
        SELECT  MIN(`created_date_time`)
        FROM trams_application a
        WHERE a.temp_application_number = trs.temp_application_number
        AND trs.uuid = @uuid
    )
    WHERE trs.uuid = @uuid;

    END IF;

    CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Finished Updates'), 1);

    SELECT
        recipient_id,
        recipient_acronym,
        legal_business_name,
        recipient_cost_center,
        fiscal_year,
		temp_application_number,
        application_number,
        amendment_number,
        application_name,
        revision_number,
        status,
        application_type,
        grantee_poc,
        fta_pre_award_manager,
        fta_post_award_manager,
        total_obligation_amount,
        total_deobligation_amount,
        current_reserved_amount,
        lapsing_funds,
        ffr_reporting_period,
        mpr_report_frequency,
        pre_award_authority,
        /*2-26-19: Modified all date fields by wrapping in NULLIF() to fix an issue with dates matching '0000-00-00 00:00:00' in permanent tables*/
        NULLIF(transmitted_date, '0000-00-00 00:00:00'),
        NULLIF(latest_transmitted_date, '0000-00-00'),
        NULLIF(submitted_date, '0000-00-00 00:00:00'),
        NULLIF(latest_submitted_date, '0000-00-00'),
		NULLIF(trams_format_date(cast(CONVERT_TZ(fain_assigned_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS fain_assigned_date_time,
        IF(submitted_to_dol_date_time='0000-00-00 00:00:00', '', trams_format_date(cast(CONVERT_TZ(submitted_to_dol_date_time, '+00:00', p_gmtOffset) AS char))) AS submitted_to_dol_date_time,
        IF(dol_certification_date_time='0000-00-00 00:00:00', '', trams_format_date(cast(CONVERT_TZ(dol_certification_date_time, '+00:00', p_gmtOffset) AS char))) AS dol_certification_date_time,
        IF(technical_concurrence_date_time='0000-00-00 00:00:00', '', trams_format_date(cast(CONVERT_TZ(technical_concurrence_date_time, '+00:00', p_gmtOffset) AS char))) AS technical_concurrence_date_time,
        IF(civil_rights_concurrence_date='0000-00-00 00:00:00', '', trams_format_date(cast(CONVERT_TZ(civil_rights_concurrence_date, '+00:00', p_gmtOffset) AS char))) AS civil_rights_concurrence_date,
        NULLIF(trams_format_date(cast(CONVERT_TZ(environmental_concurrence_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS environmental_concurrence_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(planning_concurrence_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS planning_concurrence_date_time,
        IF(operations_concurrence_date_time='0000-00-00 00:00:00', '', trams_format_date(cast(CONVERT_TZ(operations_concurrence_date_time, '+00:00', p_gmtOffset) AS char))) AS operations_concurrence_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(legal_concurrence_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS legal_concurrence_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(ra_concurrence_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS ra_concurrence_date_time,
        NULLIF(trams_format_date(logged_release_date), '0000-00-00 00:00:00') AS logged_release_date,
        NULLIF(trams_format_date(cast(CONVERT_TZ(reservation_finalized_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS reservation_finalized_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(obligated_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS obligated_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(deobligation_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS deobligation_date_time,
        NULLIF(trams_format_date(cast(CONVERT_TZ(executed_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS executed_date_time,
        IF(period_of_performance_end_date IS NOT NULL, trams_format_date(period_of_performance_end_date), 'N/A') AS period_of_performance_end_date,
        NULLIF(trams_format_date(cast(CONVERT_TZ(fta_closeout_approval_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS fta_closeout_approval_date_time,
        discretionary_funds_flag,
        NULLIF(trams_format_date(cast(CONVERT_TZ(created_date_time, '+00:00', p_gmtOffset) AS char)), '0000-00-00 00:00:00') AS created_date_time
    FROM
        trams_temp_dasbr_resultset
    WHERE uuid = @uuid
    ORDER BY recipient_id, application_number, fiscal_year;

    DELETE FROM trams_temp_dasbr_application WHERE uuid = @uuid;
    DELETE FROM trams_temp_dasbr_budget_revision_latest WHERE uuid = @uuid;
    DELETE FROM trams_temp_dasbr_resultset WHERE uuid = @uuid;

CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_civil_rights_compliance_by_program_name`()
SELECT distinct
     crc.`id` AS `id` , 
     crc.`program_name` AS `program_name`, 
     crc.`current_status` AS `current_status`, 
	 gbi.`tramsStatus` AS `trams_status`,
     crc.`recipient_id` AS `grantee_id_int` 
FROM vwtramsGeneralBusinessInfo gbi  JOIN trams_civil_rights_compliance crc    ON crc.recipient_id =  gbi.recipientId
WHERE  `program_name` = "DBE GOAL"  AND current_status not in ("N/A", "--", "Incomplete") AND  gbi.tramsStatus = "ACTIVE" 
ORDER BY gbi.recipientId DESC$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_funding_summary`(IN `p_application_id` INT, IN `p_revision_num` INT)
BEGIN

DECLARE _onClause VARCHAR(4096) DEFAULT " ";
DECLARE _appCreation DATETIME;

SET @uuid = UUID_SHORT();
SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

SET _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(NULL,p_application_id);

SET _onClause = CONCAT('fsr.LatestFlagStartDate <= ''',_appCreation,''' AND fsr.LatestFlagEndDate >= ''',_appCreation,'''');


		-- Populate Permanent tables
		INSERT INTO trams_temp_funding_line_item (id, funding_source_name, scope_code, other_budget, uuid, time_stamp)
		SELECT id, funding_source_name, scope_code, other_budget, @uuid AS uuid, @sid AS time_stamp
		FROM trams_line_item
		WHERE app_id = p_application_id;

		INSERT INTO trams_temp_funding_change_log (id, line_item_id, original_fta_amount, revised_fta_amount, uuid, time_stamp)
		SELECT id, line_item_id, original_fta_amount, revised_fta_amount, @uuid AS uuid, @sid AS time_stamp
		FROM trams_line_item_change_log
		WHERE application_id = p_application_id AND revision_number = p_revision_num;

SET @sqlStatement = CONCAT('SELECT '
			,p_application_id,' AS application_id,
			fsr.funding_source_description AS fundingSource_text,
			IFNULL(SUM(cl.original_fta_amount), 0) AS orgionalAwardAmount_text,
			IFNULL(SUM(cl.revised_fta_amount), 0) AS budgetRevisionAmount_text,
			IFNULL(SUM(cl.revised_fta_amount), 0) - IFNULL(SUM(cl.original_fta_amount), 0) AS differenceAmount_text
		FROM
			trams_temp_funding_change_log cl
			JOIN trams_temp_funding_line_item li
				ON cl.line_item_id = li.id
				AND li.uuid = @uuid	
			JOIN trams_funding_source_reference fsr
				ON li.funding_source_name = fsr.funding_source_name AND ',_onClause,'
		WHERE li.other_budget = 0
		AND cl.uuid = @uuid
		GROUP BY li.funding_source_name
		ORDER BY cl.id DESC');

		PREPARE sqlStatement FROM @sqlStatement;
		EXECUTE sqlStatement;
		
		-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
        DELETE FROM trams_temp_funding_line_item WHERE uuid = @uuid;
        DELETE FROM trams_temp_funding_change_log WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_deobligation_project_scope_funding_summary`(IN `p_contract_award_id` INT, IN `p_is_closeout` TINYINT)
    NO SQL
BEGIN
  /*
    This procedure takes in a contract_award_id and returns all data in the Project Scope Obligation Summary for the deobligation process.
    This procedure is only for TrAMS applications, There is a different procedure for TEAM applications.
    If the deobligation is a closeout, logic must be removed to pull correct data. There is an additional if statement which adds in SQL where necessary.
  */
  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _deobScopeJoin VARCHAR(1000) DEFAULT "";
  DECLARE _deobScopeCheck VARCHAR(1000) DEFAULT "";
  DECLARE _mostRecentAppId INT(11) DEFAULT 0;

  SELECT id INTO _mostRecentAppId FROM `trams_application` WHERE contractAward_id = p_contract_award_id ORDER BY id DESC LIMIT 1;


  /* IF not a closeout application */
  IF (NULLIF(p_is_closeout, '') IS NULL OR NULLIF(p_is_closeout, '') = 0) THEN
    SET _deobScopeCheck = "AND scp.latest_amend_fta_amount - scp.revised_fta_amount > 0";
    SET _deobScopeJoin = CONCAT("JOIN (
        SELECT
          tscp.po_number,
          proj.project_number_sequence_number,
          tscp.scope_code,
          tscp.scope_suffix,
          tscp.latest_amend_fta_amount,
          tscp.revised_fta_amount
        FROM `trams_application` app
          JOIN `trams_scope` tscp
            ON tscp.application_id = app.id
          JOIN `trams_project` proj
            ON tscp.project_id = proj.id
        WHERE app.id = ",_mostRecentAppId,"
      ) scp
        ON scp.po_number = fnd.po_number
        AND scp.project_number_sequence_number = fnd.project_number_sequence_number
        AND scp.scope_code = fnd.scope_number
        AND scp.scope_suffix = fnd.scope_suffix"
    );
  END IF;

  -- replaced "fnd.amendment_amount_current" with "ifnull(cat.obligation_amount,0)" on line 67
  -- added "ifnull(sum(obligation_amount),0)-ifnull(sum(deobligation_amount),0) AS obligation_amount" on line 173 to retrieve cumulative obligation amount
  SET _sql = CONCAT(
    "SELECT
      fnd.id AS trams_contract_acc_funding_id,
      app.amendment_number AS amendment_number,
      fnd.po_number AS po_number,
      fnd.project_number AS project_number,
      fnd.scope_number AS scope_number,
      fnd.scope_suffix AS scope_suffix,
      fnd.scope_name AS scope_name,
      fnd.cost_center_code AS cost_center_code,
      ifnull(dd.acc_uza_code, fnd.uza_code) AS uza_code,
      fnd.account_class_code AS account_class_code,
      fnd.fpc AS fpc,
      ifnull(cat.obligation_amount,0) AS original_obligation_amount,
      /* BEGIN data used in TrAMS_deobligationDetail */
      fnd.application_id AS application_id,
      fnd.project_id AS project_id,
      fnd.contract_award_id AS contract_award_id,
      fnd.funding_fiscal_year AS funding_fiscal_year,
      fnd.section_code AS section_code,
      fnd.limitation_code AS limitation_code,
      fnd.authorization_type AS authorization_type,
      fnd.appropriation_code AS appropriation_code,
      fnd.project_number_sequence_number AS project_number_sequence_number,
      fnd.funding_source_name AS funding_source_name,
      accl.dafis_suffix_code AS dafis_suffix_code,
      /* END data used in TrAMS_deobligationDetail */
      ifnull(newest_fnd.contract_amount_cumulative, 0) AS net_obligation_amount,
      ifnull(total_fnd.contract_amount_cumulative, 0) - ifnull(all_cat.net_disbursement_amount, 0) + ifnull(all_cat.net_refund_amount, 0) AS unliquidated_balance,
      ifnull(dd.net_deobligated_recovery_amount, 0) AS deobligation_amount
    FROM
      `trams_contract_acc_funding` fnd
      JOIN `trams_application` app
        ON fnd.application_id = app.id
      ",_deobScopeJoin,"
      LEFT JOIN `trams_account_class_code_lookup` accl
        ON fnd.account_class_code = accl.account_class_code
      LEFT JOIN (
        /* retrieves the contract_amount_cumulative from the most recent amendment */
        SELECT
          po_number,
          project_number_sequence_number,
          scope_number,
          scope_suffix,
          uza_code,
          contract_amount_cumulative,
          cost_center_code,
          account_class_code,
          fpc
        FROM `trams_contract_acc_funding`
        WHERE application_id = (
          SELECT max(application_id)
          FROM `trams_contract_acc_funding`
          WHERE contract_award_id = ",p_contract_award_id,"
          GROUP BY contract_award_id
          ORDER BY id DESC
        )
        ORDER BY id DESC
      ) newest_fnd
        ON
          fnd.po_number = newest_fnd.po_number
          AND fnd.project_number_sequence_number = newest_fnd.project_number_sequence_number
          AND fnd.scope_number = newest_fnd.scope_number
          AND fnd.scope_suffix = newest_fnd.scope_suffix
          AND fnd.uza_code = newest_fnd.uza_code
          AND fnd.cost_center_code = newest_fnd.cost_center_code
          AND fnd.account_class_code = newest_fnd.account_class_code
          AND fnd.fpc = newest_fnd.fpc
      LEFT JOIN (
        /* retrieves the contract_amount_cumulative from the most recent amendment, ignoring uza code */
        SELECT
          po_number,
          project_number_sequence_number,
          scope_number,
          scope_suffix,
          SUM(contract_amount_cumulative) AS contract_amount_cumulative,
          cost_center_code,
          account_class_code,
          fpc
        FROM `trams_contract_acc_funding`
        WHERE application_id = (
          SELECT max(application_id)
          FROM `trams_contract_acc_funding`
          WHERE contract_award_id = ",p_contract_award_id,"
          GROUP BY contract_award_id
          ORDER BY id DESC
        )
        GROUP BY
          po_number,
          project_number_sequence_number,
          scope_number,
          scope_suffix,
          cost_center_code,
          account_class_code,
          fpc
        ORDER BY id DESC
      ) total_fnd
        ON
          fnd.po_number = total_fnd.po_number
          AND fnd.project_number_sequence_number = total_fnd.project_number_sequence_number
          AND fnd.scope_number = total_fnd.scope_number
          AND fnd.scope_suffix = total_fnd.scope_suffix
          AND fnd.cost_center_code = total_fnd.cost_center_code
          AND fnd.account_class_code = total_fnd.account_class_code
          AND fnd.fpc = total_fnd.fpc
      LEFT JOIN (
        /* retrieves all deobligations for a specific amendment */
        SELECT
          po_number,
          project_number,
          scope_code,
          scope_suffix,
          uza_code,
          cost_center_code,
          account_class_code,
          fpc,
          sum(deobligation_amount) AS deobligation_amount,
          sum(disbursement_amount) AS disbursement_amount,
          sum(refund_amount) AS refund_amount,
          ifnull(sum(obligation_amount),0)-ifnull(sum(deobligation_amount),0) AS obligation_amount
        FROM
          `trams_contract_acc_transaction`
        WHERE
          contract_award_id = ",p_contract_award_id,"
        GROUP BY
          po_number,
          project_number,
          scope_code,
          scope_suffix,
          uza_code,
          cost_center_code,
          account_class_code,
          fpc
      ) cat
        ON
          fnd.po_number = cat.po_number
          AND fnd.project_number = cat.project_number
          AND fnd.scope_number = cat.scope_code
          AND fnd.scope_suffix = cat.scope_suffix
          AND fnd.uza_code = cat.uza_code
          AND fnd.cost_center_code = cat.cost_center_code
          AND fnd.account_class_code = cat.account_class_code
          AND fnd.fpc = cat.fpc
      LEFT JOIN (
        /* retrieves deobligation, disbursement, and refund data for use in calculating unliquidated balance. this is a summary column across the contract award, meaning it will get to total fundings and appear duplicated for each amendment. */
        SELECT
          proj.project_number_sequence_number,
          cat.po_number,
          cat.scope_code,
          cat.scope_suffix,
          cat.cost_center_code,
          cat.account_class_code,
          cat.fpc,
          sum(cat.deobligation_amount) AS net_deobligation_amount,
          sum(cat.disbursement_amount) AS net_disbursement_amount,
          sum(cat.refund_amount) AS net_refund_amount
        FROM
          `trams_contract_acc_transaction` cat
          LEFT JOIN `trams_project` proj ON cat.project_id = proj.id
        WHERE
          NOT cat.transaction_type = 'Obligation' AND contract_award_id = ",p_contract_award_id,"
        GROUP BY
          cat.po_number,
          proj.project_number_sequence_number,
          cat.scope_code,
          cat.scope_suffix,
          cat.cost_center_code,
          cat.account_class_code,
          cat.fpc
        ORDER BY cat.id DESC
      ) all_cat
        ON
          fnd.po_number = all_cat.po_number
          AND fnd.project_number_sequence_number = all_cat.project_number_sequence_number
          AND fnd.scope_number = all_cat.scope_code
          AND fnd.scope_suffix = all_cat.scope_suffix
          AND fnd.cost_center_code = all_cat.cost_center_code
          AND fnd.account_class_code = all_cat.account_class_code
          AND fnd.fpc = all_cat.fpc
      LEFT JOIN (
        /* retrieves temporary deobligations that are created throughout the process */
        SELECT
          po_number,
          project_id,
          scope_number,
          scope_suffix,
          acc_uza_code,
          acc_cost_center_code,
          account_class_code,
          acc_fpc,
          sum(deobligated_recovery_amount) AS net_deobligated_recovery_amount
        FROM
          `trams_deobligation_detail`
        WHERE
          deobligated_still_pending <> 0 AND contract_award_id = ", p_contract_award_id,"
        GROUP BY
          po_number,
          project_id,
          scope_number,
          scope_suffix,
          acc_uza_code,
          acc_cost_center_code,
          acc_uza_code,
          account_class_code,
          acc_fpc
      ) dd
        ON
          fnd.po_number = dd.po_number
          AND fnd.project_id = dd.project_id
          AND fnd.scope_number = dd.scope_number
          AND fnd.scope_suffix = dd.scope_suffix
          AND fnd.uza_code = dd.acc_uza_code
          AND fnd.cost_center_code = dd.acc_cost_center_code
          AND fnd.account_class_code = dd.account_class_code
          AND fnd.fpc = dd.acc_fpc
    WHERE
      fnd.contract_award_id = ",p_contract_award_id," ",_deobScopeCheck,"
    ORDER BY app.amendment_number, fnd.account_class_code ASC;"
  );

  SET @deobQuery = _sql;

  SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_deobligation_project_scope_funding_summary', concat('Deobligation Summary Query: ', @deobQuery), 1);

  PREPARE deobStmt FROM @deobQuery;
  EXECUTE deobStmt;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_discretionary_allocation_detail_report`(
	IN p_granteeId VARCHAR(2048),
	IN p_fiscalYear VARCHAR(512),
	IN p_costCenterCode VARCHAR(512),
	IN p_appType VARCHAR(512),
	IN p_appNumber VARCHAR(512), 
    IN p_appStatus VARCHAR(1024), 
    IN p_projectNumber VARCHAR(512), 
    IN p_discretionaryId VARCHAR(512),
	IN p_preAwardManager VARCHAR(512),
	IN p_postAwardManager VARCHAR(512),
    IN p_gmtOffset VARCHAR(10),
    IN p_return_limit VARCHAR(20),
    IN p_apportionmentFiscalYear VARCHAR(512)
)
BEGIN

	/*
     Stored Procedure Name : trams_get_discretionary_allocation_detail_report
     Description : To populate data for TrAMS 'Discrectionary Allocation Detail' excel report.
     Updated by  : Seung Ki Lee (04.26.2021)
    */

	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _whereClause VARCHAR(8192) DEFAULT " ";
	DECLARE _dis_whereClause VARCHAR(8192) DEFAULT " ";
	DECLARE _limitClause VARCHAR(100) DEFAULT " ";
	
	CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);
	
	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
	
	SET _sql = CONCAT("INSERT INTO trams_temp_dadr_application_discretionaryId 
 	           SELECT 
 		       NULL AS trams_temp_dadr_application_discretionaryId_id,
               a.id  AS application_id, ",
 			   @uuid," AS uuid, ",
 			   @sid," AS time_stamp 
 	           FROM trams_application a
			   WHERE 1");
	
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_id in (",NULLIF(p_granteeId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_cost_center in (",NULLIF(p_costCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_number = '",NULLIF(p_appNumber,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fiscal_year in ('",NULLIF(p_fiscalYear,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_type in ('",NULLIF(p_appType,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.status in (",NULLIF(p_appStatus,''),")"),"")));

	SET _dis_whereClause = CONCAT(_dis_whereClause, (SELECT IFNULL(CONCAT(" and ada.discretionary_id  in ('",NULLIF(p_discretionaryId,''),"')"),"")));
	SET _dis_whereClause = CONCAT(_dis_whereClause, (SELECT IFNULL(CONCAT(" and disc.original_fiscal_year in ('",NULLIF(p_apportionmentFiscalYear,''),"')"),"")));
	SET _dis_whereClause = CONCAT(_dis_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_pre_award_manager   in ('",NULLIF(p_preAwardManager,''),"')"),"")));
	SET _dis_whereClause = CONCAT(_dis_whereClause, (SELECT IFNULL(CONCAT(" and p.project_number = '",NULLIF(p_projectNumber,''),"'"),"")));
	SET _dis_whereClause = CONCAT(_dis_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_post_award_manager  in ('",NULLIF(p_postAwardManager,''),"')"),"")));
	
   
	SET @applicationQuery = CONCAT(_sql,_whereClause,_limitClause);
	
	CALL `trams_log`(@sid, 'trams_get_discretionary_allocation_detail_report', CONCAT('Get Application Query: ',@applicationQuery), 1); 
	
	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;
	
	SET @appCount=(SELECT COUNT(*) FROM trams_temp_dadr_application_discretionaryId WHERE uuid = @uuid);
	CALL `trams_log`(@sid, 'trams_get_discretionary_allocation_detail_report', CONCAT('Total Application: ',@appCount), 1); 	
	
	INSERT INTO  
		trams_temp_dadr_contract_acc_transaction_by_app 
	SELECT 
		  NULL AS trams_temp_dadr_contract_acc_transaction_by_app_id,
		  application_id AS application_id,
		  IFNULL(SUM(obligation_amount),0) AS obligation_amount,
		  IFNULL(SUM(deobligation_amount),0) AS deobligation_amount,
		  @uuid AS uuid,
	      @sid AS time_stamp 
	FROM `trams_contract_acc_transaction` GROUP by application_id ;
	
	SET @finalQuery = CONCAT("
SELECT a.recipient_id AS recipient_id, 
		   gbi.recipientAcronym AS recipient_acronym, 
		   a.recipient_name AS recipient_name, 
		   a.recipient_cost_center AS recipient_cost_center, 
		   a.fiscal_year AS fiscal_year, 
		   a.application_number AS application_number, 
		   a.application_name AS application_name, 
		   a.status AS status, 
		   a.application_type AS application_type, 
		   p.project_number AS project_number, 
		   p.project_title AS project_title, 
		   ada.discretionary_id AS discretionary_id, 
		   ada.discretionary_project_name AS discretionary_title, 
		   substring(ada.discretionary_id,2,4) AS discretionary_fiscal_year, 
		   disc.original_fiscal_year AS original_fiscal_year,
		   ada.application_applied_amount AS application_applied_amount_text, 
		   trams_format_date(acr.sent_to_release_date) AS sent_to_release_date, 
		   trams_format_date(acr.logged_release_date) AS logged_release_date,
		   trams_format_date(cast(CONVERT_TZ(re.reservation_finalized_date_time, '+00:00',",p_gmtOffset,") AS char)) AS reservationFinalizedDate_datetime,
		   trams_format_date(cast(CONVERT_TZ(aad.obligated_date_time, '+00:00',",p_gmtOffset,") AS char)) AS obligated_date,
		   trams_format_date(cast(CONVERT_TZ(aad.deobligation_date_time, '+00:00',",p_gmtOffset,") AS char)) AS deobligation_date,
		   trams_format_date(cast(CONVERT_TZ(aad.fta_closeout_approval_date_time, '+00:00',",p_gmtOffset,") AS char)) AS fta_closeout_approval_date,
		   aad.cumulative_contract_fta_amount AS total_reservation_amount,
		   aad.cumulative_contract_oblig_amount AS total_obligation_amount,
		   tca.deobligation_amount AS total_deobligation_amount,
		   a.recipient_contact AS recipient_contact, 
		   a.fta_contact_from_team AS fta_contact_from_team,
		   a.fta_pre_award_manager AS fta_pre_award_manager,   
		   a.fta_post_award_manager AS fta_post_award_manager		   
		FROM trams_application a        
		JOIN trams_app_discretionary_allocations ada 
			 ON a.id = ada.app_id       
	    LEFT JOIN trams_temp_dadr_contract_acc_transaction_by_app tca
			 ON tca.application_id = a.id	   
			 AND tca.uuid = @uuid	
		LEFT JOIN trams_project p 
			 ON a.id = p.application_id
		LEFT JOIN trams_application_award_detail aad
			 ON a.id = aad.application_id 
		LEFT JOIN trams_reservation re 
			 ON a.id = re.application_id 
		LEFT JOIN vwtramsGeneralBusinessInfo gbi 
			 ON gbi.recipientId = a.recipient_id 
		LEFT JOIN trams_app_congressional_release acr 
			 ON a.id = acr.app_id
		LEFT JOIN trams_discretionary_allocations disc 
			 ON disc.id = ada.discretionary_allocation_id			  
		WHERE a.id in (
			SELECT application_id 
			FROM trams_temp_dadr_application_discretionaryId 
			WHERE uuid = @uuid
			)", 
			_dis_whereClause,
			"order by a.recipient_id"
		);	
	
	CALL `trams_log`(@sid, 'trams_get_discretionary_allocation_detail_report', CONCAT('Final Query: ',@finalQuery), 1); 
	
	PREPARE disStmt FROM @finalQuery;
	EXECUTE disStmt;
	
	CALL `trams_log`(@sid, 'trams_get_discretionary_allocation_detail_report', 'Done!', 1);	
	
	DELETE FROM trams_temp_dadr_application_discretionaryId WHERE uuid = @uuid;
	DELETE FROM trams_temp_dadr_contract_acc_transaction_by_app WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_deobligation_funding_summary_for_closeout`(IN `p_contract_award_id` INT)
BEGIN
	/*
		This procedure takes in a contract_award_id and returns data in the Project Scope Obligation Summary for the deobligation process. 
		It must use fewer fields for legacy TEAM data, so there is a check at the beginning of the procedure to build additional group-bys and joins.
		Because this procedure is intended for closeout, it reduces multiple amendment entries for the same contract down by using only the 
		deobligation entry with the highest amendment number. Additionally, it does not need to join on the deobligation_detail table because 
		closeout deob does not iterate and add transactions to the deobligation_detail table as is done in amendment deobligation.
	*/
	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _scopeJoinClause VARCHAR(1000) DEFAULT "";
	DECLARE _newFndJoinClause VARCHAR(1000) DEFAULT "";
	DECLARE _catGroupByClause VARCHAR(1000) DEFAULT "";
	DECLARE _catJoinClause VARCHAR(1000) DEFAULT "";
	DECLARE _allCatGroupByClause VARCHAR(1000) DEFAULT "";
	DECLARE _allCatJoinClause VARCHAR(1000) DEFAULT "";
	DECLARE _appFromTeamFlag TINYINT(1) DEFAULT 0;
	DECLARE _mostRecentAppId INT(11) DEFAULT 0;
	
	SELECT id, app_from_team_flag INTO _mostRecentAppId, _appFromTeamFlag FROM `trams_application` WHERE contractAward_id = p_contract_award_id ORDER BY id DESC LIMIT 1;
	
	IF (_appFromTeamFlag <> 1) THEN
		SET _scopeJoinClause = "
			AND scp.project_number_sequence_number = fnd.project_number_sequence_number
			AND scp.scope_code = fnd.scope_number
			AND scp.scope_suffix = fnd.scope_suffix";
		SET _newFndJoinClause = "
			AND fnd.project_number_sequence_number = newest_fnd.project_number_sequence_number 
			AND fnd.scope_number = newest_fnd.scope_number
			AND fnd.scope_suffix = newest_fnd.scope_suffix
			AND fnd.uza_code = newest_fnd.uza_code";
		SET _catGroupByClause = "
			proj.project_number_sequence_number, 
			t.scope_code, 
			t.scope_suffix,";
		SET _catJoinClause = "
			AND fnd.project_number_sequence_number = cat.project_number_sequence_number 
			AND fnd.scope_number = cat.scope_code 
			AND fnd.scope_suffix = cat.scope_suffix";
		SET _allCatGroupByClause = "
			proj.project_number_sequence_number, 
			cat.scope_code, 
			cat.scope_suffix,";
		SET _allCatJoinClause = "
			AND fnd.project_number_sequence_number = all_cat.project_number_sequence_number 
			AND fnd.scope_number = all_cat.scope_code 
			AND fnd.scope_suffix = all_cat.scope_suffix";
	END IF;
	
	SET _sql = CONCAT(
		"SELECT
			fnd.id AS trams_contract_acc_funding_id,
			app.amendment_number AS amendment_number,
			fnd.po_number AS po_number,
			fnd.project_number AS project_number,
			fnd.scope_number AS scope_number,
			fnd.scope_suffix AS scope_suffix,
			fnd.scope_name AS scope_name,
			fnd.cost_center_code AS cost_center_code,
			fnd.uza_code AS uza_code,
			fnd.account_class_code AS account_class_code,
			fnd.fpc AS fpc,
			fnd.amendment_amount_current AS original_obligation_amount,
			/* BEGIN data used in TrAMS_deobligationDetail */
			fnd.application_id AS application_id,
			fnd.project_id AS project_id,
			fnd.contract_award_id AS contract_award_id,
			fnd.funding_fiscal_year AS funding_fiscal_year,
			fnd.section_code AS section_code, 
			fnd.limitation_code AS limitation_code,
			fnd.authorization_type AS authorization_type,
			fnd.appropriation_code AS appropriation_code,
			fnd.project_number_sequence_number AS project_number_sequence_number,
			fnd.funding_source_name AS funding_source_name,
			accl.dafis_suffix_code AS dafis_suffix_code,
			/* END data used in TrAMS_deobligationDetail */
			ifnull(cat.obligation_amount, 0) - ifnull(cat.deobligation_amount,0) AS net_obligation_amount,
			ROUND(ifnull(all_cat.net_obligation_amount, 0) - ifnull(all_cat.net_deobligation_amount, 0) - ifnull(all_cat.net_disbursement_amount, 0) + ifnull(all_cat.net_refund_amount, 0), 2) AS unliquidated_balance,
			0 AS deobligation_amount
		FROM
			`trams_contract_acc_funding` fnd
			JOIN `trams_application` app
				ON fnd.application_id = app.id
			JOIN `trams_account_class_code_lookup` accl
				ON fnd.account_class_code = accl.account_class_code
			LEFT JOIN (
				/* retrieves obligation and deobligation data for use in calculating net obligation amount */
				SELECT 
				    proj.project_number_sequence_number,
					t.contract_award_id, 
					t.po_number, 
					t.scope_code,
					t.scope_suffix,
					t.uza_code,
					t.cost_center_code,
					t.account_class_code,
					t.fpc,
					sum(t.obligation_amount) AS obligation_amount,
					sum(t.deobligation_amount) AS deobligation_amount
				FROM 
					`trams_contract_acc_transaction` t
					LEFT JOIN `trams_project` proj ON t.project_id = proj.id 
				WHERE 
					contract_award_id = ",p_contract_award_id, "
				GROUP BY 
					t.po_number, ", _catGroupByClause, "
					t.cost_center_code, 
					t.account_class_code, 
					t.fpc,
					t.uza_code
			) cat 
				ON 
					fnd.contract_award_id = cat.contract_award_id  ", _catJoinClause, "
					AND fnd.po_number = cat.po_number 
					AND fnd.cost_center_code = cat.cost_center_code
					AND fnd.account_class_code = cat.account_class_code
					AND fnd.fpc = cat.fpc
					AND fnd.uza_code = cat.uza_code				
					
			LEFT JOIN (
				/* retrieves deobligation, disbursement, and refund data for use in calculating unliquidated balance. this is a summary column across the contract award, meaning it will get to total fundings and appear duplicated for each amendment. */
				SELECT
					proj.project_number_sequence_number,
					cat.contract_award_id, 
					cat.po_number, 
					cat.scope_code,
					cat.scope_suffix,
					cat.uza_code,
					cat.cost_center_code,
					cat.account_class_code,
					cat.fpc,
					sum(cat.obligation_amount) AS net_obligation_amount,
					sum(cat.deobligation_amount) AS net_deobligation_amount,
					sum(cat.disbursement_amount) AS net_disbursement_amount,
					sum(cat.refund_amount) AS net_refund_amount
				FROM 
					`trams_contract_acc_transaction` cat 
					 LEFT JOIN `trams_project` proj ON cat.project_id = proj.id 
				WHERE 
					contract_award_id = ",p_contract_award_id, " 
				GROUP BY 
					cat.po_number, ", _allCatGroupByClause, "
					cat.cost_center_code, 
					cat.account_class_code, 
					cat.fpc
				ORDER BY cat.id DESC
			) all_cat 
				ON 
					fnd.contract_award_id = all_cat.contract_award_id  ", _allCatJoinClause, "
					AND fnd.po_number = all_cat.po_number 
					AND fnd.cost_center_code = all_cat.cost_center_code
					AND fnd.account_class_code = all_cat.account_class_code
					AND fnd.fpc = all_cat.fpc
					

		WHERE 
			fnd.contract_award_id = ",p_contract_award_id," 
			AND ROUND(ifnull(all_cat.net_obligation_amount, 0) - ifnull(all_cat.net_deobligation_amount, 0) - ifnull(all_cat.net_disbursement_amount, 0) + ifnull(all_cat.net_refund_amount, 0), 2) > 0
			AND fnd.application_id = (
			  SELECT max(id) 
			  FROM `trams_application` 
			  WHERE trams_application.contractAward_id = ",p_contract_award_id,")
		ORDER BY app.amendment_number, fnd.account_class_code ASC;"
	);

	SET @deobQuery = _sql;
	
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_deobligation_funding_summary_for_closeout', concat('Deobligation Summary Query: ', @deobQuery), 1);
	
	PREPARE deobStmt FROM @deobQuery;
	EXECUTE deobStmt;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_fyfap_report_excel`(p_fundingFiscalYear VARCHAR(512), p_lapseYear VARCHAR(255), p_uzaCode VARCHAR(255), p_appropriationCode VARCHAR(255), p_sectionCode VARCHAR(255), p_limitationCode VARCHAR(255), p_return_limit VARCHAR(20))
BEGIN

  /*
     Stored Procedure Name : trams_get_fyfap_report_excel
     Description : To populate the data for TrAMS nightly 'Generate Static Reports' process.
     Updated by  : Garrison Dean (2019.Mar.20)
  */

  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
  DECLARE _limitClause VARCHAR(100) DEFAULT " ";

  SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
  -- added UUID for session reference
  SET @uuid = UUID_SHORT();

  CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.uza_code in (",NULLIF(p_uzaCode,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.funding_fiscal_year in (",NULLIF(p_fundingFiscalYear,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.section_id in (",NULLIF(p_sectionCode,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.limitation_code in ('",NULLIF(p_limitationCode,''),"')"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.appropriation_code in (",NULLIF(p_appropriationCode,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.lapse_period in (",NULLIF(p_lapseYear,''),")"),"")));

  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_apportionment_fyfap_rollup_id, uuid, and sid columns
  SET @apportionment_tempQuery = CONCAT("INSERT INTO trams_temp_apportionment_fyfap_rollup
  SELECT
    NULL AS trams_temp_apportionment_fyfap_rollup_id,
    `a`.`uza_code` AS `uza_code`,
    `a`.`funding_fiscal_year` AS `funding_fiscal_year`,
    `a`.`appropriation_code` AS `appropriation_code`,
    `a`.`section_id` AS `section_id`,
    `a`.`limitation_code` AS `limitation_code`,
    `a`.`lapse_period` AS `lapse_period`,
    `a`.`active_flag` AS `active_flag`,
    IFNULL(SUM(`a`.`apportionment`), 0) AS `apportionment`,
    IFNULL(SUM(`a`.`operating_cap`), 0) AS operating_cap,
    IFNULL(SUM(`a`.`ceiling_amount`), 0) AS ceiling_amount,
    @uuid AS uuid,
    @sid AS sid
  FROM trams_apportionment a
  WHERE `a`.`active_flag` = 1 ", _whereClause,
  "GROUP BY
    uza_code,
    funding_fiscal_year,
    appropriation_code,
    section_id,
    limitation_code");

  CALL `trams_log`(@sid, 'trams_get_fyfap_report_excel', CONCAT('Apportionment temp Query: ',@apportionment_tempQuery), 1);

  PREPARE apportionmentStmtTemp FROM @apportionment_tempQuery;
  EXECUTE apportionmentStmtTemp;

  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_apportionment_history_fyfap_rollup_id, uuid, and sid columns
  SET @apportionmentHistory_tempQuery = CONCAT("INSERT INTO trams_temp_apportionment_history_fyfap_rollup
  SELECT
    NULL AS trams_temp_apportionment_history_fyfap_rollup_id,
    uza_code AS uza_code,
    funding_fiscal_year AS funding_fiscal_year,
    appropriation_code AS appropriation_code,
    section_id AS section_id,
    limitation_code AS limitation_code,
    IFNULL(SUM(ceiling_amount),0) AS ceiling_amount,
    IFNULL(SUM(cap_amount),0) AS cap_amount,
    IFNULL(SUM(reserved_amount),0) AS reserved_amount,
    IFNULL(SUM(unreserve_amount),0) AS unreserve_amount,
    IFNULL(SUM(obligated_amount),0) AS obligated_amount,
    IFNULL(SUM(recovery_amount),0) AS recovery_amount,
    IFNULL(SUM(transfer_in_amount),0) AS transfer_in_amount,
    IFNULL(SUM(transfer_out_amount),0) AS transfer_out_amount,
    IFNULL(SUM(modified_amount),0) AS modified_amount,
    IFNULL(SUM(split_amount),0) AS split_amount,
    IFNULL(SUM(cap_reserved_amt),0) AS cap_reserved_amt,
    IFNULL(SUM(cap_unreserved_amt),0) AS cap_unreserved_amt,
    IFNULL(SUM(cap_obligated_amt),0) AS cap_obligated_amt,
    IFNULL(SUM(cap_recovery_amt),0) AS cap_recovery_amt,
    IFNULL(SUM(cap_transfer_in_amt),0) AS cap_transfer_in_amt,
    IFNULL(SUM(cap_transfer_out_amt),0) AS cap_transfer_out_amt,
    IFNULL(SUM(cap_modify_amt),0) AS cap_modify_amt,
    IFNULL(SUM(ceiling_recovery_amt),0) AS ceiling_recovery_amt,
    IFNULL(SUM(ceiling_transfer_in_amt),0) AS ceiling_transfer_in_amt,
    IFNULL(SUM(ceiling_transfer_out_amt),0) AS ceiling_transfer_out_amt,
    IFNULL(SUM(ceiling_modify_amt),0) AS ceiling_modify_amt,
    IFNULL(SUM(ceiling_reserved_amt),0) AS ceiling_reserved_amt,
    IFNULL(SUM(ceiling_unreserve_amt),0) AS ceiling_unreserve_amt,
    IFNULL(SUM(ceiling_obligated_amt),0) AS ceiling_obligated_amt,
    @uuid AS uuid,
    @sid AS sid
  FROM trams_apportionment_history
  GROUP BY
    uza_code,
    funding_fiscal_year,
    appropriation_code,
    section_id,
    limitation_code");

	CALL `trams_log`(@sid, 'trams_get_fyfap_report_excel', CONCAT('Apportionment History temp Query: ',@apportionmentHistory_tempQuery), 1);

	PREPARE apportionmentHistory_tempQuery FROM @apportionmentHistory_tempQuery;
	EXECUTE apportionmentHistory_tempQuery;

-- replaced table name temp_apportionment_fyfap_rollup with trams_temp_apportionment_fyfap_rollup on line 167
-- replaced table name temp_appor_history_fyfap_rollup with trams_temp_apportionment_history_fyfap_rollup on line 168
-- added AND uuid = @uuid condition on lines 173 and 215
	SET @apportionmentQuery = CONCAT("
	SELECT
	  `a`.`uza_code` AS `uzacode_text`,
	  uza_reference.uza_name,
	  IF(`a`.`uza_code` like '%0000', state_population.state_population, uza_population.population),
	  tcc.cost_center_shortname,
	  CONCAT(`a`.`funding_fiscal_year`,'.',`a`.`appropriation_code`,'.',`a`.`section_id`,'.',`a`.`limitation_code`) AS `accountcode_text`,
	  `a`.`appropriation_code` AS `appropriationcode_text`,
	  `a`.`section_id` AS `sectionid_text`,
	  `a`.`limitation_code` AS `limitationcode_text`,
	  `a`.`funding_fiscal_year` AS `fundingfiscalyear_text`,
	  `a`.`lapse_period` AS `lapseyear_text`,
	  IFNULL(`a`.`apportionment`,0) AS `app_initialamt_dec`,
	  IFNULL(SUM(`ah`.`modified_amount`),0) AS `app_modificationamt_dec`,
	  IFNULL(SUM(`ah`.`transfer_in_amount`),0) AS `app_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`transfer_out_amount`),0) AS `app_transferoutamt_dec`,
	  IFNULL(`a`.`apportionment`,0) + IFNULL(SUM(`ah`.`modified_amount`),0)
		+ IFNULL(SUM(`ah`.`transfer_in_amount`),0) - IFNULL(SUM(`ah`.`transfer_out_amount`),0) AS `app_effectiveauthamt_dec`,
	  IFNULL(SUM(`ah`.`reserved_amount`),0) - IFNULL(SUM(`ah`.`unreserve_amount`),0) AS `app_reserveamt_dec`,
	  IFNULL(SUM(`ah`.`obligated_amount`),0) AS `app_obligationamt_dec`,
	  IFNULL(SUM(`ah`.`recovery_amount`),0) AS `app_recoveryamt_dec`,
	  (((((((IFNULL(`a`.`apportionment`,0) + ifnull(SUM(`ah`.`modified_amount`),0)) + IFNULL(SUM(`ah`.`transfer_in_amount`),0))
		+ IFNULL(SUM(`ah`.`unreserve_amount`),0)) + IFNULL(SUM(`ah`.`recovery_amount`),0))
		- IFNULL(SUM(`ah`.`reserved_amount`),0)) - IFNULL(SUM(`ah`.`obligated_amount`),0)) - IFNULL(SUM(`ah`.`transfer_out_amount`),0)) AS `app_availableamt_dec`,
	  IFNULL(SUM(`a`.`operating_cap`),0) AS `cap_initialamt_dec`,
	  IFNULL(SUM(`ah`.`cap_modify_amt`),0) AS `cap_modificaionamt_dec`,
	  IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0) AS `cap_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0) AS `cap_transferoutamt_dec`,
	  (((IFNULL(SUM(`a`.`operating_cap`),0) + IFNULL(SUM(`ah`.`cap_modify_amt`),0)) + IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0)) - IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0)) AS `cap_effectiveauthamt_dec`,
	  (IFNULL(SUM(`ah`.`cap_reserved_amt`),0) - IFNULL(SUM(`ah`.`cap_unreserved_amt`),0)) AS `cap_reservationamt_dec`,
	  IFNULL(SUM(`ah`.`cap_obligated_amt`),0) AS `cap_obligateamt_dec`,
	  IFNULL(SUM(`ah`.`cap_recovery_amt`),0) AS `cap_recoveryamt_dec`,
	  (((((((ifnull(SUM(`a`.`operating_cap`),0) + IFNULL(SUM(`ah`.`cap_modify_amt`),0)) + IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0))
		+ IFNULL(SUM(`ah`.`cap_unreserved_amt`),0)) + IFNULL(SUM(`ah`.`cap_recovery_amt`),0)) - IFNULL(SUM(`ah`.`cap_reserved_amt`),0))
		- IFNULL(SUM(`ah`.`cap_obligated_amt`),0)) - IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0)) AS `cap_availableamt_dec`,
	  IFNULL(SUM(`a`.`ceiling_amount`),0) AS `ceiling_initialamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_modify_amt`),0) AS `ceiling_modificationamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0) AS `ceiling_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0) AS `ceiling_transferoutamt_dec`,
	  (((IFNULL(SUM(`a`.`ceiling_amount`),0) + IFNULL(SUM(`ah`.`ceiling_modify_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_effectiveauthamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_recovery_amt`),0) AS `ceiling_recoveryamt_dec`,
	  (((((((IFNULL(SUM(`a`.`ceiling_amount`),0) + IFNULL(SUM(`ah`.`ceiling_modify_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_unreserve_amt`),0))
		+ IFNULL(SUM(`ah`.`ceiling_recovery_amt`),0)) - IFNULL(sum(`ah`.`ceiling_reserved_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_obligated_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_availableamt_dec`
		FROM `trams_temp_apportionment_fyfap_rollup` `a`
		LEFT JOIN `trams_temp_apportionment_history_fyfap_rollup` ah  ON `a`.`uza_code` = `ah`.`uza_code`
	  AND `a`.`funding_fiscal_year` = `ah`.`funding_fiscal_year`
	  AND `a`.`section_id` = `ah`.`section_id`
	  AND `a`.`appropriation_code` = `ah`.`appropriation_code`
	  AND `a`.`limitation_code` = `ah`.`limitation_code`
	  AND ah.uuid = @uuid
		JOIN
	  (SELECT
		a.uza_code AS uza_code,
		tucr.uza_name AS uza_name,
		tucr.cost_center_code AS cost_center_code,
		MAX(tucr.census_year) AS census_year
	  FROM trams_apportionment a
	  JOIN trams_uza_code_reference tucr ON a.uza_code = tucr.uza_code
	  GROUP BY
		a.uza_code) uza_reference ON a.uza_code = uza_reference.uza_code
		LEFT JOIN trams_cost_center tcc ON uza_reference.cost_center_code = tcc.cost_center_code
		LEFT JOIN
	  (SELECT
		tupbs.uza_code,
		tupbs.population AS population,
		MAX(tupbs.fiscal_year) AS fiscal_year
	  FROM trams_uza_population_by_state tupbs
	  WHERE tupbs.version = 8
	  GROUP BY
	  tupbs.uza_code) uza_population ON a.uza_code = uza_population.uza_code
		LEFT JOIN
	  (SELECT
		tsp.state_id AS state_id,
		ts.uza_code AS state_uza_code,
		tsp.population AS state_population,
		MAX(tsp.fiscal_year)
	  FROM trams_state_population tsp
	  JOIN trams_state ts ON ts.id = tsp.state_id
	  WHERE tsp.version = 8
	  GROUP BY ts.uza_code) state_population ON a.uza_code = state_population.state_uza_code
		WHERE 1 ",_whereClause,"
	  AND a.uuid = @uuid
		GROUP BY
	  `a`.`funding_fiscal_year`,
	  `a`.`section_id`,
	  `a`.`limitation_code`,
	  `a`.`appropriation_code`,
	  `a`.`uza_code`,
	  `a`.`lapse_period`
		ORDER BY `ah`.`funding_fiscal_year`",_limitClause);
		
	IF p_fundingFiscalYear IS NOT NULL THEN
	  SET @batchContainsValidYear = 0;
	  SET @i = 1;
	  SET @numberOfEntriesInBatch = (
		LENGTH(REPLACE(p_fundingFiscalYear, ' ', '')) - 
		LENGTH(REPLACE(REPLACE(p_fundingFiscalYear, ',', ''), ' ', ''))
		+1
	  );
	  WHILE @i <= @numberOfEntriesInBatch DO
		IF ACS_FN_SplitStringAtDelimiter(p_fundingFiscalYear, ',', @i) >= 2015 THEN
		  SET @batchContainsValidYear = 1;
		END IF;
		SET @i = @i + 1;  
	  END WHILE;
    END IF;
	
	IF p_fundingFiscalYear IS NOT NULL AND @batchContainsValidYear THEN
		SET @tramsFYAPResultSetQuery = CONCAT("INSERT INTO tramsFYAPResultSet
	SELECT
		NULL,
	  `a`.`uza_code` AS `uzacode_text`,
	  uza_reference.uza_name,
	  IF(`a`.`uza_code` like '%0000', state_population.state_population, uza_population.population),
	  tcc.cost_center_shortname,
	  CONCAT(`a`.`funding_fiscal_year`,'.',`a`.`appropriation_code`,'.',`a`.`section_id`,'.',`a`.`limitation_code`) AS `accountcode_text`,
	  `a`.`appropriation_code` AS `appropriationcode_text`,
	  `a`.`section_id` AS `sectionid_text`,
	  `a`.`limitation_code` AS `limitationcode_text`,
	  `a`.`funding_fiscal_year` AS `fundingfiscalyear_text`,
	  `a`.`lapse_period` AS `lapseyear_text`,
	  IFNULL(`a`.`apportionment`,0) AS `app_initialamt_dec`,
	  IFNULL(SUM(`ah`.`modified_amount`),0) AS `app_modificationamt_dec`,
	  IFNULL(SUM(`ah`.`transfer_in_amount`),0) AS `app_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`transfer_out_amount`),0) AS `app_transferoutamt_dec`,
	  IFNULL(`a`.`apportionment`,0) + IFNULL(SUM(`ah`.`modified_amount`),0)
		+ IFNULL(SUM(`ah`.`transfer_in_amount`),0) - IFNULL(SUM(`ah`.`transfer_out_amount`),0) AS `app_effectiveauthamt_dec`,
	  IFNULL(SUM(`ah`.`reserved_amount`),0) - IFNULL(SUM(`ah`.`unreserve_amount`),0) AS `app_reserveamt_dec`,
	  IFNULL(SUM(`ah`.`obligated_amount`),0) AS `app_obligationamt_dec`,
	  IFNULL(SUM(`ah`.`recovery_amount`),0) AS `app_recoveryamt_dec`,
	  (((((((IFNULL(`a`.`apportionment`,0) + ifnull(SUM(`ah`.`modified_amount`),0)) + IFNULL(SUM(`ah`.`transfer_in_amount`),0))
		+ IFNULL(SUM(`ah`.`unreserve_amount`),0)) + IFNULL(SUM(`ah`.`recovery_amount`),0))
		- IFNULL(SUM(`ah`.`reserved_amount`),0)) - IFNULL(SUM(`ah`.`obligated_amount`),0)) - IFNULL(SUM(`ah`.`transfer_out_amount`),0)) AS `app_availableamt_dec`,
	  IFNULL(SUM(`a`.`operating_cap`),0) AS `cap_initialamt_dec`,
	  IFNULL(SUM(`ah`.`cap_modify_amt`),0) AS `cap_modificaionamt_dec`,
	  IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0) AS `cap_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0) AS `cap_transferoutamt_dec`,
	  (((IFNULL(SUM(`a`.`operating_cap`),0) + IFNULL(SUM(`ah`.`cap_modify_amt`),0)) + IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0)) - IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0)) AS `cap_effectiveauthamt_dec`,
	  (IFNULL(SUM(`ah`.`cap_reserved_amt`),0) - IFNULL(SUM(`ah`.`cap_unreserved_amt`),0)) AS `cap_reservationamt_dec`,
	  IFNULL(SUM(`ah`.`cap_obligated_amt`),0) AS `cap_obligateamt_dec`,
	  IFNULL(SUM(`ah`.`cap_recovery_amt`),0) AS `cap_recoveryamt_dec`,
	  (((((((ifnull(SUM(`a`.`operating_cap`),0) + IFNULL(SUM(`ah`.`cap_modify_amt`),0)) + IFNULL(SUM(`ah`.`cap_transfer_in_amt`),0))
		+ IFNULL(SUM(`ah`.`cap_unreserved_amt`),0)) + IFNULL(SUM(`ah`.`cap_recovery_amt`),0)) - IFNULL(SUM(`ah`.`cap_reserved_amt`),0))
		- IFNULL(SUM(`ah`.`cap_obligated_amt`),0)) - IFNULL(SUM(`ah`.`cap_transfer_out_amt`),0)) AS `cap_availableamt_dec`,
	  IFNULL(SUM(`a`.`ceiling_amount`),0) AS `ceiling_initialamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_modify_amt`),0) AS `ceiling_modificationamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0) AS `ceiling_transferinamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0) AS `ceiling_transferoutamt_dec`,
	  (((IFNULL(SUM(`a`.`ceiling_amount`),0) + IFNULL(SUM(`ah`.`ceiling_modify_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_effectiveauthamt_dec`,
	  IFNULL(SUM(`ah`.`ceiling_recovery_amt`),0) AS `ceiling_recoveryamt_dec`,
	  (((((((IFNULL(SUM(`a`.`ceiling_amount`),0) + IFNULL(SUM(`ah`.`ceiling_modify_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_transfer_in_amt`),0)) + IFNULL(SUM(`ah`.`ceiling_unreserve_amt`),0))
		+ IFNULL(SUM(`ah`.`ceiling_recovery_amt`),0)) - IFNULL(sum(`ah`.`ceiling_reserved_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_obligated_amt`),0)) - IFNULL(SUM(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_availableamt_dec`
		FROM `trams_temp_apportionment_fyfap_rollup` `a`
		LEFT JOIN `trams_temp_apportionment_history_fyfap_rollup` ah  ON `a`.`uza_code` = `ah`.`uza_code`
	  AND `a`.`funding_fiscal_year` = `ah`.`funding_fiscal_year`
	  AND `a`.`section_id` = `ah`.`section_id`
	  AND `a`.`appropriation_code` = `ah`.`appropriation_code`
	  AND `a`.`limitation_code` = `ah`.`limitation_code`
		JOIN
	  (SELECT
		a.uza_code AS uza_code,
		tucr.uza_name AS uza_name,
		tucr.cost_center_code AS cost_center_code,
		MAX(tucr.census_year) AS census_year
	  FROM trams_apportionment a
	  JOIN trams_uza_code_reference tucr ON a.uza_code = tucr.uza_code
	  GROUP BY
		a.uza_code) uza_reference ON a.uza_code = uza_reference.uza_code
		LEFT JOIN trams_cost_center tcc ON uza_reference.cost_center_code = tcc.cost_center_code
		LEFT JOIN
	  (SELECT
		tupbs.uza_code,
		tupbs.population AS population,
		MAX(tupbs.fiscal_year) AS fiscal_year
	  FROM trams_uza_population_by_state tupbs
	  WHERE tupbs.version = 8
	  GROUP BY
	  tupbs.uza_code) uza_population ON a.uza_code = uza_population.uza_code
		LEFT JOIN
	  (SELECT
		tsp.state_id AS state_id,
		ts.uza_code AS state_uza_code,
		tsp.population AS state_population,
		MAX(tsp.fiscal_year)
	  FROM trams_state_population tsp
	  JOIN trams_state ts ON ts.id = tsp.state_id
	  WHERE tsp.version = 8
	  GROUP BY ts.uza_code) state_population ON a.uza_code = state_population.state_uza_code
		WHERE 1 ",_whereClause,"
	  AND a.uuid = @uuid
		GROUP BY
	  `a`.`funding_fiscal_year`,
	  `a`.`section_id`,
	  `a`.`limitation_code`,
	  `a`.`appropriation_code`,
	  `a`.`uza_code`,
	  `a`.`lapse_period`
		ORDER BY `ah`.`funding_fiscal_year`",_limitClause);
	END IF;

	CALL `trams_log`(@sid, 'trams_get_fyfap_report_excel', CONCAT('Apportionment Query: ',@apportionmentQuery), 1);
	CALL `trams_log`(@sid, 'trams_get_fyfap_report_excel', CONCAT('Apportionment Where Query: ',_whereClause), 1);

	PREPARE apportionmentStmt FROM @apportionmentQuery;
	EXECUTE apportionmentStmt;
	
	/*If the batch contains any valid years, we are writing the entire batch and then deleting from the table if any portions of the batch are unneeded.*/
	IF p_fundingFiscalYear IS NOT NULL AND @batchContainsValidYear THEN
		PREPARE tramsFYAPResultSetStmt FROM @tramsFYAPResultSetQuery;
		EXECUTE tramsFYAPResultSetStmt;
		DELETE FROM tramsFYAPResultSet WHERE FundingFiscalYear < 2015;
	END IF;	  

	CALL `trams_log`(@sid, 'trams_get_fyfap_report_excel', 'Done!', 1);

	DELETE FROM trams_temp_apportionment_fyfap_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_apportionment_history_fyfap_rollup WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_funding_summary_for_amendment`(IN `p_application_id` INT, IN `p_revision_num` INT)
BEGIN

DECLARE _onClause VARCHAR(4096) DEFAULT " ";
DECLARE _appCreation DATETIME;

SET _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(NULL,p_application_id);

SET _onClause = CONCAT('fsr.LatestFlagStartDate <= ''',_appCreation,''' AND fsr.LatestFlagEndDate >= ''',_appCreation,'''');

SET @uuid = UUID_SHORT();
SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

		-- Populate Permanent tables
		INSERT INTO trams_temp_amendment_funding_line_item (id, funding_source_name, scope_code, other_budget, uuid, time_stamp)
		SELECT id, funding_source_name, scope_code, other_budget, @uuid AS uuid, @sid AS time_stamp
		FROM trams_line_item
		WHERE app_id = p_application_id;

		INSERT INTO trams_temp_amendment_funding_change_log (id, line_item_id, latest_amend_fta_amount, revised_fta_amount, uuid, time_stamp)
		SELECT id, line_item_id, latest_amend_fta_amount, revised_fta_amount, @uuid AS uuid, @sid AS time_stamp
		FROM trams_line_item_change_log
		WHERE application_id = p_application_id 
		AND revision_number = p_revision_num;


		-- Run SP
SET @sqlStatement = CONCAT('SELECT '
			,p_application_id,' AS application_id,
			fsr.funding_source_description AS fundingSource_text,
			IFNULL(SUM(cl.latest_amend_fta_amount), 0) AS orgionalAwardAmount_text,
			IFNULL(SUM(cl.revised_fta_amount), 0) AS budgetRevisionAmount_text,
			IFNULL(SUM(cl.revised_fta_amount), 0) - IFNULL(SUM(cl.latest_amend_fta_amount), 0) AS differenceAmount_text
		FROM
			trams_temp_amendment_funding_change_log cl
			JOIN trams_temp_amendment_funding_line_item li
				ON cl.line_item_id = li.id
			    AND li.uuid = @uuid	
			JOIN trams_funding_source_reference fsr
				ON li.funding_source_name = fsr.funding_source_name AND ',_onClause,'
		WHERE li.other_budget = 0
		AND cl.uuid = @uuid
		GROUP BY li.funding_source_name
		ORDER BY cl.id DESC');

		PREPARE sqlStatement FROM @sqlStatement;
		EXECUTE sqlStatement;

		-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
        DELETE FROM trams_temp_amendment_funding_line_item WHERE uuid = @uuid;
        DELETE FROM trams_temp_amendment_funding_change_log WHERE uuid = @uuid;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_cumulative_apportionments`(IN `p_fundingFiscalYear` VARCHAR(255), IN `p_lapseYear` VARCHAR(255), IN `p_uzaCode` VARCHAR(255), IN `p_appropriationCode` VARCHAR(255), IN `p_sectionCode` VARCHAR(255), IN `p_limitationCode` VARCHAR(255), IN `p_return_limit` VARCHAR(20))
    NO SQL
BEGIN

	declare _sql varchar(40960) default "";
	declare _whereClause varchar(4096) default " ";
	declare _limitClause varchar(100) default " ";
	
	call trams_get_sql_limit_clause(p_return_limit,_limitClause);
		
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.uza_code in (",NULLIF(p_uzaCode,''),")"),"")));
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.funding_fiscal_year in (",NULLIF(p_fundingFiscalYear,''),")"),"")));
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.section_id in (",NULLIF(p_sectionCode,''),")"),"")));
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.limitation_code in ('",NULLIF(p_limitationCode,''),"')"),"")));
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.appropriation_code in (",NULLIF(p_appropriationCode,''),")"),"")));
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.lapse_period in (",NULLIF(p_lapseYear,''),")"),"")));
	

drop  table if exists trams_temp_apportionment_fyfap_rollup;
drop  table if exists trams_temp_appor_history_fyfap_rollup;

set @apportionment_tempQuery = concat("create table trams_temp_apportionment_fyfap_rollup 
 select 	`a`.`uza_code` AS `uza_code`, 
		`a`.`funding_fiscal_year` AS `funding_fiscal_year`, 
		`a`.`appropriation_code` AS `appropriation_code`, 
		`a`.`section_id` AS `section_id`, 
		`a`.`limitation_code` AS `limitation_code`,
		`a`.`lapse_period` AS `lapse_period`, 
		a.active_flag AS active_flag,
		Ifnull(Sum(`a`.`apportionment`), 0) AS `apportionment`,  
		Ifnull(Sum(`a`.`operating_cap`), 0) AS operating_cap,
        Ifnull(Sum(`a`.`ceiling_amount`), 0) AS ceiling_amount
from    trams_apportionment a where `a`.`active_flag` = 1 ",_whereClause,
"group by uza_code, funding_fiscal_year, appropriation_code,section_id, limitation_code");


	set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_cumulative_apportionments', concat('Apportionment temp Query: ',@apportionment_tempQuery), 1); 

	PREPARE apportionmentStmtTemp FROM @apportionment_tempQuery;
	EXECUTE apportionmentStmtTemp;

alter table trams_temp_apportionment_fyfap_rollup add index `apportionment_rollup_idx` (`uza_code`,`funding_fiscal_year`,`appropriation_code`,`section_id`,`limitation_code`);
			
set @apportionmentHistory_tempQuery = concat("create table trams_temp_appor_history_fyfap_rollup 
	select
		uza_code as uza_code,
		funding_fiscal_year as funding_fiscal_year,
		appropriation_code as appropriation_code,
		section_id as section_id,
		limitation_code as limitation_code,
		ifnull(sum(ceiling_amount),0) as ceiling_amount,
		ifnull(sum(cap_amount),0) as cap_amount,
		ifnull(sum(reserved_amount),0) as reserved_amount,
		ifnull(sum(unreserve_amount),0) as unreserve_amount,
		ifnull(sum(obligated_amount),0) as obligated_amount,
		ifnull(sum(recovery_amount),0) as recovery_amount,
		ifnull(sum(transfer_in_amount),0) as transfer_in_amount,
		ifnull(sum(transfer_out_amount),0) as transfer_out_amount,
		ifnull(sum(modified_amount),0) as modified_amount,
		ifnull(sum(split_amount),0) as split_amount,
		ifnull(sum(cap_reserved_amt),0) as cap_reserved_amt,
		ifnull(sum(cap_unreserved_amt),0) as cap_unreserved_amt,
		ifnull(sum(cap_obligated_amt),0) as cap_obligated_amt,
		ifnull(sum(cap_recovery_amt),0) as cap_recovery_amt,
		ifnull(sum(cap_transfer_in_amt),0) as cap_transfer_in_amt,
		ifnull(sum(cap_transfer_out_amt),0) as cap_transfer_out_amt,
		ifnull(sum(cap_modify_amt),0) as cap_modify_amt,
		ifnull(sum(ceiling_recovery_amt),0) as ceiling_recovery_amt,
		ifnull(sum(ceiling_transfer_in_amt),0) as ceiling_transfer_in_amt,
		ifnull(sum(ceiling_transfer_out_amt),0) as ceiling_transfer_out_amt,
		ifnull(sum(ceiling_modify_amt),0) as ceiling_modify_amt,
		ifnull(sum(ceiling_reserved_amt),0) as ceiling_reserved_amt,
		ifnull(sum(ceiling_unreserve_amt),0) as ceiling_unreserve_amt,
		ifnull(sum(ceiling_obligated_amt),0) as ceiling_obligated_amt
	from
		trams_apportionment_history
	group by 
		uza_code, funding_fiscal_year, appropriation_code, section_id, limitation_code");

			
			set @sidd = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_cumulative_apportionments', concat('Apportionment History temp Query: ',@apportionmentHistory_tempQuery), 1); 

	PREPARE apportionmentHistory_tempQuery FROM @apportionmentHistory_tempQuery;
	EXECUTE apportionmentHistory_tempQuery;
	
	alter table trams_temp_appor_history_fyfap_rollup add index `apportionment_history_rollup_idx` (`uza_code`,`funding_fiscal_year`,`appropriation_code`,`section_id`,`limitation_code`);

	drop table if exists trams_get_cumulative_apportionments_result;
	
	set @apportionmentQuery = concat("create table trams_get_cumulative_apportionments_result ","
		select `a`.`uza_code` AS `uzacode_text`,
		uza_reference.uza_name AS uza_name,
		if(`a`.`uza_code` like '%0000', state_population.state_population, uza_population.population) as state_population,
		tcc.cost_center_shortname as cost_center_shortname,
		concat(`a`.`funding_fiscal_year`,'.',`a`.`appropriation_code`,'.',`a`.`section_id`,'.',`a`.`limitation_code`) AS `accountcode_text`,
		`a`.`appropriation_code` AS `appropriationcode_text`,
		`a`.`section_id` AS `sectionid_text`,
		`a`.`limitation_code` AS `limitationcode_text`,
		`a`.`funding_fiscal_year` AS `fundingfiscalyear_text`,
		`a`.`lapse_period` AS `lapseyear_text`,
		ifnull(`a`.`apportionment`,0) AS `app_initialamt_dec`,
		
		
		ifnull(sum(`ah`.`modified_amount`),0) AS `app_modificationamt_dec`,
		ifnull(sum(`ah`.`transfer_in_amount`),0) AS `app_transferinamt_dec`,
		ifnull(sum(`ah`.`transfer_out_amount`),0) AS `app_transferoutamt_dec`,
		ifnull(`a`.`apportionment`,0) + ifnull(sum(`ah`.`modified_amount`),0)
			+ ifnull(sum(`ah`.`transfer_in_amount`),0) - ifnull(sum(`ah`.`transfer_out_amount`),0) AS `app_effectiveauthamt_dec`,
		ifnull(sum(`ah`.`reserved_amount`),0) - ifnull(sum(`ah`.`unreserve_amount`),0) AS `app_reserveamt_dec`,
		ifnull(sum(`ah`.`obligated_amount`),0) AS `app_obligationamt_dec`,
		ifnull(sum(`ah`.`recovery_amount`),0) AS `app_recoveryamt_dec`,
		
		
		(((((((ifnull(`a`.`apportionment`,0) + ifnull(sum(`ah`.`modified_amount`),0)) + ifnull(sum(`ah`.`transfer_in_amount`),0)) 
			+ ifnull(sum(`ah`.`unreserve_amount`),0)) + ifnull(sum(`ah`.`recovery_amount`),0))
			- ifnull(sum(`ah`.`reserved_amount`),0)) - ifnull(sum(`ah`.`obligated_amount`),0)) - ifnull(sum(`ah`.`transfer_out_amount`),0)) AS `app_availableamt_dec`,			
		ifnull(sum(`a`.`operating_cap`),0) AS `cap_initialamt_dec`,
		ifnull(sum(`ah`.`cap_modify_amt`),0) AS `cap_modificaionamt_dec`,
		ifnull(sum(`ah`.`cap_transfer_in_amt`),0) AS `cap_transferinamt_dec`,
		ifnull(sum(`ah`.`cap_transfer_out_amt`),0) AS `cap_transferoutamt_dec`,
		(((ifnull(sum(`a`.`operating_cap`),0) + ifnull(sum(`ah`.`cap_modify_amt`),0)) + ifnull(sum(`ah`.`cap_transfer_in_amt`),0)) - ifnull(sum(`ah`.`cap_transfer_out_amt`),0)) AS `cap_effectiveauthamt_dec`,
		(ifnull(sum(`ah`.`cap_reserved_amt`),0) - ifnull(sum(`ah`.`cap_unreserved_amt`),0)) AS `cap_reservationamt_dec`,
		ifnull(sum(`ah`.`cap_obligated_amt`),0) AS `cap_obligateamt_dec`,
		ifnull(sum(`ah`.`cap_recovery_amt`),0) AS `cap_recoveryamt_dec`,
		
		
		(((((((ifnull(sum(`a`.`operating_cap`),0) + ifnull(sum(`ah`.`cap_modify_amt`),0)) + ifnull(sum(`ah`.`cap_transfer_in_amt`),0)) 
			+ ifnull(sum(`ah`.`cap_unreserved_amt`),0)) + ifnull(sum(`ah`.`cap_recovery_amt`),0)) - ifnull(sum(`ah`.`cap_reserved_amt`),0)) 
			- ifnull(sum(`ah`.`cap_obligated_amt`),0)) - ifnull(sum(`ah`.`cap_transfer_out_amt`),0)) AS `cap_availableamt_dec`,
			
			
		ifnull(sum(`a`.`ceiling_amount`),0) AS `ceiling_initialamt_dec`,
		
		
		ifnull(sum(`ah`.`ceiling_modify_amt`),0) AS `ceiling_modificationamt_dec`,
		ifnull(sum(`ah`.`ceiling_transfer_in_amt`),0) AS `ceiling_transferinamt_dec`,
		ifnull(sum(`ah`.`ceiling_transfer_out_amt`),0) AS `ceiling_transferoutamt_dec`,
		
		
		(((ifnull(sum(`a`.`ceiling_amount`),0) + ifnull(sum(`ah`.`ceiling_modify_amt`),0)) + ifnull(sum(`ah`.`ceiling_transfer_in_amt`),0)) - ifnull(sum(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_effectiveauthamt_dec`,
		
		
		ifnull(sum(`ah`.`ceiling_recovery_amt`),0) AS `ceiling_recoveryamt_dec`,
		(((((((ifnull(sum(`a`.`ceiling_amount`),0) + ifnull(sum(`ah`.`ceiling_modify_amt`),0)) + ifnull(sum(`ah`.`ceiling_transfer_in_amt`),0)) + ifnull(sum(`ah`.`ceiling_unreserve_amt`),0))
			+ ifnull(sum(`ah`.`ceiling_recovery_amt`),0)) - ifnull(sum(`ah`.`ceiling_reserved_amt`),0)) - ifnull(sum(`ah`.`ceiling_obligated_amt`),0)) - ifnull(sum(`ah`.`ceiling_transfer_out_amt`),0)) AS `ceiling_availableamt_dec`
		from `trams_temp_apportionment_fyfap_rollup` `a` 
			left join `trams_temp_appor_history_fyfap_rollup` ah 
				on `a`.`uza_code` = `ah`.`uza_code` and 
				`a`.`funding_fiscal_year` = `ah`.`funding_fiscal_year` and 
				`a`.`section_id` = `ah`.`section_id` and 
				`a`.`appropriation_code` = `ah`.`appropriation_code` and 
				`a`.`limitation_code` = `ah`.`limitation_code`
			join 
			(select
				a.uza_code as uza_code,
				tucr.uza_name as uza_name,
				tucr.cost_center_code as cost_center_code,
				max(tucr.census_year) as census_year
			from
				trams_apportionment a
			join
				trams_uza_code_reference tucr
			on
				a.uza_code = tucr.uza_code
			group by
				a.uza_code
		/*UNION
			select
				a.uza_code as uza_code,
				'' as uza_name,
				'' as cost_center_code,
				'' as census_year
			from
				trams_apportionment a
			where
				a.uza_code = 990000*/
				) uza_reference
			on
				a.uza_code = uza_reference.uza_code
			left join trams_cost_center tcc on uza_reference.cost_center_code = tcc.cost_center_code
			left join 
			(
				select
					tupbs.uza_code,
					tupbs.population as population,
					max(tupbs.fiscal_year) as fiscal_year
				from
					trams_uza_population_by_state tupbs
				where tupbs.version = 8
				group by
					tupbs.uza_code
					) uza_population
			on 
				a.uza_code = uza_population.uza_code
			left join
			(
				select
					tsp.state_id as state_id,
					ts.uza_code as state_uza_code,
					tsp.population as state_population,
					max(tsp.fiscal_year)
				from
					trams_state_population tsp
				join
					trams_state ts
				on
					ts.id = tsp.state_id
				where
					tsp.version = 8
                group by
                	ts.uza_code) state_population
				on
					a.uza_code = state_population.state_uza_code
		where 1 ",_whereClause,"		
		group by `a`.`funding_fiscal_year`,`a`.`section_id`,`a`.`limitation_code`,`a`.`appropriation_code`,`a`.`uza_code`,`a`.`lapse_period`
		order by `ah`.`funding_fiscal_year`");

	
	set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_cumulative_apportionments', concat('Apportionment Query: ',@apportionmentQuery), 1); 
	CALL `trams_log`(@sid, 'trams_get_cumulative_apportionments', concat('Apportionment Where Query: ',_whereClause), 1); 


	PREPARE apportionmentStmt FROM @apportionmentQuery;
	EXECUTE apportionmentStmt;

	drop table if exists trams_temp_apportionment_fyfap_rollup;
	drop table if exists trams_temp_appor_history_fyfap_rollup;
	
	CALL `trams_log`(@sid, 'trams_get_cumulative_apportionments', 'Done!', 1); 	

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_cna_completion_status`(IN `p_fiscal_year` VARCHAR(4), IN `p_recipient_statuses` VARCHAR(255))
    NO SQL
    DETERMINISTIC
BEGIN

/*
     Stored Procedure Name : trams_get_cna_completion_status
     Updated by  : Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
	*/

    SET SESSION group_concat_max_len = 1000000;
    SET @select_categories = (
      SELECT 
        GROUP_CONCAT(
          CONCAT(
            'IF((original_cert.certified_date is null OR original_cert.certified_date = ''''), ''N/A'',',                                            
            'IF((SELECT applicable_flag FROM `trams_grantee_cert_assurance_line_item` item WHERE item.grantee_cert_assurance_id = info_sub.cert_id AND category=''', cats.category, '''), ''Yes'', ''No'')) AS ''Category ', cats.category, '''' 
          )
        )
      FROM (
        SELECT item.category
        FROM `trams_cert_assurance_lineitem` item 
        LEFT JOIN `trams_certification_assurance` cert ON item.certification_assurance_id = cert.id AND cert.fiscal_year = p_fiscal_year
        WHERE cert.id IS NOT NULL
        ORDER BY item.id
      ) cats
    );
  
    SET @sql = (
      CONCAT(
        'SELECT 
          info_sub.recipient_id AS ''Recipient ID'',
          info_sub.trams_status AS ''Status'',
          info_sub.legal_business_name AS ''Recipient Legal Business Name'',
          info_sub.recipient_acronym AS ''Recipient Acronym'',
          info_sub.organization_alias AS ''Organization Alias'',
          info_sub.cost_center_code AS ''Cost Center'',
          info_sub.official_name AS ''Official Name'',
          info_sub.official_cert_date AS ''Official Certification Date'',
          info_sub.attorney_name AS ''Attorney Name'',
          info_sub.attorney_cert_date AS ''Attorney Certification Date'',
          original_cert.certified_date AS ''Original Certified Date'',
          info_sub.certified_date AS ''Latest Certified Date'',
          IF(original_cert.certified_date IS NULL, "No", "Yes") AS ''Certified Overall'',',
          @select_categories,
          'FROM (
            SELECT
              info.id,
              info.recipientId AS recipient_id,
              info.tramsStatus AS ''trams_status'',
              info.legalBusinessName AS ''legal_business_name'',
              info.recipientAcronym AS ''recipient_acronym'',
              info.organizationAlias AS ''organization_alias'',
              info.costCenterCode AS ''cost_center_code'',
              active.fiscal_year AS ''fiscal_year'',
              cert.id AS ''cert_id'',
              cert.certified_date AS ''certified_date'',
              cert.official_name AS ''official_name'',
              cert.official_cert_date AS ''official_cert_date'',
              cert.attorney_name AS ''attorney_name'',
              cert.attorney_cert_date AS ''attorney_cert_date''
            FROM `trams_certification_assurance` active
            LEFT JOIN `vwtramsGeneralBusinessInfo` info ON 1
            LEFT JOIN `trams_grantee_cert_assurance` cert 
              ON cert.recipient_id = info.recipientId
              AND cert.fiscal_year = active.fiscal_year
          ) info_sub
          LEFT JOIN `trams_grantee_cert_assurance` original_cert 
            ON original_cert.recipient_id = info_sub.recipient_id 
              AND original_cert.version = 0 
              AND info_sub.fiscal_year = original_cert.fiscal_year
          LEFT JOIN `trams_grantee_cert_assurance` latest_cert 
            ON latest_cert.recipient_id = info_sub.recipient_id
              AND latest_cert.fiscal_year = info_sub.fiscal_year
              AND latest_cert.certified_date > info_sub.certified_date
          WHERE latest_cert.certified_date IS NULL AND info_sub.fiscal_year = ''', p_fiscal_year, ''' AND info_sub.trams_status IN (', p_recipient_statuses, ')
          ORDER BY info_sub.recipient_id'
      )
    );

    PREPARE stmt FROM @sql;
    EXECUTE stmt;
  END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_deobligation_project_scope_funding_roll_up`(IN `p_contract_award_id` INT, IN `p_application_id` INT)
    NO SQL
BEGIN

	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _joinClause VARCHAR(1000) DEFAULT " ON scp.po_number = dd.po_number ";
	DECLARE _selectClause VARCHAR(1000) DEFAULT " ";
	DECLARE _groupClause VARCHAR(1000) DEFAULT " GROUP BY scp.po_number ";
	DECLARE _groupClauseDD VARCHAR(1000) DEFAULT " GROUP BY dd.po_number ";
	DECLARE _whereClause VARCHAR(1000) DEFAULT " ";
	DECLARE _onClause varchar(4096) default " ";
	DECLARE _appCreation date;
	
	
	IF ((SELECT app_from_team_flag FROM `trams_application` WHERE id = p_application_id LIMIT 1) <> 1) THEN
		SET _joinClause = CONCAT(_joinClause,
			" AND proj.project_number_sequence_number = dd.project_number_sequence_number 
			  AND scp.scope_code = dd.scope_number 
			  AND scp.scope_suffix = dd.scope_suffix " 
	    );
	
		SET _selectClause = CONCAT(_selectClause,
			" scp.scope_code AS scope_code, 
			  scp.scope_suffix AS scope_suffix,
			  scp.scope_name AS scope_name, 
			  scp.scope_description AS scope_description, "
	    );
			 
		SET _groupClauseDD = CONCAT(_groupClauseDD,
			" , dd.project_number_sequence_number, dd.scope_number, dd.scope_suffix "
		);
		
		SET _groupClause = CONCAT(_groupClause,
			" , proj.project_number_sequence_number, scp.scope_code, scp.scope_suffix "
		);

		SET _whereClause = CONCAT(_whereClause,
			"  AND
			scp.latest_amend_fta_amount - scp.revised_fta_amount > 0  "
		);
	END IF;

	/*Previous _appCreation was initialized to (SELECT DATE(created_date_time) FROM trams_application WHERE contractAward_id = p_contractAwardId AND amendment_number = 0).
  Replaced the previous initialization with a function to retrieve the date directly.*/
	SET _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(p_contract_award_id,NULL);	
	/*Previous on _onClause was initialized to: if(_appCreation<p_fundingDeploy,'`fs`.`version` = 0','`fs`.`latest_flag` = 1')
  Replaced the previous initialization to retrieve the funding source record at the time the funding source latest flag was set to true for the base application.   */
	SET _onClause = CONCAT(' fsr.LatestFlagStartDate <= ''',_appCreation,''' AND fsr.LatestFlagEndDate >= ''',_appCreation,'''');
	
		
	SET _sql = CONCAT(
		"SELECT 
			scp.id, 
			scp.po_number AS po_number,
			proj.project_number AS project_number, ",_selectClause, " 
		    ifnull(sum(scp.latest_amend_fta_amount), 0) - ifnull(sum(scp.revised_fta_amount), 0) AS required_deobligation_amount,
			ifnull(sum(scp.latest_amend_fta_amount), 0) - ifnull(sum(scp.revised_fta_amount), 0) - ifnull(dd.deobligated_recovery_amount, 0) AS deobligation_difference,
			ifnull(fsr.funding_source_description, scp.funding_source_name) AS funding_source_description,
			ifnull(dd.deobligated_recovery_amount, 0) AS current_deobligation_amount
		FROM 
			`trams_scope` scp 
			JOIN `trams_project` proj
				ON scp.project_id = proj.id
			LEFT JOIN `trams_funding_source_reference` fsr
				ON scp.funding_source_name = fsr.funding_source_name AND ",_onClause,"
			LEFT JOIN (
				/* retreives the sum of the temporary deobligations that are created throughout the process */
				SELECT 
					dd.po_number, 
					dd.project_number_sequence_number, 
					dd.scope_number,
					dd.scope_suffix,
					ifnull(sum(dd.deobligated_recovery_amount), 0) AS deobligated_recovery_amount
				FROM 
					`trams_deobligation_detail` dd
				WHERE
					dd.deobligated_still_pending <> 0 
					AND dd.contract_award_id = ", p_contract_award_id,
				_groupClauseDD,
			") dd 
			",_joinClause,"
		WHERE 
			scp.application_id = ",p_application_id,
			" AND scp.other_budget = 0",
			_whereClause, 
		_groupClause, 
		" ORDER BY proj.project_number, scp.scope_code, scp.scope_suffix ASC;"
	);
	
	SET @deobQuery = _sql;
	
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_deobligation_project_scope_funding_roll_up', concat('Deobligation Roll Up Query: ', @deobQuery), 1);
	
	PREPARE deobStmt FROM @deobQuery;
	EXECUTE deobStmt;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_grantee_details_excel_report`(IN `p_granteeId` VARCHAR(50), IN `p_costCenterCode` VARCHAR(1024), IN `p_trams_status` VARCHAR(50), IN `p_sam_status` VARCHAR(20))
BEGIN

/*
     Stored Procedure Name : trams_get_grantee_details_excel_report
     Update by   : Harsh Mistry (09/23/2021)
  */

 declare _sql varchar(40960) default "";
 declare _whereClause varchar(4096) default " ";
 declare _limitClause varchar(100) default " ";


 call trams_get_sql_limit_clause(-1,_limitClause);

 set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

 set _whereClause = concat(_whereClause, (select ifnull(concat(" and gb.recipientId in (",NULLIF(p_granteeId,''),")"),"")));
 set _whereClause = concat(_whereClause, (select ifnull(concat(" and gb.costCenterCode in (",NULLIF(p_costCenterCode,''),")"),"")));
 set _whereClause = concat(_whereClause, (select ifnull(concat(" and gb.tramsStatus in (",NULLIF(p_trams_status,''),")"),"")));
 set _whereClause = concat(_whereClause, (select ifnull(concat(" and gb.samStatus in (",NULLIF(p_sam_status,''),")"),"")));

 set _sql = "SELECT 
 gb.costCenterCode AS cost_center_code,
 gb.recipientId AS recipient_id,
 gb.legalBusinessName AS legal_business_name,
 gb.recipientAcronym AS recipient_acronym, 
 l.addressLine1 AS address_line_1,
 l.addressLine2 AS address_line_2,
 l.city AS city,
 l.stateOrProvince AS state_or_province,
 l.zipCode AS zip_code,
 gb.organizationType AS organization_type_code,
 gb.DaimsBusinessTypeCode AS daims_business_type_code,
 gb.DaimsBusinessTypeLabel AS daims_business_type_label,
 gb.ueid AS ueid,
 gb.duns AS duns,
 gb.ntdCode AS ntd_code,
 gb.tramsStatus AS trams_status,
 gb.samStatus AS sam_status,
trams_format_date(gb.samExpirationDate) as sam_expiration_date,
trams_format_date(gb.samSyncDateTime) as sam_sync_date_time,
 gb.echoNumber AS echo_number,
 gb.requFlag AS requ_flag,
 gb.opacFlag AS opac_flag,
 gb.wcfFlag AS wcf_flag,
 gb.tscFlag AS tsc_flag
 FROM vwtramsGeneralBusinessInfo AS gb join vwtramsLocation AS l 
 on gb.id=l.recipientId
 where l.addressType='Physical Address' ";


 SET @Query = concat(_sql,_whereClause,_limitClause, " order by gb.recipientId");

 CALL `trams_log`(@sid, 'trams_get_user_details_excel_report', concat('Get User Detail excel Query: ',@Query), 1); 

 PREPARE granteeDetailsReportStmt FROM @Query;
 EXECUTE granteeDetailsReportStmt;
 CALL `trams_log`(@sid, 'trams_get_grantee_details_excel_report', 'Done!', 1); 

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_discretionary_details_excel_report`(
	IN p_discretionaryId VARCHAR(255),
	IN p_fiscalyear VARCHAR(255),
	IN p_program_name VARCHAR(1024),
	IN p_state VARCHAR(255),
	IN p_status VARCHAR(512), 
    IN p_gmtOffset VARCHAR(10), 
    IN p_return_limit VARCHAR(20), 
    IN p_apportionmentfiscalyear VARCHAR(255)
)
BEGIN

	/*
     Stored Procedure Name : trams_get_discretionary_details_excel_report
     Description : To populate data for TrAMS 'General Discretionary and Earmark Allocation' excel report.
     Updated by  : Seung Ki Lee (04.26.2021)
    */

	 DECLARE _sql varchar(40960) DEFAULT "";
	 DECLARE _whereClause varchar(4096) DEFAULT " ";
	 DECLARE _limitClause varchar(100) DEFAULT " ";


	 CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

	 SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.discretionary_id in ('",NULLIF(p_discretionaryId,''),"')"),"")));
	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.discretionary_fiscal_year in (",NULLIF(p_fiscalyear,''),")"),"")));
	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.original_fiscal_year in ('",NULLIF(p_apportionmentfiscalyear,''),"')"),"")));
	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.program_name in (",NULLIF(p_program_name,''),")"),"")));
	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.state like ('%",NULLIF(p_state,''),"%')"),"")));
	 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and da.status in (",NULLIF(p_status,''),")"),"")));

	 SET _sql = CONCAT("
		SELECT 
		 da.discretionary_id,
		 da.discretionary_fiscal_year,
		 da.program_name,
		 da.state,
		 da.discretionary_project_name,
		 da.public_allocation_amount,
		 da.unobligated_balance_amount,		 
		 trams_format_date(cast(CONVERT_TZ(da.unobligated_balance_date_time, '+00:00',",p_gmtOffset,") AS char)),
		 da.cost_center_code,
		 da.original_fiscal_year, 
		 da.lapse_year,
		 da.fta_program_manager,
		 da.status,
		 da.recipient_id, 
		 da.notes,
		 da.regional_comment,
		 trams_format_date(cast(CONVERT_TZ(da.last_modified_date_time, '+00:00',",p_gmtOffset,") AS char)),
		 da.last_modified_by 
		 FROM trams_discretionary_allocations as da
		 where 1"
	);

	 SET @Query = CONCAT(_sql,_whereClause,_limitClause);

	 /*CALL `trams_log`(@sid, 'trams_get_discretionary_details_excel_report', CONCAT('Get Discretionary and Earmark excel Query: ',@Query), 1); */
     
CALL `trams_log`(@sid, 'trams_get_discretionary_details_excel_report', CONCAT('Final SELECT: ',_sql), 1); 
     CALL `trams_log`(@sid, 'trams_get_discretionary_details_excel_report', CONCAT('Final WHERE: ',_whereClause), 1);
     CALL `trams_log`(@sid, 'trams_get_discretionary_details_excel_report', CONCAT('Final LIMIT: ',_limitClause), 1);
	 
	 PREPARE discretionaryEarmarkReport FROM @Query;
	 EXECUTE discretionaryEarmarkReport;
	 CALL `trams_log`(@sid, 'trams_get_discretionary_details_excel_report', 'Done!', 1); 


END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_deobligation_report_excel`(IN p_recipientId VARCHAR(50), IN p_recipientCostCenter VARCHAR(50), IN p_fainFiscalYear VARCHAR(50),
IN p_applicationNumber VARCHAR(50), IN p_status VARCHAR(1024), IN p_projectNumber VARCHAR(50), IN p_scopeCode VARCHAR(50), IN p_costCenterCode VARCHAR(1024),
IN p_uzaCode VARCHAR(50), IN p_transactionFiscalYear VARCHAR(50), IN p_fundingFiscalYear VARCHAR(50), IN p_appropriationCode VARCHAR(50), IN p_sectionCode VARCHAR(50),
IN p_limitationCode VARCHAR(50), IN p_typeAuthority VARCHAR(50), IN p_gmtOffset VARCHAR(10))
BEGIN
	
	/*
     Stored Procedure Name : trams_get_deobligation_report_excel
     Description : Populate data for TrAMS Deobligation by Funding Sources excel report.
     Updated by  : Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
	*/
	
	DECLARE _whereClause VARCHAR(4096) default "";
	
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and app.recipient_id in (", NULLIF(p_recipientId, ''),")"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and app.recipient_cost_center in (",NULLIF(p_recipientCostCenter, ''),")"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and app.fain_fiscal_year = '", NULLIF(p_fainFiscalYear, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and app.application_number = '", NULLIF(p_applicationNumber, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and app.status in (", NULLIF(p_status, ''),")"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and tp.project_number = '", NULLIF(p_projectNumber, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.scope_number = '", NULLIF(p_scopeCode, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_cost_center_code in (", NULLIF(p_costCenterCode, ''),")"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_uza_code = '", NULLIF(p_uzaCode, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.deobligated_fiscal_year = '", NULLIF(p_transactionFiscalYear, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_funding_fiscal_year = '", NULLIF(p_fundingFiscalYear, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_appropriation_code = '", NULLIF(p_appropriationCode, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_section_code = '", NULLIF(p_sectionCode, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_limitation_code = '", NULLIF(p_limitationCode, ''),"'"),"")
	));
	SET _whereClause = CONCAT(_whereClause, (
		SELECT IFNULL(CONCAT(" and dd.acc_authorization_type = '", NULLIF(p_typeAuthority, ''),"'"),"")
	));			
	
	SET @selectContractAccTransactionQuery = CONCAT("
		SELECT
			app.recipient_id, 
			gbi.recipientAcronym AS recipient_acronym,
			app.recipient_name,
			app.recipient_cost_center,
			aad.obligation_fiscal_year,
			app.application_number, 
			app.amendment_number,
			app.status,
			tp.project_number,
			dd.po_number, 
			dd.scope_number,
			ts.scope_name,
			dd.scope_suffix, 
			dd.acc_cost_center_code, 
			dd.acc_uza_code, 
			dd.account_class_code, 
			dd.acc_fpc,
			CASE
				WHEN dd.FinalizedDate IS NULL OR dd.FinalizedDate = ' '
				THEN trams_format_date(cast(CONVERT_TZ(dd.deobligated_recovery_date_time,'+00:00',",p_gmtOffset,") AS char))
				ELSE trams_format_date(cast(CONVERT_TZ(dd.FinalizedDate,'+00:00',",p_gmtOffset,") AS char))
			END AS 'Deobligation Date',
			dd.deobligated_recovery_by, 
			dd.deobligated_fiscal_year,  
			dd.deobligated_recovery_amount, 
			dd.acc_funding_fiscal_year, 
			dd.acc_appropriation_code, 
			dd.acc_section_code, 
			dd.acc_limitation_code, 
			dd.acc_authorization_type, 
			dd.obligation_fiscal_year,
			app.discretionary_funds_flag,
			ref.ref_value
		FROM
			trams_deobligation_detail dd
			JOIN `trams_application` app
				ON dd.amendment_id = app.id
			JOIN trams_project tp
				ON dd.project_id = tp.id
			JOIN `vwtramsGeneralBusinessInfo` gbi
				ON app.recipient_id = gbi.id
			JOIN `trams_application_award_detail` aad
				ON aad.application_id = dd.amendment_id
			LEFT JOIN trams_scope ts
				on dd.amendment_id = ts.application_id AND dd.scope_number = ts.scope_code AND dd.scope_suffix = ts.scope_suffix
			LEFT JOIN trams_references ref
				ON ref.id = dd.transaction_origin_ref_id
			WHERE dd.deobligated_recovery_type in ('Discretionary','Non-Lapsed Deob','Lapsed Year Deob')
			AND dd.deobligated_still_pending = 0
		", _whereClause, "
		ORDER BY app.recipient_id
	");
	
	PREPARE deobligationReportStmt FROM @selectContractAccTransactionQuery;
	EXECUTE deobligationReportStmt;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_grant_accrual_report_update`(IN p_end_date VARCHAR(20), IN p_gmtOffset VARCHAR(10))
BEGIN

	/*
     Stored Procedure Name : trams_get_grant_accrual_report_update
     Description : Populate data for TrAMS Grant Accrual excel report.
     Updated by  : Fuadul Islam (08/28/2020) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
	*/

    -- uuid will serve as the unique identifier for rows generated by this specific procedure call. Multiple procedures run at the same time would generate unique uuids
    SET @uuid = UUID_SHORT();
    -- sid will be written to the new table as a time stamp to be used for identifying and cleaning up any junk data
    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f')); 

	INSERT INTO trams_temp_contract_acc_by_ca_all 
	SELECT NULL AS tramsTempContractAccByCaAll_id,
	contract_award_id AS contract_award_id,  
	CAST(obligation_amount AS DECIMAL(19,2)) AS obligation_amount,
	CAST(deobligation_amount AS DECIMAL(19,2)) AS deobligation_amount,  
	CAST(refund_amount AS DECIMAL(19,2)) AS refund_amount,  
	CAST(disbursement_amount AS DECIMAL(19,2)) AS disbursement_amount,
    @uuid AS uuid,
	@sid AS time_stamp	
	FROM `trams_contract_acc_transaction` 
	WHERE DATE(transaction_date_time) <= p_end_date;
  
 -- This SELECT statement was previously used to generate/populate the temp table. Now the results of the select are inserted INTO the new permanent table
	
	INSERT INTO trams_temp_contract_acc_by_ca_all 
	SELECT NULL AS tramsTempContractAccByCaAll_id,
	contract_award_id AS contract_award_id,  
	CAST(obligation_amount AS DECIMAL(19,2)) AS obligation_amount,
	CAST(deobligation_amount AS DECIMAL(19,2)) AS deobligation_amount,  
	CAST(refund_amount AS DECIMAL(19,2)) AS refund_amount,  
	CAST(disbursement_amount AS DECIMAL(19,2)) AS disbursement_amount,
	@uuid AS uuid,
	@sid AS time_stamp
	FROM `trams_contract_acc_transaction` 
	WHERE transaction_date_time IS NULL;
	
 -- This SELECT statement was previously used to generate/populate the temp table. Now the results of the select are inserted into the new permanent table	

	INSERT INTO trams_temp_contract_acc_by_ca 
	SELECT NULL AS tramsTempContractAccByCa_id,
	ca.contract_award_id AS contract_award_id,  
	SUM(ca.obligation_amount) - SUM(ca.deobligation_amount) AS net_obligations,  
	SUM(ca.refund_amount) AS total_refund_amount,  
	SUM(ca.disbursement_amount) AS total_disbursement_amount, 
	SUM(ca.obligation_amount) - SUM(ca.deobligation_amount) - SUM(ca.disbursement_amount) + SUM(ca.refund_amount) AS total_undisbursed_amount,
    @uuid AS uuid,
	@sid AS time_stamp	
	FROM `trams_temp_contract_acc_by_ca_all` AS ca
	WHERE uuid = @uuid
	GROUP BY ca.contract_award_id;	

	-- This SELECT statement returns the results of the Grant Accrual Information 
	
    SELECT c.cost_center_code AS cost_center,
	c.recipient_id AS recipient_id,
	(SELECT 	CASE  WHEN g.legalBusinessName = NULL  THEN g.organizationAlias
		WHEN g.legalBusinessName = ''  THEN g.organizationAlias
		ELSE g.legalBusinessName
		END 
	FROM vwtramsGeneralBusinessInfo g WHERE g.recipientId = c.recipient_id) AS recipient_name,
	SUBSTRING_INDEX(c.application_number,'-',3) AS application_number,	
	trams_format_date(cast(CONVERT_TZ(c.fta_closeout_approval_DATE,'+00:00',p_gmtOffset) AS char)) AS fta_closeout_approval_DATE,
	trams_format_date(cast(CONVERT_TZ(c.last_obligation_DATE,'+00:00',p_gmtOffset) AS char)) AS last_obligation_date,
	trams_format_date(cast(CONVERT_TZ(c.last_deobligation_DATE,'+00:00',p_gmtOffset) AS char)) AS last_deobligation_date,
	trams_format_date(cast(CONVERT_TZ(c.last_disbursement_DATE,'+00:00',p_gmtOffset) AS char)) AS last_disbursement_date,
	cat.net_obligations,
	cat.total_refund_amount,
	cat.total_disbursement_amount,
	cat.total_undisbursed_amount,
	c.application_type,	
	CASE WHEN c.active=1 THEN 'Yes' ELSE 'No'
	     END AS active,
    CASE WHEN app.preaward_authority_flag = 0 OR NULL THEN 'No' ELSE 'Yes'
         END AS preaward_authority_flag		
	FROM trams_contract_award c
	LEFT JOIN trams_temp_contract_acc_by_ca cat
	     ON c.id = cat.contract_award_id
         AND uuid = @uuid
	LEFT JOIN trams_application app
         ON c.id = app.contractAward_id
		 AND app.amendment_number = 0
	WHERE c.active = 1 AND DATE(c.first_obligation_date) <= p_END_DATE 
	  OR ( c.active = 0 AND DATE(c.fta_closeout_approval_date) > p_END_DATE );

  -- The rows generated for the procedure are deleted to replicate the effects of a temporary table
 DELETE FROM trams_temp_contract_acc_by_ca_all WHERE uuid = @uuid;
 DELETE FROM trams_temp_contract_acc_by_ca WHERE uuid = @uuid;


END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_dbe_excel_report`(IN p_fiscalYears VARCHAR(255), IN p_costCenterCodes VARCHAR(255), IN p_reportPeriod VARCHAR(255), 
IN p_recipientIds VARCHAR(255), IN p_reportQuarter VARCHAR(255), IN p_status VARCHAR(255), IN p_isLatest TINYINT(1), IN p_recipientActive INT(1))
BEGIN

	/*
     Stored Procedure Name : trams_get_dbe_excel_report
     Description : Populate data for TrAMS DBE Appian Report
     Updated by  : Dawit Asmelash (2021.Nov.2)
	*/

	DECLARE _sql VARCHAR(16384) DEFAULT " ";
	DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
	
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_fiscalYears), " ", CONCAT(" AND dbe.fiscal_year IN (", p_fiscalYears , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_costCenterCodes), " ", CONCAT(" AND info.costCenterCode IN (", p_costCenterCodes , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportPeriod), " ", CONCAT(" AND dbe.`report_period` IN (" , p_reportPeriod , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_recipientIds), " ", CONCAT(" AND dbe.`recipient_id` IN (" , p_recipientIds , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportQuarter), " ", CONCAT(" AND dbe.`report_quarter` IN (" , p_reportQuarter , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_status), " ", CONCAT(" AND dbe.`status` IN (" , p_status , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_isLatest), " ", CONCAT(" AND dbe.`isLatest` = (" , p_isLatest , ")")));
	SET _whereClause = CONCAT(_whereClause, (CASE 
		WHEN p_recipientActive IS NULL THEN ""
		WHEN p_recipientActive = 1 THEN " and app.id IS NOT NULL"
		WHEN p_recipientActive = 0 THEN " and app.id IS NULL"
		ELSE ""
		END));
	
	SET _sql = 
	"SELECT
	    /*Casting the cost_center_code column to UNSIGNED integer to display as number  */
		CAST(info.costCenterCode AS UNSIGNED) AS cost_center_code, /* Column A*/  
		info.legalBusinessName AS legal_business_name,
		dbe.recipient_id, /* Column C*/ 
		info.recipientAcronym AS recipient_acronym,
		tl.stateOrProvince AS state_or_province, /* Column E */ 
		 /*Casting the fiscal_year column to UNSIGNED integer to display as number  */
		CAST(dbe.fiscal_year AS UNSIGNED), /* Column F*/ 
		dbe.report_period, /*Column G */
		dbe.report_due_date_time, /*Column H */
		dbe.status, /*Column I */
		dbe.Version, /*Column J */
		dbe.submitted_date_time, /* Column K */
		CAST((comp.dbe_race_conscious_goal_percentage/100) AS DECIMAL(7,4)),
		CAST((comp.dbe_race_neutral_goal_percentage/100) AS DECIMAL(7,4)),
		CAST(((comp.dbe_race_conscious_goal_percentage + comp.dbe_race_neutral_goal_percentage)/100) AS DECIMAL(7,4)), 
		/* The IFNULL function included to display specified values for all the below columns if the expresssion is null from the trams_general_business_info and trams_dbe_report tables. */ 
		IFNULL(dbe.prime_total_dollars,0),
		IFNULL(dbe.prime_total_number,0),
		IFNULL(dbe.prime_race_conscious_dollars + dbe.prime_race_neutral_dollars,0), /* Column O */
		IFNULL(dbe.prime_race_conscious_number + dbe.prime_race_neutral_number,0), /* Column P */
		CASE WHEN dbe.prime_race_conscious_dollars = 0 THEN NULL ELSE dbe.prime_race_conscious_dollars END, /* Column Q */
		CASE WHEN dbe.prime_race_conscious_number = 0 THEN NULL ELSE dbe.prime_race_conscious_number END,  /* Column R */
		IFNULL(dbe.prime_race_neutral_dollars,0),
		IFNULL(dbe.prime_race_neutral_number,0),
		CAST(IFNULL(ROUND(((dbe.prime_race_conscious_dollars + dbe.prime_race_neutral_dollars) / dbe.prime_total_dollars  * 100),2),0)/100.00 AS DECIMAL(7,4)), /* Column U */
		IFNULL(dbe.sub_total_dollars,0),
		IFNULL(dbe.sub_total_number,0),
		IFNULL(dbe.sub_race_conscious_dollars + dbe.sub_race_neutral_dollars,0), /* Column X */
		IFNULL(dbe.sub_race_conscious_number + dbe.sub_race_neutral_number,0), /* Column Y*/
		IFNULL(dbe.sub_race_conscious_dollars,0), /* Column Z*/
		IFNULL(dbe.sub_race_conscious_number,0),
		IFNULL(dbe.sub_race_neutral_dollars,0),
		IFNULL(dbe.sub_race_neutral_number,0),
		CAST(ROUND(IFNULL(((dbe.sub_race_conscious_dollars + dbe.sub_race_neutral_dollars) / dbe.sub_total_dollars  * 100),0),2)/100.00 AS DECIMAL(7,4)),/* Column AD */
		IFNULL(dbe.prime_race_conscious_dollars + dbe.prime_race_neutral_dollars + dbe.sub_race_conscious_dollars + dbe.sub_race_neutral_dollars,0), /* Column AE */
		IFNULL(dbe.prime_race_conscious_number + dbe.prime_race_neutral_number + dbe.sub_race_conscious_number + dbe.sub_race_neutral_number,0),/* Column AF */
		IFNULL(dbe.prime_race_conscious_dollars + dbe.sub_race_conscious_dollars,0),  /* Column AG*/
		IFNULL(dbe.prime_race_conscious_number + dbe.sub_race_conscious_number,0),  /* Column AH */
		IFNULL(dbe.prime_race_neutral_dollars + dbe.sub_race_neutral_dollars,0),  /* Column AI */
		IFNULL(dbe.prime_race_neutral_number + dbe.sub_race_neutral_number,0), /* Column AJ */
		CAST(IFNULL(ROUND(((dbe.prime_race_conscious_dollars + dbe.prime_race_neutral_dollars + dbe.sub_race_conscious_dollars + dbe.sub_race_neutral_dollars) / dbe.prime_total_dollars * 100.0),2),0) / 100 AS DECIMAL(7,4)), /* Column AK */
		IFNULL(dbe.women_dollar_blackamer,0),
		IFNULL(dbe.men_dollar_blackamer,0),
		IFNULL(dbe.total_dollar_blackamer,0),
		IFNULL(dbe.women_number_blackamer,0),
		IFNULL(dbe.men_number_blackamer,0),
		IFNULL(dbe.total_number_blackamer,0),/* Column AQ */
		IFNULL(dbe.women_dollar_hispanicamer,0), 
		IFNULL(dbe.men_dollar_hispanicamer,0),
		IFNULL(dbe.total_dollar_hispanicamer,0),
		IFNULL(dbe.women_number_hispanicamer,0),
		IFNULL(dbe.men_number_hispanicamer,0),
		IFNULL(dbe.total_number_hispanicamer,0),
		IFNULL(dbe.women_dollar_nativeamer,0),
		IFNULL(dbe.men_dollar_nativeamer,0),
		IFNULL(dbe.total_dollar_nativeamer,0),
		IFNULL(dbe.women_number_nativeamer,0),
		IFNULL(dbe.men_number_nativeamer,0), /* Column BB */
		IFNULL(dbe.total_number_nativeamer,0),/* Column BC */
		IFNULL(dbe.women_dollar_asianpacific,0),
		IFNULL(dbe.men_dollar_asianpacific,0),
		IFNULL(dbe.total_dollar_asianpacific,0),
		IFNULL(dbe.women_number_asianpacific,0),
		IFNULL(dbe.men_number_asianpacific,0),
		IFNULL(dbe.total_number_asianpacific,0),
		IFNULL(dbe.women_dollar_subcontasian,0),
		IFNULL(dbe.men_dollar_subcontasian,0),
		IFNULL(dbe.total_dollar_subcontasian,0),
		IFNULL(dbe.women_number_subcontasian,0),
		IFNULL(dbe.men_number_subcontasian,0),
		IFNULL(dbe.total_number_subcontasian,0),
		IFNULL(dbe.women_dollar_nonminority,0),
		IFNULL(dbe.men_dollar_nonminority,0),
		IFNULL(dbe.total_dollar_nonminority,0),
		IFNULL(dbe.women_number_nonminority,0),
		IFNULL(dbe.men_number_nonminority,0),
		IFNULL(dbe.total_number_nonminority,0),
		IFNULL(dbe.women_dollar_blackamer + dbe.women_dollar_hispanicamer + dbe.women_dollar_nativeamer + dbe.women_dollar_asianpacific + dbe.women_dollar_subcontasian + dbe.women_dollar_nonminority,0),
		IFNULL(dbe.men_dollar_blackamer + dbe.men_dollar_hispanicamer + dbe.men_dollar_nativeamer + dbe.men_dollar_asianpacific + dbe.men_dollar_subcontasian + dbe.men_dollar_nonminority,0),
		IFNULL(dbe.total_dollar_blackamer + dbe.total_dollar_hispanicamer + dbe.total_dollar_nativeamer + dbe.total_dollar_asianpacific + dbe.total_dollar_subcontasian + dbe.total_dollar_nonminority,0),
		IFNULL(dbe.women_number_blackamer + dbe.women_number_hispanicamer + dbe.women_number_nativeamer + dbe.women_number_asianpacific + dbe.women_number_subcontasian + dbe.women_number_nonminority,0),
		IFNULL(dbe.men_number_blackamer + dbe.men_number_hispanicamer + dbe.men_number_nativeamer + dbe.men_number_asianpacific + dbe.men_number_subcontasian + dbe.men_number_nonminority,0),
		IFNULL(dbe.total_number_blackamer + dbe.total_number_hispanicamer + dbe.total_number_nativeamer + dbe.total_number_asianpacific + dbe.total_number_subcontasian + dbe.total_number_nonminority,0),
		IFNULL(dbe.total_number_conts_prog,0), /* Column CB*/
		IFNULL(dbe.total_dollars_prog,0), /* Column CC*/                 
		IFNULL(dbe.total_number_dbe_conts_prog,0), /* Column CD*/
		IFNULL(dbe.total_payments_to_dbe_firms_prog,0), /* Column CE*/
		IFNULL(dbe.total_number_dbe_firms_prog,0), /* Column CF*/
		CAST(IFNULL(ROUND(((dbe.total_payments_to_dbe_firms_prog / dbe.total_dollars_prog) * 100.0),2),0)/100 AS DECIMAL(7,4)), /* Column CG*/
		IFNULL(dbe.race_conscious_prime_comp_number,0),
		IFNULL(dbe.race_conscious_prime_comp_dollar,0),
		IFNULL(dbe.race_conscious_part_needed,0),
		IFNULL(dbe.race_conscious_dbe_part,0),
		CAST(IFNULL(ROUND(((dbe.race_conscious_dbe_part / dbe.race_conscious_prime_comp_dollar) * 100.0 ),2),0)/100 AS DECIMAL(7,4)), /* Column CL*/
		IFNULL(dbe.race_neutral_prime_comp_number,0),
		IFNULL(dbe.race_neutral_prime_comp_dollar,0),
		IFNULL(dbe.race_neutral_dbe_part,0),
		CAST(IFNULL(ROUND(((dbe.race_neutral_dbe_part / dbe.race_neutral_prime_comp_dollar) * 100.0 ),2),0)/100 AS DECIMAL(7,4)),/* Column CP*/
		IFNULL(dbe.race_conscious_prime_comp_number + dbe.race_neutral_prime_comp_number,0),/* Column CQ*/
		IFNULL(dbe.race_conscious_prime_comp_dollar + dbe.race_neutral_prime_comp_dollar,0),/* Column CR*/
		IFNULL(dbe.race_conscious_dbe_part + dbe.race_neutral_dbe_part,0),/* Column CS*/
		CAST(IFNULL(ROUND((((dbe.race_conscious_dbe_part + dbe.race_neutral_dbe_part) / (dbe.race_conscious_prime_comp_dollar + dbe.race_neutral_prime_comp_dollar)) * 100.0),2),0) /100 AS DECIMAL(7,4))/* Column CT*/
	FROM 
		((trams_dbe_report dbe 
		LEFT JOIN vwtramsGeneralBusinessInfo info ON dbe.recipient_id = info.id
        LEFT JOIN trams_civil_rights_compliance comp 
			ON dbe.recipient_id = comp.recipient_id 	
			AND comp.program_name = 'DBE Goal' 
			AND comp.id IN (SELECT MAX(id) FROM trams_civil_rights_compliance sub_comp WHERE sub_comp.recipient_id = dbe.recipient_id AND sub_comp.program_name = 'DBE Goal')
		LEFT JOIN vwtramsLocation tl ON tl.tramsentity_locations_id = dbe.recipient_id AND tl.addressType = 'Physical Address')
		LEFT JOIN
                    (
                             SELECT
                                      id,
                                      recipient_id
									  
                             FROM
                                      trams_application
                             WHERE
                                      status NOT IN (\"Closed\",
                                                     \"Archived\")
                             GROUP BY
                                      recipient_id
                    )
                    as app
                    on
                              app.recipient_id = dbe.recipient_id
		)
	WHERE 
		1";
	
	SET @DBEExcelReportQuery = CONCAT(_sql,_whereClause);
	
	PREPARE dbeStmt FROM @DBEExcelReportQuery;
	EXECUTE dbeStmt;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_project_scope_funding`(IN `p_application_id` INT, IN `p_revision_num` INT)
BEGIN

DECLARE _onClause VARCHAR(4096) DEFAULT " ";
DECLARE _appCreation DATETIME;

SET @uuid = UUID_SHORT();
SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f')); 

SET _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(NULL,p_application_id);

SET _onClause = CONCAT('fsr.LatestFlagStartDate <= \'',_appCreation,'\' AND fsr.LatestFlagEndDate >= \'',_appCreation,'\'');

	INSERT INTO trams_temp_budget_revision_funding_summary_newest_change_log 
		SELECT
		    NULL AS trams_temp_budget_revision_funding_summary_newest_change_log_id,
			id AS newest_change_log_id,
			@uuid AS uuid,
	        @sid AS time_stamp
		FROM
			`trams_line_item_change_log`
		WHERE
			application_id = p_application_id	
		AND 
		    revision_number = p_revision_num
		GROUP BY
			line_item_id;
	
	INSERT INTO trams_temp_budget_revision_funding_summary_change_log 
		SELECT
		    NULL AS trams_temp_budget_revision_funding_summary_change_log_id,
			new_cng.id AS newest_change_log_id,
			new_cng.application_id AS application_id,
			li.scope_code AS scope_code,
			li.funding_source_name AS funding_source_name,
			li.scope_detailed_name AS scope_detailed_name,
			ifnull(SUM(new_cng.original_fta_amount), 0) AS original_fta_amount,
			ifnull(SUM(new_cng.revised_fta_amount), 0) AS revised_fta_amount,
			@uuid AS uuid,
	        @sid AS time_stamp
		FROM
			`trams_temp_budget_revision_funding_summary_newest_change_log` new_cng_ids
			JOIN `trams_line_item_change_log`  new_cng
				ON new_cng_ids.newest_change_log_id = new_cng.id
			JOIN `trams_line_item` li
				ON new_cng.line_item_id = li.id
		WHERE
			li.app_id = p_application_id
		AND 
		    li.other_budget = 0	
        AND 
            new_cng_ids.uuid = @uuid		
		GROUP BY
			li.funding_source_name,
			new_cng.application_id,
			li.scope_code
		ORDER BY new_cng.id DESC;

SET @sqlStatement = CONCAT('SELECT
		uuid_short() AS id,
		cng_log.application_id AS application_id,
		cng_log.scope_code AS scopeCode_text,
		fsr.funding_source_description AS fundingSource_text,
		cng_log.scope_detailed_name AS scopeDetailedName_text,
		CONCAT("$", FORMAT(SUM(cng_log.original_fta_amount), 2)) AS original_FTA_amount_text,
		CONCAT("$", FORMAT((SUM(cng_log.revised_fta_amount) - SUM(cng_log.original_fta_amount)), 2)) AS difference_amount_text,
		CONCAT("$", FORMAT(SUM(cng_log.revised_fta_amount), 2)) AS revised_FTA_amount_text,
		FORMAT(trams_get_fta_funding_amt_pct_change(
			SUM(cng_log.original_fta_amount),
			SUM(cng_log.revised_fta_amount)
		), 2) AS percent_change_text
	FROM trams_temp_budget_revision_funding_summary_change_log cng_log
		JOIN `trams_funding_source_reference` fsr
			ON cng_log.funding_source_name = fsr.funding_source_name 
			AND ',_onClause,'
	WHERE cng_log.uuid = @uuid		
	GROUP BY
		cng_log.application_id,
		cng_log.scope_code,
		fsr.funding_source_description');

		PREPARE sqlStatement FROM @sqlStatement;
		EXECUTE sqlStatement;
		
		-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
        DELETE FROM trams_temp_budget_revision_funding_summary_newest_change_log WHERE uuid = @uuid;
        DELETE FROM trams_temp_budget_revision_funding_summary_change_log WHERE uuid = @uuid;
		
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_by_lineitem_viewprint`(
  IN `p_application_id` INT,
  IN `p_revision_number` INT
)
BEGIN


SET @uuid = UUID_SHORT();
SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));



	INSERT INTO trams_temp_budget_revision_lineitem_sub
	SELECT
    NULL      AS trams_temp_budget_revision_lineitem_sub_id,
	li.app_id AS appid,
	li.project_id AS projectid,
	lc.revision_number AS revision_num,
	li.scope_code AS scope_code,
	li.scope_detailed_name AS scope_name,
	li.line_item_number AS item_number,
	li.line_item_type_name AS item_type_name,
	-- NEW: Select the custom line item name from the line item change log table
	lc.revised_custom_item_name AS CustomLineItemName,
	IFNULL(lc.original_FTA_amount,li.original_FTA_amount) AS original_FTA_amount,
	(IFNULL(lc.revised_FTA_amount,0) - IFNULL(IFNULL(lc.original_FTA_amount,li.original_FTA_amount),0)) AS difference_FTA_amount,
	IFNULL(lc.revised_FTA_amount,0) AS revised_FTA_amount,
	IFNULL((lc.original_total_eligible_amount - lc.original_FTA_amount),0) AS original_Non_FTA_amount,
	IFNULL(((lc.revised_total_eligible_amount - lc.revised_FTA_amount) - (lc.original_total_eligible_amount - lc.original_FTA_amount)),0) AS difference_Non_FTA_amount,
	IFNULL((lc.revised_total_eligible_amount - lc.revised_FTA_amount),0) AS revised_Non_FTA_amount,
	IFNULL(lc.original_total_eligible_amount,0) AS original_total_eligible_amount,
	IFNULL((lc.revised_total_eligible_amount - lc.original_total_eligible_amount),0) AS difference_total_eligible_amount,
	IFNULL(lc.revised_total_eligible_amount,0) AS revised_total_eligible_amount,
	IFNULL(lc.original_quantity,0) AS original_quantity,
	(IFNULL(IFNULL(lc.revised_quantity,li.latest_revised_quantity),0) - IFNULL(IFNULL(lc.original_quantity,li.quantity),0)) AS difference_quantity,
	IFNULL(lc.revised_quantity,0) AS revised_quantity,
	@uuid AS uuid,
	@sid AS time_stamp
	FROM Appian.trams_application a
	JOIN Appian.trams_line_item li ON a.id = li.app_id
	LEFT JOIN Appian.trams_line_item_change_log lc ON li.id = lc.line_item_id
	WHERE a.id = p_application_id 
	 AND lc.revision_number = p_revision_number;

	INSERT INTO trams_temp_budget_revision_lineitem_union 
    SELECT 
	NULL                                                            AS trams_temp_budget_revision_lineitem_union_id,
	brsub1.appid                                                    AS appid,
	brsub1.projectid                                                AS projectid,
	brsub1.revision_num                                             AS revision_num,
	brsub1.scope_code                                               AS scope_code,
	brsub1.scope_name                                               AS scope_name,
	brsub1.item_number                                              AS item_number,
	brsub1.item_type_name                                           AS item_type_name,
	-- Continue to pass down the custom line item name
	brsub1.CustomLineItemName                                       AS CustomLineItemName,
	CONCAT('$', FORMAT(brsub1.original_fta_amount, 2))              AS original_FTA_amount,
	CONCAT('$', FORMAT(brsub1.difference_fta_amount, 2))            AS difference_FTA_amount,
	CONCAT('$', FORMAT(brsub1.revised_fta_amount, 2))               AS revised_FTA_amount,
	CONCAT('$', FORMAT(brsub1.original_non_fta_amount, 2))          AS original_Non_FTA_amount,
	CONCAT('$', FORMAT(brsub1.difference_non_fta_amount, 2))        AS difference_Non_FTA_amount,
	CONCAT('$', FORMAT(brsub1.revised_non_fta_amount, 2))           AS revised_Non_FTA_amount,
	CONCAT('$', FORMAT(brsub1.original_total_eligible_amount, 2))   AS original_total_eligible_amount,
	CONCAT('$', FORMAT(brsub1.difference_total_eligible_amount, 2)) AS difference_total_eligible_amount,
	CONCAT('$', FORMAT(brsub1.revised_total_eligible_amount, 2))    AS revised_total_eligible_amount,
	brsub1.original_quantity                                        AS original_quantity,
	brsub1.difference_quantity                                      AS difference_quantity,
	brsub1.revised_quantity                                         AS revised_quantity,
	@uuid AS uuid,
	@sid AS time_stamp
	FROM   trams_temp_budget_revision_lineitem_sub brsub1
	WHERE  brsub1.uuid = @uuid;
    
	INSERT INTO trams_temp_budget_revision_lineitem_final
	SELECT 
	NULL                                                AS trams_temp_budget_revision_lineitem_final_id,
	Uuid_short()                                        AS id,
	budgetRevisionView.appid                            AS application_id,
	budgetRevisionView.projectid                        AS project_id,
	budgetRevisionView.revision_num                     AS revision_number,
	budgetRevisionView.scope_code                       AS scopeCode_text,
	budgetRevisionView.scope_name                       AS scopeName_text,
	budgetRevisionView.item_number                      AS itemNumber_text,
	budgetRevisionView.item_type_name                   AS itemTypeName_text,
	-- Continue to pass down the custom line item name
	budgetRevisionView.CustomLineItemName               AS CustomLineItemName,
	budgetRevisionView.original_fta_amount              AS originalAwardFTAAmount_text,
	budgetRevisionView.difference_fta_amount            AS differenceFTAAmount_text,
	budgetRevisionView.revised_fta_amount               AS revisedFTAAmount_text,
	budgetRevisionView.original_non_fta_amount          AS originalNonFTAAmount_text,
	budgetRevisionView.difference_non_fta_amount        AS differenceNonFTAAmount,
	budgetRevisionView.revised_non_fta_amount           AS revisedNonFTAAmount_text,
	budgetRevisionView.original_total_eligible_amount   AS originalTotalEligibleAmount_text,
	budgetRevisionView.difference_total_eligible_amount AS differenceTotalEligibleAmount_text,
	budgetRevisionView.revised_total_eligible_amount    AS revisedTotalEligibleAmount_text,
	budgetRevisionView.original_quantity                AS originalQuantity_int,
	budgetRevisionView.difference_quantity              AS differenceQuantity_int,
	budgetRevisionView.revised_quantity                 AS revisedQuantity_int,
	@uuid AS uuid,
	@sid AS time_stamp
	FROM   trams_temp_budget_revision_lineitem_union budgetRevisionView
	WHERE budgetRevisionView.uuid = @uuid;

	INSERT INTO trams_temp_budget_revision_lineitem_final
	SELECT 
	NULL                                                                AS       trams_temp_budget_revision_lineitem_final_id,
	Uuid_short()                                                        AS       id,
	brsub.appid                                                         AS       application_id,
	brsub.projectid                                                     AS       project_id,
	brsub.revision_num                                                  AS       revision_number,
	brsub.scope_code                                                    AS       scopeCode_text,
	brsub.scope_name                                                    AS       scopeName_text,
	''                                                                  AS       itemNumber_text,
	''                                                                  AS       itemTypeName_text,
	-- Pass down the custom line item name. Follows the Appian naming convention
	-- as this stored procedure maps directly to a cdt.
	brsub.CustomLineItemName                                            AS       customItemName_text,
	CONCAT('$', FORMAT(SUM(brsub.original_fta_amount), 2))              AS       originalAwardFTAAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.difference_fta_amount), 2))            AS       differenceFTAAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.revised_fta_amount), 2))               AS       revisedFTAAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.original_non_fta_amount), 2))          AS       originalNonFTAAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.difference_non_fta_amount), 2))        AS       differenceNonFTAAmount,
	CONCAT('$', FORMAT(SUM(brsub.revised_non_fta_amount), 2))           AS       revisedNonFTAAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.original_total_eligible_amount), 2))   AS       originalTotalEligibleAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.difference_total_eligible_amount), 2)) AS       differenceTotalEligibleAmount_text,
	CONCAT('$', FORMAT(SUM(brsub.revised_total_eligible_amount), 2))    AS       revisedTotalEligibleAmount_text,
	SUM(CAST(brsub.original_quantity AS UNSIGNED))                      AS       originalQuantity_int,
	SUM(CAST(brsub.difference_quantity AS UNSIGNED))                    AS       differenceQuantity_int,
	SUM(CAST(brsub.revised_quantity AS UNSIGNED))                       AS       revisedQuantity_int,
	@uuid AS uuid,
	@sid AS time_stamp
    FROM   trams_temp_budget_revision_lineitem_sub brsub
	WHERE  brsub.uuid = @uuid
    GROUP BY brsub.appid,
          brsub.projectid,
          brsub.revision_num,
          brsub.scope_code,
          brsub.scope_name;

	SELECT Uuid_short()        AS id,
	final.application_id AS appId,
	final.project_id AS project_id,
	final.revision_number AS revision_number,
	final.scopeCode_text AS scopeCode_text,
	final.scopeName_text AS scopeName_text,
	final.itemNumber_text AS itemNumber_text,
	final.itemTypeName_text AS itemTypeName_text,
	final.CustomLineItemName AS customItemName_text,
	final.revisedFTAAmount_text AS revisedFTAAmount_text,
	final.revisedNonFTAAmount_text AS revisedNonFTAAmount_text,
	final.revisedTotalEligibleAmount_text AS revisedTotalEligibleAmount_text,
	final.revisedQuantity_int AS revisedQuantity_int
	FROM   trams_temp_budget_revision_lineitem_final final
	WHERE final.uuid = @uuid
	ORDER BY final.application_id,final.project_id,final.revision_number,final.scopeCode_text,final.itemNumber_text;

	-- The rows generated for the procedure are deleted to replicate the effects of a temporary table
	DELETE FROM trams_temp_budget_revision_lineitem_sub WHERE uuid = @uuid;
	DELETE FROM trams_temp_budget_revision_lineitem_union WHERE uuid = @uuid;
	DELETE FROM trams_temp_budget_revision_lineitem_final WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_fyobl_report_excel`(IN p_recipientId VARCHAR(50), IN p_recipientCostCenterCode VARCHAR(50), IN p_applicationNumber VARCHAR(50),
IN p_projectNumber VARCHAR(50), IN p_scopeCode VARCHAR(50), IN p_fundingCostCenterCode VARCHAR(512), IN p_uzaCode VARCHAR(50), IN p_accFundingFiscalYear VARCHAR(50),
IN p_accAppropriationCode VARCHAR(50), IN p_accSectionCode VARCHAR(50), IN p_accLimitationCode VARCHAR(50), IN p_accAuthorizationType VARCHAR(50), IN p_applicationType VARCHAR(50),
IN p_obligationFiscalYear VARCHAR(20), IN p_gmtOffset VARCHAR(10), IN p_returnLimit VARCHAR(20)
)
BEGIN

  /*
   Stored Procedure Name : trams_get_fyobl_report_excel
   Description : To populate data for FYOBL2 excel report.
   Updated by  : Nathan Chatham (2021.Jun.29) - Updated query to support partial FAIN numbers
  */

	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _selectStatement VARCHAR (4096);
	DECLARE _fromTramsStatement VARCHAR (4096);
	DECLARE _fromTeamStatement VARCHAR (4096);
	DECLARE _TramsGroupByStatement VARCHAR (4096);
	DECLARE _TeamGroupByStatement VARCHAR (4096);
	DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _defaultWhereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _closeoutWhereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _tramsWhereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _teamWhereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _orderBy VARCHAR(4096) DEFAULT " ";
	DECLARE _union VARCHAR (128);
	
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', 'In trams_get_fyobl_report_excel', 1);
		
	SET SESSION max_heap_table_size = 1024*1024*768;	
		
	SET _selectStatement = CONCAT(
		"Select
			con.recipient_id AS Recipient_ID,
			gen.recipientAcronym AS Recipient_Acronym,
			app.recipient_name AS Recipient_Name,
			app.recipient_cost_center AS Recipient_Cost_Center,
			IF(
				awa.obligation_fiscal_year IS NULL OR awa.obligation_fiscal_year = '', 
				trams_get_current_fiscal_year(awa.fta_closeout_approval_date_time), 
				awa.obligation_fiscal_year
			) AS Award_Fiscal_Year,
			con.application_number AS Federal_Award_ID,
			con.app_amendment_number AS Amendment_Number,
			app.status AS Award_Status,
			app.application_type AS Award_Type,
			con.project_number AS Project_Number_Award,
			con.po_number AS PO_Number,
			con.scope_code AS Scope_Code,
			con.scope_name AS Scope_Name,
			con.scope_suffix AS Scope_Suffix,
			con.cost_center_code AS Funding_Cost_Center,
			con.uza_code AS Funding_UZA_Code,
			con.account_class_code AS Account_Classification_Code,
			con.fpc AS Financial_Purpose_Code,
			sum(IFNULL(con.obligation_amount,0)) AS Amendment_Obligation_Amount,
			trams_format_date(cast(CONVERT_TZ(max(odt.transaction_date_time),'+00:00',",p_gmtOffset,") AS char)) AS Amendment_Obligation_Date,			
			con.funding_fiscal_year AS Funding_Fiscal_Year,
			con.appropriation_code AS Appropriation_Code,
			con.section_Code AS Section_Code,
			con.limitation_code AS Limitation_Code,
			con.type_authority AS Type_Authority,
			app.discretionary_funds_flag AS Is_Discretionary_Funds,
			sum(IFNULL(con.deobligation_amount,0)) AS Amendment_Deobligation_Amount,
			trams_format_date(cast(CONVERT_TZ(max(ddt.transaction_date_time),'+00:00',",p_gmtOffset,") AS char)) AS Deobligation_Date_Time,
			sco.acc_reservation_amount AS Reservation_Amount,
			trams_format_date(cast(CONVERT_TZ(sco.reservation_date,'+00:00',",p_gmtOffset,") AS char)) AS Reservation_Date,
			ref.ref_value AS transaction_app_type,
			/* Included in the  _selectStatement with new reconciliation column */
			CASE when con.is_reconciled = 1 then 'YES' Else 'No' End as Reconciliations "); 
			
	SET _fromTramsStatement = 
		"FROM
			trams_contract_acc_transaction con 
			LEFT JOIN trams_contract_acc_transaction odt ON odt.id = con.id
		AND odt.transaction_type = 'Obligation'
		LEFT JOIN trams_contract_acc_transaction ddt ON ddt.id = con.id
		AND ddt.transaction_type = 'Deobligation'
		/* Updated trams_general_business_info table Inner Join from gen.recipient_id to gen.id */
			INNER JOIN vwtramsGeneralBusinessInfo gen on gen.id = con.recipient_id
			INNER JOIN trams_application app on app.id = con.application_id
			AND app.amendment_number = con.app_amendment_number
			INNER JOIN trams_application_award_detail awa on awa.application_id = con.application_id
			LEFT JOIN trams_scope_acc sco on sco.application_id = con.application_id
			AND sco.account_class_code = con.account_class_code
			AND sco.acc_fpc = con.fpc
			AND sco.scope_number = con.scope_code
			AND sco.scope_suffix  = con.scope_suffix
			AND sco.funding_uza_code = con.uza_code
			AND sco.cost_center_code = con.cost_center_code
		LEFT JOIN trams_references ref ON ref.id = con.transaction_origin_ref_id ";
	
	SET _fromTeamStatement = 
		"FROM
			trams_contract_acc_transaction con 
		LEFT JOIN trams_contract_acc_transaction odt ON odt.id = con.id
		AND odt.transaction_type = 'Obligation'
		LEFT JOIN trams_contract_acc_transaction ddt ON ddt.id = con.id
		AND ddt.transaction_type = 'Deobligation'
		/* Updated trams_general_business_info table Inner Join from gen.recipient_id to gen.id */
			INNER JOIN vwtramsGeneralBusinessInfo gen on gen.id = con.recipient_id
			INNER JOIN trams_application app on app.id = con.application_id
			AND app.amendment_number = con.app_amendment_number
			INNER JOIN trams_application_award_detail awa on awa.application_id = con.application_id
			LEFT JOIN trams_scope_acc sco on sco.application_id = con.application_id
			AND sco.account_class_code = con.account_class_code
			AND sco.acc_fpc = con.fpc
			AND sco.funding_uza_code = con.uza_code
			AND sco.cost_center_code = con.cost_center_code
		LEFT JOIN trams_references ref ON ref.id = con.transaction_origin_ref_id ";
			
	SET _tramsWhereClause = CONCAT(_whereClause, (" WHERE con.from_trams = 1 " ));
	SET _teamWhereClause = CONCAT(_whereClause, (" WHERE con.from_trams = 0 " ));
	SET _whereClause = CONCAT(_whereClause, (" and con.transaction_type IN ('Obligation', 'Deobligation')"));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.recipient_id in (",NULLIF(p_recipientId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.recipient_cost_center in (",NULLIF(p_recipientCostCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.application_number LIKE '",NULLIF(p_applicationNumber,''),"%'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.project_number  LIKE '",NULLIF(p_projectNumber,''),"%'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.scope_code = '",NULLIF(p_scopeCode,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.uza_code = '",NULLIF(p_uzaCode,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.cost_center_code in (",NULLIF(p_fundingCostCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and app.application_type = '",NULLIF(p_applicationType,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.funding_fiscal_year = '",NULLIF(p_accFundingFiscalYear,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.appropriation_code = '",NULLIF(p_accAppropriationCode,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.section_Code = '",NULLIF(p_accSectionCode,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.limitation_code = '",NULLIF(p_accLimitationCode,''),"'"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and con.type_authority  = '",NULLIF(p_accAuthorizationType,''),"'"),"")));
	
	/* Updated the _whereClause with obligation fiscal year to populate for the close amendments application */
	SET _defaultWhereClause = CONCAT(
		_whereClause,
		(
			SELECT IFNULL(
				CONCAT(
					" and awa.obligation_fiscal_year IS NOT NULL 
					  and awa.obligation_fiscal_year <> '' 
					  and awa.obligation_fiscal_year = '",
					  NULLIF(p_obligationFiscalYear,''),
					  "'"
				),
				""
			)
		)
	);
	SET _closeoutWhereClause = CONCAT(
		_whereClause,
		(
			SELECT IFNULL(
				CONCAT(
					" and (awa.obligation_fiscal_year IS NULL or awa.obligation_fiscal_year = '')
					  and (trams_get_current_fiscal_year(awa.fta_closeout_approval_date_time) = '",
					  NULLIF(p_obligationFiscalYear, ''),
					  "')"
				),
				""
			)
		)
	);
	SET _TramsGroupByStatement = 
		" group by 
			con.application_id,
			con.account_class_code,
			con.fpc,
			con.uza_code, 
			con.cost_center_code,
			con.scope_code,
			con.scope_suffix";
			
	SET _TeamGroupByStatement = 
		" group by 
			con.application_id,
			con.account_class_code,
			con.fpc,
			con.uza_code, 
			con.cost_center_code";
			
	SET _orderBy = 
		" Order By 
				Federal_Award_ID,
				Amendment_Number,
				Scope_Code,
				Scope_Suffix,
				Amendment_Obligation_Date";
				
	/* Changed from 'union' to 'union all' */		
			
	SET _union = " union all ";
			
	SET @query = CONCAT(_selectStatement, _fromTramsStatement, _tramsWhereClause, _defaultWhereClause, _TramsGroupByStatement, _union,
	_selectStatement, _fromTramsStatement, _tramsWhereClause, _closeoutWhereClause, _TramsGroupByStatement, _union, 
	_selectStatement, _fromTeamStatement, _teamWhereClause, _defaultWhereClause, _TeamGroupByStatement, _union, 
	_selectStatement, _fromTeamStatement, _teamWhereClause, _closeoutWhereClause, _TeamGroupByStatement, _orderBy);
	
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', CONCAT('Final Query1: ',_selectStatement, _fromTramsStatement, _tramsWhereClause, _defaultWhereClause, _TramsGroupByStatement, _union), 0);
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', CONCAT('Final Query2: ',_selectStatement, _fromTramsStatement, _tramsWhereClause, _closeoutWhereClause, _TramsGroupByStatement, _union), 0);
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', CONCAT('Final Query3: ',_selectStatement, _fromTeamStatement, _teamWhereClause, _defaultWhereClause, _TeamGroupByStatement, _union), 0);
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', CONCAT('Final Query4: ',_selectStatement, _fromTeamStatement, _teamWhereClause, _closeoutWhereClause, _TeamGroupByStatement, _orderBy), 0);
	
	PREPARE FYOBLStmt FROM @query;
	EXECUTE FYOBLStmt;
	CALL `trams_log`(@sid, 'trams_get_fyobl_report_excel', 'Done!', 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_deobligation_project_scope_funding_summary_for_team`(IN `p_contract_award_id` INT, IN `p_is_closeout` TINYINT)
    NO SQL
BEGIN
  /*
    This procedure takes in a contract_award_id and returns all data in the Project Scope Obligation Summary for the deobligation process. 
    It must use fewer fields for legacy TEAM data, so there is a check at the beginning of the procedure to build additonal group bys and joins.
    If the deobligation is a closeout, logic must be removed to pull correct data. There is an additional if statement which adds in SQL where necessary.
  */
  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _newFndJoinClause VARCHAR(1000) DEFAULT "";

    
  SET _sql = CONCAT(
    "SELECT
      fnd.id AS trams_contract_acc_funding_id,
      app.amendment_number AS amendment_number,
      fnd.po_number AS po_number,
      fnd.project_number AS project_number,
      fnd.scope_number AS scope_number,
      fnd.scope_suffix AS scope_suffix,
      fnd.scope_name AS scope_name,
      fnd.cost_center_code AS cost_center_code,
      fnd.uza_code AS uza_code,
      fnd.account_class_code AS account_class_code,
      fnd.fpc AS fpc,
      fnd.amendment_amount_current AS original_obligation_amount,
      /* BEGIN data used in TrAMS_deobligationDetail */
      fnd.application_id AS application_id,
      fnd.project_id AS project_id,
      fnd.contract_award_id AS contract_award_id,
      fnd.funding_fiscal_year AS funding_fiscal_year,
      fnd.section_code AS section_code, 
      fnd.limitation_code AS limitation_code,
      fnd.authorization_type AS authorization_type,
      fnd.appropriation_code AS appropriation_code,
      fnd.project_number_sequence_number AS project_number_sequence_number,
      fnd.funding_source_name AS funding_source_name,
      accl.dafis_suffix_code AS dafis_suffix_code,
      /* END data used in TrAMS_deobligationDetail */
      ifnull(fnd.amendment_amount_current, 0) - ifnull(cat.deobligation_amount,0) AS net_obligation_amount,
      ifnull(all_cat.net_obligation_amount, 0) - ifnull(all_cat.net_deobligation_amount, 0) - ifnull(all_cat.net_disbursement_amount, 0) + ifnull(all_cat.net_refund_amount, 0) AS unliquidated_balance,
      ifnull(dd.net_deobligated_recovery_amount, 0) AS deobligation_amount
    FROM
      `trams_contract_acc_funding` fnd
      JOIN `trams_application` app
        ON fnd.application_id = app.id
      JOIN `trams_account_class_code_lookup` accl
        ON fnd.account_class_code = accl.account_class_code
      LEFT JOIN (
        /* retrieves all deobligations for a specific amendment */
        SELECT 
            application_id,
          po_number, 
          scope_code, 
          scope_suffix,
          uza_code,
          cost_center_code,
          account_class_code,
          fpc,
          sum(deobligation_amount) AS deobligation_amount
        FROM 
          `trams_contract_acc_transaction` 
        WHERE 
          contract_award_id = ",p_contract_award_id, " 
        GROUP BY 
            application_id,
          po_number, 
          cost_center_code, 
          account_class_code, 
          fpc,
          uza_code
      ) cat 
        ON 
          fnd.application_id = cat.application_id
          AND fnd.po_number = cat.po_number 
          AND fnd.cost_center_code = cat.cost_center_code
          AND fnd.account_class_code = cat.account_class_code
          AND fnd.fpc = cat.fpc
          AND fnd.uza_code = cat.uza_code
          
      LEFT JOIN (
        /* retrieves temporary deobligations that are created throughout the process */
        SELECT 
          amendment_id,
          po_number, 
          scope_number,
          scope_suffix,       
          acc_uza_code,
          acc_cost_center_code,
          account_class_code,
          acc_fpc,
          sum(deobligated_recovery_amount) AS net_deobligated_recovery_amount
        FROM 
          `trams_deobligation_detail`
        WHERE
          deobligated_still_pending <> 0 AND contract_award_id = ",p_contract_award_id," 
        GROUP BY 
          po_number, 
          acc_cost_center_code,
          acc_uza_code,
          account_class_code, 
          acc_fpc,
          amendment_id
      ) dd 
        ON 
          fnd.po_number = dd.po_number
          AND fnd.cost_center_code = dd.acc_cost_center_code
          AND fnd.account_class_code = dd.account_class_code
          AND fnd.fpc = dd.acc_fpc
          AND fnd.uza_code = dd.acc_uza_code
          AND fnd.application_id = dd.amendment_id
          
      LEFT JOIN (
        /* retrieves deobligation, disbursement, and refund data for use in calculating unliquidated balance. this is a summary column across the contract award, meaning it will get to total fundings and appear duplicated for each amendment. */
        SELECT
          cat.contract_award_id,
          cat.po_number, 
          cat.uza_code,
          cat.cost_center_code,
          cat.account_class_code,
          cat.fpc,
          sum(cat.obligation_amount) AS net_obligation_amount,
          sum(cat.deobligation_amount) AS net_deobligation_amount,
          sum(cat.disbursement_amount) AS net_disbursement_amount,
          sum(cat.refund_amount) AS net_refund_amount
        FROM 
          `trams_contract_acc_transaction` cat 
        WHERE 
          contract_award_id = ",p_contract_award_id, " 
        GROUP BY 
          cat.po_number, 
          cat.cost_center_code, 
          cat.account_class_code, 
          cat.fpc
        ORDER BY cat.id DESC
      ) all_cat 
        ON 
          fnd.contract_award_id = all_cat.contract_award_id
          AND fnd.po_number = all_cat.po_number 
          AND fnd.cost_center_code = all_cat.cost_center_code
          AND fnd.account_class_code = all_cat.account_class_code
          AND fnd.fpc = all_cat.fpc
          

    WHERE 
      fnd.contract_award_id = ",p_contract_award_id," 
    ORDER BY app.amendment_number, fnd.account_class_code ASC;"
  );

  SET @deobQuery = _sql;
  
  SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_deobligation_project_scope_funding_summary_from_team', concat('Deobligation Summary Query: ', @deobQuery), 1);
  
  PREPARE deobStmt FROM @deobQuery;
  EXECUTE deobStmt;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_cumulative_apportionment_report`(
    IN p_uzaCode VARCHAR(50),
    IN p_fundingFiscalYear VARCHAR(500),
    IN p_appropriationCode VARCHAR(5),
    p_sectionCode VARCHAR(5),
    IN p_limitationCode VARCHAR(5),
    IN p_lapseYear VARCHAR(5)
)
BEGIN
	 
	DECLARE _insertStatement VARCHAR(4096);
	DECLARE _selectStatement VARCHAR(5000);
	DECLARE _appHistoryFromStatement VARCHAR(5000);
	DECLARE _apportionmentFromStatement VARCHAR(5000);
	DECLARE _appHistoryGroupBy VARCHAR(4096);
	DECLARE _apportionmentGroupBy VARCHAR(4096);
	DECLARE _appHistoryOrderBy VARCHAR(4096);
	DECLARE _apportionmentOrderBy VARCHAR(4096);
	DECLARE _appHistoryWhereClause VARCHAR(4096) DEFAULT " ";
	DECLARE _apportionmentWhereClause VARCHAR(4096) DEFAULT " ";
	
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
    SET @uuid = UUID_SHORT();
	
     /* Apply filters that are inputed by the user */
	SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, " WHERE 1 ");
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND appHis.uza_code LIKE '",NULLIF(p_uzaCode,''), "'"), "")));
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND appHis.funding_fiscal_year IN (",NULLIF(p_fundingFiscalYear,''),")"),"")));
	/* Updated the appropriation code parameter in open and closing quote included at the end of the double quotes at the end for the appropriation filter. */
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND appHis.appropriation_code LIKE '",NULLIF(p_appropriationCode,''), "'"), "")));
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND appHis.section_id LIKE ",NULLIF(p_sectionCode,'')), "")));
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND appHis.limitation_code LIKE '",NULLIF(p_limitationCode,''), "'"), "")));
    SET _appHistoryWhereClause = CONCAT(_appHistoryWhereClause, (SELECT IFNULL(CONCAT(" AND app.lapse_period LIKE ",NULLIF(p_lapseYear,'')), "")));
    
	SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, " WHERE app.active_flag = 1 ");
	SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, " AND NULLIF(appHis.id, '') IS NULL ");
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.uza_code LIKE '",NULLIF(p_uzaCode,''), "'"), "")));
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.funding_fiscal_year IN (",NULLIF(p_fundingFiscalYear,''),")"), "")));
    /* Updated the appropriation code parameter in open and closing quote included at the end of the double quotes at the end for the appropriation filter. */
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.appropriation_code LIKE '",NULLIF(p_appropriationCode,''), "'"), "")));
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.section_id LIKE ",NULLIF(p_sectionCode,'')), "")));
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.limitation_code LIKE '",NULLIF(p_limitationCode,''), "'"), "")));
    SET _apportionmentWhereClause = CONCAT(_apportionmentWhereClause, (SELECT IFNULL(CONCAT(" AND app.lapse_period LIKE ",NULLIF(p_lapseYear,'')), "")));
		
		/* Begin Query Statement */
	SET _insertStatement = "INSERT INTO trams_temp_dcfyap_cumulative_apportionments ";	
		
	SET _selectStatement = 
		"(SELECT
			NULL AS trams_temp_dcfyap_cumulative_apportionments_id,
			IFNULL(appHis.uza_code, app.uza_code) AS uza_code,
			uzaRef.uza_name AS uza_code_Name,
			IF(IFNULL(appHis.uza_code, app.uza_code) LIKE '%0000', staPOP.state_population, uzaPOP.uza_population) AS uza_population,
			tcc.cost_center_shortname AS cost_center_short_name,
			CONCAT(IFNULL(appHis.funding_fiscal_year, app.funding_fiscal_year), '.', IFNULL(appHis.appropriation_code, app.appropriation_code), '.', IFNULL(appHis.section_id, app.section_id), '.', IFNULL(appHis.limitation_code, app.limitation_Code)) AS account_class_code,
			IFNULL(appHis.appropriation_code, app.appropriation_code) AS appropriation_code,
			IFNULL(appHis.section_id, app.section_id) AS section_id,
			IFNULL(appHis.limitation_code, app.limitation_Code) AS limitation_code,
			IFNULL(appHis.funding_fiscal_year, app.funding_fiscal_year) AS funding_fiscal_year,
			app.lapse_period AS lapse_year,
			
			/* Apportionment Totals */
			IFNULL(app.apportionment, 0) AS apportionment_initial_amount,
			IFNULL(SUM(appHis.modified_amount), 0) AS modification_amount,
			IFNULL(SUM(appHis.transfer_in_amount), 0) AS transfer_in_amount,
			IFNULL(SUM(appHis.transfer_out_amount), 0) AS transfer_out_amount,

			/*Apportionment Effective Authority */
			(IFNULL(app.apportionment, 0) + IFNULL(SUM(appHis.modified_amount), 0) + 
			(IFNULL(SUM(appHis.transfer_in_amount), 0) - IFNULL(SUM(appHis.transfer_out_amount), 0))) AS apportionment_effective_authority,
			(IFNULL(SUM(appHis.reserved_amount), 0) - IFNULL(SUM(appHis.unreserve_amount), 0)) AS reserved_amount,
			IFNULL(SUM(appHis.obligated_amount), 0) AS obligated_amount,
			IFNULL(SUM(appHis.recovery_amount), 0) AS recovery_amount,

			/* Apportionment Available Balance */
			(
				(IFNULL(app.apportionment, 0) + IFNULL(SUM(appHis.modified_amount), 0) + 
				(IFNULL(SUM(appHis.transfer_in_amount), 0) - IFNULL(SUM(appHis.transfer_out_amount), 0))) +
				IFNULL(SUM(appHis.recovery_amount), 0) - IFNULL(SUM(appHis.obligated_amount), 0) -
				(IFNULL(SUM(appHis.reserved_amount), 0) - IFNULL(SUM(appHis.unreserve_amount), 0))
			) AS apportionment_available_balance,

			/* Cap Totals */
			IFNULL(app.operating_cap, 0) AS cap_amount,
			IFNULL(SUM(appHis.cap_modify_amt), 0) AS cap_modification_amount,
			IFNULL(SUM(appHis.cap_transfer_in_amt), 0) AS cap_transfer_in_amount,
			IFNULL(SUM(appHis.cap_transfer_out_amt), 0) AS cap_transfer_out_amount,

			/* Cap Effective Authority */
			(IFNULL(app.operating_cap, 0)+ IFNULL(SUM(appHis.cap_modify_amt), 0) +
			(IFNULL(SUM(appHis.cap_transfer_in_amt), 0) - IFNULL(SUM(appHis.cap_transfer_out_amt), 0))) AS cap_effective_authority,
			(IFNULL(SUM(appHis.cap_reserved_amt), 0) - IFNULL(SUM(appHis.cap_unreserved_amt), 0)) AS cap_reserved_amount,
			IFNULL(SUM(appHis.cap_obligated_amt),'0') AS cap_obligation_amount,
			IFNULL(SUM(appHis.cap_recovery_amt),'0') AS cap_recovery_amount,

			/* Cap Available Balance */
			(
				(IFNULL(app.operating_cap, 0) + IFNULL(SUM(appHis.cap_modify_amt), 0) +
				(IFNULL(SUM(appHis.cap_transfer_in_amt), 0) - IFNULL(SUM(appHis.cap_transfer_out_amt), 0))) +
				IFNULL(SUM(appHis.cap_recovery_amt), 0) - IFNULL(SUM(appHis.cap_obligated_amt), 0) -
				(IFNULL(SUM(appHis.cap_reserved_amt), 0) - IFNULL(SUM(appHis.cap_unreserved_amt), 0))
			) AS cap_available_amount,

			/* Ceiling Totals */
			IFNULL(app.ceiling_amount, 0) AS ceiling_amount,
			IFNULL(SUM(appHis.ceiling_modify_amt),0) AS ceiling_modification_amount,
			IFNULL(SUM(appHis.ceiling_transfer_in_amt),0) AS ceiling_transfer_in_amount,
			IFNULL(SUM(appHis.ceiling_transfer_out_amt),0) AS ceiling_transfer_out_amount,

			/* Ceiling Effective Authority - x*/
			(IFNULL(app.ceiling_amount, 0)+ IFNULL(SUM(appHis.ceiling_modify_amt), 0) +
			(IFNULL(SUM(appHis.ceiling_transfer_in_amt), 0) - IFNULL(SUM(appHis.ceiling_transfer_out_amt), 0))) AS ceiling_effective_authority,
			IFNULL(SUM(appHis.ceiling_recovery_amt),0) AS ceiling_recovery_amount,

			/* Ceiling Available Balance - x */
			(
				(IFNULL(app.ceiling_amount, 0)+ IFNULL(SUM(appHis.ceiling_modify_amt), 0) +
				(IFNULL(SUM(appHis.ceiling_transfer_in_amt), 0) - IFNULL(SUM(appHis.ceiling_transfer_out_amt), 0))) +
				IFNULL(SUM(appHis.ceiling_recovery_amt), 0) - IFNULL(SUM(appHis.ceiling_obligated_amt), 0) -
				(IFNULL(SUM(appHis.ceiling_reserved_amt), 0) - IFNULL(SUM(appHis.ceiling_unreserve_amt), 0))
			) AS ceiling_available_amount, 
			
			@uuid AS uuid,
			@sid AS time_stamp ";
	
	SET _appHistoryFromStatement = 
    "FROM trams_apportionment_history appHis 
      LEFT JOIN trams_apportionment app ON app.uza_code = appHis.uza_code
        AND app.funding_fiscal_year = appHis.funding_fiscal_year
        AND app.appropriation_code = appHis.appropriation_code
        AND app.section_id = appHis.section_id
        AND app.limitation_Code = appHis.limitation_Code
        AND app.active_flag = 1
      LEFT JOIN 
        (SELECT 
          uza_code AS uza_code,
          uza_name AS uza_name,
          cost_center_code AS cost_center_code
        FROM 
          trams_uza_code_reference
        WHERE
          version = (SELECT MAX(version) FROM trams_uza_code_reference)
        GROUP BY 
          uza_code
        ) uzaRef ON uzaRef.uza_code = appHis.uza_code
        
      /* UZA Population */
      LEFT JOIN 
        (SELECT 
          uza_code AS uza_code,
          population AS uza_population
        FROM 
          trams_uza_population_by_state
        WHERE
          version = (SELECT MAX(version) FROM trams_uza_population_by_state)
        GROUP BY
          uza_code
        ) uzaPOP ON uzaPOP.uza_code = appHis.uza_code
      
      /* State Population */
      LEFT JOIN trams_state sta ON sta.uza_code = appHis.uza_code
      LEFT JOIN 
        (SELECT
          state_id AS state_id, 
          population AS state_population
        FROM 
          trams_state_population statePOP
        WHERE
          version = (SELECT MAX(version) FROM trams_state_population)
        GROUP BY
          state_id
        ) staPOP ON staPOP.state_id = sta.id
      LEFT JOIN trams_cost_center tcc ON tcc.cost_center_code = uzaRef.cost_center_code ";
			
	SET _apportionmentFromStatement = 
    "FROM trams_apportionment app 
      LEFT JOIN trams_apportionment_history appHis ON appHis.uza_code = app.uza_code
        AND appHis.funding_fiscal_year = app.funding_fiscal_year
        AND appHis.appropriation_code = app.appropriation_code
        AND appHis.section_id = app.section_id
        AND appHis.limitation_Code = app.limitation_Code
      LEFT JOIN 
        (SELECT 
          uza_code AS uza_code,
          uza_name AS uza_name,
          cost_center_code AS cost_center_code
        FROM 
          trams_uza_code_reference
        WHERE
          version = (SELECT MAX(version) FROM trams_uza_code_reference)
        GROUP BY 
          uza_code
        ) uzaRef ON uzaRef.uza_code = app.uza_code
        
      /* UZA Population */
      LEFT JOIN 
        (SELECT 
          uza_code AS uza_code,
          population AS uza_population
        FROM 
          trams_uza_population_by_state
        WHERE
          version = (SELECT MAX(version) FROM trams_uza_population_by_state)
        GROUP BY
          uza_code
        ) uzaPOP ON uzaPOP.uza_code = app.uza_code
      
      /* State Population */
      LEFT JOIN trams_state sta ON sta.uza_code = app.uza_code
      LEFT JOIN 
        (SELECT
          state_id AS state_id, 
          population AS state_population
        FROM 
          trams_state_population statePOP
        WHERE
          version = (SELECT MAX(version) FROM trams_state_population)
        GROUP BY
          state_id
        ) staPOP ON staPOP.state_id = sta.id
      LEFT JOIN trams_cost_center tcc ON tcc.cost_center_code = uzaRef.cost_center_code ";
		
	SET _appHistoryGroupBy = 
    " GROUP BY
      appHis.uza_code,
      appHis.funding_fiscal_year,
      appHis.appropriation_code,
      appHis.section_id,
      appHis.limitation_code ";
			
	SET _apportionmentGroupBy = 
    " GROUP BY
      app.uza_code,
      app.funding_fiscal_year,
      app.appropriation_code,
      app.section_id,
      app.limitation_code ";
		
	SET _appHistoryOrderBy = 
    " ORDER BY
      appHis.funding_fiscal_year) ";
     
	SET _apportionmentOrderBy = 
    " ORDER BY
      app.funding_fiscal_year) ";
			
	/* The reason there are 2 queries with a UNION is because there are apportionments that have no transaction records and when doing a LEFT JOIN to the apportionment table those records are not included. Due to OUTER JOIN not being suppoerted in MYSQL, a LEFT and RIGHT JOIN is necessary to emulate OUTER JOIN */
	SET @insertQuery = CONCAT(_insertStatement, _selectStatement, _appHistoryfromStatement, _appHistoryWhereClause, _appHistoryGroupBy, _appHistoryOrderBy, " UNION ", _selectStatement, _apportionmentFromStatement, _apportionmentWhereClause, _apportionmentGroupBy, _apportionmentOrderBy);
	PREPARE cumAppInsertQuery FROM @insertQuery;
	EXECUTE cumAppInsertQuery;	
	
	SET @selectQuery = "
	SELECT 
		uza_code,
		uza_code_name,
		uza_population,
		cost_center_short_name,
		account_class_code,
		appropriation_code,
		section_id,
		limitation_code,
		funding_fiscal_year,
		lapse_year,
		apportionment_initial_amount,
		modification_amount,
		transfer_in_amount,
		transfer_out_amount,
		apportionment_effective_authority,
		reserved_amount,
		obligated_amount,
		recovery_amount,
		apportionment_available_balance,
		cap_amount,
		cap_modification_amount,
		cap_transfer_in_amount,
		cap_transfer_out_amount,
		cap_effective_authority,
		cap_reserved_amount,
		cap_obligation_amount,
		cap_recovery_amount,
		cap_available_amount,
		ceiling_amount,
		ceiling_modification_amount,
		ceiling_transfer_in_amount,
		ceiling_transfer_out_amount,
		ceiling_effective_authority,
		ceiling_recovery_amount,
		ceiling_available_amount
	FROM trams_temp_dcfyap_cumulative_apportionments 
	WHERE uuid = @uuid";
	PREPARE cumAppSelectQuery FROM @selectQuery;
	EXECUTE cumAppSelectQuery;
	
	DELETE FROM trams_temp_dcfyap_cumulative_apportionments WHERE uuid = @uuid;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_budget_revision_funding_summary_by_project`(IN `p_application_id` INT, IN p_fundingDeploy DATE)
BEGIN

declare _onClause varchar(4096) default " ";
declare _appCreation date;

set _appCreation = (SELECT DATE(created_date_time) FROM trams_application WHERE id = p_application_id AND amendment_number = 0);

set _onClause = if(_appCreation<p_fundingDeploy,'`fsr`.`version` = 0','`fsr`.`latest_flag` = 1');

SET @sqlStatement = concat('select uuid_short() AS `id`,`l`.`app_id` AS `application_id`,
`l`.`project_id` AS `project_id`,
ifnull((select `Appian`.`trams_funding_source_reference`.`funding_source_description` 
from `Appian`.`trams_funding_source_reference` 
where `Appian`.`trams_funding_source_reference`.`funding_source_name` = 
`l`.`funding_source_name` AND ',_onClause,'),
`l`.`funding_source_name`) AS `fundingSource_text`,
concat("$",format(sum(ifnull(`l`.`original_FTA_amount`,0)),2)) AS `orgionalAwardAmount_text`,
concat("$",format((sum(ifnull(`l`.`revised_FTA_amount`,0)) - sum(ifnull(`l`.`original_FTA_amount`,0))),2)) AS `differenceAmount_text`,
concat("$",format(sum(ifnull(`l`.`revised_FTA_amount`,0)),2)) AS `budgetRevisionAmount_text` 
from `Appian`.`trams_line_item` `l` where (`l`.`funding_source_name` is not null) 
and l.app_id = ',p_application_id, '  
group by `l`.`app_id`,`l`.`project_id`,`l`.`funding_source_name` 
order by `l`.`app_id`,`l`.`project_id`,`l`.`funding_source_name`');
 

	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_grantee_poc_details_summary`(IN p_granteeid VARCHAR(512), IN p_costcenterCode VARCHAR(512), IN p_return_limit VARCHAR(100), 
IN p_contactTypeUnion VARCHAR(10), IN p_contactTypeCEO VARCHAR(10), IN p_contactTypeMPO VARCHAR(10), IN p_contactTypeEEO VARCHAR(10), IN p_contactTypeDBE VARCHAR(10), 
IN p_contactTypeTitleVI VARCHAR(10), IN p_contactTypeSection504 VARCHAR(10), IN p_contactTypeECHO VARCHAR(10), IN p_contactTypeGrant VARCHAR(10), IN p_contactTypeGeneral VARCHAR(10), 
IN p_gmtOffSET VARCHAR(10))
BEGIN

	/*
     Stored Procedure Name : trams_get_grantee_poc_details_summary
     Description : To populate data for TrAMS 'Recipient POC Details' excel report.
     Updated by  : Garrison Dean (01.20.2021)
    */
	
	DECLARE _selectFromStatement varchar(10240) DEFAULT "";
	DECLARE _whereClause varchar(10240) DEFAULT " ";
	DECLARE _limitClause varchar(100) DEFAULT " ";
	
	CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);
	
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_granteeId), " ", CONCAT(" AND gb.recipientId IN (", p_granteeId , ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_costCenterCode), " ", CONCAT(" AND cs.cost_center_code IN (" , p_costCenterCode , ")")));
	
	IF (
		p_contactTypeCEO IS NOT NULL OR 
		p_contactTypeDBE IS NOT NULL OR 
		p_contactTypeEEO IS NOT NULL OR 
		p_contactTypeEcho IS NOT NULL OR 
		p_contactTypeGrant IS NOT NULL OR 
		p_contactTypeMPO IS NOT NULL OR 
		p_contactTypeSection504 IS NOT NULL OR 
		p_contactTypeTitleVI IS NOT NULL OR 
		p_contactTypeUnion IS NOT NULL OR 
		p_contactTypeGeneral IS NOT NULL
	) THEN
		SET _whereClause = CONCAT(_whereClause, "AND (0");
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeUnion), " ", CONCAT(" OR gp.contact_for_union_flag = " , p_contactTypeUnion)));	
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeCEO), " ", CONCAT(" OR gp.contact_for_ceo_flag = " , p_contactTypeCEO)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeMPO), " ", CONCAT(" OR gp.contact_for_mpo_flag = " , p_contactTypeMPO)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeEEO), " ", CONCAT(" OR gp.contact_for_eeo_flag = " , p_contactTypeEEO)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeDBE), " ", CONCAT(" OR gp.contact_for_dbe_flag = " , p_contactTypeDBE)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeTitleVI), " ", CONCAT(" OR gp.contact_for_titlevi_flag = " , p_contactTypeTitleVI)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeSection504), " ", CONCAT(" OR gp.contact_for_section504_flag = " , p_contactTypeSection504)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeEcho), " ", CONCAT(" OR gp.contact_for_echo_flag = " , p_contactTypeEcho)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeGrant), " ", CONCAT(" OR gp.contact_for_grant_flag = " , p_contactTypeGrant)));
		SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_contactTypeGeneral), " ", CONCAT(" OR gp.contact_for_general_flag = " , p_contactTypeGeneral)));
		SET _whereClause = CONCAT(_whereClause, ")");	
	END IF;

	SET _selectFromStatement = CONCAT(" 
		SELECT 
			gp.recipient_id AS grantee_id,
			gb.legalBusinessName AS businessname,
			CONCAT(cs.cost_center_code,' ',cs.office_description) AS costcentername,
			gp.contact_union_name AS contactunionnametext,
			gp.title AS contacttitletext,
			gp.full_name AS full_name,
			CONCAT(address_line_1,' ',address_line_2) AS contactaddress,
			gp.address_city AS contactcitytext,
			gp.state AS contactstatetext,
			gp.contact_zip_code_5 AS contactzip5text,
			gp.phone_number AS contactphonetext,
			gp.alternate_phone_number AS contactalternatephonetext,
			gp.fax_number AS contactfaxtext,
			gp.email_address AS contactemailaddresstext,
			gp.contact_website_address AS contactwebsiteaddresstext,		
			gp.contact_for_union_flag AS contacttypeunionbool,
			gp.contact_for_ceo_flag AS contacttypeceobool,
			gp.contact_for_mpo_flag AS contacttypempobool,
			gp.contact_for_eeo_flag AS contacttypeeeobool,
			gp.contact_for_dbe_flag AS contacttypedbebool,
			gp.contact_for_titlevi_flag AS contacttypetitlevibool,
			gp.contact_for_section504_flag AS contacttypesection504bool,
			gp.contact_for_echo_flag AS contacttypeechobool,
			gp.contact_for_grant_flag AS contacttypegrantsbool,
			gp.contact_for_general_flag AS contacttypegeneralftabool,
			gp.union_statewide_application_flag AS cntctninsttewideapplictinbl,
			trams_format_date(cast(CONVERT_TZ(gp.updated_date_time, '+00:00',   ",p_gmtOffset,") AS char)) AS updatedDateTime,
			gp.updated_by AS updatedBy
		FROM trams_grantee_poc gp 
		JOIN vwtramsGeneralBusinessInfo gb ON gb.recipientId = gp.recipient_id
		LEFT JOIN trams_cost_center cs ON gb.costCenterCode = cs.cost_center_code
		WHERE 1 ",
		_whereClause);
	
	
	SET @granteePocSql = CONCAT(_selectFromStatement,_limitClause);

	SET @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL trams_log(@sid, 'trams_get_grantee_poc_details_summary', CONCAT('Get Grantee POC Query: ',@granteePocSql), 1); 
	
	PREPARE granteePocStmt FROM @granteePocSql;
	EXECUTE granteePocStmt;
	
	CALL trams_log(@sid, 'trams_get_grantee_poc_details_summary', 'Done!', 1); 

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_grantee_rollup_summary`(IN `p_granteeId` VARCHAR(50), IN `p_costCenterCode` VARCHAR(50), IN `p_fiscalYear` VARCHAR(10), IN `p_return_limit` VARCHAR(10))
BEGIN

  declare _sql varchar(40960) default "";
  declare _whereClause varchar(4096) default " ";
  declare _limitClause varchar(100) default " ";
  
  call trams_get_sql_limit_clause(p_return_limit,_limitClause);
  
  drop temporary table if exists tmp_application_id;
  drop temporary table if exists tmp_contract_acc_transaction_summary;
  
  
  set _sql = "create temporary table tmp_application_id engine=memory 
              select id
              from trams_application a
        where 1 
               ";
  
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.recipient_id in (",NULLIF(p_granteeId,''),")"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.recipient_cost_center in (",NULLIF(p_costCenterCode,''),")"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fiscal_year in (",NULLIF(p_fiscalYear,''),")"),"")));
  
  SET @applicationQuery = concat(_sql,_whereClause,_limitClause);
  
  set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_grantee_rollup_summary', concat('Get Application Query: ',@applicationQuery), 1); 
  
  PREPARE applicationStmt FROM @applicationQuery;
  EXECUTE applicationStmt; 
  
  create index tmp_application_id_id on tmp_application_id(id);
  
  set @appCount=(select count(*) from tmp_application_id);
  CALL `trams_log`(@sid, 'trams_get_grantee_rollup_summary', concat('Total Application: ',@appCount), 1);   
   SET SESSION max_heap_table_size = 1024*1024*128;
  create temporary table tmp_contract_acc_transaction_summary engine=memory 
  select 
  contract_acc_transaction.application_id AS app_id,
  contract_acc_transaction.section_code AS section_code,
  sum(contract_acc_transaction.obligation_amount) AS total_obligation_amount,
  sum(contract_acc_transaction.deobligation_amount) AS total_deobligation_amount,
  sum(contract_acc_transaction.disbursement_amount) AS total_disbursement_amount,
  sum(contract_acc_transaction.refund_amount) AS total_refund_amount,
  (((ifnull(sum(contract_acc_transaction.obligation_amount),0) 
    - ifnull(sum(contract_acc_transaction.deobligation_amount),0)) 
    - ifnull(sum(contract_acc_transaction.disbursement_amount),0)) 
    + ifnull(sum(contract_acc_transaction.refund_amount),0)) AS total_unliquidated_amount, 
  (sum(contract_acc_transaction.disbursement_amount) 
    / sum(contract_acc_transaction.obligation_amount)) AS percent_disbursement_amount
  from (trams_contract_acc_transaction contract_acc_transaction join trams_application a on((contract_acc_transaction.application_id = a.id))) 
  where a.id in (select id from tmp_application_id)
  group by contract_acc_transaction.application_id,
      contract_acc_transaction.section_code;

  set @appCount=(select count(*) from tmp_contract_acc_transaction_summary);
  CALL `trams_log`(@sid, 'trams_get_grantee_rollup_summary', concat('Total rollup contract_acc_transaction: ',@appCount), 1);   
  
  create index tmp_contract_acc_transaction_summary_appid on tmp_contract_acc_transaction_summary(app_id);
  
  SELECT   a.recipient_id               AS grantee_id, 
       gb.recipient_acronym                       AS acronym, 
       gb.legal_business_name                   AS grantee_name, 
       a.recipient_cost_center            AS grantee_cost_center, 
       a.fiscal_year                    AS fiscal_year, 
       count(a.application_number)       AS number_of_applications, 
       cast(ifnull(Sum(r.total_reserved_amount),0)  as decimal(12,2)) AS total_reservation_amount, 
       cast(ifnull(Sum(av.total_obligation_amount),0)   as decimal(12,2)) AS total_obligation_amount, 
       cast(ifnull(Sum(av.total_deobligation_amount),0) as decimal(12,2)) AS total_deobligation_amount, 
       cast(ifnull(Sum(av.total_disbursement_amount),0) as decimal(12,2)) AS total_disbursement_amount, 
       cast(ifnull(Sum(`av`.`total_refund_amount`),0)       as decimal(12,2)) AS total_refund_amount, 
       cast(ifnull(Sum(`av`.`total_unliquidated_amount`),0) as decimal(12,2)) AS total_unliquidated_amount 
from trams_application a 
    join trams_general_business_info gb on a.recipient_id = gb.recipient_id     
    left join trams_fta_review fr on a.id = fr.application_id 
    left join trams_application_award_detail aa on a.id = aa.application_id
    left join trams_reservation r on a.id = r.application_id    
    left join tmp_contract_acc_transaction_summary av on a.id=av.app_id
  where a.id in (select id from tmp_application_id) 
GROUP  BY a.recipient_id, 
          a.fiscal_year  ;
  
  CALL `trams_log`(@sid, 'trams_get_grantee_rollup_summary', 'Done!', 1);   
  
  drop temporary table if exists tmp_application_id;
  drop temporary table if exists tmp_contract_acc_transaction_summary;
  

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_most_recent_milestone_progress_report_excel`(
	IN p_granteeId VARCHAR(512),
	IN p_costCenterCode VARCHAR(512),
	IN p_appNumber VARCHAR(512),
	IN p_reportStatus VARCHAR(1024),
	IN p_reportFrequency VARCHAR(512), 
    IN p_postAwardManager VARCHAR(1024), 
    IN p_reportFiscalYear VARCHAR(20), 
    IN p_reportFiscalQuarter VARCHAR(20),
	IN p_reportFiscalMonth VARCHAR(20),
	IN p_reportFinalFlag TINYINT(1),
    IN p_reportFundingSource VARCHAR(10),
    IN p_gmtOffset VARCHAR(512),
    IN p_return_limit VARCHAR(50)
)
BEGIN
    DECLARE _sql TEXT default "";
    DECLARE _whereClause varchar(4096) default "";
    DECLARE _spName varchar(255) default "trams_get_most_recent_milestone_progress_report_excel";

    SET @sid = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));

    CALL `trams_log`(@sid, _spName, CONCAT(
        now(),
        ' CALL SP Inputs: ',
        IFNULL(`p_granteeId`,'NULL'), ',',
        IFNULL(`p_costCenterCode` ,'NULL'), ',',
        IFNULL(`p_appNumber` ,'NULL'),',',
        IFNULL(`p_reportStatus` ,'NULL'),',',
        IFNULL(`p_reportFrequency` ,'NULL'),',',
        IFNULL(`p_postAwardManager` ,'NULL'),',',
        IFNULL(`p_reportFiscalYear` ,'NULL'),',',
        IFNULL(`p_reportFiscalQuarter` ,'NULL'),',',
        IFNULL(`p_reportFiscalMonth` ,'NULL'),',',
		IFNULL(`p_reportFinalFlag` ,'NULL'),',',
		IFNULL(`p_reportFundingSource` ,'NULL'),',',
        IFNULL(`p_gmtOffset` , '+00:00'), ',',
        IFNULL(`p_return_limit` ,'NULL'),','
    ), 1);

    -- construct the where clause for the input parameters
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_granteeId), " ", CONCAT(" AND mpr.RecipientId IN (", p_granteeId, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_costCenterCode), " ", CONCAT(" AND mpr.RecipientCostCenter IN (", p_costCenterCode, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_appNumber), " ", CONCAT(" AND mpr.AppNumber LIKE '%", p_appNumber, "%'")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportStatus), " ", CONCAT(" AND mpr.ReportStatus IN (", p_reportStatus, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFrequency), " ", CONCAT(" AND mpr.ReportPeriodType IN (", p_reportFrequency, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_postAwardManager), " ", CONCAT(" AND mpr.AppPostAwardManager IN (", p_postAwardManager, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalYear), " ", CONCAT(" AND mpr.ReportFiscalYear IN (", p_reportFiscalYear, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalQuarter), " ", CONCAT(" AND mpr.ReportFiscalQuarter IN (", p_reportFiscalQuarter, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalMonth), " ", CONCAT(" AND mpr.ReportFiscalMonth IN (", p_reportFiscalMonth, ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFinalFlag), " ", CONCAT(" AND mpr.ReportFinalFlag = (", p_reportFinalFlag, ")")));
	-- added new parameter to allow filtering by funding source
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFundingSource), " ", CONCAT(" AND mpr.FundingSourceName IN (", p_reportFundingSource, ")")));

    -- select the desired rows from the view
    SET _sql = CONCAT("
SELECT
    mpr.RecipientCostCenter,
    mpr.RecipientId,
    mpr.RecipientLegalBusinessName,
    mpr.AppNumber,
    mpr.AppType,
    mpr.AppPostAwardManager,
    mpr.FundingSourceName,
    mpr.ReportPeriodType,
    mpr.ReportPeriodTypeConcat,
    mpr.ReportDueDate,
    trams_format_date(cast(CONVERT_TZ(mpr.ReportSubmittedDateTime, '+00:00', ", p_gmtOffset, ") AS char)) AS `ReportSubmittedDateTime`,
    mpr.ReportStatus,
    mpr.LineItemNumber,
    mpr.LineItemActivityName,
    mpr.LineItemQuantity,
    mpr.LineItemRevisedFTAAmount,
    mpr.MilestoneTitle,
    mpr.MilestoneEstimatedCompletionDate,
    mpr.RemarkRevisedCompletionDate,
    mpr.RemarkActualCompletionDate,
    mpr.AppEndDate,
    mpr.EstimatedRevisedDateDelta,
    mpr.EstimatedActualDateDelta
FROM `vwtramsMilestoneProgressReportDetail` mpr
-- added to only pull the reports data that is contained in the vwtramsLatestMilestoneProgressReport
JOIN ( 
	SELECT ReportID 
	FROM vwtramsLatestMilestoneProgressReport
	) latest ON latest.ReportID = mpr.ReportId
WHERE 1 ");

    -- add the where clause and limit to the select statement
    SET @query = CONCAT(_sql, _whereClause, IFNULL(CONCAT(" LIMIT ", NULLIF(p_return_limit, '')), ""));


    -- excecute the statement
    PREPARE stmt FROM @query;
    EXECUTE stmt;

CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_mpr_ffr_review_report`(IN `p_fiscalYear` VARCHAR(512), IN `p_costCenterCode` VARCHAR(512), IN `p_appStatus` VARCHAR(512), IN `p_granteeId` TEXT, IN `p_active` VARCHAR(512), IN `p_appNumber` VARCHAR(512), IN `p_appType` VARCHAR(512), IN `p_reportType` VARCHAR(50), IN `p_reportStatus` VARCHAR(20), IN `p_finalReport` VARCHAR(512), IN `p_reportPeriodType` VARCHAR(7680), IN `p_lastSubmittedDateStart` VARCHAR(20), IN `p_lastSubmittedDateEnd` VARCHAR(20), IN `p_return_limit` VARCHAR(20), IN `p_reportPeriod` VARCHAR(512), IN `p_year` VARCHAR(500), IN `p_month` VARCHAR(500), IN `p_quarter` VARCHAR(500))
BEGIN
		declare _sql_final varchar(40960) default "";
		declare _whereClause_mpr_ffr varchar(40960) default " ";
		declare _whereClause_ffr varchar(40960) default " ";
		declare _whereClause_mpr varchar(40960) default " ";
		declare _limitClauseSingle varchar(100) default " limit 1000 ";

    set _whereClause_mpr_ffr = concat(_whereClause_mpr_ffr, (select ifnull(concat(" and 	`AP`.`fain_fiscal_year`  = (",NULLIF(p_fiscalYear,''),")"),"")));
		set _whereClause_mpr_ffr =concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.recipient_cost_center in (",NULLIF(p_costCenterCode,''),")"),"")));
		set _whereClause_mpr_ffr =concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.`status` in ('",NULLIF(p_appStatus,''),"')"),""))); 
		set _whereClause_mpr_ffr =concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.recipient_id in (",NULLIF(p_granteeId,''),")"),"")));
		set _whereClause_mpr_ffr =concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.active_flag in (",NULLIF(p_active,''),")"),"")));
		set _whereClause_mpr_ffr =concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.application_number like ('",NULLIF(p_appNumber,''),"%')"),"")));
    set _whereClause_mpr_ffr = concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.`modified_date_time` >= '",NULLIF(p_lastSubmittedDateStart,''),"'"),"")));
    set _whereClause_mpr_ffr = concat(_whereClause_mpr_ffr, (select ifnull(concat(" and `AP`.`modified_date_time` <= '",NULLIF(p_lastSubmittedDateEnd,''),"'"),"")));
		
    if (p_reportType like '%FFR%') then
      set _whereClause_ffr = concat(_whereClause_ffr, (select ifnull(concat(" and `FFR`.`report_status` in ('",NULLIF(p_reportStatus,''),"')"),"")));
      set _whereClause_ffr = concat(_whereClause_ffr, (select ifnull(concat(" and `FFR`.`final_report_flag` in (",NULLIF(p_finalReport,''),")"),"")));
      set _whereClause_ffr = concat(_whereClause_ffr, (select ifnull(concat(" and `FFR`.`report_period_type_concat` in (",NULLIF(p_reportPeriodType,''),")"),"")));
      set _whereClause_ffr = concat(_whereClause_ffr, (select ifnull(concat(" and `FFR`.`report_period_type` in (",NULLIF(p_reportPeriod,''),")"),"")));
      set _whereClause_ffr = concat(_whereClause_mpr_ffr, _whereClause_ffr);
    end if;
    if (p_reportType like '%MPR%') then
      set _whereClause_mpr = concat(_whereClause_mpr, (select ifnull(concat(" and `MPR`.`report_status` in ('",NULLIF(p_reportStatus,''),"')"),"")));
      set _whereClause_mpr = concat(_whereClause_mpr, (select ifnull(concat(" and `MPR`.`final_report_flag` in (",NULLIF(p_finalReport,''),")"),"")));
      set _whereClause_mpr = concat(_whereClause_mpr, (select ifnull(concat(" and `MPR`.`report_period_type_concat` in (",NULLIF(p_reportPeriodType,''),")"),"")));
      set _whereClause_mpr = concat(_whereClause_mpr, (select ifnull(concat(" and `MPR`.`report_period_type` in (",NULLIF(p_reportPeriod,''),")"),"")));
      set _whereClause_mpr = concat(_whereClause_mpr_ffr, _whereClause_mpr);
    end if;	
		
		set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f')); 
	/*	CALL `trams_log`(@sid, 'trams_get_mpr_ffr_review_report', concat('Get Query: ',@applicationQuery), 1); */

		/*set @unionMPRFFRCount=(select count(*) from tmp_trams_mpr_ffr_review);*/
	/*	CALL `trams_log`(@sid, 'trams_get_mpr_ffr_review_report', concat('Total MPR FFR Report Count: ',@unionMPRFFRCount), 1); */
	
	set _sql_final = "";
	if (p_reportType like '%FFR%') then
	    set _sql_final = concat(
        "select 
        FFR.`id` AS `reportSearchResultstempID`,
        `AP`.`contractAward_id` AS `contract_award_ID`,
        `AP`.`id` AS `application_ID`,
        'FFR' AS `report_type`,
        `FFR`.`id` AS `report_ID`,
        `AP`.`application_number` AS `application_number_text`,
        `AP`.`recipient_name`  AS `grantee_name`,
        `FFR`.`report_status` AS `report_status_text`,
        `FFR`.`fiscal_year` AS `fiscal_year_report_text`,
        `FFR`.`fiscal_month` AS `fiscal_month`,
        `FFR`.`fiscal_quarter` AS `fiscal_quarter`,	
		`FFR`.`last_modified_by` as `last_updated_by`,
        `FFR`.`last_modified_date_time` as `last_updated_on`,
        `AP`.`modified_date_time` AS `modified_date_time`,
        `AP`.`recipient_id` AS `recipient_ID_text`,
        CASE WHEN `FFR`.`final_report_flag` = 1 THEN 'Final Report' 
		ELSE `FFR`.`report_period_type` 
		END AS 'report_period_type_text',
		`FFR`.`report_period_type` AS `actual_report_period_type`,
        CASE WHEN `FFR`.`final_report_flag` = 1 THEN 'Final Report' 
		ELSE `FFR`.`report_period_type_concat` 
		END AS 'period'
      from  `Appian`.`trams_application` `AP` join `Appian`.`trams_federal_financial_report` `FFR` 
        on `AP`.`contractAward_id` = `FFR`.`contract_award_id`",_whereClause_ffr);
	end if;
	
	

	
  if (p_reportType like '%MPR%') then
    if (p_reportType like '%FFR%') then 
      set _sql_final = concat(_sql_final, " union all ");
    end if;
    set _sql_final = concat(_sql_final,
        "select 
        `MPR`.`id` AS `reportSearchResultstempID`,
        `AP`.`contractAward_id` AS `contract_award_ID`,
        `AP`.`id` AS `application_ID`,
        'MPR' AS `report_type`,
        `MPR`.`id` AS `report_ID`,
        `AP`.`application_number` AS `application_number_text`,
        `AP`.`recipient_name`  AS `grantee_name`,
        `MPR`.`report_status` AS `report_status_text`,
        `MPR`.`fiscal_year` AS `fiscal_year_report_text`,
        `MPR`.`fiscal_month` AS `fiscal_month`,
        `MPR`.`fiscal_quarter` AS `fiscal_quarter`,
        `MPR`.`updated_by` as `last_updated_by`,
        `MPR`.`updated_date_time` as `last_updated_on`,
        `AP`.`modified_date_time` AS `modified_date_time`,
        `AP`.`recipient_id` AS `recipient_ID_text`,
        CASE WHEN `MPR`.`final_report_flag` = 1 THEN 'Final Report' 
		ELSE `MPR`.`report_period_type` 
		END AS 'report_period_type_text',
		`MPR`.`report_period_type` AS `actual_report_period_type`,
        CASE WHEN `MPR`.`final_report_flag` = 1 THEN 'Final Report' 
		ELSE `MPR`.`report_period_type_concat` 
		END AS 'period'
      from `Appian`.`trams_application` `AP` join `Appian`.`trams_milestone_progress_report` `MPR` 
        on `AP`.`contractAward_id` = `MPR`.`contract_award_id`",_whereClause_mpr);
	end if;

		SET @finalQuery = concat(_sql_final,_limitClauseSingle);

	/*	CALL `trams_log`(@sid, 'trams_get_mpr_ffr_review_report', concat('Get final Query: ',@finalQuery), 1); */

		PREPARE finalStmt FROM @finalQuery;
		EXECUTE finalStmt;

	/*	CALL `trams_log`(@sid, 'trams_get_mpr_ffr_review_report', 'Done!', 1); */
	END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_milestone_progress`(IN `p_report_id` INT)
BEGIN

create temporary table tmp_milestone_progress_status 
  select `mr`.`id` AS `id`,
  `mr`.`milestone_id` AS `milestone_id`,
  `ms`.`line_item_id` AS `line_item_id`,
  `mpr`.`contract_award_id` AS `contract_award_id`,
  `mr`.`report_id` AS `report_id`,
  `mr`.`remark` AS `remark`,`mr`.`remark_by` AS `remark_by`,
  `mr`.`remark_on_date_time` AS `remark_on`,`ms`.`milestone_title` AS `milestone_title`,
  `mr`.`revised_completion_date_time` AS `revised_completion_date`,
  `mr`.`revision_number` AS `revision_number`,`mr`.`actual_completion_date_time` AS `actual_completion_date`,
  `ms`.`estimated_completion_date_time` AS `estimated_completion_date`,
  `trams_get_mpr_status`(`mpr`.`due_date_time`,`ms`.`estimated_completion_date_time`,`mr`.`revised_completion_date_time`,`mr`.`actual_completion_date_time`,`mr`.`revision_number`) AS `mpr_status`,
  (case when isnull(`mr`.`actual_completion_date_time`) then '0' else '1' end) AS `is_final`  
  from ((`trams_milestone_progress_remarks` `mr` 
  join `trams_milestones` `ms` on((`mr`.`milestone_id` = `ms`.`id`))) 
  join `trams_milestone_progress_report` `mpr` on((`mr`.`report_id` = `mpr`.`id`))) 
  where `mpr`.`fiscal_year` > year(now() - interval 10 year) and `mr`.`report_id`=p_report_id 
  and `mpr`.`report_status` = 'Work In Progress';
    
select uuid_short() AS `id`,`p`.`projectnumbertext` AS `projectnumbertext`,`sv`.`report_id` AS `report_id`,`sv`.`contract_award_id` AS `contract_award_id`,
`l`.`id` AS `line_item_id`,`l`.`scope_code` AS `scope_code`,`l`.`scope_detailed_name` AS `scope_detailed_name`,`l`.`item_number` AS `item_number`,
`l`.`custom_item_name` AS `line_Item_Name`,
count(distinct `m`.`id`) AS `num_milestones`,
sum((case when (`sv`.`mpr_status` = 'UPDATED') then 1 else 0 end)) AS `num_mpr_updated`,
sum((case when (`sv`.`mpr_status` = 'PENDING') then 1 else 0 end)) AS `num_mpr_pending`,
sum((case when (`sv`.`mpr_status` = 'ON TRACK') then 1 else 0 end)) AS `num_mpr_ontrack` 
from ((((`line_item` `l` join `application` `a` on((`l`.`app_id` = `a`.`id`))) 
join `project` `p` on((`l`.`project_id` = `p`.`id`))) 
join `milestones` `m` on((`m`.`line_item_id` = `l`.`id`))) 
left join `tmp_milestone_progress_status` `sv` on((`m`.`id` = `sv`.`milestone_id`)))
 where (`a`.`active` = 1) and `sv`.`report_id` > 0 
 group by `sv`.`report_id`,`p`.`projectnumbertext`,`l`.`id`,`l`.`scope_code`,`l`.`scope_detailed_name`,`l`.`item_number`,`l`.`custom_item_name`
order by `l`.`id`;

  drop temporary table if exists tmp_milestone_progress_status;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_milestone_progress_report`(IN `p_reportId` VARCHAR(512),
IN `p_appId` VARCHAR(512))
    NO SQL
BEGIN
 -- sid will be written to the new table as a time stamp to be used for identifying and cleaning up any junk data
  SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
 -- uuid will serve as the unique identifier for rows generated by this specific procedure call. Multiple procedures run at the same time would generate unique uuids
  SET @uuid = UUID_SHORT();

  -- This select statement was previously used to generate/populate the temp table. Now the results of the select are inserted into the new permanent table
 INSERT INTO `trams_temp_milestone_progress_status` (
 SELECT
   NULL AS trams_temp_milestone_progress_status_id,
   mr.id AS remark_id,
   mr.milestone_id AS milestone_id,
   ms.line_item_id AS line_item_id,
   mpr.contract_award_id AS contract_award_id,
   mr.report_id AS report_id,
   mr.revised_completion_date_time AS revised_completion_date,
   mr.revision_number AS revision_number,
   mr.actual_completion_date_time AS actual_completion_date,
   ms.estimated_completion_date_time AS estimated_completion_date,
   trams_get_mpr_status(
     mpr.report_period_end_date,
     ms.estimated_completion_date_time,
     mr.revised_completion_date_time,
     mr.actual_completion_date_time,
     mr.revision_number
  	) AS mpr_status,
   (CASE WHEN isnull(mr.actual_completion_date_time) THEN '0' ELSE '1' END) AS is_final,
   -- the new columns of the table have been added for selection and cleanup purposes
  @uuid AS uuid,
  @sid AS time_stamp
 FROM ((trams_milestone_progress_remarks mr
 JOIN trams_milestones ms ON((mr.milestone_id = ms.id)))
 JOIN trams_milestone_progress_report mpr ON mr.report_id = mpr.id AND mpr.id = p_reportId)
);

 SELECT uuid_short() AS id,
   sv.report_id AS reportID_int,
   sv.contract_award_id AS contractAwardID_int,
   p.project_number AS projectNumber_text,
   l.id AS lineItemID_int,l.scope_code AS scopeCode_text,
   l.scope_detailed_name AS scopeDetailedName_text,
   l.line_item_number AS lineItemNumber_text,
   l.custom_item_name AS lineItemName_text,
   count(distinct m.id) AS numberOfMilestones_int,
   sum((CASE WHEN (sv.mpr_status = 'UPDATED') THEN 1 ELSE 0 END)) AS numberOfUpdatedMilestones_int,
   sum((CASE WHEN (sv.mpr_status = 'PENDING') THEN 1 ELSE 0 END)) AS numberOfPendingMilestones_int,
   sum((CASE WHEN (sv.mpr_status = 'ON TRACK') THEN 1 ELSE 0 END)) AS numberOfOnTrackMilestones_int
 FROM trams_line_item l JOIN trams_application a ON l.app_id = a.id AND a.id = p_appId
   JOIN trams_project p ON l.project_id = p.id
   JOIN trams_milestones m ON m.line_item_id = l.id
   -- this join has been updated to query the new table for the appropriate rows
   JOIN trams_temp_milestone_progress_status sv ON m.id = sv.milestone_id AND sv.uuid = @uuid
 GROUP BY sv.report_id,l.project_id,l.id,l.scope_code,l.scope_detailed_name,l.line_item_number,l.custom_item_name ORDER BY l.id;

 -- the rows generated for the procedure are deleted to replicate the effects of a temporary table
 DELETE FROM trams_temp_milestone_progress_status WHERE uuid = @uuid;

 END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_milestone_progress_report_excel`(
	IN p_granteeId VARCHAR(512),
	IN p_costCenterCode VARCHAR(512),
	IN p_appNumber VARCHAR(512),
	IN p_reportStatus VARCHAR(1024),
	IN p_reportFrequency VARCHAR(512), 
    IN p_postAwardManager VARCHAR(1024), 
    IN p_reportFiscalYear VARCHAR(20), 
    IN p_reportFiscalQuarter VARCHAR(20),
	IN p_reportFiscalMonth VARCHAR(20),
	IN p_reportFinalFlag TINYINT(1),
    IN p_reportFundingSource VARCHAR(10),
    IN p_gmtOffset VARCHAR(512),
    IN p_return_limit VARCHAR(50)
)
BEGIN
    DECLARE _sql TEXT default "";
    DECLARE _whereClause varchar(4096) default "";
    DECLARE _spName varchar(255) default "trams_get_milestone_progress_report_excel";
    SET @sid = (SELECT date_format(now(), '%Y%m%d%H%i%s%f'));

    CALL `trams_log`(@sid, _spName, CONCAT(
        now(),
        ' CALL SP Inputs: ',
        IFNULL(`p_granteeId`,'NULL'), ',',
        IFNULL(`p_costCenterCode` ,'NULL'), ',',
        IFNULL(`p_appNumber` ,'NULL'),',',
        IFNULL(`p_reportStatus` ,'NULL'),',',
        IFNULL(`p_reportFrequency` ,'NULL'),',',
        IFNULL(`p_postAwardManager` ,'NULL'),',',
        IFNULL(`p_reportFiscalYear` ,'NULL'),',',
        IFNULL(`p_reportFiscalQuarter` ,'NULL'),',',
        IFNULL(`p_reportFiscalMonth` ,'NULL'),',',
		IFNULL(`p_reportFinalFlag` ,'NULL'),',',
		-- added now parameter for funding source filter
		IFNULL(`p_reportFundingSource` ,'NULL'),',',
        IFNULL(`p_gmtOffset` , 'NULL'), ',',
        IFNULL(`p_return_limit` ,'NULL'),','
    ), 1);

    -- construct the where clause for the input parameters
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_granteeId), " ", CONCAT(" AND mpr.RecipientId IN (", p_granteeId, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_costCenterCode), " ", CONCAT(" AND mpr.RecipientCostCenter IN (", p_costCenterCode, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_appNumber), " ", CONCAT(" AND mpr.AppNumber LIKE '%", p_appNumber, "%'")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportStatus), " ", CONCAT(" AND mpr.ReportStatus IN (", p_reportStatus, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFrequency), " ", CONCAT(" AND mpr.ReportPeriodType IN (", p_reportFrequency, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_postAwardManager), " ", CONCAT(" AND mpr.AppPostAwardManager IN (", p_postAwardManager, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalYear), " ", CONCAT(" AND mpr.ReportFiscalYear IN (", p_reportFiscalYear, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalQuarter), " ", CONCAT(" AND mpr.ReportFiscalQuarter IN (", p_reportFiscalQuarter, ")")));
    SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFiscalMonth), " ", CONCAT(" AND mpr.ReportFiscalMonth IN (", p_reportFiscalMonth, ")")));
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFinalFlag), " ", CONCAT(" AND mpr.ReportFinalFlag = (", p_reportFinalFlag, ")")));
	-- added new where clause for funding source filter
	SET _whereClause = CONCAT(_whereClause, IF(ISNULL(p_reportFundingSource), " ", CONCAT(" AND mpr.FundingSourceName IN (", p_reportFundingSource, ")")));

    -- now selects all required data from the view
    SET _sql = CONCAT("
SELECT
    mpr.RecipientCostCenter,
    mpr.RecipientId,
    mpr.RecipientLegalBusinessName,
    mpr.AppNumber,
    mpr.AppType,
    mpr.AppPostAwardManager,
    mpr.FundingSourceName,
    mpr.ReportPeriodType,
    mpr.ReportPeriodTypeConcat,
    mpr.ReportDueDate,
    trams_format_date(cast(CONVERT_TZ(mpr.ReportSubmittedDateTime, '+00:00', ", p_gmtOffset, ") AS char)) AS `ReportSubmittedDateTime`,
    mpr.ReportStatus,
    mpr.LineItemNumber,
    mpr.LineItemActivityName,
    mpr.LineItemQuantity,
    mpr.LineItemRevisedFTAAmount,
    mpr.MilestoneTitle,
    mpr.MilestoneEstimatedCompletionDate,
    mpr.RemarkRevisedCompletionDate,
    mpr.RemarkActualCompletionDate,
    mpr.AppEndDate,
    mpr.EstimatedRevisedDateDelta,
    mpr.EstimatedActualDateDelta
FROM `vwtramsMilestoneProgressReportDetail` mpr
WHERE 1 ");

    -- add the where clause and limit to the select statement
    SET @query = CONCAT(_sql, _whereClause, IFNULL(CONCAT(" LIMIT ", NULLIF(p_return_limit, '')), ""));

    -- excecute the statement
    PREPARE stmt FROM @query;
    EXECUTE stmt;

CALL `trams_log`(@sid, _spName, CONCAT( now(), 'Done!'), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_project_budget_report_HELPDESK`(IN `p_granteeId` VARCHAR(512), IN `p_costCenterCode` VARCHAR(512), IN `p_fiscalYear` VARCHAR(512), IN `p_appNumber` VARCHAR(512), IN `p_appStatus` VARCHAR(1024), IN `p_appType` VARCHAR(512), IN `p_preAwardManager` VARCHAR(1024), IN `p_postAwardManager` VARCHAR(1024), IN `p_projectNumber` VARCHAR(1024), IN `p_sectionCode` VARCHAR(512), IN `p_return_limit` VARCHAR(20))
    NO SQL
BEGIN

declare _sql varchar(40960) default "";
declare _whereClause varchar(4096) default " ";
declare _limitClause varchar(100) default " ";
declare _spName varchar(255) default "trams_get_project_budget_report_HELPDESK"; 
call trams_get_sql_limit_clause(p_return_limit,_limitClause);

drop temporary table if exists tmp_application_id;
drop temporary table if exists tmp_scope_acc_rollup;
drop temporary table if exists tmp_contract_acc_transaction_rollup;
drop temporary table if exists tmp_contract_acc_transaction_rollup2;
drop temporary table if exists tmp_line_item_rollup;    

set _sql = "create temporary table tmp_application_id    
select 
a.id as application_id,
p.id as project_id,
a.contractAward_id
from 
trams_application a 
join 
trams_project p 
on 
a.id=p.application_id
where 1 
          ";

set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.recipient_id in (",NULLIF(p_granteeId,''),")"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.recipient_cost_center in (",NULLIF(p_costCenterCode,''),")"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fiscal_year in (",NULLIF(p_fiscalYear,''),")"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.application_number in ('",NULLIF(p_appNumber,''),"')"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.status in (",NULLIF(p_appStatus,''),")"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.application_type in ('",NULLIF(p_appType,''),"')"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fta_pre_award_manager in ('",NULLIF(p_preAwardManager,''),"')"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fta_post_award_manager in ('",NULLIF(p_postAwardManager,''),"')"),"")));
set _whereClause = concat(_whereClause, (select ifnull(concat(" and p.project_number in ('",NULLIF(p_projectNumber,''),"')"),"")));

SET @applicationQuery = concat(_sql,_whereClause,_limitClause); 

set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
CALL `trams_log`(@sid, _spName, concat('Start CALL SP Inputs: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_projectNumber` ,'NULL'),',',IFNULL(  `p_sectionCode` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1); 
CALL `trams_log`(@sid, _spName, concat('Get Application & Project Query: ',@applicationQuery), 1); 

PREPARE applicationStmt FROM @applicationQuery;
EXECUTE applicationStmt;




create index tmp_application_id_appid on tmp_application_id(application_id);
create index tmp_application_id_projid on tmp_application_id(project_id);

set @appCount=(select count(*) from tmp_application_id);
CALL `trams_log`(@sid, _spName, concat('Total Application: ',@appCount), 1); 


set @scopeAccRollupQuery = "
create temporary table tmp_scope_acc_rollup engine=innodb    
select 
project_id AS projectId_int,
acc_section_code as section_code,
sum(acc_reservation_amount) AS total_reservation_amt 
from 
trams_scope_acc 
where 
project_id in (select project_id from tmp_application_id) 
group by 
project_id, 
funding_source_name ";

CALL `trams_log`(@sid, _spName, concat('Get scope_acc_rollup Query: ',@scopeAccRollupQuery), 1);

PREPARE scopeAccRollupStmt FROM @scopeAccRollupQuery;
EXECUTE scopeAccRollupStmt;

create index tmp_scope_acc_rollup_projectId_int on tmp_scope_acc_rollup(projectId_int);
create index tmp_scope_acc_rollup_section_code on tmp_scope_acc_rollup(section_code);

set @cnt=(select count(*) from tmp_scope_acc_rollup);
CALL `trams_log`(@sid, _spName, concat('Total rollup scope_acc_transaction: ',@cnt), 1); 


set @contractAccTransactionRollupQuery = concat("
create temporary table tmp_contract_acc_transaction_rollup engine=memory    
select 
 trans.project_id AS project_id,
 trans.section_code AS section_code,
 trans.project_number as project_number,
 LEFT(trans.project_number, length(trans.project_number) - 2) as proj_num_no_amend,
 sum(trans.obligation_amount) AS total_obligation_amt,
 sum(trans.deobligation_amount) AS total_deobligation_amt,
 sum(trans.disbursement_amount) AS total_disbursement_amt
from trams_contract_acc_transaction trans 
join trams_funding_source_reference f on trans.funding_source_name = f.funding_source_name
where project_id in (select project_id from tmp_application_id) ",
(select ifnull(concat(" and trans.section_code in (",NULLIF(p_sectionCode,''),")"),"")),
" group by trans.project_number, trans.funding_source_name, trans.section_code");

CALL `trams_log`(@sid, _spName, concat('Get contract_acc_transaction rollup Query: ',@contractAccTransactionRollupQuery), 1); 

PREPARE contractAccTransactionRollupStmt FROM @contractAccTransactionRollupQuery;
EXECUTE contractAccTransactionRollupStmt;

create index tmp_contract_acc_transaction_rollup_section_code on tmp_contract_acc_transaction_rollup(section_code);
create index tmp_contract_acc_transaction_rollup_proj_num_no_amend on tmp_contract_acc_transaction_rollup(proj_num_no_amend);


set @contractAccTransactionRollupQuery = "
create temporary table tmp_contract_acc_transaction_rollup2 engine=memory    
select 
 *
from tmp_contract_acc_transaction_rollup";
PREPARE contractAccTransactionRollupStmt FROM @contractAccTransactionRollupQuery;
EXECUTE contractAccTransactionRollupStmt;

create index tmp_contract_acc_transaction_rollup2_section_code on tmp_contract_acc_transaction_rollup2(section_code);
create index tmp_contract_acc_transaction_rollup2_proj_num_no_amend on tmp_contract_acc_transaction_rollup2(proj_num_no_amend);
create index tmp_contract_acc_transaction_rollup2_project_id on tmp_contract_acc_transaction_rollup2(project_id);


set @cnt=(select count(*) from tmp_contract_acc_transaction_rollup);
CALL `trams_log`(@sid, _spName, concat('Total rollup contract_acc_transaction: ',@cnt), 1); 


set @lineItemRollupQuery = "
create temporary table tmp_line_item_rollup engine=innodb     
select 
li.app_id AS app_id,
li.project_id AS project_id,
li.section_code AS section_code,
sum(li.revised_FTA_amount) AS total_project_fta_amt,
(sum(li.revised_total_amount) - sum(revised_FTA_amount)) AS total_project_non_fta_amt,
sum(li.revised_total_amount) AS total_project_eligible_cost,
p.project_number
from 
trams_line_item li
join
trams_project p
on
li.project_id = p.id
where 
project_id in (select project_id from tmp_application_id)  
group by 
project_id, 
app_id, 
section_code";

CALL `trams_log`(@sid, _spName, concat('Get line_item Rollup Query: ',@lineItemRollupQuery), 1); 

PREPARE lineItemRollupStmt FROM @lineItemRollupQuery;
EXECUTE lineItemRollupStmt;

create index tmp_line_item_rollup_project_id on tmp_line_item_rollup(project_id);
create index tmp_line_item_rollup_section_code on tmp_line_item_rollup(section_code);

set @cnt=(select count(*) from tmp_line_item_rollup);
CALL `trams_log`(@sid, _spName, concat('Total rollup line_item: ',@cnt), 1); 

drop  temporary table if exists tmp_max_project;
create   temporary table tmp_max_project
select 
   tcatr.project_id,
      tcatr.section_code,
    max(tcatr.project_number) AS max_project_number,
       tcatr.proj_num_no_amend,
      tcatr.total_obligation_amt,
      tcatr.total_deobligation_amt,
      tcatr.total_disbursement_amt
  from tmp_contract_acc_transaction_rollup tcatr
  group by tcatr.section_code, tcatr.proj_num_no_amend;

create index tmp_max_project__max_project_number on tmp_max_project(max_project_number);

drop temporary table if exists tmp_line_item_totals;
create temporary table tmp_line_item_totals
select
     max(tcatr2.project_number) AS project_number,
      tcatr2.proj_num_no_amend AS proj_num_no_amend,
     tcatr2.section_code AS section_code,
     sum(tlir.total_project_fta_amt) AS total_project_fta_amt,
     sum(tlir.total_project_non_fta_amt) AS total_project_non_fta_amt,
     sum(tlir.total_project_eligible_cost) AS total_project_eligible_cost,
     sum(tsar.total_reservation_amt) AS total_reservation_amt,
     sum(tcatr2.total_obligation_amt) AS total_obligation_amt,
     sum(tcatr2.total_deobligation_amt) AS total_deobligation_amt,
     sum(tcatr2.total_disbursement_amt) AS total_disbursement_amt
   from tmp_contract_acc_transaction_rollup2 tcatr2
   join tmp_scope_acc_rollup tsar on tcatr2.project_id = tsar.projectId_int and tsar.section_code = tcatr2.section_code
   join tmp_line_item_rollup tlir on tcatr2.project_id = tlir.project_id and tsar.section_code = tlir.section_code
   group by tcatr2.section_code, tcatr2.proj_num_no_amend;
   
 create index tmp_line_item_totals_project_number on tmp_line_item_totals(project_number);
 create index tmp_line_item_totals_section_code on tmp_line_item_totals(section_code);

drop temporary table if exists tmp_resultset;

drop temporary table if exists tmp_congressional_name_and_id;
create temporary table tmp_congressional_name_and_id    
select 
trmsprject_prjectgegrphy_id AS "Project Id",
GROUP_CONCAT(CONCAT(congressional_id,"-", congressional_representative_name) separator "\n") AS "Congressional Representative"
from 
trams_project_geography g
where 
trmsprject_prjectgegrphy_id in (select project_id from tmp_application_id) 
group by 
trmsprject_prjectgegrphy_id;

drop temporary table if exists tmp_uza_name_and_code;
create temporary table tmp_uza_name_and_code  
select 
trmsprject_prjectgegrphy_id AS "Project Id", 
GROUP_CONCAT(CONCAT(g.uza_code,"-", r.uza_name) separator "\n") AS "UZA"
from 
trams_project_geography g
join trams_uza_code_reference r ON (r.uza_code=g.uza_code)
where 
trmsprject_prjectgegrphy_id in (select project_id from tmp_application_id) AND r.version=8
group by 
trmsprject_prjectgegrphy_id;

set @finalQuery = concat(   
"
create temporary table tmp_resultset
select 
 a.recipient_id AS grantee_id,
 gb.recipient_acronym AS acronym,
 gb.legal_business_name AS grantee_name,
 a.recipient_cost_center AS grantee_cost_center,
 a.fiscal_year AS fiscal_year,
 a.application_number AS application_number,
 a.amendment_number AS amendment_number,
 a.application_name AS application_name,
 a.`status` AS `status`,
 a.application_type AS application_type,
 fr.transmitted_date AS transmitted_date1,
 fr.submitted_date AS submitted_date,
 a.recipient_contact AS grantee_poc,    
 a.fta_pre_award_manager AS pre_award_mgr,
 a.fta_post_award_manager AS post_award_mgr,
 p.project_number AS project_number,
 p.project_title AS project_name,  
 max_project.section_code AS section_code,
 totals.total_project_fta_amt,
 totals.total_project_non_fta_amt,
 totals.total_project_eligible_cost,
 totals.total_reservation_amt,
 totals.total_obligation_amt,
 totals.total_deobligation_amt,
 totals.total_disbursement_amt,
 a.discretionary_funds_flag AS discretionary_funds_bool,
 p.major_capital_project_flag AS major_cap_project_bool,
 DATE(aa.obligated_date_time) AS obligated_date_time,
 ( SELECT MIN(tms.estimated_completion_date_time) FROM trams_milestones tms WHERE tms.line_item_id = tli.id  ) AS Project_Start_Date ,
( SELECT MAX(tms.estimated_completion_date_time) FROM trams_milestones tms WHERE tms.line_item_id = tli.id  ) AS Project_End_Date,
c.`Congressional Representative` AS congressional_representative_name,
u.`UZA` AS uza
from trams_project p
 join tmp_max_project max_project
      on p.project_number = max_project.max_project_number
 join trams_application a on a.id = p.application_id
 join trams_general_business_info gb on a.recipient_id = gb.recipient_id
 left join trams_fta_review fr on a.id = fr.application_id
 left join trams_application_award_detail aa on a.application_number = aa.application_number 
join tmp_line_item_totals totals
  on max_project.max_project_number = totals.project_number
  and totals.section_code = max_project.section_code 
left join trams_line_item tli on tli.project_id = p.id
join tmp_congressional_name_and_id c on c.`Project Id`=p.id
join tmp_uza_name_and_code u on u.`Project Id`=p.id
where p.id in (select project_id from tmp_application_id)"
,(select ifnull(concat(" and max_project.section_code in (",NULLIF(p_sectionCode,''),")"),"")), " order by a.recipient_id");

CALL `trams_log`(@sid, _spName, concat('Get Final Query: ',@finalQuery), 1); 

PREPARE finalQueryStmt FROM @finalQuery;
EXECUTE finalQueryStmt;

drop temporary table if exists tmp_no_contract_award_line_item_rollup;
create temporary table tmp_no_contract_award_line_item_rollup
select 
li.scope_code,
li.app_id AS app_id,
li.project_id AS project_id,
li.section_code AS section_code,
li.funding_source_name,
max(p.project_number) as project_number,
sum(li.revised_FTA_amount) AS total_project_fta_amt,
(sum(li.revised_total_amount) - sum(li.revised_FTA_amount)) AS total_project_non_fta_amt,
sum(li.revised_total_amount) AS total_project_eligible_amt 
from 
trams_line_item li
join
trams_project p
on 
li.project_id = p.id
where 
project_id in (select project_id from tmp_application_id where contractAward_id is null or contractAward_id = '') 
group by 
project_number;

create index tmp_no_contract_award_line_item_rollup_project_number on tmp_no_contract_award_line_item_rollup(project_number);


drop temporary table if exists tmp_no_contract_award_scope_acc_rollup;
create temporary table tmp_no_contract_award_scope_acc_rollup     
select 
scope_name,
scope_number,
project_number,
project_id as projectId_int,
acc_section_code as section_code,
sum(acc_reservation_amount) AS total_acc_reservation_amt 
from 
trams_scope_acc sa
where 
project_id in (select project_id from tmp_application_id) 
group by 
project_number;

create index tmp_no_contract_award_scope_acc_rollup_project_number on tmp_no_contract_award_scope_acc_rollup(project_number);

SET @appCount=(SELECT COUNT(*) FROM tmp_resultset  );
CALL `trams_log`(@sid, _spName, CONCAT('Total Matched Transactions: ',@appCount), 1);   

set @finalQuery = concat(   
"
insert into tmp_resultset
select 
a.recipient_id AS grantee_id,
gb.recipient_acronym AS acronym,
gb.legal_business_name AS grantee_name,
a.recipient_cost_center AS grantee_cost_center,
a.fiscal_year AS fiscal_year,
a.application_number AS application_number,
a.amendment_number AS amendment_number,
a.application_name AS application_name,
a.`status` AS `status`,
a.application_type AS application_type,
fr.transmitted_date AS transmitted_date1,
fr.submitted_date AS submitted_date,
a.recipient_contact AS grantee_poc, 
a.fta_pre_award_manager AS pre_award_mgr,
a.fta_post_award_manager AS post_award_mgr,
p.project_number AS project_number,
p.project_title AS project_name,  
lir.section_code as section_code,
lir.total_project_fta_amt as total_project_fta_amt,
lir.total_project_non_fta_amt as total_project_non_fta_amt,
lir.total_project_eligible_amt as total_project_eligible_amt,
sar.total_acc_reservation_amt as total_acc_reservation_amt,
'' as total_obligation_amt,
'' as total_deobligation_amt,
'' as total_disbursement_amt,
a.discretionary_funds_flag AS discretionary_funds_bool,
p.major_capital_project_flag AS major_cap_project_bool,
DATE(aa.obligated_date_time) AS obligated_date_time,
( SELECT MIN(tms.estimated_completion_date_time) FROM trams_milestones tms WHERE tms.line_item_id = tli.id  ) AS Project_Start_Date ,
( SELECT MAX(tms.estimated_completion_date_time) FROM trams_milestones tms WHERE tms.line_item_id = tli.id  ) AS Project_End_Date,
c.`Congressional Representative` AS congressional_representative_name,
u.`UZA` AS uza
from 
trams_project p
join 
trams_application a on a.id = p.application_id
join 
trams_general_business_info gb on a.recipient_id = gb.recipient_id
left join 
trams_fta_review fr on a.id = fr.application_id
left join trams_application_award_detail aa on a.application_number = aa.application_number 
left join
tmp_no_contract_award_line_item_rollup lir
on
p.project_number = lir.project_number
left join
tmp_no_contract_award_scope_acc_rollup sar
on
p.project_number = sar.project_number
left join trams_line_item tli on tli.project_id = p.id
join tmp_congressional_name_and_id c on c.`Project Id`=p.id
join tmp_uza_name_and_code u on u.`Project Id`=p.id
where 
p.id in (select project_id from tmp_application_id) and
(a.contractAward_Id is null or contractAward_Id = '')", (select ifnull(concat(" and lir.section_code in ('",NULLIF(p_sectioncode,''),"')"),""))); 

CALL `trams_log`(@sid, _spName, concat('Perform Insert SQL: ', now(), ' ',@finalQuery), 1); 

PREPARE finalQueryStmt FROM @finalQuery;
EXECUTE finalQueryStmt;

SET @appCount=(SELECT COUNT(*) FROM tmp_resultset  );

CALL `trams_log`(@sid, _spName, concat('Total Applications: ', now(), ' ',@appCount), 1); 

CALL `trams_log`(@sid, _spName, concat('Final Selection Starting ', now()), 1); 

drop temporary table if exists tmp_project_date_rollup;
create temporary table tmp_project_date_rollup
SELECT project_number, MIN(project_Start_Date) AS MinDate, MAX(Project_End_Date) AS MaxDate
       FROM tmp_resultset
       GROUP BY project_number;

create index tmp_project_date_rollup_project_number on tmp_project_date_rollup(project_number);

drop table if exists tmp_final_results;
create table tmp_final_results(
grantee_id INT(11),
acronym VARCHAR(512),
grantee_name VARCHAR(512),
grantee_cost_center VARCHAR(512),
fiscal_year VARCHAR(512),
application_number VARCHAR(512),
amendment_number VARCHAR(512),
application_name VARCHAR(512),
status VARCHAR(512),
application_type VARCHAR(512),
transmitted_date1 VARCHAR(512),
submitted_date VARCHAR(512),
grantee_poc VARCHAR(512),
pre_award_mgr VARCHAR(512),
post_award_mgr VARCHAR(512),
project_number VARCHAR(512),
project_name VARCHAR(512),
section_code INT(11),
total_project_fta_amt DECIMAL,
total_project_non_fta_amt DECIMAL,
total_project_eligible_cost DECIMAL,
total_reservation_amt DECIMAL,
total_obligation_amt DECIMAL,
total_deobligation_amt DECIMAL,
total_disbursement_amt DECIMAL,
discretionary_funds_bool TINYINT(1),
major_cap_project_bool TINYINT(1),
obligated_date_time VARCHAR(512),
Project_Start_Date VARCHAR(512),
Project_End_Date VARCHAR(512),
congressional_representative_name VARCHAR(512),
uza VARCHAR(512)
);

insert into tmp_final_results
SELECT tmp.grantee_id, 
tmp.acronym, 
tmp.grantee_name, 
tmp.grantee_cost_center,
tmp.fiscal_year,
tmp.application_number,
tmp.amendment_number,
tmp.application_name,
tmp.status,
tmp.application_type,
trams_format_date(tmp.transmitted_date1) AS transmitted_date1,
trams_format_date(tmp.submitted_date) AS submitted_date,
tmp.grantee_poc, 
tmp.pre_award_mgr,
tmp.post_award_mgr,
tmp.project_number,
tmp.project_name,  
tmp.section_code,
tmp.total_project_fta_amt,
tmp.total_project_non_fta_amt,
tmp.total_project_eligible_cost,
tmp.total_reservation_amt,
tmp.total_obligation_amt,
tmp.total_deobligation_amt,
tmp.total_disbursement_amt,
tmp.discretionary_funds_bool,
tmp.major_cap_project_bool,
trams_format_date(tmp.obligated_date_time) AS obligated_date_time,
trams_format_date(t.minDate) AS Project_Start_Date,
trams_format_date(t.MaxDate) AS Project_End_Date,
tmp.congressional_representative_name,
tmp.uza
FROM tmp_resultset tmp INNER JOIN 
tmp_project_date_rollup t ON tmp.project_number = t.project_number
GROUP BY project_number;

CALL `trams_log`(@sid, _spName, concat('Done! ', now()), 1); 

drop temporary table if exists tmp_application_id;
drop temporary table if exists tmp_scope_acc_rollup;
drop temporary table if exists tmp_contract_acc_transaction_rollup;
drop temporary table if exists tmp_contract_acc_transaction_rollup2;
drop temporary table if exists tmp_line_item_rollup;
drop table if exists tmp_resultset;     

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_otrak_monthly_report`(IN `p_generate_date` VARCHAR(20170622))
BEGIN


create temporary table trams_total_obligation_by_grantee engine=memory

 select acc.recipient_id,
 sum(acc.obligation_amount) as total_obligation_amount 
 from trams_contract_acc_transaction acc
 inner join trams_contract_award ca 
 on ca.id = acc.contract_award_id 
 and ca.recipient_id = acc.recipient_id
 and ca.active = 1 
 and date(acc.transaction_date_time) < p_generate_date
 group by acc.recipient_id;
 
create temporary table trams_grantee_start_month_temp 
(
grantee_id int(11) null,fy_start_month int(11) null
) ;


INSERT INTO trams_grantee_start_month_temp VALUES (1000,10),(1001,7),(1002,7),(1003,7),(1004,7),(1005,7),(1006,7),(1007,9),(1009,7),(1011,7),(1013,10),(1014,10),(1015,10),(1016,10),(1018,10),(1019,10),(1020,10),(1021,7),(1022,7),(1023,7),(1024,10),(1025,7),(1026,7),(1027,7),(1028,7),(1029,10),(1030,7),(1031,7),(1032,10),(1033,7),(1034,10),(1035,10),(1036,7),(1037,7),(1038,7),(1039,7),(1040,7),(1041,7),(1042,7),(1043,7),(1045,7),(1046,7),(1047,7),(1048,7),(1049,7),(1050,7),(1051,7),(1052,7),(1053,7),(1054,7),(1056,7),(1057,7),(1058,7),(1059,1),(1060,7),(1061,7),(1062,7),(1063,7),(1064,7),(1065,7),(1066,7),(1067,7),(1068,7),(1069,7),(1070,7),(1072,7),(1073,10),(1075,10),(1076,10),(1078,10),(1079,10),(1080,10),(1081,10),(1082,10),(1083,10),(1084,10),(1085,10),(1086,10),(1087,7),(1089,10),(1090,7),(1091,10),(1092,10),(1093,7),(1094,10),(1095,10),(1096,10),(1097,7),(1098,10),(1099,7),(1101,7),(1103,7),(1104,7),(1105,7),(1106,7),(1107,10),(1108,10),(1109,10),(1110,7),(1111,7),(1112,3),(1113,1),(1114,7),(1115,10),(1117,7),(1118,10),(1119,7),(1120,7),(1121,7),(1122,7),(1123,7),(1124,7),(1125,7),(1126,1),(1127,7),(1128,10),(1130,1),(1131,11),(1132,1),(1133,10),(1134,10),(1135,10),(1136,1),(1137,1),(1138,1),(1139,1),(1140,1),(1142,1),(1143,1),(1144,10),(1145,10),(1146,10),(1147,10),(1148,10),(1149,7),(1150,7),(1151,7),(1152,10),(1153,7),(1154,2),(1155,1),(1156,1),(1157,1),(1158,7),(1159,1),(1160,1),(1161,1),(1162,1),(1163,1),(1164,10),(1165,7),(1166,7),(1167,10),(1168,7),(1169,7),(1171,7),(1172,1),(1173,1),(1174,1),(1175,1),(1176,1),(1177,7),(1178,1),(1179,7),(1180,12),(1181,7),(1182,1),(1185,1),(1186,7),(1187,1),(1188,7),(1189,1),(1190,1),(1191,1),(1192,1),(1193,1),(1194,1),(1195,1),(1196,1),(1197,1),(1198,1),(1200,1),(1201,1),(1202,1),(1203,1),(1204,1),(1207,10),(1208,10),(1209,9),(1210,1),(1211,1),(1212,10),(1213,1),(1214,1),(1215,10),(1216,10),(1217,10),(1218,10),(1220,10),(1221,1),(1222,1),(1223,10),(1224,1),(1225,7),(1226,1),(1227,1),(1228,1),(1229,1),(1230,1),(1231,1),(1232,1),(1233,1),(1234,1),(1235,1),(1236,1),(1237,1),(1238,1),(1239,1),(1240,1),(1241,1),(1242,1),(1243,7),(1244,1),(1245,7),(1247,1),(1252,1),(1256,1),(1258,1),(1259,1),(1260,1),(1261,1),(1262,1),(1264,1),(1266,1),(1268,1),(1271,1),(1272,1),(1273,12),(1274,7),(1275,1),(1276,7),(1277,7),(1278,1),(1279,1),(1281,1),(1282,1),(1284,1),(1285,1),(1286,1),(1287,1),(1288,1),(1289,1),(1291,1),(1292,1),(1293,7),(1294,1),(1295,1),(1296,1),(1297,1),(1298,1),(1299,1),(1300,1),(1301,1),(1303,1),(1304,1),(1305,1),(1306,1),(1307,1),(1308,7),(1309,1),(1310,7),(1311,1),(1312,1),(1313,1),(1314,1),(1315,1),(1316,7),(1317,1),(1318,1),(1319,1),(1321,1),(1322,1),(1323,1),(1324,1),(1325,1),(1326,8),(1329,7),(1330,7),(1331,7),(1332,7),(1333,1),(1334,7),(1335,7),(1336,7),(1337,7),(1339,7),(1340,7),(1342,1),(1344,3),(1345,7),(1346,7),(1347,7),(1348,1),(1349,7),(1350,1),(1351,7),(1352,7),(1353,7),(1354,10),(1355,7),(1356,1),(1357,7),(1358,7),(1359,1),(1362,7),(1363,7),(1364,7),(1365,7),(1366,7),(1368,7),(1369,7),(1370,7),(1371,7),(1372,7),(1373,7),(1374,7),(1375,7),(1376,7),(1378,7),(1379,7),(1380,7),(1381,7),(1382,1),(1383,7),(1384,7),(1385,7),(1386,7),(1387,7),(1388,7),(1391,1),(1392,9),(1393,7),(1394,7),(1395,7),(1396,7),(1397,10),(1398,7),(1399,7),(1400,7),(1401,7),(1402,7),(1403,7),(1404,7),(1405,7),(1406,7),(1407,1),(1409,1),(1410,7),(1412,7),(1413,1),(1414,7),(1415,7),(1416,7),(1417,7),(1418,7),(1419,7),(1420,7),(1421,7),(1422,7),(1423,1),(1424,7),(1425,7),(1426,7),(1427,7),(1428,7),(1429,7),(1430,1),(1431,7),(1432,7),(1433,7),(1434,7),(1435,1),(1436,7),(1437,7),(1438,7),(1439,1),(1441,7),(1442,7),(1443,7),(1444,7),(1445,7),(1446,7),(1447,7),(1448,7),(1449,10),(1451,7),(1452,7),(1453,7),(1454,7),(1455,7),(1456,7),(1457,7),(1458,7),(1459,7),(1460,7),(1461,1),(1462,7),(1464,7),(1465,7),(1466,7),(1467,7),(1468,1),(1469,7),(1470,10),(1471,1),(1472,7),(1473,7),(1474,7),(1475,7),(1476,7),(1477,7),(1478,7),(1479,7),(1480,12),(1481,7),(1482,7),(1483,7),(1484,7),(1485,7),(1487,10),(1488,1),(1489,7),(1491,7),(1492,7),(1493,7),(1495,10),(1496,7),(1498,7),(1500,7),(1501,7),(1503,12),(1504,1),(1505,1),(1506,10),(1507,7),(1508,5),(1509,1),(1510,1),(1512,10),(1513,5),(1514,1),(1518,7),(1519,10),(1520,1),(1521,1),(1523,7),(1524,7),(1525,7),(1526,10),(1527,7),(1528,10),(1530,10),(1531,10),(1532,10),(1535,1),(1537,1),(1538,10),(1539,9),(1540,10),(1541,10),(1542,1),(1543,1),(1545,1),(1546,1),(1547,10),(1549,1),(1551,10),(1552,10),(1553,10),(1554,10),(1556,10),(1558,10),(1560,10),(1561,10),(1562,7),(1564,10),(1565,1),(1566,10),(1567,9),(1569,6),(1570,10),(1571,6),(1572,1),(1573,1),(1574,1),(1575,7),(1576,7),(1577,7),(1578,1),(1579,7),(1580,7),(1584,1),(1585,7),(1586,7),(1588,10),(1589,1),(1590,7),(1591,10),(1592,1),(1593,1),(1594,5),(1595,5),(1596,10),(1597,1),(1598,1),(1601,1),(1602,1),(1604,1),(1605,10),(1607,1),(1609,1),(1610,10),(1612,10),(1613,7),(1615,7),(1618,7),(1620,7),(1621,10),(1622,7),(1623,7),(1625,10),(1626,1),(1627,7),(1628,10),(1629,7),(1630,10),(1632,7),(1633,7),(1634,7),(1635,7),(1638,7),(1640,10),(1641,7),(1642,10),(1643,7),(1644,7),(1646,10),(1647,7),(1648,7),(1649,7),(1650,7),(1651,7),(1652,7),(1653,7),(1655,7),(1656,7),(1657,7),(1658,7),(1659,7),(1663,7),(1664,7),(1665,7),(1666,10),(1667,7),(1668,7),(1669,7),(1670,7),(1671,7),(1673,7),(1674,7),(1675,7),(1676,1),(1677,7),(1678,7),(1679,7),(1680,10),(1681,7),(1682,7),(1683,7),(1684,1),(1685,7),(1686,7),(1688,7),(1690,7),(1691,7),(1695,7),(1697,7),(1701,7),(1703,7),(1704,10),(1705,7),(1706,7),(1707,1),(1708,6),(1709,6),(1710,8),(1711,7),(1712,8),(1713,8),(1714,1),(1715,1),(1716,1),(1717,7),(1718,1),(1719,1),(1720,1),(1721,1),(1722,7),(1723,7),(1724,7),(1725,7),(1726,7),(1728,7),(1729,1),(1730,1),(1731,1),(1732,7),(1733,1),(1734,7),(1735,7),(1736,10),(1737,6),(1738,7),(1739,7),(1740,7),(1741,7),(1742,1),(1743,1),(1744,1),(1745,1),(1746,1),(1747,1),(1748,1),(1749,1),(1750,1),(1751,7),(1752,7),(1754,7),(1755,7),(1758,7),(1759,6),(1760,7),(1761,7),(1762,7),(1763,7),(1764,7),(1765,7),(1767,7),(1769,7),(1771,7),(1772,7),(1773,7),(1774,1),(1776,4),(1777,4),(1778,4),(1779,4),(1780,4),(1781,1),(1782,7),(1783,4),(1784,4),(1785,4),(1786,1),(1787,4),(1788,7),(1789,7),(1791,4),(1792,4),(1793,4),(1794,4),(1795,1),(1797,4),(1798,1),(1799,1),(1800,1),(1801,4),(1802,4),(1803,1),(1805,7),(1809,7),(1812,7),(1813,7),(1814,7),(1815,7),(1817,7),(1818,7),(1819,7),(1820,3),(1821,7),(1822,7),(1823,7),(1824,1),(1825,10),(1826,7),(1827,1),(1828,10),(1829,7),(1830,7),(1831,7),(1832,3),(1833,3),(1834,3),(1835,7),(1836,3),(1837,7),(1839,1),(1841,7),(1842,7),(1843,7),(1844,7),(1845,7),(1846,7),(1847,7),(1848,7),(1849,7),(1850,7),(1851,1),(1852,7),(1853,7),(1856,7),(1857,7),(1858,4),(1859,7),(1860,10),(1864,1),(1865,7),(1866,7),(1867,10),(1868,1),(1869,1),(1870,7),(1871,11),(1872,7),(1876,7),(1877,4),(1878,7),(1879,7),(1881,5),(1883,10),(1884,7),(1885,7),(1886,7),(1888,1),(1889,1),(1892,1),(1896,9),(1897,3),(1898,1),(1899,7),(1900,7),(1901,7),(1903,7),(1904,7),(1905,10),(1906,7),(1907,7),(1909,1),(1910,1),(1911,7),(1912,7),(1914,7),(1919,1),(1920,1),(1921,1),(1923,4),(1924,1),(1925,1),(1929,1),(1937,10),(1944,1),(1947,7),(1953,1),(1955,1),(1957,7),(1970,7),(1971,7),(1972,7),(1973,7),(1974,3),(1975,7),(1976,3),(1977,1),(1978,7),(1990,7),(1991,4),(1993,10),(1994,1),(1999,1),(2002,10),(2003,7),(2004,10),(2005,1),(2006,10),(2007,4),(2008,7),(2009,1),(2010,7),(2011,7),(2013,7),(2014,7),(2015,9),(2016,1),(2017,10),(2018,1),(2019,7),(2020,1),(2021,1),(2022,1),(2023,1),(2024,1),(2025,10),(2026,1),(2027,1),(2028,1),(2029,4),(2030,1),(2031,7),(2032,7),(2033,1),(2035,4),(2036,1),(2037,7),(2038,7),(2039,7),(2041,1),(2044,1),(2045,7),(2046,7),(2047,1),(2048,7),(2049,7),(2052,7),(2053,1),(2054,1),(2059,1),(2061,1),(2062,4),(2063,7),(2064,1),(2066,7),(2067,1),(2069,10),(2070,10),(2071,1),(2073,1),(2074,1),(2075,7),(2076,1),(2077,1),(2078,7),(2079,1),(2080,1),(2081,1),(2082,1),(2083,1),(2084,7),(2085,7),(2086,4),(2087,1),(2089,1),(2090,1),(2091,10),(2093,7),(2094,1),(2095,1),(2096,1),(2097,1),(2098,1),(2099,1),(2100,1),(2101,1),(2102,7),(2103,7),(2104,7),(2105,1),(2106,1),(2107,10),(2108,1),(2109,1),(2110,7),(2111,4),(2112,4),(2113,1),(2114,7),(2115,1),(2116,1),(2117,6),(2118,1),(2119,1),(2120,1),(2121,1),(2122,2),(2123,1),(2125,1),(2126,7),(2127,7),(2128,1),(2129,10),(2130,6),(2131,1),(2132,1),(2133,10),(2134,7),(2135,7),(2136,1),(2138,1),(2139,1),(2140,1),(2141,1),(2142,1),(2143,1),(2145,7),(2146,1),(2147,11),(2148,1),(2149,1),(2150,1),(2151,7),(2152,1),(2153,1),(2154,1),(2155,1),(2156,1),(2157,1),(2159,7),(2160,1),(2161,7),(2162,1),(2164,1),(2165,1),(2166,7),(2167,1),(2168,1),(2170,7),(2171,1),(2172,7),(2173,1),(2174,1),(2175,1),(2176,1),(2177,7),(2178,7),(2179,10),(2180,4),(2182,10),(2183,6),(2184,1),(2185,6),(2186,1),(2187,1),(2188,5),(2189,4),(2190,4),(2191,7),(2192,1),(2193,1),(2194,1),(2195,1),(2196,1),(2197,7),(2198,1),(2199,1),(2200,10),(2201,1),(2202,1),(2203,3),(2204,1),(2205,1),(2206,1),(2207,10),(2208,1),(2209,1),(2210,1),(2211,1),(2212,1),(2213,1),(2214,1),(2215,1),(2216,1),(2217,1),(2218,1),(2219,7),(2220,1),(2221,1),(2222,7),(2223,6),(2225,5),(2227,1),(2229,1),(2230,12),(2231,1),(2232,9),(2233,10),(2234,8),(2235,9),(2236,4),(2237,1),(2238,1),(2239,1),(2240,1),(2241,1),(2242,1),(2243,1),(2244,10),(2245,1),(2246,1),(2247,1),(2248,7),(2249,4),(2250,6),(2251,1),(2252,1),(2253,1),(2255,1),(2256,4),(2257,1),(2259,1),(2261,1),(2262,9),(2263,1),(2264,1),(2266,4),(2267,1),(2268,1),(2269,1),(2270,1),(2271,7),(2272,1),(2273,7),(2274,1),(2275,1),(2276,1),(2277,1),(2278,1),(2279,4),(2280,1),(2281,1),(2282,10),(2283,1),(2284,1),(2285,1),(2286,1),(2287,1),(2288,1),(2289,1),(2290,1),(2291,10),(2292,1),(2293,1),(2294,1),(2295,4),(2296,1),(2297,1),(2298,1),(2299,10),(2301,7),(2302,1),(2303,1),(2304,10),(2305,1),(2306,1),(2307,1),(2308,1),(2309,1),(2310,1),(2311,1),(2312,1),(2313,1),(2314,1),(2315,9),(2316,1),(2317,1),(2318,4),(2319,1),(2320,1),(2321,1),(2322,1),(2323,1),(2324,1),(2325,1),(2326,1),(2327,1),(2328,1),(2329,10),(2330,1),(2331,1),(2332,1),(2333,1),(2334,1),(2335,1),(2336,1),(2337,1),(2338,1),(2339,1),(2340,1),(2341,1),(2342,1),(2343,1),(2344,1),(2345,1),(2346,1),(2347,1),(2348,1),(2349,1),(2350,1),(2351,1),(2352,1),(2353,1),(2354,10),(2355,9),(2356,1),(2357,1),(2358,1),(2359,1),(2360,1),(2361,1),(2362,1),(2363,1),(2364,1),(2365,12),(2366,1),(2367,1),(2368,1),(2369,1),(2370,1),(2371,6),(2372,1),(2373,1),(2374,1),(2375,1),(2376,1),(2377,1),(2378,1),(2379,1),(2380,1),(2381,1),(2382,1),(2383,1),(2384,1),(2385,1),(2386,1),(2387,1),(2388,1),(2389,1),(2390,1),(2391,1),(2392,1),(2393,1),(2394,1),(2395,7),(2396,1),(2397,7),(2398,1),(2399,1),(2400,4),(2401,4),(2402,4),(2403,3),(2404,1),(2405,1),(2406,1),(2407,1),(2408,1),(2409,1),(2413,7),(2414,1),(2415,4),(2416,6),(2417,7),(2418,1),(2419,7),(2420,1),(2421,1),(2422,1),(2423,1),(2424,9),(2425,1),(2426,1),(2427,1),(2428,7),(2429,1),(2430,1),(2431,7),(2432,1),(2433,1),(2434,1),(2435,1),(2436,1),(2437,1),(2438,1),(2439,1),(2440,1),(2441,5),(2442,1),(2443,1),(2445,1),(2500,9),(2501,1),(2502,1),(2503,1),(2504,7),(2505,1),(2506,1),(2507,1),(2508,1),(2509,1),(2510,7),(2511,1),(2512,1),(2513,1),(2514,6),(2515,9),(2516,1),(2517,1),(2518,10),(2519,7),(2520,1),(2521,1),(2522,7),(2523,1),(2524,6),(2525,1),(2526,6),(2527,7),(2528,1),(2529,1),(2530,3),(2531,1),(2532,1),(2533,1),(2534,1),(2535,7),(2536,1),(2537,7),(2538,1),(2539,1),(2540,1),(2541,9),(2542,12),(2543,7),(2544,1),(2545,6),(2546,1),(2547,1),(2548,1),(2549,1),(2550,1),(2551,1),(2552,7),(2553,1),(2554,1),(2555,9),(2556,8),(2557,1),(2558,7),(2559,1),(2560,1),(2561,1),(2562,1),(2563,6),(2564,1),(2565,1),(2566,7),(2568,1),(2570,7),(2573,1),(2574,7),(2575,1),(2576,1),(2577,1),(2578,1),(2581,7),(2583,1),(2584,7),(2585,1),(2587,7),(2597,1),(2607,1),(2612,1),(2613,7),(2620,1),(2622,1),(2626,1),(2634,1),(2636,1),(2639,1),(2642,1),(2643,1),(2644,1),(2645,7),(2647,1),(2651,1),(2652,1),(2653,7),(2654,1),(2666,4),(2671,4),(2678,1),(2681,1),(2682,10),(2683,10),(2684,10),(2686,10),(2687,10),(2688,10),(2689,10),(2690,10),(2691,10),(2692,10),(2693,10),(2694,6),(2695,1),(2697,6),(2698,1),(2699,10),(2700,1),(2701,1),(2702,10),(2703,1),(2704,10),(2705,10),(2706,10),(2707,10),(2708,10),(2709,10),(2710,7),(2711,10),(2712,10),(2713,7),(2714,10),(2715,10),(2717,10),(2718,10),(2719,10),(2720,10),(2721,1),(2722,1),(2723,10),(2724,7),(2725,7),(2726,1),(2727,7),(2728,1),(2729,10),(2730,1),(2731,1),(2732,1),(2733,7),(2734,1),(2735,1),(2736,1),(2737,1),(2738,1),(2739,1),(2740,7),(2741,1),(2742,1),(2744,1),(2745,1),(2746,1),(2747,1),(2748,1),(2749,1),(2750,11),(2752,7),(2753,1),(2754,7),(2755,1),(2757,1),(2758,1),(2759,1),(2760,1),(2761,10),(2762,1),(2763,1),(2764,1),(2765,7),(2766,1),(2767,1),(2768,7),(2769,1),(2770,1),(2771,1),(2772,1),(2773,1),(2774,7),(2775,10),(2776,1),(2777,1),(2778,1),(2779,1),(2780,1),(2781,1),(2783,1),(2784,1),(2785,1),(2786,1),(2787,1),(2788,1),(2789,1),(2790,1),(2791,10),(2792,1),(2793,1),(2794,1),(2795,1),(2797,1),(2798,1),(2799,1),(2800,1),(2801,1),(2802,1),(2803,1),(2804,1),(2805,1),(2806,1),(2807,1),(2808,1),(2809,1),(2810,1),(2811,1),(2812,1),(2813,5),(2814,1),(2815,12),(2816,1),(2817,1),(2818,1),(2819,1),(2820,1),(2822,1),(2823,1),(2824,1),(2825,1),(2826,1),(2827,1),(2828,1),(2829,4),(2830,1),(2831,1),(2833,9),(2834,1),(2835,1),(2836,1),(2837,1),(2838,1),(2839,1),(2840,1),(2841,1),(2842,7),(2846,1),(2847,7),(2848,7),(2850,10),(2851,7),(2853,7),(2854,10),(2855,7),(2857,1),(2858,7),(2859,1),(2860,7),(2866,7),(2867,7),(2868,7),(2869,7),(2870,1),(2871,7),(2872,7),(2873,7),(2874,10),(2875,1),(2877,1),(2878,7),(2879,7),(2880,7),(2882,7),(2883,1),(2884,7),(2885,1),(2886,1),(2887,7),(2888,7),(2889,7),(2890,7),(2891,7),(2892,7),(2893,10),(2895,1),(2896,1),(2897,10),(2898,1),(2899,1),(2900,1),(2901,1),(2902,1),(2903,1),(2904,1),(2905,1),(2906,1),(2907,1),(2908,1),(2909,1),(2910,1),(2911,7),(2912,1),(2913,1),(2914,1),(2915,1),(2916,1),(2917,1),(2918,1),(2919,1),(2920,1),(2921,1),(2924,1),(2925,1),(2926,1),(2927,1),(2928,1),(2930,1),(2931,1),(2932,1),(2933,1),(2934,1),(2935,1),(2936,1),(2938,1),(2939,1),(2941,1),(2942,9),(2943,1),(2944,3),(2945,1),(2946,1),(2947,1),(2949,7),(2952,1),(2953,1),(2954,1),(2955,1),(2956,1),(2957,1),(2958,1),(2959,1),(2960,1),(2961,1),(2962,1),(2963,10),(2965,1),(2966,10),(2968,10),(2969,10),(2970,7),(2971,1),(2972,9),(2973,1),(2974,1),(2975,1),(2976,1),(2977,1),(2978,1),(2979,1),(2980,1),(2982,1),(2983,1),(2984,1),(2987,1),(2988,1),(2989,1),(2990,1),(2991,1),(2993,1),(2994,1),(2995,1),(2997,1),(2998,1),(2999,1),(3000,1),(3001,1),(3002,1),(3003,5),(3004,1),(3006,7),(3007,1),(3008,9),(3009,1),(3010,7),(3011,1),(3012,1),(3013,1),(3014,1),(3015,7),(3016,1),(3017,1),(3018,1),(3019,1),(3020,1),(3022,1),(3023,1),(3024,1),(3025,7),(3026,7),(3027,7),(3028,7),(3029,7),(3030,1),(3031,1),(3032,1),(3033,1),(3034,4),(3035,1),(3036,1),(3037,1),(3038,1),(3039,1),(3040,10),(3041,10),(3042,10),(3043,10),(3044,10),(3045,10),(3046,7),(3047,1),(3048,1),(3049,7),(3050,7),(3051,7),(3052,7),(3053,7),(3054,7),(3055,7),(3056,1),(3057,1),(3058,9),(3059,1),(3060,1),(3061,1),(3062,5),(3063,7),(3064,7),(3065,1),(3066,1),(3067,1),(3068,1),(3069,7),(3070,1),(3072,1),(3073,1),(3074,1),(3075,1),(3076,1),(3077,1),(3078,1),(3079,7),(3080,7),(3081,7),(3082,7),(3083,7),(3084,7),(3085,7),(3086,7),(3087,11),(3089,7),(3090,7),(3091,7),(3092,7),(3094,7),(3095,1),(3096,1),(3097,1),(3099,1),(3100,1),(3101,7),(3102,7),(3103,1),(3104,1),(3105,1),(3106,1),(3107,1),(3108,1),(3110,3),(3111,7),(3112,7),(3113,7),(3114,7),(3115,7),(3116,7),(3117,7),(3118,7),(3119,7),(3120,7),(3121,7),(3122,1),(3123,7),(3124,7),(3125,7),(3126,1),(3127,7),(3128,7),(3129,7),(3130,7),(3131,7),(3132,1),(3133,7),(3135,1),(3137,1),(3138,1),(3139,6),(3140,1),(3141,1),(3142,10),(3143,4),(3144,4),(3145,4),(3146,4),(3147,4),(3148,4),(3149,4),(3150,4),(3151,4),(3152,1),(3153,1),(3154,1),(3155,4),(3156,4),(3157,1),(3158,1),(3159,1),(3160,1),(3161,1),(3162,1),(3163,1),(3164,1),(3165,1),(3166,1),(3167,1),(3168,1),(3169,4),(3170,1),(3171,1),(3172,1),(3174,1),(3175,1),(3176,1),(3177,1),(3178,1),(3179,1),(3180,1),(3181,1),(3182,1),(3183,1),(3184,1),(3185,1),(3186,1),(3187,1),(3188,1),(3189,1),(3190,1),(3191,1),(3192,1),(3193,1),(3195,1),(3196,1),(3197,1),(3198,1),(3199,1),(3200,1),(3201,1),(3202,1),(3203,7),(3204,1),(3205,1),(3206,1),(3207,1),(3208,1),(3209,1),(3210,9),(3211,1),(3213,1),(3214,7),(3216,1),(3217,11),(3218,1),(3219,1),(3220,1),(3221,1),(3222,1),(3223,1),(3224,1),(3225,5),(3226,1),(3227,1),(3228,1),(3229,1),(3230,1),(3234,1),(3235,1),(3236,1),(3237,1),(3238,1),(3239,1),(3240,7),(3241,7),(3242,7),(3243,7),(3245,7),(3246,1),(3247,1),(3248,7),(3249,1),(3251,1),(3252,1),(3253,1),(3254,1),(3255,1),(3257,10),(3260,6),(3261,1),(3262,6),(3263,5),(3264,1),(3265,1),(3266,1),(3267,7),(3268,1),(3269,1),(3270,1),(3271,1),(3272,1),(3273,1),(3274,1),(3275,1),(3277,1),(3279,1),(3281,1),(3283,1),(3284,1),(3285,1),(3286,7),(3287,1),(3288,1),(3289,1),(3290,1),(3291,1),(3292,1),(3294,1),(3295,1),(3296,1),(3297,1),(3299,1),(3300,1),(3301,1),(3302,1),(3303,1),(3304,1),(3305,1),(3306,1),(3307,1),(3308,1),(5000,1),(5001,7),(5002,7),(5004,1),(5005,3),(5006,5),(5007,10),(5008,7),(5010,1),(5011,1),(5012,1),(5013,1),(5014,1),(5016,1),(5017,1),(5019,1),(5021,1),(5022,1),(5024,1),(5026,7),(5027,7),(5029,10),(5031,10),(5032,10),(5033,7),(5034,1),(5035,1),(5036,7),(5037,1),(5038,1),(5039,1),(5040,1),(5041,7),(5042,7),(5043,1),(5044,1),(5045,1),(5046,7),(5047,1),(5048,1),(5049,1),(5050,1),(5051,10),(5052,1),(5053,1),(5054,1),(5056,1),(5057,7),(5058,7),(5059,7),(5060,7),(5061,10),(5063,1),(5066,1),(5067,1),(5068,1),(5069,1),(5070,1),(5071,1),(5072,1),(5073,1),(5074,7),(5076,7),(5079,1),(5081,10),(5084,1),(5085,2),(5086,1),(5087,1),(5089,1),(5090,1),(5091,1),(5092,1),(5093,7),(5094,1),(5095,1),(5096,1),(5097,10),(5101,1),(5104,7),(5105,10),(5106,1),(5107,1),(5109,7),(5110,1),(5112,1),(5113,1),(5114,1),(5115,1),(5116,9),(5117,1),(5118,1),(5119,1),(5120,7),(5121,1),(5122,1),(5123,1),(5124,1),(5125,1),(5126,1),(5127,7),(5128,1),(5129,1),(5130,1),(5131,1),(5132,1),(5133,6),(5134,1),(5135,1),(5136,1),(5137,1),(5140,1),(5142,7),(5143,10),(5144,6),(5145,7),(5146,1),(5148,1),(5150,1),(5152,7),(5153,8),(5154,11),(5155,1),(5156,10),(5157,10),(5158,7),(5160,1),(5161,1),(5162,1),(5163,1),(5164,1),(5165,1),(5166,7),(5167,1),(5169,1),(5170,1),(5171,1),(5173,1),(5174,1),(5175,1),(5176,1),(5177,7),(5178,7),(5179,1),(5180,1),(5181,7),(5183,1),(5184,6),(5185,1),(5186,1),(5187,1),(5188,2),(5190,1),(5192,7),(5193,10),(5195,7),(5196,1),(5197,10),(5198,1),(5200,1),(5201,1),(5202,1),(5203,1),(5204,1),(5206,1),(5207,1),(5209,4),(5210,1),(5211,1),(5212,4),(5215,1),(5216,7),(5217,1),(5218,7),(5219,1),(5220,1),(5223,10),(5224,1),(5225,7),(5226,6),(5227,7),(5228,1),(5229,10),(5230,10),(5231,7),(5232,1),(5233,1),(5234,7),(5235,1),(5236,1),(5237,1),(5238,1),(5239,1),(5240,1),(5241,1),(5242,1),(5243,1),(5244,1),(5245,1),(5246,7),(5247,9),(5248,7),(5249,4),(5250,9),(5251,1),(5253,1),(5254,1),(5255,7),(5257,1),(5258,1),(5260,1),(5261,1),(5262,1),(5263,1),(5264,1),(5265,1),(5266,1),(5267,1),(5268,1),(5269,10),(5270,7),(5271,10),(5272,1),(5273,1),(5274,1),(5275,1),(5276,7),(5277,6),(5278,1),(5280,1),(5281,3),(5282,1),(5283,1),(5284,1),(5285,1),(5286,1),(5287,1),(5288,1),(5289,1),(5290,1),(5291,9),(5292,6),(5296,7),(5297,1),(5298,1),(5300,7),(5301,1),(5303,1),(5304,1),(5306,2),(5307,3),(5308,3),(5309,3),(5311,3),(5312,3),(5313,3),(5314,3),(5315,3),(5316,3),(5317,3),(5318,10),(5319,4),(5320,7),(5321,4),(5322,7),(5323,8),(5324,5),(5325,5),(5326,5),(5327,5),(5328,4),(5329,5),(5330,5),(5331,5),(5333,7),(5334,7),(5335,5),(5336,6),(5337,3),(5338,6),(5339,7),(5340,8),(5342,8),(5344,8),(5345,8),(5347,1),(5348,7),(5349,9),(5350,9),(5351,8),(5352,9),(5354,9),(5355,9),(5356,8),(5357,7),(5358,9),(5363,9),(5364,9),(5365,1),(5366,9),(5367,9),(5368,9),(5369,9),(5370,9),(5371,7),(5372,1),(5373,9),(5374,9),(5375,9),(5376,9),(5377,9),(5378,9),(5380,9),(5381,9),(5382,7),(5383,9),(5386,10),(5387,10),(5388,9),(5389,10),(5390,9),(5392,9),(5393,9),(5394,9),(5395,9),(5396,9),(5397,9),(5398,9),(5400,1),(5401,1),(5402,12),(5403,12),(5404,12),(5405,12),(5406,2),(5408,1),(5409,1),(5411,2),(5412,2),(5413,3),(5414,7),(5415,7),(5416,7),(5417,4),(5418,3),(5419,4),(5420,3),(5421,10),(5422,7),(5423,7),(5424,4),(5425,6),(5426,6),(5427,6),(5428,7),(5429,7),(5430,6),(5431,7),(5432,7),(5433,7),(5434,9),(5435,10),(5436,8),(5437,8),(5438,10),(5439,9),(5440,1),(5441,7),(5442,11),(5443,9),(5444,10),(5445,1),(5446,1),(5447,1),(5448,7),(5449,7),(5450,2),(5451,2),(5452,5),(5453,5),(5454,7),(5455,7),(5456,10),(5457,6),(5458,7),(5459,7),(5460,7),(5461,8),(5462,7),(5463,7),(5464,7),(5465,8),(5466,7),(5467,9),(5468,10),(5469,12),(5470,1),(5471,7),(5472,1),(5473,2),(5474,2),(5475,1),(5476,3),(5477,4),(5478,4),(5479,5),(5480,6),(5481,9),(5482,6),(5483,6),(5484,6),(5485,6),(5486,7),(5487,7),(5488,8),(5489,7),(5490,9),(5491,7),(5492,7),(5493,8),(5494,9),(5495,10),(5496,7),(5497,10),(5498,9),(5499,9),(5500,9),(5501,9),(5502,9),(5503,9),(5504,9),(5505,9),(5506,10),(5507,11),(5508,2),(5509,7),(5510,7),(5511,2),(5512,4),(5513,3),(5514,10),(5515,4),(5516,4),(5517,4),(5518,4),(5519,4),(5520,4),(5521,7),(5522,7),(5523,7),(5524,10),(5525,7),(5526,7),(5527,7),(5528,5),(5529,5),(5530,7),(5531,6),(5532,7),(5533,6),(5534,6),(5535,6),(5536,6),(5537,6),(5538,7),(5539,7),(5540,7),(5541,7),(5542,7),(5543,6),(5544,6),(5545,7),(5546,7),(5547,7),(5548,8),(5549,9),(5550,7),(5551,7),(5552,10),(5553,9),(5554,10),(5555,11),(5556,7),(5557,12),(5559,2),(5560,2),(5561,2),(5564,7),(5565,1),(5566,7),(5567,7),(5568,8),(5569,8),(5570,8),(5571,8),(5572,5),(5573,6),(5574,7),(5575,10),(5576,8),(5578,7),(5579,7),(5580,7),(5582,10),(5583,7),(5584,7),(5585,7),(5586,7),(5588,8),(5589,8),(5590,8),(5591,9),(5593,9),(5594,9),(5595,1),(5596,9),(5597,10),(5598,10),(5599,9),(5600,10),(5601,7),(5604,10),(5605,1),(5606,1),(5607,5),(5608,12),(5609,7),(5610,10),(5611,1),(5612,4),(5613,2),(5614,2),(5615,3),(5616,3),(5617,7),(5618,7),(5619,7),(5620,7),(5621,4),(5622,7),(5623,6),(5624,7),(5625,6),(5626,9),(5628,7),(5629,7),(5630,10),(5631,7),(5632,7),(5633,7),(5634,8),(5635,8),(5636,10),(5637,8),(5638,7),(5639,9),(5640,9),(5641,9),(5642,4),(5643,9),(5645,10),(5646,10),(5647,11),(5648,12),(5649,7),(5650,1),(5651,7),(5652,3),(5653,1),(5654,7),(5655,2),(5656,1),(5657,1),(5658,2),(5659,2),(5660,2),(5661,2),(5662,2),(5663,1),(5664,7),(5665,4),(5666,3),(5667,4),(5668,4),(5669,4),(5670,4),(5671,4),(5672,4),(5673,4),(5674,4),(5676,7),(5677,5),(5678,4),(5679,4),(5680,5),(5681,5),(5682,5),(5683,7),(5684,7),(5685,7),(5686,7),(5687,8),(5688,8),(5689,9),(5690,9),(5691,7),(5692,9),(5693,9),(5694,9),(5695,10),(5696,11),(5697,11),(5699,1),(5700,12),(5701,2),(5702,1),(5703,2),(5704,3),(5705,4),(5706,7),(5707,5),(5708,9),(5709,6),(5710,6),(5711,7),(5712,8),(5713,9),(5714,9),(5715,1),(5716,10),(5717,1),(5718,1),(5719,7),(5720,3),(5721,2),(5722,2),(5723,7),(5724,4),(5726,5),(5727,6),(5728,7),(5729,6),(5730,6),(5731,7),(5732,6),(5733,6),(5734,10),(5735,9),(5736,7),(5737,9),(5738,8),(5739,8),(5740,9),(5741,9),(5742,9),(5743,9),(5744,9),(5745,9),(5746,9),(5747,9),(5748,9),(5749,9),(5750,9),(5751,9),(5752,10),(5753,2),(5754,7),(5755,3),(5756,12),(5757,1),(5758,7),(5759,6),(5760,5),(5761,5),(5762,5),(5763,1),(5764,5),(5765,6),(5766,6),(5767,6),(5768,8),(5769,1),(5770,1),(5771,5),(5772,7),(5773,10),(5774,1),(5775,1),(5776,8),(5777,10),(5778,10),(5779,10),(5780,7),(5781,11),(5782,8),(5783,12),(5784,9),(5785,12),(5786,12),(5787,12),(5788,12),(5789,12),(5790,12),(5791,12),(5792,12),(5793,12),(5794,12),(5795,12),(5796,12),(5797,12),(5798,12),(5799,7),(5801,7),(5802,7),(5803,7),(5804,1),(5806,1),(5807,7),(5808,1),(5809,7),(5810,7),(5812,9),(5813,1),(5814,4),(5815,7),(5816,1),(5817,7),(5818,5),(5819,7),(5820,5),(5821,5),(5822,7),(5823,6),(5824,6),(5825,6),(5826,7),(5827,12),(5828,6),(5829,11),(5830,7),(5831,6),(5832,6),(5833,6),(5834,6),(5835,1),(5836,7),(5837,7),(5838,7),(5839,7),(5840,9),(5841,7),(5842,8),(5843,8),(5844,8),(5845,7),(5846,8),(5847,8),(5848,7),(5849,9),(5850,7),(5851,1),(5852,9),(5853,9),(5854,9),(5855,9),(5856,9),(5857,12),(5858,9),(5859,7),(5860,9),(5861,9),(5862,9),(5863,10),(5864,10),(5865,10),(5866,10),(5867,10),(5868,10),(5869,10),(5870,10),(5871,7),(5872,1),(5873,1),(5874,7),(5875,1),(5876,1),(5877,1),(5878,2),(5879,2),(5880,2),(5881,2),(5882,2),(5883,2),(5884,2),(5885,2),(5886,2),(5887,2),(5888,2),(5889,3),(5890,3),(5891,7),(5892,7),(5893,1),(5894,3),(5895,4),(5896,7),(5897,4),(5898,5),(5899,5),(5900,5),(5901,1),(5902,1),(5903,5),(5904,6),(5905,6),(5906,7),(5907,7),(5908,7),(5909,8),(5910,7),(5911,8),(5912,7),(5913,8),(5914,8),(5915,9),(5916,9),(5917,9),(5918,9),(5919,9),(5920,9),(5921,9),(5922,9),(5923,9),(5924,9),(5925,9),(5926,9),(5927,9),(5928,9),(5929,9),(5930,10),(5931,9),(5932,9),(5933,9),(5934,9),(5935,9),(5936,9),(5937,9),(5938,10),(5939,9),(5940,9),(5941,9),(5942,9),(5943,9),(5944,9),(5945,10),(5946,10),(5947,10),(5948,10),(5949,10),(5950,5),(5951,10),(5952,10),(5953,10),(5954,10),(5955,10),(5956,10),(5957,10),(5958,4),(5959,11),(5960,11),(5961,1),(5962,11),(5963,11),(5964,11),(5965,1),(5966,7),(5967,11),(5968,11),(5969,12),(5970,1),(5971,10),(5972,1),(5973,1),(5974,1),(5975,7),(5976,2),(5977,2),(5978,2),(5979,7),(5980,9),(5981,2),(5982,2),(5983,2),(5984,2),(5985,7),(5986,3),(5987,3),(5988,3),(5989,7),(5990,4),(5991,3),(5992,4),(5993,4),(5994,4),(5995,4),(5996,4),(5997,5),(5998,5),(5999,5),(6000,1),(6001,1),(6002,1),(6003,1),(6004,1),(6005,1),(6006,1),(6007,1),(6008,1),(6009,1),(6010,1),(6011,1),(6012,1),(6013,1),(6014,1),(6015,7),(6016,1),(6017,1),(6018,1),(6019,1),(6020,1),(6021,1),(6022,1),(6023,1),(6024,1),(6025,1),(6026,1),(6027,1),(6028,1),(6029,1),(6030,1),(6031,1),(6032,1),(6033,1),(6034,1),(6035,1),(6036,1),(6037,1),(6038,1),(6039,1),(6040,1),(6041,1),(6042,1),(6043,1),(6044,1),(6045,1),(6046,1),(6047,1),(6048,1),(6049,1),(6050,1),(6051,1),(6052,1),(6053,1),(6054,1),(6055,1),(6056,1),(6057,1),(6058,1),(6059,1),(6060,10),(6061,1),(6062,1),(6063,1),(6064,1),(6065,1),(6066,1),(6067,1),(6068,5),(6069,7),(6070,5),(6071,7),(6072,5),(6073,6),(6074,9),(6075,8),(6076,6),(6077,6),(6078,6),(6079,1),(6080,7),(6081,6),(6082,1),(6083,6),(6084,6),(6085,6),(6086,6),(6087,6),(6088,7),(6089,7),(6090,7),(6091,7),(6092,6),(6093,4),(6094,8),(6095,7),(6096,7),(6097,9),(6098,8),(6099,8),(6100,9),(6101,12),(6102,7),(6103,8),(6104,8),(6105,9),(6106,1),(6107,10),(6108,10),(6109,10),(6110,6),(6111,10),(6112,10),(6113,10),(6114,10),(6115,10),(6116,10),(6117,10),(6118,1),(6119,10),(6120,10),(6121,10),(6122,10),(6123,10),(6124,1),(6125,10),(6126,7),(6127,1),(6128,1),(6129,10),(6130,11),(6131,1),(6132,10),(6133,10),(6134,4),(6135,11),(6136,12),(6137,12),(6138,10),(6139,12),(6140,12),(6141,12),(6142,1),(6143,1),(6144,1),(6145,1),(6146,1),(6147,4),(6148,2),(6149,2),(6150,7),(6151,10),(6152,10),(6153,2),(6154,3),(6155,1),(6156,1),(6157,3),(6158,3),(6159,7),(6160,3),(6161,3),(6162,3),(6163,7),(6164,7),(6165,3),(6166,3),(6167,3),(6168,7),(6169,3),(6170,1),(6171,3),(6172,7),(6173,3),(6174,4),(6175,4),(6176,1),(6177,7),(6178,4),(6179,10),(6180,5),(6181,7),(6182,5),(6183,5),(6184,5),(6185,5),(6186,6),(6187,7),(6188,7),(6189,7),(6190,6),(6191,6),(6192,1),(6193,6),(6194,6),(6195,1),(6196,10),(6197,11),(6198,7),(6199,7),(6200,10),(6201,7),(6202,1),(6203,7),(6204,7),(6205,7),(6206,7),(6207,7),(6208,7),(6209,10),(6210,7),(6211,7),(6212,1),(6213,7),(6214,7),(6215,7),(6216,8),(6217,8),(6218,3),(6219,3),(6220,7),(6221,8),(6222,9),(6223,8),(6224,9),(6225,9),(6226,10),(6227,9),(6228,9),(6229,10),(6230,9),(6231,9),(6232,9),(6233,9),(6234,10),(6235,4),(6236,10),(6237,11),(6238,10),(6239,10),(6240,11),(6241,11),(6242,1),(6243,11),(6244,11),(6245,1),(6246,1),(6247,1),(6248,10),(6249,1),(6250,1),(6251,1),(6252,1),(6253,1),(6254,1),(6255,1),(6256,1),(6257,1),(6258,1),(6259,1),(6260,7),(6261,12),(6262,1),(6263,10),(6264,7),(6265,7),(6266,1),(6267,12),(6268,7),(6269,2),(6270,2),(6271,2),(6272,2),(6273,1),(6274,12),(6275,12),(6276,7),(6277,7),(6278,1),(6279,10),(6280,7),(6281,1),(6282,1),(6283,10),(6284,1),(6285,3),(6286,2),(6287,7),(6288,7),(6289,9),(6290,3),(6291,11),(6292,3),(6293,3),(6294,3),(6295,7),(6296,7),(6297,10),(6298,7),(6299,10),(6300,7),(6301,6),(6302,4),(6303,6),(6304,1),(6305,12),(6306,12),(6307,5),(6308,5),(6309,11),(6310,7),(6311,1),(6312,7),(6313,6),(6314,6),(6315,7),(6316,6),(6317,5),(6318,4),(6319,6),(6320,6),(6321,7),(6322,10),(6323,6),(6324,7),(6325,8),(6326,7),(6327,7),(6328,8),(6329,8),(6330,7),(6331,8),(6332,1),(6333,8),(6334,8),(6335,8),(6336,8),(6337,7),(6338,9),(6339,1),(6340,8),(6341,6),(6342,8),(6343,8),(6344,9),(6345,12),(6346,9),(6347,10),(6348,9),(6349,9),(6350,9),(6351,11),(6352,10),(6353,10),(6354,10),(6355,4),(6356,10),(6357,9),(6358,11),(6359,1),(6360,11),(6361,11),(6362,8),(6363,11),(6364,12),(6365,12),(6366,12),(6367,12),(6368,12),(6370,1),(6371,10),(6372,12),(6373,12),(6374,12),(6375,12),(6376,1),(6377,1),(6378,1),(6379,6),(6380,1),(6381,1),(6382,1),(6383,1),(6384,7),(6385,1),(6386,1),(6387,1),(6388,7),(6389,2),(6390,2),(6391,2),(6392,2),(6393,2),(6394,7),(6395,3),(6396,3),(6397,3),(6398,6),(6399,3),(6400,3),(6401,1),(6402,4),(6403,4),(6404,5),(6405,5),(6406,5),(6407,6),(6408,5),(6409,9),(6410,8),(6411,6),(6413,6),(6414,7),(6415,6),(6416,6),(6417,7),(6418,10),(6419,6),(6420,7),(6421,7),(6422,7),(6423,7),(6424,3),(6425,8),(6426,12),(6427,10),(6428,10),(6429,8),(6430,8),(6431,9),(6432,9),(6433,7),(6434,9),(6435,9),(6436,9),(6437,9),(6438,9),(6439,9),(6440,11),(6441,10),(6442,7),(6443,11),(6444,11),(6445,10),(6446,11),(6447,11),(6448,8),(6449,11),(6450,1),(6451,11),(6452,1),(6453,7),(6454,1),(6455,1),(6456,12),(6457,1),(6458,2),(6459,1),(6460,1),(6461,1),(6462,10),(6463,2),(6464,10),(6465,2),(6466,2),(6467,2),(6468,2),(6469,2),(6470,1),(6471,6),(6472,10),(6473,2),(6474,1),(6475,2),(6476,10),(6477,7),(6478,3),(6479,3),(6480,7),(6481,3),(6482,3),(6483,7),(6484,7),(6485,3),(6486,4),(6487,5),(6488,4),(6489,1),(6490,4),(6491,4),(6492,4),(6493,4),(6494,4),(6495,5),(6496,10),(6497,7),(6498,7),(6499,7),(6500,10),(6501,1),(6502,7),(6503,1),(6504,5),(6505,6),(6506,7),(6507,6),(6508,6),(6509,7),(6510,7),(6511,1),(6512,7),(6513,7),(6514,7),(6515,7),(6516,7),(6517,8),(6518,9),(6519,8),(6520,4),(6521,9),(6522,9),(6523,4),(6524,9),(6525,9),(6526,10),(6527,10),(6528,10),(6529,11),(6530,11),(6531,11),(6532,7),(6533,11),(6534,11),(6535,10),(6536,7),(6537,10),(6538,10),(6539,10),(6540,12),(6541,12),(6542,12),(6543,1),(6544,1),(6545,1),(6546,1),(6547,1),(6548,12),(6549,1),(6550,1),(6551,1),(6552,1),(6553,1),(6554,1),(6555,2),(6556,1),(6557,2),(6558,10),(6559,1),(6560,1),(6561,2),(6562,2),(6563,2),(6564,7),(6565,2),(6566,3),(6567,2),(6568,10),(6569,3),(6570,7),(6571,3),(6572,3),(6573,3),(6574,3),(6575,7),(6576,3),(6577,3),(6578,3),(6579,3),(6580,3),(6581,3),(6582,3),(6583,3),(6584,7),(6585,7),(6586,4),(6587,4),(6588,5),(6589,5),(6590,4),(6591,10),(6592,5),(6593,4),(6594,6),(6595,10),(6596,5),(6597,6),(6598,6),(6599,1),(6600,6),(6601,6),(6602,6),(6603,1),(6604,7),(6605,6),(6606,6),(6607,6),(6608,9),(6609,11),(6610,7),(6611,7),(6612,7),(6613,8),(6614,6),(6615,7),(6616,1),(6617,1),(6618,7),(6619,10),(6620,10),(6621,10),(6622,8),(6623,8),(6624,8),(6625,9),(6626,10),(6627,10),(6628,10),(6629,10),(6630,10),(6631,10),(6632,10),(6633,10),(6634,1),(6635,7),(6636,9),(6637,11),(6639,12),(6640,10),(6641,1),(6642,7),(6643,7),(6644,7),(6645,1),(6646,1),(6647,1),(6648,1),(6649,2),(6650,2),(6651,1),(6652,7),(6653,2),(6654,1),(6655,1),(6656,1),(6657,3),(6658,1),(6659,3),(6660,10),(6661,1),(6662,1),(6663,3),(6664,4),(6665,4),(6666,10),(6667,10),(6668,7),(6669,7),(6670,4),(6671,1),(6672,1),(6673,7),(6674,1),(6675,7),(6676,5),(6677,6),(6678,1),(6679,5),(6680,5),(6681,5),(6682,10),(6683,10),(6684,5),(6685,10),(6686,10),(6687,5),(6688,5),(6689,5),(6690,4),(6691,5),(6692,7),(6693,5),(6694,5),(6695,10),(6696,5),(6697,1),(6698,5),(6699,5),(6700,5),(6701,5),(6702,7),(6703,7),(6704,10),(6705,5),(6706,5),(6707,5),(6708,5),(6709,10),(6710,1),(6711,6),(6712,6),(6713,6),(6714,6),(6715,6),(6716,5),(6717,6),(6718,6),(6719,6),(6720,6),(6721,6),(6722,10),(6723,6),(6724,6),(6725,6),(6726,6),(6727,6),(6728,6),(6729,6),(6730,6),(6731,6),(6732,6),(6733,6),(6734,6),(6735,6),(6736,6),(6737,7),(6738,10),(6739,1),(6740,7),(6741,7),(6742,7),(6743,7),(6744,7),(6745,7),(6746,7),(6747,7),(6748,7),(6749,1),(6750,10),(6751,8),(6752,8),(6753,9),(6754,7),(6755,5),(6756,8),(6757,8),(6758,8),(6759,10),(6760,8),(6761,8),(6762,9),(6763,9),(6764,9),(6765,9),(6766,9),(6767,9),(6768,9),(6769,10),(6770,10),(6771,10),(6772,10),(6773,10),(6774,10),(6775,10),(6776,10),(6777,7),(6778,11),(6779,11),(6780,11),(6781,11),(6782,7),(6783,12),(6784,12),(6785,12),(6786,12),(6787,12),(6788,1),(6789,1),(6790,2),(6791,2),(6792,2),(6793,2),(6794,2),(6795,2),(6796,6),(6797,2),(6798,2),(6799,3),(6800,3),(6801,3),(6802,3),(6803,3),(6804,3),(6805,3),(6806,3),(6807,3),(6808,3),(6809,3),(6810,3),(6811,3),(6812,3),(6813,7),(6814,3),(6815,3),(6816,4),(6817,4),(6818,4),(6819,4),(6820,4),(6821,4),(6822,7),(6823,4),(6824,7),(6825,7),(6826,5),(6827,7),(6828,5),(6829,5),(6830,5),(6831,5),(6832,5),(6833,5),(6834,5),(6835,5),(6836,1),(6837,5),(6838,6),(6839,6),(6840,6),(6841,6),(6842,1),(6843,6),(6844,7),(6845,7),(6846,7),(6847,7),(6848,7),(6849,7),(6850,7),(6851,7),(6852,1),(6853,7),(6854,7),(6855,7),(6856,7),(6857,8),(6858,8),(6859,8),(6860,8),(6861,9),(6862,9),(6863,9),(6864,9),(6865,9),(6866,9),(6867,7),(6868,10),(6869,10),(6870,10),(6871,10),(6872,10),(6873,11),(6874,11),(6875,11),(6876,11),(6877,11),(6878,11),(6879,11),(6880,11),(6881,11),(6882,11),(6883,11),(6884,11),(6885,11),(6886,12),(6887,12),(6888,12),(6889,12),(6890,12),(6891,12),(6892,12),(6893,1),(6894,10),(6895,1),(6896,1),(6897,1),(6898,1),(6899,1),(6900,1),(6901,10),(6902,2),(6903,2),(6904,10),(6905,2),(6906,2),(6907,2),(6908,2),(6909,2),(6910,2),(6911,2),(6912,2),(6913,2),(6914,2),(6915,3),(6916,3),(6917,3),(6918,3),(6919,3),(6920,3),(6921,3),(6922,3),(6923,3),(6924,3),(6925,3),(6926,3),(6927,4),(6928,4),(6929,4),(6930,7),(6931,4),(6932,4),(6933,5),(6934,5),(6935,6),(6936,6),(6937,6),(6938,10),(6939,6),(6940,6),(6941,6),(6942,6),(6943,6),(6944,7),(6945,7),(6946,7),(6947,7),(6948,7),(6949,7),(6950,7),(6951,7),(6952,7),(6953,6),(6954,7),(6955,7),(6956,8),(6957,8),(6958,9),(6959,1),(6960,7),(6961,1),(6962,12),(6963,10),(6964,1),(6965,10),(6966,10),(6967,10),(6968,11),(6969,11),(6970,11),(6971,12),(6972,7),(6973,1),(6974,1),(6975,1),(6976,1),(6977,1),(6978,1),(6979,1),(6980,2),(6981,1),(6982,1),(6983,2),(6984,2),(6985,2),(6986,2),(6987,2),(6988,2),(6989,7),(6990,3),(6991,1),(6992,3),(6993,3),(6994,3),(6995,10),(6996,4),(6997,7),(6998,4),(6999,6),(7000,5),(7001,5),(7002,5),(7003,5),(7004,5),(7005,5),(7006,6),(7007,6),(7008,6),(7009,6),(7010,7),(7011,7),(7012,8),(7013,9),(7014,9),(7015,10),(7016,11),(7017,11),(7018,11),(7019,11),(7020,11),(7021,11),(7022,1),(7023,1),(7024,1),(7025,1),(7026,2),(7027,2),(7028,2),(7029,2),(7030,2),(7031,2),(7032,2),(7033,2),(7034,3),(7035,3),(7036,3),(7037,3),(7038,3),(7039,4),(7040,4),(7041,4),(7042,4),(7043,4),(7044,4),(7045,4),(7046,4),(7047,4),(7048,4),(7049,4),(7050,4),(7051,4),(7052,4),(7053,4),(7054,5),(7055,5),(7056,5),(7057,5),(7058,5),(7059,5),(7060,5),(7061,5),(7062,5),(7063,5),(7064,5),(7065,5),(7066,5),(7067,5),(7068,5),(7069,5),(7070,5),(7071,6),(7072,6),(7073,6),(7074,6),(7075,6),(7076,6),(7077,7),(7078,7),(7079,7),(7080,7),(7081,7),(7082,8),(7083,8),(7084,8),(7085,8),(7086,8),(7087,8),(7088,8),(7089,9),(7090,9),(7091,9),(7092,9),(7093,9),(7094,9),(7095,9),(7096,9),(7097,10),(7098,10),(7099,10),(7100,10),(7101,11),(7102,11),(7103,11),(7104,11),(7105,11),(7106,12),(7107,12),(7108,1),(7109,1),(7110,1),(7111,2),(7112,2),(7113,10),(7114,2),(7115,2),(7116,2),(7117,2),(7118,3),(7119,3),(7120,3),(7121,3),(7122,3),(7123,3),(7124,3),(7125,3),(7126,3),(7127,3),(7128,4),(7129,4),(7130,4),(7131,4),(7132,4),(7133,4),(7134,4),(7135,4),(7136,4),(7137,4),(7138,5),(7139,5),(7140,5),(7141,5),(7142,5),(7143,5),(7144,6),(7145,6),(7146,6),(7147,6),(7148,6),(7149,6),(7150,6),(7151,6),(7152,6),(7153,6),(7154,7),(7155,7),(7156,7),(7157,7),(7158,7),(7159,8),(7160,8),(7161,8),(7162,8),(7163,8),(7164,8),(7165,8),(7166,8),(7167,8),(7168,8),(7169,9),(7170,9),(7171,10),(7172,10),(7173,10),(7174,11),(7175,11),(7176,12),(7177,12),(7178,12),(7179,12),(7180,12),(7181,12),(7182,1),(7183,2),(7184,2),(7185,2),(7186,2),(7187,2),(7188,3),(7189,3),(7190,3),(7191,3),(7192,3),(7193,3),(7194,3),(7195,3),(7196,3),(7197,3),(7198,3),(7199,4),(7200,4),(7201,4),(7202,4),(7203,4),(7204,5),(7205,5),(7206,5),(7207,5),(7208,5),(7209,5),(7210,1),(7211,6),(7212,7),(7213,7),(7214,1),(7215,7),(7216,1),(7217,1),(7218,1),(7219,1),(7220,1),(7221,6),(7222,9),(7223,10),(7224,6),(7225,7),(7227,1),(7228,1),(7229,7),(7230,10),(7231,10),(7232,1),(7234,7),(7235,1),(7236,6),(7237,10),(7238,1),(7239,1),(7240,10),(7241,1),(7242,7),(7243,7),(7244,7),(7245,1),(7246,1),(7247,1),(7249,1),(7250,4),(7251,1),(7252,10),(7253,10),(7254,1),(7255,1),(7256,1),(7257,1),(7258,10),(7259,1),(7260,1),(7261,7),(7262,1),(7263,1),(7264,1),(7265,5),(7266,7),(7267,1),(7268,1),(7269,1),(7270,10),(7271,1),(7272,1),(7273,1),(7274,1),(7275,7),(7276,7),(7277,1),(7278,7),(7279,12),(7280,1),(7281,1),(7282,1),(7283,1),(7284,10),(7285,7),(7286,1),(7287,1),(7288,4),(7289,7),(7290,1),(7291,6),(7292,10),(7293,1),(7294,10),(7295,3),(7296,7),(7297,7),(7298,7),(7299,1),(7300,7),(7301,8),(7302,10),(7303,8),(7304,7),(7305,8),(7306,10),(7307,10),(7308,1),(7309,9),(7310,7),(7311,9),(7312,7),(7313,1),(9999,10);

select  g.recipient_id, 
		g.duns, 
		case  when g.legal_business_name = null  then g.organization_alias
		when g.legal_business_name = ''  then g.organization_alias
		else g.legal_business_name
		end as legal_business_name,
		ifnull(l.city,'') as city, 
		ifnull(l.state_or_province,'') as state,
		g.cost_center_code,
		ifnull(Concat('$', Format(ob.total_obligation_amount,2)),'$0') as obligation_amount,
		g.recipient_acronym, 		
		upper(left(MONTHNAME(STR_TO_DATE(gs.fy_start_month, '%m')),3)) as start_month
FROM  trams_general_business_info g
left join trams_location l
on g.recipient_id = l.grantee_id
and l.address_type = 'Physical Address'
left join trams_total_obligation_by_grantee ob
on g.recipient_id = ob.recipient_id
left join trams_grantee_start_month_temp gs
on g.recipient_id = gs.grantee_id
where g.trams_status = 'Active' and g.recipient_id <> 728
order by g.recipient_id asc;


drop  temporary table if exists trams_total_obligation_by_grantee;
drop  temporary table if exists trams_grantee_start_month_temp;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_obligation_details`(IN `p_application_id` INT)
    NO SQL
begin

SELECT po_number,substring(project_number,1,length(project_number)-3) project_number_main,project_number,cost_center_code,scope_code,scope_suffix,uza_code,account_class_code,fpc,sum(`obligation_amount`) as 'cumulative_obligation_amount' FROM `trams_contract_acc_transaction` 
where application_id=p_application_id
group by po_number,project_number,cost_center_code,scope_code,scope_suffix,uza_code,account_class_code,fpc;

SELECT po_number,substring(project_number,1,length(project_number)-3) project_number_main,cost_center_code,scope_code,scope_suffix,uza_code,account_class_code,fpc,sum(`obligation_amount`) as 'cumulative_obligation_amount' FROM `trams_contract_acc_transaction` 
where contract_award_id=(select contract_award_id from trams_application where id = p_application_id)
group by po_number,substring(project_number,1,length(project_number)-3),cost_center_code,scope_code,scope_suffix,uza_code,account_class_code,fpc;

end$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_milestone_status_by_reportId_by_contractAwardId`(
  IN p_report_id int,
  IN p_contract_award_id int
)
BEGIN

  declare _sql varchar(40960) default "";
  declare _whereClause varchar(4096) default " ";
  declare _whereOtherClause varchar(4096) default " ";
  
 set _sql = "SELECT `mr`.`id`AS `id`,
        `mr`.`milestone_id` AS `milestone_id`,
        `ms`.`line_item_id` AS `line_item_id`,
        `mpr`.`contract_award_id` AS `contract_award_id`,
        `mr`.`report_id` AS `report_id`,
        `mr`.`remark` AS `remark`,
        `mr`.`remark_by` AS `remark_by`,
        `mr`.`remark_on_date_time` AS `remark_date`,
        `ms`.`milestone_title` AS `milestone_title`,
        `mr`.`revised_completion_date_time` AS `revised_completion_date`,
        `mr`.`revision_number` AS `revision_number`,
        `mr`.`actual_completion_date_time` AS `actual_completion_date`,
        `ms`.`estimated_completion_date_time` AS`estimated_completion_date`,
    `trams_get_mpr_status`(`mpr`.`due_date_time`, `ms`.`estimated_completion_date_time`, `mr`.`revised_completion_date_time`, `mr`.`actual_completion_date_time`, `mr`.`revision_number`) AS `mpr_status`,
    ( CASE WHEN Isnull(`mr`.`actual_completion_date_time`) THEN '0' ELSE '1' end ) AS `is_final`
    FROM    ((`trams_milestone_progress_remarks` `mr` JOIN `trams_milestones` `ms` ON(( `mr`.`milestone_id` = `ms`.`id` )))
        JOIN `trams_milestone_progress_report` `mpr` ON(( `mr`.`report_id` = `mpr`.`id` )))   ";
  
    set _whereOtherClause = " AND `mpr`.`fiscal_year` > 2008  AND  `mr`.`id` IS NOT NULL  AND  `mpr`.`report_status` <> 'FTA Review Complete' ";
  
    if p_contract_award_id = 0  then
      if p_report_id = 0 then 
        set _whereClause = " where 1<>1 ";
      else 
          set _whereClause = concat(" where mpr.id = ",p_report_id,_whereOtherClause);
      end if;
    else 
      if p_report_id = 0 then 
        set _whereClause = concat(" where mpr.id = mr.report_id ",_whereOtherClause," and `mpr`.`contract_award_id` = ",p_contract_award_id);
      else  
        set _whereClause = concat(" where mpr.id = ",p_report_id,_whereOtherClause, " and `mpr`.`contract_award_id` = ",p_contract_award_id);
      end if;
    end if;
    
    SET @mprStatusQuery = concat(_sql,_whereClause);
  
  set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));     
  CALL `trams_log`(@sid, 'trams_get_milestone_status_by_reportId_by_contractAwardId', concat('Get Query: ',@mprStatusQuery), 1); 
  
  PREPARE mprStmt FROM @mprStatusQuery;
  EXECUTE mprStmt;
    
    
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_milestone_status_by_reportId_or_contractAwardId`(IN p_report_id INT(11), IN p_contract_award_id INT(11))
BEGIN

  /*
   Stored Procedure Name : trams_get_milestone_status_by_reportId_or_contractAwardId
   Description : To populate the data for TrAMS MPR View Print.
   Updated by  : Garrison Dean (2020.Apr.01)
  */

  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _sqlCnt VARCHAR(40960) DEFAULT "";
  DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
  DECLARE _whereClause2 VARCHAR(4096) DEFAULT " ";
  DECLARE _whereOtherClause VARCHAR(4096) DEFAULT " ";
  -- added _intoClause variable
  DECLARE _intoCLause VARCHAR(64) DEFAULT " ";
  DECLARE _orderByClause VARCHAR(64) DEFAULT " ";

  -- added COUNT(*)
   SET _sqlCnt = "SELECT COUNT(*)
    FROM `trams_milestone_progress_remarks` `mr`
      JOIN `trams_milestones` `ms` ON `mr`.`milestone_id` = `ms`.`id`
      JOIN `trams_milestone_progress_report` `mpr` ON `mr`.`report_id` = `mpr`.`id`";

   SET _sql = "SELECT `mr`.`id`AS `id`,
        `mr`.`milestone_id` AS `milestone_id`,
        `ms`.`line_item_id` AS `line_item_id`,
        `mpr`.`contract_award_id` AS `contract_award_id`,
        `mr`.`report_id` AS `report_id`,
        `mr`.`remark` AS `remark`,
        `mr`.`remark_by` AS `remark_by`,
        `mr`.`remark_on_date_time` AS `remark_date`,
        `ms`.`milestone_title` AS `milestone_title`,
        `mr`.`revised_completion_date_time` AS `revised_completion_date`,
        `mr`.`revision_number` AS `revision_number`,
        `mr`.`actual_completion_date_time` AS `actual_completion_date`,
        `ms`.`estimated_completion_date_time` AS`estimated_completion_date`,
  `trams_get_mpr_status`(`mpr`.`due_date_time`, `ms`.`estimated_completion_date_time`, `mr`.`revised_completion_date_time`, `mr`.`actual_completion_date_time`, `mr`.`revision_number`) AS `mpr_status`,
  ( CASE WHEN Isnull(`mr`.`actual_completion_date_time`) THEN '0' ELSE '1' end ) AS `is_final`
  FROM    ((`trams_milestone_progress_remarks` `mr` JOIN `trams_milestones` `ms` ON(( `mr`.`milestone_id` = `ms`.`id` )))
        JOIN `trams_milestone_progress_report` `mpr` ON(( `mr`.`report_id` = `mpr`.`id` )))   ";

  SET _whereOtherClause = " AND `mpr`.`fiscal_year` > YEAR(DATE_SUB(NOW() , INTERVAL 10 YEAR))";
  SET _whereClause = concat(" where mpr.id = ",p_report_id,_whereOtherClause);
  SET _orderByClause = " ORDER BY ms.line_item_id, ms.estimated_completion_date_time";
  -- setting _intoClause in order to save output variable
  SET _intoClause = " INTO @cnt";

  -- _intoClause added as a CONCAT parameter
  SET @mprStatusQuery = CONCAT(_sqlCnt,_whereClause,_intoClause);

  SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_milestone_status_by_reportId_or_contractAwardId', concat('Get Query: ',@mprStatusQuery), 1);

  PREPARE mprStmt FROM @mprStatusQuery;
  EXECUTE mprStmt;

  CALL `trams_log`(@sid, 'trams_get_milestone_status_by_reportId_or_contractAwardId', concat('Total tmp_get_milestone_status_by_report_id: ',@cnt), 1);

  IF @cnt = 0 THEN
    SET _whereClause2 = concat(" where mpr.id = mr.report_id ",_whereOtherClause," and `mpr`.`contract_award_id` = ",p_contract_award_id);
    SET @mprStatusFinalQuery = concat( _sql, _whereClause2, _orderByClause);
  ELSE
    SET @mprStatusFinalQuery = concat( _sql, _whereClause, _orderByClause);
  END IF;

  CALL `trams_log`(@sid, 'trams_get_milestone_status_by_reportId_or_contractAwardId', concat('Get Insert Query: ',@mprStatusFinalQuery), 1);
  PREPARE mprStmtFinal FROM @mprStatusFinalQuery;
  EXECUTE mprStmtFinal;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_project_level_budget_report`(IN p_granteeId VARCHAR(2048), IN p_costCenterCode VARCHAR(512), IN p_fiscalYear VARCHAR(512),
IN p_appNumber VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_appType VARCHAR(512), IN p_preAwardManager VARCHAR(1024), IN p_postAwardManager VARCHAR(1024),
IN p_projectNumber VARCHAR(1024), IN p_sectionCode VARCHAR(512), IN p_gmtOffset VARCHAR(10), IN p_return_limit VARCHAR(20)
)
BEGIN

  /*
   Stored Procedure Name : trams_get_project_level_budget_report
   Description : To populate data for Project Budget excel report.
   Updated by  : Hunter Wang (2020.Aug.25)
  */

	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _whereClause VARCHAR(8192) DEFAULT " ";
	DECLARE _limitClause VARCHAR(100) DEFAULT " ";
	DECLARE _spName VARCHAR(255) DEFAULT "trams_get_project_level_budget_report";
	CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);


	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

	SET _sql = "INSERT INTO trams_temp_pb_application (application_id, project_id, contractAward_id, current_amendment_number, uuid, time_stamp)
	SELECT
		a.id AS application_id,
		p.id AS project_id,
		a.contractAward_id,
		ca.current_amendment_number,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_application a
	JOIN
		trams_project p
	ON a.id=p.application_id
	LEFT JOIN
		trams_contract_award ca
		ON ca.id = a.contractAward_ID
	WHERE (a.id = (SELECT MAX(id) FROM trams_application WHERE contractAward_ID = a.contractAward_ID)
	OR a.contractAward_ID is null)
			";

	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.recipient_id IN (",NULLIF(p_granteeId,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.recipient_cost_center IN (",NULLIF(p_costCenterCode,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fiscal_year IN (",NULLIF(p_fiscalYear,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.application_number IN ('",NULLIF(p_appNumber,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.status IN (",NULLIF(p_appStatus,''),")"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.application_type IN ('",NULLIF(p_appType,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fta_pre_award_manager IN ('",NULLIF(p_preAwardManager,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.fta_post_award_manager IN ('",NULLIF(p_postAwardManager,''),"')"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND p.project_number IN ('",NULLIF(p_projectNumber,''),"')"),"")));

	SET @applicationQuery = CONCAT(_sql,_whereClause,_limitClause);

	CALL `trams_log`(@sid, _spName, CONCAT('Start CALL SP Inputs: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_projectNumber` ,'NULL'),',',IFNULL(  `p_sectionCode` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1);
	CALL `trams_log`(@sid, _spName, CONCAT('Get Application & Project Query: ',@applicationQuery), 1);

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	 SET @appCount=(SELECT COUNT(*) FROM trams_temp_pb_application WHERE uuid = @uuid);
	 CALL `trams_log`(@sid, _spName, CONCAT('Total Application: ',@appCount), 1);


	SET @scopeAccRollupQuery = "
	INSERT INTO trams_temp_pb_scope_acc_rollup (projectId_int, section_code, total_reservation_amt, uuid, time_stamp)
	SELECT
		project_id AS projectId_int,
		acc_section_code AS section_code,
		SUM(acc_reservation_amount) AS total_reservation_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_scope_acc
	WHERE
		project_id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid)
	GROUP BY
		project_id,
		funding_source_name,
		section_code ";


	CALL `trams_log`(@sid, _spName, CONCAT('Get scope_acc_rollup Query: ',@scopeAccRollupQuery), 1);

	PREPARE scopeAccRollupStmt FROM @scopeAccRollupQuery;
	EXECUTE scopeAccRollupStmt;

	 SET @cnt=(SELECT COUNT(*) FROM trams_temp_pb_scope_acc_rollup WHERE uuid = @uuid);
	 CALL `trams_log`(@sid, _spName, CONCAT('Total rollup scope_acc_transaction: ',@cnt), 1);

	SET @contractAccTransactionRollupQuery = CONCAT("
	INSERT INTO trams_temp_pb_contract_acc_transaction_rollup (project_id, section_code, project_number, proj_num_no_amend, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, uuid, time_stamp)
	SELECT
		trans.project_id AS project_id,
		trans.section_code AS section_code,
		trans.project_number AS project_number,
		LEFT(trans.project_number, length(trans.project_number) - 2) AS proj_num_no_amend,
		SUM(trans.obligation_amount) AS total_obligation_amt,
		SUM(trans.deobligation_amount) AS total_deobligation_amt,
		SUM(trans.disbursement_amount) AS total_disbursement_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM trams_contract_acc_transaction trans
	-- removed unnecessary funding source join
	WHERE project_id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid) ",
	(SELECT IFNULL(CONCAT(" AND trans.section_code IN (",NULLIF(p_sectionCode,''),")"),"")),
	-- changed to use project id in place of project number
	" GROUP BY trans.project_id, trans.funding_source_name, trans.section_code");

	CALL `trams_log`(@sid, _spName, CONCAT('Get contract_acc_transaction rollup Query: ',@contractAccTransactionRollupQuery), 1);

	PREPARE contractAccTransactionRollupStmt FROM @contractAccTransactionRollupQuery;
	EXECUTE contractAccTransactionRollupStmt;

	-- removed unnecessary temp table
	SET @lineItemRollupQuery = "
	INSERT INTO trams_temp_pb_line_item_rollup (app_id, project_id, section_code, total_project_fta_amt, total_project_non_fta_amt, total_project_eligible_cost, project_number, uuid, time_stamp)
	SELECT
		li.app_id AS app_id,
		li.project_id AS project_id,
		li.section_code AS section_code,
		SUM(li.revised_FTA_amount) AS total_project_fta_amt,
		(SUM(li.revised_total_amount) - SUM(revised_FTA_amount)) AS total_project_non_fta_amt,
		SUM(li.revised_total_amount) AS total_project_eligible_cost,
		p.project_number,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_line_item li
	JOIN
		trams_project p
		on
		li.project_id = p.id
	WHERE
		project_id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid)
	GROUP BY
		project_id,
		app_id,
		section_code";

	CALL `trams_log`(@sid, _spName, CONCAT('Get line_item Rollup Query: ',@lineItemRollupQuery), 1);

	PREPARE lineItemRollupStmt FROM @lineItemRollupQuery;
	EXECUTE lineItemRollupStmt;

	SET @cnt=(SELECT COUNT(*) FROM trams_temp_pb_line_item_rollup WHERE uuid = @uuid);
	CALL `trams_log`(@sid, _spName, CONCAT('Total rollup line_item: ',@cnt), 1);

	INSERT INTO trams_temp_pb_max_project (project_id, section_code, max_project_number, proj_num_no_amend, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, uuid, time_stamp)
	SELECT
		tcatr.project_id,
		tcatr.section_code,
		MAX(tcatr.project_number) AS max_project_number,
		tcatr.proj_num_no_amend,
		tcatr.total_obligation_amt,
		tcatr.total_deobligation_amt,
		tcatr.total_disbursement_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM trams_temp_pb_contract_acc_transaction_rollup tcatr
	WHERE tcatr.uuid = @uuid
	GROUP BY tcatr.section_code, tcatr.proj_num_no_amend;

	INSERT INTO trams_temp_pb_line_item_totals (project_number, proj_num_no_amend, section_code, total_project_fta_amt, total_project_non_fta_amt, total_project_eligible_cost, total_reservation_amt, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, uuid, time_stamp)
	SELECT
		MAX(tcatr2.project_number) AS project_number,
		tcatr2.proj_num_no_amend AS proj_num_no_amend,
		tcatr2.section_code AS section_code,
		tlir.total_project_fta_amt AS total_project_fta_amt,
		tlir.total_project_non_fta_amt AS total_project_non_fta_amt,
		tlir.total_project_eligible_cost AS total_project_eligible_cost,
		tsar.total_reservation_amt AS total_reservation_amt,
		tcatr2.total_obligation_amt AS total_obligation_amt,
		tcatr2.total_deobligation_amt AS total_deobligation_amt,
		tcatr2.total_disbursement_amt AS total_disbursement_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM trams_temp_pb_contract_acc_transaction_rollup tcatr2
	JOIN trams_temp_pb_scope_acc_rollup tsar ON tcatr2.project_id = tsar.projectId_int
		AND tsar.section_code = tcatr2.section_code
		AND tsar.uuid = @uuid
	JOIN trams_temp_pb_line_item_rollup tlir ON tcatr2.project_id = tlir.project_id
		AND tsar.section_code = tlir.section_code
		AND tlir.uuid = @uuid
	WHERE tcatr2.uuid = @uuid
	GROUP BY tcatr2.section_code, tcatr2.proj_num_no_amend;



	SET @finalQuery = CONCAT("
	INSERT INTO trams_temp_pb_resultset (grantee_id, acronym, grantee_name, grantee_cost_center, fiscal_year, application_number, amendment_number, application_name, status, application_type, transmitted_date1, latest_transmitted_date, submitted_date, latest_submitted_date, grantee_poc, pre_award_mgr, post_award_mgr, project_number, project_name, section_code, total_project_fta_amt, total_project_non_fta_amt, total_project_eligible_cost, total_reservation_amt, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, discretionary_funds_bool, major_cap_project_bool, obligated_date_time, Project_Start_Date, Project_End_Date, uuid, time_stamp)
	SELECT
		a.recipient_id AS grantee_id,
		gb.recipientAcronym AS acronym,
		gb.legalBusinessName AS grantee_name,
		a.recipient_cost_center AS grantee_cost_center,
		a.fiscal_year AS fiscal_year,
		a.application_number AS application_number,
		a.amendment_number AS amendment_number,
		a.application_name AS application_name,
		a.`status` AS `status`,
		a.application_type AS application_type,
		CASE
			WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(MAX(fr.transmitted_date))
		END AS transmitted_date,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00',",p_gmtOffset,") AS char)),
		CASE
			WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(MAX(fr.submitted_date))
		END AS submitted_date,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00',",p_gmtOffset,") AS char)),
		a.recipient_contact AS grantee_poc,
		a.fta_pre_award_manager AS pre_award_mgr,
		a.fta_post_award_manager AS post_award_mgr,
		p.project_number AS project_number,
		p.project_title AS project_name,
		max_project.section_code AS section_code,
		totals.total_project_fta_amt,
		totals.total_project_non_fta_amt,
		totals.total_project_eligible_cost,
		totals.total_reservation_amt,
		totals.total_obligation_amt,
		totals.total_deobligation_amt,
		totals.total_disbursement_amt,
		a.discretionary_funds_flag AS discretionary_funds_bool,
		p.major_capital_project_flag AS major_cap_project_bool,	
		trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time,'+00:00',",p_gmtOffset,") AS char)) AS obligated_date_time,		
		(SELECT trams_format_date(MIN(tms.estimated_completion_date_time)) FROM trams_milestones tms WHERE tms.line_item_id = tli.id) AS Project_Start_Date,
		(SELECT trams_format_date(MAX(tms.estimated_completion_date_time)) FROM trams_milestones tms WHERE tms.line_item_id = tli.id) AS Project_End_Date,		
		@uuid AS uuid,
		@sid AS time_stamp
	FROM trams_project p
	JOIN trams_temp_pb_max_project max_project
	-- switched to use project id instead of project number
		ON p.id = max_project.project_id
		AND max_project.uuid = @uuid
	JOIN trams_application a ON a.id = p.application_id
	JOIN vwtramsGeneralBusinessInfo gb ON a.recipient_id = gb.recipientId
	LEFT JOIN trams_fta_review fr ON a.id = fr.application_id
	LEFT JOIN trams_application_award_detail aa ON a.application_number = aa.application_number
	JOIN trams_temp_pb_line_item_totals totals
		ON max_project.max_project_number = totals.project_number
		AND totals.section_code = max_project.section_code
		AND totals.uuid = @uuid
	LEFT JOIN
		(
			SELECT
				a.id AS applicationId,
				MAX(th.TransmittedDate) AS latest_transmitted_date,
				MAX(th.SubmittedDate) AS latest_submitted_date,
				MIN(th.TransmittedDate) AS original_transmitted_date,
				MIN(th.SubmittedDate) AS original_submitted_date
			FROM
				tramsApplicationTransmissionHistory th
			JOIN
				trams_application a
			ON
				a.id = th.applicationId
			GROUP BY th.applicationId
		) latest_transmission_history
		ON a.id = latest_transmission_history.applicationId
	LEFT JOIN trams_line_item tli ON tli.project_id = p.id
	WHERE p.id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid)"
	,(SELECT IFNULL(CONCAT(" AND max_project.section_code IN (",NULLIF(p_sectionCode,''),")"),"")),
	"GROUP BY project_number, section_code
	order by a.recipient_id");

	CALL `trams_log`(@sid, _spName, CONCAT('Get Final Query: ',@finalQuery), 1);

	PREPARE finalQueryStmt FROM @finalQuery;
	EXECUTE finalQueryStmt;


	INSERT INTO trams_temp_pb_no_contract_award_line_item_rollup (scope_code, app_id, project_id, section_code, funding_source_name, project_number, total_project_fta_amt, total_project_non_fta_amt, total_project_eligible_amt, uuid, time_stamp)
	SELECT
		li.scope_code,
		li.app_id AS app_id,
		li.project_id AS project_id,
		li.section_code AS section_code,
		li.funding_source_name,
		MAX(p.project_number) AS project_number,
		SUM(li.revised_FTA_amount) AS total_project_fta_amt,
		(SUM(li.revised_total_amount) - SUM(li.revised_FTA_amount)) AS total_project_non_fta_amt,
		SUM(li.revised_total_amount) AS total_project_eligible_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_line_item li
	JOIN
		trams_project p
	ON
		li.project_id = p.id
	WHERE
		project_id IN (SELECT project_id FROM trams_temp_pb_application WHERE (contractAward_id IS NULL OR contractAward_id = '' OR application_id > current_amendment_number) AND uuid = @uuid)
	GROUP BY
	-- switched to use project id instead of project number
		project_id, section_code;


	INSERT INTO trams_temp_pb_no_contract_award_scope_acc_rollup(scope_name, scope_number, project_number, projectId_int, section_code, total_acc_reservation_amt, uuid, time_stamp)
	SELECT
		scope_name,
		scope_number,
		project_number,
		project_id AS projectId_int,
		acc_section_code AS section_code,
		SUM(acc_reservation_amount) AS total_acc_reservation_amt,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_scope_acc sa
	WHERE
		project_id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid)
	GROUP BY
	-- switched to use project id instead of project number
		project_id, section_code;


	SET @appCount=(SELECT COUNT(*) FROM trams_temp_pb_resultset WHERE uuid = @uuid  );
	CALL `trams_log`(@sid, _spName, CONCAT('Total Matched Transactions: ',@appCount), 1);


	SET @finalQuery = CONCAT("
	INSERT INTO trams_temp_pb_resultset (grantee_id, acronym, grantee_name, grantee_cost_center, fiscal_year, application_number, amendment_number, application_name, status, application_type, transmitted_date1, latest_transmitted_date, submitted_date, latest_submitted_date, grantee_poc, pre_award_mgr, post_award_mgr, project_number, project_name, section_code, total_project_fta_amt, total_project_non_fta_amt, total_project_eligible_cost, total_reservation_amt, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, discretionary_funds_bool, major_cap_project_bool, obligated_date_time, Project_Start_Date, Project_End_Date, uuid, time_stamp)
	SELECT
		a.recipient_id AS grantee_id,
		gb.recipientAcronym AS acronym,
		gb.legalBusinessName AS grantee_name,
		a.recipient_cost_center AS grantee_cost_center,
		a.fiscal_year AS fiscal_year,
		a.application_number AS application_number,
		a.amendment_number AS amendment_number,
		a.application_name AS application_name,
		a.`status` AS `status`,
		a.application_type AS application_type,
		CASE
			WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(MAX(fr.transmitted_date))
		END AS transmitted_date,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00',",p_gmtOffset,") AS char)),
		CASE
			WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', ",p_gmtOffset,") AS char))
			ELSE trams_format_date(MAX(fr.submitted_date))
		END AS submitted_date,
		trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00',",p_gmtOffset,") AS char)),
		a.recipient_contact AS grantee_poc,
		a.fta_pre_award_manager AS pre_award_mgr,
		a.fta_post_award_manager AS post_award_mgr,
		p.project_number AS project_number,
		p.project_title AS project_name,
		lir.section_code AS section_code,
		lir.total_project_fta_amt AS total_project_fta_amt,
		lir.total_project_non_fta_amt AS total_project_non_fta_amt,
		lir.total_project_eligible_amt AS total_project_eligible_amt,
		sar.total_acc_reservation_amt AS total_acc_reservation_amt,
		'' AS total_obligation_amt,
		'' AS total_deobligation_amt,
		'' AS total_disbursement_amt,
		a.discretionary_funds_flag AS discretionary_funds_bool,
		p.major_capital_project_flag AS major_cap_project_bool,		
		trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time,'+00:00',",p_gmtOffset,") AS char)) AS obligated_date_time,		
		(SELECT trams_format_date(MIN(tms.estimated_completion_date_time)) FROM trams_milestones tms WHERE tms.line_item_id = tli.id) AS Project_Start_Date,
		(SELECT trams_format_date(MAX(tms.estimated_completion_date_time)) FROM trams_milestones tms WHERE tms.line_item_id = tli.id) AS Project_End_Date,
		@uuid AS uuid,
		@sid AS time_stamp
	FROM
		trams_project p
	JOIN
		trams_application a ON a.id = p.application_id
	JOIN
		vwtramsGeneralBusinessInfo gb ON a.recipient_id = gb.recipientId
	LEFT JOIN
		trams_fta_review fr ON a.id = fr.application_id
	LEFT JOIN
		trams_application_award_detail aa ON a.application_number = aa.application_number
	LEFT JOIN
	-- switched to use project id instead of project number
		trams_temp_pb_no_contract_award_line_item_rollup lir ON p.id = lir.project_id
		AND lir.uuid = @uuid
	LEFT JOIN
	-- switched to use project id instead of project number
		trams_temp_pb_no_contract_award_scope_acc_rollup sar ON lir.project_id = sar.projectId_int
		AND lir.section_code = sar.section_code
		AND sar.uuid = @uuid
	LEFT JOIN
		trams_line_item tli ON tli.project_id = p.id
	LEFT JOIN
		(
			SELECT
				a.id AS applicationId,
				MAX(th.TransmittedDate) AS latest_transmitted_date,
				MAX(th.SubmittedDate) AS latest_submitted_date,
				MIN(th.TransmittedDate) AS original_transmitted_date,
				MIN(th.SubmittedDate) AS original_submitted_date
			FROM
				tramsApplicationTransmissionHistory th
			JOIN
				trams_application a
			ON
				a.id = th.applicationId
			GROUP BY th.applicationId
		) latest_transmission_history
		ON a.id = latest_transmission_history.applicationId
	LEFT JOIN
		trams_contract_award ca ON ca.id = a.contractAward_ID
	WHERE
		p.id IN (SELECT project_id FROM trams_temp_pb_application WHERE uuid = @uuid)
		AND (a.contractAward_Id is null OR contractAward_Id = '' OR a.id > ca.current_amendment_number)", (SELECT IFNULL(CONCAT(" AND lir.section_code IN ('",NULLIF(p_sectioncode,''),"')"),
	-- switched to use project id instead of project number
	"GROUP BY p.id, section_code"
	))
	);


	CALL `trams_log`(@sid, _spName, CONCAT('Perform Insert SQL: ', now(), ' ',@finalQuery), 1);

	 PREPARE finalQueryStmt FROM @finalQuery;
	 EXECUTE finalQueryStmt;

	 SET @appCount=(SELECT COUNT(*) FROM trams_temp_pb_resultset WHERE uuid = @uuid  );

	 CALL `trams_log`(@sid, _spName, CONCAT('Total Applications: ', now(), ' ',@appCount), 1);

	CALL `trams_log`(@sid, _spName, CONCAT('Final Selection Starting ', now()), 1);

	SELECT
		tmp.grantee_id,
		tmp.acronym,
		tmp.grantee_name,
		tmp.grantee_cost_center,
		tmp.fiscal_year,
		tmp.application_number,
		tmp.amendment_number,
		tmp.application_name,
		tmp.status,
		tmp.application_type,
		IF(tmp.transmitted_date1='0000-00-00 00:00:00', '', trams_format_date(tmp.transmitted_date1)) AS transmitted_date,
		IF(tmp.latest_transmitted_date='0000-00-00', '', trams_format_date(tmp.latest_transmitted_date)) AS latest_transmitted_date,
		IF(tmp.submitted_date='0000-00-00 00:00:00', '', trams_format_date(tmp.submitted_date)) AS submitted_date,
		IF(tmp.latest_submitted_date='0000-00-00', '', trams_format_date(tmp.latest_submitted_date)) AS latest_submitted_date,
		tmp.grantee_poc,
		tmp.pre_award_mgr,
		tmp.post_award_mgr,
		tmp.project_number,
		tmp.project_name,
		tmp.section_code,
		tmp.total_project_fta_amt,
		tmp.total_project_non_fta_amt,
		tmp.total_project_eligible_cost,
		tmp.total_reservation_amt,
		tmp.total_obligation_amt,
		tmp.total_deobligation_amt,
		tmp.total_disbursement_amt,
		tmp.discretionary_funds_bool,
		tmp.major_cap_project_bool,
		IF(tmp.obligated_date_time='0000-00-00 00:00:00', '', trams_format_date(tmp.obligated_date_time)),		
		IF(tmp.Project_Start_Date='0000-00-00 00:00:00', '', trams_format_date(tmp.Project_Start_Date)),
		IF(tmp.Project_End_date='0000-00-00 00:00:00', '', trams_format_date(tmp.Project_End_Date))		
	FROM trams_temp_pb_resultset tmp
	WHERE tmp.uuid = @uuid
	GROUP BY project_number, section_code;

	DELETE FROM trams_temp_pb_application WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_scope_acc_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_contract_acc_transaction_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_line_item_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_max_project WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_line_item_totals WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_resultset WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_no_contract_award_line_item_rollup WHERE uuid = @uuid;
	DELETE FROM trams_temp_pb_no_contract_award_scope_acc_rollup WHERE uuid = @uuid;

	CALL `trams_log`(@sid, _spName, CONCAT('Done! ', now()), 1);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_line_item_by_project_id`(IN `p_active` INT, IN `p_project_id` VARCHAR(50))
BEGIN

  declare _sql varchar(40960) default "";
  declare _whereClause varchar(4096) default " ";
  declare _limitClause varchar(100) default " ";
  

  set _sql = "
  select id as lineItemId ,
   status as status_text,
   created_date_time as dateCreated_datetime,
   modified_date_time as dateModified_datetime,
   modified_by as modifiedBy_text,
   duns as recipientDUNS_text ,
   section_code as sectionCode_text ,
   scc_line_item_name as sccItemName_text,
   scc_line_item_code as sccItemCode_text,
   scope_detailed_name as scopeDetailedName_text ,
   scope_code as scopeCode_text ,
   line_item_number as lineItemNumber_text ,
   activity_name as activityName_text ,
   activity_code as activityCode_text ,
   allocation_name as allocationName_text ,
   allocation_code as allocationCode_text ,
   line_item_type_name as itemTypeName_text,
   line_item_type_code as itemTypeCode_text,
   custom_item_name as customItemName_text ,
   extended_budget_description as description_text ,
   quantity as quantity_int,
   total_amount as totalAmount_dec,
   fuel_name as fuel_text ,
   propulsion as propulsion_text ,
   vehicle_condition as vehicleCondition_text ,
   vehicle_size as vehicleSize_int ,
   latest_amend_quantity as latestAmendQuantity_int,
   latest_revised_quantity as latestRevisedQuantity_int ,
   project_id as projectId ,
   app_id as appId ,
   original_line_item_id as originalLineItemId ,
   revised_line_item_description as revisedItemDescription_text ,
   latest_amend_total_amount as latestAmendQuantity_int,
   revised_total_amount as revisedTotalAmount_dec ,
   funding_source_name as fundingSourceName_text ,
   original_FTA_amount as originalFTAAmount_dec ,
   latest_amend_fta_amount as latestAmendFTAAmount_dec ,
   revised_FTA_amount as revisedFTAAmount_dec ,
   original_state_amount as originalStateAmount_dec ,
   latest_amend_state_amount as latestAmendStateAmount_dec ,
   revised_state_amount as revisedStateAmount_dec ,
   original_local_amount as originalLocalAmount_dec ,
   latest_amend_local_amount as latestAmendLocalAmount_dec ,
   revised_local_amount as revisedLocalAmount_dec ,
   original_other_fed_amount as originalOtherFEDAmount_dec ,
   latest_amend_other_fed_amount as latestAmendOtherFEDAmount_dec ,
   revised_other_fed_amount as revisedOtherFEDAmount_dec ,
   original_state_in_kind_amount as originalStateInKindAmount_dec ,
   latest_amend_state_inkind_amount as latestAmendStateInKindAmount_dec ,
   revised_state_in_kind_amount as revisedStateInKindAmount_dec ,
   original_local_in_kind_amount as originalLocalInKindAmount_dec ,
   latest_amend_local_inkind_amount as latestAmendLocalInKindAmount_dec ,
   revised_local_in_kind_amount as revisedLocalInKindAmount_dec ,
   original_toll_revenue_amount as originalTollRevenueAmount_dec ,
   latest_amend_toll_rev_amount as latestAmendTollRevenueAmount_dec ,
   revised_toll_revenue_amount as revisedTollRevenueAmount_dec ,
   third_party_flag as thirdPartyFlag_bool 
   from trams_line_item where 1 ";
  
    
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and project_id in (",NULLIF(p_project_id,''),")"),"")));  
  
  SET @finalQuery = concat(_sql,_whereClause,_limitClause);
  
  set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_line_item_by_project_id', concat('Get trams_get_line_item_by_project_id Query: ',@finalQuery), 1); 
  
  PREPARE finalStmt FROM @finalQuery;
  EXECUTE finalStmt;

  CALL `trams_log`(@sid, 'trams_get_line_item_by_project_id', 'Done!', 1);    
    
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_scope_acc_reservation_apportionment`(IN `p_cost_center_codes` VARCHAR(4096), IN `p_section_codes` VARCHAR(4096), IN `p_uza_codes` VARCHAR(4096), IN `p_scope_codes` VARCHAR(4096), IN `p_project_ids` VARCHAR(4096), IN `p_funding_source_names` VARCHAR(4096), IN `p_uza_reference_version` INT(11))
BEGIN

DECLARE _fiscal_year VARCHAR(4);
DECLARE _done INT DEFAULT 0;
DECLARE _cost_center_code VARCHAR(6);
DECLARE _uza_code VARCHAR(6);

DECLARE parameter_cursor CURSOR FOR SELECT 
  cost_center_code, 
  uza_code 
FROM tmp_costCenterCodes c 
  JOIN tmp_uzaCodes u ON 
    c.id = u.id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done = 1;

SET _fiscal_year = fn_get_cur_fiscal_year(now());

DROP TEMPORARY TABLE IF EXISTS tmp_acc_acchistory_rollup;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_resv_apportionment_rollup;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation_apportionment;
DROP TEMPORARY TABLE IF EXISTS tmp_costCenterCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_uzaCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_sectionCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_projectIds;
DROP TEMPORARY TABLE IF EXISTS tmp_scopeCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_fundingSourceNames;
DROP TEMPORARY TABLE IF EXISTS tmp_get_scope_acc_resv_apport_result;

/* For Logging*/
SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter Cost Center: ', p_cost_center_codes), 0); 
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter Section Code: ', p_section_codes), 0); 
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter UZA Code: ', p_uza_codes), 0); 
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter Project Id: ', p_project_ids), 0); 
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter Scope Code: ', p_scope_codes), 0); 
CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Parameter Funding Source Name: ', p_funding_source_names), 0);

CREATE TEMPORARY TABLE tmp_costCenterCodes(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, cost_center_code VARCHAR(6)) engine=memory;
SET @costCenterQuery = CONCAT(
  'INSERT INTO tmp_costCenterCodes (cost_center_code) VALUES ',
  p_cost_center_codes
);
PREPARE costCenterStmt FROM @costCenterQuery;
EXECUTE costCenterStmt;

CREATE TEMPORARY TABLE tmp_uzaCodes(id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, uza_code VARCHAR(6)) engine=memory;
SET @uzaQuery = CONCAT(
  'INSERT INTO tmp_uzaCodes (uza_code) VALUES ',
  p_uza_codes
);
PREPARE uzaStmt FROM @uzaQuery;
EXECUTE uzaStmt;

CREATE TEMPORARY TABLE tmp_sectionCodes(section_code VARCHAR(4)) engine=memory;
SET @sectionQuery = CONCAT(
  'INSERT INTO tmp_sectionCodes (section_code) SELECT 
    DISTINCT acc_section_code 
  FROM `trams_account_class_code` 
  WHERE acc_section_code IN (',
  p_section_codes,
  ')'
);
PREPARE sectionStmt FROM @sectionQuery;
EXECUTE sectionStmt;

CREATE TEMPORARY TABLE tmp_projectIds(project_id int) engine=memory;
SET @projectQuery = CONCAT(
  'INSERT INTO tmp_projectIds (project_id) SELECT 
    id 
  FROM trams_project 
  WHERE id IN (',
  p_project_ids,
  ')'
);
PREPARE projectStmt FROM @projectQuery;
EXECUTE projectStmt;

CREATE TEMPORARY TABLE tmp_scopeCodes(scope_number VARCHAR(20)) engine=memory;
SET @scopeQuery = CONCAT(
  'INSERT INTO tmp_scopeCodes (scope_number) SELECT 
    DISTINCT scope_number 
  FROM `trams_scope_acc`  
  WHERE scope_number IN (',
  p_scope_codes,
  ')'
);
PREPARE scopeStmt FROM @scopeQuery;
EXECUTE scopeStmt;

CREATE TEMPORARY TABLE tmp_fundingSourceNames(funding_source_name VARCHAR(50)) engine=memory;
SET @fundingSourceQuery = CONCAT(
  'INSERT INTO tmp_fundingSourceNames (funding_source_name) SELECT 
    DISTINCT funding_source_name 
  FROM `trams_scope_acc`  
  WHERE funding_source_name IN (',
  p_funding_source_names,
  ')'
);
PREPARE fundingSourceStmt FROM @fundingSourceQuery;
EXECUTE fundingSourceStmt;

open parameter_cursor;

  -- begin loop through the parameters 
  SET @createResultTable = true;
  get_result: LOOP
    CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', 'In loop',0);
    
    -- fetch the parameter
    fetch parameter_cursor into _cost_center_code, _uza_code;
    
    IF _done = 1 THEN
      leave get_result;
    END IF;
    
    DROP TEMPORARY TABLE IF EXISTS tmp_acc_acchistory_rollup;  
    CREATE TEMPORARY TABLE tmp_acc_acchistory_rollup engine=memory 
      SELECT 
        a.account_class_code,
        a.acc_description AS `acct_class_code_desc`,
        a.acc_funding_fiscal_year AS `funding_fiscal_year`,
        a.acc_appropriation_code AS `appropriation_code`,
        a.acc_section_code AS `section_code`,
        a.acc_limitation_code AS `limitation_code`,
        a.acc_auth_type AS `auth_type`,
        a.code_new_amount AS initialamtdec,
        max(a.code_new_amount) 
          + ifnull(sum(acch.`transfer_in_amount`), 0) 
          + ifnull(sum(acch.`unreserve_amount`), 0)
          - ifnull(sum(acch.`reserved_amount`), 0) 
          - ifnull(sum(acch.`obligated_amount`), 0) 
          - ifnull(sum(acch.`transfer_out_amount`), 0) 
          AS `oper_budget_avail`,
        `a`.`trmsprtn_ccntclss_prtngbdg` AS `opb_id`,
        `a`.`id` AS `acctClassCode_Id`,
         a.cost_center_name AS `cost_center`,
        `a`.`cost_center_code` AS `cost_center_code`,
        `a`.`fiscal_year` AS `fiscal_year`    
      FROM `trams_account_class_code` a 
        LEFT JOIN trams_account_class_code_history acch ON 
          `a`.`account_class_Code` = acch.account_class_code
          AND `a`.`fiscal_year` = acch.transaction_fiscal_year 
          AND `a`.`cost_center_code` = acch.cost_center_code
      WHERE a.authorized_flag = 1 
        AND a.cost_center_code = _cost_center_code
        AND a.acc_section_code IN (SELECT section_code FROM tmp_sectionCodes)
        AND a.fiscal_year = _fiscal_year
      GROUP BY `a`.`account_class_code`,
        `a`.`fiscal_year`,
        `a`.`cost_center_code`;
    SET @cnt = (SELECT count(*) FROM tmp_acc_acchistory_rollup);
    CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Total tmp_acc_acchistory_rollup: ', @cnt), 0);   
  
    DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation;
    CREATE TEMPORARY TABLE tmp_acc_reservation engine=memory 
      SELECT 
        uuid_short() AS `a_id`,
        `accf`.`account_class_code` AS `acct_class_code`,
        `accf`.`fpc` AS `fpc`,
        `a`.*
      FROM `trams_acc_fpc_reference` `accf` 
        JOIN `tmp_acc_acchistory_rollup` a 
          ON `a`.`account_class_code` = `accf`.`account_class_code`
      GROUP BY 
        `accf`.`account_class_code`,
        `accf`.`fpc`;
    SET @cnt = (SELECT count(*) FROM tmp_acc_reservation);
    CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Total tmp_acc_reservation: ', @cnt), 0); 
    
    DROP TEMPORARY TABLE IF EXISTS tmp_acc_resv_apportionment_rollup;
    CREATE TEMPORARY TABLE tmp_acc_resv_apportionment_rollup engine=memory  
      SELECT 
        a.uza_code AS `uza_code`,
        a.funding_fiscal_year,
        a.appropriation_code,
        a.section_id,
        a.limitation_code,
        max(ifnull(a.apportionment,0)) AS apportionment,
        max(ifnull(a.operating_cap,0)) AS operating_cap,
        max(ifnull(a.ceiling_amount,0)) AS ceiling_amount,
        sum(ifnull(ah.reserved_amount,0)) AS reserved_amount,
        sum(ifnull(ah.unreserve_amount,0)) AS unreserve_amount,
        sum(ifnull(ah.obligated_amount,0)) AS obligated_amount,
        sum(ifnull(ah.recovery_amount,0)) AS recovery_amount,
        sum(ifnull(ah.transfer_in_amount,0)) AS transfer_in_amount,
        sum(ifnull(ah.transfer_out_amount,0)) AS transfer_out_amount,
        sum(ifnull(ah.modified_amount,0)) AS modified_amount,
        (
          (
            (
              (
                (
                  (
                    (
                      max(ifnull(`a`.`apportionment`,0)) 
                        + sum(ifnull(`ah`.`modified_amount`,0))
                    ) + sum(ifnull(`ah`.`transfer_in_amount`,0))
                  ) + sum(ifnull(`ah`.`unreserve_amount`,0))
                ) + sum(ifnull(`ah`.`recovery_amount`,0))
              ) - sum(ifnull(`ah`.`reserved_amount`,0))
            ) - sum(ifnull(`ah`.`obligated_amount`,0))
          ) - sum(ifnull(`ah`.`transfer_out_amount`,0))
        ) AS `available_amount`
      FROM trams_apportionment a 
        LEFT JOIN trams_apportionment_history ah ON 
          a.uza_code = ah.uza_code 
          AND a.funding_fiscal_year = ah.funding_fiscal_year
          AND a.appropriation_code = ah.appropriation_code 
          AND a.section_id = ah.section_id
          AND a.limitation_code = ah.limitation_code 
          AND ah.transaction_fiscal_year = _fiscal_year
      WHERE a.active_flag = 1 
        AND a.section_id IN (SELECT section_code FROM tmp_sectionCodes) 
        AND ah.uza_code = _uza_code
      GROUP BY a.uza_code,
        a.funding_fiscal_year,
        a.appropriation_code,
        a.section_id,
        a.limitation_code;
    SET @cnt = (SELECT count(*) FROM tmp_acc_resv_apportionment_rollup);
    CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Total tmp_acc_resv_apportionment_rollup: ', @cnt), 0); 
    
    DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation_apportionment;
    CREATE TEMPORARY TABLE tmp_acc_reservation_apportionment engine=memory
      SELECT 
        `ap`.`uza_code` AS `uza_code`,
        `uv`.`uza_name` AS `uza_name`,
        `ac`.`acct_class_code` AS `account_class_code`,
        `ac`.`acct_class_code_desc` AS `acct_class_code_desc`,
        `ac`.`funding_fiscal_year` AS `funding_fiscal_year`,
        `ac`.`appropriation_code` AS `appropriation_code`,
        `ac`.`section_code` AS `section_code`,
        `ac`.`limitation_code` AS `limitation_code`,
        `ac`.`auth_type` AS `auth_type`,
        `ac`.`fpc` AS `fpc`,
        `ac`.`oper_budget_avail` AS `oper_budget_avail`,
        `ac`.`opb_id` AS `opb_id`,
        `ac`.`cost_center` AS `cost_center`,
        `ac`.`cost_center_code` AS `cost_center_code`,
        `ac`.`fiscal_year` AS `fiscal_year`,
        ifnull(`ap`.`available_amount`,0) AS `available_amount`,
        ifnull(`ap`.`operating_cap`,0) AS `operating_cap`,
        ifnull(`ap`.`ceiling_amount`,0) AS `ceiling_amount` 
      FROM (`tmp_acc_reservation` `ac` 
        LEFT JOIN `tmp_acc_resv_apportionment_rollup` `ap` ON 
          `ac`.`funding_fiscal_year` = `ap`.`funding_fiscal_year` 
          AND `ac`.`section_code` = `ap`.`section_id`
          AND `ac`.`limitation_code` = `ap`.`limitation_code` 
          AND `ac`.`appropriation_code` = `ap`.`appropriation_code`
        JOIN `trams_uza_code_reference` `uv` ON 
          `ap`.`uza_code` = `uv`.`uza_code`)
      WHERE `uv`.`version` = p_uza_reference_version;
    SET @cnt = (SELECT count(*) FROM tmp_acc_reservation_apportionment);
    CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', CONCAT('Total tmp_acc_reservation_apportionment: ', @cnt), 0); 
    
    /* Final Result (create the result table from the result data if the result table not exists, otherwise insert the results) */
    SET @selectResultQuery = "    
      SELECT 
        s.project_id AS projectId,
        s.project_number AS projectNumber_text,
        s.scope_number AS scopeNumber_text,
        s.scope_name AS scopeName_text,
        s.funding_source_name AS fundingSourceName_text,  
        a.uza_code AS uzaCode_text,
        a.uza_name AS uzaName_text,
        a.account_class_code AS accountClassCode_text,
        a.acct_class_code_desc AS accountClassCodeDesc_text,
        a.funding_fiscal_year AS fundingFiscalYear_text,
        a.appropriation_code AS appropriationCode_text,
        a.section_code AS sectionCode_text,
        a.limitation_code AS limitationCode_text,
        a.auth_type AS authType_text,
        a.fpc AS fpc_text,
        a.opb_id AS operatingBudgetId,
        a.oper_budget_avail AS operBudgetAvail_dec,
        a.cost_center AS costCenter_text,
        a.cost_center_code AS costCenterCode_text,
        a.fiscal_year AS fiscalYear_text,
        a.available_amount AS apportionmentAvailable_dec,
        s.acc_reservation_amount AS accReservationAmount_dec,
        a.operating_cap AS operatingCap_dec,
        a.ceiling_amount AS ceiling_dec 
      FROM tmp_acc_reservation_apportionment a 
        JOIN trams_scope_acc s ON 
          a.uza_code = s.funding_uza_code 
          AND a.cost_center_code = s.cost_center_code 
          AND a.fpc = s.acc_fpc 
          AND a.account_class_code = s.account_class_code
      WHERE s.project_id IN (SELECT project_id FROM tmp_projectIds)
        AND s.scope_number IN (SELECT scope_number FROM tmp_scopeCodes) 
        AND s.funding_source_name IN (SELECT funding_source_name FROM tmp_fundingSourceNames)
      GROUP BY s.project_id, 
        s.scope_number, 
        s.funding_source_name,
        a.uza_code,
        a.account_class_code, 
        a.opb_id, 
        a.cost_center_code, 
        a.fiscal_year, 
        a.fpc";
      
    -- create table with the data 
    IF @createResultTable = true THEN
      DROP TEMPORARY TABLE IF EXISTS tmp_get_scope_acc_resv_apport_result;
      SET @createResultTableQuery = CONCAT("CREATE TEMPORARY TABLE tmp_get_scope_acc_resv_apport_result engine=memory ", @selectResultQuery);
      PREPARE createResultTableStmt FROM @createResultTableQuery;
      EXECUTE createResultTableStmt;
      
      SET @createResultTable = false;
      
    ELSE
      SET @insertResultTableQuery = CONCAT("insert into tmp_get_scope_acc_resv_apport_result ", @selectResultQuery);
      PREPARE insertResultTableStmt FROM @insertResultTableQuery;
      EXECUTE insertResultTableStmt;
      
    END IF;    
    
  END LOOP get_result;
  
  /*Return result*/
  SELECT DISTINCT * FROM tmp_get_scope_acc_resv_apport_result;

close parameter_cursor;  
/* end of loop of parameters*/

DROP TEMPORARY TABLE IF EXISTS tmp_acc_acchistory_rollup;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_resv_apportionment_rollup;
DROP TEMPORARY TABLE IF EXISTS tmp_acc_reservation_apportionment;
DROP TEMPORARY TABLE IF EXISTS tmp_costCenterCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_uzaCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_get_scope_acc_resv_apport_result;
DROP TEMPORARY TABLE IF EXISTS tmp_sectionCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_projectIds;
DROP TEMPORARY TABLE IF EXISTS tmp_scopeCodes;
DROP TEMPORARY TABLE IF EXISTS tmp_fundingSourceNames;

CALL `trams_log`(@sid, 'trams_get_scope_acc_reservation_apportionment', 'Done!', 0); 

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_small_uza_apportionment_excel_report`(IN p_uzaCode VARCHAR(100), IN p_deobligationFiscalYear_text VARCHAR(100), IN p_fundingFiscalYear VARCHAR(100),IN p_appropriationCode VARCHAR(150),IN p_sectionCode VARCHAR(200), IN p_limitationCode VARCHAR(500), IN p_UZACodeVersionNumber INT(10), IN p_gmtOffset VARCHAR(10))
    NO SQL
    DETERMINISTIC
BEGIN
		
		DECLARE _selectStatement VARCHAR(4096);
		DECLARE _fromStatement VARCHAR(4096);
		DECLARE _whereClause VARCHAR(4096);
		DECLARE _orderBy VARCHAR(4096);
		
		/* Preparing SQL Statement */
		SET _selectStatement = 
		CONCAT(
		"SELECT
			deob.acc_uza_code AS UZA_Code,
			ref.parent_uza_code AS Parent_UZA_Code,
			deob.acc_funding_fiscal_year AS Funding_Fiscal_Year,
			deob.acc_appropriation_code AS Appropriation_code,
			deob.acc_section_code AS Section_Code,
			deob.acc_limitation_code AS Limitation_Code,
			deob.deobligated_fiscal_year AS Deobligation_Fiscal_Year,
			trams_format_date(cast(CONVERT_TZ(deob.deobligated_recovery_date_time, '+00:00',",p_gmtOffset,") AS char)) As Deobligation_Date,
			deob.obligation_fiscal_year AS Obligation_Fiscal_Year,
			IFNULL(CONCAT('$', FORMAT(deob.deobligated_recovery_amount, 2)), '$0') AS Deobligation_Amount ");
		SET _fromStatement = 
		"FROM trams_deobligation_detail deob
			JOIN trams_uza_code_reference ref ON ref.uza_code = deob.acc_uza_code
				AND ref.uza_code <> ref.parent_uza_code ";
		SET _whereClause = 
		CONCAT( 
		"WHERE
			ref.version = ", p_UZACodeVersionNumber, 
			" AND deob.deobligated_recovery_type IN ('Discretionary','Non-Lapsed Deob','Lapsed Year Deob', 'Current Year Deob') ");
		SET _orderBy =
		"ORDER BY
			deob.acc_uza_code,
			deob.deobligated_fiscal_year ";
		
		/* Applying any filters inputed on the form */
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.acc_uza_code IN (",NULLIF(p_uzaCode,''),")"),"")));
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.deobligated_fiscal_year IN (",NULLIF(p_deobligationFiscalYear_text,''),")"),"")));
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.acc_funding_fiscal_year IN (",NULLIF(p_fundingFiscalYear,''),")"),"")));
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.acc_appropriation_code IN (",NULLIF(p_appropriationCode,''),")"),"")));
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.acc_section_code IN (",NULLIF(p_sectionCode,''),")"),"")));
		SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND deob.acc_limitation_code IN (",NULLIF(p_limitationCode,''),")"),"")));
		
		/* Concatenating the SQL and executing it */
		SET @mainQuery = CONCAT(_selectStatement, _fromStatement, _whereClause, _orderBy);
		PREPARE smallUZAApportionment FROM @mainQuery;
		EXECUTE smallUZAApportionment;
		
	END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_operbud_report_excel`(
  IN `p_operbudFiscalYear` VARCHAR(255),
  IN `p_costCenterCode` VARCHAR(512),
  IN `p_fundingFiscalYear` VARCHAR(255),
  IN `p_appropriationCode` VARCHAR(255),
  IN `p_sectionCode` VARCHAR(255),
  IN `p_limitationCode` VARCHAR(255),
  IN `p_authorizationType` VARCHAR(255),
  IN `p_operbudStatus` VARCHAR(1024),
  IN `p_appropriation_type` VARCHAR(20),
  IN `p_return_limit` VARCHAR(20)
)
    NO SQL
BEGIN

  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
  DECLARE _limitClause VARCHAR(100) DEFAULT " ";

  CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.fiscal_year = '",NULLIF(p_operbudFiscalYear,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.cost_center_code = '",NULLIF(p_costCenterCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_funding_fiscal_year = '",NULLIF(p_fundingFiscalYear,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_limitation_code = '",NULLIF(p_limitationCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_appropriation_code = '",NULLIF(p_appropriationCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_section_code = '",NULLIF(p_sectionCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_auth_type = '",NULLIF(p_authorizationType,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.status IN (",NULLIF(p_operbudStatus,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND al.appropriation_type_flag IN (",NULLIF(p_appropriation_type,''),")"),"")));

  SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
  -- added UUID for session reference
  SET @uuid = UUID_SHORT();

  SET @tempAuthorizedQuery = "";
  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_operbud_amounts_id, uuid, and sid columns
  SET @tempAuthorizedQuery = CONCAT("INSERT INTO trams_temp_operbud_authorized
    SELECT
      NULL AS trams_temp_operbud_amounts_id,
      a.account_class_code AS `account_class_code`,
      a.fiscal_year AS `fiscal_year`,
      a.cost_center_code AS `cost_center_code`,
      sum(a.code_new_amount) AS `sum_code_new_amount`,
      @uuid AS uuid,
      @sid AS sid
    FROM `trams_account_class_code_lookup` al
      JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
      JOIN trams_operating_budget o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
    WHERE o.status='Authorized' AND a.authorized_flag=1 AND a.code_transaction_type <> 'Recovery' ",_whereClause,"
    GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`  ",_limitClause);

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('Complete Temporary Operbud Query: ',@tempAuthorizedQuery), 1);
  PREPARE tmpoperbudAuthorizedStmt FROM @tempAuthorizedQuery;
  EXECUTE tmpoperbudAuthorizedStmt;

  SET @tempAmountsQuery = "";
  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_operbud_amounts_id, uuid, and sid columns
  SET @tempAmountsQuery = CONCAT("INSERT INTO trams_temp_operbud_amounts
    SELECT
      NULL AS trams_temp_operbud_amounts_id,
      acch.account_class_code,
      acch.transaction_fiscal_year,
      acch.cost_center_code,
      SUM(acch.transfer_in_amount) AS `transfer_in_amount`,
      SUM(acch.transfer_out_amount) AS `transfer_out_amount`,
      IFNULL(SUM(acch.reserved_amount),0) - IFNULL(SUM(acch.unreserve_amount),0) AS `reservation_amount`,
      SUM(acch.obligated_amount) AS `obligation_amount`,
      SUM(acch.recovery_amount) AS `recovery_amount`,
      @uuid AS uuid,
      @sid AS sid
    FROM trams_account_class_code_history acch
    WHERE account_class_code IN (SELECT account_class_code FROM trams_account_class_code_lookup)
    GROUP BY `acch`.`account_class_code`,`acch`.`transaction_fiscal_year`,`acch`.`cost_center_code` ",_limitClause);
  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('Complete Temporary Operbud Query: ',@tempAmountsQuery), 1);
  PREPARE tmpoperbudAmountsStmt FROM @tempAmountsQuery;
  EXECUTE tmpoperbudAmountsStmt;

  SET @operbudQuery = "";
  IF (p_operbudStatus IS NULL OR p_operbudStatus LIKE '%Authorized%') THEN
    -- added JOIN conditions: tmp.uuid = @uuid (line 124) and acch.uuid = @uuid (line 130)
    SET @operbudQuery = CONCAT("
    SELECT
      o.fiscal_year,
      o.cost_center_code AS `cost_center`,
      o.status,
      a.account_class_code,
      a.acc_funding_fiscal_year AS `funding_fiscal_year`,
      a.acc_appropriation_code AS `appropriation_code`,
      a.acc_section_code as `section_code`,
      a.acc_limitation_code AS `limitation_code`,
      a.acc_auth_type AS `auth_type`,
      al.appropriation_type_flag,
      IFNULL(tmp.sum_code_new_amount,0) AS `authorized_amount`,
      0 AS `pending_amount`,
      `acch`.`transfer_in_amount` AS `transfer_in_amount`,
      `acch`.`transfer_out_amount` AS `transfer_out_amount`,
      `acch`.`reservation_amount` AS `reservation_amount`,
      `acch`.`obligation_amount` AS `obligation_amount`,
      `acch`.`recovery_amount` AS `recovery_amount`,
      IFNULL(tmp.sum_code_new_amount,0) + IFNULL(acch.`transfer_in_amount`,0) - IFNULL(acch.reservation_amount,0) - IFNULL(acch.`obligation_amount`,0)
        - IFNULL(acch.`transfer_out_amount`,0) + IFNULL(acch.`recovery_amount`,0) AS `oper_budget_avail`
    FROM `trams_account_class_code_lookup` al
      JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
      LEFT JOIN `trams_temp_operbud_authorized` tmp
        ON tmp.account_class_code = a.account_class_code
        AND tmp.fiscal_year = `a`.`fiscal_year`
        AND tmp.cost_center_code = `a`.`cost_center_code`
        AND tmp.uuid = @uuid
      JOIN `trams_operating_budget` o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      LEFT JOIN trams_temp_operbud_amounts acch
          ON `a`.`account_class_code`=acch.account_class_code
          AND `a`.`fiscal_year`=acch.transaction_fiscal_year
          AND `a`.`cost_center_code`=acch.cost_center_code
          AND acch.uuid = @uuid
    WHERE o.status='Authorized' AND a.authorized_flag=1",_whereClause,"
    GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`  ",_limitClause);
  END IF;

  IF (p_operbudStatus IS NULL OR p_operbudStatus LIKE '%Pending%') THEN
    IF (p_operbudStatus IS NULL) THEN
      SET @operbudQuery = CONCAT(@operbudQuery, " union all ");
    END IF;
    SET @operbudQuery = CONCAT(@operbudQuery, "
      SELECT
        o.fiscal_year,
        o.cost_center_code AS `cost_center`,
        o.status,
        a.account_class_code,
        a.acc_funding_fiscal_year AS `funding_fiscal_year`,
        a.acc_appropriation_code AS `appropriation_code`,
        a.acc_section_code AS `section_code`,
        a.acc_limitation_code AS `limitation_code`,
        a.acc_auth_type AS `auth_type`,
        al.appropriation_type_flag,
        0 AS `authorized_amount`,
        sum(a.code_change_amount) AS `pending_amount`,
        0 AS `transfer_in_amount`,
        0 AS `transfer_out_amount`,
        0 AS `reservation_amount`,
        0 AS `obligation_amount`,
        0 AS `recovery_amount`,
        0 AS `oper_budget_avail`
      FROM `trams_account_class_code_lookup` al
        JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
        JOIN trams_operating_budget o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      WHERE o.status = 'Pending'",_whereClause,"
      GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`",_limitClause);
  END IF;

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('Complete Operbud Query: ',@operbudQuery), 1);

  PREPARE operbudStmt FROM @operbudQuery;
  EXECUTE operbudStmt;

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', 'Done!', 1);

  -- the rows generated for the procedure are deleted to replicate the effects of a temporary table
  DELETE FROM trams_temp_operbud_authorized WHERE uuid = @uuid;
  DELETE FROM trams_temp_operbud_amounts WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_rollup_by_allotment_code`(IN `p_allotment_code` VARCHAR(4096))
BEGIN

DECLARE _done int default 0;
DECLARE _allotment_code VARCHAR(20);
DECLARE parameter_cursor CURSOR FOR select allotment_code_text from tmp_allotmentCodes;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done=1;

set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', concat('Parameter p_allotment_code: ',p_allotment_code), 1); 

drop temporary table if exists tmp_allotmentCodes;
create temporary table tmp_allotmentCodes(id int not null AUTO_INCREMENT PRIMARY KEY, allotment_code_text varchar(20)) engine=memory;

SET @allotmentCodeQuery = concat('INSERT INTO tmp_allotmentCodes (allotment_code_text) VALUES ',p_allotment_code);
PREPARE allotmentCodeStmt FROM @allotmentCodeQuery;
EXECUTE allotmentCodeStmt;



open parameter_cursor;


  -- begin loop through the parameters 
  set @createResultTable = true;
  get_result: LOOP
    CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', 'In loop',0);
    
    -- fetch the parameter
    fetch parameter_cursor into _allotment_code;
    
    if _done=1 then
      leave get_result;
    end if;

      /* 
          get authorized (latest) account class code data which is operating buget is Authorized
        The account class codes can be in the latest authorized operating budget 
        (records in trams_account_class_code table where authorized_flag =1)
        Use code_new_amount as the value for the authorized records
      */
      drop temporary table if exists tmp_get_authorized_account_class_code_1; 
      create temporary table tmp_get_authorized_account_class_code_1 engine=memory
      SELECT `ac`.`id`                          AS `id`, 
           `ac`.`account_class_code`                AS `account_code`, 
           `ac`.`code_new_amount`                AS `code_new_amt`, 
           `ac`.`trmsprtn_ccntclss_prtngbdg`  AS `trmsprtn_ccntclss_prtngbdg`, 
           `ac`.`trmsprtngbdg_ccntclsscds_dx` AS `trmsprtngbdg_ccntclsscds_dx`
      FROM   `trams_account_class_code` `ac` join trams_operating_budget o on ac.trmsprtn_ccntclss_prtngbdg = o.id
      WHERE   ac.authorized_flag = 1 and o.status = 'Authorized' and 
      o.fiscal_year = trams_get_current_fiscal_year(now()) ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_authorized_account_class_code_1", 1); 

      /* 
          get authorized (latest) account class code data which is operating buget is Pending
        The account class codes can be in any pending operating budget 
        (records in trams_account_class_code table where trams_operating_budget.status = "Pending"
        Use code_change_amount as the value for the pending records
      */
            insert into tmp_get_authorized_account_class_code_1 
      SELECT `ac`.`id`                          AS `id`, 
           `ac`.`account_class_code`                AS `account_code`, 
           `ac`.`code_change_amount`                AS `code_new_amt`, 
           `ac`.`trmsprtn_ccntclss_prtngbdg`  AS `trmsprtn_ccntclss_prtngbdg`, 
           `ac`.`trmsprtngbdg_ccntclsscds_dx` AS `trmsprtngbdg_ccntclsscds_dx`
      FROM   `trams_account_class_code` `ac` join trams_operating_budget o on ac.trmsprtn_ccntclss_prtngbdg = o.id
      WHERE   o.status = 'Pending' and 
      o.fiscal_year = trams_get_current_fiscal_year(now()) ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_authorized_account_class_code_1 updated", 1); 

      
      /* get allotment code from authorized account class code data */
      drop temporary table if exists tmp_get_amount_from_acc_2;   
      create temporary table tmp_get_amount_from_acc_2 engine=memory
      SELECT Concat(Substr(`aa`.`account_code`, 1, 8), Substr(`aa`.`account_code`, 15, 1)) AS `allotment_code`, 
           Substr(`aa`.`account_code`, 1, 4) AS `fiscal_year`, 
           Substr(`aa`.`account_code`, 6, 2) AS `appropriation_code`, 
           Substr(`aa`.`account_code`, 15, 1) AS `auth_type`, 
           Ifnull(Sum(`aa`.`code_new_amt`), 0) AS `new_total_amount` 
      FROM   tmp_get_authorized_account_class_code_1 `aa` 
      GROUP  BY `allotment_code`, 
            `fiscal_year`, 
            `appropriation_code`, 
            `auth_type` ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_amount_from_acc_2",1); 
            

      /* get final total amount group by allotment code from ACC */ 
      drop temporary table if exists tmp_get_final_amount_from_acc_3;   
      create temporary table tmp_get_final_amount_from_acc_3 engine=memory
      SELECT `v`.`allotment_code`        AS `allotmentCode_text`, 
           `v`.`fiscal_year`           AS `fiscalYear_text`, 
           `v`.`appropriation_code`    AS `appropriationCode_text`, 
           `v`.`auth_type`             AS `authType_text`, 
           Sum(`v`.`new_total_amount`) AS `newTotalAmount_dec` 
      FROM   `tmp_get_amount_from_acc_2` `v` 
      GROUP  BY `v`.`allotment_code`, 
            `v`.`fiscal_year`, 
            `v`.`appropriation_code`, 
            `v`.`auth_type`;        
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_final_amount_from_acc_3", 1); 
            
      /* get new amount group by allotment Code */  
      drop temporary table if exists tmp_get_amount_group_by_allotment_code_4;    
      create temporary table tmp_get_amount_group_by_allotment_code_4 engine=memory
            SELECT `allotment_code_text`     AS `allotmentCode_text`, 
           Sum(code_new_amount) AS `codeNewAmount_dec` 
      FROM   `trams_allotment_code` ac join trams_allotment_advice aa on aa.id = ac.allotmentAdviceID_int 
      WHERE  ac.authorized_flag = 1  and aa.fiscal_year = trams_get_current_fiscal_year(now()) 
      GROUP  BY `allotmentcode_text` ;        
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_amount_group_by_allotment_code_4", 1); 

      /* get final result from ACC and Allotment code*/
      drop temporary table if exists tmp_get_final_amount_by_allotment_code_5;    
      create temporary table tmp_get_final_amount_by_allotment_code_5 engine=memory
      SELECT `v4`.`allotmentcode_text`           AS `allotmentCode_text`, 
           `v4`.`fiscalyear_text`              AS `fiscalYear_text`, 
           `v4`.`appropriationcode_text`       AS `appropriationcode_text`, 
           `v4`.`authtype_text`                AS `authType_text`, 
           `v4`.`newtotalamount_dec`           AS `newtotalAmount_dec`, 
           Ifnull(`v3`.`codenewamount_dec`, 0) AS `codeNewAmount_dec` 
      FROM   (tmp_get_final_amount_from_acc_3 `v4` 
          LEFT JOIN `tmp_get_amount_group_by_allotment_code_4` `v3` 
               ON(( `v4`.`allotmentcode_text` = `v3`.`allotmentcode_text` ))) ;
               
      insert into tmp_get_final_amount_by_allotment_code_5
      SELECT `v3`.`allotmentcode_text`               AS `allotmentCode_text`, 
           Substr(`v3`.`allotmentcode_text`, 1, 4) AS `fiscalYear_text`, 
           Substr(`v3`.`allotmentcode_text`, 6, 2) AS `appropriationcode_text`, 
           Substr(`v3`.`allotmentcode_text`, 9, 1) AS `authType_text`, 
           `v4`.`newtotalamount_dec`               AS `newtotalAmount_dec`, 
           Ifnull(`v3`.`codenewamount_dec`, 0)     AS `codeNewAmount_dec` 
      FROM   (`tmp_get_amount_group_by_allotment_code_4` `v3` 
          LEFT JOIN `tmp_get_final_amount_from_acc_3` `v4` 
               ON(( `v4`.`allotmentcode_text` = `v3`.`allotmentcode_text` ))) 
      WHERE  ( Isnull(`v4`.`allotmentcode_text`) 
           AND ( `v3`.`allotmentcode_text` <> '' ) )   ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_final_amount_by_allotment_code_5",1); 
        
      set @selectResultQuery= concat("  
      SELECT 
      Uuid_short()  AS `id`, 
      final.`allotmentcode_text` AS `allotmentCode_text`, 
      final.`fiscalyear_text` AS `fiscalYear_text`, 
      final.`appropriationcode_text` AS `appropriationCode_text`, 
      final.`authtype_text`          AS `authType_text`, 
      final.`newtotalamount_dec`     AS `operatingBudgetNewTotalAmount_dec`, 
      final.`codenewamount_dec`      AS `allotmentCodeNewTotalAmount_dec` 
      FROM   `tmp_get_final_amount_by_allotment_code_5` final 
      Where final.`allotmentcode_text` = '",_allotment_code,"'");

CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', @selectResultQuery,1); 

      
    -- create table with the data 
    if @createResultTable = true then
      drop temporary table IF EXISTS tmp_operating_budget_latest_rollup_by_allotment_code_result;
      set @createResultTableQuery = concat("create temporary table tmp_operating_budget_latest_rollup_by_allotment_code_result engine=memory ", @selectResultQuery);
      PREPARE createResultTableStmt FROM @createResultTableQuery;
      EXECUTE createResultTableStmt;
      
      set @createResultTable = false;
    else
      set @insertResultTableQuery = concat("insert into tmp_operating_budget_latest_rollup_by_allotment_code_result ", @selectResultQuery);
      PREPARE insertResultTableStmt FROM @insertResultTableQuery;
      EXECUTE insertResultTableStmt;
      
    end if;   
    
    
    
  END LOOP get_result;
  
  /*Return result*/
  select distinct * from tmp_operating_budget_latest_rollup_by_allotment_code_result;

close parameter_cursor; 
/* end of loop of parameters*/

     
/* drop all temporary tables */ 
drop temporary table if exists tmp_allotmentCodes; 
drop temporary table if exists tmp_get_authorized_account_class_code_1;
drop temporary table if exists tmp_get_amount_from_acc_2; 
drop temporary table if exists tmp_get_final_amount_from_acc_3;
drop temporary table if exists tmp_get_amount_group_by_allotment_code_4;   
drop temporary table if exists tmp_get_final_amount_by_allotment_code_5;  
drop temporary table IF EXISTS tmp_operating_budget_latest_rollup_by_allotment_code_result;
  

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_project_funding_status`(IN `p_contractAwardId` INT, IN `p_fromTeam` TINYINT(1), IN `p_projSeqNum` INT)
    NO SQL
BEGIN

declare _havingClause varchar(4096) default " ";
declare _groupByClause varchar(4096) default " ";
declare _onClause varchar(4096) default " ";
-- changed data type from date to datetime
declare _appCreation datetime;

/* For Logging*/
set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
CALL `trams_log`(@sid, 'trams_get_project_funding_status', concat('Parameter contract award id: ',p_contractAwardId), 0);

set _havingClause = concat(_havingClause, "sum(`trams_contract_acc_transaction`.`deobligation_amount`)<>0 OR sum(`trams_contract_acc_transaction`.`obligation_amount`) <> 0 OR sum(`trams_contract_acc_transaction`.`disbursement_amount`) <> 0 OR sum(`trams_contract_acc_transaction`.`refund_amount`) <> 0");
set _groupByClause = concat(_groupByClause,
	" `trams_contract_acc_transaction`.`po_number`,",
	" `trams_contract_acc_transaction`.`cost_center_code`,",
	" `trams_contract_acc_transaction`.`account_class_code`,",
	" `trams_contract_acc_transaction`.`fpc`",
	(select if(p_fromTeam = 1, " ",
		concat(", ",
		" `trams_contract_acc_transaction`.`scope_code`,",
		" `trams_contract_acc_transaction`.`scope_suffix`"
		)
	  )
	)
);

/*Previous _appCreation was initialized to (SELECT DATE(created_date_time) FROM trams_application WHERE contractAward_id = p_contractAwardId AND amendment_number = 0).
  Replaced the previous initialization with a function to retrieve the date directly.*/
set _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(p_contractAwardId,NULL);
/*Previous on _onClause was initialized to: if(_appCreation<p_fundingDeploy,'`fs`.`version` = 0','`fs`.`latest_flag` = 1')
  Replaced the previous initialization to retrieve the funding source record at the time the funding source latest flag was set to true for the base application.   */
set _onClause = CONCAT("fs.LatestFlagStartDate <= '",_appCreation,"' AND fs.LatestFlagEndDate >= '",_appCreation,"'");


/* Award Funding Summary */
SET @awardFundingQuery = concat("
select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
`trams_contract_acc_transaction`.`po_number` AS `poNumber_text`,
`trams_contract_acc_transaction`.`funding_source_name` AS `funding_source_short_name`,
`trams_contract_acc_transaction`.`section_code` AS `section_code`,
`fs`.`funding_source_description` AS `fundingSourceName_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from ((`trams_contract_acc_transaction` join `trams_application` `a` on((`trams_contract_acc_transaction`.`application_id` = `a`.`id`)))
	left join `trams_funding_source_reference` `fs` on((`trams_contract_acc_transaction`.`funding_source_name` = `fs`.`funding_source_name`
	and ",_onClause,")))
where `trams_contract_acc_transaction`.`contract_award_id` = ", p_contractAwardId,
(select if(p_fromTeam = 1, " ", concat(" AND CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4), LENGTH(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER) = ", NULLIF(CAST(p_projSeqNum AS CHAR),''),""))),
" group by `trams_contract_acc_transaction`.`contract_award_id`,
`trams_contract_acc_transaction`.`funding_source_name`,
`trams_contract_acc_transaction`.`po_number`"
);

PREPARE awardFundingStmt FROM @awardFundingQuery;
EXECUTE awardFundingStmt;

/* Award Funding - Account Class Code */
SET @awardFundingAccQuery = concat("
select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`application_number` AS `applicationNumber_text`,
ifnull(`trams_contract_acc_transaction`.`app_amendment_number`,0) AS `amendmentNumber_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
`trams_contract_acc_transaction`.`project_number` AS `projectNumber_text`,
`trams_contract_acc_transaction`.`po_number` AS `poNumber_text`,
`trams_contract_acc_transaction`.`scope_code` AS `scopeNumber_text`,
`trams_contract_acc_transaction`.`scope_name` AS `scopeName_text`,
`trams_contract_acc_transaction`.`scope_suffix` AS `scopeSuffix_text`,
`trams_contract_acc_transaction`.`cost_center_code` AS `costCenterCode_text`,
`trams_contract_acc_transaction`.`account_class_code` AS `accountClassCode_text`,
`trams_contract_acc_transaction`.`funding_fiscal_year` AS `fundingFiscalYear_text`,
`trams_contract_acc_transaction`.`appropriation_code` AS `appropriationCode_text`,
`trams_contract_acc_transaction`.`section_code` AS `sectionCode_text`,
`trams_contract_acc_transaction`.`limitation_code` AS `limitationCode_text`,
`trams_contract_acc_transaction`.`type_authority` AS `authorizationType_text`,
`trams_contract_acc_transaction`.`fpc` AS `fpc_text`,
`trams_contract_acc_transaction`.`funding_source_name` AS `fundingSourceName_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from `trams_contract_acc_transaction`",
" WHERE `trams_contract_acc_transaction`.`contract_award_id`= ", p_contractAwardId,
(select if(p_fromTeam = 1, " ", concat(" AND CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4), LENGTH(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER) = ", NULLIF(CAST(p_projSeqNum AS CHAR),''),""))),
" group by ",
_groupByClause,
" HAVING ",
_havingClause
);

CALL `trams_log`(@sid, 'trams_get_project_funding_status', concat('Get Award Funding - Account Class Code SQL: ', @awardFundingAccQuery), 0);
PREPARE awardFundingAccStmt FROM @awardFundingAccQuery;
EXECUTE awardFundingAccStmt;

/* Award Funding - Financial Purpose Code (FPC) */
SET @awardFundingFPCQuery = concat("
select uuid_short() AS `id`,
`trams_contract_acc_transaction`.`recipient_id` AS `granteeId_int`,
`trams_contract_acc_transaction`.`contract_award_id` AS `contractAwardId_int`,
`trams_contract_acc_transaction`.`application_id` AS `applicationId_int`,
`trams_contract_acc_transaction`.`project_id` AS `projectId_int`,
`trams_contract_acc_transaction`.`po_number` AS `poNumber_text`,
`trams_contract_acc_transaction`.`fpc` AS `fpc_text`,
`trams_contract_acc_transaction`.`fpc_description` AS `fpcDescription_text`,
sum(`trams_contract_acc_transaction`.`obligation_amount`) AS `obligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`deobligation_amount`) AS `deobligationAmount_dec`,
sum(`trams_contract_acc_transaction`.`disbursement_amount`) AS `disbursementAmount_dec`,
sum(`trams_contract_acc_transaction`.`refund_amount`) AS `refundAmount_dec`,
(((ifnull(sum(`trams_contract_acc_transaction`.`obligation_amount`),0) - ifnull(sum(`trams_contract_acc_transaction`.`deobligation_amount`),0)) - ifnull(sum(`trams_contract_acc_transaction`.`disbursement_amount`),0)) + ifnull(sum(`trams_contract_acc_transaction`.`refund_amount`),0)) AS `unliquidAmount_dec`
from `trams_contract_acc_transaction`
where `trams_contract_acc_transaction`.`contract_award_id` = ", p_contractAwardId,
(select if(p_fromTeam = 1, " ", concat(" AND CONVERT((REPLACE(SUBSTRING(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4), LENGTH(SUBSTRING_INDEX(`trams_contract_acc_transaction`.`project_number`, '-', 4-1)) + 1), '-', '')), SIGNED INTEGER) = ", NULLIF(CAST(p_projSeqNum AS CHAR),''),""))),
" group by
`trams_contract_acc_transaction`.`fpc`,
`trams_contract_acc_transaction`.`po_number`"
);

PREPARE awardFundingFPCStmt FROM @awardFundingFPCQuery;
EXECUTE awardFundingFPCStmt;


CALL `trams_log`(@sid, 'trams_get_project_funding_status', 'Done', 0);
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_reservation_by_pids`(IN `p_project_ids` VARCHAR(4096))
    NO SQL
BEGIN

drop temporary table if exists tmp_projectIds;
 
create temporary table tmp_projectIds(id int) engine=memory;
 
SET @query = concat('INSERT INTO tmp_projectIds (id) select id from trams_project where id in (',p_project_ids,')');
PREPARE stmt FROM @query;
EXECUTE stmt;
  SELECT  uuid_short() AS scopeResId_int,
          li.project_id AS projectNumber_int,
          p.project_number AS projectNumber_text,
          p.project_title AS projectName_text,
          li.scope_detailed_name AS scopeName_text,
          li.scope_code AS scopeNumber_text,
          li.section_code AS sectionCode_text,                             
          fsr.funding_source_name AS fundingSourceName_text,
          ifnull(sum(li.revised_FTA_amount), 0) - ifnull(sum(li.latest_amend_fta_amount), 0) AS scopeFTAamount_dec 
  FROM    trams_line_item li 
            join
          trams_funding_source_reference fsr 
            on   li.scope_detailed_name is not null and 
                fsr.funding_source_name = li.funding_source_name
            join
          trams_project p 
            on p.id = li.project_id 
  WHERE  p.id in (select id from tmp_projectIds)
  GROUP BY  li.project_id,
            li.scope_code,
            fsr.funding_source_name;

  drop temporary table if exists tmp_projectIds;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_project_scope_reservation_by_pids`(IN `p_project_ids` VARCHAR(4096))
    NO SQL
BEGIN

	DECLARE _applicationId INT(11) DEFAULT 0;
	DECLARE _appFromTeamFlag TINYINT(1) DEFAULT 0;
	DECLARE _contractAwardId INT(11) DEFAULT 0;
	DECLARE _lineItemGroupByClause VARCHAR(1000) DEFAULT "";
	DECLARE _scopeSelect VARCHAR(1000) DEFAULT ",NULL AS item_scope, NULL AS scope_number";
	DECLARE _scopeAccJoinClause VARCHAR(1000) DEFAULT "";
	DECLARE _resultGroupByClause VARCHAR(1000) DEFAULT "";
	DECLARE _sql VARCHAR(40960) DEFAULT "";
	DECLARE _onClause VARCHAR(4096) DEFAULT " ";
	DECLARE _appCreation DATETIME;
	
	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));

	/* Changed previous temp table creation statement to insert into the new table */
	SET @projQuery = CONCAT("
		INSERT INTO trams_temp_pid_projectIds (project_id, uuid, time_stamp) 
			SELECT 
				id AS project_id,
				@uuid AS uuid,
				@sid AS time_stamp
			FROM trams_project 
			WHERE id IN (", p_project_ids, ")");
	PREPARE projStmt FROM @projQuery;
	EXECUTE projStmt;

	/* Replaced reference to temp table with call to new table and added uuid condition */
	SELECT application_id INTO _applicationId FROM `trams_project` WHERE id IN (SELECT project_id FROM trams_temp_pid_projectIds WHERE uuid = @uuid) ORDER BY id DESC LIMIT 1;
	SELECT app_from_team_flag, contractAward_Id INTO _appFromTeamFlag, _contractAwardId FROM `trams_application` WHERE id = _applicationId;

	SET _appCreation = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(_contractAwardId,_applicationId);

	SET _onClause = CONCAT("fsr.LatestFlagStartDate <= '",_appCreation,"' AND fsr.LatestFlagEndDate >= '",_appCreation,"'");

	IF (_appFromTeamFlag <> 1) THEN
		SET _lineItemGroupByClause = ", li.scope_code, fsr.funding_source_name";
		SET _scopeSelect = ", li.scope_detailed_name AS item_scope, li.scope_code AS scope_number";
		SET _scopeAccJoinClause = " AND sa.scope_number = tli.scope_number AND sa.funding_source_name = tli.funding_source";
		SET _resultGroupByClause = ", tli.scope_number, tli.funding_source";
	END IF;

	/* Changed previous temp table creation statement to insert into the new table */
	SET @lineItemQuery = CONCAT(
		"INSERT INTO trams_temp_pid_line_item (project_id, project_number, project_title, item_scope, scope_number, funding_source, section_code, li_amount, cl_amount, uuid, time_stamp)
		SELECT
			li.project_id AS project_id,
			p.project_number AS project_number,
			p.project_title AS project_title",
			_scopeSelect, ",
			fsr.funding_source_name AS funding_source,
			li.section_code AS section_code,
			ifnull(sum(li.revised_FTA_amount), 0) - ifnull(sum(li.latest_amend_fta_amount), 0) AS li_amount,
			ifnull(sum(cl.revised_FTA_amount), 0) AS cl_amount,
			@uuid AS uuid,
			@sid AS time_stamp
		FROM
			trams_line_item li
				JOIN trams_funding_source_reference fsr
					ON li.scope_detailed_name is not null and fsr.funding_source_name = li.funding_source_name AND ",_onClause, "
				JOIN trams_project p
					ON p.id = li.project_id
				JOIN trams_line_item_change_log cl
					ON li.id = cl.line_item_id
		WHERE
			li.other_budget = 0
			/* Replaced reference to temp table with call to new table and added uuid condition */
			AND p.id IN (SELECT project_id FROM trams_temp_pid_projectIds WHERE uuid = @uuid)
			AND cl.budget_revision_id = (
				SELECT max(budget_revision_id)
				FROM trams_line_item_change_log
				WHERE application_id = ",_applicationId,"
				GROUP BY application_id
				ORDER BY id DESC
			)
		GROUP BY
			li.project_id",
			_lineItemGroupByClause
	);
	PREPARE lineItemStmt FROM @lineItemQuery;
	EXECUTE lineItemStmt;

	SET _sql = CONCAT(
		"SELECT
		uuid_short() AS scopeResId_int,
		tli.project_id AS projectId_int,
		tli.project_number AS projectNumber_text,
		tli.project_title AS projectName_text,
		tli.item_scope AS scopeName_text,
		tli.scope_number AS scopeNumber_text,
		tli.section_code AS sectionCode_text,
		tli.funding_source AS fundingSourceName_text,
		ifnull(tli.li_amount,0) AS scopeFTAamount_dec,
		ifnull(tli.cl_amount,0) AS cumulativeScopeFTAamount_dec,
		ifnull(sum(sa.acc_reservation_amount),0) AS reservedAmount_dec,
		ifnull(sum(sa.cumulative_reservation_amount),0) AS cumulativeReservedAmount_dec,
		(ifnull(tli.li_amount,0) - ifnull(sum(sa.acc_reservation_amount),0)) AS unreservedAmount_dec,
		(ifnull(tli.cl_amount,0) - ifnull(sum(sa.cumulative_reservation_amount),0)) AS cumulativeUnreservedAmount_dec
		FROM
			/* Replaced reference to temp table with call to new table and added uuid condition */
			trams_temp_pid_line_item tli
				LEFT JOIN trams_scope_acc sa
					ON sa.project_id = tli.project_id ",
					_scopeAccJoinClause,
		" WHERE tli.uuid = @uuid 
		GROUP BY
			tli.project_id",
			_resultGroupByClause

	);
	SET @query = _sql;

	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	CALL `trams_log`(@sid, 'trams_get_project_scope_reservation_by_pids', CONCAT('Get Project Scope Reservation Query: ', @query), 1);

	PREPARE stmt FROM @query;
	EXECUTE stmt;

	DELETE FROM trams_temp_pid_projectIds WHERE uuid = @uuid;
	DELETE FROM trams_temp_pid_line_item WHERE uuid = @uuid;

 END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_sql_limit_clause`(
    in p_return_limit varchar(20),
    out p_limit_clause varchar(20))
    NO SQL
BEGIN
    
  if p_return_limit='' or p_return_limit is null then
    set p_limit_clause = " limit 100 ";
  elseif p_return_limit = -1 then
      set p_limit_clause = " ";
  else
    set p_limit_clause = concat(" limit ", p_return_limit);
  end if;
  

  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_operbud_report_excel_for_yec`(
  IN `p_operbudFiscalYear` VARCHAR(255),
  IN `p_costCenterCode` VARCHAR(512),
  IN `p_fundingFiscalYear` VARCHAR(255),
  IN `p_appropriationCode` VARCHAR(255),
  IN `p_sectionCode` VARCHAR(255),
  IN `p_limitationCode` VARCHAR(255),
  IN `p_authorizationType` VARCHAR(255),
  IN `p_operbudStatus` VARCHAR(1024),
  IN `p_appropriation_type` VARCHAR(20),
  IN `p_return_limit` VARCHAR(20)
)
    NO SQL
BEGIN

  DECLARE _sql VARCHAR(40960) DEFAULT "";
  DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
  DECLARE _limitClause VARCHAR(100) DEFAULT " ";

  CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.fiscal_year = '",NULLIF(p_operbudFiscalYear,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.cost_center_code = '",NULLIF(p_costCenterCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_funding_fiscal_year = '",NULLIF(p_fundingFiscalYear,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_limitation_code = '",NULLIF(p_limitationCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_appropriation_code = '",NULLIF(p_appropriationCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_section_code = '",NULLIF(p_sectionCode,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_auth_type = '",NULLIF(p_authorizationType,''),"'"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND o.status in ( '", NULLIF(p_operbudStatus,''),"' )"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND al.appropriation_type_flag in (",NULLIF(p_appropriation_type,''),")"),"")));

  SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
  -- added UUID for session reference
  SET @uuid = UUID_SHORT();

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('1. Where clause : ',_whereClause), 1);

  SET @tempAuthorizedQuery = "";
  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_operbud_authorized_yec_id, uuid, and sid columns
  SET @tempAuthorizedQuery = CONCAT("INSERT INTO trams_temp_operbud_authorized_yec
    SELECT
      NULL AS trams_temp_operbud_authorized_yec_id,
      a.account_class_code AS `account_class_code`,
      a.fiscal_year AS `fiscal_year`,
      a.cost_center_code AS `cost_center_code`,
      SUM(a.code_new_amount) AS `sum_code_new_amount`,
      @uuid AS uuid,
      @sid AS time_stamp
    FROM `trams_account_class_code_lookup` al
      JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
      JOIN  trams_operating_budget o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
    WHERE o.status='Authorized'
      AND a.authorized_flag=1
      AND a.code_transaction_type <> 'Recovery' ",_whereClause,"
    GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code` ",_limitClause);

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('2. Complete Temporary Operbud Query: ',@tempAuthorizedQuery), 1);

  PREPARE tmpoperbudAuthorizedStmt FROM @tempAuthorizedQuery;
  EXECUTE tmpoperbudAuthorizedStmt;

  SET @tempAmountsQuery = "";
  -- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_operbud_amounts_yec_id, uuid, and sid columns
  -- added "WHERE uuid = @uuid" clause in SELECT statement on line 93
  SET @tempAmountsQuery = CONCAT("INSERT INTO trams_temp_operbud_amounts_yec
    SELECT
      NULL AS trams_temp_operbud_amounts_yec_id,
      acch.account_class_code,
      acch.transaction_fiscal_year,
      acch.cost_center_code,
      SUM(acch.transfer_in_amount) AS `transfer_in_amount`,
      SUM(acch.transfer_out_amount) AS `transfer_out_amount`,
      IFNULL(SUM(acch.reserved_amount),0) - IFNULL(SUM(acch.unreserve_amount),0) AS `reservation_amount`,
      SUM(acch.obligated_amount) AS `obligation_amount`,
      SUM(acch.recovery_amount) AS `recovery_amount`,
      @uuid AS uuid,
      @sid AS time_stamp
    FROM trams_account_class_code_history acch
    WHERE account_class_code IN (SELECT account_class_code FROM trams_temp_operbud_authorized_yec WHERE uuid = @uuid)
    GROUP BY `acch`.`account_class_code`,`acch`.`transaction_fiscal_year`,`acch`.`cost_center_code` ",_limitClause);

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('3. Complete Temporary Operbud Query: ',@tempAmountsQuery), 1);

  PREPARE tmpoperbudAmountsStmt FROM @tempAmountsQuery;
  EXECUTE tmpoperbudAmountsStmt;

  SET @operbudQuery = "";

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('4. before the if start '), 1);

  IF (p_operbudStatus IS NULL OR p_operbudStatus LIKE '%Authorized%' OR p_operbudStatus=  '' ) THEN
    CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('5.  After if  : p_operbudStatus'), 1);

    -- added AND statements on lines 135 and 140 in order to join on @uuid
    SET @operbudQuery = CONCAT("
      SELECT
        o.fiscal_year,
        o.cost_center_code AS `cost_center`,
        o.status,
        a.account_class_code,
        a.acc_funding_fiscal_year AS `funding_fiscal_year`,
        a.acc_appropriation_code AS `appropriation_code`,
        a.acc_section_code AS `section_code`,
        a.acc_limitation_code AS `limitation_code`,
        a.acc_auth_type AS `auth_type`,
        al.appropriation_type_flag,
        tmp.sum_code_new_amount AS `authorized_amount`,
        0 AS `pending_amount`,
        `acch`.`transfer_in_amount` AS `transfer_in_amount`,
        `acch`.`transfer_out_amount` AS `transfer_out_amount`,
        `acch`.`reservation_amount` AS `reservation_amount`,
        `acch`.`obligation_amount` AS `obligation_amount`,
        `acch`.`recovery_amount` AS `recovery_amount`,
        IFNULL(tmp.sum_code_new_amount,0) + IFNULL(acch.`transfer_in_amount`,0) - IFNULL(acch.reservation_amount,0) - IFNULL(acch.`obligation_amount`,0)
          - IFNULL(acch.`transfer_out_amount`,0) + IFNULL(`acch`.`recovery_amount`,0) AS `oper_budget_avail`
      FROM `trams_account_class_code_lookup` al
        JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
        JOIN `trams_temp_operbud_authorized_yec` tmp ON tmp.account_class_code = a.account_class_code
          AND tmp.fiscal_year = `a`.`fiscal_year`
          AND tmp.cost_center_code = `a`.`cost_center_code`
          AND tmp.uuid = @uuid
        JOIN `trams_operating_budget` o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
        LEFT JOIN trams_temp_operbud_amounts_yec acch ON `a`.`account_class_code`=acch.account_class_code
            AND `a`.`fiscal_year`=acch.transaction_fiscal_year
            AND `a`.`cost_center_code`=acch.cost_center_code
            AND acch.uuid = @uuid
      WHERE o.status='Authorized'
        AND a.authorized_flag=1",_whereClause,"
      GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code` ",_limitClause);

    CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('6. Before the end of if: ',@operbudQuery), 1);
  END IF;

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('7. before the second if start '), 1);

  IF (p_operbudStatus IS NULL OR p_operbudStatus LIKE '%Pending%' OR p_operbudStatus=  '') THEN

    CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('8. after the seond if  : p_operbudStatus'), 1);

    IF (p_operbudStatus IS NULL  OR p_operbudStatus=  '') THEN

      CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('9. before the third if  : p_operbudStatus'), 1);
      SET @operbudQuery = CONCAT(@operbudQuery, " union all ");
    END IF;

    SET @operbudQuery = CONCAT(@operbudQuery, "
        SELECT
          o.fiscal_year,
          o.cost_center_code AS `cost_center`,
          o.status,
          a.account_class_code,
          a.acc_funding_fiscal_year AS `funding_fiscal_year`,
          a.acc_appropriation_code AS `appropriation_code`,
          a.acc_section_code AS `section_code`,
          a.acc_limitation_code AS `limitation_code`,
          a.acc_auth_type AS `auth_type`,
          al.appropriation_type_flag,
          0 AS `authorized_amount`,
          SUM(a.code_change_amount) AS `pending_amount`,
          0 AS `transfer_in_amount`,
          0 AS `transfer_out_amount`,
          0 AS `reservation_amount`,
          0 AS `obligation_amount`,
          0 AS `recovery_amount`,
          0 AS `oper_budget_avail`
      FROM `trams_account_class_code_lookup` al
        JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
        JOIN trams_operating_budget o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      WHERE o.status = 'Pending'",_whereClause,"
      GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`",_limitClause);

    CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('10. Complete Temporary Operbud Query: ',@operbudQuery), 1);
  END IF;

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', CONCAT('11. Complete Operbud Query: ',@operbudQuery), 1);

  PREPARE operbudStmt FROM @operbudQuery;
  EXECUTE operbudStmt;

  CALL `trams_log`(@sid, 'trams_get_operbud_report_excel', '12. Done!', 1);

  -- removing entries that were inserted by session
  DELETE FROM trams_temp_operbud_authorized_yec WHERE uuid = @uuid;
  DELETE FROM trams_temp_operbud_amounts_yec WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_operating_budget_latest_rollup_by_allotment_code`(
  IN p_allotment_code VARCHAR(4096)
)
BEGIN

DECLARE _done int default 0;
DECLARE _allotment_code VARCHAR(20);
DECLARE parameter_cursor CURSOR FOR select allotment_code_text from tmp_allotmentCodes;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET _done=1;

set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', concat('Parameter p_allotment_code: ',p_allotment_code), 1); 

drop temporary table if exists tmp_allotmentCodes;
create temporary table tmp_allotmentCodes(id int not null AUTO_INCREMENT PRIMARY KEY, allotment_code_text varchar(20)) engine=memory;

SET @allotmentCodeQuery = concat('INSERT INTO tmp_allotmentCodes (allotment_code_text) VALUES ',p_allotment_code);
PREPARE allotmentCodeStmt FROM @allotmentCodeQuery;
EXECUTE allotmentCodeStmt;



open parameter_cursor;


  -- begin loop through the parameters 
  set @createResultTable = true;
  get_result: LOOP
    CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', 'In loop',0);
    
    -- fetch the parameter
    fetch parameter_cursor into _allotment_code;
    
    if _done=1 then
      leave get_result;
    end if;

      /* get authorized (latest) account class code data */
      drop temporary table if exists tmp_get_authorized_account_class_code_1; 
      create temporary table tmp_get_authorized_account_class_code_1 engine=memory
      SELECT `ac`.`id`                          AS `id`, 
           `ac`.`account_class_code`                AS `account_code`, 
           `ac`.`code_new_amount`                AS `code_new_amt`, 
           `ac`.`trmsprtn_ccntclss_prtngbdg`  AS `trmsprtn_ccntclss_prtngbdg`, 
           `ac`.`trmsprtngbdg_ccntclsscds_dx` AS `trmsprtngbdg_ccntclsscds_dx`
      FROM   `trams_account_class_code` `ac` 
      WHERE  version = (select max(version) from `trams_account_class_code` acc where acc.id = ac.id)
      and fiscal_year = trams_get_current_fiscal_year(now());
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_authorized_account_class_code_1", 1); 

      /* get allotment code from authorized account class code data */
      drop temporary table if exists tmp_get_amount_from_acc_2;   
      create temporary table tmp_get_amount_from_acc_2 engine=memory
      SELECT Concat(Substr(`aa`.`account_code`, 1, 8), Substr(`aa`.`account_code`, 15, 1)) AS `allotment_code`, 
           Substr(`aa`.`account_code`, 1, 4) AS `fiscal_year`, 
           Substr(`aa`.`account_code`, 6, 2) AS `appropriation_code`, 
           Substr(`aa`.`account_code`, 15, 1) AS `auth_type`, 
           Ifnull(Sum(`aa`.`code_new_amt`), 0) AS `new_total_amount` 
      FROM   tmp_get_authorized_account_class_code_1 `aa` 
      GROUP  BY `allotment_code`, 
            `fiscal_year`, 
            `appropriation_code`, 
            `auth_type` ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_amount_from_acc_2",1); 
            

      /* get final total amount group by allotment code from ACC */ 
      drop temporary table if exists tmp_get_final_amount_from_acc_3;   
      create temporary table tmp_get_final_amount_from_acc_3 engine=memory
      SELECT `v`.`allotment_code`        AS `allotmentCode_text`, 
           `v`.`fiscal_year`           AS `fiscalYear_text`, 
           `v`.`appropriation_code`    AS `appropriationCode_text`, 
           `v`.`auth_type`             AS `authType_text`, 
           Sum(`v`.`new_total_amount`) AS `newTotalAmount_dec` 
      FROM   `tmp_get_amount_from_acc_2` `v` 
      GROUP  BY `v`.`allotment_code`, 
            `v`.`fiscal_year`, 
            `v`.`appropriation_code`, 
            `v`.`auth_type`;        
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_final_amount_from_acc_3", 1); 
            
      /* get new amount group by allotment Code */  
      drop temporary table if exists tmp_get_amount_group_by_allotment_code_4;    
      create temporary table tmp_get_amount_group_by_allotment_code_4 engine=memory
            SELECT `allotment_code_text`     AS `allotmentCode_text`, 
           Sum(code_new_amount) AS `codeNewAmount_dec` 
      FROM   `trams_allotment_code` ac join trams_allotment_advice aa on aa.id = ac.allotmentAdviceID_int 
      WHERE  ac.authorized_flag = 1  and aa.fiscal_year = trams_get_current_fiscal_year(now()) 
      GROUP  BY `allotmentcode_text` ;        
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_amount_group_by_allotment_code_4", 1); 

      /* get final result from ACC and Allotment code*/
      drop temporary table if exists tmp_get_final_amount_by_allotment_code_5;    
      create temporary table tmp_get_final_amount_by_allotment_code_5 engine=memory
      SELECT `v4`.`allotmentcode_text`           AS `allotmentCode_text`, 
           `v4`.`fiscalyear_text`              AS `fiscalYear_text`, 
           `v4`.`appropriationcode_text`       AS `appropriationcode_text`, 
           `v4`.`authtype_text`                AS `authType_text`, 
           `v4`.`newtotalamount_dec`           AS `newtotalAmount_dec`, 
           Ifnull(`v3`.`codenewamount_dec`, 0) AS `codeNewAmount_dec` 
      FROM   (tmp_get_final_amount_from_acc_3 `v4` 
          LEFT JOIN `tmp_get_amount_group_by_allotment_code_4` `v3` 
               ON(( `v4`.`allotmentcode_text` = `v3`.`allotmentcode_text` ))) ;
               
      insert into tmp_get_final_amount_by_allotment_code_5
      SELECT `v3`.`allotmentcode_text`               AS `allotmentCode_text`, 
           Substr(`v3`.`allotmentcode_text`, 1, 4) AS `fiscalYear_text`, 
           Substr(`v3`.`allotmentcode_text`, 6, 2) AS `appropriationcode_text`, 
           Substr(`v3`.`allotmentcode_text`, 9, 1) AS `authType_text`, 
           `v4`.`newtotalamount_dec`               AS `newtotalAmount_dec`, 
           Ifnull(`v3`.`codenewamount_dec`, 0)     AS `codeNewAmount_dec` 
      FROM   (`tmp_get_amount_group_by_allotment_code_4` `v3` 
          LEFT JOIN `tmp_get_final_amount_from_acc_3` `v4` 
               ON(( `v4`.`allotmentcode_text` = `v3`.`allotmentcode_text` ))) 
      WHERE  ( Isnull(`v4`.`allotmentcode_text`) 
           AND ( `v3`.`allotmentcode_text` <> '' ) )   ;
      CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', "tmp_get_final_amount_by_allotment_code_5",1); 
        
      set @selectResultQuery= concat("  
      SELECT 
      Uuid_short()  AS `id`, 
      final.`allotmentcode_text` AS `allotmentCode_text`, 
      final.`fiscalyear_text` AS `fiscalYear_text`, 
      final.`appropriationcode_text` AS `appropriationCode_text`, 
      final.`authtype_text`          AS `authType_text`, 
      final.`newtotalamount_dec`     AS `operatingBudgetNewTotalAmount_dec`, 
      final.`codenewamount_dec`      AS `allotmentCodeNewTotalAmount_dec` 
      FROM   `tmp_get_final_amount_by_allotment_code_5` final 
      Where final.`allotmentcode_text` = '",_allotment_code,"'");

CALL `trams_log`(@sid, 'trams_get_rollup_by_allotment_code', @selectResultQuery,1); 

      
    -- create table with the data 
    if @createResultTable = true then
      drop temporary table IF EXISTS tmp_operating_budget_latest_rollup_by_allotment_code_result;
      set @createResultTableQuery = concat("create temporary table tmp_operating_budget_latest_rollup_by_allotment_code_result engine=memory ", @selectResultQuery);
      PREPARE createResultTableStmt FROM @createResultTableQuery;
      EXECUTE createResultTableStmt;
      
      set @createResultTable = false;
    else
      set @insertResultTableQuery = concat("insert into tmp_operating_budget_latest_rollup_by_allotment_code_result ", @selectResultQuery);
      PREPARE insertResultTableStmt FROM @insertResultTableQuery;
      EXECUTE insertResultTableStmt;
      
    end if;   
    
    
    
  END LOOP get_result;
  
  /*Return result*/
  select distinct * from tmp_operating_budget_latest_rollup_by_allotment_code_result;

close parameter_cursor; 
/* end of loop of parameters*/

     
/* drop all temporary tables */ 
drop temporary table if exists tmp_allotmentCodes; 
drop temporary table if exists tmp_get_authorized_account_class_code_1;
drop temporary table if exists tmp_get_amount_from_acc_2; 
drop temporary table if exists tmp_get_final_amount_from_acc_3;
drop temporary table if exists tmp_get_amount_group_by_allotment_code_4;   
drop temporary table if exists tmp_get_final_amount_by_allotment_code_5;  
drop temporary table IF EXISTS tmp_operating_budget_latest_rollup_by_allotment_code_result;
  

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_project_scope_level_budget_report`(IN p_granteeId VARCHAR(2048), IN p_costCenterCode VARCHAR(512), IN p_fiscalYear VARCHAR(512),
IN p_appNumber VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_appType VARCHAR(512), IN p_preAwardManager VARCHAR(1024), IN p_postAwardManager VARCHAR(1024),
IN p_projectNumber VARCHAR(512), IN p_sectionCode VARCHAR(512), IN p_scopeCode VARCHAR(512), IN p_gmtOffset VARCHAR(10), IN p_return_limit VARCHAR(20)
)
BEGIN

	/*
     Stored Procedure Name : trams_get_project_scope_level_budget_report
     Description : To populate data for TrAMS 'Project Scope Budget' excel report.
     Updated by  : Hunter Wang (2020.Aug.25)
    */

    DECLARE _spName VARCHAR(255) DEFAULT "trams_get_project_scope_level_budget_report";
    DECLARE _applicationQuery VARCHAR(4096);
    DECLARE _whereClause VARCHAR(4096) DEFAULT "";
    DECLARE _saWhereClause VARCHAR(4096) DEFAULT "";
    DECLARE _limitClause VARCHAR(4096) DEFAULT "";
    DECLARE _sql VARCHAR(16384);

    CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);

    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_id in (",NULLIF(p_granteeId,''),")"),"")));

    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.recipient_cost_center in (",NULLIF(p_costCenterCode,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fiscal_year in (",NULLIF(p_fiscalYear,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_number in ('",NULLIF(p_appNumber,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.status in (",NULLIF(p_appStatus,''),")"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.application_type in ('",NULLIF(p_appType,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_pre_award_manager in ('",NULLIF(p_preAwardManager,''),"')"),"")));

    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.fta_post_award_manager in ('",NULLIF(p_postAwardManager,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and p.project_number in ('",NULLIF(p_projectNumber,''),"')"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" and a.app_from_team_flag =0 "),"")));

    SET _saWhereClause = CONCAT(_saWhereClause, (SELECT IFNULL(CONCAT(" and sa.acc_section_code in (",NULLIF(p_sectionCode,''),")"),"")));
    SET _saWhereClause = CONCAT(_saWhereClause, (SELECT IFNULL(CONCAT(" and sa.scope_number in (",NULLIF(p_scopeCode,''),")"),"")));

    SET @uuid = UUID_SHORT();
    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

    CALL `trams_log`(@sid, _spName, CONCAT(now(),' CALL SP Inputs: ', IFNULL(`p_granteeId`,'NULL'),',',IFNULL(  `p_costCenterCode` ,'NULL'),',',IFNULL(  `p_fiscalYear` ,'NULL'),',',IFNULL(  `p_appNumber` ,'NULL'),',',IFNULL(  `p_appStatus` ,'NULL'),',',IFNULL(  `p_appType` ,'NULL'),',',IFNULL(  `p_preAwardManager` ,'NULL'),',',IFNULL(  `p_postAwardManager` ,'NULL'),',',IFNULL(  `p_projectNumber` ,'NULL'),',',IFNULL(  `p_sectionCode` ,'NULL'),',',IFNULL(  `p_scopeCode` ,'NULL'),',',IFNULL(  `p_return_limit` ,'NULL'),','), 1);

    SET _sql =
    "INSERT INTO trams_temp_psb_application (recipient_id, recipient_cost_center, fiscal_year, application_number, amendment_number, application_name, status, application_type, recipient_contact, fta_post_award_manager, fta_pre_award_manager, application_id, contractAward_id, project_id, project_number, project_title, uuid, time_stamp)
        SELECT
            a.recipient_id,
            a.recipient_cost_center,
            a.fiscal_year,
            a.application_number,
            a.amendment_number,
            a.application_name,
            a.status,
            a.application_type,
            a.recipient_contact,
            a.fta_post_award_manager,
            a.fta_pre_award_manager,
            a.id as application_id,
            a.contractAward_id,
            p.id as project_id,
            p.project_number,
            p.project_title,
            @uuid AS uuid,
            @sid AS time_stamp
        FROM
            trams_application a
        JOIN
            trams_project p
        ON
            a.id = p.application_id
        WHERE 1 ";

    SET @applicationQuery = CONCAT(_sql,_whereClause, _limitClause);

    CALL `trams_log`(@sid, _spName, CONCAT('Get Application & Project Query: ',@applicationQuery), 1);

    PREPARE applicationStmt FROM @applicationQuery;
    EXECUTE applicationStmt;

    SET @appCount=(SELECT COUNT(*) FROM trams_temp_psb_application WHERE uuid = @uuid);

    SET _sql = CONCAT("
    INSERT INTO trams_temp_psb_scope_acc_rollup (scope_name, scope_number, project_number, projectId_int, proj_num_no_amend, section_code, total_acc_reservation_amt, uuid, time_stamp)
    SELECT
        scope_name,
        scope_number,
        project_number,
        project_id AS projectId_int,
        left(project_number, length(project_number) - 2) AS proj_num_no_amend,
        acc_section_code AS section_code,
        sum(acc_reservation_amount) AS total_acc_reservation_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_scope_acc sa
    WHERE
        /* Replaced reference to temp table and added uuid condition */
        project_id in (SELECT project_id FROM trams_temp_psb_application WHERE uuid = @uuid)",
        _saWhereClause,
    " GROUP BY
        proj_num_no_amend,
        scope_number");

    SET @applicationQuery = _sql;
    PREPARE applicationStmt FROM @applicationQuery;
    EXECUTE applicationStmt;


    INSERT INTO trams_temp_psb_line_item_rollup (scope_code, app_id, project_id, section_code, project_number, proj_num_no_amend, total_scope_fta_amt, total_scope_non_fta_amt, total_scope_eligible_amt, uuid, time_stamp)
    SELECT
        li.scope_code,
        li.app_id AS app_id,
        li.project_id AS project_id,
        li.section_code AS section_code,
        MAX(p.project_number) AS project_number,
        LEFT(p.project_number, LENGTH(p.project_number) - 2) AS proj_num_no_amend,
        SUM(li.revised_FTA_amount) AS total_scope_fta_amt,
        (SUM(li.revised_total_amount) - SUM(li.revised_FTA_amount)) AS total_scope_non_fta_amt,
        SUM(li.revised_total_amount) AS total_scope_eligible_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_line_item li
    JOIN
        trams_project p
    ON
        li.project_id = p.id
    WHERE
        project_id IN (SELECT project_id FROM trams_temp_psb_application WHERE uuid = @uuid)
    GROUP BY
        proj_num_no_amend,
        li.scope_code;

    INSERT INTO trams_temp_psb_contract_acc_transaction_rollup (project_id, section_code, scope_name, project_number, scope_code, funding_source_name, proj_num_no_amend, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, uuid, time_stamp)
    SELECT
        trans.project_id,
        trans.section_code,
        trans.scope_name,
        MAX(trans.project_number) AS project_number,
        trans.scope_code,
        trans.funding_source_name,
        LEFT(trans.project_number, LENGTH(trans.project_number) - 2) AS proj_num_no_amend,
        sum(trans.obligation_amount) AS total_obligation_amt,
        sum(trans.deobligation_amount) AS total_deobligation_amt,
        sum(trans.disbursement_amount) AS total_disbursement_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_contract_acc_transaction trans
    JOIN
        trams_funding_source_reference f ON trans.funding_source_name = f.funding_source_name
        -- added AND clauses to join on a specific funding source version
        AND f.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(trans.contract_award_id, NULL)
        AND f.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(trans.contract_award_id, NULL)
    WHERE
        project_id IN (SELECT project_id FROM trams_temp_psb_application WHERE uuid = uuid)
    GROUP BY
        proj_num_no_amend,
        trans.scope_code;


    INSERT INTO trams_temp_psb_contract_acc_transaction_rollup2 (project_id, section_code, scope_name, project_number, scope_code, funding_source_name, proj_num_no_amend, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, uuid, time_stamp)
    SELECT
        project_id,
        section_code,
        scope_name,
        project_number,
        scope_code,
        funding_source_name,
        proj_num_no_amend,
        total_obligation_amt,
        total_deobligation_amt,
        total_disbursement_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_temp_psb_contract_acc_transaction_rollup
    WHERE uuid = @uuid;

    INSERT INTO trams_temp_psb_no_contract_award_line_item_rollup (scope_code, app_id, project_id, section_code, funding_source_name, project_number, total_scope_fta_amt, total_scope_non_fta_amt, total_scope_eligible_amt, uuid, time_stamp)
    SELECT
        li.scope_code,
        li.app_id AS app_id,
        li.project_id AS project_id,
        li.section_code AS section_code,
        li.funding_source_name,
        MAX(p.project_number) AS project_number,
        SUM(li.revised_FTA_amount) AS total_scope_fta_amt,
        (SUM(li.revised_total_amount) - SUM(li.revised_FTA_amount)) AS total_scope_non_fta_amt,
        SUM(li.revised_total_amount) AS total_scope_eligible_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_line_item li
    JOIN
        trams_project p
    ON
        li.project_id = p.id
    WHERE
        project_id IN (SELECT project_id FROM trams_temp_psb_application WHERE uuid = @uuid AND (contractAward_id IS NULL OR contractAward_id = ''))
    GROUP BY
        project_number,
        li.scope_code;

    SET _sql = CONCAT("
    INSERT INTO trams_temp_psb_no_contract_award_scope_acc_rollup (scope_name, scope_number, project_number, projectId_int, section_code, total_acc_reservation_amt, uuid, time_stamp)
    SELECT
        scope_name,
        scope_number,
        project_number,
        project_id AS projectId_int,
        acc_section_code AS section_code,
        sum(acc_reservation_amount) AS total_acc_reservation_amt,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_scope_acc sa
    WHERE
        /* Replaced reference to temp table and added uuid condition */
        project_id IN (SELECT project_id FROM trams_temp_psb_application WHERE uuid = @uuid)",
        _saWhereClause,
    " GROUP BY
        project_number,
        scope_number");

    SET @applicationQuery = _sql;
    PREPARE applicationStmt FROM @applicationQuery;
    EXECUTE applicationStmt;

    SET @appCount = (SELECT COUNT(*) FROM trams_temp_psb_application WHERE uuid = @uuid);
    CALL `trams_log`(@sid, _spName, CONCAT(' Build Resultset Applications: ',now(),' ',@appCount), 1);

    INSERT INTO trams_temp_psb_resultset (grantee_id, acronym, grantee_name, grantee_cost_center, fiscal_year, application_number, amendment_number, application_name, status, application_type, transmitted_date, latest_transmitted_date, submitted_date, latest_submitted_date, grantee_poc, pre_award_mgr, post_award_mgr, project_number, project_name, fundingSourceName_text, accSectionCode_text, scopeName_text, scopeNumber_text, total_scope_fta_amt, total_scope_non_fta_amt, total_scope_eligible_amt, total_acc_reservation_amt, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, percent_disbursement_amt, last_disbursement_date, uuid, time_stamp)
    SELECT
        p.recipient_id AS grantee_id,
        gb.recipientAcronym AS acronym,
        gb.legalBusinessName AS grantee_name,
        p.recipient_cost_center AS grantee_cost_center,
        p.fiscal_year AS fiscal_year,
        p.application_number AS application_number,
        p.amendment_number AS amendment_number,
        p.application_name AS application_name,
        p.status AS status,
        p.application_type AS application_type,
		CASE 
			WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', p_gmtOffset) AS char))
			ELSE trams_format_date(fr.transmitted_date)
		END AS transmitted_date,
        trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00', p_gmtOffset) AS char)) AS latest_transmitted_date,
        CASE 
			WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00',p_gmtOffset) AS char))
			ELSE trams_format_date(fr.submitted_date)
		END AS submitted_date,			
        trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00',p_gmtOffset) AS char)) AS latest_submitted_date,
        p.recipient_contact AS grantee_poc,
        p.fta_pre_award_manager AS pre_award_mgr,
        p.fta_post_award_manager AS post_award_mgr,
        p.project_number AS project_number,
        p.project_title AS project_name,
        totals.funding_source_name AS fundingSourceName_text,
        totals.section_code AS accSectionCode_text,
        totals.scope_name AS scopeName_text,
        totals.scope_code AS scopeNumber_text,
        lir.total_scope_fta_amt AS total_scope_fta_amt,
        lir.total_scope_non_fta_amt AS total_scope_non_fta_amt,
        lir.total_scope_eligible_amt AS total_scope_eligible_amt,
        sar.total_acc_reservation_amt AS total_acc_reservation_amt,
        totals.total_obligation_amt AS total_obligation_amt,
        totals.total_deobligation_amt AS total_deobligation_amt,
        totals.total_disbursement_amt AS total_disbursement_amt,
        ((totals.total_disbursement_amt / totals.total_obligation_amt) * 100) AS percent_disbursement_amt,
        trams_format_date(cast(CONVERT_TZ(aa.disbursement_date_time, '+00:00', p_gmtOffset) AS char)) AS last_disbursement_date,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_temp_psb_application p
    JOIN
    (
        SELECT
            scope_code,
            section_code,
            MAX(project_number) AS max_project_number,
            proj_num_no_amend
        FROM
            /* Replaced reference to temp table and added uuid condition */
            trams_temp_psb_contract_acc_transaction_rollup
        WHERE uuid = @uuid
        GROUP BY
            proj_num_no_amend
    ) max_project
    ON
        p.project_number = max_project.max_project_number
    LEFT JOIN
        vwtramsGeneralBusinessInfo gb
    ON
        p.recipient_id = gb.recipientId
    LEFT JOIN
        trams_fta_review fr
    ON
        p.application_id = fr.application_id
    LEFT JOIN
        trams_application_award_detail aa
    ON
        p.application_id = aa.application_id
    LEFT JOIN
        trams_temp_psb_contract_acc_transaction_rollup2 totals
    ON
        max_project.proj_num_no_amend = totals.proj_num_no_amend
        AND totals.uuid = @uuid
    LEFT JOIN
        trams_temp_psb_line_item_rollup lir
    ON
        totals.proj_num_no_amend = lir.proj_num_no_amend
        AND totals.scope_code = lir.scope_code
        AND lir.uuid = @uuid
    LEFT JOIN
        (
            SELECT
                a.id AS applicationId,
                MAX(th.TransmittedDate) AS latest_transmitted_date,
                MAX(th.SubmittedDate) AS latest_submitted_date,
                MIN(th.TransmittedDate) AS original_transmitted_date,
                MIN(th.SubmittedDate) AS original_submitted_date
            FROM
                tramsApplicationTransmissionHistory th
            JOIN
                trams_application a
            ON
                a.id = th.applicationId
            GROUP BY th.applicationId
        ) latest_transmission_history
        ON p.application_id = latest_transmission_history.applicationId
    JOIN
        trams_temp_psb_scope_acc_rollup sar
    ON
        totals.proj_num_no_amend = sar.proj_num_no_amend
        AND totals.scope_code = sar.scope_number
        AND sar.uuid = @uuid
    WHERE p.uuid = @uuid;

    SET @appCount = (SELECT COUNT(*) FROM trams_temp_psb_application cp WHERE cp.uuid = @uuid AND (cp.contractAward_id IS NULL OR cp.contractAward_id = ''));
    call `trams_log`(@sid, _spName, concat(' Build Resultset No Award: ',now(),' ',@appCount), 1);

    INSERT INTO trams_temp_psb_resultset (grantee_id, acronym, grantee_name, grantee_cost_center, fiscal_year, application_number, amendment_number, application_name, status, application_type, transmitted_date, latest_transmitted_date, submitted_date, latest_submitted_date, grantee_poc, pre_award_mgr, post_award_mgr, project_number, project_name, fundingSourceName_text, accSectionCode_text, scopeName_text, scopeNumber_text, total_scope_fta_amt, total_scope_non_fta_amt, total_scope_eligible_amt, total_acc_reservation_amt, total_obligation_amt, total_deobligation_amt, total_disbursement_amt, percent_disbursement_amt, last_disbursement_date, uuid, time_stamp)
    SELECT
        p.recipient_id AS grantee_id,
        gb.recipientAcronym AS acronym,
        gb.legalBusinessName AS grantee_name,
        p.recipient_cost_center AS grantee_cost_center,
        p.fiscal_year AS fiscal_year,
        p.application_number AS application_number,
        p.amendment_number AS amendment_number,
        p.application_name AS application_name,
        p.status AS status,
        p.application_type AS application_type,
		CASE
			WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date, '+00:00', p_gmtOffset) AS char))
			ELSE trams_format_date(fr.transmitted_date)
		END AS transmitted_date,
        trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date, '+00:00',  p_gmtOffset) AS char)) AS latest_transmitted_date,
        CASE
			WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
			THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date, '+00:00', p_gmtOffset) AS char))
			ELSE trams_format_date(fr.submitted_date)
		END AS submitted_date,
        trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date, '+00:00', p_gmtOffset)AS char)) AS latest_submitted_date,
        p.recipient_contact AS grantee_poc,
        p.fta_pre_award_manager AS pre_award_mgr,
        p.fta_post_award_manager AS post_award_mgr,
        p.project_number AS project_number,
        p.project_title AS project_name,
        lir.funding_source_name AS funding_source_name,
        lir.section_code AS section_code,
        sar.scope_name AS scopeName_text,
        lir.scope_code AS scopeNumber_text,
        lir.total_scope_fta_amt AS total_scope_fta_amt,
        lir.total_scope_non_fta_amt AS total_scope_non_fta_amt,
        lir.total_scope_eligible_amt AS total_scope_eligible_amt,
        sar.total_acc_reservation_amt AS total_acc_reservation_amt,
        '' AS total_obligation_amt,
        '' AS total_deobligation_amt,
        '' AS total_disbursement_amt,
        '' AS percent_disbursement_amt,
        trams_format_date(cast(CONVERT_TZ(aa.disbursement_date_time, '+00:00', p_gmtOffset) AS char)) AS last_disbursement_date,
        @uuid AS uuid,
        @sid AS time_stamp
    FROM
        trams_temp_psb_application p
    LEFT JOIN
        vwtramsGeneralBusinessInfo gb
    ON
        p.recipient_id = gb.recipientId
    LEFT JOIN
        trams_fta_review fr
    ON
        p.application_id = fr.application_id
    LEFT JOIN
        trams_application_award_detail aa
    ON
        p.application_id = aa.application_id
    LEFT JOIN
        trams_temp_psb_no_contract_award_line_item_rollup lir
    ON
        p.project_number = lir.project_number
        AND lir.uuid = @uuid
    LEFT JOIN
        (
            SELECT
                a.id AS applicationId,
                MAX(th.TransmittedDate) AS latest_transmitted_date,
                MAX(th.SubmittedDate) AS latest_submitted_date,
                MIN(th.TransmittedDate) AS original_transmitted_date,
                MIN(th.SubmittedDate) AS original_submitted_date
            FROM
                tramsApplicationTransmissionHistory th
            JOIN
                trams_application a
            ON
                a.id = th.applicationId
            GROUP BY th.applicationId
        ) latest_transmission_history
        ON p.application_id = latest_transmission_history.applicationId
    JOIN
        trams_temp_psb_no_contract_award_scope_acc_rollup sar
    ON
        lir.project_number = sar.project_number
        AND lir.scope_code = sar.scope_number
        AND sar.uuid = @uuid
    WHERE
        p.recipient_id > ' ' AND (p.contractAward_id IS NULL OR p.contractAward_id = '') AND p.uuid = @uuid;

    CALL `trams_log`(@sid, _spName, concat(now(),' Done!'), 1);

    SELECT
        grantee_id,
        acronym,
        grantee_name,
        grantee_cost_center,
        fiscal_year,
        application_number,
        amendment_number,
        application_name,
        status,
        application_type,
        IF(transmitted_date = '0000-00-00', '', transmitted_date),
        IF(latest_transmitted_date = '0000-00-00', '', latest_transmitted_date),
        IF(submitted_date = '0000-00-00', '', submitted_date),
        IF(latest_submitted_date = '0000-00-00', '', latest_submitted_date),
        grantee_poc,
        pre_award_mgr,
        post_award_mgr,
        project_number,
        project_name,
        fundingSourceName_text,
        accSectionCode_text,
        scopeName_text,
        scopeNumber_text,
        total_scope_fta_amt,
        total_scope_non_fta_amt,
        total_scope_eligible_amt,
        total_acc_reservation_amt,
        total_obligation_amt,
        total_deobligation_amt,
        total_disbursement_amt,
        percent_disbursement_amt,
        IF(last_disbursement_date = '0000-00-00', '', last_disbursement_date)
    FROM trams_temp_psb_resultset
    WHERE uuid = @uuid;

    DELETE FROM trams_temp_psb_application WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_scope_acc_rollup WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_line_item_rollup WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_contract_acc_transaction_rollup WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_contract_acc_transaction_rollup2 WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_no_contract_award_line_item_rollup WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_no_contract_award_scope_acc_rollup WHERE uuid = @uuid;
    DELETE FROM trams_temp_psb_resultset WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_mpr_ffr_sub_review_report`(
  IN `p_contractAwardId` INT,
  IN `p_fiscalYear` VARCHAR(50),
  IN `p_fiscalQuarter` VARCHAR(50),
  IN `p_fiscalMonth` VARCHAR(50),
  IN `p_reportTypeToSearch` VARCHAR(20),
  IN `p_report_period_type` VARCHAR(50),
  IN `isFinalReport` TINYINT(1),
  IN `p_return_limit` VARCHAR(20)
)
BEGIN
 declare _sql varchar(40960) default "";
 declare _sql_final varchar(40960) default "";
 declare _finalWhereClause varchar(40960) default " ";
 declare _reportWhereClause varchar(40960) default " ";
 declare _limitClause varchar(100) default " ";
 CALL trams_get_sql_limit_clause(p_return_limit,_limitClause);
 -- sid will be written to the new table as a time stamp to be used for identifying and cleaning up any junk data
 SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
 -- uuid will serve as the unique identifier for rows generated by this specific procedure call. Multiple procedures run at the same time would generate unique uuids
 SET @uuid = UUID_SHORT();
 CALL `trams_log`(@sid, 'trams_get_mpr_ffr_sub_review', 'Started', 1);
  /*
  Set whereClaues for FFR or MPR
 */
    IF  p_report_period_type = 'Initial' THEN
      set _finalWhereClause = concat(_finalWhereClause,(select ifnull(concat(" and `final`.`fiscal_quarter`  = ",NULLIF(p_fiscalQuarter,'')),"")));
	  set _finalWhereClause = concat(_finalWhereClause,(select ifnull(concat(" and `final`.`fiscal_month`  = ",NULLIF(p_fiscalMonth,'')),"")));
    ELSEIF p_report_period_type = 'Monthly' THEN
      set _finalWhereClause = concat(_finalWhereClause,(select ifnull(concat(" and `final`.`fiscal_month`  = ",NULLIF(p_fiscalMonth,'')),"")));
    ELSEIF p_report_period_type = 'Quarterly' THEN
      set _finalWhereClause = concat(_finalWhereClause,(select ifnull(concat(" and `final`.`fiscal_quarter`  = ",NULLIF(p_fiscalQuarter,'')),"")));
    -- replaced "ELSEIF p_report_period_type = 'Annual' THEN" with "ELSE"
    ELSE
      set _finalWhereClause = concat(_finalWhereClause, " and 1=1");
    END IF;
 /*
Select FFR or  MPR  based on the where clause
 */
    IF p_reportTypeToSearch = "MPR" THEN
      INSERT INTO trams_temp_mpr_ffr_sub_review
      SELECT
        NULL AS `trams_temp_mpr_ffr_sub_review_id`,
        `MPR`.`id` AS `report_ID`,
        'MPR' AS `type`,
        `MPR`.`recipient_id` AS `recipient_ID_text`,
        `MPR`.`application_id` AS `application_id`,
        `MPR`.`contract_award_id`  as  `contract_award_id`,
        `MPR`.`report_period_type` AS `report_period_type`,
        `MPR`.`report_status` AS `report_status`,
        `MPR`.`fiscal_year` AS `fiscal_year_report`,
        `MPR`.`fiscal_quarter` AS `fiscal_quarter`,
        `MPR`.`fiscal_month` AS `fiscal_month`,
        trams_get_ffr_period_name(`MPR`.`report_period_type`,`MPR`.`fiscal_year`,`MPR`.`fiscal_month`,`MPR`.`fiscal_quarter`) AS `period`,
        `MPR`.`final_report_flag`  AS `final_report`,
        `MPR`.`due_date_time` AS `due_date`,
        `MPR`.`updated_by` AS `last_updated_by`,
        `MPR`.`updated_date_time` AS `last_updated_on`,
        `MPR`.`report_period_begin_date` AS `report_period_begin_date`,
        `MPR`.`report_period_end_date`   AS `report_period_end_date`,
        -- uuid column added
        @uuid AS `uuid`,
        -- sid column added
        @sid AS `time_stamp`
      FROM `trams_milestone_progress_report` `MPR`
      WHERE `MPR`.`contract_award_id` = p_contractAwardId
        AND `MPR`.`fiscal_year` = p_fiscalYear
        AND `MPR`.`report_period_type`=  p_report_period_type
        AND `MPR`.`final_report_flag`  = isFinalReport ;
    ELSEIF p_reportTypeToSearch = "FFR" THEN
    INSERT INTO trams_temp_mpr_ffr_sub_review
    SELECT
      NULL AS `trams_temp_mpr_ffr_sub_review_id`,
      `FFR`.`id` AS `report_ID`,
      'FFR' AS `type`,
      `FFR`.`recipient_id` AS `recipient_ID_text`,
      `FFR`.`application_id` AS `application_id`,
      `FFR`.`contract_award_id` AS `contract_award_id`,
      `FFR`.`report_period_type` AS `report_period_type`,
      `FFR`.`report_status` AS `report_status`,
      `FFR`.`fiscal_year` AS `fiscal_year_report`,
      `FFR`.`fiscal_quarter` AS `fiscal_quarter`,
      `FFR`.`fiscal_month` AS `fiscal_month`,
      trams_get_ffr_period_name(`FFR`.`report_period_type`,`FFR`.`fiscal_year`,`FFR`.`fiscal_month`,`FFR`.`fiscal_quarter`) AS `period`,
      `FFR`.`final_report_flag`  AS `final_report`,
      `FFR`.`due_date_time` AS `due_date`,
      `FFR`.`last_modified_by` AS `last_updated_by`,
      `FFR`.`last_modified_date_time` AS `last_updated_on`,
      `FFR`.`report_period_begin_date` AS `report_period_begin_date`,
      `FFR`.`report_period_end_date` AS `report_period_end_date`,
      -- uuid column added
      @uuid AS `uuid`,
      -- sid column added
      @sid AS `time_stamp`
    FROM  `trams_federal_financial_report` `FFR`
    WHERE `FFR`.`contract_award_id` = p_contractAwardId
      AND `FFR`.`fiscal_year` = p_fiscalYear
      AND `FFR`.`report_period_type`=  p_report_period_type
      AND  `FFR`.`final_report_flag` = isFinalReport ;
    END IF;
    SET _sql_final =
      "SELECT
         uuid_short() AS `reportSearchResultstempID`,
         `final`.`recipient_ID_text` AS `recipient_ID_text`,
         `final`.`application_id` AS `application_ID`,
         `final`.`contract_award_id` AS `contract_award_ID`,
         `final`.`report_ID` AS `report_ID`,
         `final`.`type` AS `report_type`,
         `final`.`report_period_type` AS `report_period_type_text`,
         `final`.`report_status` AS `report_status_text`,
         `final`.`fiscal_year_report` AS `fiscal_year_report_text`,
         `final`.`fiscal_quarter` AS `fiscal_quarter`,
         `final`.`fiscal_month` AS `fiscal_month`,
         `final`.`period` AS `period`,
         `final`.`final_report`  AS `final_report`,
         `final`.`due_date` AS `due_date`,
         `final`.`last_updated_by` AS `last_updated_by`,
         `final`.`last_updated_on` AS `last_updated_on`
       FROM trams_temp_mpr_ffr_sub_review final ";
-- uuid clause set
 SET @uuidClause = CONCAT(' where `final`.`uuid` = ', @uuid);
-- uuid clause added as CONCAT parameter
 SET @finalQuery = CONCAT(_sql_final,@uuidClause,_finalWhereClause,_limitClause);
 PREPARE finalStmt FROM @finalQuery;
 EXECUTE finalStmt;
-- deleting entries from table
 DELETE FROM trams_temp_mpr_ffr_sub_review WHERE uuid = @uuid;
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_report_search_result_for_closeout`(IN `p_contractAward` INT, IN `p_reportType` VARCHAR(50))
BEGIN

  declare _sql varchar(40960) default "";
  declare _whereClause varchar(4096) default " ";
  declare _limitClause varchar(100) default " ";
  
  case p_reportType 
  when 'MPR' then 
  set _sql = "select uuid_short() AS `reportSearchResultstempID `,
  `AP`.`contractAward_id` AS `contract_award_ID`,
  `AP`.`id` AS `application_ID`,
  `MPR`.`id` AS `report_ID`,
  'MPR' AS `report_type`,
  CAST(`AP`.`application_name` as CHAR(255)) AS `application_name_text`,
  `AP`.`application_number` AS `application_number_text`,
  `AP`.`application_type` AS `application_type_text`,
  `AP`.`recipient_cost_center` AS `cost_center_text`,
  `AP`.`recipient_id` AS `recipient_ID_text`,
  `AP`.`status` AS `status_text`,
  `AP`.`fiscal_year` AS `fiscal_year_text`,
  `AP`.`active_flag`  AS `active_text`,
  `MPR`.`report_period_type` AS `report_period_type_text`,
  `MPR`.`report_status` AS `report_status_text`,
  `MPR`.`fiscal_year` AS `fiscal_year_report_text`,
  `MPR`.`fiscal_quarter` AS `fiscal_quarter`,
  `MPR`.`fiscal_month` AS `fiscal_month`,
  trams_get_ffr_period_name(`MPR`.`report_period_type`,`MPR`.`fiscal_year`,`MPR`.`fiscal_month`,`MPR`.`fiscal_quarter`) AS `period`,
  `MPR`.`final_report_flag`  AS `final_report`,
  `MPR`.`due_date_time` AS `due_date`,
  `MPR`.`updated_by` AS `last_updated_by`,
  `MPR`.`updated_date_time` AS `last_updated_on` 
  from `trams_application` `AP` join `trams_milestone_progress_report` `MPR` 
  on `AP`.`contractAward_id` = `MPR`.`contract_award_id` 
  and `MPR`.`fiscal_year` > YEAR(DATE_SUB(NOW() , INTERVAL 10 YEAR)) ";
  
  
  when 'FFR' then 
  set _sql = "select uuid_short() AS `reportSearchResultstempID `,
  `AP`.`contractAward_id` AS `contract_award_ID`,
  `AP`.`id` AS `application_ID`,
  `FFR`.`id` AS `report_ID`,
  'FFR' AS `report_type`,
  CAST(`AP`.`application_name` as CHAR(255)) AS `application_name_text`,
  `AP`.`application_number` AS `application_number_text`,
  `AP`.`application_type` AS `application_type_text`,
  `AP`.`recipient_cost_center` AS `cost_center_text`,
  `AP`.`recipient_id`  AS `recipient_ID_text`,
  `AP`.`status` AS `status_text`,
  `AP`.`fiscal_year` AS `fiscal_year_text`,
  `AP`.`active_flag` AS `active_text`,
  `FFR`.`report_period_type` AS `report_period_type_text`,
  `FFR`.`report_status` AS `report_status_text`,
  `FFR`.`fiscal_year` AS `fiscal_year_report_text`,
  `FFR`.`fiscal_quarter` AS `fiscal_quarter`,
  `FFR`.`fiscal_month` AS `fiscal_month`,
  trams_get_ffr_period_name(`FFR`.`report_period_type`,`FFR`.`fiscal_year`,`FFR`.`fiscal_month`,`FFR`.`fiscal_quarter`) as `period`,
  `FFR`.`final_report_flag`  AS `final_report`,
  `FFR`.`due_date_time` AS `due_date`,
  `FFR`.`last_modified_by` AS `last_updated_by`,
  `FFR`.`last_modified_date_time` AS `last_updated_on` 
  from  `trams_application` `AP` join `trams_federal_financial_report` `FFR` 
  on `AP`.`contractAward_id` = `FFR`.`contract_award_id` 
  and `AP`.`active_flag` = 1 
  and `FFR`.`fiscal_year` > YEAR(DATE_SUB(NOW() , INTERVAL 10 YEAR))" ;
  else 
  set _sql = "
  select uuid_short() AS `reportSearchResultstempID `,
  `AP`.`contractAward_id` AS `contract_award_ID`,
  `AP`.`id` AS `application_ID`,
  `MPR`.`id` AS `report_ID`,
  'MPR' AS `report_type`,
  CAST(`AP`.`application_name` as CHAR(255)) AS `application_name_text`,
  `AP`.`application_number` AS `application_number_text`,
  `AP`.`application_type` AS `application_type_text`,
  `AP`.`recipient_cost_center` AS `cost_center_text`,
  `AP`.`recipient_id` AS `recipient_ID_text`,
  `AP`.`status` AS `status_text`,
  `AP`.`fiscal_year` AS `fiscal_year_text`,
  `AP`.`active_flag`  AS `active_text`,
  `MPR`.`report_period_type` AS `report_period_type_text`,
  `MPR`.`report_status` AS `report_status_text`,
  `MPR`.`fiscal_year` AS `fiscal_year_report_text`,
  `MPR`.`fiscal_quarter` AS `fiscal_quarter`,
  `MPR`.`fiscal_month` AS `fiscal_month`,
  trams_get_ffr_period_name(`MPR`.`report_period_type`,`MPR`.`fiscal_year`,`MPR`.`fiscal_month`,`MPR`.`fiscal_quarter`) AS `period`,
  `MPR`.`final_report_flag`  AS `final_report`,
  `MPR`.`due_date_time` AS `due_date`,
  `MPR`.`updated_by` AS `last_updated_by`,
  `MPR`.`updated_date_time` AS `last_updated_on` 
  from `trams_application` `AP` join `trams_milestone_progress_report` `MPR` 
  on `AP`.`contractAward_id` = `MPR`.`contract_award_id` 
  and `MPR`.`fiscal_year` > YEAR(DATE_SUB(NOW() , INTERVAL 10 YEAR)) ";
  end case;
    
  set _whereClause = concat(_whereClause, (select ifnull(concat(" where `AP`.`contractAward_id` in (",NULLIF(p_contractAward,''),")"),"")));  
  
  SET @finalQuery = concat(_sql,_whereClause,_limitClause);
  
  set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  CALL `trams_log`(@sid, 'trams_get_report_search_result_for_closeout', concat('Get report search result for closeout Query: ',@finalQuery), 1); 
  
  PREPARE finalStmt FROM @finalQuery;
  EXECUTE finalStmt;

  CALL `trams_log`(@sid, 'trams_get_report_search_result_for_closeout', 'Done!', 1);    
    
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_quarterly_disbursement_report_query`(
IN `p_recipientId` VARCHAR(60), IN `p_FAIN` VARCHAR(150), IN `p_project_number` VARCHAR(100), IN `p_cost_center_code` VARCHAR(60), IN `p_account_class_code` VARCHAR(80), IN `p_FPC` VARCHAR(40), IN `p_transaction_type` VARCHAR(30), IN `p_transaction_date` VARCHAR(10),
IN `p_transaction_date_compare` VARCHAR(2),
IN `p_transaction_date_end` VARCHAR(10),
IN `p_transaction_amount` VARCHAR(20),
IN `p_transaction_amnt_compare` VARCHAR(2),
IN `p_transaction_amount_end` VARCHAR(20),
IN `p_award_status` VARCHAR(40),
IN `p_scope_code` VARCHAR(40),
IN `p_scope_suffix` VARCHAR(10),
IN `p_section_code` VARCHAR(40),
IN `p_start_page` VARCHAR(4),
IN `p_pages` VARCHAR(4),
IN `p_page_size` VARCHAR(4),
IN `p_mode` VARCHAR(10), IN `p_return_limit` VARCHAR(20))
    NO SQL
BEGIN
 
 DECLARE _spName varchar(255) DEFAULT "trams_get_quarterly_disbursement_report_query";
 DECLARE _sql VARCHAR(4096) DEFAULT "";
 DECLARE _whereClause VARCHAR(4096) DEFAULT " ";
 DECLARE _limitClause VARCHAR(100) DEFAULT " ";
 DECLARE _limitSize INTEGER DEFAULT 100;  DECLARE _orderbyClause VARCHAR(100) DEFAULT " ";
 DECLARE _contract_award_id INTEGER DEFAULT NULL;
 DECLARE _cccWhere VARCHAR(4096) DEFAULT "";

  DECLARE _SQLStateCode CHAR(5) DEFAULT '00000';
 DECLARE _ErrorNumber INTEGER;
 DECLARE _MessageText VARCHAR(1000) DEFAULT ' ';
	
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
 BEGIN
	GET DIAGNOSTICS CONDITION 1
		_SQLStateCode = RETURNED_SQLSTATE,
		_ErrorNumber = MYSQL_ERRNO,
		_MessageText = MESSAGE_TEXT;
	IF _ErrorNumber <> '00000' THEN
		CALL `trams_log`(@sid, _spName, CONCAT('SQL STATE/ERROR: ',IFNULL(_SQLStateCode,'?'),' ',IFNULL(_ErrorNumber,'?'),' ',IFNULL(_MessageText,'?')), 1);
	END IF;
 END;

 SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
 SET SESSION max_heap_table_size = 1024*1024*128;
 SET @cnt = 0;
 SET @p_mode = p_mode;

 SET _cccWhere = (SELECT IFNULL(CONCAT(" AND tgbi.cost_center_code IN ('",NULLIF(p_cost_center_code,''),"')"),""));
 
 IF p_pages IS NULL OR p_page_size IS NULL OR p_start_page IS NULL
 THEN
	CALL trams_get_sql_limit_clause(200,_limitClause);
 ELSE
	SET _limitSize = (p_start_page + p_pages) * p_page_size;
	CALL trams_get_sql_limit_clause(_limitSize,_limitClause);
 END IF;
 
 DROP TEMPORARY TABLE IF EXISTS tmp_quarterly_disbursement_report_excel;
 
 SET _sql = "CREATE TEMPORARY TABLE tmp_quarterly_disbursement_report_excel
SELECT
--	(@cnt := @cnt + 1) AS rowNumber,
	tcat.recipient_id AS 'Recipient_Id',
	tcat.application_number AS 'FAIN',
	tcat.project_number AS 'Project_Number',
	-- project_title AS 'Project Title',
	tgbi.cost_center_code AS 'Cost_Center_Code',
	tcat.account_class_code AS 'Account_Class',
	tcat.fpc AS 'FPC',
	tcat.scope_code AS 'Scope_Code',
	tcat.scope_name AS 'Scope_Name',
	tcat.scope_suffix AS 'Scope_Suffix',
	tcat.section_code AS 'Section_Code',
	tfp.fpc_description AS 'Financial_Purpose',
	tcat.contract_award_id AS 'Award_Id',
	-- tapp.status AS `Award_Status`,
	if(tcat.transaction_type = 'Refund','Refund','Disbursement') AS 'Transaction_Type',
	date_format(date(tcat.transaction_date_time),'%Y-%m-%d') AS 'Transaction_Date',
	if(@p_mode = 'Excel',
	cast(replace(if(tcat.disbursement_amount = 0,concat('-',format(tcat.refund_amount,2)),format(tcat.disbursement_amount,2)),',','') as decimal(12,2)),
	if(tcat.disbursement_amount = 0,concat('-',format(tcat.refund_amount,2)),format(tcat.disbursement_amount,2))) AS 'Transaction_Amount'
	FROM `trams_contract_acc_transaction` tcat -- USE INDEX DisbursementsLookup
	JOIN trams_financial_purpose tfp ON tfp.fpc = tcat.fpc
    	JOIN trams_general_business_info tgbi on tgbi.recipient_id = tcat.recipient_id
	-- JOIN trams_application tapp on tapp.contractAward_id = tcat.contract_Award_id / cannot do this here
	-- JOIN trams_project tpt ON tpt.project_number = tcat.project_number
	WHERE tcat.transaction_type NOT IN ('Obligation','Deobligation','Payment') 
	AND (tcat.disbursement_amount <> 0 OR (tcat.refund_amount <> 0))
              ";

 IF p_FAIN IS NOT NULL THEN
	set @appTrans = (SELECT COUNT(*) FROM trams_contract_acc_transaction chkFAIN WHERE chkFAIN.application_number = p_FAIN LIMIT 1);
	IF @appTrans = 0 THEN
		set _contract_award_id = (select contract_award_id from trams_contract_acc_funding where application_number = p_FAIN LIMIT 1);
		IF _contract_award_id IS NOT NULL THEN
			set p_FAIN = NULL;
		END IF;
	END IF;
 END IF;

 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.recipient_id IN (",NULLIF(p_recipientId,''),")"),"")));

 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.application_number IN ('",NULLIF(p_FAIN,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.contract_award_id IN ('",NULLIF(_contract_award_id,''),"')"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.project_number IN ('",NULLIF(p_project_number,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT REPLACE(_cccWhere, '"', '')));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.account_class_code IN ('",NULLIF(p_account_class_code,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.fpc IN ('",NULLIF(p_FPC,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.scope_code IN ('",NULLIF(p_scope_code,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.scope_suffix IN ('",NULLIF(p_scope_suffix,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.section_code IN ('",NULLIF(p_section_code,''),"')"),"")));
 SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.transaction_type LIKE ('%",NULLIF(p_transaction_type,''),"%')"),"")));
 

 IF p_transaction_date_end IS NULL AND p_transaction_date IS NOT NULL THEN
   SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND date(tcat.transaction_date_time) ",IFNULL(p_transaction_date_compare,"=")," '",NULLIF(p_transaction_date,''),"'"),"")));
 ELSE
  IF p_transaction_date IS NOT NULL THEN
   SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND date(tcat.transaction_date_time) ",">="," '",NULLIF(p_transaction_date,''),"'"),"")));
   SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND date(tcat.transaction_date_time) ","<="," '",NULLIF(p_transaction_date_end,''),"'"),"")));
  END IF;
 END IF;

 IF p_transaction_amount_end IS NULL AND p_transaction_amount IS NOT NULL THEN
   IF INSTR(p_transaction_amount,'-') THEN
   	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND ((tcat.disbursement_amount < 0.01 AND abs(tcat.refund_amount) ",IFNULL(p_transaction_amnt_compare,"=")," abs('",REPLACE(NULLIF(p_transaction_amount,''),',',''),"'))"),"")));
	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" OR (tcat.disbursement_amount < 0.01 AND abs(tcat.disbursement_amount) ",IFNULL(p_transaction_amnt_compare,"=")," abs('",REPLACE(NULLIF(p_transaction_amount,''),',',''),"')))"),"")));
   ELSE
   	SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.disbursement_amount ",IFNULL(p_transaction_amnt_compare,"=")," '",REPLACE(NULLIF(p_transaction_amount,''),',',''),"'"),"")));
   END IF;
 ELSE
  IF p_transaction_amount IS NOT NULL THEN
   IF INSTR(p_transaction_amount,'-') THEN
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND ((tcat.disbursement_amount < 0.01 AND abs(tcat.refund_amount) ",">="," abs('",REPLACE(NULLIF(p_transaction_amount,''),',',''),"') AND abs(tcat.refund_amount) <= abs(",p_transaction_amount_end,"))"),"")));
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" OR ((tcat.disbursement_amount < 0.01 AND abs(tcat.disbursement_amount) ",">="," abs('",REPLACE(NULLIF(p_transaction_amount,''),',',''),"') AND abs(tcat.disbursement_amount) <= abs(",p_transaction_amount_end,"))))"),"")));
       ELSE
    SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.disbursement_amount ",">="," '",REPLACE(NULLIF(p_transaction_amount,''),',',''),"'"),"")));
    IF INSTR(p_transaction_amount_end,'-') THEN
     SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" OR ((tcat.disbursement_amount < 0.01 AND abs(tcat.disbursement_amount) ",">="," abs('",REPLACE(NULLIF(p_transaction_amount,''),',',''),"') AND abs(tcat.disbursement_amount) <= abs(",p_transaction_amount_end,"))))"),"")));
    ELSE
     SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND tcat.disbursement_amount ","<="," '",REPLACE(NULLIF(p_transaction_amount_end,''),',',''),"'"),"")));
    END IF;
   END IF;
  END IF;
 END IF;

 SET _orderbyClause = " ORDER BY tcat.project_id, tcat.application_number, tcat.recipient_id,  tcat.transaction_date_time ";  
 SET @applicationQuery = CONCAT(_sql,replace(_whereClause,'''''',''''),_orderbyClause,_limitClause);

 CALL `trams_log`(@sid, _spName, concat(now(),' Start CALL SP Inputs (',connection_id(),') : (',
IFNULL(`p_recipientId`,'NULL'),',',
IFNULL(`p_FAIN`,'NULL'),',',
IFNULL(`p_project_number`,'NULL'),',',
IFNULL(`p_cost_center_code`,'NULL'),',',
IFNULL(`p_account_class_code`,'NULL'),',',
IFNULL(`p_FPC`,'NULL'),',',
IFNULL(`p_transaction_type`,'NULL'),',',
IFNULL(`p_transaction_date`,'NULL'),',',
IFNULL(`p_transaction_date_compare`,'NULL'),',',
IFNULL(`p_transaction_date_end`,'NULL'),',',
IFNULL(`p_transaction_amount`,'NULL'),',',
IFNULL(`p_transaction_amnt_compare`,'NULL'),',',
IFNULL(`p_transaction_amount_end`,'NULL'),',',
IFNULL(`p_award_status`,'NULL'),',',
IFNULL(`p_scope_code`,'NULL'),',',
IFNULL(`p_scope_suffix`,'NULL'),',',
IFNULL(`p_section_code`,'NULL'),',',
IFNULL(`p_start_page`,'NULL'),',',
IFNULL(`p_pages`,'NULL'),',',
IFNULL(`p_page_size`,'NULL'),',',
IFNULL(`p_mode`,'NULL'),',',
IFNULL(`p_return_limit`,'NULL'),')'), 1);   

 CALL `trams_log`(@sid, _spName, CONCAT('Data Selection Query: ',@applicationQuery), 1);
 
 PREPARE applicationStmt FROM @applicationQuery;
 EXECUTE applicationStmt;


 SET @appCount=(SELECT COUNT(*) FROM tmp_quarterly_disbursement_report_excel);
 CALL `trams_log`(@sid, _spName, CONCAT(now(),' Total Matched Transactions: ',@appCount), 1);

 IF @appCount > 0 THEN
	 CREATE INDEX tmp_recipient_id_id ON tmp_quarterly_disbursement_report_excel(Recipient_Id);
	 ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Project_Title` VARCHAR(510) NULL AFTER `Project_Number`;
	 ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Recipient_Name` VARCHAR(510) NULL AFTER `Recipient_Id`;
 	 ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Award_Status` VARCHAR(40) NULL AFTER `Recipient_Name`;
 
 	 UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.Award_Status =
	  (SELECT status FROM trams_application app
		WHERE app.contractAward_id = tqdr.Award_Id AND status IS NOT NULL AND (status IN ('Closed') OR (status LIKE 'Active%')) LIMIT 1);

	 UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.Project_Title =
	  (SELECT ltp.project_title FROM trams_project ltp WHERE ltp.project_number = tqdr.project_number LIMIT 1);

	 UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.Project_Title = IFNULL(tqdr.Project_Title,'--') WHERE tqdr.Project_Title IS NULL;

	 UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET
	  tqdr.Recipient_Name =
	   (SELECT tapp.recipient_name FROM trams_application tapp WHERE tapp.recipient_id = tqdr.Recipient_Id LIMIT 1)
	 ;

	 UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.Recipient_Name = IFNULL(tqdr.Recipient_Name,'???') WHERE tqdr.Recipient_Name IS NULL;


	 IF p_award_status IS NOT NULL THEN
		DELETE FROM `tmp_quarterly_disbursement_report_excel`
		WHERE Award_Status IS NULL OR (NOT (Award_Status LIKE concat(p_award_status,'%')));
	 END IF;

	 SET @appCount=(SELECT COUNT(*) FROM tmp_quarterly_disbursement_report_excel);

	 IF @appCount < 1 THEN
		INSERT INTO tmp_quarterly_disbursement_report_excel (Project_Number,Project_Title) VALUES ('0000','-- No Records Found --');
	 END IF;
 ELSE
	ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Project_Title` VARCHAR(510) NULL AFTER `Project_Number`;
	ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Recipient_Name` VARCHAR(510) NULL AFTER `Recipient_Id`;
	ALTER TABLE `tmp_quarterly_disbursement_report_excel` ADD `Award_Status` VARCHAR(40) NULL AFTER `Recipient_Name`;
	INSERT INTO tmp_quarterly_disbursement_report_excel (Project_Number,Project_Title) VALUES ('0000','-- No Records Found --');
 END IF;

 SET @curRow = 0;

 IF p_pages IS NULL OR p_page_size IS NULL OR p_start_page IS NULL
 THEN
	ALTER TABLE tmp_quarterly_disbursement_report_excel DROP COLUMN Award_Id;

	ALTER TABLE tmp_quarterly_disbursement_report_excel ADD rowNumber INTEGER DEFAULT 0;

	UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.rowNumber = (@curRow := @curRow + 1)
	 ORDER BY Project_Number, FAIN, Recipient_Id, Transaction_Date DESC;  
	IF p_mode = 'Excel' THEN
    	SELECT
	tqdr.Project_Number,
	tqdr.Project_Title,
	tqdr.FAIN,
	tqdr.Recipient_Id,
	tqdr.Recipient_Name,
	tqdr.Award_Status,
	tqdr.Cost_Center_Code,
	tqdr.Account_Class,
	tqdr.FPC,
	tqdr.Financial_Purpose,
	tqdr.Scope_Code,
	tqdr.Scope_Name,
	tqdr.Scope_Suffix,
	tqdr.Section_Code,
	tqdr.Transaction_Type,
	tqdr.Transaction_Date,
	Cast( tqdr.Transaction_Amount AS decimal(12,2) ) AS 'Transaction_Amount'
	FROM tmp_quarterly_disbursement_report_excel tqdr
	ORDER BY tqdr.rowNumber ASC;
    
    ELSE
        SELECT
	tqdr.Project_Number,
	tqdr.Project_Title,
	tqdr.FAIN,
	tqdr.Recipient_Id,
	tqdr.Recipient_Name,
	tqdr.Award_Status,
	tqdr.Cost_Center_Code,
	tqdr.Account_Class,
	tqdr.FPC,
	tqdr.Financial_Purpose,
	tqdr.Scope_Code,
	tqdr.Scope_Name,
	tqdr.Scope_Suffix,
	tqdr.Section_Code,
	tqdr.Transaction_Type,
	tqdr.Transaction_Date,
	tqdr.Transaction_Amount
	FROM tmp_quarterly_disbursement_report_excel tqdr
	ORDER BY tqdr.rowNumber ASC;
    END IF;

 ELSE
	ALTER TABLE tmp_quarterly_disbursement_report_excel ADD rowNumber INTEGER DEFAULT 0;
	UPDATE `tmp_quarterly_disbursement_report_excel` tqdr
	 SET tqdr.rowNumber = (@curRow := @curRow + 1)
	 ORDER BY Project_Number, FAIN, Recipient_Id, Transaction_Date DESC;  
	SET @startRow = p_start_page * p_page_size;
	
	ALTER TABLE tmp_quarterly_disbursement_report_excel DROP COLUMN Award_Id;

	SET @totalRows = (SELECT count(*) FROM tmp_quarterly_disbursement_report_excel);

	IF @totalRows = _limitSize THEN
		SET _sql = "CREATE TEMPORARY TABLE tmp_records_count
	SELECT
 	1 AS rowNumber,
	count(*) AS totalRecordsCnt
	FROM `trams_contract_acc_transaction` tcat
	JOIN trams_financial_purpose tfp ON tfp.fpc = tcat.fpc
    	JOIN trams_general_business_info tgbi on tgbi.recipient_id = tcat.recipient_id
    	-- JOIN  trams_application tapp on tcat.contract_award_id = tapp.contractaward_id
	-- JOIN  trams_project tpt ON tpt.project_number = tcat.project_number
	WHERE tcat.transaction_type NOT IN ('Obligation','Deobligation','Payment') AND (tcat.disbursement_amount <> 0 OR (tcat.refund_amount <> 0))
              ";
 	SET @applicationQuery = CONCAT(_sql,replace(_whereClause,'''''',''''));

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

	SET @totalRecords = (SELECT totalRecordsCnt FROM tmp_records_count WHERE rowNumber = 1);
      		IF @totalRecords > _limitSize THEN
		    INSERT INTO tmp_quarterly_disbursement_report_excel (Project_Number,Project_Title)
		    VALUES ('0000',concat('-- ',_limitSize,' records returned from total of: ',@totalRecords,' records --'));
      		END IF;
	END IF;

	IF p_mode = 'Excel' THEN
    	SELECT
	tqdr.Project_Number,
	tqdr.Project_Title,
	tqdr.FAIN,
	tqdr.Recipient_Id,
	tqdr.Recipient_Name,
	tqdr.Award_Status,
	tqdr.Cost_Center_Code,
	tqdr.Account_Class,
	tqdr.FPC,
	tqdr.Financial_Purpose,
	tqdr.Scope_Code,
	tqdr.Scope_Name,
	tqdr.Scope_Suffix,
	tqdr.Section_Code,
	tqdr.Transaction_Type,
	tqdr.Transaction_Date,
    	Cast( tqdr.Transaction_Amount AS decimal(12,2) )  AS 'Transaction_Amount'
	FROM tmp_quarterly_disbursement_report_excel tqdr
	WHERE tqdr.rowNumber >= @startRow
	ORDER BY tqdr.rowNumber ASC;      
    ELSE
	SELECT
	tqdr.Project_Number,
	tqdr.Project_Title,
	tqdr.FAIN,
	tqdr.Recipient_Id,
	tqdr.Recipient_Name,
	tqdr.Award_Status,
	tqdr.Cost_Center_Code,
	tqdr.Account_Class,
	tqdr.FPC,
	tqdr.Financial_Purpose,
	tqdr.Scope_Code,
	tqdr.Scope_Name,
	tqdr.Scope_Suffix,
	tqdr.Section_Code,
	tqdr.Transaction_Type,
	tqdr.Transaction_Date,
    	tqdr.Transaction_Amount
	FROM tmp_quarterly_disbursement_report_excel tqdr
	WHERE tqdr.rowNumber >= @startRow
	ORDER BY tqdr.rowNumber ASC; 
    
    END IF;
	
 END IF;

 CALL `trams_log`(@sid, _spName,concat(now(),' Done!'), 1);
 
 DROP TEMPORARY TABLE IF EXISTS tmp_quarterly_disbursement_report_excel;
 DROP TEMPORARY TABLE IF EXISTS tmp_records_count;
 
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_get_user_details_excel_report`(p_granteeId VARCHAR(10),
p_userName VARCHAR(50),
p_userType VARCHAR(1024),
p_costCenter VARCHAR(20),
p_userStatus VARCHAR(255),
p_userRole VARCHAR(500),
p_return_limit VARCHAR(10)
)
    NO SQL
BEGIN
	declare _sql blob(65535) default "";
	declare _whereClause varchar(4096) default "WHERE true ";
	declare _whereStatus varchar(4096) default " ";
	declare _limitClause varchar(100) default " ";
	declare _role varchar(256) default "";
	declare _roleList varchar(2048) default "";
	declare _roleLength int default 0;
    declare _firstRoleFlag varchar(10) default "";
    declare _roleListKeep varchar(2048) default "";
	
	call trams_get_sql_limit_clause(p_return_limit,_limitClause);
	
	set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
	
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and `suo`.`tramsOrgId` in (",NULLIF(p_granteeId,''),")"),"")));
	
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and `su`.`username` in ('",NULLIF(p_userName,''),"')"),"")));
	
	if p_userType like '%Grantee%' then set _whereClause = concat(_whereClause, " and su.isGranteeFlag = '1'");
	end if;
	if p_userType like '%Reporter%' then set _whereClause = concat(_whereClause, " and su.isReporterFlag = '1'");
	end if;
	if p_userType like '%FTA%' then set _whereClause = concat(_whereClause, " and su.isFTAflag = '1'");
	end if;
	
	set _whereClause = concat(_whereClause, (select ifnull(concat(" and `suo`.`costCenter` in (",NULLIF(p_costCenter,''),")"),"")));
	
	set _roleList = p_userRole;
    if (_roleList = '' or _roleList is null) then
		set _roleListKeep = "";
	else
		set _roleListKeep = p_userRole;
	end if;
set _firstRoleFlag = true;
	get_roles_loop:
		loop
			if (_roleList = '' or _roleList is null) then
				leave get_roles_loop;
			end if;
			
			set _role = substring_index(_roleList, ',', 1);
			set _roleList = mid(_roleList, length(_role) + 2, length(_roleList));
			
			if _firstRoleFlag = true then
				set _firstRoleFlag = false;
				if (_roleList = '' or _roleList is null) then
					set _whereClause = concat(_whereClause, " and ur.group_name like '%", _role, "%'");
				else
					set _whereClause = concat(_whereClause, " and (ur.group_name like '%", _role, "%'");
				end if;
			else 
				if (_roleList = '' or _roleList is null) then
					set _whereClause = concat(_whereClause, " or ur.group_name like '%", _role, "%')");
				else 
					set _whereClause = concat(_whereClause, " or ur.group_name like '%", _role, "%'");
				end if;
			end if;
            
		end loop get_roles_loop;
		
	if p_userStatus like '%Active%' then set _whereStatus = concat(_whereStatus, " or (`su`.`active`=1 and (`lock`.`isLocked`=0 or `lock`.`isLocked` IS NULL))");
	end if;
	if p_userStatus like '%Locked%' then set _whereStatus = concat(_whereStatus, " or `lock`.`isLocked`=1");
	end if;
	if p_userStatus like '%Deactivated%' then set _whereStatus = concat(_whereStatus, " or `su`.`active`=0");
	end if;	
	
	if _whereStatus != " " then set _whereClause = concat(_whereClause,"and (false",_whereStatus,")");
	end if;
	
	set _sql = concat(
	"SELECT `su`.`username` , 
       concat(`su`.`firstname`, ' ', Ifnull(Nullif(concat(`su`.`middlename`, ' '), ''), ''), `su`.`lastname`)  AS `name`, 
       `su`.`workemail`, 
       `su`.`workphone`, 
       `su`.`address1`, 
       `su`.`address2`, 
       `su`.`addresscity`, 
       `su`.`addressstate`, 
       `su`.`addresszip`, 
       `su`.`title`, 
       trams_format_date(`su`.`lastcertifieddate`) AS lastcertifieddate, 
       `su`.`lastcertifiedby` as `lastcertifiedby`, 
		
         (CASE 
           WHEN `su`.`isGranteeFlag` THEN 'Grantee' 
           WHEN `su`.`isReporterFlag` THEN 'Reporter' 
           WHEN `su`.`isFTAflag` THEN 'FTA' 
           ELSE 'N/A' 
         end) as  `usertype`, 
	    (CASE 
           WHEN '", _roleListKeep, "' = '' THEN `ur`.`group_name`
           WHEN '", _roleListKeep, "' is null THEN `ur`.`group_name`
           ELSE trams_format_user_roles(`ur`.`group_name`,'", _roleListKeep , "')
         end) as  `userRoles`,
		`suo`.`tramsOrgId` ,
		`suo`.`costCenter` , 
		(CASE 
           WHEN `lock`.`isLocked` THEN 'Locked' 
           WHEN `su`.`active` THEN 'Active' 
           ELSE 'Deactivated' 
         end) as `userstatus`,
        trams_format_date(`ur`.`active_date`) AS active_date  , 
       trams_format_date(`ur`.`deactivated_date_time`) AS deactivated_date_time   
	FROM   `FACS_SystemUsers` `su` 
        JOIN `trams_user_roles` `ur` 
        ON `su`.`username` = `ur`.`user_name`
		JOIN `FACS_SystemUserOrganizationSponsor` `suo`
		ON `su`.`userId` = `suo`.`userId`
		LEFT JOIN `FACS_UserLock` `lock`
		ON `su`.`userId` = `lock`.`userId`");
		
	SET @Query = concat(_sql,_whereClause,_limitClause, " order by suo.tramsOrgId");
	
    
    
	PREPARE getUserDetailsStmt FROM @Query;
    EXECUTE getUserDetailsStmt;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_retrieve_fms_compare_report_data`( OUT `p_fullError` VARCHAR(2000) )
    NO SQL
BEGIN 

   /* **************************************************************************************************
  ERROR HANDLER
  **************************************************************************************************** */ 
  DECLARE EXIT HANDLER FOR SQLEXCEPTION
  BEGIN
    GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, 
      @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
    SET p_fullError = CONCAT("ERROR ", @errno, " (", @sqlstate, "): ", @text);
  END;
  
  SELECT rpad(concat( 
         run_no, SPACE(1), 
         doc_id_type, SPACE(1), 
         rpad(doc_id_num, 9, ' '), SPACE(1), 
         doc_id_ffy, SPACE(1),
         doc_id_suffix, SPACE(1),
         rpad(ifnull(obj_clas, ''), 4, ' '), SPACE(1),
         rpad(approp_code, 4, ' '), SPACE(1),
         approp_code_lim, SPACE(1),
         rpad(ifnull(cost_cntr, ''), 6, ' '), SPACE(1),
         rpad(ifnull(org_code, ''), 3, ' '), SPACE(1),
         fpc, SPACE(1),
         concat(rpad(prog_elem, 5, ' '), 
         public_gov), SPACE(1),
         concat(rpad(echono, 9, ' '), 
         rpad(vendor_ssn, 10, ' ')), 
         rpad(reserv_date, 6, ' '), SPACE(1),
         rpad(first_ob_date, 6, ' '), SPACE(1),
         rpad(last_ob_date, 6, ' '), SPACE(1),
         rpad(last_disb_date, 6, ' '), SPACE(1),
         rpad(close_date, 6, ' '), SPACE(1),
         if(cm0_obligation < 0, '-', if(cm0_obligation < 1000000000, ' ', '')), lpad(round(abs(cm0_obligation), 2), if(cm0_obligation < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_deobs < 0, '-', if(cm0_deobs < 1000000000, ' ', '')), lpad(round(abs(cm0_deobs), 2), if(cm0_deobs < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_payment < 0, '-', if(cm0_payment < 1000000000, ' ', '')), lpad(round(abs(cm0_payment), 2), if(cm0_payment < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_refunds < 0, '-', if(cm0_refunds < 1000000000, ' ', '')), lpad(round(abs(cm0_refunds), 2), if(cm0_refunds < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_auth_disb < 0, '-', if(cm0_auth_disb < 1000000000, ' ', '')), lpad(round(abs(cm0_auth_disb), 2), if(cm0_auth_disb < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_obligation < 0, '-', if(py0_obligation < 1000000000, ' ', '')), lpad(round(abs(py0_obligation), 2), if(py0_obligation < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_deobs < 0, '-', if(py0_deobs < 1000000000, ' ', '')), lpad(round(abs(py0_deobs),2), if(py0_deobs < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_payment < 0, '-', if(py0_payment < 1000000000, ' ', '')), lpad(round(abs(py0_payment), 2), if(py0_payment < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_refunds < 0, '-', if(py0_refunds < 1000000000, ' ', '')), lpad(round(abs(py0_refunds), 2), if(py0_refunds < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_auth_disb < 0, '-', if(py0_auth_disb < 1000000000, ' ', '')), lpad(round(abs(py0_auth_disb), 2), if(py0_auth_disb < 1000000000, 12, 13), 0), SPACE(2),
         "N"
         ), 293, " ") as compare_output
  FROM `trams_fms_compare_team_final`
  UNION ALL
  SELECT concat( 
         run_no, SPACE(1), 
         doc_id_type, SPACE(1), 
         rpad(doc_id_num, 9, ' '), SPACE(1), 
         doc_id_ffy, SPACE(1),
         doc_id_suffix, SPACE(1),
         rpad(ifnull(obj_clas, ''), 4, ' '), SPACE(1),
         rpad(approp_code, 4, ' '), SPACE(1),
         approp_code_lim, SPACE(1),
         rpad(ifnull(cost_cntr, ''), 6, ' '), SPACE(1),
         rpad(ifnull(org_code, ''), 3, ' '), SPACE(1),
         fpc, SPACE(1),
         concat(rpad(prog_elem, 5, ' '), 
         public_gov), SPACE(1),
         concat(rpad(echono, 9, ' '), 
         rpad(vendor_ssn, 10, ' ')),
         rpad(reserv_date, 6, ' '), SPACE(1),
         rpad(first_ob_date, 6, ' '), SPACE(1),
         rpad(last_ob_date, 6, ' '), SPACE(1),
         rpad(last_disb_date, 6, ' '), SPACE(1),
         rpad(close_date, 6, ' '), SPACE(1),
         if(cm0_obligation < 0, '-', if(cm0_obligation < 1000000000, ' ', '')), lpad(round(abs(cm0_obligation), 2), if(cm0_obligation < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_deobs < 0, '-', if(cm0_deobs < 1000000000, ' ', '')), lpad(round(abs(cm0_deobs), 2), if(cm0_deobs < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_payment < 0, '-', if(cm0_payment < 1000000000, ' ', '')), lpad(round(abs(cm0_payment), 2), if(cm0_payment < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_refunds < 0, '-', if(cm0_refunds < 1000000000, ' ', '')), lpad(round(abs(cm0_refunds), 2), if(cm0_refunds < 1000000000, 12, 13), 0), SPACE(1),
         if(cm0_auth_disb < 0, '-', if(cm0_auth_disb < 1000000000, ' ', '')), lpad(round(abs(cm0_auth_disb), 2), if(cm0_auth_disb < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_obligation < 0, '-', if(py0_obligation < 1000000000, ' ', '')), lpad(round(abs(py0_obligation), 2), if(py0_obligation < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_deobs < 0, '-', if(py0_deobs < 1000000000, ' ', '')), lpad(round(abs(py0_deobs),2), if(py0_deobs < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_payment < 0, '-', if(py0_payment < 1000000000, ' ', '')), lpad(round(abs(py0_payment), 2), if(py0_payment < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_refunds < 0, '-', if(py0_refunds < 1000000000, ' ', '')), lpad(round(abs(py0_refunds), 2), if(py0_refunds < 1000000000, 12, 13), 0), SPACE(1),
         if(py0_auth_disb < 0, '-', if(py0_auth_disb < 1000000000, ' ', '')), lpad(round(abs(py0_auth_disb), 2), if(py0_auth_disb < 1000000000, 12, 13), 0), SPACE(2),
         "Y", SPACE(2),
         rpad(ifnull(application_fain,''), 11, '0'), SPACE(2),
         rpad(ifnull(project_identifier,''), 13, '0'), SPACE(2),
         rpad(ifnull(scope_code,''), 4, ' '), SPACE(2),
         rpad(ifnull(scope_suffix,''), 2, ' ')
         ) as compare_output
  FROM `trams_fms_compare_trams_final`;
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_insert_fiscal_year_apportionments_for_YEC`(IN `p_current_fiscal_year` VARCHAR(4))
    NO SQL
begin
insert into trams_fiscal_year_apportionments (`congressional_appropriation_id`, 
`fiscal_year`, 
`funding_source_name`, 
`funding_fiscal_year`, 
`appropriation_code`, 
`section_code`, 
`limitation_code`, 
`uza_code`, 
`lapse_year`, 
`initial_carryover_amount`, 
`initial_ceiling_amount`, 
`initial_cap_amount`)
SELECT 
a.congressional_appropriation_id as `congressional_appropriation_id`,
p_current_fiscal_year+1 as `fiscal_year`, 
a.formula_program_name as `funding_source_name`, 
a.funding_fiscal_year as `funding_fiscal_year`, 
a.appropriation_code as `appropriation_code`, 
a.section_id as `section_code`, 
a.limitation_code as `limitation_code`, 
a.uza_code as `uza_code`, 
a.lapse_period as `lapse_year`, 
ar.app_availableamt_dec as `initial_carryover_amount`, 
ar.ceiling_availableamt_dec as `initial_ceiling_amount`, 
ar.cap_availableamt_dec as `initial_cap_amount`
FROM `trams_apportionment` a join trams_announced_apportionment_rollup_view ar 
  on a.uza_code=ar.uzacode_text and
    a.funding_fiscal_year=ar.fundingfiscalyear_text and
    a.appropriation_code=ar.appropriationcode_text and
    a.section_id=ar.sectionid_text and
    a.limitation_code=ar.limitationcode_text
WHERE active_flag=1 and ar.app_availableamt_dec>0 and a.lapse_period>p_current_fiscal_year
group by 
`uza_code`,
`funding_fiscal_year`, 
`section_id`, 
`limitation_code`, 
`appropriation_code`;

end$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_select_mpr_ffr_with_non_add_scope_issue`()
BEGIN
  
  

  DROP TABLE IF EXISTS `trams_tmp_mpr_created_date`;
  CREATE TEMPORARY TABLE `trams_tmp_mpr_created_date`
  SELECT `id`,
         `application_id`,
         date_add(`report_period_end_date`, INTERVAL 1 DAY) as created_date
  FROM `trams_milestone_progress_report`
  WHERE `final_report_flag` = 0
    AND `from_team` = 0;

  

  DROP TABLE IF EXISTS `trams_tmp_ffr_created_date`;
  CREATE TEMPORARY TABLE `trams_tmp_ffr_created_date`
  SELECT `id`,
         `application_id`,
         date_add(`report_period_end_date`, INTERVAL 1 DAY) as created_date
  FROM `trams_federal_financial_report`
  WHERE `final_report_flag` = 0
    AND `from_team` = 0;

  
  
  DROP TABLE IF EXISTS `trams_tmp_non_add_scope_applications`;
  CREATE TEMPORARY TABLE `trams_tmp_non_add_scope_applications`
  SELECT a.`id` as application_id,
         d.`obligated_date_time` as cutoff_date
  FROM `trams_application` a
  INNER JOIN `trams_application_award_detail` d ON d.`application_id` = a.`id`
  INNER JOIN `trams_line_item` l ON l.`app_id` = a.`id`
  WHERE a.`created_date_time` > '2016-01-01'
    AND a.`app_from_team_flag` = 1
    AND l.`other_budget` = 1
  GROUP BY application_id
  UNION
  SELECT a.`id` as application_id,
         b.`approved_date` as cutoff_date
  FROM `trams_application` a
  INNER JOIN `trams_budget_revision` b ON b.`application_id` = a.`id`
  INNER JOIN `trams_line_item` l ON l.`app_id` = a.`id`
  WHERE a.`app_from_team_flag` = 1
    AND b.`revision_status` = 'Approved'
    AND b.`created_date_time` > '2016-01-01'
    AND b.`revision_number` > 0
    AND l.`other_budget` = 1
  GROUP BY b.`id`;

  

  SELECT *
  FROM `trams_tmp_non_add_scope_applications` a
  INNER JOIN `trams_tmp_mpr_created_date` t ON t.`application_id` = a.`application_id`
  INNER JOIN `trams_milestone_progress_report` m ON m.`id` = t.`id`
  INNER JOIN `trams_application` app ON app.`id` = a.`application_id`
  WHERE t.`created_date` >= a.`cutoff_date`
  GROUP BY m.`id`;

  

  SELECT *
  FROM `trams_tmp_non_add_scope_applications` a
  INNER JOIN `trams_tmp_ffr_created_date` t ON t.`application_id` = a.`application_id`
  INNER JOIN `trams_federal_financial_report` f ON f.`id` = t.`id`
  INNER JOIN `trams_application` app ON app.`id` = a.`application_id`
  WHERE t.`created_date` >= a.`cutoff_date`
  GROUP BY f.`id`;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_run_usa_spending`(IN p_begin_date DATE, IN p_end_date DATE, IN p_file_type VARCHAR(1), IN p_budget_revision BOOLEAN)
BEGIN

/*
    Stored Procedure Name : trams_run_usa_spending	 
    Update by: Jerrin Thomas (1/13/2022) as part of TOM-12625	 
    Changes: Updated to gather applications with PoP End Dates changed during budget revision         
*/

  DECLARE v_fiscal_year VARCHAR(4);
  SET v_fiscal_year = trams_get_current_fiscal_year(NOW());
	SET @uuid = UUID_SHORT();
	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
  
  IF p_budget_revision = 1 THEN 
    INSERT INTO trams_temp_CFDATable (application_id, CFDA_number, total_amount, funding_source_name, uuid, time_stamp)
        SELECT
        ta.id AS application_Id,
        fdaNum.cfda_program_number AS CFDA_number,
        SUM(tpn.amendment_Obligation_amount) AS total_amount,
        tfsr.funding_source_name AS funding_source_name,    
        @uuid AS uuid,  
        @sid AS time_stamp
        FROM trams_application ta
        JOIN trams_po_number tpn                 ON ta.id = tpn.application_id
        JOIN trams_funding_source_reference tfsr ON tpn.funding_source_name = tfsr.funding_source_name
        AND tfsr.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        AND tfsr.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        JOIN trams_fda_number fdaNum             ON fdaNum.fed_domestic_asst_num = tfsr.fed_dom_assistance_num
        JOIN (
          /* Subquery to avoid errors when calculating total_amount */
          SELECT
            pop.ApplicationId
          FROM
            tramsPOPChangelog pop 
          JOIN
            trams_status_history sh
            ON pop.ApplicationId = sh.context_id
          WHERE
            pop.SourceOfChangeId = 69 /* Budget Revision */
            AND pop.isApproved = 1
            AND sh.context_details = 'Application Status'
            AND sh.previous_status = 'Active / Budget Revision Under Review'
            AND sh.new_status = 'Active (Executed)'
            AND (DATE(sh.change_date) BETWEEN p_begin_date AND p_end_date)
          GROUP BY
            pop.ApplicationId
        ) popApp on ta.id = popApp.ApplicationId
        GROUP BY
        Application_Id,
        CFDA_number;
  ELSE
    INSERT INTO trams_temp_CFDATable (application_id, CFDA_number, total_amount, funding_source_name, uuid, time_stamp)
        SELECT
        ta.id AS application_Id,
        fdaNum.cfda_program_number AS CFDA_number,
        SUM(tpn.amendment_Obligation_amount) AS total_amount,
        tfsr.funding_source_name AS funding_source_name,	
        @uuid AS uuid,
        @sid AS time_stamp
        FROM trams_application ta
        JOIN trams_po_number tpn                 ON ta.id = tpn.application_id
        JOIN trams_funding_source_reference tfsr ON tpn.funding_source_name = tfsr.funding_source_name
        AND tfsr.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        AND tfsr.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
        JOIN trams_fda_number fdaNum             ON fdaNum.fed_domestic_asst_num = tfsr.fed_dom_assistance_num
        JOIN trams_application_award_detail aa   ON ta.id = aa.application_id
        WHERE
        (DATE(aa.obligated_date_time) BETWEEN p_begin_date AND p_end_date OR DATE(aa.fta_closeout_approval_date_time) BETWEEN p_begin_date AND p_end_date)
        GROUP BY
        Application_Id,
        CFDA_number;
  END IF;

  
	
	INSERT INTO trams_temp_nonFedTotalAmount (application_id, funding_source_name, CFDANumber, local_funding_amount, uuid, time_stamp)
		SELECT
		ta.id AS application_id,
		li.funding_source_name AS funding_source_name,
		fdaNum.cfda_program_number AS CFDANumber,
		IF(ta.amendment_number = 0,
			(SUM(IFNULL(li.revised_total_amount, 0)) - SUM(IFNULL(li.revised_FTA_amount, 0)) - SUM(IFNULL(li.revised_other_fed_amount, 0))),
			((SUM(IFNULL(li.revised_total_amount, 0)) - SUM(IFNULL(li.revised_FTA_amount, 0)) - SUM(IFNULL(li.revised_other_fed_amount, 0))) -
			(SUM(IFNULL(li.latest_amend_total_amount, 0)) - SUM(IFNULL(li.latest_amend_FTA_amount, 0)) - SUM(IFNULL(li.latest_amend_other_fed_amount, 0))))
		) AS local_funding_amount,
		@uuid AS uuid,
		@sid AS time_stamp
		FROM trams_application ta
		JOIN trams_application_award_detail aa ON aa.application_id = ta.id
		JOIN trams_line_item li                ON li.app_id  = ta.id
			AND li.other_budget = 0
		JOIN trams_funding_source_reference tfsr ON li.funding_source_name = tfsr.funding_source_name
			AND tfsr.LatestFlagStartDate <= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
			AND tfsr.LatestFlagEndDate >= fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(ta.contractAward_id, NULL)
		JOIN trams_fda_number fdaNum             ON fdaNum.fed_domestic_asst_num = tfsr.fed_dom_assistance_num
		WHERE
		ta.id IN (SELECT application_id FROM trams_temp_CFDATable WHERE uuid = @uuid)
		GROUP BY
		application_id,
		CFDANumber;

    SELECT
      IFNULL(CFDA.CFDA_number, '00.000') AS CFDA_number,
      (CASE
        WHEN
        (
          sa.eo12372_review_flag = 0
            AND
            (
              UPPER(tca.clearinghouse_number) = ' '
              OR  UPPER(tca.clearinghouse_number) LIKE '%NOT%APPLICABLE%'
              OR  UPPER(tca.clearinghouse_number) LIKE '%SEE%PROJ%DESCR%'
              OR  UPPER(tca.clearinghouse_number) LIKE '%N/A%'
              OR  UPPER(tca.clearinghouse_number) LIKE '%NON%'
              OR  UPPER(tca.clearinghouse_number) IN ('1', '00', 'N', '.', 'NO')
            )
        ) THEN 'SAI EXEMPT'
        WHEN
        (
          (
            sa.eo12372_review_flag = 1
            AND UPPER(tca.clearinghouse_number) = ' '
          )
            OR UPPER(tca.clearinghouse_number) = ' '
            OR UPPER(tca.clearinghouse_number) LIKE '%N/A%'
            OR UPPER(tca.clearinghouse_number) LIKE '%NO%SPECIFIED%'
            OR UPPER(tca.clearinghouse_number) IN ('STATE', 'TENNESSEE', 'WV-1')
        ) THEN 'SAI NOT AVAILABLE'
        ELSE ''
        END
      ) AS SAI_Number,
	  
	  -- Placeholder field added in TOM-11772
	  "" AS FundingOpportunityNumber,
      IF(NULLIF(ta.recipient_name, ' ') = NULL, 'N/A', ta.recipient_name) AS AwardeeOrRecipientLegalEntityName,
      LEFT(tl.zipCode, 5) AS LegalEntityZIP5,
      IFNULL(NULLIF(tl.zipPlus4,''),'0000') AS LegalEntityZIPLast4,
      '' AS LegalEntityForeignCityName,
      IFNULL(gb.DaimsBusinessTypeCode, 'X') AS BusinessType,
      (CASE
        WHEN ta.amendment_reason = 'New Application' AND IFNULL(aa.total_fta_amount,0) >= 0 AND ta.amendment_number = '0' THEN 'A'
        WHEN IFNULL(aa.total_fta_amount,0) = 0 AND ta.amendment_number > 0  THEN 'B'
        ELSE 'C'
        END
      ) AS ActionType,
      '6955' AS AwardingSubTierAgencyCode,
      TRIM(TRAILING RIGHT(ta.application_number, 3) FROM ta.application_number) AS FAIN,
      ta.amendment_number AS AwardModificationAmendmentNumber,
      IFNULL(CFDA.total_amount,0) AS FederalActionObligation,
      li.local_funding_amount AS NonFederalFundingAmount,
	  -- Placeholder field added in TOM-11772
	  "" AS IndirectCostFederalShareAmount,
      IF(NULLIF(aa.fta_closeout_approval_date_time, '') IS NULL, DATE_FORMAT(aa.obligated_date_time, '%Y%m%d'), DATE_FORMAT(aa.fta_closeout_approval_date_time, '%Y%m%d')) AS ActionDate,
      IF(ta.start_date IS NULL OR ta.start_date = '',
        '',
        DATE_FORMAT(ta.start_date, '%Y%m%d')
      ) AS PeriodOfPerformanceStartDate,
	  -- Gets the PeriodOfPerformanceCurrentEndDate from a function instead of trams_application
      DATE_FORMAT(fntramsGetApplicationPOPEndDateFinalized(ta.id, ta.contractAward_id), '%Y%m%d') AS PeriodOfPerformanceCurrentEndDate,
      IF(ta.application_type = 'Grant', '04', '05') AS AssistanceType,
       '2' AS RecordType,
      IFNULL(p_file_type, '') as CorrectionLateDeleteIndicator,
      CONCAT(
		trams_get_current_fiscal_year(
			IF(NULLIF(aa.fta_closeout_approval_date_time, '') IS NULL,
				DATE_FORMAT(aa.obligated_date_time, '%Y%m%d'),
				DATE_FORMAT(aa.fta_closeout_approval_date_time, '%Y%m%d')
			)
		),
        trams_get_current_fiscal_quarter(IF(NULLIF(aa.fta_closeout_approval_date_time, '') IS NULL, DATE_FORMAT(aa.obligated_date_time, '%Y%m%d'), DATE_FORMAT(aa.fta_closeout_approval_date_time, '%Y%m%d')))
        ) AS FiscalYearAndQuarterCorrection,
      CONCAT(ta.fain_state, '*****') AS PrimaryPlaceOfPerformanceCode,
      '' AS PrimaryPlaceOfPerformanceZIPPlus4,
      '' AS LegalEntityForeignProvinceName,
      LEFT(REPLACE(ta.application_description, '"', ''), 18000) AS AwardDescription,
	  -- Placeholder field added in TOM-11772
	  "" AS FundingOpportunityGoalsText,
      IF(tl.addressLine1 = '', 'N/A', tl.addressLine1) AS LegalEntityAddressLine1,
      IF(tl.addressLine2 = '', 'N/A', tl.addressLine2) AS LegalEntityAddressLine2,
      'N/A' AS LegalEntityAddressLine3,
      '' AS FaceValueLoanGuarantee,
      '' AS OriginalLoanSubsidyCost,
      'NON' AS BusinessFundsIndicator,
      'USA' AS LegalEntityCountryCode,
      'USA' AS PrimaryPlaceOfPerformanceCountryCode,
      CONCAT(ta.application_number, '-', CFDA_number) AS URI,
      '' AS LegalEntityForeignPostalCode,
      '069' AS FundingAgencyCode,
      '6955' AS FundingSubTierAgencyCode,
      '693JJ8' AS FundingOfficeCode,
	  -- Field added in TOM-11772
	  gb.duns AS AwardeeOrRecipientDUNS,
	  -- Field added in TOM-7447
	  gb.ueid AS AwardeeOrRecipientUEI,
      '069' AS AwardingAgencyCode,
	  -- Case statement added to choose the correct code for each cost center. Cost centers not listed here use the code for TPM
      (CASE 
		-- Region 1
        WHEN ta.recipient_cost_center = 78100 THEN '691384'
		-- Region 2
        WHEN ta.recipient_cost_center = 78200 THEN '692387'
		-- Region 3
        WHEN ta.recipient_cost_center = 78300 THEN '693601'
		-- Region 4
        WHEN ta.recipient_cost_center = 78400 THEN '6943DB'
		-- Region 5
		WHEN ta.recipient_cost_center = 78500 THEN '695124'
		-- Region 6
        WHEN ta.recipient_cost_center = 78600 THEN '6974U7'
		-- Region 7
        WHEN ta.recipient_cost_center = 78700 THEN '696482'
		-- Region 8
        WHEN ta.recipient_cost_center = 78800 THEN '6982SY'
		-- Region 9
        WHEN ta.recipient_cost_center = 78900 THEN '699J58'
		-- Region 10
		WHEN ta.recipient_cost_center = 79000 THEN '6905A5'
		-- TAD
        WHEN ta.recipient_cost_center = 62000 THEN '693195'
		-- TBP
        WHEN ta.recipient_cost_center = 66000 THEN '69A442'
		-- TSO
        WHEN ta.recipient_cost_center = 74000 THEN '69A444'
		-- TRI
        WHEN ta.recipient_cost_center = 67000 THEN '69A445'
		-- TPE
		WHEN ta.recipient_cost_center = 71000 THEN '69A446'
		-- TPM
        ELSE '69A443'
        END
      ) AS AwardingOfficeCode,
      '' AS PrimaryPlaceOfPerformanceForeignLocationDescription,
      (CASE
        WHEN ta.fain_state IN ('AK', 'DE', 'MT', 'ND', 'SD', 'VT', 'WY') THEN '00'
        WHEN ta.fain_state IN ('AS', 'DC', 'GU', 'MP', 'PR', 'VI') THEN '98'
        WHEN ta.fain_state IN ('FM', 'MH', 'PW', 'UM') THEN '99'
        WHEN COUNT(geo.congressional_id) = 1 THEN LPAD(geo.congressional_id,2,0)
        ELSE '90'
        END
      ) AS PrimaryPlaceOfPerformanceCongressionalDistrict,
    /* Added 'congressional_district' field from the TrAMS_Location table to populate the  Legal Entity Congressional District field information  */  
	  tl.congressionalDistrict AS  LegalEntityCongressionalDistrict,
      tl.city AS city_text,
      tl.stateOrProvince AS state_text
    FROM trams_application ta
      JOIN trams_temp_CFDATable CFDA                      ON CFDA.application_id = ta.id
		AND CFDA.uuid = @uuid
      JOIN trams_state_application_review sa   ON ta.id = sa.applicationidint
      JOIN trams_contract_award tca            ON ta.contractAward_id = tca.id
      JOIN vwtramsGeneralBusinessInfo gb      ON gb.recipientId = ta.recipient_id
      JOIN vwtramsLocation tl                   ON tl.recipientId = gb.recipientId
        AND  tl.addressType = 'Physical Address'
      JOIN trams_application_award_detail aa   ON ta.id = aa.application_id
      JOIN trams_project pro                   ON pro.application_id = ta.id
      JOIN trams_project_geography geo         ON geo.trmsprject_prjectgegrphy_id = pro.id
        AND geo.congressional_id IS NOT NULL
      JOIN trams_temp_nonFedTotalAmount li                ON li.application_id  = ta.id
        AND li.CFDANumber = CFDA.cfda_number
		AND li.uuid = @uuid
    WHERE
      ta.fiscal_year <= v_fiscal_year
      AND ta.status IN ('Active (Executed)', 'Active Award / Inactive Amendment', 'Active / Budget Revision In-Progress', 'Active / Budget Revision Under Review', 'Obligated / Ready for Execution', 'Closed')
    GROUP BY
      CFDA_number,
      FAIN,
      AwardModificationAmendmentNumber;

	 DELETE FROM trams_temp_CFDATable WHERE uuid = @uuid;
	 DELETE FROM trams_temp_nonFedTotalAmount WHERE uuid = @uuid;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_repopulate_trams_contract_award_funding_acc_mpr_table`()
    NO SQL
begin

drop table if exists trams_contract_award_funding_acc_mpr;
create table trams_contract_award_funding_acc_mpr
  select 
    grantee_id AS grantee_id, 
    contract_award_id AS contract_award_id, 
    application_id AS application_id, 
    application_number AS application_number, 
    amendment_number AS amendment_number, 
    obligation_amount AS obligation_amount, 
    disbursement_amount AS disbursement_amount, 
    unliquid_amount AS unliquid_amount 
  from (
    select 
      uuid_short() AS a_id,
      cacct.recipient_id AS grantee_id,
      cacct.contract_award_id AS contract_award_id,
      cacct.application_id AS application_id,
      cacct.application_number AS application_number,
      a.amendment_number AS amendment_number,
      cacct.project_id AS project_id,
      cacct.project_number AS project_number,
      cacct.scope_code AS scope_number,
      cacct.scope_name AS scope_name,
      cacct.uza_code AS uza_code,
      cacct.cost_center_code AS cost_center_code,
      cacct.account_class_code AS account_class_code,
      cacct.funding_fiscal_year AS funding_fiscal_year,
      cacct.appropriation_code AS appropriation_code,
      cacct.section_code AS section_code,
      cacct.limitation_code AS limitation_code,
      cacct.type_authority AS authorization_type,
      cacct.fpc AS fpc,
      cacct.funding_source_name AS funding_source_name,
      sum(cacct.obligation_amount) AS obligation_amount,
      sum(cacct.deobligation_amount) AS deobligation_amount,
      sum(cacct.disbursement_amount) AS disbursement_amount,
      sum(cacct.refund_amount) AS refund_amount,
      (((ifnull(sum(cacct.obligation_amount),0) - 
      ifnull(sum(cacct.deobligation_amount),0)) - 
      ifnull(sum(cacct.disbursement_amount),0)) + 
      ifnull(sum(cacct.refund_amount),0)) AS unliquid_amount 
    from trams_contract_acc_transaction cacct
    join trams_application a 
    on cacct.application_id = a.id 
    where a.fiscal_year > YEAR(DATE_SUB(NOW() , INTERVAL 10 YEAR))  
    group by 
      cacct.recipient_id,
      cacct.contract_award_id,
      cacct.application_id,
      cacct.application_number,
      cacct.project_id,
      cacct.project_number,
      cacct.scope_code,
      cacct.scope_name,
      cacct.uza_code,
      cacct.cost_center_code,
      cacct.account_class_code,
      cacct.funding_fiscal_year,
      cacct.appropriation_code,
      cacct.section_code,
      cacct.limitation_code,
      cacct.type_authority,
      cacct.fpc,
      cacct.funding_source_name
  ) as contract_award_funding_sums;
    
alter table trams_contract_award_funding_acc_mpr modify grantee_id varchar(510) collate 'utf8_general_ci';
alter table trams_contract_award_funding_acc_mpr modify application_id varchar(510) collate 'utf8_general_ci';
alter table trams_contract_award_funding_acc_mpr modify contract_award_id varchar(510) collate 'utf8_general_ci';

alter table trams_contract_award_funding_acc_mpr add index (grantee_id);
alter table trams_contract_award_funding_acc_mpr add index (application_id);
alter table trams_contract_award_funding_acc_mpr add index (contract_award_id);

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_search_application`(IN `p_recipientId` VARCHAR(2048), IN `p_recipientName` VARCHAR(50), IN `p_applicationName` VARCHAR(255), IN `p_fiscalYear` VARCHAR(4), IN `p_applicationNumber` VARCHAR(50), IN `p_applicationState` VARCHAR(2), IN `p_sectionCode` VARCHAR(50), IN `p_applicationStatus` VARCHAR(100), IN `p_returnLimit` VARCHAR(20))
    NO SQL
begin
  
  /*
     Stored Procedure Name : trams_search_application
     Updated by  : Hunter Wang (2020.Aug.25) - Updated trams_general_business_info to vwtramsGeneralBusinessInfo
    */
  
  declare _sql varchar(40960) default "";
  declare _whereClause varchar(4096) default " ";
  declare _limitClause varchar(100) default " ";
  
  call trams_get_sql_limit_clause(p_returnLimit,_limitClause);

    set @sid = (select DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

  CALL `trams_log`(@sid, 'trams_search_application', 'In trams_search_application', 1); 
      
  /* Query application / project / scope_acc information and ACC if there are ACC related parameters
  */
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.recipient_id IN (",NULLIF(p_recipientId,''),")"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and match(g.legalBusinessName)against('",NULLIF(p_recipientName,''),"' in natural language mode)"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and match(a.application_name)against('",NULLIF(p_applicationName,''),"' in natural language mode)"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fiscal_year = '",NULLIF(p_fiscalYear,''),"'"),"")));
    set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.application_number like '",NULLIF(p_applicationNumber,''),"%'"),"")));  
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.fain_state = '",NULLIF(p_applicationState,''),"'"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and l.section_code = '",NULLIF(p_sectionCode,''),"'"),"")));
  set _whereClause = concat(_whereClause, (select ifnull(concat(" and a.status = '",NULLIF(p_applicationStatus,''),"'"),"")));
  
  set @selectAppQuery = '
      select `id`, `application_number` as applicationNumber_text,  `application_name` as applicationName_text, 
       `status` as status_text, `recipient_id` as granteeID_text, `recipient_name` as recipientName_text, 
       `fiscal_year` as fiscalYear_text, `created_by` as initiator_text, `created_date_time` as dateCreated_datetime, 
       `modified_date_time` as dateModified_datetime, `modified_by` as modifiedBy_text from trams_application where id in (
    SELECT a.id
    FROM trams_application a 
      inner join `vwtramsGeneralBusinessInfo` `g` on((`g`.`recipientId` = `a`.`recipient_id`))
      left JOIN `trams_line_item` l on a.id = l.app_id '; 
        
  
    set @selectAppQuery = concat( @selectAppQuery, ' WHERE 1 ', _whereClause, " ) ", _limitClause);

    
  CALL `trams_log`(@sid, 'trams_search_application', concat('Final Query: ',@selectAppQuery), 0); 

  PREPARE appStmt FROM @selectAppQuery;
    EXECUTE appStmt;

  
end$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetProjectScopeReservationSummary`(
  IN ApplicationId int(11)
)
BEGIN

    SET @ApplicationId = ApplicationId;
    SET @FromTrams = NULL;
    SET @ApplicationCreatedDate = NULL;

    /* Initialize the local variables from the application table */
    SET @SqlString = CONCAT('
        SELECT
            NOT app_from_team_flag
        INTO
            @FromTrams
        FROM
            trams_application
        WHERE
            id = ?;
    ');

    PREPARE Statement FROM @SqlString;

    EXECUTE Statement USING @ApplicationId;

    DEALLOCATE PREPARE Statement;

    /*
     * Set the created date of the base application on the award. Used to
     * determine the funding source reference version.
     */
    SET @ApplicationCreatedDate = fntramsGetBaseAppCreationDateByContractAwardIdOrAppId(NULL, ApplicationId);

    /* 
     * Subquery the project table to filter the projects for the specified 
     * Application ID.
     */
    SET @ProjectTable = CONCAT('
        (
            SELECT 
                id AS ProjectId,
                project_number AS ProjectNumber,
                project_number_sequence_number AS ProjectNumberSequenceNumber,
                project_title AS ProjectName
            FROM
                trams_project
            WHERE
                application_id = ?
        ) AS ProjectTable
    ');

    /*
     * Subquery the line item table and filter by the specified Application ID.
     * Reject any line items that are non-add scopes (i.e. other budget is false).
     * For TrAMS applications, roll up the unique project, scope, funding source 
     * combinations. TEAM applications will only have one project and roll up to
     * exactly one entry. Calculate the amount to reserve as the sum of the line
     * item change amounts. Filter rows that require a reservation (i.e. where
     * FTA Amount is positive).
     */
    SET @LineItemTable = CONCAT('
        (
            SELECT
                LineItem.project_id AS ProjectId,
                LineItem.scope_code AS ScopeCode,
                Scope.scope_suffix AS ScopeSuffix,
                Scope.scope_name AS ScopeName,
                LineItem.section_code AS SectionCode,
                LineItem.funding_source_name AS FundingSourceName,
                FundingSourceReference.funding_source_description AS FundingSourceDescription,
                COALESCE(SUM(LineItem.revised_FTA_amount), 0) - COALESCE(SUM(LineItem.latest_amend_fta_amount), 0) as FtaAmount
            FROM
                trams_line_item LineItem
            LEFT JOIN
                trams_scope Scope
                    ON Scope.id = LineItem.scope_id
            LEFT JOIN
                trams_funding_source_reference FundingSourceReference
                    ON FundingSourceReference.funding_source_name = LineItem.funding_source_name
                    AND @ApplicationCreatedDate >= FundingSourceReference.LatestFlagStartDate
                    AND @ApplicationCreatedDate < FundingSourceReference.LatestFlagEndDate
            WHERE
                LineItem.app_id = ?
                AND LineItem.other_budget = 0
                AND LineItem.is_line_item = 1 ',
            if(@FromTrams,
                'GROUP BY
                    LineItem.project_id,
                    LineItem.scope_code,
                    LineItem.funding_source_name ',
                'GROUP BY
                    LineItem.project_id '
            ),
            'HAVING
                COALESCE(SUM(LineItem.revised_FTA_amount), 0) - COALESCE(SUM(LineItem.latest_amend_fta_amount), 0) > 0
        ) AS LineItemTable
    ');

    /*
     * Subquery the scope ACC table and filter by the specified Application ID.
     * For TrAMS applications, roll up the unique project, scope, funding
     * source combinations. TEAM applications will only have one project and
     * roll up to exactly one entry. Calculate the total amount already reserved
     * as the sum of the reservation amounts.
     */
    SET @ScopeAccTable = CONCAT('
        (
            SELECT
                project_id AS ProjectId,
                scope_number AS ScopeCode,
                scope_suffix AS ScopeSuffix,
                funding_source_name AS FundingSourceName,
                COALESCE(SUM(acc_reservation_amount), 0) AS TotalReservationAmount
            FROM
                trams_scope_acc
            WHERE
                application_id = ? ',
            if(@FromTrams,
                'GROUP BY
                    project_id,
                    scope_number,
                    funding_source_name ',
                'GROUP BY
                    project_id '
            ),
        ') AS ScopeAccTable
    ');

    /*
     * Subquery the account class code reservation table and filter by the
     * specified Application ID. For TrAMS applications, roll up the unique
     * project, scope, funding source combinations. TEAM applications will
     * only have one project and roll up to exactly one entry. Calculate the
     * amount already reserved as the sum of the reservation amounts.
     */
    SET @AccountClassCodeReservationTable = CONCAT('
        (
            SELECT
                ProjectId AS ProjectId,
                ScopeCode AS ScopeCode,
                ScopeSuffix AS ScopeSuffix,
                FundingSourceName AS FundingSourceName,
                COALESCE(SUM(ReservationAmount), 0) AS AccReservationAmount
            FROM
                tramsAccountClassCodeReservation
            WHERE
                ApplicationId = ? ',
            if(@FromTrams,
                'GROUP BY
                    ProjectId,
                    ScopeCode,
                    FundingSourceName ',
                'GROUP BY
                    ProjectId '
            ),
        ') AS AccountClassCodeReservationTable
    ');

    /*
     * Subquery the application holding pot reservation table and filter by the
     * specified application ID. Exclude any holding pots that are inactive or
     * are associated with budget revisions (type ID = 5). Roll up the unique
     * project, scope, funding source combinations. TEAM applications will only
     * have one project and roll up to exactly one entry. Calculate the amount
     * reserved from holding pots as the sum of the reservation amounts.
     */
    SET @HoldingPotReservationTable = CONCAT('
        (
            SELECT
                HoldingPotReservation.ProjectId AS ProjectId,
                HoldingPotReservation.ScopeCode AS ScopeCode,
                HoldingPotReservation.FundingSourceName AS FundingSourceName,
                COALESCE(SUM(HoldingPotReservation.ReservationAmount), 0) AS HoldingPotReservationAmount
            FROM
                tramsApplicationHoldingPot HoldingPot
            LEFT JOIN
                tramsApplicationHoldingPotReservation HoldingPotReservation
                    ON HoldingPotReservation.ApplicationHoldingPotId = HoldingPot.ApplicationHoldingPotId
            WHERE
                HoldingPot.ApplicationId = ?
                AND HoldingPot.ActiveFlag = 1
                AND HoldingPot.TypeId <> 5 ',
            if(@FromTrams,
                'GROUP BY
                    HoldingPotReservation.ProjectId,
                    HoldingPotReservation.ScopeCode,
                    HoldingPotReservation.FundingSourceName ',
                'GROUP BY
                    HoldingPotReservation.ProjectId '
            ),
        ') AS HoldingPotReservationTable
    ');

    /*
     * Prepare and execute the final output query, passing in the relevant parameters.
     * Calculate the total reservation amount as ACC reservation amount + holding pot
     * reservation amount.
     * Calculate the unreserved amount as FTA amount - total reservation amount. 
     */
    SET @SqlString = CONCAT('
        SELECT
            ACS_FN_GenerateRowNumber() AS ProjectScopeReservationId,
            ProjectTable.ProjectId AS ProjectId,
            ProjectTable.ProjectNumber AS ProjectNumber,
            ProjectTable.ProjectNumberSequenceNumber AS ProjectNumberSequenceNumber,
            ProjectTable.ProjectName AS ProjectName,
            LineItemTable.ScopeCode AS ScopeCode,
            LineItemTable.ScopeSuffix AS ScopeSuffix,
            LineItemTable.ScopeName AS ScopeName,
            LineItemTable.SectionCode AS SectionCode,
            LineItemTable.FundingSourceName AS FundingSourceName,
            LineItemTable.FundingSourceDescription AS FundingSourceDescription,
            COALESCE(LineItemTable.FtaAmount, 0) AS FtaAmount,
            COALESCE(AccountClassCodeReservationTable.AccReservationAmount, 0) AS AccReservationAmount,
            COALESCE(HoldingPotReservationTable.HoldingPotReservationAmount, 0) AS HoldingPotReservationAmount,
            COALESCE(ScopeAccTable.TotalReservationAmount, 0) AS TotalReservationAmount,
            (
                COALESCE(LineItemTable.FtaAmount, 0) -
                COALESCE(ScopeAccTable.TotalReservationAmount, 0)
            ) AS UnreservedAmount
        FROM',
            @ProjectTable,
        'JOIN',
            @LineItemTable,
                'ON LineItemTable.ProjectId = ProjectTable.ProjectId
        LEFT JOIN',
            @AccountClassCodeReservationTable,
                if(@FromTrams,
                    'ON AccountClassCodeReservationTable.ProjectId = LineItemTable.ProjectId
                    AND AccountClassCodeReservationTable.ScopeCode = LineItemTable.ScopeCode
                    AND AccountClassCodeReservationTable.FundingSourceName = LineItemTable.FundingSourceName ',
                    'ON AccountClassCodeReservationTable.ProjectId = LineItemTable.ProjectId '
                ),
        'LEFT JOIN',
            @HoldingPotReservationTable,
                if(@FromTrams,
                    'ON HoldingPotReservationTable.ProjectId = LineItemTable.ProjectId
                    AND HoldingPotReservationTable.ScopeCode = LineItemTable.ScopeCode
                    AND HoldingPotReservationTable.FundingSourceName = LineItemTable.FundingSourceName ',
                    'ON HoldingPotReservationTable.ProjectId = LineItemTable.ProjectId '
                ),
        'LEFT JOIN',
            @ScopeAccTable,
                if(@FromTrams,
                    'ON ScopeAccTable.ProjectId = LineItemTable.ProjectId
                    AND ScopeAccTable.ScopeCode = LineItemTable.ScopeCode
                    AND ScopeAccTable.FundingSourceName = LineItemTable.FundingSourceName;',
                    'ON ScopeAccTable.ProjectId = LineItemTable.ProjectId;'
                )
    );

    PREPARE Statement FROM @SqlString;

    EXECUTE Statement USING
        @ApplicationId, /* Project table subquery where clause */
        @ApplicationId, /* LineItem table subquery where clause */
        @ApplicationId, /* ScopeAcc table subquery where clause */
        @ApplicationId, /* AccountClassCodeReservation table subquery where clause */
        @ApplicationId; /* HoldingPotReservation table subquery where clause */

    DEALLOCATE PREPARE Statement;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetQuarterlyDisbursementReport`(
	IN recipientId VARCHAR(2048),
	IN fain VARCHAR(150),
	IN projectNumber VARCHAR(100),
	IN accountClassCode VARCHAR(80),
	IN fpc VARCHAR(40), 
    IN transactionType VARCHAR(30), 
    IN transactionBeginDate VARCHAR(40), 
    IN transactionDateCompare VARCHAR(2),
    IN transactionEndDate VARCHAR(40), 
    IN awardStatus VARCHAR(40), 
    IN scopeCode VARCHAR(40), 
    IN scopeSuffix VARCHAR(10), 
    IN sectionCode VARCHAR(40),
    IN fundingCostCenter VARCHAR(512),
    IN fundingFiscalYear VARCHAR(512)
)
BEGIN
      
	  /*
     Stored Procedure Name : usptramsGetQuarterlyDisbursementReport
     Updated by  : Hunter Wang (2020.Aug.25)
    */
	  
      /* Declare Variables */
      DECLARE selectStatement VARCHAR(4096);
      DECLARE fromStatement VARCHAR(4096);
      DECLARE whereClause VARCHAR(4096);
      DECLARE limitClause VARCHAR(20);
      DECLARE orderBy VARCHAR(4096);
      
	/* 
		Applying any filters inputed on the form, include 10,000 as the number of rows that can be retrieved,
		and include any necessary conditions in the WHERE clause
	*/
	  SET limitClause = " LIMIT 10000;";
      /* Applying any filters inputed on the form */
      SET whereClause = " WHERE con.transaction_type NOT IN ('Obligation', 'Deobligation') and (con.disbursement_amount <> 0 OR con.refund_amount <> 0)";
			/* Award Status Filter */
			IF NULLIF(awardStatus, '') IS NULL THEN
				SET whereClause = CONCAT(whereClause, " AND app.status != 'Archived'");
				ELSE
				SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.status LIKE ('", NULLIF(awardStatus,''),"')"),"")));
			END IF;
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.recipient_id IN (", NULLIF(recipientId,''),")"),"")));
	  
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.application_number LIKE ('", NULLIF(fain,''),"%')"),"")));
	  
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.project_number LIKE ('", NULLIF(projectNumber,''),"%')"),"")));
	  
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.account_class_code LIKE ('", NULLIF(accountClassCode,''),"')"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.fpc LIKE ('", NULLIF(fpc,''),"')"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.transaction_type LIKE ('", NULLIF(transactionType,''),"')"),"")));
      
        /* Date Range */
	  IF NULLIF(transactionBeginDate, '') IS NOT NULL AND NULLIF(transactionEndDate, '') IS NOT NULL THEN
        SET whereClause = CONCAT(whereClause, " AND DATE(con.transaction_date_time) BETWEEN '", transactionBeginDate,"' AND '", transactionEndDate, "'");
        ELSE
        SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND DATE(con.transaction_date_time) ", transactionDateCompare, " '", NULLIF(transactionBeginDate,''), "'"),"")));
      END IF;
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.scope_code LIKE ('", NULLIF(scopeCode,''),"')"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.scope_suffix LIKE ('", NULLIF(scopeSuffix,''),"')"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.section_code IN (", NULLIF(sectionCode,''),")"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.cost_center_code IN (", NULLIF(fundingCostCenter,''),")"),"")));
      SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND con.funding_fiscal_year IN (", NULLIF(fundingFiscalYear,''),")"),"")));
      
      /* Preparing SQL Statement */
      SET selectStatement = 
      "SELECT
        con.project_number AS Project_Number,
        pro.project_title AS Project_Title,
        app.application_number AS FAIN,
        con.recipient_id AS Recipient_Id,
        gen.legalBusinessName AS Recipient_Name,
        app.status AS Award_Status,
        con.cost_center_code AS Cost_Center_Code,
        con.account_class_code AS Account_Class_Code,
        con.fpc AS FPC,
        con.fpc_description AS Financial_Purpose,
        con.scope_code As Scope_Code,
        con.scope_name AS Scope_Name,
        con.scope_suffix AS Scope_Suffix,
        con.section_code AS section_code,
        con.transaction_type AS Transaction_Type,
        DATE(con.transaction_date_time) AS Transaction_Date,
        (CASE
          WHEN con.transaction_type = 'Refund' THEN (CASE WHEN con.refund_amount > 0 THEN CONCAT('($', con.refund_amount, ')') ELSE CONCAT('$', REPLACE(con.refund_amount, '-','')) END)
          WHEN con.transaction_type = 'Disbursement' THEN (CASE WHEN con.disbursement_amount < 0 THEN CONCAT('($', REPLACE(con.disbursement_amount, '-', ''), ')') ELSE CONCAT('$', con.disbursement_amount) END)
          END
        ) AS Transaction_Amount ";
      
      SET fromStatement = 
      " FROM trams_contract_acc_transaction con
          JOIN trams_application app ON app.id  = con.application_id
          JOIN trams_project pro ON pro.id = con.project_id
          JOIN vwtramsGeneralBusinessInfo gen ON gen.recipientId = con.recipient_id";
	
	/*TOM-3630 - added orderBy clause*/
	  SET orderBy = 
      " ORDER BY con.transaction_date_time desc";
		  
	
      
      /* Executing SQL */
      SET @finalQuery = CONCAT(selectStatement, fromStatement, whereClause, orderBy, limitClause);
	  /*updated the final query to include the orderBy calsue*/
      PREPARE disbursementReport FROM @finalQuery;
      EXECUTE disbursementReport;
    
    END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetActivityLineItemBudgetReport`(IN granteeId VARCHAR(1024), IN costCenterCode VARCHAR(512), IN fiscalYear VARCHAR(512), IN appNumber VARCHAR(50), IN appStatus VARCHAR(1024),
IN appType VARCHAR(50), IN preAwardManager VARCHAR(50), IN postAwardManager VARCHAR(50), IN projectNumber VARCHAR(50), IN sectionCode VARCHAR(50), IN scopeCode VARCHAR(50), IN aliNumber VARCHAR(50),
IN aliName VARCHAR(512), IN aliCustomName VARCHAR(512), IN GMTOffset VARCHAR(10), IN returnLimit VARCHAR(20))
BEGIN

	/*
     Stored Procedure Name : usptramsGetActivityLineItemBudgetReport
     Description : Populate data for TrAMS nightly 'Generate Static Reports' process.
     Updated by  : Garrison Dean (2021.Jul.21)
	*/

	DECLARE sqlClause VARCHAR(5120) DEFAULT " ";
	DECLARE whereClause VARCHAR(4096) DEFAULT " ";
	DECLARE limitClause VARCHAR(100) DEFAULT " ";
	DECLARE groupByClause VARCHAR(100) DEFAULT " ";
	DECLARE orderByClause VARCHAR(100) DEFAULT " ";

	CALL trams_get_sql_limit_clause(returnLimit,limitClause);

	SET sqlClause = CONCAT("
			   SELECT
	app.recipient_id AS `Recipient ID`,
	gb.recipientAcronym AS `Recipient Acronym`,
	gb.legalBusinessName AS `Recipient Name`,
	app.recipient_cost_center AS `Recipient Cost Center`,
	app.fiscal_year AS `Application Fiscal Year`,
	app.application_number AS `Application Number`,
	app.amendment_number AS `Amendment Number`,
	app.application_name AS `Application Name`,
	app.`status` AS `Application Status`,
	app.application_type AS `Application Type`,
	CASE
		WHEN latest_transmission_history.original_transmitted_date IS NOT NULL AND latest_transmission_history.original_transmitted_date <> ' '
		THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_transmitted_date,'+00:00',",GMTOffset,") AS char))
		ELSE trams_format_date(MAX(fr.transmitted_date))
	END AS `Initial Application Transmitted Date`,
	trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_transmitted_date,'+00:00',",GMTOffset,") AS char)) AS `Latest Application Transmitted Date`,
	CASE
		WHEN latest_transmission_history.original_submitted_date IS NOT NULL AND latest_transmission_history.original_submitted_date <> ' '
		THEN trams_format_date(cast(CONVERT_TZ(latest_transmission_history.original_submitted_date,'+00:00',",GMTOffset,") AS char)) 
		ELSE trams_format_date(MAX(fr.submitted_date))
	END AS `Initial Application Submitted Date`,
	trams_format_date(cast(CONVERT_TZ(latest_transmission_history.latest_submitted_date,'+00:00',",GMTOffset,") AS char)) AS `Latest Application Submitted Date`,
	app.recipient_contact AS `Recipient Point of Contact`,
	app.fta_pre_award_manager AS `FTA Pre-Award Manager`,
	app.fta_post_award_manager AS `FTA Post-Award Manager`,
	p.project_number AS `Project Number`,
	p.project_title AS `Project Name`,
	l.funding_source_name AS `Funding Source Name`,
	l.section_code AS `Section Code`,
	l.scope_detailed_name AS `Scope Name`,
	l.scope_code AS `Scope Code`,
	l.custom_item_name AS `Custom Line Item Name`,
	l.line_item_type_name AS `Budget ALI Name`,
	l.line_item_number AS `Budget ALI Code`,
	l.quantity AS `Budget ALI Quantity`,
	l.fuel_name AS `Alternative Fuel Code`,
	IFNULL(l.revised_FTA_amount,0) - IFNULL(prevLine.revised_FTA_amount, 0) AS `Amendment FTA Total Amount`,
	IFNULL(l.revised_total_amount - l.revised_FTA_amount, 0) - IFNULL(prevLine.revised_total_amount - prevLine.revised_FTA_amount, 0) AS `Amendment Non-FTA Amount`,
	IFNULL(l.revised_total_amount,0) - IFNULL(prevLine.revised_total_amount,0) AS `Amendment Total Amount`,
	trams_format_date(cast(CONVERT_TZ(app.amended_date,'+00:00',",GMTOffset,") AS char)) AS `Amendment Date`,
	trams_format_date(cast(CONVERT_TZ(aa.obligated_date_time,'+00:00',",GMTOffset,") AS char)) AS `Obligation_Date`,
			CASE
				WHEN l.third_party_flag = 0
					THEN 'N'
				WHEN l.third_party_flag = 1
					THEN 'Y'
				ELSE ''
			END AS 'Third Party Contract (Y/N)'
	FROM
		trams_application app
			LEFT JOIN
		trams_application prevApp ON app.contractAward_id = prevApp.contractAward_id
			AND app.amendment_number - 1 = prevApp.amendment_number
			LEFT JOIN
		trams_line_item l ON l.app_id = app.id
			LEFT JOIN
		trams_line_item prevLine ON prevLine.original_line_item_id = l.original_line_item_id
			AND prevLine.app_id = prevApp.id
	JOIN vwtramsGeneralBusinessInfo gb ON app.recipient_id = gb.recipientId
	JOIN trams_project p ON p.id = l.project_id
	JOIN trams_application_award_detail aa ON app.id = aa.application_id
	LEFT JOIN trams_fta_review fr ON app.id = fr.application_id
	LEFT JOIN 
	(
		SELECT 
			a.id AS applicationId,
			MAX(th.TransmittedDate) AS latest_transmitted_date,
			MAX(th.SubmittedDate) AS latest_submitted_date,
			MIN(th.TransmittedDate) AS original_transmitted_date,
			MIN(th.SubmittedDate) AS original_submitted_date
		FROM
			tramsApplicationTransmissionHistory th
		JOIN
			trams_application a
		ON 
			a.id = th.applicationId
		GROUP BY th.applicationId
	) latest_transmission_history
	ON app.id = latest_transmission_history.applicationId
	WHERE l.line_item_number IS NOT NULL
	AND l.is_line_item = 1 ");

	-- Incude search criteria from user into AND clause of the statement
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND l.section_code IN ('",NULLIF(sectionCode,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND l.scope_code IN ('",NULLIF(scopeCode,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND l.line_item_number IN ('",NULLIF(aliNumber,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND l.line_item_type_name IN ('",NULLIF(aliName,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND l.custom_item_name LIKE ('",NULLIF(aliCustomName,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND p.project_number LIKE ('",NULLIF(projectNumber,''),"%')"),"")));

	-- Include search criteria from user into the WHERE clause of the final statement
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_id IN (",NULLIF(granteeId,''),")"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_cost_center IN (",NULLIF(costCenterCode,''),")"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.fiscal_year IN (",NULLIF(fiscalYear,''),")"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.application_number LIKE ('",NULLIF(appNumber,''),"%')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.status IN (",NULLIF(appStatus,''),")"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.application_type IN ('",NULLIF(appType,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.fta_pre_award_manager IN ('",NULLIF(preAwardManager,''),"')"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.fta_post_award_manager IN ('",NULLIF(postAwardManager,''),"')"),"")));

	SET groupByClause = CONCAT(" GROUP BY l.id ");

	SET orderByClause = CONCAT(" ORDER BY app.application_number, p.project_number, l.line_item_number  ");

	SET @applicationQuery = CONCAT(sqlClause,whereClause, groupByClause, orderByClause, limitClause);

	SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));

	-- the calls below creates logs of the inputs/query that were used in the above query

	CALL `trams_log`(@sid, 'usptramsGetActivityLineItemBudgetReport', concat(now(),' CALL SP Inputs: ', IFNULL(`granteeId`,'NULL'),',',IFNULL(  `costCenterCode` ,'NULL'),',',IFNULL(  `fiscalYear` ,'NULL'),',',IFNULL(  `appNumber` ,'NULL'),',',IFNULL(  `appStatus` ,'NULL'),',',IFNULL(  `appType` ,'NULL'),',',IFNULL(  `preAwardManager` ,'NULL'),',',IFNULL(  `postAwardManager` ,'NULL'),',',IFNULL(  `projectNumber` ,'NULL'),',',IFNULL(  `sectionCode` ,'NULL'),',',IFNULL(  `scopeCode` ,'NULL'),',',IFNULL(  `aliNumber` ,'NULL'),',',IFNULL(  `aliName` ,'NULL'),',',IFNULL(  `returnLimit` ,'NULL'),','), 1);

	CALL `trams_log`(@sid, 'usptramsGetActivityLineItemBudgetReport', CONCAT(now(), 'Get Application & Project Query: ',@applicationQuery), 1);

	PREPARE applicationStmt FROM @applicationQuery;
	EXECUTE applicationStmt;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetDeobligationProjectScopeFundingSummary`(
  IN `p_contract_award_id` int(11), 
  IN `p_is_closeout` tinyint(1)
)
BEGIN
  
  /*
   * usptramsGetDeobligationProjectScopeFundingSummary.sql
   * Date Created: 2019-04-01
   * Date Modified: 2020-04-27
   *
   * Input: p_contract_award_id (int) The primary ID of the contract award
   * Input: p_is_closeout (tinyint) True if the amendment is a closeout
   *
   * This procedure takes in a contract award ID and returns all data in the
   * Project Scope Obligation Summary for the deobligation process. This
   * procedure is only for TrAMS applications, There is a different procedure for
   * TEAM applications. If the deobligation is a closeout, logic must be removed
   * to pull correct data. There is an additional if statement which adds in SQL
   * where necessary. 
   *
   * Changelog
   * ---------
   * 2019-04-01, William Taylor: Initial version
   * 2020-04-27, Ivan Chen: TOM-8133 Updated unliquidated balance calculation to exclude UZA code
   */
  
  DECLARE _sql varchar(40960) DEFAULT "";
  DECLARE _deobScopeJoin varchar(1000) DEFAULT "";
  DECLARE _deobScopeCheck varchar(1000) DEFAULT "";
  DECLARE _mostRecentAppId int(11) DEFAULT 0;

  SELECT id INTO _mostRecentAppId FROM `trams_application` WHERE contractAward_id = p_contract_award_id ORDER BY id DESC LIMIT 1;

  /* IF not a closeout application */
  IF (NULLIF(p_is_closeout, '') IS NULL OR NULLIF(p_is_closeout, '') = 0) THEN
    SET _deobScopeCheck = "AND scp.latest_amend_fta_amount - scp.revised_fta_amount > 0";
    SET _deobScopeJoin = CONCAT("JOIN (
        SELECT
          tscp.po_number,
          proj.project_number_sequence_number,
          tscp.scope_code,
          tscp.scope_suffix,
          tscp.latest_amend_fta_amount,
          tscp.revised_fta_amount
        FROM `trams_application` app
          JOIN `trams_scope` tscp
            ON tscp.application_id = app.id
          JOIN `trams_project` proj
            ON tscp.project_id = proj.id
        WHERE app.id = ",_mostRecentAppId,"
      ) scp
        ON scp.po_number = trans.po_number
        AND scp.project_number_sequence_number = proj.project_number_sequence_number
        AND scp.scope_code = trans.scope_code
        AND scp.scope_suffix = trans.scope_suffix"
    );
  END IF;

  /* TOM-8133 Added a new join (aliased as unliquidated) on a subquery to retrieve the 
     unliquidated balance from the transactions while now excluding the uza code */
  SET _sql = CONCAT(
    "SELECT
      trans.id AS trams_contract_acc_funding_id,
      trans.po_number AS po_number,
      trans.project_number AS project_number,
      trans.scope_code AS scope_number,
      trans.scope_suffix AS scope_suffix,
      trans.scope_name AS scope_name,
      trans.cost_center_code AS cost_center_code,
      trans.uza_code AS uza_code,
      trans.account_class_code AS account_class_code,
      trans.fpc AS fpc,
      trans.application_id AS application_id,
      trans.project_id AS project_id,
      trans.contract_award_id AS contract_award_id,
      trans.funding_fiscal_year AS funding_fiscal_year,
      trans.section_code AS section_code,
      trans.limitation_code AS limitation_code,
      trans.type_authority AS authorization_type,
      trans.appropriation_code AS appropriation_code,
      proj.project_number_sequence_number AS project_number_sequence_number,
      trans.funding_source_name AS funding_source_name,
      trans.dafis_suffix_code AS dafis_suffix_code,
      sum(trans.obligation_amount) - sum(trans.deobligation_amount) AS net_obligation_amount,
      unliquidated.unliquidated_balance AS unliquidated_balance,
      dd.net_deobligated_recovery_amount AS deobligation_amount
    FROM trams_contract_acc_transaction trans
    LEFT JOIN trams_project proj ON trans.project_id = proj.id
    LEFT JOIN (
      /* Subquery to retrieve the unliquidated balance for each row */
      SELECT
        sub_trans.po_number AS po_number,
        sub_trans.scope_code AS scope_code,
        sub_trans.scope_suffix AS scope_suffix,
        sub_trans.cost_center_code AS cost_center_code,
        sub_trans.account_class_code AS account_class_code,
        sub_trans.fpc AS fpc,
        sub_trans.project_id AS project_id,
        sub_trans.contract_award_id AS contract_award_id,
        sub_proj.project_number_sequence_number AS project_number_sequence_number,
        sum(sub_trans.obligation_amount) - sum(sub_trans.deobligation_amount) - sum(sub_trans.disbursement_amount) + sum(sub_trans.refund_amount) AS unliquidated_balance
      FROM trams_contract_acc_transaction sub_trans
      LEFT JOIN trams_project sub_proj ON sub_trans.project_id = sub_proj.id
      WHERE sub_trans.contract_award_id = ", p_contract_award_id, "
      GROUP BY
        sub_trans.po_number,
        sub_proj.project_number_sequence_number,
        sub_trans.scope_code,
        sub_trans.scope_suffix,
        sub_trans.cost_center_code,
        sub_trans.account_class_code,
        sub_trans.fpc
    ) unliquidated
      ON
        trans.po_number = unliquidated.po_number
        AND proj.project_number_sequence_number = unliquidated.project_number_sequence_number
        AND trans.scope_code = unliquidated.scope_code
        AND trans.scope_suffix = unliquidated.scope_suffix
        AND trans.cost_center_code = unliquidated.cost_center_code
        AND trans.account_class_code = unliquidated.account_class_code
        AND trans.fpc = unliquidated.fpc
    LEFT JOIN (
      /* retrieves temporary deobligations that are created throughout the process */
      SELECT
        po_number,
        project_number_sequence_number,
        scope_number,
        scope_suffix,
        acc_uza_code,
        acc_cost_center_code,
        account_class_code,
        acc_fpc,
        sum(deobligated_recovery_amount) AS net_deobligated_recovery_amount
      FROM
        `trams_deobligation_detail`
      WHERE
        deobligated_still_pending <> 0 AND contract_award_id = ", p_contract_award_id, "
      GROUP BY
        po_number,
        project_number_sequence_number,
        scope_number,
        scope_suffix,
        acc_uza_code,
        acc_cost_center_code,
        acc_uza_code,
        account_class_code,
        acc_fpc
    ) dd
      ON
        trans.po_number = dd.po_number
        AND proj.project_number_sequence_number = dd.project_number_sequence_number
        AND trans.scope_code = dd.scope_number
        AND trans.scope_suffix = dd.scope_suffix
        AND trans.uza_code = dd.acc_uza_code
        AND trans.cost_center_code = dd.acc_cost_center_code
        AND trans.account_class_code = dd.account_class_code
        AND trans.fpc = dd.acc_fpc
    LEFT JOIN trams_application app ON app.id = trans.application_id
    ", _deobScopeJoin, "
    WHERE trans.contract_award_id = ", p_contract_award_id, " ", _deobScopeCheck, " 
      /* TOM-8133 Added an additional where condition to filter on obligations and deobligations only */
      AND trans.transaction_type IN ('Obligation', 'Deobligation')
    GROUP BY 
      trans.po_number,
      proj.project_number_sequence_number,
      trans.scope_code,
      trans.scope_suffix,
      trans.uza_code,
      trans.cost_center_code,
      trans.account_class_code,
      trans.fpc
    ORDER BY app.amendment_number, trans.account_class_code ASC;"
  );

    SET @deobQuery = _sql;

    SET @sid = (SELECT DATE_FORMAT(now(), '%Y%m%d%H%i%s%f'));
    CALL `trams_log`(@sid, 'usptramsGetDeobligationProjectScopeFundingSummary', concat('Deobligation Summary Query: ', @deobQuery), 1);

    PREPARE deobStmt FROM @deobQuery;
    EXECUTE deobStmt;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsTruncateFYAPResultSet`()
BEGIN

	IF EXISTS (
		/* checks whether the table already exists and truncates it */
		SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS 
		WHERE
			TABLE_SCHEMA = 'Appian' 
			AND TABLE_NAME = 'tramsFYAPResultSet' 
	) THEN
		TRUNCATE TABLE tramsFYAPResultSet;
	END IF;

	IF EXISTS (
		SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS 
		WHERE
			TABLE_SCHEMA = 'Appian' 
			AND TABLE_NAME = 'trams_temp_apportionment_fyfap_rollup' 
	) THEN
		TRUNCATE TABLE trams_temp_apportionment_fyfap_rollup;
	END IF;

	IF EXISTS (
		SELECT 1 FROM INFORMATION_SCHEMA.STATISTICS 
		WHERE
			TABLE_SCHEMA = 'Appian' 
			AND TABLE_NAME = 'trams_temp_apportionment_history_fyfap_rollup' 
	) THEN
		TRUNCATE TABLE trams_temp_apportionment_history_fyfap_rollup;
	END IF;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetAvailableOperatingBudgetAndApportionment`(
    IN CostCenterCode varchar(5),
    IN SectionCodes varchar(255),
    IN UzaCode varchar(6),
    IN UzaCodeReferenceVersion int(11)
)
BEGIN

    SET @CostCenterCode = CostCenterCode;
    SET @SectionCodes = SectionCodes;
    SET @UzaCode = UzaCode;
    SET @UzaCodeReferenceVersion = UzaCodeReferenceVersion;
    SET @CurrentFiscalYear = trams_get_current_fiscal_year(NOW());

    /*
     * Subquery the account class code table and join on the account class code
     * history table. Filter by the specified fiscal year, cost center code,
     * and section codes. Exclude any account class codes of type Recovery to
     * avoid double counting (these transactions will still be included from
     * the account class code history table). Group by the unique account class
     * code combinations. Calculate the available operating budget amount as
     * the initial authority + transfer in - transfer out - reservations +
     * unreservations - obligations + recoveries.
     */
     SET @AccountClassCodeTable = CONCAT('
        (
            SELECT
                AccountClassCode.fiscal_year AS FiscalYear,
                AccountClassCode.cost_center_code AS CostCenterCode,
                AccountClassCodeLookup.account_class_code AS AccountClassCode,
                AccountClassCodeLookup.acc_description AS AccDescription,
                AccountClassCodeLookup.funding_fiscal_year AS FundingFiscalYear,
                AccountClassCodeLookup.appropriation_code AS AppropriationCode,
                AccountClassCodeLookup.section_code AS SectionCode,
                AccountClassCodeLookup.limitation_code AS LimitationCode,
                AccountClassCodeLookup.authority_type AS AuthorityType,
                (
                    COALESCE(AccountClassCode.code_new_amount, 0) +
                    COALESCE(SUM(AccountClassCodeHistory.transfer_in_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.transfer_out_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.reserved_amount), 0) +
                    COALESCE(SUM(AccountClassCodeHistory.unreserve_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.obligated_amount), 0) +
                    COALESCE(SUM(AccountClassCodeHistory.recovery_amount), 0)
                ) AS OperatingBudgetAvailableBalance
            FROM
                trams_account_class_code AccountClassCode
            JOIN
                trams_account_class_code_lookup AccountClassCodeLookup
                    ON AccountClassCodeLookup.account_class_code = AccountClassCode.account_class_code
            LEFT JOIN
                trams_account_class_code_history AccountClassCodeHistory
                    ON AccountClassCodeHistory.transaction_fiscal_year = AccountClassCode.fiscal_year
                    AND AccountClassCodeHistory.cost_center_code = AccountClassCode.cost_center_code
                    AND AccountClassCodeHistory.account_class_code = AccountClassCode.account_class_code
            WHERE
                AccountClassCode.fiscal_year = @CurrentFiscalYear
                AND AccountClassCode.cost_center_code = ?
                AND FIND_IN_SET(AccountClassCode.acc_section_code, ?)
                AND AccountClassCode.authorized_flag = 1
                AND AccountClassCode.code_transaction_type <> \'Recovery\'
                AND AccountClassCodeLookup.lapse_year > @CurrentFiscalYear
            GROUP BY
                AccountClassCode.account_class_code
        ) AS AccountClassCodeTable
    ');

     /*
      * Subquery the apportionment table and join on the apportionment history
      * table. Filter by the specified UZA code and section codes. Exclude any
      * apportionments that are lapsed (i.e. the lapse year is equal to or 
      * before the current fiscal year). Group by the unique apportionment
      * fields. Calculate the available apportionment amount as the initial
      * authority + modifications + transfer in - transfer out - reservations +
      * unreservations - obligations + recoveries.
      */
     SET @ApportionmentTable = CONCAT('
        (
            SELECT
                Apportionment.uza_code AS UzaCode,
                Apportionment.funding_fiscal_year AS FundingFiscalYear,
                Apportionment.appropriation_code AS AppropriationCode,
                Apportionment.section_id AS SectionCode,
                Apportionment.limitation_code AS LimitationCode,
                (
                    COALESCE(Apportionment.apportionment, 0) +
                    COALESCE(SUM(ApportionmentHistory.modified_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.transfer_in_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.transfer_out_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.reserved_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.unreserve_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.obligated_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.recovery_amount), 0)
                ) AS ApportionmentAvailableBalance
            FROM
                trams_apportionment Apportionment
            LEFT JOIN
                trams_apportionment_history ApportionmentHistory
                    ON ApportionmentHistory.uza_code = Apportionment.uza_code
                    AND ApportionmentHistory.funding_fiscal_year = Apportionment.funding_fiscal_year
                    AND ApportionmentHistory.appropriation_code = Apportionment.appropriation_code
                    AND ApportionmentHistory.section_id = Apportionment.section_id
                    AND ApportionmentHistory.limitation_code = Apportionment.limitation_code
            WHERE
                Apportionment.uza_code = ?
                AND FIND_IN_SET(Apportionment.section_id, ?)
                AND Apportionment.lapse_period > @CurrentFiscalYear
                AND Apportionment.active_flag = 1
            GROUP BY
                Apportionment.uza_code,
                Apportionment.funding_fiscal_year,
                Apportionment.appropriation_code,
                Apportionment.section_id,
                Apportionment.limitation_code
        ) AS ApportionmentTable
    ');

     /*
      * Subquery the UZA code reference table and filter by the specified
      * UZA code and version number. Limit to 1 result.
      */
    SET @UzaCodeReferenceTable = CONCAT('
        (
            SELECT
                uza_code AS UzaCode,
                uza_name AS UzaName
            FROM
                trams_uza_code_reference
            WHERE
                uza_code = ?
                AND version = ?
            LIMIT 1
        ) AS UzaCodeReferenceTable
    ');

    /*
     * Prepare and execute the final output query, passing in the relevant
     * parameters. Join on the acc_fpc_reference table to determine the
     * available FPCs for each account class code.
     */
    SET @SqlString = CONCAT('
        SELECT
            ACS_FN_GenerateRowNumber() AS AvailableAccountClassCodeAndApportionmentId,
            AccountClassCodeTable.FiscalYear AS FiscalYear,
            AccountClassCodeTable.AccountClassCode AS AccountClassCode,
            AccountClassCodeTable.AccDescription AS AccDescription,
            AccountClassCodeTable.FundingFiscalYear AS FundingFiscalYear,
            AccountClassCodeTable.AppropriationCode AS AppropriationCode,
            AccountClassCodeTable.SectionCode AS SectionCode,
            AccountClassCodeTable.LimitationCode AS LimitationCode,
            AccountClassCodeTable.AuthorityType AS AuthorityType,
            AccountClassCodeTable.CostCenterCode AS CostCenterCode,
            AccFpcReference.fpc AS Fpc,
            UzaCodeReferenceTable.UzaCode AS UzaCode,
            UzaCodeReferenceTable.UzaName AS UzaName,
            AccountClassCodeTable.OperatingBudgetAvailableBalance OperatingBudgetAvailableBalance,
            ApportionmentTable.ApportionmentAvailableBalance AS ApportionmentAvailableBalance
        FROM',
            @AccountClassCodeTable,
        'JOIN
            trams_acc_fpc_reference AccFpcReference
                ON AccFpcReference.account_class_code = AccountClassCodeTable.AccountClassCode
        JOIN',
            @ApportionmentTable,
                'ON ApportionmentTable.FundingFiscalYear = AccountClassCodeTable.FundingFiscalYear
                AND ApportionmentTable.AppropriationCode = AccountClassCodeTable.AppropriationCode
                AND ApportionmentTable.SectionCode = AccountClassCodeTable.SectionCode
                AND ApportionmentTable.LimitationCode = AccountClassCodeTable.LimitationCode
        JOIN',
            @UzaCodeReferenceTable,
                'ON UzaCodeReferenceTable.UzaCode = ApportionmentTable.UzaCode;
    ');

    PREPARE Statement FROM @SqlString;

    EXECUTE Statement USING
        @CostCenterCode, /* AccountClassCode table subquery where clause */
        @SectionCodes, /* AccountClassCode table subquery where clause */
        @UzaCode, /* Apportionment table subquery where clause */
        @SectionCodes, /* Apportionment table subquery where clause */
        @UzaCode, /* UzaCodeReference table subquery where clause */
        @UzaCodeReferenceVersion; /* UzaCodeReference table subquery where clause */

    DEALLOCATE PREPARE Statement;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_undo_approved_budget_revision`(
  IN p_budget_revision_id int(11)
)
BEGIN

  DECLARE _application_id int(11);
  DECLARE _application_number varchar(255);
  DECLARE _current_revision_number int(11);
  DECLARE _previous_revision_number int(11);

  SELECT `application_id`,
         `revision_number`,
         (`revision_number` - 1)
  INTO _application_id,
       _current_revision_number,
       _previous_revision_number
  FROM `trams_budget_revision`
  WHERE `id` = p_budget_revision_id;

  SELECT `application_number`
  INTO _application_number
  FROM `trams_application`
  WHERE `id` = _application_id;

  /*
   * Gather the changelogs from the previous budget revision
   */
  
  DROP TEMPORARY TABLE IF EXISTS `trams_tmp_line_item_change_log_previous_revision`;
  CREATE TEMPORARY TABLE `trams_tmp_line_item_change_log_previous_revision`
  SELECT * FROM `trams_line_item_change_log`
  WHERE `application_id` = _application_id
    AND `revision_number` = _previous_revision_number;

  /*
   * Revert the existing line items to their values from the previous revision
   */

  UPDATE `trams_line_item` l
  INNER JOIN `trams_tmp_line_item_change_log_previous_revision` c ON c.`line_item_id` = l.`id`
  SET l.`extended_budget_description` = c.`original_line_item_description`,
      l.`revised_line_item_description` = c.`revised_line_item_description`,
      l.`quantity` = c.`original_quantity`,
      l.`latest_amend_quantity` = c.`latest_amend_quantity`,
      l.`latest_revised_quantity` = c.`revised_quantity`,
      l.`total_amount` = c.`original_total_eligible_amount`,
      l.`latest_amend_total_amount` = c.`latest_amend_total_eligible_amount`,
      l.`revised_total_amount` = c.`revised_total_eligible_amount`,
      l.`original_FTA_amount` = c.`original_FTA_amount`,
      l.`latest_amend_fta_amount` = c.`latest_amend_fta_amount`,
      l.`revised_FTA_amount` = c.`revised_FTA_amount`,
      l.`original_state_amount` = c.`original_state_amount`,
      l.`latest_amend_state_amount` = c.`latest_amend_state_amount`,
      l.`revised_state_amount` = c.`revised_state_amount`,
      l.`original_local_amount` = c.`original_local_amount`,
      l.`latest_amend_local_amount` = c.`latest_amend_local_amount`,
      l.`revised_local_amount` = c.`revised_local_amount`,
      l.`original_other_fed_amount` = c.`original_other_fed_amount`,
      l.`latest_amend_other_fed_amount` = c.`latest_amend_other_fed_amount`,
      l.`revised_other_fed_amount` = c.`revised_other_fed_amount`,
      l.`original_state_in_kind_amount` = c.`original_state_in_kind_amount`,
      l.`latest_amend_state_inkind_amount` = c.`latest_amend_state_inkind_amount`,
      l.`revised_state_in_kind_amount` = c.`revised_state_in_kind_amount`,
      l.`original_local_in_kind_amount` = c.`original_local_in_kind_amount`,
      l.`latest_amend_local_inkind_amount` = c.`latest_amend_local_inkind_amount`,
      l.`revised_local_in_kind_amount` = c.`revised_local_in_kind_amount`,
      l.`original_toll_revenue_amount` = c.`original_toll_revenue_amount`,
      l.`latest_amend_toll_rev_amount` = c.`latest_amend_toll_rev_amount`,
      l.`revised_toll_revenue_amount` = c.`revised_toll_revenue_amount`,
      l.`original_adjustment_amount` = c.`original_adjustment_amount`,
      l.`latest_amend_adjustment_amount` = c.`latest_amend_adjustment_amount`,
      l.`revised_adjustment_amount` = c.`revised_adjustment_amount`;

  /*
   * Revert any newly created line items to their original null values
   */

  UPDATE `trams_line_item` l
  SET l.`extended_budget_description` = NULL,
      l.`revised_line_item_description` = NULL,
      l.`quantity` = NULL,
      l.`latest_amend_quantity` = NULL,
      l.`latest_revised_quantity` = NULL,
      l.`total_amount` = NULL,
      l.`latest_amend_total_amount` = NULL,
      l.`revised_total_amount` = NULL,
      l.`original_FTA_amount` = NULL,
      l.`latest_amend_fta_amount` = NULL,
      l.`revised_FTA_amount` = NULL,
      l.`original_state_amount` = NULL,
      l.`latest_amend_state_amount` = NULL,
      l.`revised_state_amount` = NULL,
      l.`original_local_amount` = NULL,
      l.`latest_amend_local_amount` = NULL,
      l.`revised_local_amount` = NULL,
      l.`original_other_fed_amount` = NULL,
      l.`latest_amend_other_fed_amount` = NULL,
      l.`revised_other_fed_amount` = NULL,
      l.`original_state_in_kind_amount` = NULL,
      l.`latest_amend_state_inkind_amount` = NULL,
      l.`revised_state_in_kind_amount` = NULL,
      l.`original_local_in_kind_amount` = NULL,
      l.`latest_amend_local_inkind_amount` = NULL,
      l.`revised_local_in_kind_amount` = NULL,
      l.`original_toll_revenue_amount` = NULL,
      l.`latest_amend_toll_rev_amount` = NULL,
      l.`revised_toll_revenue_amount` = NULL,
      l.`original_adjustment_amount` = NULL,
      l.`latest_amend_adjustment_amount` = NULL,
      l.`revised_adjustment_amount` = NULL
  WHERE `app_id` = _application_id
    AND `revision_number` = _current_revision_number;
  
  /*
   * Group the newly updated line items by scope suffix
   */
  
  DROP TEMPORARY TABLE IF EXISTS `trams_tmp_line_item_rollup_previous_revision`;
  CREATE TEMPORARY TABLE `trams_tmp_line_item_rollup_previous_revision`
  SELECT `scope_id`,
         sum(ifnull(`quantity`, 0)) as `quantity`,
         sum(ifnull(`latest_amend_quantity`, 0)) as `latest_amend_quantity`,
         sum(ifnull(`latest_revised_quantity`, 0)) as `latest_revised_quantity`,
         sum(ifnull(`total_amount`, 0)) as `total_amount`,
         sum(ifnull(`latest_amend_total_amount`, 0)) as `latest_amend_total_amount`,
         sum(ifnull(`revised_total_amount`, 0)) as `revised_total_amount`,
         sum(ifnull(`original_FTA_amount`, 0)) as `original_FTA_amount`,
         sum(ifnull(`latest_amend_fta_amount`, 0)) as `latest_amend_fta_amount`,
         sum(ifnull(`revised_FTA_amount`, 0)) as `revised_FTA_amount`,
         sum(ifnull(`original_state_amount`, 0)) as `original_state_amount`,
         sum(ifnull(`latest_amend_state_amount`, 0)) as `latest_amend_state_amount`,
         sum(ifnull(`revised_state_amount`, 0)) as `revised_state_amount`,
         sum(ifnull(`original_local_amount`, 0)) as `original_local_amount`,
         sum(ifnull(`latest_amend_local_amount`, 0)) as `latest_amend_local_amount`,
         sum(ifnull(`revised_local_amount`, 0)) as `revised_local_amount`,
         sum(ifnull(`original_other_fed_amount`, 0)) as `original_other_fed_amount`,
         sum(ifnull(`latest_amend_other_fed_amount`, 0)) as `latest_amend_other_fed_amount`,
         sum(ifnull(`revised_other_fed_amount`, 0)) as `revised_other_fed_amount`,
         sum(ifnull(`original_state_in_kind_amount`, 0)) as `original_state_in_kind_amount`,
         sum(ifnull(`latest_amend_state_inkind_amount`, 0)) as `latest_amend_state_inkind_amount`,
         sum(ifnull(`revised_state_in_kind_amount`, 0)) as `revised_state_in_kind_amount`,
         sum(ifnull(`original_local_in_kind_amount`, 0)) as `original_local_in_kind_amount`,
         sum(ifnull(`latest_amend_local_inkind_amount`, 0)) as `latest_amend_local_inkind_amount`,
         sum(ifnull(`revised_local_in_kind_amount`, 0)) as `revised_local_in_kind_amount`,
         sum(ifnull(`original_toll_revenue_amount`, 0)) as `original_toll_revenue_amount`,
         sum(ifnull(`latest_amend_toll_rev_amount`, 0)) as `latest_amend_toll_rev_amount`,
         sum(ifnull(`revised_toll_revenue_amount`, 0)) as `revised_toll_revenue_amount`,
         sum(ifnull(`original_adjustment_amount`, 0)) as `original_adjustment_amount`,
         sum(ifnull(`latest_amend_adjustment_amount`, 0)) as `latest_amend_adjustment_amount`,
         sum(ifnull(`revised_adjustment_amount`, 0)) as `revised_adjustment_amount`
  FROM `trams_line_item`
  WHERE `app_id` = _application_id
  GROUP BY `scope_id`;

  /*
   * Update the scope records based on the newly updated line items
   */

  UPDATE `trams_scope` s
  INNER JOIN `trams_tmp_line_item_rollup_previous_revision` l ON l.`scope_id` = s.`id`
  SET s.`original_quantity` = l.`quantity`,
      s.`latest_amend_quantity` = l.`latest_amend_quantity`,
      s.`revised_quantity` = l.`latest_revised_quantity`,
      s.`original_total_eligible_cost` = l.`total_amount`,
      s.`latest_amend_total_eligible_cost` = l.`latest_amend_total_amount`,
      s.`revised_total_eligible_cost` = l.`revised_total_amount`,
      s.`original_fta_amount` = l.`original_FTA_amount`,
      s.`latest_amend_fta_amount` = l.`latest_amend_fta_amount`,
      s.`revised_fta_amount` = l.`revised_FTA_amount`,
      s.`original_state_amount` = l.`original_state_amount`,
      s.`latest_amend_state_amount` = l.`latest_amend_state_amount`,
      s.`revised_state_amount` = l.`revised_state_amount`,
      s.`original_local_amount` = l.`original_local_amount`,
      s.`latest_amend_local_amount` = l.`latest_amend_local_amount`,
      s.`revised_local_amount` = l.`revised_local_amount`,
      s.`original_other_fed_amount` = l.`original_other_fed_amount`,
      s.`latest_amend_other_fed_amount` = l.`latest_amend_other_fed_amount`,
      s.`revised_other_fed_amount` = l.`revised_other_fed_amount`,
      s.`original_state_in_kind_amount` = l.`original_state_in_kind_amount`,
      s.`latest_amend_state_in_kind_amount` = l.`latest_amend_state_inkind_amount`,
      s.`revised_state_in_kind_amount` = l.`revised_state_in_kind_amount`,
      s.`original_local_in_kind_amount` = l.`original_local_in_kind_amount`,
      s.`latest_amend_local_in_kind_amount` = l.`latest_amend_local_inkind_amount`,
      s.`revised_local_in_kind_amount` = l.`revised_local_in_kind_amount`,
      s.`original_toll_revenue_amount` = l.`original_toll_revenue_amount`,
      s.`latest_amend_toll_revenue_amount` = l.`latest_amend_toll_rev_amount`,
      s.`revised_toll_revenue_amount` = l.`revised_toll_revenue_amount`,
      s.`original_adjustment_amount` = l.`original_adjustment_amount`,
      s.`latest_amend_adjustment_amount` = l.`latest_amend_adjustment_amount`,
      s.`revised_adjustment_amount` = l.`revised_adjustment_amount`;

  /*
   * Mark the budget revision as pending
   */
  
  UPDATE `trams_budget_revision`
  SET `revision_status` = 'Pending',
      `approved_by` = NULL,
      `approved_date` = NULL
  WHERE `id` = p_budget_revision_id;

  INSERT INTO `trams_audit_history`(
    `context_id`,
    `context_details`,
    `action_taken_by`,
    `action_date_time`,
    `action_description`
  )
  VALUES (
    _application_id,
    'Application',
    'trams.administrator',
    now(),
    concat('Help Desk action to undo approved budget revision for ',
           _application_number, '.')
  );

  SET @audit_history_id = last_insert_id();

  DROP TEMPORARY TABLE IF EXISTS `trams_tmp_line_item_change_log_previous_revision`;
  DROP TEMPORARY TABLE IF EXISTS `trams_tmp_line_item_rollup_previous_revision`;

  SELECT * FROM `trams_budget_revision`
  WHERE `id` = p_budget_revision_id;

  SELECT * FROM `trams_scope`
  WHERE `application_id` = _application_id;

  SELECT * FROM `trams_line_item`
  WHERE `app_id` = _application_id;

  SELECT * FROM `trams_audit_history`
  WHERE `id` >= @audit_history_id;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_log`(IN `p_sid` VARCHAR(40), IN `p_name` VARCHAR(512), IN `p_text` VARCHAR(8192), IN `p_level` INT)
    NO SQL
BEGIN
  DECLARE _sid varchar(40);
  DECLARE _log_enabled int default 0;
  

  /*
      log level:
    debug: 0
    info: 1
    warn: 2
    error: 3
  */
  create table if not exists trams_sp_log_c(
    name varchar(255) not null unique,
    log_level int not null
  );

  create table if not exists trams_sp_log(
      caller_number varchar(40) not null,
      created_date_time datetime default now(),
    name varchar(255) not null,
    log_text varchar(8192) not null,
    log_level varchar(20) not null
  );  
  
  set _log_enabled = (select count(*) from trams_sp_log_config where lower(name)=lower(p_name) and p_level>=log_level);
  
  if _log_enabled >0 then
    insert into trams_sp_log (caller_number, name, log_text, log_level) values(p_sid, p_name, p_text, p_level);
  end if;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `trams_update_line_item_reference_null`()
BEGIN



CREATE temporary TABLE IF NOT EXISTS trams_line_item_reference_null_update (
  `line_item_code` varchar(20) DEFAULT NULL,
  `line_item_name` varchar(100) DEFAULT NULL,
  `activity_type` varchar(100) DEFAULT NULL,
  `activity_code` varchar(10) DEFAULT NULL,
  `allocation_code` varchar(10) DEFAULT NULL,
  `scope_code` varchar(10) DEFAULT NULL
) ;

--
-- Dumping data for table `trams_line_item_reference_null_update`
--

INSERT INTO `trams_line_item_reference_null_update` (`line_item_code`, `line_item_name`, `activity_type`, `activity_code`, `allocation_code`, `scope_code`) VALUES
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - CAPITAL BUS', '7D', '11', '117-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - CAPITAL BUS', '7D', '11', '117-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - CAPITAL BUS', '7K', '11', '117-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - CAPITAL BUS', '7K', '11', '117-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - CAPITAL BUS', '7K', '11', '117-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - CAPITAL BUS', '7K', '11', '117-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - CAPITAL BUS', '7N', '11', '117-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - CAPITAL BUS', '7N', '11', '117-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - CAPITAL RAIL', '7D', '12', '127-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - CAPITAL RAIL', '7D', '12', '127-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - CAPITAL RAIL', '7K', '12', '127-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - CAPITAL RAIL', '7K', '12', '127-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - CAPITAL RAIL', '7K', '12', '127-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - CAPITAL RAIL', '7K', '12', '127-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - CAPITAL RAIL', '7N', '12', '127-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - CAPITAL RAIL', '7N', '12', '127-00'),
('13.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - NEW STARTS', '7D', '13', '137-00'),
('13.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - NEW STARTS', '7D', '13', '137-00'),
('13.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - NEW STARTS', '7K', '13', '137-00'),
('13.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - NEW STARTS', '7K', '13', '137-00'),
('13.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - NEW STARTS', '7K', '13', '137-00'),
('13.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - NEW STARTS', '7K', '13', '137-00'),
('13.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - NEW STARTS', '7N', '13', '137-00'),
('13.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - NEW STARTS', '7N', '13', '137-00'),
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - OTHER PROGRAM COSTS', '7D', '11', '600-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - OTHER PROGRAM COSTS', '7D', '11', '600-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - OTHER PROG COSTS', '7N', '11', '600-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - OTHER PROG COSTS', '7N', '11', '600-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - OTHER PROGRAM COSTS', '7D', '11', '600-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - OTHER PROGRAM COSTS', '7D', '11', '600-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - OTHER PROG COSTS', '7K', '11', '600-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - OTHER PROG COSTS', '7N', '11', '600-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - OTHER PROG COSTS', '7N', '11', '600-00'),
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - INTERCITY BUS', '7D', '11', '634-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - INTERCITY BUS', '7D', '11', '634-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '11', '634-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '11', '634-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '11', '634-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '11', '634-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - INTERCITY BUS', '7N', '11', '634-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - INTERCITY BUS', '7N', '11', '634-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - INTERCITY BUS', '7D', '12', '634-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - INTERCITY BUS', '7D', '12', '634-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '12', '634-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '12', '634-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '12', '634-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - INTERCITY BUS', '7K', '12', '634-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - INTERCITY BUS', '7N', '12', '634-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - INTERCITY BUS', '7N', '12', '634-00'),
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - 5310', '7D', '11', '641-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - 5310', '7D', '11', '641-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - 5310', '7K', '11', '641-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - 5310', '7K', '11', '641-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - 5310', '7K', '11', '641-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - 5310', '7K', '11', '641-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - 5310', '7N', '11', '641-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - 5310', '7N', '11', '641-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - 5310', '7D', '12', '641-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - 5310', '7D', '12', '641-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - 5310', '7K', '12', '641-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - 5310', '7K', '12', '641-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - 5310', '7K', '12', '641-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - 5310', '7K', '12', '641-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - 5310', '7N', '12', '641-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - 5310', '7N', '12', '641-00'),
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - JARC', '7D', '11', '646-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - JARC', '7D', '11', '646-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - JARC', '7K', '11', '646-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - JARC', '7K', '11', '646-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - JARC', '7K', '11', '646-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - JARC', '7K', '11', '646-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - JARC', '7N', '11', '646-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - JARC', '7N', '11', '646-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - JARC', '7D', '12', '646-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - JARC', '7D', '12', '646-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - JARC', '7K', '12', '646-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - JARC', '7K', '12', '646-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - JARC', '7K', '12', '646-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - JARC', '7K', '12', '646-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - JARC', '7N', '12', '646-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - JARC', '7N', '12', '646-00'),
('11.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - NEW FREEDOM', '7D', '11', '647-00'),
('11.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - NEW FREEDOM', '7D', '11', '647-00'),
('11.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '11', '647-00'),
('11.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '11', '647-00'),
('11.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '11', '647-00'),
('11.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '11', '647-00'),
('11.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - NEW FREEDOM', '7N', '11', '647-00'),
('11.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - NEW FREEDOM', '7N', '11', '647-00'),
('12.7D.01', 'OVER THE ROAD BUS PRGM.', 'TRAINING - NEW FREEDOM', '7D', '12', '647-00'),
('12.7D.02', 'EMPLOYEE EDUCATION/TRAINING', 'TRAINING - NEW FREEDOM', '7D', '12', '647-00'),
('12.7K.01', 'SECURITY AND EMERGENCY RESPONSE PLANS', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '12', '647-00'),
('12.7K.02', 'BIOLOGICAL/CHEMICAL AGENT DETECTION', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '12', '647-00'),
('12.7K.03', 'EMERGENCY RESPONSE DRILLS', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '12', '647-00'),
('12.7K.04', 'SECURITY TRAINING', 'CRIME PREVENT/SECURITY - NEW FREEDOM', '7K', '12', '647-00'),
('12.7N.01', 'FUEL FOR VEHICLE OPERATIONS', 'SPECIAL FUEL PROVISION - NEW FREEDOM', '7N', '12', '647-00'),
('12.7N.02', 'UTILITY COSTS FOR ELECTRICAL VEHICLES', 'SPECIAL FUEL PROVISION - NEW FREEDOM', '7N', '12', '647-00');




update trams_line_item_reference r
inner join trams_line_item_reference_null_update u
on r.line_item_code = u.line_item_code
and r.activity_code= u.activity_code
and r.allocation_code = u.allocation_code
and r.scope_code = u.scope_code
and r.activity_type is null
set r.activity_type = u.activity_type;


drop  temporary table if exists trams_line_item_reference_null_update;

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetIndirectCostsClaimedReport`(IN p_recipientId VARCHAR(2048), IN p_costCenterCode VARCHAR(512), IN p_appStatus VARCHAR(1024), IN p_indirectCostType TINYINT(1))
BEGIN

	/*
	 Stored Procedure Name : usptramsGetIndirectCostsClaimedReport
     Description : Populate data for TrAMS Indirect Costs Excel Report
     Updated by  : Nathan Chatham (03.28.2021)
	*/

  DECLARE _whereClause VARCHAR(8192) DEFAULT " ";
  
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_id IN (",NULLIF(p_recipientId,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_cost_center IN (",NULLIF(p_costCenterCode,''),")"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND app.status IN (",NULLIF(p_appStatus,''),")", " AND app.status NOT IN ('Archived')"),"")));
  SET _whereClause = CONCAT(_whereClause, (SELECT IFNULL(CONCAT(" AND app.indirect_costs = ",NULLIF(p_indirectCostType,'')), " AND app.indirect_costs IN (1, 2)")));

  SET @SQLStmt = CONCAT("
	SELECT
		app.application_number AS 'Application Number',
		app.status AS 'Application Status',
		app.application_name AS 'Application Name',
		app.recipient_name AS 'Recipient Name',
		app.recipient_id AS 'Recipient ID',
		app.recipient_cost_center AS 'Cost Center',
		MAX(ftr.financial_report_freq) AS 'FFR Reporting Frequency',
		CASE
			WHEN app.indirect_costs = 1 THEN 'Yes - Apply Approved Rate'
			WHEN app.indirect_costs = 2 THEN 'Yes - De Minimis'
		END AS 'Indirect Costs',
		app.indirect_cost_description AS 'Indirect Cost Percentage Description'
	FROM trams_application app
	LEFT JOIN trams_fta_review ftr ON ftr.application_id = app.id
	WHERE 1 ", 
	_whereClause,
	" GROUP BY app.id
	  ORDER BY app.application_number, app.fiscal_year DESC
	"
  );
	
  PREPARE indirectCostsStmt FROM @SQLStmt;
  EXECUTE indirectCostsStmt;
	
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetOperbudSummaryExcelReport`(
	IN operbudFiscalYear VARCHAR(255),
	IN controlNumber VARCHAR(255),
	IN fundingFiscalYear VARCHAR(255),
	IN limitationCode VARCHAR(255),
	IN appropriationCode VARCHAR(255),
	IN sectionCode VARCHAR(255),
    IN operbudStatus VARCHAR(255),
    IN appropriationType VARCHAR(20),
	IN gmtOffset VARCHAR(10),
    IN returnLimit VARCHAR(20)
    -- returnLimit will receive '-1' in our call to the SP
)
BEGIN

	DECLARE whereClause VARCHAR(4096) DEFAULT " ";
	DECLARE limitClause VARCHAR(100) DEFAULT " ";

    /*
		This calls an existing function in Production that sets the limit
		clause based on a given return limit value
	*/
	CALL trams_get_sql_limit_clause(returnLimit,limitClause);

    /*
		Each whereClause statement below concats the previous IN parameter value with its parameter value
		ultimately into one string of text where each value has a value provided by the user or ' '
		e.g. , and o.fiscal_year = '####' and o.control_number = '#########' and a.acc_funding_fiscal_year.....
    */
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND o.fiscal_year = '",NULLIF(operbudFiscalYear,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND o.control_number = '",NULLIF(controlNumber,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_funding_fiscal_year = '",NULLIF(fundingFiscalYear,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_limitation_code = '",NULLIF(limitationCode,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_appropriation_code = '",NULLIF(appropriationCode,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND a.acc_section_code = '",NULLIF(sectionCode,''),"'"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND o.status IN (",NULLIF(operbudStatus,''),")"),"")));
	SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND al.appropriation_type_flag IN (",NULLIF(appropriationType,''),")"),"")));

  /*
	Grabs the time the SP is called as an ID for our existing table trams_sp_log to
    eventually log information about the given stored procedure that was called
  */
  SET @sid = (SELECT DATE_FORMAT(NOW(), '%Y%m%d%H%i%s%f'));
	-- added UUID for session reference
  SET @uuid = UUID_SHORT();

  SET @TempAuthorizedQuery = "";
  /*
	After creating our temp table, the goal is to retrieve all account class codes associated
    to authorized operating budgets (based on trmsprtn_ccntclss_prtngbdg foreign key) from our
    trams_account_class_lookup table (essentially our ref table for all account class codes (ACCs),
    we join on the other tables based on FK & ACC since we will be pulling data from these tables
	to avoid having multiple rows for each account class codes for every fiscal year and
    every cost center in the nation, we group by the account_class_code, then by fiscal year, then cost center code
  */

	-- changed CREATE TEMPORARY TABLE to INSERT INTO statement
  -- added trams_temp_operbud_summary_authorized_id, uuid, and sid columns
  SET @TempAuthorizedQuery = CONCAT("INSERT INTO trams_temp_operbud_summary_authorized
    SELECT
			NULL AS trams_temp_operbud_summary_authorized_id,
      a.account_class_code AS `account_class_code`,
      a.fiscal_year AS `fiscal_year`,
      a.cost_center_code AS `cost_center_code`,
      SUM(a.code_new_amount) AS `sum_code_new_amount`,
			@uuid AS uuid,
      @sid AS sid
    FROM `trams_account_class_code_lookup` al
      JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
      JOIN `trams_operating_budget` o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
    WHERE o.status='Authorized'
      AND a.authorized_flag=1
      AND a.code_transaction_type <> 'Recovery' ",whereClause,"
    GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`	",limitClause);

   /*
	Another write is made to trams_sp_log via call of trams_log, for
    historical / debug purposes, that the above variable was set successfully
   */
  CALL `trams_log`(@SId, 'usptramsGetOperbudSummaryExcelReport', CONCAT('Complete Temporary Operbud Summary Query: ',@TempAuthorizedQuery), 1);
	PREPARE tmpoperbudsummaryAuthorizedStmt FROM @TempAuthorizedQuery;
	EXECUTE tmpoperbudsummaryAuthorizedStmt;

    /*
		TempAmountsQuery looks at our account_class_code_history table,
        in conjunction with the WHERE clause, to determine which account class codes
        should be included in the retrieval of the amounts for the given account class codes
        (and are grouped by the same criteria as before except it's from the account_class_code_history table)
		trams_account_class_history table, in our system, is used to record changes
	*/
	SET @TempAmountsQuery = "";
	-- changed CREATE TEMPORARY TABLE to INSERT INTO statement
	-- added trams_temp_operbud_summary_amounts_id, uuid, and sid columns
	SET @TempAmountsQuery = CONCAT("INSERT INTO trams_temp_operbud_summary_amounts
    SELECT
			NULL AS trams_temp_operbud_summary_amounts_id,
      acch.account_class_code,
      acch.transaction_fiscal_year,
      acch.cost_center_code,
      SUM(acch.transfer_in_amount) AS `transfer_in_amount`,
      SUM(acch.transfer_out_amount) AS `transfer_out_amount`,
      IFNULL(SUM(acch.reserved_amount),0) - IFNULL(SUM(acch.unreserve_amount),0) AS `reservation_amount`,
      SUM(acch.obligated_amount) AS `obligation_amount`,
      SUM(acch.recovery_amount) AS `recovery_amount`,
			@uuid AS uuid,
      @sid AS sid
    FROM trams_account_class_code_history acch
    WHERE account_class_code IN (SELECT account_class_code from trams_account_class_code_lookup)
    GROUP BY `acch`.`account_class_code`,`acch`.`transaction_fiscal_year`,`acch`.`cost_center_code`	",limitClause);

   -- Same as last call to trams_log

	CALL `trams_log`(@SId, 'usptramsGetOperbudSummaryExcelReport', CONCAT('Complete Temporary Operbud Summary Query: ',@TempAmountsQuery), 1);
	PREPARE tmpoperbudsummaryAmountsStmt FROM @TempAmountsQuery;
	EXECUTE tmpoperbudsummaryAmountsStmt;

	/*
		We set operbudsummaryQuery to be the select statement below
		if the operbud status the user chooses nothing for a status or Authorized
	*/
	SET @OperbudSummaryQuery = "";
	IF (operbudStatus IS NULL OR operbudStatus LIKE '%Authorized%') THEN
    SET @OperbudSummaryQuery = CONCAT("
    SELECT
      o.fiscal_year,
      o.cost_center_code AS `cost_center`,
      o.status,
      a.account_class_code,
      o.control_number,
      a.code_transaction_type AS `transaction_type`,
      a.code_previous_amount AS `previous_amount`,
      0 as `pending_amount`,
      IFNULL(tmp.sum_code_new_amount,0) AS `authorized_amount`,
      IFNULL(tmp.sum_code_new_amount,0) + IFNULL(acch.`transfer_in_amount`,0) - IFNULL(acch.reservation_amount,0) - IFNULL(acch.`obligation_amount`,0)
        - IFNULL(acch.`transfer_out_amount`,0) + IFNULL(`acch`.`recovery_amount`,0) AS `oper_budget_avail`,
		
		trams_format_date(cast(CONVERT_TZ(o.created_date_time,'+00:00',",gmtOffset,") AS char)) AS `date created`
		
    FROM `trams_account_class_code_lookup` al
      JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
      LEFT JOIN trams_temp_operbud_summary_authorized tmp
        ON tmp.account_class_code = a.account_class_code
        AND tmp.fiscal_year = `a`.`fiscal_year`
        AND tmp.cost_center_code = `a`.`cost_center_code`
				AND tmp.uuid = @uuid
      JOIN `trams_operating_budget` o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      LEFT JOIN trams_temp_operbud_summary_amounts acch

        ON `a`.`account_class_code`=acch.account_class_code
        AND `a`.`fiscal_year`=acch.transaction_fiscal_year
        AND `a`.`cost_center_code`=acch.cost_center_code
				AND acch.uuid = @uuid
    WHERE o.status='Authorized' AND a.authorized_flag=1",whereClause,"
    GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`	",limitClause);

	END IF;
  /*
	This if condition also checks if the operbudStatus is Pending and NOT Authorized.
    We do this to see if doing a union all is needed or not. If we receive both
    authorized and pending for operbudStatus, we need both select statements
    and the data they would retrieve based on the WHERE clause
  */
  IF (operbudStatus LIKE '%Pending%' AND operbudStatus NOT LIKE '%Authorized%') THEN
	SET @OperbudSummaryQuery = CONCAT("
      SELECT
        o.fiscal_year,
        o.cost_center_code AS `cost_center`,
        o.status,
        a.account_class_code,
        o.control_number,
        a.code_transaction_type AS `transaction_type`,
        SUM(a.code_previous_amount) AS `previous_amount`,
        SUM(a.code_change_amount) AS `pending_amount`,
        0 as `authorized_amount`,
        0 AS `oper_budget_avail`,
		
		trams_format_date(cast(CONVERT_TZ(o.created_date_time,'+00:00',",gmtOffset,") AS char)) AS `date created`
		
      FROM `trams_account_class_code_lookup` al
				JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
        JOIN `trams_operating_budget` o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      WHERE o.status = 'Pending'",whereClause,"
      GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`",limitClause);

      /*
		If the operating budget status (operbudStatus) selected is Pending, we select values
        that are being requested (fiscal year, cost center code, etc.) and show dollar amounts
        for the following fields: code_previous_amount (previous amount for the ACC) and
		code_change_amount (the amount that needs authorizing). If the only status selected was
        'Pending', then we ignore the select / join statement for Authorized OBs
        and only retrieve data for pending OBs
    */
    ELSE
		SET @OperbudSummaryQuery = CONCAT(@OperbudSummaryQuery, " UNION ALL ", "
        SELECT
        o.fiscal_year,
        o.cost_center_code AS `cost_center`,
        o.status,
        a.account_class_code,
        o.control_number,
        a.code_transaction_type AS `transaction_type`,
        SUM(a.code_previous_amount) AS `previous_amount`,
        SUM(a.code_change_amount) AS `pending_amount`,
        0 AS `authorized_amount`,
        0 AS `oper_budget_avail`,
		
		trams_format_date(cast(CONVERT_TZ(o.created_date_time,'+00:00',",gmtOffset,") AS char)) AS `date created`
		
      FROM `trams_account_class_code_lookup` al
				JOIN `trams_account_class_code` a ON al.account_class_code = a.account_class_code
        JOIN trams_operating_budget o ON o.id = `a`.`trmsprtn_ccntclss_prtngbdg`
      WHERE o.status = 'Pending'",whereClause,"
      GROUP BY `a`.`account_class_code`,`a`.`fiscal_year`,`a`.`cost_center_code`",limitClause);

	END IF;

    /*
		We make a call to our SP to write to trams_sp_log table right before the statement is executed
		for historical purposes
	*/

	CALL `trams_log`(@SId, 'usptramsGetOperbudSummaryExcelReport', CONCAT('Complete Operbud Summary Query: ',@OperbudSummaryQuery), 1);

	PREPARE operbudsummaryStmt FROM @OperbudSummaryQuery;
	EXECUTE operbudsummaryStmt;

    /*
		After execution, we write to our trams_sp_log table for historical purposes
    */
	CALL `trams_log`(@SId, 'usptramsGetOperbudSummaryExcelReport', 'Done!', 1);

	DELETE FROM trams_temp_operbud_summary_authorized WHERE uuid = @uuid;
  DELETE FROM trams_temp_operbud_summary_amounts WHERE uuid = @uuid;


END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetAvailableDAFISSuffixCodes`(IN p_lapseYear VARCHAR(255),  IN p_sectionCode VARCHAR(255), IN p_limitNum INT(11))
BEGIN

	/*
	 Deployment script name: usptramsGetAvailableDAFISSuffixCodes
	 Description: Returns list of Available DAFIS Suffix codes
	 Created by: Garrison Dean (2022.Feb.01)
	*/
	
	SET @lapseYear = p_lapseYear;
	SET @sectionCode = p_sectionCode;
	SET @limitNum = p_limitNum;	
	
	/*Returns unavailable DAFIS codes currently in the system based on active lapsed ACCs(lapse year) and active awards*/
	SET @unavailableDAFISCodeTable = CONCAT('
		(
		SELECT DISTINCT 
			acc1.dafis_suffix_code
		FROM
			trams_account_class_code_lookup acc1
		WHERE
			acc1.section_code IN (', @sectionCode, ')
		AND acc1.lapse_year >= ', @lapseYear, '
		AND acc1.active_flag = 1
		AND acc1.dafis_suffix_code IS NOT NULL
		AND acc1.dafis_suffix_code <> \' \'
		
		UNION
		
		SELECT DISTINCT
			acc2.dafis_suffix_code
		FROM 
			trams_account_class_code_lookup acc2
		JOIN 
			trams_contract_acc_funding fund ON fund.account_class_code = acc2.account_class_code
		JOIN 
			trams_application app ON app.id = fund.application_id AND app.status NOT IN (\'Closed\', \'Archived\')
		WHERE 
			acc2.section_code IN (', @sectionCode, ')
		AND acc2.dafis_suffix_code IS NOT NULL
		AND acc2.dafis_suffix_code <> \' \'
		) AS unavailableDAFISCodeTable
	');
	
	/*Returns the available DAFIS codes*/
	SET @finalSQLStmt = CONCAT('
		SELECT DISTINCT 
			DAFISRef.DAFISCodeId, 
			DAFISRef.DAFISCode
		FROM 
			tramsDAFISSuffixCodeReference DAFISRef
		LEFT JOIN ', @unavailableDAFISCodeTable, 
		' ON unavailableDAFISCodeTable.dafis_suffix_code = DAFISRef.DAFISCode
		WHERE 
			unavailableDAFISCodeTable.dafis_suffix_code IS NULL
		AND DAFISRef.isActive = 1
		ORDER BY DAFISRef.DAFISCodeId
		LIMIT ', @limitNum
	);	
	
	PREPARE finalSQLStmt FROM @finalSQLStmt;
	EXECUTE finalSQLStmt;
	DEALLOCATE PREPARE finalSQLStmt;	

END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetFederalFinancialReportExcel`(IN granteeId VARCHAR(2048), IN costCenterCode VARCHAR(1024), IN fiscalYear VARCHAR(255),
IN appNumber VARCHAR(50), IN appStatus VARCHAR(1024), IN appType VARCHAR(1024), IN reportStatus VARCHAR(255), IN finalReport VARCHAR(255), 
IN reportPeriodType VARCHAR(6000), IN lastSubmittedDateStart VARCHAR(20), IN lastSubmittedDateEnd VARCHAR(20), IN reportPeriod VARCHAR(20),
IN gmtOffset VARCHAR(10), IN returnLimit VARCHAR(20))
BEGIN

	/*
     Stored Procedure Name : usptramsGetFederalFinancialReportExcel
     Description : To populate data for dynamic TrAMS 'FFR Detail' excel report.
     Updated by  : Hunter Wang (2020.Aug.25)
    */
  
	DECLARE sqlClause VARCHAR(40960) DEFAULT "";
    DECLARE whereClause VARCHAR(8192) DEFAULT " ";
    DECLARE limitClause VARCHAR(100) DEFAULT " ";
    DECLARE ffrWhereClause VARCHAR(8192) DEFAULT " ";
  
    CALL trams_get_sql_limit_clause(750000, limitClause); 
   
	-- setting whereClause to include search criteria provided from user 
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_id IN (",NULLIF(granteeId,''),")"),"")));
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.recipient_cost_center IN (",NULLIF(costCenterCode,''),")"),"")));
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.fiscal_year IN (",NULLIF(fiscalYear,''),")"),"")));
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.application_number IN ('",NULLIF(appNumber,''),"')"),"")));
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.application_type IN ('",NULLIF(appType,''),"')"),"")));
    SET whereClause = CONCAT(whereClause, (SELECT IFNULL(CONCAT(" AND app.status IN ('",NULLIF(appStatus,''),"')"),"")));

	-- setting ffrWhereClause to include all ffr-related search criteria the user has provided
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.report_status IN ('",NULLIF(reportStatus,''),"')"),"")));
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.final_report_flag IN (",NULLIF(finalReport,''),")"),"")));
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.report_period_type_CONCAT IN (",NULLIF(reportPeriodType,''),")"),"")));
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.report_period_begin_date >= '",NULLIF(lastSubmittedDateStart,''),"'"),"")));
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.report_period_end_date <= '",NULLIF(lastSubmittedDateEnd,''),"'"),"")));
    SET ffrWhereClause = CONCAT(ffrWhereClause, (SELECT IFNULL(CONCAT(" AND ffr.report_period_type IN (",NULLIF(reportPeriod,''),")"),"")));
  
    SET SESSION max_heap_table_size = 1024*1024*512;
  

	SET @ffrQuery = 
	       CONCAT("SELECT 
                    `app`.`recipient_id` AS `grantee_id`,
                     REPLACE(`gbi`.`recipientAcronym`, ',' , ' ') AS `grantee_acronym`,
                     REPLACE(`gbi`.`legalBusinessName`, ',' , ' ') AS `grantee_name`,
					 `app`.`recipient_cost_center` AS `grantee_cost_center`,
					 REPLACE(app.fain_state, ',' , ' ') AS `award_state`,
					 `app`.`fiscal_year` AS `latest_award_fiscal_year`,
					 SUBSTRING(`app`.`application_number`, 1, CHAR_LENGTH(`app`.`application_number`)-3) AS `latest_award_number`,
					 `app`.`amendment_number` AS `latest_amendment_number`,
					 REPLACE(`app`.`application_name`, ',' , ' ') AS `latest_award_name`,
					 `app`.`status` AS `latest_award_status`,
					 `app`.`application_type` AS `latest_award_type`,
					 `app`.`discretionary_funds_flag` AS `is_discretionary_grant`,
					 REPLACE(`app`.`recipient_contact`, ',' , ' ') AS `grantee_point_of_contact`,
					 REPLACE(`app`.`fta_post_award_manager`, ',' , ' ') AS `fta_post_award_mgr`,
					 `ffr`.`report_status` AS `ffr_status`,
					 `ffr`.`fiscal_year` AS `ffr_fiscal_year`,
					 `ffr`.`report_period_type` AS `ffr_period_type`,
					  Trams_get_ffr_period_name(`ffr`.`report_period_type`, `ffr`.`fiscal_year`, `ffr`.`fiscal_month`, `ffr`.`fiscal_quarter`) AS `ffr_fiscal_period`, 
					 `ffr`.`final_report_flag` AS `ffr_final_report_flag`,
					  trams_format_date(cast(CONVERT_TZ(ffr.initial_submission_date_time, '+00:00', ",gmtOffset,") AS char)) AS `ffr_submitted_date`,
					  REPLACE(`ffr`.`initial_submission_by`, ',' , ' ') AS `ffr_submitted_by`,
					  `ffr`.`fed_cash_onhand_cum_begin_amt` AS `federalCashOnHandCumBeginAmt`,
					  `ffr`.`fed_cash_receipt_cum_amt` AS `federalCashReceiptCumulativeAmt`,
					  `ffr`.`fed_cash_disburse_cum_amt` AS `federalCashDisburseCumulativeAmt`,
					  `ffr`.`fed_cash_onhand_cum_begin_amt` AS `federalCashOnHandCumEndAmt`,
					  `ffr`.`total_federal_authorized_amount` AS `totalFederalAuthorizedAmt`,
					  `ffr`.`fed_share_expend_cum_amt` AS `fedShareExpendCumulativeAmt`,
					  `ffr`.`recipient_share_expend_cum_amt` AS `granteeShareExpendCumulativeAmt`,
					  `ffr`.`total_expenditure_cumulative_amount` AS `totalExpenditureCumulativeAmt`,
					  `ffr`.`fed_share_unliquid_obligation_amt` AS `fedShareUnliquidObligationAmt`,
					  `ffr`.`recipient_share_unliquid_obligation_amt` AS `granteeShareUnliquidObligationAmt`,
					  `ffr`.`total_unliquid_obligation_amount` AS `totalUnliquidObligationAmt`,
					  `ffr`.`total_federal_share_amount` AS `totalFederalShareAmt`,
					  `ffr`.`fed_unobligated_balance_amt` AS `federalUnobligatedBalanceAmt`,
					  (IFNULL(`cat`.`obligation_amount`, 0) - IFNULL(`cat`.`deobligation_amount`,0)) AS `award_obligation_amount`,
					  (IFNULL(`cat`.`disbursement_amount`,0) - IFNULL(`cat`.`refund_amount`,0)) AS `award_disbursement_amount`,
					  `cat`.`unliquidated_amount` AS `award_unliquidated_amount`,
					  trams_format_date(cast(CONVERT_TZ(aad.obligated_date_time, '+00:00',",gmtOffset,") AS char)) AS `award_obligated_date`,
					  trams_format_date(cast(CONVERT_TZ(aad.disbursement_date_time, '+00:00',",gmtOffset,") AS char)) AS `award_disbursement_date`,
					  trams_format_date(cast(CONVERT_TZ(aad.deobligation_date_time, '+00:00',",gmtOffset,") AS char)) AS `award_deobligated_date`
					  FROM trams_federal_financial_report ffr
                  LEFT JOIN 
				  -- This subquery replaced use of an external table
				         (
						    SELECT 
								`cat`.`contract_award_id` AS `contract_award_id`,
								SUM(`cat`.`obligation_amount`) AS `obligation_amount`,
								SUM(`cat`.`deobligation_amount`) AS `deobligation_amount`,
								SUM(`cat`.`disbursement_amount`) AS `disbursement_amount`,
								SUM(refund_amount) AS refund_amount,
								(((IFNULL(SUM(obligation_amount),0) 
								- IFNULL(SUM(deobligation_amount),0)) 
								- IFNULL(SUM(disbursement_amount),0)) 
								+ IFNULL(SUM(refund_amount),0)) AS unliquidated_amount
							FROM 
									`trams_contract_acc_transaction` cat
							GROUP BY cat.contract_award_id
						 ) cat ON ffr.contract_award_id = cat.contract_award_id
                  LEFT JOIN 
				         trams_application app ON app.contractAward_id = cat.contract_award_id
                  LEFT JOIN 
				         trams_application_award_detail aad ON aad.application_id = app.id
                  LEFT JOIN 
				         vwtramsGeneralBusinessInfo gbi ON gbi.recipientId = app.recipient_id
                  WHERE 
					-- This subquery replaced use of an external table
				     app.id IN (
						SELECT 
							MAX(id) AS application_id
	                    FROM 
	                        trams_application app
	                    WHERE 
						    status IN ('Closed', 'Active (Executed)', 'Active / Budget Revision Under Review', 'Active / Budget Revision In-Progress')
                        GROUP BY contractAward_id
					 ) ", whereClause, ffrWhereClause, limitClause);
  
	-- log above query
    CALL `trams_log`(@sid, 'usptramsGetFederalFinancialReportExcel', CONCAT('FFR Query: ',@ffrQuery), 1); 

	PREPARE ffrStmt FROM @ffrQuery;
    EXECUTE ffrStmt;
  
    -- log compeltion of all the above queries successfully completing
    CALL `trams_log`(@sid, 'usptramsGetFederalFinancialReportExcel', 'Done!', 1);   
  
END$$
DELIMITER ;

DELIMITER $$
CREATE PROCEDURE `usptramsGetReservationAccountClassCodeDetail`(IN ApplicationId INT(11), IN UzaCodeReferenceVersion INT(11))
BEGIN

    SET @ApplicationId = ApplicationId;
    SET @UzaCodeReferenceVersion = UzaCodeReferenceVersion;
    SET @CurrentFiscalYear = trams_get_current_fiscal_year(NOW());

    /*
     * Subquery the reservation selected account class code table and join on
     * the account class code table and the account class code history table.
     * Filter by the specified application ID. Exclude any account class codes
     * of type Recovery to avoid double counting (these transactions will still
     * be included from the account class code history table). Group by the
     * unique account class code combinations. Calculate the available operating
     * budget amount as the initial authority + transfer in - transfer out -
     * reservations + unreservations - obligations + recoveries.
	 
		Stored Procedure Name : usptramsGetReservationAccountClassCodeDetail
		Description : Used during Selection of ACCS via Reservation workflow
		Updated by  : Garrison Dean (2021.Dec.16)
     */
	 
    SET @AccountClassCodeTable = CONCAT('
        (
            SELECT
                @CurrentFiscalYear AS FiscalYear,
                ReservationSelectedAccountClassCode.CostCenterCode AS CostCenterCode,
                ReservationSelectedAccountClassCode.UzaCode AS UzaCode,
                AccountClassCodeLookup.account_class_code AS AccountClassCode,
                AccountClassCodeLookup.acc_description AS AccDescription,
                ReservationSelectedAccountClassCode.FundingFiscalYear AS FundingFiscalYear,
                ReservationSelectedAccountClassCode.AppropriationCode AS AppropriationCode,
                ReservationSelectedAccountClassCode.SectionCode AS SectionCode,
                ReservationSelectedAccountClassCode.LimitationCode AS LimitationCode,
                ReservationSelectedAccountClassCode.AuthorityType AS AuthorityType,
                ReservationSelectedAccountClassCode.Fpc AS Fpc,
                (
                    COALESCE(AccountClassCode.code_new_amount, 0) +
                    COALESCE(SUM(AccountClassCodeHistory.transfer_in_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.transfer_out_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.reserved_amount), 0) +
                    COALESCE(SUM(AccountClassCodeHistory.unreserve_amount), 0) -
                    COALESCE(SUM(AccountClassCodeHistory.obligated_amount), 0) +
                    COALESCE(SUM(AccountClassCodeHistory.recovery_amount), 0)
                ) AS OperatingBudgetAvailableBalance
            FROM
                tramsReservationSelectedAccountClassCode ReservationSelectedAccountClassCode
            JOIN
                trams_account_class_code AccountClassCode
                    ON AccountClassCode.cost_center_code = ReservationSelectedAccountClassCode.CostCenterCode
                    AND AccountClassCode.acc_funding_fiscal_year = ReservationSelectedAccountClassCode.FundingFiscalYear
                    AND AccountClassCode.acc_appropriation_code = ReservationSelectedAccountClassCode.AppropriationCode
                    AND AccountClassCode.acc_section_code = ReservationSelectedAccountClassCode.SectionCode
                    AND AccountClassCode.acc_limitation_code = ReservationSelectedAccountClassCode.LimitationCode
                    AND AccountClassCode.acc_auth_type = ReservationSelectedAccountClassCode.AuthorityType
            JOIN
                trams_account_class_code_lookup AccountClassCodeLookup
                    ON AccountClassCodeLookup.account_class_code = AccountClassCode.account_class_code
            LEFT JOIN
                trams_account_class_code_history AccountClassCodeHistory
                    ON AccountClassCodeHistory.transaction_fiscal_year = AccountClassCode.fiscal_year
                    AND AccountClassCodeHistory.cost_center_code = AccountClassCode.cost_center_code
                    AND AccountClassCodeHistory.account_class_code = AccountClassCode.account_class_code
            WHERE
                ReservationSelectedAccountClassCode.ApplicationId = ?
                AND AccountClassCode.authorized_flag = 1
                AND AccountClassCode.code_transaction_type <> \'Recovery\'
				AND AccountClassCode.fiscal_year = @CurrentFiscalYear
                AND AccountClassCodeLookup.lapse_year > @CurrentFiscalYear
            GROUP BY
                ReservationSelectedAccountClassCode.CostCenterCode,
                ReservationSelectedAccountClassCode.UzaCode,
                ReservationSelectedAccountClassCode.FundingFiscalYear,
                ReservationSelectedAccountClassCode.AppropriationCode,
                ReservationSelectedAccountClassCode.SectionCode,
                ReservationSelectedAccountClassCode.LimitationCode,
                ReservationSelectedAccountClassCode.AuthorityType,
                ReservationSelectedAccountClassCode.Fpc
        ) AS AccountClassCodeTable
    ');

    /*
     * Subquery the reservation selected account class code table and join on
     * the apportionment table and the apportionment history table. Filter by
     * the specified application ID. Group by the unique apportionment fields.
     * Calculate the available apportionment amount as the initial authority +
     * modifications + transfer in - transfer out - reservations +
     * unreservations - obligations + recoveries.
     */
     SET @ApportionmentTable = CONCAT('
        (
            SELECT
                @CurrentFiscalYear AS FiscalYear,
                ReservationSelectedAccountClassCode.CostCenterCode AS CostCenterCode,
                ReservationSelectedAccountClassCode.UzaCode AS UzaCode,
                ReservationSelectedAccountClassCode.FundingFiscalYear AS FundingFiscalYear,
                ReservationSelectedAccountClassCode.AppropriationCode AS AppropriationCode,
                ReservationSelectedAccountClassCode.SectionCode AS SectionCode,
                ReservationSelectedAccountClassCode.LimitationCode AS LimitationCode,
                ReservationSelectedAccountClassCode.AuthorityType AS AuthorityType,
                ReservationSelectedAccountClassCode.Fpc AS Fpc,
                (
                    COALESCE(Apportionment.apportionment, 0) +
                    COALESCE(SUM(ApportionmentHistory.modified_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.transfer_in_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.transfer_out_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.reserved_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.unreserve_amount), 0) -
                    COALESCE(SUM(ApportionmentHistory.obligated_amount), 0) +
                    COALESCE(SUM(ApportionmentHistory.recovery_amount), 0)
                ) AS ApportionmentAvailableBalance
            FROM
                tramsReservationSelectedAccountClassCode ReservationSelectedAccountClassCode
            JOIN
                trams_apportionment Apportionment
                    ON Apportionment.uza_code = ReservationSelectedAccountClassCode.UzaCode
                    AND Apportionment.funding_fiscal_year = ReservationSelectedAccountClassCode.FundingFiscalYear
                    AND Apportionment.appropriation_code = ReservationSelectedAccountClassCode.AppropriationCode
                    AND Apportionment.section_id = ReservationSelectedAccountClassCode.SectionCode
                    AND Apportionment.limitation_code = ReservationSelectedAccountClassCode.LimitationCode
            LEFT JOIN
                trams_apportionment_history ApportionmentHistory
                    ON ApportionmentHistory.uza_code = Apportionment.uza_code
                    AND ApportionmentHistory.funding_fiscal_year = Apportionment.funding_fiscal_year
                    AND ApportionmentHistory.appropriation_code = Apportionment.appropriation_code
                    AND ApportionmentHistory.section_id = Apportionment.section_id
                    AND ApportionmentHistory.limitation_code = Apportionment.limitation_code
            WHERE
                ReservationSelectedAccountClassCode.ApplicationId = ?
                AND Apportionment.lapse_period > @CurrentFiscalYear
                AND Apportionment.active_flag = 1
            GROUP BY
                ReservationSelectedAccountClassCode.CostCenterCode,
                ReservationSelectedAccountClassCode.UzaCode,
                ReservationSelectedAccountClassCode.FundingFiscalYear,
                ReservationSelectedAccountClassCode.AppropriationCode,
                ReservationSelectedAccountClassCode.SectionCode,
                ReservationSelectedAccountClassCode.LimitationCode,
                ReservationSelectedAccountClassCode.AuthorityType,
                ReservationSelectedAccountClassCode.Fpc
        ) AS ApportionmentTable
    ');

    /*
     * Subquery the application holding pot table and join on the application
     * holding pot reservation table. Filter by the specified application ID.
     * Exclude any holding pots that are inactive or are associated with budget
     * revisions (type ID = 5). Group by UZA code, cost center code, account
     * class code, and FPC. Calculate the available holding pot balance as
     * holding amount - reservation amount.
     */
    SET @HoldingPotReservationTable = CONCAT('
        (
            SELECT
                HoldingPot.ApplicationHoldingPotId AS ApplicationHoldingPotId,
                HoldingPot.UzaCode AS UzaCode,
                HoldingPot.CostCenterCode AS CostCenterCode,
                HoldingPot.AccountClassCode AS AccountClassCode,
                HoldingPot.FPC AS Fpc,
                (
                    COALESCE(HoldingPot.HoldingAmount, 0) -
                    COALESCE(SUM(HoldingPotReservation.ReservationAmount), 0)
                ) AS HoldingPotAvailableBalance
            FROM
                tramsApplicationHoldingPot HoldingPot
            LEFT JOIN
                tramsApplicationHoldingPotReservation HoldingPotReservation
                    ON HoldingPotReservation.ApplicationHoldingPotId = HoldingPot.ApplicationHoldingPotId
            WHERE
                HoldingPot.ApplicationId = ?
                AND HoldingPot.ActiveFlag = 1
                AND HoldingPot.TypeId <> 5
            GROUP BY
                HoldingPot.ApplicationHoldingPotId,
                HoldingPot.UzaCode,
                HoldingPot.CostCenterCode,
                HoldingPot.AccountClassCode,
                HoldingPot.FPC
        ) AS HoldingPotReservationTable
    ');

    /*
      * Subquery the UZA code reference table and filter by the specified
      * version number. Group by the unique UZA codes.
      */
    SET @UzaCodeReferenceTable = CONCAT('
        (
            SELECT
                uza_code AS UzaCode,
                uza_name AS UzaName
            FROM
                trams_uza_code_reference
            WHERE
                version = ?
            GROUP BY
                uza_code
        ) AS UzaCodeReferenceTable
    ');

    /*
     * Prepare and execute the final output query, passing in the relevant
     * parameters.
     */
    SET @SqlString = CONCAT('
        SELECT
            ACS_FN_GenerateRowNumber() AS ReservationAccountClassCodeDetailId,
            HoldingPotReservationTable.ApplicationHoldingPotId AS ApplicationHoldingPotId,
            AccountClassCodeTable.FiscalYear AS FiscalYear,
            AccountClassCodeTable.AccountClassCode AS AccountClassCode,
            AccountClassCodeTable.AccDescription AS AccDescription,
            AccountClassCodeTable.FundingFiscalYear AS FundingFiscalYear,
            AccountClassCodeTable.AppropriationCode AS AppropriationCode,
            AccountClassCodeTable.SectionCode AS SectionCode,
            AccountClassCodeTable.LimitationCode AS LimitationCode,
            AccountClassCodeTable.AuthorityType AS AuthorityType,
            AccountClassCodeTable.CostCenterCode AS CostCenterCode,
            AccountClassCodeTable.Fpc AS Fpc,
            UzaCodeReferenceTable.UzaCode AS UzaCode,
            UzaCodeReferenceTable.UzaName AS UzaName,
            COALESCE(AccountClassCodeTable.OperatingBudgetAvailableBalance, 0) OperatingBudgetAvailableBalance,
            COALESCE(ApportionmentTable.ApportionmentAvailableBalance, 0) AS ApportionmentAvailableBalance,
            COALESCE(HoldingPotReservationTable.HoldingPotAvailableBalance, 0) AS HoldingPotAvailableBalance
        FROM',
            @AccountClassCodeTable,
        'JOIN',
            @ApportionmentTable,
                'ON ApportionmentTable.FiscalYear = AccountClassCodeTable.FiscalYear
                AND ApportionmentTable.CostCenterCode = AccountClassCodeTable.CostCenterCode
                AND ApportionmentTable.UzaCode = AccountClassCodeTable.UzaCode
                AND ApportionmentTable.FundingFiscalYear = AccountClassCodeTable.FundingFiscalYear
                AND ApportionmentTable.AppropriationCode = AccountClassCodeTable.AppropriationCode
                AND ApportionmentTable.SectionCode = AccountClassCodeTable.SectionCode
                AND ApportionmentTable.LimitationCode = AccountClassCodeTable.LimitationCode
                AND ApportionmentTable.AuthorityType = AccountClassCodeTable.AuthorityType
                AND ApportionmentTable.Fpc = AccountClassCodeTable.Fpc
        LEFT JOIN',
            @HoldingPotReservationTable,
                'ON HoldingPotReservationTable.UzaCode = AccountClassCodeTable.UzaCode
                AND HoldingPotReservationTable.CostCenterCode = AccountClassCodeTable.CostCenterCode
                AND HoldingPotReservationTable.AccountClassCode = AccountClassCodeTable.AccountClassCode
                AND HoldingPotReservationTable.Fpc = AccountClassCodeTable.Fpc
        JOIN',
            @UzaCodeReferenceTable,
                'ON UzaCodeReferenceTable.UzaCode = ApportionmentTable.UzaCode;
    ');

    PREPARE Statement FROM @SqlString;

    EXECUTE Statement USING
        @ApplicationId, /* AccountClassCode table subquery where clause */
        @ApplicationId, /* Apportionment table subquery where clause */
        @ApplicationId, /* HoldingPotReservation table subquery where clause */
        @UzaCodeReferenceVersion; /* UzaCodeReference table subquery where clause */

    DEALLOCATE PREPARE Statement;

END$$
DELIMITER ;
